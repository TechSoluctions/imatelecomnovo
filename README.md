ImaTelecom
==========

A Symfony project created on August 17, 2018, 7:40 pm.

# Propel
php app/console propel:database:reverse ima_telecom
php app/console propel:build


https://git-scm.com/book/pt-br/v1/Git-Essencial-Tagging

Criando TAGs no GIT
Para criar uma TAG chamada FRG83-R002 no commit atual execute:

git tag FRG83-R002
Para criar uma TAG chamada FRG83-R001 no commit 4b8ef995de6d77 execute:

git tag FRG83-R001 4b8ef995de6d77
Para submete-la para o servidor execute:

git push --tags origin master
Para deletar uma TAG execute:

git tag -d FRG83-R001
git push origin :refs/tags/FRG83-R001
Para voce mover para o commit da TAG execute:

git checkout FRG83-R001


# Docker
docker rm imatelecom
docker stop imatelecom
docker ps
docker build --pull -t registry.censanet.com.br/imatelecom:adamantium .
docker run --restart=unless-stopped -d -p 8096:80 --name imatelecom registry.censanet.com.br/imatelecom:adamantium