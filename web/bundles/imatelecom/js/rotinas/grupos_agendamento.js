/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {    
    var table = $('table.table').dataTable({
        lengthMenu: [[15, 50, -1], [15, 50, "All"]],
        order: [[1, "asc"]],
        columnDefs: [            
            {
                targets: [1, 2],
                render: $.fn.dataTable.render.ellipsis(35)
            },
            {
                targets: 3,
                orderable: false
            }
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        },
        responsive: true
    });

    new $.fn.dataTable.FixedHeader(table);
    

    var popup = new $.Popup($("section.main"));

    $('.open-popup').click(function (e) {
        e.preventDefault();
        
        var url = $(this).data('url');
        popup.Mostrar(url);

        return false;
    });

    $('.edicao-grupo').click(function (e) {
        e.preventDefault();

        popup.setTitle('Grupo de agendamento');

        return false;
    });
});