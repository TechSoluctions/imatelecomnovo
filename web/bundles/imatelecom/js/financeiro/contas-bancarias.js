/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var atualizarContainer = null;
var dialogContaBancaria = null;

$(document).ready(function (e) {
    $('table.table').dataTable({
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        }
    });
    
    $('body').on('click', '.ajax', function (e) {
        e.preventDefault();

        var title = $(this).data('title');
        var url = $(this).data('url');                

        $.ajax({
            url: url,
            type: 'GET',
            success: function (response, textStatus, jqXHR) {
                dialogContaBancaria = bootbox.dialog({
                    title: title,
                    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                    size: 'large',
                    onEscape: function () {                        
                    }
                });

                dialogContaBancaria.init(function () {
                    setTimeout(function () {
                        dialogContaBancaria.find('.bootbox-body').html(response);
                    }, 3000);
                });

                $('.modal-dialog').css('width:1000px !important');
            },
            error: function (request, textStatus, errorThrown) {
                console.log(request, textStatus, errorThrown);
                mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
            }
        });
    });
    
    $('.excluir-conta-bancaria').click(function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $("html").offset().top
        }, 500);

        let url = $(this).data('url');

        bootbox.confirm({
            title: "Exclusão da Conta Bancária",
            message: "Você realmente deseja Excluir esta Conta Bancária? Isto implicará nas Baixas Bancárias por causa da Conta Caixa.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar',
                    className: 'm-bottom-0 btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar',
                    className: 'm-left-20 btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        success: function (data, textStatus, jqXHR) {
                            if (data.status) {
                                let encode = Base64.encode(data.mensagem);
                                encode = encode.replaceAll('=', '');
                                window.location.href = data.redirect_url + '#' + encode;
                                window.location.reload();
                            } else {
                                mostrarNotificacao('Não foi possível executar esta ação!', data.erros.join('\n'));
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown);
                            mostrarNotificacao('Não foi possível executar esta ação!', errorThrown);
                        }
                    });
                }
            }
        });
    });
});