/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var atualizarContainer = 'convenios-rastreamento-contas';
var dialogContaBancaria = null;

function AtualizarConvenioRastreamentoContas() { // usado em contas-bancarias-conta.js
    let url = $('#contas_bancarias').data('url');
    let urlOpcoesEditar = $('#contas_bancarias').data('opcoes-editar');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (response, textStatus, jqXHR) {
            let tbody = '';

            $.each(response.contasBancarias, function (key, conta) {
                let tipoConta = conta.TipoConta === 'poupanca' ? 'Poupança' : 'Corrente';
                let urlEdicao = urlOpcoesEditar.replace('_IdbancoAgenciaConta', conta.IdbancoAgenciaConta);

                tbody += '<tr>';
                tbody += `<td class="centro">${conta.IdbancoAgenciaConta}</td>`;
                tbody += `<td>${conta.NomeBanco}</td>`;
                tbody += `<td>${conta.NomeAgencia}</td>`;
                tbody += `<td class="centro">${tipoConta}</td>`;
                tbody += `<td class="direita">${conta.NumeroConta}</td>`;
                tbody += `<td class="direita"><a href="#" data-url="${urlEdicao}" data-title="Cadastro da Conta Bancária" title="Cadastro da Conta Bancária" class="conta-bancaria col-separator"><i class="icon fa fa-edit" aria-hidden="true"></i></a></td>`;
                tbody += '</tr>';
            });

            $('#contas_bancarias tbody').html(tbody);
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
        }
    });
}

function CarregarContaBancaria(objeto) {
    var title = $(objeto).data('title');
    var url = $(objeto).data('url');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (response, textStatus, jqXHR) {
            dialogContaBancaria = bootbox.dialog({
                title: title,
                message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                size: 'large',
                onEscape: function () {
                }
            });

            dialogContaBancaria.init(function () {
                setTimeout(function () {
                    dialogContaBancaria.find('.bootbox-body').html(response);
                }, 3000);
            });

            $('.modal-dialog').css('width:1000px !important');
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
        }
    });
}