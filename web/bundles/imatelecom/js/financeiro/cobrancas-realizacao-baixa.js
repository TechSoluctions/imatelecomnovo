/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//var valorTotal = 0.0;
var valoresSelecionados = {};
var formasPagamentoAdicionado = {};

$(function () {
    $('.money').mask("#.##0,00", {reverse: true});

    $('body').on('reset', '#formulario_realizacao_baixa', function () {
        bootbox.hideAll();
    });

    $('#formulario_realizacao_baixa').submit(function (e) {
        e.preventDefault();
        
        let valoresFinais = AtualizarValoresLancamentos();
        
        if (Object.keys(formasPagamentoAdicionado).length === 0) {
            alert('Por favor, selecione ao menos uma Forma de Pagamento.')
            return ;
        }
        
        if (Object.keys(valoresFinais.descriminado).length === 0) {
            alert('Por favor, selecione ao menos um Lançamento.')
            return ;
        }
        
        let valorTotalFormasPagamento = 0.0;
        $.each(formasPagamentoAdicionado, function (key, dict) {
            valorTotalFormasPagamento += dict.Valor;
        });
                
        if (valorTotalFormasPagamento !== valoresFinais.consolidado.Total) {
            alert('Por favor, verifique se o Valor Total bate com os valores da Forma de Pagamento.')
            return ;
        }
        
        let dados = {};
        //dados.dataPagamento = $('#dataPagamento').val();
        dados.contaCaixa = $('#contaCaixa').val();
        dados.dataBaixa = $('#dataBaixa').val();
        dados.valorConsolidado = valoresFinais.consolidado;
        dados.valorDescriminado = valoresFinais.descriminado;
        dados.formaPagamento = formasPagamentoAdicionado;
        dados.descricaoMovimento = $('#descricaoMovimento').val();
        
        let url = $(this).attr('action');
        let type = $(this).attr('method');
        
        $.ajax({
            url: url,
            type: type,
            data: dados,
            success: function (data, textStatus, jqXHR) {
                if (data.status) {
                    alert(data.mensagem);
                } else {
                    alert(data.erros.join('\n'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    });

    moment.locale('pt-br'); // default the locale to Portuguese Brazil
    $('#dataBaixa').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
    
    $('#dataPagamento').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $('#contaCaixa').chosen();
    $('#formaPagamento').chosen();

    TemNumeroDocumento('#formaPagamento');
    $('body').on('change', '#formaPagamento', function () {
        TemNumeroDocumento(this);
    });

    AtualizarValoresGlobais();

    $('body').on('click', '.selecionar-lancamento', function () {
        let container = $(this).data('container-linha');
        AtualizarValoresGlobais();
    });

    $('body').on('keyup', '.alteracao-valor-lancamento', function () {
        let container = $(this).data('container-linha');
        AtualizarValoresPorLancamento(container);
        AtualizarValoresGlobais();
    });

    $('body').on('keyup', '.alteracao-lancamentos', function () {
        AtualizarValoresLancamentos();
    });

    let valoresAtuais = AtualizarValoresLancamentos();
    $('#valorFormaPagamento').val(valoresAtuais.consolidado.Total);
});


function TemNumeroDocumento(objeto) {
    let temNumeroDocumento = $(objeto).find('option:selected').data('tem-numero-documento');
    if (temNumeroDocumento) {
        $('#container_numero_documento').removeClass('oculto');
    } else {
        $('#container_numero_documento').addClass('oculto');
    }
}

function AtualizarValoresPorLancamento(objeto) {
    let idTotal = $(objeto).data('id-valor-total');
    let valores = ValorLinha(objeto);

    if (valores.TemValor) {
        $(idTotal).data('valor-total', valores.Final.format(2));
        $(idTotal).text('R$ ' + valores.Final.format(2));
    }
}

function ValorLinha(objeto) {
    let idOriginal = $(objeto).data('id-valor-original');
    let idMulta = $(objeto).data('id-valor-multa');
    let idJuros = $(objeto).data('id-valor-juros');
    let idDesconto = $(objeto).data('id-valor-desconto');
    let idTotal = $(objeto).data('id-valor-total');

    let valores = {};
    valores.TemValor = false;

    if (idOriginal !== undefined) {
        valores.TemValor = true;
        valores.Original = parseFloat($(idOriginal).data('valor-original').replaceAll(',', ''));
        valores.Multa = parseFloat($(idMulta).val().replaceAll('.', '').replaceAll(',', '.'));
        valores.Juros = parseFloat($(idJuros).val().replaceAll('.', '').replaceAll(',', '.'));
        valores.Desconto = parseFloat($(idDesconto).val().replaceAll('.', '').replaceAll(',', '.'));
        valores.Total = $(idTotal).data('valor-total').replaceAll(',', '');

        if (isNaN(valores.Multa)) {
            valores.Multa = 0.00;
        }

        if (isNaN(valores.Juros)) {
            valores.Juros = 0.00;
        }

        if (isNaN(valores.Desconto)) {
            valores.Desconto = 0.00;
        }

        valores.Final = valores.Original + valores.Multa + valores.Juros - valores.Desconto;
    }

    return valores;
}

// o javascript está sendo executado mais de uma vez por causa do popup, por isso o dict
function AtualizarValoresGlobais() {
    let valoresFinais = {};
    valoresFinais.Original = 0.0;
    valoresFinais.Multa = 0.0;
    valoresFinais.Juros = 0.0;
    valoresFinais.Desconto = 0.0;
    valoresFinais.Total = 0.0;
    valoresFinais.Final = 0.0;

    $.each($('.selecionar-lancamento'), function (index, objeto) {
        let container = $(objeto).data('container-linha');
        let valoresLinha = ValorLinha(container);

        if (valoresLinha.TemValor) {
            let id = $(container).data('id-lancamento');
            let checkbox = $(container).data('id-checkbox');
            let isChecked = $(checkbox).is(':checked');

            if (isChecked) {
                if (!(id in valoresSelecionados)) {
                    valoresSelecionados[id] = valoresLinha;
                }

                valoresFinais.Original += valoresLinha.Original;
                valoresFinais.Multa += valoresLinha.Multa;
                valoresFinais.Juros += valoresLinha.Juros;
                valoresFinais.Desconto += valoresLinha.Desconto;
                //valoresFinais.Total += valoresLinha.Total;
                valoresFinais.Final += valoresLinha.Final;
            } else if (!isChecked && id in valoresSelecionados) {
                delete valoresSelecionados[id];
            }
        }
    });
    
    $('#valorOriginal').val(valoresFinais.Original.format(2));
    $('#valorMulta').val(valoresFinais.Multa.format(2));
    $('#valorJuros').val(valoresFinais.Juros.format(2));
    $('#valorDesconto').val(valoresFinais.Desconto.format(2));
    $('#valorTotal').val(valoresFinais.Final.format(2));
}

function AtualizarValoresLancamentos() {
    let valores = {};
    valores.Original = parseFloat($('#valorOriginal').val().replaceAll(',', ''));
    valores.Multa = parseFloat($('#valorMulta').val().replaceAll('.', '').replaceAll(',', '.'));
    valores.Juros = parseFloat($('#valorJuros').val().replaceAll('.', '').replaceAll(',', '.'));
    valores.Desconto = parseFloat($('#valorDesconto').val().replaceAll('.', '').replaceAll(',', '.'));
    valores.Total = parseFloat($('#valorTotal').val().replaceAll(',', ''));

    if (isNaN(valores.Multa)) {
        valores.Multa = 0.00;
    }

    if (isNaN(valores.Juros)) {
        valores.Juros = 0.00;
    }

    if (isNaN(valores.Desconto)) {
        valores.Desconto = 0.00;
    }

    valores.Final = valores.Original + valores.Multa + valores.Juros - valores.Desconto;

    $('#valorTotal').val(valores.Final.format(2));

    let valoresDescriminados = [];
    var totalLancamentosSelecionados = $('.selecionar-lancamento:checked').length;
    $.each($('.selecionar-lancamento:checked'), function (index, objeto) {
        let container = $(objeto).data('container-linha');
        let valoresLinha = ValorLinha(container);

        valoresLinha.Multa = valores.Multa / totalLancamentosSelecionados;
        valoresLinha.Juros = valores.Juros / totalLancamentosSelecionados;
        valoresLinha.Desconto = valores.Desconto / totalLancamentosSelecionados;
        valoresLinha.Final = valoresLinha.Original + valoresLinha.Multa + valoresLinha.Juros - valoresLinha.Desconto;

        let idLancamento = $(container).data('id-lancamento');
        let idMulta = $(container).data('id-valor-multa');
        let idJuros = $(container).data('id-valor-juros');
        let idDesconto = $(container).data('id-valor-desconto');
        let idTotal = $(container).data('id-valor-total');

        $(idMulta).val(valoresLinha.Multa.format(2));
        $(idJuros).val(valoresLinha.Juros.format(2));
        $(idDesconto).val(valoresLinha.Desconto.format(2));
        $(idTotal).data('valor-total', valoresLinha.Final.format(2));
        $(idTotal).text('R$ ' + valoresLinha.Final.format(2));
        
        valoresLinha.Lancamento = idLancamento;
        valoresDescriminados.push(valoresLinha);
    });
    
    return {
        consolidado: valores,
        descriminado: valoresDescriminados
    };
}

function AdicionarFormaPagamento(objeto) {
    let formaPagamento = $('#formaPagamento').val(); 
    let detalhesPagamento = {};
    detalhesPagamento.TemNumeroDocumento = $('#formaPagamento').find('option:selected').data('tem-numero-documento');
    detalhesPagamento.Tipo = formaPagamento;
    detalhesPagamento.NumeroDocumento = $('#numeroDocumento').val();
    //detalhesPagamento.Valor = parseFloat($('#valorFormaPagamento').val().replaceAll('.', '').replaceAll(',', '.'));
    detalhesPagamento.Valor = $('#valorFormaPagamento').val();
    
    if (detalhesPagamento.Valor.includes(',')) {
        detalhesPagamento.Valor = parseFloat($('#valorFormaPagamento').val().replaceAll('.', '').replaceAll(',', '.'));
    } else {
        detalhesPagamento.Valor = parseFloat($('#valorFormaPagamento').val());
    }
    
    let row = '<tr id="'+detalhesPagamento.Tipo+'">';
    let nomeFormaPagamento = $('#formaPagamento').find('option:selected').text();
    let tipo = '<td class="centro">' + nomeFormaPagamento + '</td>';
    let numeroDocumento = '<td class="direita">' + detalhesPagamento.NumeroDocumento + '</td>';
    let valor = '<td class="direita">R$ ' + detalhesPagamento.Valor + '</td>';
    let opcoes = '<td class="direita"><a href="javascript:ExcluirFormaPagamento('+detalhesPagamento.Tipo+');" class="col-separator" title="Excluir Forma de Pagamento"><i class="icon fa fa-trash-o" aria-hidden="true"></i></a></td>';

    row += tipo + numeroDocumento + valor + opcoes + '</tr>';           

    let mensagemErro = '';
    let teveErros = false;
    
    if (isNaN(detalhesPagamento.Valor) || detalhesPagamento.Valor === 0.0) {
        teveErros = true;
        mensagemErro = mensagemErro + 'Valor do Pagamento não pode estar vazio.\n';
    }
    
    if (detalhesPagamento.TemNumeroDocumento && (isEmptyOrSpaces(detalhesPagamento.NumeroDocumento) || isNaN(detalhesPagamento.NumeroDocumento))) {
        teveErros = true;
        mensagemErro = mensagemErro + 'Número do Documento não pode estar vazio.\n';
    }
        
    if (!(formaPagamento in formasPagamentoAdicionado) && !teveErros) {
        $('#formas_pagamento > tbody:last-child').append(row);
        formasPagamentoAdicionado[formaPagamento] = detalhesPagamento;
        $('#valorFormaPagamento').val('');
    } else {
        if (!teveErros) {
            mensagemErro = mensagemErro + 'Forma de Pagamento já foi adicionado.';
        }
        
        alert(mensagemErro);
    }
}

function ExcluirFormaPagamento(formaPagamento) {
    if (formaPagamento in formasPagamentoAdicionado) {
        $('table tbody tr#' + formaPagamento).remove();
        delete formasPagamentoAdicionado[formaPagamento];
    }
}