/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {
    $('body').confirmation({
        selector: '[data-toggle="confirmation"]'
    });

    var table = $('.table').dataTable({
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        }
    });

    $('body').on('confirm.bs.confirmation', '.confirmation-excluir', function () {
        var url = $(this).data('url');
        $.ajax({
            url: url,
            type: 'POST',
            success: function (response, textStatus, jqXHR) {
                if (response.status) {
                    var encode = Base64.encode(response.mensagem);
                    encode = encode.replaceAll('=', '');
                    window.location.href = response.redirect_url + '#' + encode;
                    location.reload();
                } else {
                    var erros = response.erros.join('<br/>');
                    mostrarNotificacao('Não foi possível excluir!', erros);
                }
            },
            error: function (request, textStatus, errorThrown) {
                console.log(request, textStatus, errorThrown);
                mostrarNotificacao('Não foi possível excluir!', errorThrown);
            }
        });
    });
});