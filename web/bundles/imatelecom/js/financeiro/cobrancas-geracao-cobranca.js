/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('.chosen-select').chosen();

    moment.locale('pt-br'); // default the locale to Portuguese Brazil
    $('#vencimento').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $('body').on('reset', '#formulario_geracao_cobrancas', function () {
        bootbox.hideAll();
    });

    $('.forcar-vencimento').on('change', function (e) {
        $('#container_vencimento').toggleClass('oculto');
    });

    $('#formulario_geracao_cobrancas').submit(function (e) {
        e.preventDefault();

        $('#container_alerta').addClass('oculto');

        var url = $(this).attr('action');
        var type = $(this).attr('method');

        var notify = $.notify({
            title: '<strong>Gerando Cobranças</strong><br/>',
            message: 'Não feche esta página...'
        },
                {
                    type: 'info',
                    allow_dismiss: false,
                    showProgressbar: true,
                    progress: '0',
                    delay: 300000 // 5 minutos
                });

        setTimeout(function () {
            notify.update('message', 'Verificando erros.');
            gerarCobrancas(notify, type, url);
        }, 2000);
    });

    function gerarCobrancas(notify, type, url) {
        var status = true;
        var data = $('#formulario_geracao_cobrancas').serialize();

        $.ajax({
            url: url,
            type: type,
            data: data,
            beforeSend: function ()
            {
                notify.update('message', 'Acessando o servidor. Por favor, espere!!.');
            },
            success: function (response, textStatus, jqXHR) {
                var isDownloaded = typeof (response) === 'string';
                setTimeout(function () {
                    notify.update('delay', '5000');
                    if (isDownloaded === false) {
                        if (response.status === true) {
                            notify.update('type', 'success');
                        } else {
                            notify.update('type', 'danger');
                        }
                    } else {
                        notify.update('type', 'success');
                        mostrarMensagemDownload('Download do arquivo foi iniciado com sucesso!');
                    }

                    notify.update('progress', '100');
                    notify.close();
                }, 2500);

                setTimeout(function () {
                    if (isDownloaded === false && response.status === false) {
                        alert(response.erros.join('\n'));
                    } else if (isDownloaded === true) {
                        DownloadFile(response, textStatus, jqXHR);
                    }
                }, 3000);
            },
            error: function (request, textStatus, errorThrown) {
                setTimeout(function () {
                    notify.update('delay', '5000');
                    notify.update('type', 'danger');
                    notify.update('progress', '100');
                    notify.close();
                }, 2500);

                setTimeout(function () {
                    alert(errorThrown);
                }, 3000);
            }
        });
    }

    function mostrarMensagemDownload(mensagem) {
        $('#mensagem_alerta').text(mensagem);
        $('#container_alerta').removeClass('oculto');
        setTimeout(function () {
            $('#container_alerta').addClass('oculto');
        }, 30000);
    }

    $('.tipo-cobranca-geracao').change(function (e) {
        var tipo = $(this).val();
        if (tipo == 'todos') {
            $('#container_cliente').hide();
            $('#container_tipo_pessoa').show();
        } else {
            $('#container_tipo_pessoa').hide();
            $('#container_cliente').show();
        }
    });
});