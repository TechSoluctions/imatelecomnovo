/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {
    var table = $('table.table').dataTable({
        lengthMenu: [[15, 50, -1], [15, 50, "Todos"]],
        order: [[0, "desc"]],
        columnDefs: [
            {
                targets: [3],
                orderable: false
            }
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        },
        responsive: true
    });

    new $.fn.dataTable.FixedHeader(table);

    $('.open-popup').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        var popup = new $.Popup($("section.main"));
        popup.Mostrar(url);
        popup.setTitle('Informações sobre a Competência');

        return false;
    });

    $('.section.popup a.close-popup').click(function (e) {
        e.preventDefault();

        $('section.main section.open-popup').remove();

        return false;
    });
});