/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 $(document).ready(function () {
 
 moment.locale('pt-br'); // default the locale to Portuguese Brazil
 $('#dataVencimento').daterangepicker({
 singleDatePicker: true,
 showDropdowns: true,
 locale: {
 format: 'DD/MM/YYYY'
 }
 });
 
 $('#selecionar_competencia').change(function () {
 var url = $(this).val();
 window.location.href = url;
 });
 
 var table = $('table.table').dataTable({
 language: {
 url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
 },
 responsive: true,
 order: [[1, "asc"]]
 });
 
 new $.fn.dataTable.FixedHeader(table);
 
 $('#gerar_cobrancas').click(function (e) {
 e.preventDefault();
 
 var url = $(this).attr('href');
 var forcarVencimento = $('.forcar-vencimento').filter(':checked').val();
 var vencimento = $('#dataVencimento').val();
 var data = {};
 data.forcarVencimento = forcarVencimento;
 data.vencimento = vencimento;
 
 $.ajax({
 url: url,
 type: 'POST',
 data: data,
 success: function (response, status, xhr) {
 DownloadFile(response, status, xhr);
 window.location.reload();
 },
 error: function (e) {
 console.log(e);
 },
 complete: function (jqXHR, textStatus) {
 console.log(jqXHR, textStatus);
 }
 });
 });
 
 $('.excluir-cobranca').click(function (e) {
 var url = $(this).data('url');
 
 $.ajax({
 url: url,
 type: 'POST',
 success: function (response, status, xhr) {
 alert(response.mensagem);
 window.location.reload();
 },
 error: function (e) {
 console.log(e);
 },
 complete: function (jqXHR, textStatus) {
 console.log(jqXHR, textStatus);
 }
 });
 });
 
 $('#container_vencimento').hide();
 $('.forcar-vencimento').click(function () {
 var forcarVencimento = $(this).val();
 if (forcarVencimento == '1') {
 $('#container_vencimento').show();
 } else {
 $('#container_vencimento').hide();
 }
 });
 });*/

$(document).ready(function (e) {
    $('.chosen-select').chosen();
    $('#competencia, #tipo_lancamento').change(function () {
        var url = $(this).find(':selected').data('url');
        window.location.href = url;
    });

    $('body').on('click', '.ajax', function (e) {
        e.preventDefault();

        let url = $(this).data('url');
        let titulo = $(this).data('title');
        let tipo = $(this).data('tipo');
        let data = {};
        let size = 'medium';

        if (tipo === 'baixa' || tipo === 'estorno') {
            data.lancamentos = Object.keys(cobrancasSelecionadas);

            if (data.lancamentos.length == 0) {
                alert('Por favor, selecione algum lançamento.');
                return;
            }
            
            size = 'large';
        }
        
        $.ajax({
            url: url,
            type: 'GET',
            data: data,
            success: function (response, textStatus, xhr) {
                var dialog = bootbox.dialog({
                    title: titulo,
                    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                    size: size,
                    onEscape: function () {
                        location.reload();
                    }
                });

                dialog.init(function () {
                    setTimeout(function () {
                        dialog.find('.bootbox-body').html(response);
                    }, 3000);
                });

                $('.modal-dialog').css('width:1000px !important')
            }
        });
    });
});