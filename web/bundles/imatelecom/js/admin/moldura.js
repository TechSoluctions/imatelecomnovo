/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {
    $("ul#links li a").click(function (e) {
        var subMenuId = '#' + $(this).data('menu');
        if (subMenuId != '#undefined') {
            e.preventDefault();

            if ($(subMenuId).is(':visible')) {
                $(this).removeClass('selected');
                $(subMenuId).slideUp("slow");
            } else {
                $(this).addClass('selected');
                $(subMenuId).slideDown("slow");
            }
        }
    });

    $("a#mostrar_dados_competencia").click(function (e) {
        e.preventDefault();

        $("section.main header#detalhes_competencia table tbody").toggleClass('oculto');

        return false;
    });

    $(".no-clicked").click(function (e) {
        e.preventDefault();
        return false;
    });

    $("div.alert").on("click", "button.close", function () {
        //$(this).parent().animate({opacity: 0}, 500).hide('slow');
        $(this).parent().addClass('oculto').animate({opacity: 0}, 500).slideUp('slow');
    });

    var table = $('table.table');

    if ($.fn.dataTable != null) {
        $.fn.dataTable.render.ellipsis = function (cutoff) {
            return function (data, type, row) {
                return type === 'display' && data.length > cutoff ?
                        data.substr(0, cutoff) + '…' :
                        data;
            };
        };
    }

    $("a.menu-ajax").on('click', function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        $.ajax({
            url: url,
            type: 'POST',
            async: false,
            beforeSend: function ()
            {
                $("section.main").append("<section class='loading'/>");
            },
            success: function (data, textStatus, jqXHR) {
                $("section.loading").remove();
                console.log(data, textStatus, jqXHR);

                if (data.status == true) {
                    $("#alerta_topo").removeClass('alert-danger');
                    $("#alerta_mensagem").text(data.mensagem);
                    $("#alerta_topo").addClass('alert-success');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-success', 15000);
                } else {
                    var erros = "";
                    $.each(data.erros, function (key, erro) {
                        erros += "<br/>" + erro;
                    });

                    $("#alerta_mensagem").html(erros);
                    $("#alerta_topo").addClass('alert-danger');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-danger', 15000);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);

                $("section.loading").remove();
                $("#alerta_mensagem").text(errorThrown);
                $("#alerta_topo").addClass('alert-danger');
                $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                MostrarAlertaTopo('alert-success', 15000);
            }
        });

        return false;
    });



});

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

function Capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function MostrarAlertaTopo(tipoClasse, tempoDeEspera) {
    setTimeout(function () {
        $("div.alert").slideUp(500, function () {
            $(this).addClass('oculto').animate({opacity: 0}, 500).slideUp('slow');
            $("#alerta_topo").removeClass(tipoClasse);
        });
    }, tempoDeEspera);
}

function DownloadFile(response, status, xhr) {
    // check for a filename
    var filename = "";
    var disposition = xhr.getResponseHeader('Content-Disposition');
    if (disposition && disposition.indexOf('attachment') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1])
            filename = matches[1].replace(/['"]/g, '');
    }
    
    console.log(filename);

    var type = xhr.getResponseHeader('Content-Type');
    var blob = new Blob([response], {type: type});

    if (typeof window.navigator.msSaveBlob !== 'undefined') {
        // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
        window.navigator.msSaveBlob(blob, filename);
    } else {
        var URL = window.URL || window.webkitURL;
        var downloadUrl = URL.createObjectURL(blob);

        if (filename) {
            // use HTML5 a[download] attribute to specify filename
            var a = document.createElement("a");
            // safari doesn't support this yet
            if (typeof a.download === 'undefined') {
                window.location = downloadUrl;
            } else {
                a.href = downloadUrl;
                a.download = filename;
                document.body.appendChild(a);
                a.click();
            }
        } else {
            window.location = downloadUrl;
        }

        setTimeout(function () {
            URL.revokeObjectURL(downloadUrl);
        }, 100); // cleanup
    }
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function buscarEnderecoPorCep(cep_code) {
    if (cep_code.length <= 0)
        return;

    $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", {code: cep_code},
            function (result) {
//                    if (result.status != 1) {
//                        alert(result.message || "Houve um erro desconhecido");
//                        return;
//                    }

                //$("input#inputCep").val(result.code);
                console.log(result);

                if (result.status == 1) {
                    var city = result.city.replaceAll(' ', '_');
                    var value = $("#chosenSelectCidade").children("option[data-value='" + city + "']").val(); //("option[data-value='"+ city +"'");
                    $("#chosenSelectCidade").val(value).trigger("chosen:updated");

                    $("input#inputEndereco").val(result.address);
                    $("input#inputBairro").val(result.district);
                    $("input#inputCidade").val(result.city);
                    $("select#selectUf").val(result.state);
                } else {
                    alert(result.message);
                }
            });
}