/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//$(document).ready(function () {

/*
 * Dashboard - eCommerce
 */

function valorPorMesJson() {
    let data = [];
    let meses = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    for (let mes in meses) {
        let index = parseInt(mes) + 1;
        data[index] = 0;
    }

    return data;
}

function getValorFaturamento(faturamentoAnual) {
    let data = [];
    for (let mes in faturamentoAnual) {
        data.push(faturamentoAnual[mes]);
    }

    return data;
}

function graficoFaturamentoAnual(dataJson) {
    dataJson = JSON.parse(dataJson.replaceAll('&quot;', '"'));

    let faturamento = valorPorMesJson();
    for (let key in dataJson.Boletos) {
        faturamento[dataJson.Boletos[key].MesVencimento] = dataJson.Boletos[key].ValorTotal;
    }

    let data = getValorFaturamento(faturamento);
    var ctx = document.getElementById("thisYearRevenue"); //.getContext("2d");

    var data1 = {
        labels: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        datasets: [{/*
         label: "This year dataset",
         fillColor: "#28a745",
         strokeColor: "#28a745",
         pointColor: "transparent",
         pointStrokeColor: "transparent",
         pointHighlightFill: "#fff",
         pointHighlightStroke: "#9C2E9D",
         
         */
                label: "Faturamento Mensal",
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgba(255,99,132,1)',
                /*
                 backgroundColor: [
                 'rgba(255, 99, 132, 0.2)',
                 'rgba(54, 162, 235, 0.2)',
                 'rgba(255, 206, 86, 0.2)',
                 'rgba(75, 192, 192, 0.2)',
                 'rgba(153, 102, 255, 0.2)',
                 'rgba(255, 159, 64, 0.2)'
                 ],
                 borderColor: [
                 'rgba(255,99,132,1)',
                 'rgba(54, 162, 235, 1)',
                 'rgba(255, 206, 86, 1)',
                 'rgba(75, 192, 192, 1)',
                 'rgba(153, 102, 255, 1)',
                 'rgba(255, 159, 64, 1)'
                 ],
                 */
                borderWidth: 1,
                pointColor: "transparent",
                pointStrokeColor: "transparent",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "#9C2E9D",
                data: data
            }]
    };

    let options = {
        scales: {
            yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
        },
        tooltips: {
            callbacks: {
                label: function (tooltipItem, data) {
                    var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    if (label) {
                        label += ': ';
                    }
                    label += Math.round(tooltipItem.yLabel * 100) / 100;
                    return label;
                }
            }
        }
    };

    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data1,
        options: options
    });


    /*
     var myChart = new Chart(ctx, {
     type: 'line',
     data: {
     labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
     datasets: [{
     label: '# of Votes',
     data: [12, 19, 3, 5, 2, 3],
     backgroundColor: [
     'rgba(255, 99, 132, 0.2)',
     'rgba(54, 162, 235, 0.2)',
     'rgba(255, 206, 86, 0.2)',
     'rgba(75, 192, 192, 0.2)',
     'rgba(153, 102, 255, 0.2)',
     'rgba(255, 159, 64, 0.2)'
     ],
     borderColor: [
     'rgba(255,99,132,1)',
     'rgba(54, 162, 235, 1)',
     'rgba(255, 206, 86, 1)',
     'rgba(75, 192, 192, 1)',
     'rgba(153, 102, 255, 1)',
     'rgba(255, 159, 64, 1)'
     ],
     borderWidth: 1
     }]
     },
     options: {
     scales: {
     yAxes: [{
     ticks: {
     beginAtZero: true
     }
     }]
     }
     }
     });*/
}

$(document).ready(function (e) {
$('body').on('click', '.ajax', function (e) {
    e.preventDefault();

    var title = $(this).data('title');
    var url = $(this).data('url');
    var refresh = $(this).data('refresh');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (response, textStatus, jqXHR) {
            var dialog = bootbox.dialog({
                title: title,
                message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                //size: 'large',
                onEscape: function () {
                    // you can do anything here you want when the user dismisses dialog
                    console.log("Fechando");
                    if (refresh !== false) {
                        location.reload();
                    }
                }
            });

            dialog.init(function () {
                setTimeout(function () {
                    dialog.find('.bootbox-body').html(response);
                }, 3000);
            });

            $('.modal-dialog').css('width:1000px !important');
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
        }
    });
});
});