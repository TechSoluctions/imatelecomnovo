/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    $.Popup = function (element) {
        var container = (element instanceof $) ? element : $(element);

        this.mostrar = function (url) {
            this.openModal();
            this.setTitle('teste')
            this.closeModal();

            $.ajax({
                url: url,
                type: 'GET',
                success: function (response, textStatus, jqXHR) {
                    if (textStatus === 'success') {
                        container.children('section.popup').children('div.content-popup').html(response);                        
                    }
                }, error: function (response, textStatus, jqXHR) {
                    console.log(response, textStatus, jqXHR);
                }
            });
        },
                this.setTitle = function (title) {
                    $('.popup-title').text(title);
                },
                this.openModal = function () {
                    $('body').addClass('no-scrolled');
                    container.children('section.popup').remove();
                    container.append("\
                        <section class='popup'>\n\
                            <div class='content-header-popup'>\n\
                                <h3 class='popup-title'></h3>\n\
                                <a class='close-popup direita' href='#'>X</a>\n\
                            </div>\n\
                            <div class='content-popup'></div>\n\
                        </section>\n\
                    ");
                    
                    container.parent().addClass('avoid-clicks-parent');
                    container.children('section.popup').addClass('avoid-clicks-child');
                    $('div.daterangepicker').addClass('avoid-clicks-child');
                },
                this.closeModal = function () {
                    $('.close-popup').on('click', function (e) {
                        e.preventDefault();

                        $('body').removeClass('no-scrolled');
                        container.children('section.popup').remove();
                        container.parent().removeClass('avoid-clicks-parent');
                    });
                },
                this.close = function () {
                    $('body').removeClass('no-scrolled');
                    container.children('section.popup').remove();
                    container.parent().removeClass('avoid-clicks-parent');
                },
                this.loading = function () {
                },
                this.settingsQuestion = function (title, message) {
                    $('body').addClass('no-scrolled');
                    container.children('section.popup-alert').remove();
                    container.append("\
                        <section class='popup-alert question'>\n\
                            <div class='content-header-popup'>\n\
                                <h3 class='popup-alert-title'>" + title + "</h3>\n\
                            </div>\n\
                            <div class='content-popup'>\n\
                                <div class='popup-alert-message'>" + message + "</div>\n\
                                <div class='options direita'>\n\
                                    <a href='javascript:void(0);' class='popup-button popup-question-close'>\n\
                                        Cancelar\n\
                                    </a>\n\
                                    <a href='javascript:void(0);' class='popup-button popup-question-confirm'>\n\
                                        Confirmar\n\
                                    </a>\n\
                                </div>\n\
                            </div>\n\
                        </section>\n\
                    ");
                    
                    container.parent().addClass('avoid-clicks-parent');
                    container.children('section.popup-alert.question').addClass('avoid-clicks-child');
                    $('div.daterangepicker').addClass('avoid-clicks-child');
                   
                    $('.popup-question-close').click(function (e) {
                        e.preventDefault();

                        container.parent().removeClass('avoid-clicks-parent');
                        $("section.popup-alert.question").remove();
                        return false;
                    });
                },
                this.question = function (title, message, url) {
                    this.questionRedirect(title, message, url, true, '');
                },
                this.questionRedirect = function (title, message, url, refresh, urlRedirect) {
                    this.settingsQuestion(title, message);

                    $('.popup-question-confirm').click(function (e) {
                        e.preventDefault();

                        container.parent().removeClass('avoid-clicks-parent');
                        $.ajax({
                            url: url,
                            type: 'POST',
                            success: function (response, textStatus, jqXHR) {
                                $("section.popup-alert.question").remove();

                                if (response.status == true) {
                                    $("#alerta_topo").removeClass('alert-danger');
                                    $("#alerta_mensagem").text(response.mensagem);
                                    $("#alerta_topo").addClass('alert-success');
                                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                                    if (refresh) {
                                        if (urlRedirect !== null && urlRedirect !== undefined && urlRedirect !== '')
                                            $("body").load(urlRedirect);
                                        else
                                            $("body").load(window.location.href);
                                    } else {
                                        MostrarAlertaTopo('alert-success', 5000);
                                    }
                                } else {
                                    var erros = "";
                                    $.each(response.erros, function (key, erro) {
                                        erros += "<br/>" + erro;
                                    });

                                    $("#alerta_mensagem").html(erros);
                                    $("#alerta_topo").addClass('alert-danger');
                                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                                    MostrarAlertaTopo('alert-danger', 15000);
                                }
                            }, error: function (response, textStatus, jqXHR) {
                                console.log(response, textStatus, jqXHR);
                            }
                        });

                        return false;
                    });
                },
                this.alert = function (title, message) {
                    this.settingsQuestion(title, message);

                    $('.popup-question-confirm').click(function (e) {
                        e.preventDefault();                       
                        container.children('section.popup-alert').remove();
                        container.parent().removeClass('avoid-clicks-parent');
                        
                        return false;
                    });
                }
    };

    $.Popup.prototype = {
        Mostrar: function (url) {
            this.mostrar(url);
        },
        Close: function () {
            this.close();
        },
        setTitle: function (title) {
            this.setTitle(title);
        },
        Question: function (title, message, url) {
            this.question(title, message, url);
        },
        QuestionRedirect: function (title, message, url, refresh, urlRedirect) {
            this.questionRedirect(title, message, url, refresh, urlRedirect);
        },
        Alert: function (title, message) {
            this.alert(title, message);
        }
        /*
         InitEvents: function () {
         //`this` references the instance object inside of an instace's method,
         //however `this` is set to reference a DOM element inside jQuery event
         //handler functions' scope. So we take advantage of JS's lexical scope
         //and assign the `this` reference to another variable that we can access
         //inside the jQuery handlers
         var that = this;
         console.log("OI");
         }*/
    };

    $.Popup.defaultOptions = {
        container: ""
    };
}($));