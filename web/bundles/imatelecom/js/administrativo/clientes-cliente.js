/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var currentTab = null;
$(document).ready(function () {
    $('#container_tabs a.active').tab('show');
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        currentTab = $(e.target);
        var target = $(e.target).data("url"); // activated tab
        var container = $(e.target).attr("href"); // activated tab

        if (target !== undefined) {
            //LimparAbas();
            AtualizarAba(container, target);
        }
    });

    $('body').on('click', '.ajax', function (e) {
        e.preventDefault();

        var title = $(this).data('title');
        var url = $(this).data('url');
        var refresh = $(this).data('refresh');

        let containerAtualizacao = currentTab.attr('href');
        let urlAtualizacao = currentTab.data('url');

        $.ajax({
            url: url,
            type: 'GET',
            success: function (response, textStatus, jqXHR) {
                var dialog = bootbox.dialog({
                    title: title,
                    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                    size: 'large',
                    onEscape: function () {
                        if (refresh !== false) {
                            AtualizarAba(containerAtualizacao, urlAtualizacao);
                        }
                    }
                });

                dialog.init(function () {
                    setTimeout(function () {
                        dialog.find('.bootbox-body').html(response);
                    }, 3000);
                });

                $('.modal-dialog').css('width:1000px !important');
            },
            error: function (request, textStatus, errorThrown) {
                console.log(request, textStatus, errorThrown);
                mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
            }
        });
    });
});

function mostrarMensagem(className, titulo, subtitulo, mensagem) {
    $('#alerta_status_popup').attr('class', 'alert alert-' + className);
    $('#alerta_titulo_popup').text(titulo);
    $('#alerta_subtitulo_popup').text(subtitulo);
    $('#erros_alerta_popup').html(mensagem);
    $('#container_alerta_popup').removeClass('oculto');
    setTimeout(function () {
        $('#container_alerta_popup').addClass('oculto');
    }, 30000);
}

function AtualizarDadosPessoais() {
    let container = $('#cliente-tab').attr('href');
    let url = $('#cliente-tab').data("url");

    AtualizarAba(container, url);
}

function AtualizarFornecedor() {
    let container = $('#fornecedor-tab').attr('href');
    let url = $('#fornecedor-tab').data("url");

    AtualizarAba(container, url);
}

function AtualizarTelefones() {
    let container = $('#telefones-tab').attr('href');
    let url = $('#telefones-tab').data("url");

    AtualizarAba(container, url);
}

function AtualizarServicos() {
    let container = $('#servicos-tab').attr('href');
    let url = $('#servicos-tab').data("url");

    AtualizarAba(container, url);
}

function AtualizarCobrancas() {
    let container = $('#cobrancas-tab').attr('href');
    let url = $('#cobrancas-tab').data("url");

    AtualizarAba(container, url);
}

function AtualizarNotasFiscais() {
    let container = $('#notas-fiscais-tab').attr('href');
    let url = $('#notas-fiscais-tab').data("url");

    AtualizarAba(container, url);
}

function AtualizarAba(container, url) {
    let isAdded = $(container).has("div").length;
    if (isAdded >= 0) {        
        $.ajax({
            url: url,
            type: 'GET',
            cache: false,
            beforeSend: function ()
            {
                $(container).html('<p class="m-top-15 centro"><i class="fa fa-spin fa-spinner"></i> Loading...</p>');
            },
            success: function (response, textStatus, jqXHR) {
                $(container).children().detach();
                $(container).html(response);
            },
            error: function (request, textStatus, errorThrown) {
                mostrarNotificacao('Não foi possível carregar a aba!', errorThrown);
            }
        });
    }
}

function LimparAbas() {
    let abas = $('#myTabContent').children();
    //console.log(abas);
    $.each(abas, function () {
        $(this).empty();
    });
}

function cancelarTelefone() {
    $('#telefoneId').val("");
}

function adicionarTelefone(objeto) {
    $('html, body').animate({
        scrollTop: $("html").offset().top
    }, 500);

    let url = $(objeto).attr('action');
    let type = $(objeto).attr('method');
    let data = $(objeto).serializeArray();

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response, textStatus, jqXHR) {
            if (response.status) {
                mostrarNotificacao('Beem Feito', response.mensagem, 5000, 'success');
                AtualizarTelefones();
            } else {
                mostrarNotificacao('Ops... Aconteceu algo de errado!',
                        'Por favor, verifique os erros ocorridos que foram listados abaixo. <br/><br/>' +
                        response.erros.join('\n'));
            }
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarNotificacao('Ops... Aconteceu algo de errado!',
                    'Por favor, verifique os erros ocorridos que foram listados abaixo. <br/><br/>' + errorThrown);
        }
    });
}

function excluirTelefone(objeto) {
    $('html, body').animate({
        scrollTop: $("html").offset().top
    }, 500);

    let url = $(objeto).data('url');

    console.log(url);
    bootbox.confirm({
        title: "Excluir Telefone?",
        message: "Você quer excluir este telefone agora? Isto não poderá ser desfeito.",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar',
                className: 'm-bottom-0 btn-danger'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar',
                className: 'm-left-20 btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (response, textStatus, jqXHR) {
                        if (response.status) {
                            mostrarNotificacao('Beem Feito', response.mensagem, 5000, 'success');
                            AtualizarTelefones();
                        } else {
                            var erros = response.erros.join('<br/>');
                            mostrarNotificacao('Não foi possível executar esta ação!', erros);
                        }
                    },
                    error: function (request, textStatus, errorThrown) {
                        console.log(request, textStatus, errorThrown);
                        mostrarNotificacao('Não foi possível executar esta ação!', errorThrown);
                    }
                });
            }
        }
    });
}

function editarTelefone(objeto) {
    $('html, body').animate({
        scrollTop: $("html").offset().top
    }, 500);

    let url = $(objeto).data('url');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (response, textStatus, jqXHR) {
            if (response.status) {
                let telefone = JSON.parse(response.telefone);
                let id = telefone.Id;
                let tipo = telefone.Tipo;
                let numero = telefone.Ddd.toString() + telefone.Numero.toString();

                $('#telefoneId').val(id);
                $('#tipoTelefone').val(tipo).trigger("chosen:updated");
                $('#numero').val(numero).trigger('input');
            } else {
                mostrarNotificacao('Ops... Aconteceu algo de errado!',
                        'Por favor, verifique os erros ocorridos que foram listados abaixo. <br/><br/>' +
                        response.erros.join('\n'));
            }
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarNotificacao('Ops... Aconteceu algo de errado!',
                    'Por favor, verifique os erros ocorridos que foram listados abaixo. <br/><br/>' + errorThrown);
        }
    });
}

function excluirServico(objeto) {
    $('html, body').animate({
        scrollTop: $("html").offset().top
    }, 500);

    let url = $(objeto).data('url');

    bootbox.confirm({
        title: "Excluir Serviço?",
        message: "Você quer excluir este serviço agora? Isto não poderá ser desfeito.",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar',
                className: 'm-bottom-0 btn-danger'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar',
                className: 'm-left-20 btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (response, textStatus, jqXHR) {
                        if (response.status) {
                            mostrarNotificacao('Beem Feito', response.mensagem, 5000, 'success');
                            AtualizarServicos();
                        } else {
                            var erros = response.erros.join('<br/>');
                            mostrarNotificacao('Não foi possível executar esta ação!', erros);
                        }
                    },
                    error: function (request, textStatus, errorThrown) {
                        console.log(request, textStatus, errorThrown);
                        mostrarNotificacao('Não foi possível executar esta ação!', errorThrown);
                    }
                });
            }
        }
    });
}

function carregarPopup(objeto) {
    var title = $(objeto).data('title');
    var url = $(objeto).data('url');
    var refresh = $(objeto).data('refresh');

    let containerAtualizacao = currentTab.attr('href');
    let urlAtualizacao = currentTab.data('url');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (response, textStatus, jqXHR) {
            var dialog = bootbox.dialog({
                title: title,
                message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                size: 'large',
                onEscape: function () {
                    if (refresh !== false) {
                        AtualizarAba(containerAtualizacao, urlAtualizacao);
                    }
                }
            });

            dialog.init(function () {
                setTimeout(function () {
                    dialog.find('.bootbox-body').html(response);
                }, 3000);
            });

            $('.modal-dialog').css('width:1000px !important');
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
        }
    });
}

function salvarServico(objeto) {
    let url = $(objeto).attr('action');
    let type = $(objeto).attr('method');
    let data = $(objeto).serializeArray();

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response, textStatus, jqXHR) {
            if (response.status) { // atualizar link do form e mostrar a mensagem de salvo na propria pagina pode ser uma opção
                mostrarNotificacao('Beem Feito', response.mensagem, 5000, 'success');
                bootbox.hideAll();
                AtualizarServicos();
            } else {
                mostrarMensagemServico('danger', 'Ops... Aconteceu algo de errado!',
                        'Por favor, verifique os erros ocorridos que foram listados abaixo.',
                        response.erros.join('\n'));
            }
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarMensagemServico('danger', 'Ops... Aconteceu algo de errado!',
                    'Por favor, verifique os erros ocorridos que foram listados abaixo.', errorThrown);
        }
    });
}

function excluirCobranca(objeto) {
    $('html, body').animate({
        scrollTop: $("html").offset().top
    }, 500);

    let url = $(objeto).data('url');

    console.log(url);
    bootbox.confirm({
        title: "Excluir Cobrança?",
        message: "Você quer excluir esta cobrança agora? Isto não poderá ser desfeito.",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar',
                className: 'm-bottom-0 btn-danger'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar',
                className: 'm-left-20 btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (response, textStatus, jqXHR) {
                        if (response.status) {
                            mostrarNotificacao('Beem Feito', response.mensagem, 5000, 'success');
                            AtualizarCobrancas();
                        } else {
                            var erros = response.erros.join('<br/>');
                            mostrarNotificacao('Não foi possível executar esta ação!', erros);
                        }
                    },
                    error: function (request, textStatus, errorThrown) {
                        console.log(request, textStatus, errorThrown);
                        mostrarNotificacao('Não foi possível executar esta ação!', errorThrown);
                    }
                });
            }
        }
    });
}

function cancelarNotaFiscal(objeto) {
    $('html, body').animate({
        scrollTop: $("html").offset().top
    }, 500);

    let url = $(objeto).data('url');

    console.log(url);
    bootbox.confirm({
        title: "Cancelar Nota?",
        message: "Você quer cancelar esta nota agora? Isto não poderá ser desfeito.",
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancelar',
                className: 'm-bottom-0 btn-danger'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirmar',
                className: 'm-left-20 btn-primary'
            }
        },
        callback: function (result) {
            if (result) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    success: function (response, textStatus, jqXHR) {
                        if (response.status) {
                            mostrarNotificacao('Beem Feito', response.mensagem, 5000, 'success');
                            AtualizarNotasFiscais();
                            /*var encode = Base64.encode(response.mensagem);
                             encode = encode.replaceAll('=', '');
                             window.location.href = response.redirect_url + '#' + encode;
                             location.reload();*/
                        } else {
                            var erros = response.erros.join('<br/>');
                            mostrarNotificacao('Não foi possível executar esta ação!', erros);
                        }
                    },
                    error: function (request, textStatus, errorThrown) {
                        console.log(request, textStatus, errorThrown);
                        mostrarNotificacao('Não foi possível executar esta ação!', errorThrown);
                    }
                });
            }
        }
    });
}

function mostrarMensagemServico(className, titulo, subtitulo, mensagem) {
    $('.alerta_status_servico').attr('class', 'alerta_status_popup alert alert-' + className);
    $('.alerta_titulo_servico').text(titulo);
    $('.alerta_subtitulo_servico').text(subtitulo);
    $('.erros_alerta_servico').html(mensagem);
    $('.container_alerta_servico').removeClass('oculto');
    setTimeout(function () {
        $('.container_alerta_servico').addClass('oculto');
    }, 20000);
}

function mostrarMensagemNotasFiscais(className, titulo, subtitulo, mensagem) {
    $('#alerta_status_notas_fiscais').attr('class', 'alerta_status_popup alert alert-' + className);
    $('#alerta_titulo_notas_fiscais').text(titulo);
    $('#alerta_subtitulo_notas_fiscais').text(subtitulo);
    $('#erros_alerta_notas_fiscais').html(mensagem);
    $('#container_alerta_notas_fiscais').removeClass('oculto');
    setTimeout(function () {
        $('#container_alerta_notas_fiscais').addClass('oculto');
    }, 20000);
}