/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$('.servico-cancelado').click(function () {
    var isCancelado = $(this).val();
    if (isCancelado == '1')
        $('#container_cancelamento_servico').show();
    else
        $('#container_cancelamento_servico').hide();
});
/*
$('#inputNomeServicoPrestadoAutocomplete').autocomplete({
    top: 147,
    left: 48,
    showResults: function (objects) {
        var data = [];
        $.each(objects, function (index, object) {
            var dataResultado = {
                Id: object.IdservicoPrestado,
                Label: object.Codigo + " - " + object.Nome,
                Object: object
            };

            data.push(dataResultado);
        });

        return data;
    },
    onSelect: function (object) {
        $('#inputNomeServicoPrestadoAutocomplete').val(object.Nome);
        $('#servicoPrestadoId').val(object.IdservicoPrestado);
        $('#servicoPrestadoCodigo').val(object.Codigo.toUpperCase());
        $('#servicoPrestadoTecnologia').val(object.Tecnologia.capitalize());
        $('#servicoPrestadoDownload').val(object.Download);
        $('#servicoPrestadoUpload').val(object.Upload);
        $('#servicoPrestadoGarantiaBanda').val(object.GarantiaBanda);
        $('#servicoPrestadoFranquia').val(object.Franquia);
    }
});
*/
moment.locale('pt-br'); // default the locale to Portuguese Brazil
$('#dataContratado').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
        format: 'DD/MM/YYYY'
    }
});

$('#dataCancelado').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
        format: 'DD/MM/YYYY'
    }
});

$("#chosenSelectServico").chosen(
        {
            max_selected_options: 1,
            disable_search_threshold: 1,
            no_results_text: "Oops, nada encontrado!"
        }
);


$("#chosenSelectServico").chosen().change(function() {     
    var url = $(this).children('option:selected').data('url');
   
    $.ajax({
        url: url,
        type: 'GET',
        success: function(response, textStatus, jqXHR) {            
            var servicoPrestado = JSON.parse(response.servico);
            
            $('#servicoPrestadoPlanoNome').val(servicoPrestado.planoNome);
            $('#servicoPrestadoCodigo').val(servicoPrestado.Codigo);
            $('#servicoPrestadoTecnologia').val(Capitalize(servicoPrestado.Tecnologia));
            $('#servicoPrestadoDownload').val(servicoPrestado.Download);
            $('#servicoPrestadoUpload').val(servicoPrestado.Upload);
            $('#servicoPrestadoGarantiaBanda').val(servicoPrestado.GarantiaBanda);
            $('#servicoPrestadoFranquia').val(servicoPrestado.Franquia);
            $('#planoValorOriginal').val(servicoPrestado.planoValor);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            
        }
    });
});

$("#chosenSelectCidade").chosen(
        {
            max_selected_options: 1,
            disable_search_threshold: 1,
            no_results_text: "Oops, nada encontrado!"
        }
);

$('#inputCep').mask('00000-000');

$('div.daterangepicker').addClass('avoid-clicks-child');

$('.servico-contrato-tem-sva').click(function () {
    var possui = $(this).val();
    if (possui == '1') {
        $('#container_contrato_sva').show();
        $("input[name='tipoSva'][value='valor']").prop('checked', true);
        $("input[name='sva']").removeAttr('maxlength').removeAttr('minlength');
    } else {
        $('#container_contrato_sva').hide();
        $("input[name='sva']").val('');
        $("input[name='tipoSva'][value='valor']").prop('checked', false);
        $("input[name='tipoSva'][value='percentual']").prop('checked', false);
        //atualizarValorForcado();
    }
});

$('.servico-contrato-tem-scm').click(function () {
    var possui = $(this).val();
    if (possui == '1') {
        $('#container_contrato_scm').show();
        $("input[name='tipoScm'][value='valor']").prop('checked', true);
        $("input[name='scm']").removeAttr('maxlength').removeAttr('minlength');
    } else {
        $('#container_contrato_scm').hide();
        $("input[name='scm']").val('');
        $("input[name='tipoScm'][value='valor']").prop('checked', false);
        $("input[name='tipoScm'][value='percentual']").prop('checked', false);
        //atualizarValorForcado();
    }
});

$('.servico-contrato-forcar-valor').click(function () {
    var possui = $(this).val();
    if (possui == '0') {
        $("#valorForcado").val('');
        $("#valorForcado").attr('readonly', true);
    } else {
        //$("#valor").attr('readonly', false);
        $("#valorForcado").removeAttr('readonly');
    }
});

$(".servico-contrato-tipo-scm, .servico-contrato-tipo-sva").click(function () {
    var tipo = $(this).val();
    if (tipo == 'valor') {
        //atualizarValorForcado();
    }
});

$("input[name='scm']").keyup(function () {
    //atualizarValorForcado();
});

$("input[name='sva']").keyup(function () {
    //atualizarValorForcado();
});

$('#valorForcado').keyup(function () {
    atualizarValorTotal();
});

$('#valorDesconto').keyup(function () {
    atualizarValorTotal();
});

function atualizarValorTotal() {
    var valorOriginal = parseFloat($('#planoValorOriginal').val());
    var valorForcado = parseFloat($('#valorForcado').val());
    var valorDesconto = parseFloat($('#valorDesconto').val());
    var valorTotal = 0.0;
    
    if (valorForcado == '' || isNaN(valorForcado)) {
        valorTotal = valorOriginal;
    } else {
        valorTotal = valorForcado;
    }
    
    if (valorDesconto == '' || isNaN(valorDesconto)) {
        valorDesconto = 0.0;
    }
    
    valorTotal -= valorDesconto;
    $('#valorTotal').val(valorTotal);
}

function atualizarValorForcado() {
    $(".servico-contrato-forcar-valor[value='1']").click();
    var valorSva = $("input[name='sva']").val() == '' ? 0 : parseFloat($("input[name='sva']").val());
    var valorScm = $("input[name='scm']").val() == '' ? 0 : parseFloat($("input[name='scm']").val());
    var valor = 0.0;

    if ($('.servico-contrato-tipo-scm:checked').val() == 'valor') {
        valor += valorScm;
    }

    if ($('.servico-contrato-tipo-sva:checked').val() == 'valor') {
        valor += valorSva;
    }

    $("#valorForcado").val(valor);
}

$("#buscar_cep").click(function () {
    var cep_code = $("#inputCep").val();
    buscarEnderecoPorCep(cep_code);
});

$('#formulario_servico').submit(function (e) {
    e.preventDefault();

    var url = $(this).attr('action');
    var type = $(this).attr('method');
    var data = $(this).serialize();

    $.ajax({
        url: url,
        type: type,
        data: data,
        success: function (response, textStatus, jqXHR) {
            if (response.status == true) {
                $("#alerta_topo").removeClass('alert-danger');
                $("#alerta_mensagem").text(response.mensagem);
                $("#alerta_topo").addClass('alert-success');
                $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                MostrarAlertaTopo('alert-success', 5000);
                var popup = new $.Popup($("section.main"));
                popup.Close();
                cliente.atualizarServicos('#container_servicos_cliente');
            } else {
                var erros = "";
                $.each(response.erros, function (key, erro) {
                    erros += "<br/>" + erro;
                });

                var popup = new $.Popup($("section.main"));
                popup.Alert('Atenção', erros);

                /*
                 $("#content_alerta_mensagem").html(erros);
                 $("#content_alerta_topo").addClass('alert-danger');
                 $("#content_alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);
                 */
                MostrarAlertaTopo('alert-danger', 15000);
            }
        },
        error: function (e) {

        }
    });
});