/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function (e) {
    $("#chosenSelectCidade").chosen(
            {
                max_selected_options: 1,
                disable_search_threshold: 1,
                no_results_text: "Oops, nada encontrado!"
            }
    );

    $("#chosenSelectUf").chosen(
            {
                max_selected_options: 1,
                disable_search_threshold: 1,
                no_results_text: "Oops, nada encontrado!"
            }
    );


    moment.locale('pt-br'); // default the locale to Portuguese Brazil
    $('#inputDataNascimento').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
    
    $('#inputDataContratoCliente').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });
    
//    $('#inputDataCanceladoCliente').daterangepicker({
//        singleDatePicker: true,
//        showDropdowns: true,
//        locale: {
//            format: 'DD/MM/YYYY'
//        }
//    });

    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
    $('#inputCep').mask('00000-000');

    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
            spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

    $('.telefone').mask(SPMaskBehavior, spOptions);

    $("#chosenSelectTipoPessoa").change(function (e) {
        var tipo = $(this).val();

        if (tipo == 'fisica') {
            $(".juridica").hide();
            $(".fisica").show();
        } else {
            $(".fisica").hide();
            $(".juridica").show();
        }
    });

    $('.tipo-pessoa').change(function () {
        var tipo = $(this).val();
        if (tipo == 'fisica') {
            $(".juridica").hide();
            $(".fisica").show();
        } else {
            $(".fisica").hide();
            $(".juridica").show();
        }
    });

    $("div.alert").on("click", "button.close", function () {
        //$(this).parent().animate({opacity: 0}, 500).hide('slow');
        $(this).parent().addClass('oculto').animate({opacity: 0}, 500).slideUp('slow');
    });

    $("#formulario_pessoa").submit(function (e) {
        e.preventDefault();

        var url = $(this).attr('action');
        var method = $(this).attr('method');
        var data = $(this).serializeArray();

        $.ajax({
            url: url,
            type: method,
            data: data,
            success: function (response, textStatus, jqXHR) {
                if (response.status == true) {
                    $("#alerta_topo").removeClass('alert-danger');
                    $("#alerta_mensagem").text(response.mensagem);
                    $("#alerta_topo").addClass('alert-success');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-success', 5000);
                    //location.reload();
                } else {
                    var erros = "";
                    $.each(response.erros, function (key, erro) {
                        erros += "<br/>" + erro;
                    });

                    $("#alerta_mensagem").html(erros);
                    $("#alerta_topo").addClass('alert-danger');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-danger', 15000);
                }
            }
        });

        return false;
    });

    $("#buscar_cep").click(function () {
        var cep_code = $("#inputCep").val();
        buscarEnderecoPorCep(cep_code);
    });

    $("#adicionar_telefone").click(function (event) {
        event.preventDefault();

        var numero = $('#inputNumeroTelefone').val();
        var ddd = numero.replace('(', '').replace(')', '').substr(0, 3);
        numero = numero.replace('-', '').substr(4, numero.length);

        var url = $(this).data('href');
        var data = {};
        data.tipo = $('#chosenSelectTipoTelefone').val();
        data.ddd = ddd.trim();
        data.numero = numero.trim();

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function (response, textStatus, jqXHR) {
                if (response.status == true) {
                    var telefonesUrl = url.replace('adicionar-telefone', 'telefones');
                    AtualizarTelefones(telefonesUrl);

                    $("#alerta_topo").removeClass('alert-danger');
                    $("#alerta_mensagem").text(response.mensagem);
                    $("#alerta_topo").addClass('alert-success');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-success', 5000);
                } else {
                    var erros = "";
                    $.each(response.erros, function (key, erro) {
                        erros += "<br/>" + erro;
                    });

                    $("#alerta_mensagem").html(erros);
                    $("#alerta_topo").addClass('alert-danger');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-danger', 15000);
                }
            }
        });

        return false;
    }); 


    $("#cancelamento_cliente").click(function (e) {
        e.preventDefault();
        
        var url = $(this).data('href');

        $.ajax({
            url: url,
            type: 'GET',
            success: function (response, textStatus, jqXHR) {
                console.log(response, textStatus, jqXHR);
                alert(response.mensagem);
            }
        });
        return false;
    }); 
});


function MostrarAlertaTopo(tipoClasse, tempoDeEspera) {
    setTimeout(function () {
        $("div.alert").slideUp(500, function () {
            $(this).addClass('oculto').animate({opacity: 0}, 500).slideUp('slow');
            $("#alerta_topo").removeClass(tipoClasse);
        });
    }, tempoDeEspera);
}

function AtualizarTelefones(url) {
    $("#container_telefones").load(url, function (templates, status, xhr) {
        /*
         if (status == "error") {
         var msg = "Sorry but there was an error: ";
         $("#error").html(msg + xhr.status + " " + xhr.statusText);
         }*/
        console.log(templates);
    });

}