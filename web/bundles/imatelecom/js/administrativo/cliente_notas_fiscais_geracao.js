/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {    
    
    moment.locale('pt-br'); // default the locale to Portuguese Brazil
    $('#dataEmissao').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $('div.daterangepicker').addClass('avoid-clicks-child');

    $("#formulario_geracao_notas_fiscais").submit(function (e) {
        e.preventDefault();

        var url = $(this).attr('action');
        var type = $(this).attr('method');
        var data = {};
        data.dataEmissao = $('#dataEmissao').val();
        //data.cobrancas = cobrancasParaGeracao();

        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function (response, textStatus, jqXHR) {
                console.log(response);
                if (response.status == true) {
                    $("#alerta_topo").removeClass('alert-danger');
                    $("#alerta_mensagem").text(response.mensagem);
                    $("#alerta_topo").addClass('alert-success');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-success', 5000);
                    var popup = new $.Popup($("section.main"));
                    popup.Close();
                    //cliente.atualizarCobrancas('#container_cobrancas_cliente');
                } else {
                    var erros = "";
                    $.each(response.erros, function (key, erro) {
                        erros += "<br/>" + erro;
                    });

                    var popup = new $.Popup($("section.main"));
                    popup.Alert('Atenção', erros);

                    MostrarAlertaTopo('alert-danger', 15000);
                }
            },
            error: function (e) {

            }
        });

        return false;
    });
});

function atualizarValorTotal() {
    var valorTotal = 0.0;
    $('#servicosGeracaoCobranca tr:has(td)').find('input[name="selecionarServico"]').each(function () {
        var isChecked = $(this).prop('checked');
        if (isChecked) {
            var id = $(this).data('id');
            var valor = $('#' + id).val();

            valor = valor.replace('.', '').replace(',', '.');
            if (valor.length == 0)
                valor = 0;
            valorTotal += parseFloat(valor);
        }
    });

    valorTotal = parseFloat(valorTotal, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();

    $('#valor_total').text(valorTotal);
    $('#valor_total').formatCurrency({symbol: 'R$ ', region: 'pt-BR', digitGroupSymbol: ',', decimalSymbol: '.'});
}

function cobrancasParaGeracao() {
    var cobrancas = [];
    $('#servicosGeracaoCobranca tr:has(td)').find('input[name="selecionarServico"]').each(function () {
        var isChecked = $(this).prop('checked');
        if (isChecked) {
            var id = $(this).data('id');
            var valor = $('#' + id).val();

            valor = valor.replace('.', '').replace(',', '.');
            if (valor.length == 0)
                valor = 0;

            var servico = $(this).data('id-servico');
            var cobranca = {};
            cobranca.servico = servico;
            cobranca.valor = valor;
            cobrancas.push(cobranca);
        }
    });

    return cobrancas;
}