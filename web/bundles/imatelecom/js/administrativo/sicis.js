/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {
    var table = $('table.table').dataTable({
        lengthMenu: [[15, 50, -1], [15, 50, "All"]],
        order: [[0, "desc"]],
        columnDefs: [
            {
                targets: 3,
                orderable: false
            }
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        },
        responsive: true
    });

    new $.fn.dataTable.FixedHeader(table);


    $('.gerar-arquivo-sici').click(function (e) {
        e.preventDefault();
        
        var popup = new $.Popup($("section.main"));
        var url = $(this).data('url');
        var urlRedirect = $(this).data('url-redirect');
        popup.QuestionRedirect('Atenção', 'Você realmente deseja gerar o arquivo?', url, true, urlRedirect);

        return false;
    });

    $('.excluir-arquivo-sici').click(function (e) {
        e.preventDefault();
        
        var popup = new $.Popup($("section.main"));
        var url = $(this).data('url');
        var urlRedirect = $(this).data('url-redirect');
        popup.QuestionRedirect('Atenção', 'Você realmente deseja excluir este arquivo?', url, true, urlRedirect);

        return false;
    });
});