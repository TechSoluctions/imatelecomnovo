<?php

//namespace Ima\DefaultBundle\Scripts;

use ImaTelecomBundle\Lib\SUC;
use Propel\Runtime\Propel;

use ImaTelecomBundle\Model\Pessoa;
use ImaTelecomBundle\Model\PessoaQuery;
use ImaTelecomBundle\Model\CompetenciaQuery;
use ImaTelecomBundle\Model\CronTaskLogExecucaoQuery;

include  __DIR__ . '/ImportacaoPessoaSuc.php';
include  __DIR__ . '/ImportacaoTelefoneSuc.php';
include  __DIR__ . '/ImportacaoClientesSuc.php';
include  __DIR__ . '/ImportacaoEnderecoServClientesSuc.php';
include  __DIR__ . '/ImportacaoServicoClientesSuc.php';
include  __DIR__ . '/ImportacaoEnderecoClienteSuc.php';
include  __DIR__ . '/ImportacaoBoletosSuc.php';
include  __DIR__ . '/ImportacaoBoletoItensSuc.php';

function init($parametrosArgc, $parametrosArgv) {
    echo "***** INICIANDO IMPORTAÇÃO DO SUC ***** \n";

    // SITE DA INFORMAÇÂO 
    // https://github.com/propelorm/Propel2/issues/856
    
    $loader = require_once __DIR__ . '/../../../vendor/autoload.php';    
    $loader->add('', __DIR__ . '/generated-classes');
    \Propel\Runtime\Propel::disableInstancePooling();

    gc_enable();

    // Create connections in Propel.
    $serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
    $serviceContainer->checkVersion('2.0.0-dev');
    // Database connection 1.
    $serviceContainer->setAdapterClass('ima_telecom', 'mysql');
    $manager = new \Propel\Runtime\Connection\ConnectionManagerSingle();
    $manager->setConfiguration(array(
        'classname' => 'Propel\\Runtime\\Connection\\ConnectionWrapper',
        'dsn' => 'mysql:host=cnetdev01.censanet.com.br;dbname=ima',
		//'dsn' => 'mysql:host=cnetdb05.censanet.com.br;dbname=ima_producao',
        'user' => 'ima',
        'password' => 'Im4Tel3C$',
        'attributes' =>
        array(
            'ATTR_EMULATE_PREPARES' => false,
        ),
        'settings' =>
        array(
            'charset' => 'utf8',
            'queries' =>
            array(
                'utf8' => 'SET NAMES \'UTF8\'',
            ),
        )
    ));

    $manager->setName('ima_telecom');
    $serviceContainer->setConnectionManager('ima_telecom', $manager);
    // Set the default connection.
    $serviceContainer->setDefaultDatasource('ima_telecom');

    // Set connection manangers.
    $connectionManager1 = \Propel\Runtime\Propel::getConnectionManager('ima_telecom');
    $connection1 = $connectionManager1->getWriteConnection($serviceContainer->getAdapter());
    $connectionManager1->setConnection($connection1);

	// Salvar Log
    $logName = "";
	$log = null;
    if ($parametrosArgc > 1) {
		// pegar o id do log para atualizar ao final
        $logName = $parametrosArgv[1];
		//file_put_contents($logName, $write);				
    } else {
        $today = date("Y-m-d__H_i_s");
        $logName = __DIR__ . "/../../../logs/t_log_importacao_$today.txt";
    }
	 

	$write = "------------------ INICIO ------------------";
	file_put_contents($logName, $write);


	/************************************** INICIO DA IMPORTAÇÃO **************************************/
	
	// Importando Pessoas
	$write .= "\n\r\n\r\n\r------------ Importação de Pessoas ------------\n\r";
	file_put_contents($logName, $write);

	$contentPessoas = ImportacaoPessoaSuc();
    $contentPessoa = implode("\n\r", $contentPessoas);

	$write .= "$contentPessoa";
	file_put_contents($logName, $write);


	// Importando Telefones
	$write .= "\n\r\n\r\n\r------------ Importação de Telefones ------------\n\r";
	file_put_contents($logName, $write);

	$contentTelefones = ImportacaoTelefoneSuc();
    $contentTelefone = implode("\n\r", $contentTelefones);
	
	$write .= "$contentTelefone";
	file_put_contents($logName, $write);

	
	// Importando Clientes
	$write .= "\n\r\n\r\n\r------------ Importação de Clientes ------------\n\r";
	file_put_contents($logName, $write);
	
	$contentClientes = ImportacaoClientesSuc();
	$contentCliente = implode("\n\r", $contentClientes);

	$write .= "$contentCliente";
	file_put_contents($logName, $write);


	// Importando Endereços dos Clientes
	$write .= "\n\r\n\r\n\r------------ Importação dos Endereços dos Clientes ------------\n\r";
	file_put_contents($logName, $write);

	$contentEnderecosClientes = ImportacaoEnderecoClienteSuc();
	$contentEnderecosCliente = implode("\n\r", $contentEnderecosClientes);

	$write .= "$contentEnderecosCliente";
	file_put_contents($logName, $write);
	

	// Importando Serviços dos Clientes
	$write .= "\n\r\n\r\n\r------------ Importação dos Serviços dos Clientes ------------\n\r";
	file_put_contents($logName, $write);

	$contentServicosClientes = ImportacaoServicoClientesSuc();
	$contentServicosCliente = implode("\n\r", $contentServicosClientes);

	$write .= "$contentServicosCliente";
	file_put_contents($logName, $write);


	// Importando Endereços dos Serviços dos Clientes
	$write .= "\n\r\n\r\n\r------------ Importação dos Endereços dos Serviço dos Clientes ------------\n\r";
	file_put_contents($logName, $write);

	$contentEnderecosServicoClientes = ImportacaoEnderecoServClientesSuc();	
	$contentEnderecosServicoCliente = implode("\n\r", $contentEnderecosServicoClientes);

	$write .= "$contentEnderecosServicoCliente";
	file_put_contents($logName, $write);
	

	// Importando Boletos
	$write .= "\n\r\n\r\n\r------------ Importação de Boletos ------------\n\r";
	file_put_contents($logName, $write);

	$contentBoletos = ImportacaoBoletosSuc();
	$contentBoleto = implode("\n\r", $contentBoletos);
	
	$write .= "$contentBoleto";
	file_put_contents($logName, $write);

     
	// Importando Boletos Itens
    $write .= "\n\r\n\r\n\r------------ Importação de Boletos Itens ------------\n\r";
	file_put_contents($logName, $write);
	
	$contentBoletosItens = ImportacaoBoletoItensSuc();
    $contentBoletosItem = implode("\n\r", $contentBoletosItens);

    $write .= "$contentBoletosItem";
	file_put_contents($logName, $write);

	/************************************** FINAL DA IMPORTAÇÃO **************************************/

	try {
		$log= CronTaskLogExecucaoQuery::create()->findPk($parametrosArgv[2]);
		$log->setLogEmAndamento(0);
		//$log->setDataAlterado(new \DateTime('now'));
		$log->save();		
		
		$write .= "\n\r\n\r\n\rAtualizando LOG: " . $parametrosArgv[2];
		file_put_contents($logName, $write);
	} catch(\PropelException $propelException) {
		$write .= "\n\r\n\r\n\rErro: " . $propelException->getMessage();
		file_put_contents($logName, $write);
	}

	$write .= "\n\r\n\r\n\r------------------ FINAL ------------------\n\r\n\r\n\r";
	file_put_contents($logName, $write);

    gc_collect_cycles();
    
    echo "***** FINALIZANDO A IMPORTAÇÃO DO SUC ***** \n\n";
}

init($argc, $argv);

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    return preg_replace('/-+/', ' ', $string); // Replaces multiple hyphens with single one.
}


function remove_accents($string) {
	$replace = array('&lt;' => '', '&gt;' => '', '\'' => '', '&amp;' => '',
            '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
            '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae', 'ª' => 'a',
            'Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D',
            'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E',
            'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G',
            'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I',
            'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
            'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',
            'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N',
            'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
            'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O',
            'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S',
            'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
            'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U',
            '&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
            'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z',
            'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
            'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
            'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
            'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
            'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e',
            'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h',
            'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i',
            'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j',
            'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l',
            'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n',
            'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
            '&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe',
            'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u',
            'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y',
            'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss',
            'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
            'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
            'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
            'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '',
            'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a',
            'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
            'ю' => 'yu', 'я' => 'ya'
	);

	$result = str_replace(array_keys($replace), $replace, $string);

	return $result;
}



function getAnoMes($competencia) {
    $date = date('Y-m-d', strtotime('+1 month', strtotime(date_format($competencia->getPeriodoInicial(), 'Y-m-d H:i:s' ))));
    $competencia->setMes(date("m", strtotime($date)));
    $competencia->setAno(date("Y", strtotime($date)));
    
    return $competencia;
}

/*
  $output = shell_exec('crontab -l');
  file_put_contents('crontab.txt', $output.'* * * * * NEW_CRON'.PHP_EOL);
  echo exec('crontab crontab.txt');
 */
/*
  $output = shell_exec('crontab -l');
  file_put_contents('crontab.txt', $output.'* * * * * NEW_CRON2'.PHP_EOL);
  //echo exec('crontab crontab.txt');
  exec('crontab crontab.txt');
 */
?>