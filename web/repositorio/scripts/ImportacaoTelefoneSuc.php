<?php

//namespace Ima\DefaultBundle\Scripts;

use ImaTelecomBundle\Lib\SUC;
use Propel\Runtime\Propel;

use ImaTelecomBundle\Model\Telefone;
use ImaTelecomBundle\Model\TelefoneQuery;
use ImaTelecomBundle\Model\PessoaQuery;
use ImaTelecomBundle\Model\CompetenciaQuery;


function ImportacaoTelefoneSuc() {
    echo "\t- Inciando a Importação dos Telefones das Pessoas ***** \n";
        
    $erros = array();
    $suc = SUC::getInstancia();

    $resp = $suc->autenticar();
    if ($resp[0] == True) {
        $webserviceImatelecom = $suc->getModule("webservice_imatelecom");

        //$competenciaAtual = $this->getCompetenciaAtual();
        $competenciaAtual = CompetenciaQuery::create()->findOneByAtivo(1);      
        $competenciaAtual = getAnoMes($competenciaAtual);
        $telefones = $webserviceImatelecom->exportarTelefonesIma($competenciaAtual->getAno(), $competenciaAtual->getMes());            

        $linha = 1;
        foreach ($telefones[0] as $telefoneExportacao) {
            gc_enable();
            Propel::disableInstancePooling();

            $erro = array();                
            $telefone = TelefoneQuery::create()->findByImportId($telefoneExportacao[0])->getFirst();
            if ($telefone == null) {
                $telefone = new Telefone();
                $telefone->setImportId($telefoneExportacao[0]);                

                $pessoa = PessoaQuery::create()->findByImportId($telefoneExportacao[1])->getFirst();

                if ($pessoa == null) {
                    $erro[] = "Pessoa Inexistente";
                } else {                    
                    $telefone->setPessoa($pessoa);
                }

                $telefone->setTipo($telefoneExportacao[2]);        
                $telefone->setDdd($telefoneExportacao[3]);
                $telefone->setNumero($telefoneExportacao[4]);                                
                $telefone->setDataCadastro(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $telefone->setDataAlterado(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $telefone->setUsuarioAlterado(1);

                if (sizeof($erro) == 0) {
                    try {
                        $telefone->save();
                        $erros[] = "\n\rLinha $linha: Código: " . $telefoneExportacao[0] . " - Importado com sucesso.";
                    } catch (\PropelException $e) {
                        $erro[] = $e->getMessage();
                        $erros[] = "\n\rLinha $linha: Código: " . $telefoneExportacao[0] . " - " . implode(" | ", $erro);
                    }   
                } else {
                    $erros[] = "\n\rLinha $linha: Código: " . $telefoneExportacao[0] . " - " . implode(" | ", $erro);
                }
            } else {
                $erros[] = "\n\rLinha $linha: Código: " . $telefoneExportacao[0] . " - Telefone já existe no Sistema.";
            }

            $linha++;
        }
    }

    echo "\t- Fim da Importaçãodos Telefones das Pessoas ***** \n\n";

    return $erros;         
}

?>