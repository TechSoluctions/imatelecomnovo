<?php

//namespace Ima\DefaultBundle\Scripts;

use ImaTelecomBundle\Lib\SUC;
use Propel\Runtime\Propel;

use ImaTelecomBundle\Model\Cliente;
use ImaTelecomBundle\Model\ClienteQuery;
use ImaTelecomBundle\Model\PessoaQuery;
use ImaTelecomBundle\Model\CompetenciaQuery;


function ImportacaoClientesSuc() {
    echo "\t- Iniciando da Importação dos Clientes ***** \n";
        
    $erros = array();
    $suc = SUC::getInstancia();

    $resp = $suc->autenticar();
    if ($resp[0] == True) {
        $webserviceImatelecom = $suc->getModule("webservice_imatelecom");

        //$competenciaAtual = $this->getCompetenciaAtual();
        $competenciaAtual = CompetenciaQuery::create()->findOneByAtivo(1);      
        $competenciaAtual = getAnoMes($competenciaAtual);
        $clientes = $webserviceImatelecom->exportarClientesIma($competenciaAtual->getAno(), $competenciaAtual->getMes());            

        $linha = 1;
        foreach ($clientes[0] as $clienteExportacao) {
            gc_enable();
            Propel::disableInstancePooling();

            $erro = array();

            $cliente = ClienteQuery::create()->findByImportId($clienteExportacao[0])->getFirst();
            if ($cliente == null) {
                $cliente = new Cliente();
                $cliente->setImportId($clienteExportacao[0]);                

                $pessoa = PessoaQuery::create()->findByImportId($clienteExportacao[1])->getFirst();

                if ($pessoa == null) {
                    $erro[] = "Pessoa Inexistente";
                } else {
                    // ajustar o metodo do cliente para pegar o import_id de pessoa
                    $cliente->setPessoa($pessoa);
                }

                if ($clienteExportacao[2] != '0' and $clienteExportacao[2] != '1') {
                    $erro[] = 'Ativo deve ter o valor 1 ou 0';
                } else {
                    $cliente->setAtivo((int) $clienteExportacao[2]);
                }

                try {
                    if (!empty($clienteExportacao[3])) {
                        $cliente->setDataContrato(date_format(new \DateTime($clienteExportacao[3]), 'Y-m-d H:i:s' ));
                    } else {
                        $cliente->setDataContrato(null);
                    }
                } catch (\Exception $e) {
                   // $erro[] = "Data de Contrato Inválida";
					$cliente->setDataContrato(null);
                }

                try {
                    if (!empty($clienteExportacao[4])) {
                        $cliente->setDataCancelado(date_format(new \DateTime($clienteExportacao[4]), 'Y-m-d H:i:s' ));
                    } else {
                        $cliente->setDataCancelado(null);
                    }
                } catch (\Exception $e) {
                    //$erro[] = "Data de Cancelamento Inválida";
					$cliente->setDataCancelado(null);
                }

                $cliente->setDataCadastro(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $cliente->setDataAlterado(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $cliente->setUsuarioAlterado(1);

                if (sizeof($erro) == 0) {
                    try {
                        $cliente->save();
                        $erros[] = "\n\rLinha $linha: Código: " . $clienteExportacao[0] . " - Importado com sucesso.";
                    } catch (\PropelException $e) {
                        $erro[] = $e->getMessage();
                        $erros[] = "\n\rLinha $linha: Código: " . $clienteExportacao[0] . " - " . implode(" | ", $erro);
                    }                
                } else {
                    $erros[] = "\n\rLinha $linha: Código: " . $clienteExportacao[0] . " - " . implode(" | ", $erro);
                }
            } else {
                $erros[] = "\n\rLinha $linha: Código: " . $clienteExportacao[0] . " - Cliente já existe no Sistema.";
            }

            $linha++;
        }
    }
    
    echo "\t- Fim da Importação dos Clientes ***** \n\n";
    
    return $erros;
}

?>