<?php

//namespace Ima\DefaultBundle\Scripts;

use ImaTelecomBundle\Lib\SUC;
use Propel\Runtime\Propel;

use ImaTelecomBundle\Model\BoletoItem;
use ImaTelecomBundle\Model\BoletoItemQuery;
use ImaTelecomBundle\Model\BoletoQuery;
use ImaTelecomBundle\Model\ServicoClienteQuery;
use ImaTelecomBundle\Model\CompetenciaQuery;


function ImportacaoBoletoItensSuc() {
    echo "\t- Inciando a Importação dos Boletos Itens ***** \n";
        
    $erros = array();
    $suc = SUC::getInstancia();

    $resp = $suc->autenticar();
    if ($resp[0] == True) {
        $webserviceImatelecom = $suc->getModule("webservice_imatelecom");

        //$competenciaAtual = $this->getCompetenciaAtual();
        $competenciaAtual = CompetenciaQuery::create()->findOneByAtivo(1);      
        $competenciaAtual = getAnoMes($competenciaAtual);
        $boletos = $webserviceImatelecom->exportarBoletoItensIma($competenciaAtual->getAno(), $competenciaAtual->getMes());            

        $linha = 1;
        foreach ($boletos[0] as $boletosExportacao) {
            gc_enable();
            Propel::disableInstancePooling();

            $erro = array();                

            $boletoItem = BoletoItemQuery::create()->findByImportId($boletosExportacao[0])->getFirst();
            if ($boletoItem == null) {
                $boletoItem = new BoletoItem();
                $boletoItem->setImportId($boletosExportacao[0]);                               
                $boletoItem->setValor($boletosExportacao[1]);

                $boleto = BoletoQuery::create()->findByImportId($boletosExportacao[2])->getFirst();

                if ($boleto == null) {
                    $erro[] = "Boleto Inexistente";
                } else {
                    $boletoItem->setBoleto($boleto);
                }

                $servocp = ServicoClienteQuery::create()->findByImportId($boletosExportacao[3])->getFirst();

                if ($servocp == null) {
                    $erro[] = "Serviço do Cliente Inexistente";
                } else {
                    $boletoItem->setServicoCliente($servocp);
                }

                $boletoItem->setDataCadastro(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $boletoItem->setDataAlterado(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $boletoItem->setUsuarioAlterado(1);

                if (sizeof($erro) == 0) {
                    try {
                        $boletoItem->save();
                        $erro[] = "\n\rLinha $linha: Código: " . $boletosExportacao[0] . " - Importado com sucesso.";
                    } catch (\PropelException $e) {
                        $erro[] = $e->getMessage();
                        $erros[] = "\n\rLinha $linha: Código: " . $boletosExportacao[0] . " - " . implode(" | ", $erro);
                    }   
                } else {
                    $erros[] = "\n\rLinha $linha: Código: " . $boletosExportacao[0] . " - " . implode(" | ", $erro);
                }

                $boletoItem->clearAllReferences(true);
                unset($boletoItem);
                gc_collect_cycles();
            } else {
                $erros[] = "\n\rLinha $linha: Código: " . $boletosExportacao[0] . " - Boleto Item já existe no Sistema.";
            }

            $linha++;
        }
    }

    echo "\t- Fim da Importação dos Boleto Itens ***** \n\n";

    return $erros;         
}

?>