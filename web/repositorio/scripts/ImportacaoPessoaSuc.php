<?php

//namespace Ima\DefaultBundle\Scripts;

use ImaTelecomBundle\Lib\SUC;
use Propel\Runtime\Propel;

use ImaTelecomBundle\Model\Pessoa;
use ImaTelecomBundle\Model\PessoaQuery;
use ImaTelecomBundle\Model\CompetenciaQuery;


function ImportacaoPessoaSuc() {
    echo "\t- Inciando a Importação das Pessoas ***** \n";
    
    $erros = array();
    $suc = SUC::getInstancia();
    
    $resp = $suc->autenticar();
    if ($resp[0] == True) {        
        //$user = $this->get('security.context')->getToken()->getUser();
        $pessoa = $suc->getModule("webservice_imatelecom");

        //$competenciaAtual = $this->getCompetenciaAtual();
        $competenciaAtual = CompetenciaQuery::create()->findOneByAtivo(1);      
        $competenciaAtual = getAnoMes($competenciaAtual);
        $pessoas = $pessoa->exportarPessoasIma($competenciaAtual->getAno(), $competenciaAtual->getMes());

        $linha = 1;
        foreach ($pessoas[0] as $pessoaExportacao) {
            gc_enable();
            Propel::disableInstancePooling();

            $pessoa = PessoaQuery::create()->findByImportId($pessoaExportacao[0])->getFirst();
            $erro = array();

            if ($pessoa == null) {
                $pessoa = new Pessoa();
                $pessoa->setImportId($pessoaExportacao[0]);

                if (!empty($pessoaExportacao[19])) {
                    $pessoa->setSexo(str_replace(' ', '', $pessoaExportacao[19]));
                } else {
                    $pessoa->setSexo('m');
                }

                $pessoa->setContato(utf8_encode(clean($pessoaExportacao[18])));
                $pessoa->setResponsavel(utf8_encode(clean($pessoaExportacao[17])));

                if (!empty($pessoaExportacao[16]))
                    $pessoa->setCidadeId($pessoaExportacao[16]);
                else
                    $erro[] = "Código Cidade inválido";

                //$pessoaExportacao[15] = str_replace(array("\xC2", "\\n", "\\r"), "", $pessoaExportacao[15]);
                //$pessoaExportacao[15] = $this->clean($pessoaExportacao[15]);
                $pessoa->setObs(utf8_encode(clean($pessoaExportacao[15])));

                $pessoa->setDataCadastro(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $pessoa->setDataAlterado(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                //$pessoa->setUsuarioAlterado($this->getUsuarioLogado());

                if (!empty($pessoaExportacao[14])) {
                    //$date = \DateTime::createFromFormat('d/m/Y', $pessoaExportacao[14]);
                    //$dataNascimento = $date->format('Y-m-d');            
                    $pessoa->setDataNascimento($pessoaExportacao[14]);
                } else {
                    $pessoa->setDataNascimento(null);
                }

                if (!empty($pessoaExportacao[13])) {
                    if ($pessoaExportacao[13] == 'fisica' || $pessoaExportacao[13] == 'juridica')
                        $pessoa->setTipoPessoa($pessoaExportacao[13]);
                    else
                        $erro[] = 'Tipo pessoa formato inválido.';
                } else {
                    $erro[] = "Tipo pessoa inexistente.";
                }

                $pessoa->setComplemento(utf8_encode(clean($pessoaExportacao[12])));
                $pessoa->setCpfCnpj($pessoaExportacao[8]);

                if (!empty($pessoaExportacao[11])) {
                    $pessoa->setEmail($pessoaExportacao[11]);
                }

                if (!empty($pessoaExportacao[9])) {
                    $pessoa->setRgInscrest($pessoaExportacao[9]);
                } else {
                    // $erros[] = "RG inexistente.";
                }

                if (!empty($pessoaExportacao[5])) {
                    if (strlen($pessoaExportacao[5]) == 8)
                        $pessoa->setCep($pessoaExportacao[5]);
                    else
                        $erro[] = 'CEP formato inválido.';
                } else {
                    //$erros[] = 'CEP inexistente.';
                }

                $pessoa->setPontoReferencia(utf8_encode(clean($pessoaExportacao[10])));

                if (!empty($pessoaExportacao[7])) {
                    if (strlen($pessoaExportacao[7]) == 2)
                        $pessoa->setUf($pessoaExportacao[7]);
                    else
                        $erro[] = 'UF formato inválido.';
                } else {
                    $erro[] = 'UF inexistente.';
                }

                if (!empty($pessoaExportacao[6]))
                    $pessoa->setNum($pessoaExportacao[6]);
                else
                    $pessoa->setNum(0);

                $pessoa->setBairro(utf8_encode(clean($pessoaExportacao[4])));
                $pessoa->setRua(utf8_encode(clean($pessoaExportacao[3])));
                $pessoa->setRazaoSocial(utf8_encode(clean($pessoaExportacao[2])));
                $pessoa->setNome(utf8_encode(clean($pessoaExportacao[1])));

                if (sizeof($erro) == 0) {
                    try {
                        $pessoa->save();
                        $erros[] = "\n\rLinha $linha: Código: " . $pessoaExportacao[0] . " - Importado com sucesso.";
                    } catch (\PropelException $e) {
                        $erro[] = $e->getMessage();
                        $erros[] = "\n\rLinha $linha: Código: " . $pessoaExportacao[0] . " - " . implode(" | ", $erro);
                    }
                } else {
                    $erros[] = "\n\rLinha $linha: Código: " . $pessoaExportacao[0] . " - " . implode(" | ", $erro);
                }
            } else {
                $erros[] = "\n\rLinha $linha: Código: " . $pessoaExportacao[0] . " - Pessoa já existe no Sistema.";
            }

            $pessoa->clearAllReferences(true);
            unset($pessoa);
            gc_collect_cycles();

            $linha++;
        }
    } else {
        throw new \Exception("Falha ao Acessar Servidor");
    }
    
    echo "\t- Fim da Importação das Pessoas ***** \n\n";
    
    return $erros;
}

?>