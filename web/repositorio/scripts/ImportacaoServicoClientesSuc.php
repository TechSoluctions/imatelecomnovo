<?php

//namespace Ima\DefaultBundle\Scripts;

use ImaTelecomBundle\Lib\SUC;
use Propel\Runtime\Propel;

use ImaTelecomBundle\Model\ServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery;
use ImaTelecomBundle\Model\ServicoPrestadoQuery;
use ImaTelecomBundle\Model\ClienteQuery;
use ImaTelecomBundle\Model\CompetenciaQuery;


function ImportacaoServicoClientesSuc() {
    echo "\t- Inicio da Importação dos Serviços de Clientes ***** \n\n";
        
    $erros = array();
    $suc = SUC::getInstancia();

    $resp = $suc->autenticar();
    if ($resp[0] == True) {
        $webserviceImatelecom = $suc->getModule("webservice_imatelecom");

        //$competenciaAtual = $this->getCompetenciaAtual();
        $competenciaAtual = CompetenciaQuery::create()->findOneByAtivo(1);      
        $competenciaAtual = getAnoMes($competenciaAtual);
        $servicoClientes = $webserviceImatelecom->exportarServicoClientesIma($competenciaAtual->getAno(), $competenciaAtual->getMes());            

        $linha = 1;
        foreach ($servicoClientes[0] as $servicoClientesExportacao) {
            gc_enable();
            Propel::disableInstancePooling();

            $erro = array();                
            $servicoCliente = ServicoClienteQuery::create()->findByImportId($servicoClientesExportacao[0])->getFirst();

            if ($servicoCliente == null) {
                $servicoCliente = new ServicoCliente();
                $servicoCliente->setImportId($servicoClientesExportacao[0]);                

                $servicoPrestado = ServicoPrestadoQuery::create()->findPk($servicoClientesExportacao[1]);
                $cliente = ClienteQuery::create()->findByImportId($servicoClientesExportacao[2])->getFirst();

                if ($servicoPrestado != null) {
                    $servicoCliente->setServicoPrestado($servicoPrestado);
                } else {
                    $erro[] = "Serviço Prestado Inexistente";
                }

                if ($cliente != null) {
                    $servicoCliente->setCliente($cliente);
                } else {
                    $erro[] = "Cliente Inexistente";
                }

                if (!empty($servicoClientesExportacao[3])) {
                    $servicoCliente->setDataContratado($servicoClientesExportacao[3]);
                } else {
                    $servicoCliente->setDataContratado(null);
                }

                if (!empty($servicoClientesExportacao[4]) && $servicoClientesExportacao[4] != "0000-00-00 00:00:00") {
                    $servicoCliente->setDataCancelado($servicoClientesExportacao[4]);
                } else {
                    $servicoCliente->setDataCancelado(null);
                }

                if (!empty($servicoClientesExportacao[5])) {
                    $servicoCliente->setCodCesta($servicoClientesExportacao[5]);
                } else {
                    $servicoCliente->setCodCesta(null);
                }

                $servicoCliente->setDataCadastro(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $servicoCliente->setDataAlterado(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $servicoCliente->setUsuarioAlterado(1);

                if (sizeof($erro) == 0) {
                    try {
                        $servicoCliente->save();
                        $erros[] = "\n\rLinha $linha: Código: " . $servicoClientesExportacao[0] . " - Importado com sucesso.";
                    } catch (\PropelException $e) {
                        $erro[] = $e->getMessage();
                        $erros[] = "\n\rLinha $linha: Código: " . $servicoClientesExportacao[0] . " - " . implode(" | ", $erro);
                    }   
                } else {
                    $erros[] = "\n\rLinha $linha: Código: " . $servicoClientesExportacao[0] . " - " . implode(" | ", $erro);
                }
            } else {
                $erros[] = "\n\rLinha $linha: Código: " . $servicoClientesExportacao[0] . " - Serviço do Cliente já existe no Sistema.";
            }

            $linha++;
        }
    }

    echo "\t- Fim da Importação dos Serviços de Clientes ***** \n\n";
    
    return $erros;
}

?>