<?php

//namespace Ima\DefaultBundle\Scripts;

use ImaTelecomBundle\Lib\SUC;
use Propel\Runtime\Propel;

use ImaTelecomBundle\Model\EnderecoServCliente;
use ImaTelecomBundle\Model\EnderecoServClienteQuery;
use ImaTelecomBundle\Model\ServicoClienteQuery;
use ImaTelecomBundle\Model\CidadeQuery;
use ImaTelecomBundle\Model\CompetenciaQuery;

function ImportacaoEnderecoServClientesSuc() {
    echo "\t- Iniciando da Importação dos Endereços dos Serviços de Clientes ***** \n\n";

    $erros = array();
    $suc = SUC::getInstancia();

    $resp = $suc->autenticar();
    if ($resp[0] == True) {
        $webserviceImatelecom = $suc->getModule("webservice_imatelecom");

        //$competenciaAtual = $this->getCompetenciaAtual();
        $competenciaAtual = CompetenciaQuery::create()->findOneByAtivo(1);      
        $competenciaAtual = getAnoMes($competenciaAtual);
        $enderecoServClientes = $webserviceImatelecom->exportarEnderecoServClientesIma($competenciaAtual->getAno(), $competenciaAtual->getMes());            

        $linha = 1;
        foreach ($enderecoServClientes[0] as $enderecoServClienteExportacao) {
            gc_enable();
            Propel::disableInstancePooling();

            $erro = array();

            $endereco = EnderecoServClienteQuery::create()->findByImportId($enderecoServClienteExportacao[0])->getFirst();
            if ($endereco == null) {
                $endereco = new EnderecoServCliente();
                $endereco->setImportId($enderecoServClienteExportacao[0]);
                $servocp = ServicoClienteQuery::create()->findByImportId($enderecoServClienteExportacao[1])->getFirst();

                if ($servocp == null) {
                    $erro[] = "Serviço do Cliente Inexistente";
                } else {
                    $endereco->setServicoCliente($servocp);
                }

                $endereco->setRua(clean(remove_accents($enderecoServClienteExportacao[2])));
                $endereco->setBairro(clean(remove_accents($enderecoServClienteExportacao[3])));

                if (!empty($enderecoServClienteExportacao[4])) {
                    $endereco->setNum($enderecoServClienteExportacao[4]);
                } else {
                    $endereco->setNum(null);
                }

                $endereco->setComplemento(clean(remove_accents($enderecoServClienteExportacao[5])));

                if (!empty($enderecoServClienteExportacao[7])) {
                    $endereco->setCep($enderecoServClienteExportacao[7]);
                } else {
                    $endereco->setCep(null);
                }

                $endereco->setUf($enderecoServClienteExportacao[8]);
                $endereco->setPontoReferencia(clean(remove_accents($enderecoServClienteExportacao[9])));
                $endereco->setContato(clean(remove_accents($enderecoServClienteExportacao[10])));

                $cidade = CidadeQuery::create()->findPk($enderecoServClienteExportacao[6]);

                if ($cidade == null) {
                    $erro[] = "Cidade Inexistente";
                } else {
                    $endereco->setCidade($cidade);
                }

                $endereco->setDataCadastro(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $endereco->setDataAlterado(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $endereco->setUsuarioAlterado(1);

                if (sizeof($erro) == 0) {
                    try {
                        $endereco->save();
                        $erros[] = "\n\rLinha $linha: Código: " . $enderecoServClienteExportacao[0] . " - Importado com sucesso.";
                    } catch (\PropelException $e) {
                        $erro[] = $e->getMessage();
                        $erros[] = "\n\rLinha $linha: Código: " . $enderecoServClienteExportacao[0] . " - " . implode(" | ", $erro);
                    }   
                } else {
                    $erros[] = "\n\rLinha $linha: Código: " . $enderecoServClienteExportacao[0] . " - " . implode(" | ", $erro);
                }
            } else {
                $erros[] = "\n\rLinha $linha: Código: " . $enderecoServClienteExportacao[0] . " - Endereço já existe no Sistema.";
            }

            $linha++;
        }
    }

    echo "\t- Fim da Importação dos Endereços dos Serviços de Clientes ***** \n\n";
    
    return $erros;
}

?>