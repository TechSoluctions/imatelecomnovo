<?php

//namespace Ima\DefaultBundle\Scripts;

use ImaTelecomBundle\Lib\SUC;
use Propel\Runtime\Propel;

use ImaTelecomBundle\Model\EnderecoCliente;
use ImaTelecomBundle\Model\EnderecoClienteQuery;
use ImaTelecomBundle\Model\ClienteQuery;
use ImaTelecomBundle\Model\CidadeQuery;
use ImaTelecomBundle\Model\CompetenciaQuery; 


function ImportacaoEnderecoClienteSuc() {
    echo "\t- Inciando a Importação dos Endereços dos Clientes ***** \n";
        
    $erros = array();
        $suc = SUC::getInstancia();

        $resp = $suc->autenticar();
        if ($resp[0] == True) {
            $webserviceImatelecom = $suc->getModule("webservice_imatelecom");
            
            //$competenciaAtual = $this->getCompetenciaAtual();
            $competenciaAtual = CompetenciaQuery::create()->findOneByAtivo(1);      
            $competenciaAtual = getAnoMes($competenciaAtual);
            $enderecoClientes = $webserviceImatelecom->exportarEnderecoClientesIma($competenciaAtual->getAno(), $competenciaAtual->getMes());            
            
            $linha = 1;
            foreach ($enderecoClientes[0] as $enderecoClientesExportacao) {
                gc_enable();
                Propel::disableInstancePooling();

                $erro = array();                
                $endereco = EnderecoClienteQuery::create()->findByImportId($enderecoClientesExportacao[0])->getFirst();
                if ($endereco == null) {
                    $endereco = new EnderecoCliente();
                    $endereco->setImportId($enderecoClientesExportacao[0]);
                    $cliente = ClienteQuery::create()->findByImportId($enderecoClientesExportacao[1])->getFirst();

                    if ($cliente == null) {
                        $erro[] = "Cliente Inexistente";
                    } else {
                        $endereco->setCliente($cliente);
                    }

                    $endereco->setRua(clean(remove_accents($enderecoClientesExportacao[2])));
                    $endereco->setBairro(clean(remove_accents($enderecoClientesExportacao[3])));

                    if (!empty($enderecoClientesExportacao[4])) {
                        $endereco->setNum($enderecoClientesExportacao[4]);
                    } else {
                        $endereco->setNum(null);
                    }

                    $endereco->setComplemento(clean(remove_accents($enderecoClientesExportacao[5])));

                    if (!empty($enderecoClientesExportacao[7])) {
                        $endereco->setCep($enderecoClientesExportacao[7]);
                    } else {
                        $endereco->setCep(null);
                    }

                    $endereco->setUf($enderecoClientesExportacao[8]);
                    $endereco->setPontoReferencia(clean(remove_accents($enderecoClientesExportacao[9])));
                    $endereco->setContato(clean(remove_accents($enderecoClientesExportacao[10])));

                    $cidade = CidadeQuery::create()->findPk($enderecoClientesExportacao[6]);

                    if ($cidade == null) {
                        $erro[] = "Cidade Inexistente";
                    } else {
                        $endereco->setCidade($cidade);
                    }


                    $endereco->setDataCadastro(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                    $endereco->setDataAlterado(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                    $endereco->setUsuarioAlterado(1);

                    if (sizeof($erro) == 0) {
                        try {
                            $endereco->save();
                            $erros[] = "\n\rLinha $linha: Código: " . $enderecoClientesExportacao[0] . " - Importado com sucesso.";
                        } catch (\PropelException $e) {
                            $erro[] = $e->getMessage();
                            $erros[] = "\n\rLinha $linha: Código: " . $enderecoClientesExportacao[0] . " - " . implode(" | ", $erro);
                        }   
                    } else {
                        $erros[] = "\n\rLinha $linha: Código: " . $enderecoClientesExportacao[0] . " - " . implode(" | ", $erro);
                    }
                } else {
                    $erros[] = "\n\rLinha $linha: Código: " . $enderecoClientesExportacao[0] . " - Endereço do Cliente já existe no Sistema.";
                }

                $linha++;
            }
        }

    echo "\t- Fim da Importação dos Endereços dos Clientes ***** \n\n";

    return $erros;         
}

?>