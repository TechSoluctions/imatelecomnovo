<?php

//namespace Ima\DefaultBundle\Scripts;

use ImaTelecomBundle\Lib\SUC;
use Propel\Runtime\Propel;

use ImaTelecomBundle\Model\Boleto;
use ImaTelecomBundle\Model\BoletoQuery;
use ImaTelecomBundle\Model\ClienteQuery;
use ImaTelecomBundle\Model\CompetenciaQuery;


function ImportacaoBoletosSuc() {
    echo "\t- Inciando a Importação dos Boletos ***** \n";
        
    $erros = array();
    $suc = SUC::getInstancia();

    $resp = $suc->autenticar();
    if ($resp[0] == True) {
        $webserviceImatelecom = $suc->getModule("webservice_imatelecom");

                    //$competenciaAtual = $this->getCompetenciaAtual();
        $competenciaAtual = CompetenciaQuery::create()->findOneByAtivo(1);        
        $competenciaAtual = getAnoMes($competenciaAtual);
        $boletos = $webserviceImatelecom->exportarBoletosIma($competenciaAtual->getAno(), $competenciaAtual->getMes());                        
        
        $linha = 1;
        foreach ($boletos[0] as $boletosExportacao) {
            gc_enable();
            Propel::disableInstancePooling();

            $erro = array();                

            $boleto = BoletoQuery::create()->findByImportId($boletosExportacao[0])->getFirst();
            if ($boleto == null) {
                $boleto = new Boleto();
                $boleto->setImportId($boletosExportacao[0]);                
                $boleto->setValor($boletosExportacao[1]);   

                try {
                    if (!empty($boletosExportacao[2])) {
                        //$boleto->setVencimento(new \DateTime($boletosExportacao[2]));
                        $boleto->setVencimento($boletosExportacao[2]);
                    } else {
                        $boleto->setVencimento(null);
                    }
                } catch (\Exception $e) {
                    $erro[] = "Data de Vencimento Inválida";
                }

                $cliente = ClienteQuery::create()->findByImportId($boletosExportacao[3])->getFirst();

                if ($cliente == null) {
                    $erro[] = "Cliente Inexistente";
                } else {
                    $boleto->setCliente($cliente);
                }

                $boleto->setDataCadastro(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                $boleto->setDataAlterado(date_format( new \DateTime('now'), 'Y-m-d H:i:s' ));
                //$boleto->setUsuarioAlterado($this->getUsuarioLogado());                               
                $boleto->setUsuarioAlterado(1);

                if ($boleto->getCompetenciaId() != null && $boleto->getCompetenciaId() != 0) {
                    $erro[] = "Boleto já se encontra importado no sistema, nenhum dado pôde ser atualizado por motivos de segurança.";
                } else {
                    $boleto->setCompetenciaId($competenciaAtual->getId());
                }                

                if (($competenciaAtual->getAtivo() == 0 || $competenciaAtual->getEncerramento() == 1) && $competenciaAtual->getPeriodoFuturo() != 1) {
                    $erro[] = "Verifique se a Competência está ativa e/ou não esteja encerrada.";
                }

                if (sizeof($erro) == 0) {
                    try {
                        $boleto->save();
                        $erros[] = "\n\rLinha $linha: Código: " . $boletosExportacao[0] . " - Importado com sucesso.";
                    } catch (\PropelException $e) {
                        $erro[] = $e->getMessage();
                        $erros[] = "\n\rLinha $linha: Código: " . $boletosExportacao[0] . " - " . implode(" | ", $erro);
                    }   
                } else {
                    $erros[] = "\n\rLinha $linha: Código: " . $boletosExportacao[0] . " - " . implode(" | ", $erro);
                } 
            } else {
                $erros[] = "\n\rLinha $linha: Código: " . $boletosExportacao[0] . " - Boleto já existe no Sistema.";
            }

            $linha++;
        } 
    }

    echo "\t- Fim da Importação dos Boleto ***** \n\n";

    return $erros;         
}

?>