/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Cliente {
    constructor(clienteId) {
        this.clienteId = clienteId;
    }

    formatarUrl(novaUrl) {
        var url = window.location.href;
        var posIni = url.indexOf("/admin/");
        var urlReplace = url.substr(posIni, url.length);
        return url.replace(urlReplace, novaUrl);
    }

    atualizarServicos(container) {
        var url = this.formatarUrl('/admin/administrativo/servicos/cliente/' + this.clienteId);
   
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response, textStatus, jqXHR) {
                $(container).html(response);
            }
        });
    }

    atualizarCobrancas(container) {
        var url = this.formatarUrl('/admin/administrativo/cobrancas/cliente/' + this.clienteId);
   
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response, textStatus, jqXHR) {
                $(container).html(response);
            }
        });
    }
}