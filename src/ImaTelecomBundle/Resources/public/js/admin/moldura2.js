"use strict";
$(document).ready(function () {
    var mensagemEncode = window.location.hash.slice(1);
    var mensagem = Base64.decode(mensagemEncode);

    if (mensagem != '') {
        $.notify(mensagem, {allow_dismiss: true, type: 'info'});
        window.location.hash = '';
    }



    /*
     $("li.niagaweb-hasmenu").on('click', function (e) {
     e.preventDefault();
     console.log("oi")      
     
     $(this).toggleClass('niagaweb-trigger')
     if ($('.niagaweb-submenu', this).length) {
     var elm = $('.niagaweb-submenu:first', this);
     var off = elm.offset();
     var l = off.left;
     var w = elm.width();
     var docH = $(window).height();
     var docW = $(window).width();
     
     var isEntirelyVisible = (l + w <= docW);
     if (!isEntirelyVisible) {
     $(this).toggleClass('edge');
     }
     
     }
     });*/

    $('.user-profile').click(function (e) {
        let child = $(this).children('.show-notification');
        child.toggleClass('item-visivel');
    });

    $('li.niagaweb-hasmenu').click(function (e) {
        $(this).toggleClass('niagaweb-trigger');
        $(this).children('ul').slideToggle('edge');
    });

    var $window = $(window);
    //add id to main menu for mobile menu start
    var getBody = $("body");
    var bodyClass = getBody[0].className;
    $(".main-menu").attr('id', bodyClass);
    //add id to main menu for mobile menu end

    // card js start
    $(".card-header-right .close-card").on('click', function () {
        var $this = $(this);
        $this.parents('.card').animate({
            'opacity': '0',
            '-webkit-transform': 'scale3d(.3, .3, .3)',
            'transform': 'scale3d(.3, .3, .3)'
        });

        setTimeout(function () {
            $this.parents('.card').remove();
        }, 800);
    });
    $(".card-header-right .reload-card").on('click', function () {
        var $this = $(this);

        $this.parents('.card').addClass("card-load");
        $this.parents('.card').append('<div class="card-loader"><i class="icofont icofont-refresh rotate-refresh"></div>');
        setTimeout(function () {
            $this.parents('.card').children(".card-loader").remove();
            $this.parents('.card').removeClass("card-load");
        }, 3000);
    });
    $(".card-header-right .card-option .icofont-simple-left").on('click', function () {
        var $this = $(this);
        if ($this.hasClass('icofont-simple-right')) {
            $this.parents('.card-option').animate({
                'width': '35px',
            });
        } else {
            $this.parents('.card-option').animate({
                'width': '180px',
            });
        }
        $(this).toggleClass("icofont-simple-right").fadeIn('slow');
        // $this.children("li .icofont-simple-left").toggleClass("");
    });

    $(".card-header-right .minimize-card").on('click', function () {
        var $this = $(this);
        var port = $($this.parents('.card'));
        var card = $(port).children('.card-block').slideToggle();
        $(this).toggleClass("icofont-plus").fadeIn('slow');
    });
    $(".card-header-right .full-card").on('click', function () {
        var $this = $(this);
        var port = $($this.parents('.card'));
        port.toggleClass("full-card");
        $(this).toggleClass("icofont-resize");
    });

    $(".card-header-right .icofont-spinner-alt-5").on('mouseenter mouseleave', function () {
        $(this).toggleClass("rotate-refresh").fadeIn('slow');
    });
    $("#more-details").on('click', function () {
        $(".more-details").slideToggle(500);
    });
    $(".mobile-options").on('click', function () {
        $(".navbar-container .nav-right").slideToggle('slow');
    });
    $(".main-search").on('click', function () {
        $("#morphsearch").addClass('open');
    });
    $(".morphsearch-close").on('click', function () {
        $("#morphsearch").removeClass('open');
    });
    // card js end

    $('#mobile-collapse,.sidebar_toggle a, .niagaweb-overlay-box,.menu-toggle a').on("click", function () {
        //$(this).parent().find('.menu-icon').toggleClass("is-clicked");
        /*
         var dt = $('#' + oid).attr("niagaweb-device-type");
         if (dt == "desktop") {
         var dmc = settings.onToggleVerticalMenu.desktop;
         var dm = settings.defaultVerticalMenu.desktop;
         var dn = $('#' + oid).attr("vertical-nav-type");
         if (dn == dm) {
         $('#' + oid).attr("vertical-nav-type", dmc);
         } else if (dn == dmc) {
         $('#' + oid).attr("vertical-nav-type", dm);
         } else {
         return false;
         }
         } else if (dt == "tablet") {
         var tmc = settings.onToggleVerticalMenu.tablet;
         var tm = settings.defaultVerticalMenu.tablet;
         var tn = $('#' + oid).attr("vertical-nav-type");
         if (tn == tm) {
         $('#' + oid).attr("vertical-nav-type", tmc);
         } else if (dn == dmc) {
         $('#' + oid).attr("vertical-nav-type", tm);
         }
         } else if (dt == "phone") {
         var pmc = settings.onToggleVerticalMenu.phone;
         var pm = settings.defaultVerticalMenu.phone;
         var pn = $('#' + oid).attr("vertical-nav-type");
         if (pn == pm) {
         $('#' + oid).attr("vertical-nav-type", pmc);
         } else if (dn == dmc) {
         $('#' + oid).attr("vertical-nav-type", pm);
         }
         }*/

        console.log($('.niagaweb-navbar').is(':visible'))
        if ($('.niagaweb-navbar').is(':visible') == true) {
            $('.niagaweb-navbar').hide();
            $('.niagaweb-content').css('marginLeft', '0px');
        } else {
            $('.niagaweb-navbar').show();
            $('.niagaweb-content').css('marginLeft', '270px');
        }
    });

    // check if container has slimScroll, if yes remove it
    if ($('.main-menu').parent('.slimScrollDiv').size() > 0)
        $('.main-menu').parent().replaceWith($('.main-menu'));

    // now (re)assign slimScroll
    $('.main-menu').slimScroll({
        height: '100%',
        position: 'right',
        size: "7px",
        color: '#bbb',
        alwaysVisible: false
    });
});


function DownloadFile(response, status, xhr) {
    // check for a filename
    var filename = "";
    var disposition = xhr.getResponseHeader('Content-Disposition');
    if (disposition && disposition.indexOf('attachment') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1])
            filename = matches[1].replace(/['"]/g, '');
    }

    if (filename === '' || filename === 'undefined') {
        return false;
    }

    var type = xhr.getResponseHeader('Content-Type');
    var blob = new Blob([response], {type: type});

    if (typeof window.navigator.msSaveBlob !== 'undefined') {
        // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
        window.navigator.msSaveBlob(blob, filename);
    } else {
        var URL = window.URL || window.webkitURL;
        var downloadUrl = URL.createObjectURL(blob);

        if (filename) {
            // use HTML5 a[download] attribute to specify filename
            var a = document.createElement("a");
            // safari doesn't support this yet
            if (typeof a.download === 'undefined') {
                window.location = downloadUrl;
            } else {
                a.href = downloadUrl;
                a.download = filename;
                document.body.appendChild(a);
                a.click();
            }
        } else {
            window.location = downloadUrl;
        }

        setTimeout(function () {
            URL.revokeObjectURL(downloadUrl);
        }, 100); // cleanup
    }

    return true;
}


// toggle full screen
function toggleFullScreen() {
    var a = $(window).height() - 10;

    if (!document.fullscreenElement && // alternative standard method
            !document.mozFullScreenElement && !document.webkitFullscreenElement) { // current working methods
        if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function mostrarNotificacao(titulo, erros, timer = 60000, type = 'danger') {
    $.notify({
        title: titulo,
        message: erros
    }, {
        allow_dismiss: true,
        type: type,
        timer: timer, // 1 minuto                                
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        template: '<div data-notify="container" class="col-xs-11 col-sm-4 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title" class="negrito">{1}</span> <hr class="separator-h-10">' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
    });
}

function Capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function mostrarMensagem(className, titulo, subtitulo, mensagem) {
    $('#alerta_status_popup').attr('class', 'alert alert-' + className);
    $('#alerta_titulo_popup').text(titulo);
    $('#alerta_subtitulo_popup').text(subtitulo);
    $('#erros_alerta_popup').html(mensagem);
    $('#container_alerta_popup').removeClass('oculto');
    setTimeout(function () {
        $('#container_alerta_popup').addClass('oculto');
    }, 60000);
}


// colocar apenas para os que possuem popup
function mostrarMensagemPopup(className, titulo, subtitulo, mensagem) {
    $('.alerta_status_popup').attr('class', 'alerta_status_popup alert alert-' + className);
    $('.alerta_titulo_popup').text(titulo);
    $('.alerta_subtitulo_popup').text(subtitulo);
    $('.erros_alerta_popup').html(mensagem);
    $('.container_alerta_popup').removeClass('oculto');
    setTimeout(function () {
        $('.container_alerta_popup').addClass('oculto');
    }, 60000);
}

Number.prototype.formatMoney = function (decPlaces, thouSeparator, decSeparator) {
    var n = this,
            decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSeparator = decSeparator == undefined ? "." : decSeparator,
            thouSeparator = thouSeparator == undefined ? "," : thouSeparator,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
};

/**
 * Number.prototype.format(n, x)
 * 
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */
Number.prototype.format = function (n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

function RemoveAllDialogs() {
    $('')
}

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}

function FormatarData(data, formato) {
    let dataFormatada = '';
    let formatos = formato.split("");

    formatos.forEach(function (letter, index, array) {
        switch (letter) {
            case "d":
                let dia = data.getDate().toString(),
                        diaFormatado = (dia.length === 1) ? ("0" + dia) : dia;
                dataFormatada += diaFormatado;
                break;

            case "m":
                let mes = (data.getMonth() + 1).toString(),
                        mesFormatado = (mes.length === 1) ? ("0" + mes) : mes;
                dataFormatada += mesFormatado;
                break;

            case "Y":
                dataFormatada += data.getFullYear();
                break;

            case "H":
                let hora = data.getHours().toString(),
                        horaFormatado = (hora.length === 1) ? ("0" + hora) : hora;
                dataFormatada += horaFormatado;
                break;

            case "i":
                let minuto = data.getMinutes().toString(),
                        minutoFormatado = (minuto.length === 1) ? ("0" + minuto) : minuto;
                dataFormatada += minutoFormatado;
                break;

            case "s":
                let segundo = data.getSeconds().toString(),
                        segundoFormatado = (segundo.length === 1) ? ("0" + segundo) : segundo;
                dataFormatada += segundoFormatado;
                break;

            default:
                dataFormatada += String(letter);
                break;
        }
    });

    return dataFormatada;
}