/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 (function (factory) {
 "use strict";
 
 if (typeof define === 'function' && define.amd) {
 // AMD
 define(['jquery'], function ($) {
 return factory($, window, document);
 });
 } else if (typeof exports === 'object') {
 // CommonJS
 module.exports = function (root, $) {
 if (!root) {
 // CommonJS environments without a window global must pass a
 // root. This will give an error otherwise
 root = window;
 }
 
 if (!$) {
 $ = typeof window !== 'undefined' ? // jQuery's factory checks for a global window
 require('jquery') :
 require('jquery')(root);
 }
 
 return factory($, root, root.document);
 };
 } else {
 // Browser
 factory(jQuery, window, document);
 }
 }
 (function ($, window, document, undefined) {
 "use strict";
 
 var AutoComplete = function (options)
 {
 this.$ = function (sSelector, oOpts)
 {
 return this.api(true).$(sSelector, oOpts);
 };
 
 this._ = function (sSelector, oOpts)
 {
 return this.api(true).rows(sSelector, oOpts).data();
 };
 
 this.api = function (traditional)
 {
 return traditional ?
 new _Api(
 _fnSettingsFromNode(this[ _ext.iApiIndex ])
 ) :
 new _Api(this);
 };               
 
 return this;
 }
 
 var _ext; // DataTable.ext
 var _Api; // DataTable.Api
 //
 
 AutoComplete.setUrl = function( url )
 {
 console.log(url);
 };
 
 // jQuery access
 $.fn.autoComplete = AutoComplete;
 
 // Provide access to the host jQuery object (circular reference)
 AutoComplete.$ = $;
 
 return $.fn.autoComplete;
 }));
 */

(function ($) {
    
    $.fn.autocomplete = function (options) {

        var defaults = {
            url: '',
            type: 'GET',
            data: {
                nome: '',
                limit: 5
            },
            showResults: '',
            top: 35,
            left: 1
        };

        //var options = $.extend({}, $.fn.autocomplete.defaults, options);
        var options = $.extend({}, defaults, options);
        var _autocomplete = this;
        var results = [];
        var isSelected = false;
        /*
         if (options && typeof(options) == 'string') {
         if (options == 'onClickListener') {
         this.onClickListener( arg );
         }
         else if (options == 'onClickListener2') {
         //myplugin_method2( arg );
         }
         return;
         }*/
        
        $('body').children('section.autocomplete').remove();
        $(this).attr('autocomplete', 'off');
        
//        $(this).focusout(function() {
//            $('body').children('section.autocomplete').remove();
//        });
        
        $(this).click(function () {
            _autocomplete.search(this);
        });
        
        $(this).keyup(function () {
            _autocomplete.search(this);
        });
        
        this.search = function (element) {
            options.url = $(element).data('url');
            options.data.nome = $(element).val();
            
            $.ajax({
                url: options.url,
                type: options.type,
                data: options.data,
                success: function (response, textStatus, jqXHR) {
                    _autocomplete.showResults(response);
                }, error: function (response, textStatus, jqXHR) {
                    console.log(response, textStatus, jqXHR);
                }
            });
        };

        this.onSelect = function () {
            var input = $(this);
            $('.autocomplete-select').click(function (e) {
                e.preventDefault();

                var id = $(this).data('object');
                $.each(_autocomplete.results, function (index, object) {                    
                    if (id === object.Id) {                        
                        if (options.hasOwnProperty('onSelect')) {
                            options.onSelect(object.Object);
                        } else {
                            input.val(object.Label);
                        }
                    
                        $('body').children('section.autocomplete').remove();
                        return;
                    }
                });
            });
        };
        
        this.showResults = function (objects) {
            var _objects = options.showResults(objects);
            _autocomplete.results = _objects;

            var html = '<section class="autocomplete avoid-clicks-child">';
            html += '       <ul>';

            $.each(_objects, function (index, object) {
                html += '       <li>';
                html += '           <a class="autocomplete-select" data-object="' + object.Id + '">';
                html += object.Label;
                html += '           </a>';
                html += '       </li>';
            });

            html += '       </ul>';
            html += '   </section>';

            $('body').children('section.autocomplete').remove();
            $('body').append(html);

            var parent = $(this);
            var positionParent = $(parent).position();
            var paddingLeftParent = parseInt($(parent).css('padding-left').replace("px", ""));
            var paddingRightParent = parseInt($(parent).css('padding-right').replace("px", ""));
            var widthParent = $(parent).width();
            var width = paddingLeftParent + widthParent + paddingRightParent;

            $('section.autocomplete').css({
                top: positionParent.top + parseInt(options.top),
                left: positionParent.left + parseInt(options.left),
                width: width
            });
            
            this.onSelect();

            return;
        };

        return _autocomplete;
    };
}(jQuery));
