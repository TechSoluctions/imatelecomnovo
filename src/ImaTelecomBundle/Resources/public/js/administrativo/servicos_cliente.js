/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() {
    $('.open-popup.servico-editar').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        var popup = new $.Popup($("section.main"));
        popup.Mostrar(url);
        popup.setTitle('Cadastro de Serviço');

        return false;
    });
    
    $('.open-popup.servico-notas-fiscais').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        var popup = new $.Popup($("section.main"));
        popup.Mostrar(url);
        popup.setTitle('Notas Fiscais de Serviço');

        return false;
    });

    $('.section.popup a.close-popup').click(function (e) {
        e.preventDefault();

        $('section.main section.open-popup').remove();

        return false;
    });
});