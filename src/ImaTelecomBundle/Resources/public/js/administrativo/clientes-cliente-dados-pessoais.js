/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $('.chosen-select').chosen();
    moment.locale('pt-br'); // default the locale to Portuguese Brazil

    $('#dataNascimento').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    let dataContratoCliente = $('#dataContratoCliente').attr('readonly');
    if (dataContratoCliente !== 'readonly') {
        $('#dataContratoCliente').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
    }

    $('.tipo-pessoa').change(function () {
        var tipo = $(this).val();
        var containerFisica = '.container-pessoa-fisica';
        var containerJuridica = '.container-pessoa-juridica';

        if (tipo === 'juridica') {
            $(containerFisica).addClass('oculto');
            $(containerJuridica).removeClass('oculto');
        } else {
            $(containerJuridica).addClass('oculto');
            $(containerFisica).removeClass('oculto');
        }
    });
    /*
    $('#fornecedor').change(function () {
        let isChecked = $(this).is(':checked');
        
        if (isChecked) {
            $('#menu_fornecedor').removeClass('oculto');
        } else {
            $('#menu_fornecedor').addClass('oculto');
        }
    });*/

    $('body').on('submit', '#formulario_pessoa', function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $("html").offset().top
        }, 500);

        let url = $(this).attr('action');
        let type = $(this).attr('method');
        let data = $(this).serializeArray();

        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function (response, textStatus, jqXHR) {
                if (response.status) {
                    //mostrarMensagem('success', 'Bem Feeito!!', '', response.mensagem);                    
                    if ('redirect_url' in response) {
                        window.location.href = response.redirect_url + '#' + Base64.encode(response.mensagem).slice(0, -1);
                        window.location.reload();
                    } else {
                        mostrarMensagem('success', 'Bem Feeito!!', '', response.mensagem);
                    }                    
                } else {
                    mostrarMensagem('danger', 'Ops... Aconteceu algo de errado!',
                            'Por favor, verifique os erros ocorridos que foram listados abaixo.',
                            response.erros.join('<br/>'));
                }
            },
            error: function (request, textStatus, errorThrown) {
                console.log(request, textStatus, errorThrown);
                mostrarMensagem('danger', 'Ops... Aconteceu algo de errado!',
                        'Por favor, verifique os erros ocorridos que foram listados abaixo.', errorThrown);
            }
        });
    });

});