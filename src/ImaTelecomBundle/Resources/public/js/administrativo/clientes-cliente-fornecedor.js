/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var atualizarContainer = 'fornecedor';
var dialogContaBancaria = null;

$(document).ready(function () {
    $('.chosen-conta').chosen();

    $('#formulario_fornecedor').submit(function (e) {
        e.preventDefault();

        let dados = $(this).serialize();
        let url = $(this).attr('action');
        let type = $(this).attr('method');

        $.ajax({
            url: url,
            type: type,
            data: dados,
            success: function (data, textStatus, jqXHR) {
                if (data.status) {
                    mostrarNotificacao('Beem Feito', data.mensagem, 5000, 'success');
                    AtualizarFornecedor();
                } else {
                    mostrarNotificacao('Não foi possível executar esta ação!', data.erros.join('\n'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
                mostrarNotificacao('Não foi possível executar esta ação!', errorThrown);
            }
        });
    });

    $('.toggleFornecedor').click(function (e) {
        e.preventDefault();

        $('html, body').animate({
            scrollTop: $("html").offset().top
        }, 500);

        let url = $(this).data('url');

        bootbox.confirm({
            title: "Mudar Status do Fornecedor",
            message: "Você quer mudar o Status do Fornecedor? Isto implicará no Contas à Pagar na geração de Cobranças para ele.",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> Cancelar',
                    className: 'm-bottom-0 btn-danger'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> Confirmar',
                    className: 'm-left-20 btn-primary'
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        success: function (data, textStatus, jqXHR) {
                            if (data.status) {
                                mostrarNotificacao('Beem Feito', data.mensagem, 5000, 'success');
                                AtualizarFornecedor();
                            } else {
                                mostrarNotificacao('Não foi possível executar esta ação!', data.erros.join('\n'));
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR, textStatus, errorThrown);
                            mostrarNotificacao('Não foi possível executar esta ação!', errorThrown);
                        }
                    });
                }
            }
        });
    });

    $('.conta-bancaria').click(function (e) {
        e.preventDefault();

        var title = $(this).data('title');
        var url = $(this).data('url');

        $.ajax({
            url: url,
            type: 'GET',
            success: function (response, textStatus, jqXHR) {
                dialogContaBancaria = bootbox.dialog({
                    title: title,
                    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                    size: 'large',
                    onEscape: function () {
                    }
                });

                dialogContaBancaria.init(function () {
                    setTimeout(function () {
                        dialogContaBancaria.find('.bootbox-body').html(response);
                    }, 3000);
                });

                $('.modal-dialog').css('width:1000px !important');
            },
            error: function (request, textStatus, errorThrown) {
                console.log(request, textStatus, errorThrown);
                mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
            }
        });
    });
});