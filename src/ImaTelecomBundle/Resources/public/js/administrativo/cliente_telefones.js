/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $(".excluir-telefone").click(function (event) {
        event.preventDefault();

        var urlTelefones = $('#adicionar_telefone').data('href');
        urlTelefones = urlTelefones.replace('adicionar-telefone', 'telefones');
        var url = $(this).data('href');
        $.ajax({
            url: url,
            type: 'POST',
            success: function (response, textStatus, jqXHR) {
                if (response.status == true) {
                    AtualizarTelefones(urlTelefones);

                    $("#alerta_topo").removeClass('alert-danger');
                    $("#alerta_mensagem").text(response.mensagem);
                    $("#alerta_topo").addClass('alert-success');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-success', 5000);
                } else {
                    var erros = "";
                    $.each(response.erros, function (key, erro) {
                        erros += "<br/>" + erro;
                    });

                    $("#alerta_mensagem").html(erros);
                    $("#alerta_topo").addClass('alert-danger');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-danger', 15000);
                }
            }
        });

        return false;
    });
});