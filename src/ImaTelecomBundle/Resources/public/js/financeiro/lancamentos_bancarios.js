/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {
    var table = $('table.table').dataTable({
        lengthMenu: [[15, 50, -1], [15, 50, "Todos"]],
        order: [[0, "desc"]],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        }
    });

    new $.fn.dataTable.FixedHeader(table);

    $('.detalhes-lancamento').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        var popup = new $.Popup($("section.main"));
        popup.Mostrar(url);
        popup.setTitle('Detalhes do Lançamento Bancário');

        return false;
    });

    $('.baixar-lancamento').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        var popup = new $.Popup($("section.main"));
        popup.Mostrar(url);
        popup.setTitle('Realizar Baixa do Lançamento Bancário');

        return false;
    });

    $('.geracao-lancamento').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        var popup = new $.Popup($("section.main"));
        popup.Mostrar(url);
        popup.setTitle('Geração do Lançamento Bancário');

        return false;
    });

    $('.download-lancamento').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');

        $.ajax({
            url: url,
            type: 'POST',
            beforeSend: function ()
            {
                $("body").append("<section class='loading'/>");
            },
            success: function (response, status, xhr) {
                $("section.loading").remove();
                DownloadFile(response, status, xhr);
            },
            error: function (e) {
                console.log(e);
                $("section.loading").remove();
            }
        });
        
        return false;
    });
});