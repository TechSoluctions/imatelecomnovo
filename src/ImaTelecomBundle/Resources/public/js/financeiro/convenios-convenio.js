/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {
    $('.chosen-select').chosen();

    $('body').on('reset', '#formulario_convenio', function () {
        bootbox.hideAll();
    });

    $('#formulario_convenio').submit(function (e) {
        e.preventDefault();

        let dados = $(this).serialize();
        let url = $(this).attr('action');
        let type = $(this).attr('method');

        $.ajax({
            url: url,
            type: type,
            data: dados,
            success: function (data, textStatus, jqXHR) {
                if (data.status) {
                    alert(data.mensagem);

                    let encode = Base64.encode(data.mensagem);
                    encode = encode.replaceAll('=', '');
                    window.location.href = data.redirect_url + '#' + encode;
                    window.location.reload();
                } else {
                    alert(data.erros.join('\n'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    });
});