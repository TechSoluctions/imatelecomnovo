/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $('.chosen-select').chosen();

    $('#formulario_conta').on('reset', function () {
        dialogContaBancaria.modal('hide');
    });

    $('#formulario_conta').submit(function (e) {
        e.preventDefault();

        let dados = $(this).serialize();
        let url = $(this).attr('action');
        let type = $(this).attr('method');

        $.ajax({
            url: url,
            type: type,
            data: dados,
            success: function (data, textStatus, jqXHR) {
                if (data.status) {
                    alert(data.mensagem);

                    if (atualizarContainer === 'fornecedor') {
                        AtualizarFornecedor();
                    } else if (atualizarContainer === 'convenios-rastreamento-contas') { // possível edição no rastreamento das contas pelo convenio
                        AtualizarConvenioRastreamentoContas();
                    } else {
                        let encode = Base64.encode(data.mensagem);
                        encode = encode.replaceAll('=', '');
                        window.location.href = data.redirect_url + '#' + encode;
                        window.location.reload();
                    }
                } else {
                    alert(data.erros.join('\n'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    });
});