/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {
    $('.chosen-select').chosen();

    $('body').on('reset', '#formulario_conta_caixa', function () {
        bootbox.hideAll();
    });

    $('#formulario_conta_caixa').submit(function (e) {
        e.preventDefault();

        let dados = $(this).serialize();
        let url = $(this).attr('action');
        let type = $(this).attr('method');

        $.ajax({
            url: url,
            type: type,
            data: dados,
            success: function (data, textStatus, jqXHR) {
                if (data.status) {
                    alert(data.mensagem);
                    if (atualizarContainer === 'contas-bancarias-rastreamento-contas') { // possível edição no rastreamento das contas pelo convenio
                        AtualizarContasBancariasRastreamentoContas();
                    } else {
                        let encode = Base64.encode(data.mensagem);
                        encode = encode.replaceAll('=', '');
                        window.location.href = data.redirect_url + '#' + encode;
                        window.location.reload();
                    }
                } else {
                    alert(data.erros.join('\n'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    });

    $("#bancoAgenciaContaId").chosen().change(function () {
        DadosContaBancaria(this);
    });

    DadosContaBancaria("#bancoAgenciaContaId");
});

function DadosContaBancaria(objeto) {
    var url = $(objeto).children('option:selected').data('url');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (response, textStatus, jqXHR) {
            let contaBancaria = response.contaBancaria; //JSON.parse(response.servico);
            let tipoConta = contaBancaria.TipoConta === 'poupanca' ? 'Poupança' : 'Corrente';

            $('#bancoNome').val(contaBancaria.BancoNome);
            $('#bancoAgenciaNome').val(contaBancaria.BancoAgenciaNome);
            $('#tipoConta').val(tipoConta);
            $('#numeroConta').val(contaBancaria.NumeroConta);
            $('#digitoVerificador').val(contaBancaria.DigitoVerificador);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            mostrarNotificacao('Erro ao atualizar Serviço Prestado!', errorThrown);
        }
    });
}