/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function CarregarDetalhesHistorico(objeto) {
    var url = $(objeto).data('url');

    $('#container_detalhes_historico').hide();
    
    $.ajax({
        url: url,
        type: 'GET',
        success: function (response, textStatus, jqXHR) {
            let lancamento = response.lancamento,
                    cliente = response.cliente,
                    cobranca = response.historico,
                    baixa = response.baixa,
                    formasPagamento = response.formasPagamento,
                    estorno = response.estorno;

            PreencherLancamento(lancamento);
            PreencherCliente(cliente);
            PreencherCobranca(cobranca);
            PreencherBaixa(baixa, formasPagamento);
            PreencherEstorno(estorno);
            
            $('#container_detalhes_historico').show(2000);
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
        }
    });
}

function PreencherLancamento(lancamento) {
    if (lancamento.Idlancamento !== null) {
        $('#convenioNome').val(lancamento.ConvenioNome);
        $('#convenioNumero').val(lancamento.ConvenioCodigo);
        $('#convenioDigitoVerificador').val(lancamento.ConvenioDigitoVerificador);
        $('#sequencialArquivoCobranca').val(lancamento.SequencialArquivoCobranca);
        $('#lancamentoCompetencia').val(lancamento.CompetenciaAnoMes);
    }
}

function PreencherCliente(cliente) {
    if (cliente.Id !== null) {
        let nomePessoa = cliente.Nome;
        let tipoPessoa = 'Física';

        if (cliente.TipoPessoa === 'juridica') {
            nomePessoa = cliente.RazaoSocial;
            tipoPessoa = 'Jurídica';
        }

        $('#clienteNome').val(nomePessoa);
        $('#clienteTipoPessoa').val(tipoPessoa);
        $('#clienteCpfCnpj').val(cliente.CpfCnpj);
    }
}

function PreencherCobranca(cobranca) {
    if (cobranca.IdboletoBaixaHistorico !== null) {
        let dataVencimento = null,
                dataPagamento = null,
                dataRecusa = null,
                status = '';

        if (cobranca.DataVencimento !== null) {
            dataVencimento = new Date(cobranca.DataVencimento);
            dataVencimento = FormatarData(dataVencimento, 'd/m/Y');
        }

        if (cobranca.DataPagamento !== null) {
            dataPagamento = new Date(cobranca.DataPagamento);
            dataPagamento = FormatarData(dataPagamento, 'd/m/Y H:i:s');
        }

        if (cobranca.DataRecusa !== null) {
            dataRecusa = new Date(cobranca.DataRecusa);
            dataRecusa = FormatarData(dataRecusa, 'd/m/Y H:i:s');
        }

        if (cobranca.Status === 'em_aberto') {
            status = 'Em Aberto';
        } else {
            status = Capitalize(cobranca.Status);
        }

        $('#cobrancaVencimento').val(dataVencimento);
        $('#cobrancaNumeroDocumento').val(cobranca.NumeroDocumento);
        $('#cobrancaCodigoMovimento').val(cobranca.CodigoMovimento);
        $('#cobrancaDescricaoMovimento').val(cobranca.DescricaoMovimento);
        $('#cobrancaValorOriginal').val(cobranca.ValorOriginal);
        $('#cobrancaValorMulta').val(cobranca.ValorMulta);
        $('#cobrancaValorJuros').val(cobranca.ValorJuros);
        $('#cobrancaValorDesconto').val(cobranca.Desconto);
        $('#cobrancaValorTotal').val(cobranca.ValorTotal);
        $('#cobrancaValorTarifa').val(cobranca.ValorTarifaCusto);
        $('#cobrancaDataPagamento').val(dataPagamento);
        $('#cobrancaDataRecusa').val(dataRecusa);
        $('#cobrancaStatus').val(status);
        $('#cobrancaMensagemRetorno').val(cobranca.MensagemRetorno);
    } else {
        let clears = [
            '#cobrancaVencimento',
            '#cobrancaNumeroDocumento',
            '#cobrancaCodigoMovimento',
            '#cobrancaDescricaoMovimento',
            '#cobrancaValorOriginal',
            '#cobrancaValorMulta',
            '#cobrancaValorJuros',
            '#cobrancaValorDesconto',
            '#cobrancaValorTotal',
            '#cobrancaValorTarifa',
            '#cobrancaDataPagamento',
            '#cobrancaDataRecusa',
            '#cobrancaStatus',
            '#cobrancaMensagemRetorno'
        ];

        $.each(clears, function (index, value) {
            $(value).val('');
        });
    }
}

function PreencherBaixa(baixa, formasPagamento) {
    if (baixa.Idbaixa !== null) {
        let dataBaixa = null,
                dataPagamento = null,
                tbody = '';

        if (baixa.DataBaixa !== null) {
            dataBaixa = new Date(baixa.DataBaixa);
            dataBaixa = FormatarData(dataBaixa, 'd/m/Y');
        }

        if (baixa.DataPagamento !== null) {
            dataPagamento = new Date(baixa.DataPagamento);
            dataPagamento = FormatarData(dataPagamento, 'd/m/Y');
        }

        $('#baixaContaCaixa').val(baixa.ContaCaixaDescricao);
        $('#baixaDataBaixa').val(dataBaixa);
        $('#baixaDataPagamento').val(dataPagamento);
        $('#baixaUsuarioBaixa').val(baixa.UsuarioBaixaLogin);
        $('#baixaValorOriginal').val(baixa.ValorOriginal);
        $('#baixaValorMulta').val(baixa.ValorMulta);
        $('#baixaValorJuros').val(baixa.ValorJuros);
        $('#baixaValorDesconto').val(baixa.ValorDesconto);
        $('#baixaValorTotal').val(baixa.ValorTotal);
        $('#baixaCompetencia').val(baixa.CompetenciaAnoMes);
        $('#baixaDescricaoMovimento').val(baixa.DescricaoMovimento);

        $.each(formasPagamento, function (key, forma) {
            let numeroDocumento = forma.NumeroDocumento !== null ? forma.NumeroDocumento : '';
            
            tbody += '<tr>';
            tbody += `<td>${forma.FormaPagamentoNome}</td>`;
            tbody += `<td class="direita">${numeroDocumento}</td>`;
            tbody += `<td>${forma.Valor}</td>`;
            tbody += '</tr>';
        });

        $('#baixaFormasPagamento tbody').html(tbody);
    } else {
        let clears = [
            '#baixaContaCaixa',
            '#baixaDataBaixa',
            '#baixaDataPagamento',
            '#baixaUsuarioBaixa',
            '#baixaValorOriginal',
            '#baixaValorMulta',
            '#baixaValorJuros',
            '#baixaValorDesconto',
            '#baixaValorTotal',
            '#baixaCompetencia',
            '#baixaDescricaoMovimento'
        ];

        $.each(clears, function (index, value) {
            $(value).val('');
        });
        
        $('#baixaFormasPagamento tbody').html('');
    }
}

function PreencherEstorno(estorno) {
    if (estorno.IdbaixaEstorno !== null) {
        let dataEstorno = null;

        if (estorno.DataEstorno !== null) {
            dataEstorno = new Date(estorno.DataEstorno);
            dataEstorno = FormatarData(dataEstorno, 'd/m/Y');
        }

        $('#estornoContaCaixa').val(estorno.ContaCaixaDescricao);
        $('#estornoDataEstorno').val(dataEstorno);
        $('#estornoUsuarioEstorno').val(estorno.UsuarioEstornoLogin);
        $('#estornoValorOriginal').val(estorno.ValorOriginal);
        $('#estornoValorMulta').val(estorno.ValorMulta);
        $('#estornoValorJuros').val(estorno.ValorJuros);
        $('#estornoValorDesconto').val(estorno.ValorDesconto);
        $('#estornoValorTotal').val(estorno.ValorTotal);
        $('#estornoCompetencia').val(estorno.CompetenciaAnoMes);
        $('#estornoDescricaoMovimento').val(estorno.DescricaoMovimento);
    } else {
        let clears = [
            '#estornoContaCaixa',
            '#estornoDataEstorno',
            '#estornoUsuarioEstorno',
            '#estornoValorOriginal',
            '#estornoValorMulta',
            '#estornoValorJuros',
            '#estornoValorDesconto',
            '#estornoValorTotal',
            '#estornoCompetencia',
            '#estornoDescricaoMovimento'
        ];

        $.each(clears, function (index, value) {
            $(value).val('');
        });
    }
}