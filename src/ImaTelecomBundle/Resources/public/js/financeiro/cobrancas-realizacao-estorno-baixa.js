/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//var valorTotal = 0.0;
var valoresSelecionados = {};

$(function () {
    $('.money').mask("#.##0,00", {reverse: true});

    moment.locale('pt-br'); // default the locale to Portuguese Brazil
    $('#dataEstorno').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });


    $('body').on('reset', '#formulario_realizacao_estorno_baixa', function () {
        bootbox.hideAll();
    });

    $('#formulario_realizacao_estorno_baixa').submit(function (e) {
        e.preventDefault();

        if (Object.keys(valoresSelecionados).length === 0) {
            alert('Por favor, selecione ao menos um Lançamento.')
            return;
        }


        let dados = {};
        dados.dataEstorno = $('#dataEstorno').val();
        dados.descricaoMovimento = $('#descricaoMovimento').val();
        dados.lancamentos = valoresSelecionados;

        let url = $(this).attr('action');
        let type = $(this).attr('method');

        $.ajax({
            url: url,
            type: type,
            data: dados,
            success: function (data, textStatus, jqXHR) {
                if (data.status) {
                    alert(data.mensagem);
                } else {
                    alert(data.erros.join('\n'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
            }
        });
    });

    AtualizarValoresGlobais();

    $('body').on('click', '.selecionar-lancamento', function () {
        let container = $(this).data('container-linha');
        AtualizarValoresGlobais();
    });
});


function ValorLinha(objeto) {
    let idOriginal = $(objeto).data('id-valor-original');
    let idMulta = $(objeto).data('id-valor-multa');
    let idJuros = $(objeto).data('id-valor-juros');
    let idDesconto = $(objeto).data('id-valor-desconto');
    let idPago = $(objeto).data('id-valor-pago');

    let valores = {};
    valores.TemValor = false;

    if (idOriginal !== undefined) {
        valores.TemValor = true;
        valores.Original = parseFloat($(idOriginal).data('valor-original').replaceAll(',', ''));
        valores.Multa = parseFloat($(idMulta).data('valor-multa').replaceAll(',', ''));
        valores.Juros = parseFloat($(idJuros).data('valor-juros').replaceAll(',', ''));
        valores.Desconto = parseFloat($(idDesconto).data('valor-desconto').replaceAll(',', ''));
        valores.Pago = parseFloat($(idPago).data('valor-pago').replaceAll(',', ''));

        if (isNaN(valores.Multa)) {
            valores.Multa = 0.00;
        }

        if (isNaN(valores.Juros)) {
            valores.Juros = 0.00;
        }

        if (isNaN(valores.Desconto)) {
            valores.Desconto = 0.00;
        }

        valores.Final = valores.Original + valores.Multa + valores.Juros - valores.Desconto;
    }

    return valores;
}

function AtualizarValoresGlobais() {
    let valoresFinais = {};
    valoresFinais.Original = 0.0;
    valoresFinais.Multa = 0.0;
    valoresFinais.Juros = 0.0;
    valoresFinais.Desconto = 0.0;
    valoresFinais.Pago = 0.0;
    valoresFinais.Final = 0.0;

    $.each($('.selecionar-lancamento'), function (index, objeto) {
        let container = $(objeto).data('container-linha');
        let valoresLinha = ValorLinha(container);

        if (valoresLinha.TemValor) {
            let id = $(container).data('id-lancamento');
            let checkbox = $(container).data('id-checkbox');
            let isChecked = $(checkbox).is(':checked');

            if (isChecked) {
                if (!(id in valoresSelecionados)) {
                    valoresSelecionados[id] = valoresLinha;
                }

                valoresFinais.Original += valoresLinha.Original;
                valoresFinais.Multa += valoresLinha.Multa;
                valoresFinais.Juros += valoresLinha.Juros;
                valoresFinais.Desconto += valoresLinha.Desconto;
                valoresFinais.Pago += valoresLinha.Pago;
                valoresFinais.Final += valoresLinha.Final;
            } else if (!isChecked && id in valoresSelecionados) {
                delete valoresSelecionados[id];
            }
        }
    });


    $('#valorOriginal').val(valoresFinais.Original.format(2));
    $('#valorMulta').val(valoresFinais.Multa.format(2));
    $('#valorJuros').val(valoresFinais.Juros.format(2));
    $('#valorDesconto').val(valoresFinais.Desconto.format(2));
    $('#valorPago').val(valoresFinais.Pago.format(2));
    $('#valorTotal').val(valoresFinais.Final.format(2));
}