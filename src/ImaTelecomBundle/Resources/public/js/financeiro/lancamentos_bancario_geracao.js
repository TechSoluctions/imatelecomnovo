/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$("#tabs").tabs();
var todasCobrancasTable = $('#todas-cobrancas-lancamento').dataTable({
    lengthMenu: [[15, 50, -1], [15, 50, "Todos"]],
    order: [[1, "asc"]],
    language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
    },
    responsive: true
});

new $.fn.dataTable.FixedHeader(todasCobrancasTable);

var algumasCobrancasTable = $('#algumas-cobrancas-lancamento').dataTable({
    lengthMenu: [[15, 50, -1], [15, 50, "Todos"]],
    order: [[1, "asc"]],
    language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
    },
    responsive: true
});

new $.fn.dataTable.FixedHeader(algumasCobrancasTable);

var tipoConvenioCobrancasTable = $('#tipo-convenio-cobrancas-lancamento').dataTable({
    lengthMenu: [[15, 50, -1], [15, 50, "Todos"]],
    order: [[2, "asc"]],
    language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
    },
    responsive: true
});

new $.fn.dataTable.FixedHeader(tipoConvenioCobrancasTable);

$('.tipo-cobranca-geracao').change(function () {
    var tipo = $(this).val();
    if (tipo == 'todos') {
        $("#container_geracao_algumas_cobrancas").hide();
        $("#container_geracao_todas_cobrancas").show();
    } else {
        $("#container_geracao_todas_cobrancas").hide();
        $("#container_geracao_algumas_cobrancas").show();
    }
});

$("table tbody td a.visualizar-detalhes-cobranca").click(function (e) {
    e.preventDefault();

    $("section.main div.item-nota").remove();

    var $row = $(this).closest("tr"), // Finds the closest row <tr> 
            $tds = $row.find("td"),
            wrapper = $("section.main"),
            parentOffset = wrapper.offset(),
            relX = e.pageX - parentOffset.left + wrapper.scrollLeft(),
            relY = e.pageY - parentOffset.top + wrapper.scrollTop(),
            url = $(this).data('url');

    $.ajax({
        url: url,
        type: 'GET',
        beforeSend: function ()
        {
            $("body").append("<section class='loading'/>");
        },
        success: function (response) {
            var tableContent = $(response).addClass('item-nota').css(
                    {
                        left: relX - 90,
                        top: relY + 21
                    }
            );

            $("section.loading").remove();
            $("section.main").append(tableContent);
            $.each($tds, function () {
                $(this).addClass('selected');
            });
        },
        error: function (e) {
            console.log(e);
        }
    });

    return false;
});

$('.convenio').change(function () {
    var url = $(this).data('url');
    $.ajax({
        url: url,
        type: 'GET',
        beforeSend: function ()
        {
            $("body").append("<section class='loading'/>");
        },
        success: function (response) {
            $("section.loading").remove();
            $("#dados-bancario-convenio").html(response);
        },
        error: function (e) {
            console.log(e);
        }
    });
});




$('.todos-adicionar-cobranca-geracao').click(function (e) {
    AdicionarCobranca(this, false);
});

$('.algumas-adicionar-cobranca-geracao').click(function (e) {
    AdicionarCobranca(this, true);
});

$('.todos-remover-cobranca-geracao').click(function (e) {
    RemoverCobranca(this, true);
});

$('.algumas-remover-cobranca-geracao').click(function (e) {
    RemoverCobranca(this, false);
});

function AdicionarCobranca(object, checked) {
    var id = $(object).data('id');
    $(id).prop('checked', checked);
    $(object).addClass('oculto');
    $(object).prev().removeClass('oculto');
}

function RemoverCobranca(object, checked) {
    var id = $(object).data('id');
    $(id).prop('checked', checked);
    $(object).addClass('oculto');
    $(object).next().removeClass('oculto');
}

$('#formulario_geracao_lancamentos').submit(function (e) {
    e.preventDefault();

    var url = $(this).attr('action');
    var type = $(this).attr('method');

    var convenio = $('input[name="convenio"]:checked').val();
    if (convenio != null) {
        var tipoCobrancaGeracao = $('input[name="tipoCobrancaGeracao"]:checked').val();

        var status = true;
        var data = {};
        var cobrancas = [];
        if (tipoCobrancaGeracao === 'todos') {
            var cobrancasRemovidas = $('input[name="cobrancaRemovidaGeracao"]');
            $.each(cobrancasRemovidas, function (index, object) {
                var isChecked = $(object).prev().prop('checked');
                if (isChecked) {
                    cobrancas.push($(object).val());
                }
            });
        } else {
            var cobrancasAdicionadas = $('input[name="cobrancaAdicionadaGeracao"]');
            $.each(cobrancasAdicionadas, function (index, object) {
                var isChecked = $(object).prev().prop('checked');
                if (isChecked) {
                    cobrancas.push($(object).val());
                }
            });

            if (cobrancas.length === 0) {
                alert('Por favor, selecione pelo menos uma cobrança para a geração.');
                status = false;
            }
        }

        if (status) {
            data.tipo = tipoCobrancaGeracao;
            data.cobrancas = cobrancas;
            data.convenio = convenio;

            $.ajax({
                url: url,
                type: type,
                data: data,
                beforeSend: function ()
                {
                    $("body").append("<section class='loading'/>");
                },
                success: function (response, status, xhr) {
                    $("section.loading").remove();
                    DownloadFile(response, status, xhr);
                },
                error: function (e) {
                    console.log(e);
                    $("section.loading").remove();
                }
            });
        }
    } else {
        alert('O Convênio não foi selecionado! Por favor, selecione um Convênio.');
    }
});