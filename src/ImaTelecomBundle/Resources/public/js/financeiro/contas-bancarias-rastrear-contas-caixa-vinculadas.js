/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var atualizarContainer = 'contas-bancarias-rastreamento-contas';
var dialogContaCaixa = null;

function AtualizarContasBancariasRastreamentoContas() { // usado em contas-bancarias-conta.js
    let url = $('#contas_bancarias').data('url');
    let urlOpcoesEditar = $('#contas_bancarias').data('opcoes-editar');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (response, textStatus, jqXHR) {
            let tbody = '';
            
            $.each(response.contasCaixa, function (key, conta) {                
                let urlEdicao = urlOpcoesEditar.replace('_IdcontaCaixa', conta.IdcontaCaixa);
                let ativo = conta.Ativo ? 'checked="true"' : '';
                
                tbody += '<tr>';
                tbody += `<td class="centro">${conta.IdcontaCaixa}</td>`;
                tbody += `<td>${conta.Descricao}</td>`;
                tbody += `<td class="centro"><input type="checkbox" class="no-clicked" ${ativo}><label></label></td>`;
                tbody += `<td class="direita"><a href="#" data-url="${urlEdicao}" data-title="Cadastro da Conta Caixa" title="Cadastro da Conta Caixa" onClick='javascript:CarregarContaCaixa(this);' class="col-separator"><i class="icon fa fa-edit" aria-hidden="true"></i></a></td>`;
                tbody += '</tr>';
            });

            $('#contas_bancarias tbody').html(tbody);
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
        }
    });
}

function CarregarContaCaixa(objeto) {
    var title = $(objeto).data('title');
    var url = $(objeto).data('url');

    $.ajax({
        url: url,
        type: 'GET',
        success: function (response, textStatus, jqXHR) {
            dialogContaCaixa = bootbox.dialog({
                title: title,
                message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                size: 'large',
                onEscape: function () {
                }
            });

            dialogContaCaixa.init(function () {
                setTimeout(function () {
                    dialogContaCaixa.find('.bootbox-body').html(response);
                }, 3000);
            });

            $('.modal-dialog').css('width:1000px !important');
        },
        error: function (request, textStatus, errorThrown) {
            console.log(request, textStatus, errorThrown);
            mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
        }
    });
}