/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var cobrancasSelecionadas = {};
var cobrancasPagas = {};
var cobrancasNaoPagas = {};
var searchString = '';

function filter(string) {
    return string.toLowerCase().includes(searchString.toLowerCase());
}

$(document).ready(function (e) {
    $('body').confirmation({
        selector: '[data-toggle="confirmation"]'
    });

    $('body').on('confirm.bs.confirmation', '.confirmation-excluir', function () {
        var url = $(this).data('url');
        $.ajax({
            url: url,
            type: 'POST',
            success: function (response, textStatus, jqXHR) {
                if (response.status) {
                    var encode = Base64.encode(response.mensagem);
                    encode = encode.replaceAll('=', '');
                    window.location.href = response.redirect_url + '#' + encode;
                    location.reload();
                } else {
                    var erros = response.erros.join('<br/>');
                    mostrarNotificacao('Não foi possível excluir!', erros);
                }
            },
            error: function (request, textStatus, errorThrown) {
                console.log(request, textStatus, errorThrown);
                mostrarNotificacao('Não foi possível excluir!', errorThrown);
            }
        });
    });

    $('body').on('click', '.selecionar-cobranca', function () {
        let cobrancaId = $(this).data('id-cobranca');
        let cobrancaPaga = $(this).data('esta-pago');
        let isChecked = $(this).is(':checked');

        if (isChecked) {
            cobrancasSelecionadas[cobrancaId] = cobrancaId;
        } else {
            delete cobrancasSelecionadas[cobrancaId];
        }

        if (isChecked && cobrancaPaga) {
            cobrancasPagas[cobrancaId] = cobrancaId;
        } else if (isChecked && !cobrancaPaga) {
            cobrancasNaoPagas[cobrancaId] = cobrancaId;
        } else if (!isChecked && (cobrancaId in cobrancasPagas)) {
            delete cobrancasPagas[cobrancaId];
        } else if (!isChecked && (cobrancaId in cobrancasNaoPagas)) {
            delete cobrancasNaoPagas[cobrancaId];
        }

        if (Object.keys(cobrancasPagas).length > 0) {
            $('#estorno').removeClass('oculto');
        } else {
            $('#estorno').addClass('oculto');
        }

        if (Object.keys(cobrancasNaoPagas).length > 0) {
            $('#baixa').removeClass('oculto');
        } else {
            $('#baixa').addClass('oculto');
        }
    });

    $('body').on("click", ".table tbody tr", function () {
        let cobranca = $(this).find('.selecionar-cobranca');
        let cobrancaId = $(cobranca).data('id-cobranca');
        let cobrancaPaga = $(cobranca).data('esta-pago');
        let isChecked = !$(cobranca).is(':checked');
        $(cobranca).prop('checked', isChecked);

        if (isChecked) {
            cobrancasSelecionadas[cobrancaId] = cobrancaId;
        } else {
            delete cobrancasSelecionadas[cobrancaId];
        }

        if (isChecked && cobrancaPaga) {
            cobrancasPagas[cobrancaId] = cobrancaId;
        } else if (isChecked && !cobrancaPaga) {
            cobrancasNaoPagas[cobrancaId] = cobrancaId;
        } else if (!isChecked && (cobrancaId in cobrancasPagas)) {
            delete cobrancasPagas[cobrancaId];
        } else if (!isChecked && (cobrancaId in cobrancasNaoPagas)) {
            delete cobrancasNaoPagas[cobrancaId];
        }

        if (Object.keys(cobrancasPagas).length > 0) {
            $('#estorno').removeClass('oculto');
        } else {
            $('#estorno').addClass('oculto');
        }

        if (Object.keys(cobrancasNaoPagas).length > 0) {
            $('#baixa').removeClass('oculto');
        } else {
            $('#baixa').addClass('oculto');
        }
    });

    $('body').on('click', '.table tbody tr td:last-child', function (e) {
        e.stopPropagation();
    });

    $('body').on('click', '.historico', function (e) {
        e.preventDefault();

        let url = $(this).data('url');
        let titulo = $(this).data('title');        

        $.ajax({
            url: url,
            type: 'GET',
            success: function (response, textStatus, xhr) {
                var dialog = bootbox.dialog({
                    title: titulo,
                    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                    size: 'large'
                });

                dialog.init(function () {
                    setTimeout(function () {
                        dialog.find('.bootbox-body').html(response);
                    }, 3000);
                });

                $('.modal-dialog').css('width:1000px !important');
            }
        });
    });
});