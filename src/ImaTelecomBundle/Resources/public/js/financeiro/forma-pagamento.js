/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {
    $('body').on('click', '.excluir-forma-pagamento', function () {
        var url = $(this).data('url');
        $.ajax({
            url: url,
            type: 'POST',
            success: function (response, textStatus, jqXHR) {
                if (response.status) {
                    var encode = Base64.encode(response.mensagem);
                    encode = encode.replaceAll('=', '');
                    window.location.href = response.redirect_url + '#' + encode;
                } else {
                    var erros = response.erros.join('<br/>');
                    mostrarNotificacao('Não foi possível excluir!', erros);
                }
            },
            error: function (request, textStatus, errorThrown) {
                console.log(request, textStatus, errorThrown);
                mostrarNotificacao('Não foi possível excluir!', errorThrown);
            }
        });
    });
    
    $('form').submit(function (e) {
        e.preventDefault();

        var url = $(this).attr('action');
        var type = $(this).attr('method');
        var data = $(this).serializeArray();

        $.ajax({
            url: url,
            type: type,
            data: data,
            success: function (response, textStatus, jqXHR) {
                if (response.status) {
                    window.location.href = response.redirect_url + '#' + Base64.encode(response.mensagem).slice(0, -1);
                } else {
                    var erros = response.erros.join('<br/>');
                    mostrarNotificacao('Corrija os seguintes erros para poder prosseguir!', erros);
                }
            },
            error: function (request, textStatus, errorThrown) {
                mostrarNotificacao('Não foi possível excluir!', errorThrown);
            }
        });
    });
});