/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {
    $('table.table').dataTable({
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        }
    });
    
    $('body').on('click', '.ajax', function (e) {
        e.preventDefault();

        var title = $(this).data('title');
        var url = $(this).data('url');                

        $.ajax({
            url: url,
            type: 'GET',
            success: function (response, textStatus, jqXHR) {
                var dialog = bootbox.dialog({
                    title: title,
                    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                    size: 'large',
                    onEscape: function () {                        
                    }
                });

                dialog.init(function () {
                    setTimeout(function () {
                        dialog.find('.bootbox-body').html(response);
                    }, 3000);
                });

                $('.modal-dialog').css('width:1000px !important');
            },
            error: function (request, textStatus, errorThrown) {
                console.log(request, textStatus, errorThrown);
                mostrarNotificacao('Foi encontrado alguns erros!', errorThrown);
            }
        });
    });
});