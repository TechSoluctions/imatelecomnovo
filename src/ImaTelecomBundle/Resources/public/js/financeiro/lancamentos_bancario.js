/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$("#tabs").tabs();
var cobrancasTable = $('#cobrancas-lancamento').dataTable({
    lengthMenu: [[15, 50, -1], [15, 50, "Todos"]],
    order: [[0, "desc"]],
    language: {
        url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
    },
    responsive: true
});

new $.fn.dataTable.FixedHeader(cobrancasTable);


$("table tbody td a.visualizar-detalhes-cobranca").click(function (e) {
    e.preventDefault();

    $("section.main div.item-nota").remove();

    var $row = $(this).closest("tr"), // Finds the closest row <tr> 
            $tds = $row.find("td"),
            wrapper = $("section.main"),
            parentOffset = wrapper.offset(),
            relX = e.pageX - parentOffset.left + wrapper.scrollLeft(),
            relY = e.pageY - parentOffset.top + wrapper.scrollTop(),
            url = $(this).data('url');

    $.ajax({
        url: url,
        type: 'GET',
        beforeSend: function ()
        {
            $("body").append("<section class='loading'/>");
        },
        success: function (response) {
            var tableContent = $(response).addClass('item-nota').css(
                    {
                        left: relX - 90,
                        top: relY + 21
                    }
            );

            $("section.loading").remove();
            $("section.main").append(tableContent);
            $.each($tds, function () {
                $(this).addClass('selected');
            });
        },
        error: function (e) {
            console.log(e);
        }
    });

    return false;
});