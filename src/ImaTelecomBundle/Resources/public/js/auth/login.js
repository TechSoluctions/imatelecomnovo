/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {
    $("div.alert").on("click", "button.close", function () {
        $(this).parent().animate({opacity: 0}, 500).hide('slow');
    });


    window.setTimeout(function () {
        //$(".custom-alert").alert('close'); <--- Do not use this

        $("div.alert").slideUp(500, function () {
            $(this).remove();
        });
    }, 10000);
});