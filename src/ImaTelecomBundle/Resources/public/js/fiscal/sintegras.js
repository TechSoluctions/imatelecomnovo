$(document).ready(function (e) {
    var table = $('table.table').dataTable({
        lengthMenu: [[15, 50, -1], [15, 50, "Todos"]],
        order: [[1, "asc"]],
        columnDefs: [
            {
                targets: 4,
                orderable: false
            }
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        },
        responsive: true
    });

    new $.fn.dataTable.FixedHeader(table);



    $('.geracao-arquivo-sintegra').click(function (e) {
        e.preventDefault();

        var popup = new $.Popup($("section.main"));
        var url = $(this).data('url');
        var nota = '<br/><br/><span class="negrito">Nota:</span> <span class="italico">A geração deste arquivo irá se basear na Competência Atual, podendo ser a competência futura, caso a mesma esteja trabalhando.</span>';
        popup.Question('Atenção', 'Você realmente deseja gerar este arquivo? ' + nota, url);
        return false;
    });
    
    $('.regerar-arquivo-sintegra').click(function (e) {
        e.preventDefault();

        var popup = new $.Popup($("section.main"));
        var url = $(this).data('url');
        popup.QuestionRedirect('Atenção', 'Você realmente deseja regerar este arquivo?', url);
        return false;
    });
    
    $('.excluir-arquivo-sintegra').click(function (e) {
        e.preventDefault();

        var popup = new $.Popup($("section.main"));
        var url = $(this).data('url');
        popup.Question('Atenção', 'Você realmente deseja excluir este arquivo?', url);
        return false;
    });
});