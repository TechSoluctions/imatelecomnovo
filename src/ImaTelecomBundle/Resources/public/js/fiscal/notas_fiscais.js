/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $('.cancelar-nota-fiscal').click(function (e) {
        e.preventDefault();

        var url = $(this).attr('href');

        $.ajax({
            url: url,
            type: 'POST',
            success: function (response) {
                if (response.status == false) {
                    var erros = "";
                    $.each(response.erros, function (key, erro) {
                        //erros += "<br/>" + erro;
                        erros += erro;
                    });
                    
                    alert(erros);
                } else {
                    alert('Nota cancelada com sucesso');
                    window.location.reload();
                }
            }
        });
    });

    $('.regerar-nota-fiscal').click(function (e) {
        e.preventDefault();

        var url = $(this).attr('href');

        $.ajax({
            url: url,
            type: 'POST',
            success: function (response) {
                if (response.status == false) {
                    var erros = "";
                    $.each(response.erros, function (key, erro) {
                        erros += "<br/>" + erro;
                    });
                    
                    alert(erros);
                } else {
                    alert('Nota regerada com sucesso');
                    window.location.reload();
                }
            }
        });
    });    
});