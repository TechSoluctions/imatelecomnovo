/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function (e) {

    moment.locale('pt-br'); // default the locale to Portuguese Brazil
    $('#dataEmissao').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD/MM/YYYY'
        }
    });

    $('#selecionar_competencia').change(function () {
        var url = $(this).val();
        window.location.href = url;
    });

    $('#gerar_nota').click(function (e) {
        e.preventDefault();
      
        var url = $(this).attr('href');
        var dataEmissao = $('#dataEmissao').val();
        var tipoGeracao = $('#tipoGeracao').val();
        var data = {};
        data.dataEmissao = dataEmissao;
        data.tipoGeracao = tipoGeracao;

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            beforeSend: function ()
            {
                var exists = $("body section.loading").length;
                if (exists == 0) {
                    $("body").append("<section class='loading'/>");
                }
            },
            success: function (response) {
                $("section.loading").remove();
                if (response.status) {
                    $("#alerta_topo").removeClass('alert-danger');
                    $("#alerta_mensagem").text(response.mensagem);
                    $("#alerta_topo").addClass('alert-success');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-success', 5000);
                    window.location.reload();
                } else {
                    var erros = "";
                    $.each(response.erros, function (key, erro) {
                        erros += "<br/>" + erro;
                    });

                    $("#alerta_mensagem").html(erros);
                    $("#alerta_topo").addClass('alert-danger');
                    $("#alerta_topo").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                    MostrarAlertaTopo('alert-danger', 15000);
                }
            },
            error: function (request, textStatus, errorThrown) {
                $('#gerar_nota').click();
            }
        });
    });

    var table = $('table.table').dataTable({
        lengthMenu: [[15, 50, -1], [15, 50, "Todos"]],
        order: [[1, "asc"]],
        columnDefs: [
            {
                targets: 6,
                orderable: false
            },
            {
                targets: 1,
                render: $.fn.dataTable.render.ellipsis(35)
            }
        ],
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json"
        },
        responsive: true
    });

    new $.fn.dataTable.FixedHeader(table);

    $('.visualizar-item').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        $.ajax({
            url: url,
            type: 'GET',
            success: function (response) {
                console.log(response);
            },
            error: function (e) {
                console.log(e);
            }
        });

        return false;
    });

    var popup = new $.Popup($("section.main"));

    $('.open-popup').click(function (e) {
        e.preventDefault();

        var url = $(this).data('url');
        popup.Mostrar(url);

        return false;
    });

    $('.geracao-notas').click(function (e) {
        e.preventDefault();

        popup.setTitle('Geração de Notas Fiscais');

        return false;
    });





    $("table tbody td a.visualizar-item-nota").click(function (e) {
        e.preventDefault();

        $("section.main div.item-nota").remove();

        var $row = $(this).closest("tr"), // Finds the closest row <tr> 
                $tds = $row.find("td"),
                wrapper = $("section.main"),
                parentOffset = wrapper.offset(),
                relX = e.pageX - parentOffset.left + wrapper.scrollLeft(),
                relY = e.pageY - parentOffset.top + wrapper.scrollTop(),
                url = $(this).data('url');

        $.ajax({
            url: url,
            type: 'GET',
            beforeSend: function ()
            {
                $("body").append("<section class='loading'/>");
            },
            success: function (response) {
                var tableContent = $(response).addClass('item-nota').css(
                        {
                            left: relX - 90,
                            top: relY + 21
                        }
                );

                $("section.loading").remove();
                $("section.main").append(tableContent);
                $.each($tds, function () {
                    $(this).addClass('selected');
                });
            },
            error: function (e) {
                console.log(e);
            }
        });

        return false;
    });
});