/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


new Clipboard('.copy-text');

$(".no-clicked").click(function (e) {
    e.preventDefault();
    return false;
});

$("#arquivosExecucao").fileinput({
    maxFileCount: 20,
    allowedFileExtensions: ["php"]
});

$(".arquivo-log-execucao-excluir").click(function (e) {
    e.preventDefault();

    var popup = new $.Popup($("section.main"));
    var url = $(this).data('url');
    popup.Question('Atenção', 'Você realmente deseja excluir este arquivo de log?', url);    

    return false;
});

$(".arquivo-execucao-excluir").click(function (e) {
    e.preventDefault();

    var popup = new $.Popup($("section.main"));
    var url = $(this).data('url');
    popup.Question('Atenção', 'Você realmente deseja excluir este arquivo?', url);

    return false;
});

$(".formulario-edicao-agendamento").submit(function (e) {
    e.preventDefault();

    var url = $(this).attr('action'),
            type = $(this).attr('method'),
            data = $(this).serialize();

    var formData = new FormData($(this)[0]);

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        type: type,
        url: url,
        data: formData,
        beforeSend: function ()
        {
            $(".container-edicao-agendamento").append("<section class='loading'/>");
        },
        success: function (response, textStatus, jqXHR) {
            console.log(response);
            $("section.loading").remove();

            if (response.status == true) {
                $("#alerta_topo_interno").removeClass('alert-danger');
                $("#alerta_mensagem_interno").text(response.mensagem);
                $("#alerta_topo_interno").addClass('alert-success');
                $("#alerta_topo_interno").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                MostrarAlertaTopo('alert-success', 15000);
            } else {
                var erros = "";
                $.each(response.erros, function (key, erro) {
                    erros += "<br/>" + erro;
                });

                $("#alerta_mensagem_interno").html(erros);
                $("#alerta_topo_interno").addClass('alert-danger');
                $("#alerta_topo_interno").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

                MostrarAlertaTopo('alert-danger', 15000);
            }
        },
        error: function (response, textStatus, jqXHR) {
            console.log("AKI");
            console.log(response, textStatus, jqXHR);

            $("section.loading").remove();
            $("#alerta_mensagem_interno").text(jqXHR);
            $("#alerta_topo_interno").addClass('alert-danger');
            $("#alerta_topo_interno").removeClass('oculto').css({display: 'block', opacity: 1}).slideDown(500);

            MostrarAlertaTopo('alert-success', 15000);
        }
    });

    return false;
});