<?php

namespace ImaTelecomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Propel\Runtime\Propel;
use Propel\Runtime\Exception\PropelException;
use ImaTelecomBundle\Model\CronTask;
use ImaTelecomBundle\Model\CronTaskQuery;
use ImaTelecomBundle\Model\CronTaskGrupo;
use ImaTelecomBundle\Model\CronTaskGrupoQuery;
use ImaTelecomBundle\Model\CronTaskArquivos;
use ImaTelecomBundle\Model\CronTaskArquivosQuery;
use ImaTelecomBundle\Model\CronTaskLogExecucao;
use ImaTelecomBundle\Model\CronTaskLogExecucaoQuery;
use ImaTelecomBundle\Model\DadosFiscaisGeracao;
use ImaTelecomBundle\Model\DadosFiscaisGeracaoQuery;

class RotinaController extends AdminController {

    public function indexAction(Request $request) {
        return $this->render('ImaTelecomBundle:Admin:index.html.twig');
    }

    public function gruposAgendamentoAction(Request $request) {
        $grupos = CronTaskGrupoQuery::create()->find()->toArray();

        $retornoView = array();
        $retornoView['grupos'] = $grupos;
        return $this->render('ImaTelecomBundle:Rotina:grupos_agendamento.html.twig', $retornoView);
    }

    public function gruposAgendamentoGrupoAction(Request $request, $grupoId) {
        $grupo = CronTaskGrupoQuery::create()->findPk($grupoId);
        if ($grupoId == null) {
            $grupo = new CronTaskGrupo();
        }

        $retornoView = array();
        $retornoView['grupo'] = $grupo;
        return $this->render('ImaTelecomBundle:Rotina:grupos_agendamento_grupo.html.twig', $retornoView);
    }

    public function gruposAgendamentoGrupoSalvarAction(Request $request, $grupoId) {
        $user = $this->getUser();
        $status = true;
        $erros = array();

        $nome = $request->get('nome', '');
        $descricao = $request->get('descricao', '');

        $grupo = CronTaskGrupoQuery::create()->findPk($grupoId);
        if ($grupo == null) {
            $grupo = new CronTaskGrupo();
            $grupo->setDataCadastro(new \DateTime('now'));
        }

        $grupo->setNome($nome);
        $grupo->setDescricao($descricao);
        $grupo->setDataAlterado(new \DateTime('now'));
        $grupo->setUsuarioAlterado($user->getIdusuario());

        try {
            $grupo->save();
        } catch (PropelException $ex) {
            $erros = $ex->getMessage();
            $status = false;
        }

        $retornoView = array();
        $retornoView['status'] = $status;
        $retornoView['erros'] = $erros;
        $retornoView['mensagem'] = "Grupo salvo com sucesso.";
        return new JsonResponse($retornoView);
    }

    private function getAgendamentos() {
        $sql = "select c.*, l.log_em_andamento, g.nome as grupo_nome
from cron_task_grupo g, cron_task c left join cron_task_log_execucao l on c.id = l.cron_task_id and l.log_em_andamento = 1
where g.id = c.cron_task_grupo_id
group by c.id";

        $con = Propel::getConnection();
        $stmt = $con->prepare($sql);
        $stmt->execute();
        $agendamentos = $stmt->fetchall();

        return $agendamentos;
    }

    public function agendamentosAction(Request $request) {
        $agendamentos = $this->getAgendamentos();

        $retornoView = array();
        $retornoView['agendamentos'] = $agendamentos;
        return $this->render('ImaTelecomBundle:Rotina:agendamentos.html.twig', $retornoView);
    }

    public function agendamentosAgendamentoAction(Request $request, $agendamentoId) {
        if ($agendamentoId == 0) {
            $agendamento = new CronTask();
        } else {
            $agendamento = CronTaskQuery::create()->findPk($agendamentoId);
        }

        $retornoView = array();
        $retornoView['agendamento'] = $agendamento;
        return $this->render('ImaTelecomBundle:Rotina:agendamentos_agendamento.html.twig', $retornoView);
    }

    public function agendamentosAgendamentoSalvarAction(Request $request, $agendamentoId) {
        $user = $this->getUser();
        $status = true;
        $erros = array();

        $nome = $request->get('nome', '');
        $grupo = $request->get('grupo', '');
        $tipoAgendamento = $request->get('tipoAgendamento', '');
        $ativo = $request->get('ativo', '');
        $prioridade = $request->get('prioridade', '');
        $tipoExecucao = $request->get('tipoExecucao', '');
        $descricao = $request->get('descricao', '');
        $arquivosExecucao = $request->files->get('arquivosExecucao');

        $agendamento = CronTaskQuery::create()->findPk($agendamentoId);
        if ($agendamento == null) {
            $agendamento = new CronTask();
            $agendamento->setDataCadastro(new \DateTime('now'));
        }

        $agendamento->setNome($nome);
        $agendamento->setCronTaskGrupoId($grupo);
        $agendamento->setTipo($tipoAgendamento);
        $agendamento->setAtivo($ativo);
        $agendamento->setPrioridade($prioridade);
        $agendamento->setTipoExecucaoArquivo($tipoExecucao);
        $agendamento->setDescricao($descricao);
        $agendamento->setDataAlterado(new \DateTime('now'));
        $agendamento->setUsuarioAlterado($user->getIdusuario());

        try {
            $agendamento->save();

            $diretorioScript = $this->getParameter('repositorio_scripts');
            if (!file_exists($diretorioScript)) {
                mkdir($diretorioScript, 0777, true);
            }

            foreach ($arquivosExecucao as $uploadedFile) {
                if ($uploadedFile != null) {
                    $nomeArquivo = $uploadedFile->getClientOriginalName();
                    $file = $uploadedFile->move($diretorioScript, $nomeArquivo);

                    if ($file !== null) {
                        $cronTaskArquivo = new CronTaskArquivos();
                        $cronTaskArquivo->setDiretorioFilho($diretorioScript);
                        $cronTaskArquivo->setArquivo($nomeArquivo);
                        $cronTaskArquivo->setCronTaskId($agendamento->getId());
                        $cronTaskArquivo->setDataCadastro(new \DateTime('now'));
                        $cronTaskArquivo->setDataAlterado(new \DateTime('now'));
                        $cronTaskArquivo->setConteudoScript("");
                        $cronTaskArquivo->setTipoArquivo("unico");
                        $cronTaskArquivo->save();
                    }
                }
            }
        } catch (PropelException $ex) {
            $erros = $ex->getMessage();
            $status = false;
        }

        $retornoView = array();
        $retornoView['status'] = $status;
        $retornoView['erros'] = $erros;
        $retornoView['mensagem'] = "Agendamento salvo com sucesso.";
        return new JsonResponse($retornoView);
    }

    public function agendamentosAgendamentoExecutarAction(Request $request, $agendamentoId) {
        $retornoView = array();
        $retornoView['status'] = true;

        $agendamento = CronTaskQuery::create()->findPk($agendamentoId);
        if ($agendamento != null) {
            if ($agendamento->getAtivo()) {
                $agendamentoArquivos = CronTaskArquivosQuery::create()->filterByExecutar(TRUE);
                if ($agendamento->getTipoExecucaoArquivo() != 'unica') {
                    $agendamentoArquivos->orderByOrdemExecucao();
                }

                $arquivos = $agendamentoArquivos->findByCronTaskId($agendamentoId);
                $retornoView['teste'] = $arquivos->toArray();
                foreach ($arquivos as $arquivo) {
                    $nomeArquivo = $arquivo->getArquivo();
                    $caminhoArquivo = $arquivo->getDiretorioFilho();
                    $comando = "php $caminhoArquivo/$nomeArquivo";

                    $logExecucao = $this->criarArquivoLogExecucaoAgendamento($agendamentoId, $caminhoArquivo, $comando);
                    $multithread_exec = " > /dev/null 2>&1 &";

                    exec($logExecucao . $multithread_exec);
                    //shell_exec($logExecucao . $multithread_exec);
                }

                if ($arquivos->count() == 0) {
                    $retornoView['status'] = false;
                }
            } else {
                $retornoView['status'] = false;
            }
        }

        if ($retornoView['status']) {
            $retornoView['mensagem'] = 'O Agendamento: <span class="negrito">' . $agendamento->getNome() . '</span> foi executado com sucesso.';
        } else {
            $retornoView['mensagem'] = 'O Agendamento: <span class="negrito">' . $agendamento->getNome() . '</span> ';

            if ($agendamento->getAtivo() == 0) {
                $retornoView['mensagem'] .= 'não está ativo para a execução.';
            } else {
                $retornoView['mensagem'] .= 'não possui arquivos para execução.';
            }
        }

        $agendamentos = $this->getAgendamentos();
        $retornoView['agendamentos'] = $agendamentos;

        return $this->redirect('ImaTelecomBundle:Rotina:agendamentos.html.twig', $retornoView);
    }

    private function criarArquivoLogExecucaoAgendamento($agendamentoId, $caminhoArquivo, $comandoExecucao) {
        $diretorioLogs = $this->getParameter('repositorio_logs');
        if (!is_dir($diretorioLogs)) {
            mkdir($diretorioLogs);
        }

        $today = date("Y-m-d__H_i_s");
        $logNome = "log_importacao_$today.txt";
        $filename = "$diretorioLogs/$logNome";
        file_put_contents($filename, "");

        $cronTaskLogExecucao = new CronTaskLogExecucao();
        $cronTaskLogExecucao->setArquivo($logNome);
        $cronTaskLogExecucao->setDiretorioFilho($caminhoArquivo);
        $cronTaskLogExecucao->setCronTaskId($agendamentoId);
        $cronTaskLogExecucao->setLogEmAndamento(1);
        $cronTaskLogExecucao->setComandoExecutadoShell("$comandoExecucao $filename");
        $cronTaskLogExecucao->setDataCadastro(new \DateTime('now'));
        $cronTaskLogExecucao->setDataAlterado(new \DateTime('now'));

        if ($cronTaskLogExecucao->save()) {
            $cronTaskLogExecucao->setComandoExecutadoShell("$comandoExecucao $filename " . $cronTaskLogExecucao->getId());
            $cronTaskLogExecucao->save();
        }

        // atualizar setComandoExecutadoShell adicionando o id do log gerado e retornar
        // o nome do arquiivo e o id do log criado e passsar por execução o id do Log
        // para atualizar o log final
        return $cronTaskLogExecucao->getComandoExecutadoShell();
    }

    public function agendamentoArquivoExecucaoExcluirAction(Request $request, $agendamentoId, $arquivoId) {
        $status = false;
        $erros = array();

        $arquivo = CronTaskArquivosQuery::create()->findPk($arquivoId);
        if ($arquivo->getCronTaskId() == $agendamentoId) {
            $nomeArquivo = $arquivo->getArquivo();
            $caminhoArquivo = $arquivo->getDiretorioFilho();

            try {
                $filename = "$caminhoArquivo/$nomeArquivo";
                if (file_exists($filename)) {
                    unlink($filename);
                }
            } catch (Exception $ex) {
                $erros = $ex->getMessage();
            }

            try {
                $arquivo->delete();
                $status = true;
            } catch (PropelException $ex) {
                $erros = $ex->getMessage();
            }
        } else {
            $erros[] = "Não foi possível localizar este registro.";
        }

        $retornoView = array();
        $retornoView['status'] = $status;
        $retornoView['erros'] = $erros;
        $retornoView['mensagem'] = "Arquivo de Execução foi excluído com sucesso.";
        return new JsonResponse($retornoView);
    }

    public function agendamentoArquivoLogExecucaoExcluirAction(Request $request, $agendamentoId, $arquivoId) {
        $status = false;
        $erros = array();

        $arquivo = CronTaskLogExecucaoQuery::create()->findPk($arquivoId);
        if ($arquivo->getCronTaskId() == $agendamentoId) {
            $nomeArquivo = $arquivo->getArquivo();
            $caminhoArquivo = $arquivo->getDiretorioFilho();

            if ($arquivo->getLogEmAndamento() == 0) {
                try {
                    $filename = "$caminhoArquivo/$nomeArquivo";
                    if (file_exists($filename)) {
                        unlink($filename);
                    }
                } catch (Exception $ex) {
                    $erros = $ex->getMessage();
                }

                try {
                    $arquivo->delete();
                    $status = true;
                } catch (PropelException $ex) {
                    $erros = $ex->getMessage();
                }
            } else {
                $erros[] = "O arquivo de log ainda se encontra em execução, por favor espere a conclusão.";
            }
        } else {
            $erros[] = "Não foi possível localizar este registro.";
        }

        $retornoView = array();
        $retornoView['status'] = $status;
        $retornoView['erros'] = $erros;
        $retornoView['mensagem'] = "Arquivo de Log foi excluído com sucesso.";
        return new JsonResponse($retornoView);
    }

    public function verificarNotasEmAbertoAction(Request $request) {
        $user = $this->getUser();
        $retornoView = array();
        
        $competencia = $this->getCompetenciaAtual();
        $competenciaId = $competencia->getId();
        
        $sql = "select bi.idboleto_item FROM boleto b, boleto_item bi
where competencia_id = $competenciaId and b.idboleto = bi.boleto_id";

        $con = Propel::getConnection();
        $stmt = $con->prepare($sql);
        $stmt->execute();
        $faturas = $stmt->fetchall();
        
        if (sizeof($faturas)) {
            foreach ($faturas as $fatura) {
                $dadoGeracao = DadosFiscaisGeracaoQuery::create()->findOneByBoletoItemId($fatura['idboleto_item']);

                if ($dadoGeracao == null) {
                    $dadoGeracao = new DadosFiscaisGeracao();
                    $dadoGeracao->setBoletoItemId($fatura['idboleto_item']);
                    $dadoGeracao->setStatus('a gerar');
                    $dadoGeracao->setCompetenciaId($competenciaId);
                    $dadoGeracao->setDataCadastro(new \DateTime('now'));
                    $dadoGeracao->setDataAlterado(new \DateTime('now'));
                    $dadoGeracao->setUsuarioAlterado($user->getIdusuario());

                    $dadoGeracao->save();
                }
            }

            $retornoView['status'] = True;
            $retornoView['mensagem'] = "Faturas geradas com sucesso.";
        } else {
            $retornoView['status'] = False;
            $retornoView['mensagem'] = "Nenhuma Fatura localizada para a competência atual.";
        }
        
        return new JsonResponse($retornoView);
    }
}
