<?php

namespace ImaTelecomBundle\Controller;

use ImaTelecomBundle\Interfaces\CompetenciaAtualInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use ImaTelecomBundle\Model\Competencia;
use ImaTelecomBundle\Model\CompetenciaQuery;
use ImaTelecomBundle\Lib\ExtendedFPDF;
use \DateTime;
use Propel\Runtime\Exception\PropelException;
use ImaTelecomBundle\Model\DadosFiscal;
use ImaTelecomBundle\Model\DadosFiscalQuery;
use ImaTelecomBundle\Model\DadosFiscalItem;
use ImaTelecomBundle\Model\ServicoClienteQuery;
use ImaTelecomBundle\Model\Boleto;
use ImaTelecomBundle\Model\BoletoQuery;
use ImaTelecomBundle\Model\BoletoItem;
use ImaTelecomBundle\Model\EnderecoCliente;
use Propel\Runtime\Propel;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use Propel\Runtime\ActiveQuery\Criteria;
use ImaTelecomBundle\Model\Map\DadosFiscalTableMap;
use ImaTelecomBundle\Model\ClienteQuery;
use ImaTelecomBundle\Model\LancamentosBoletosQuery;
use ImaTelecomBundle\Model\EmpresaQuery;
use ImaTelecomBundle\Lib\Utils;
use ImaTelecomBundle\Model\Map\EmpresaTableMap;
use Symfony\Component\Debug\Exception\FatalErrorException;
use ImaTelecomBundle\Lib\NFSE\NFSE;
use ImaTelecomBundle\Model\BoletoBaixaHistorico;

class AdminController extends Controller implements CompetenciaAtualInterface {

    public function indexAction(Request $request) {
        $competencia = $this->getCompetenciaAtual();
        /*
          $totalClientesNovos = ClienteQuery::create()->where('year(data_contrato) = year(now()) and month(data_contrato) = month(now())')->count();
          $valorTotalServicosNovos = ServicoClienteQuery::create()
          ->select(array('ValorTotal'))->withColumn('round(sum(valor - valor_desconto), 2)', 'ValorTotal')
          ->where('year(data_contratado) = year(now()) and month(data_contratado) = month(now())')
          ->findOne();
         */
        $valorTotalNotasGeradasCompetencia = DadosFiscalQuery::create()
                        ->select(array('ValorTotal'))->withColumn('round(sum(valor_nota), 2)', 'ValorTotal')
                        ->filterByCompetencia($competencia)->findOne();

        $notasRecentes = DadosFiscalQuery::create()->select(array('Id', 'Nome', 'Situacao', 'DataEmissao', 'ValorNota', 'Cliente', 'TipoNota', 'BoletoId'))
                        ->useBoletoQuery()->useClienteQuery()->withColumn('idcliente', 'Cliente')
                        ->usePessoaQuery()->withColumn('nome', 'Nome')->endUse()->endUse()->endUse()
                        ->filterByCompetencia($competencia)
                        ->orderByDataEmissao(Criteria::DESC)->limit(10)->find();


        $valorTotalLancamentosCompetencia = LancamentosBoletosQuery::create()
                ->select(array('ValorTotal'))
                ->useBoletoQuery()->withColumn('round(sum(valor), 2)', 'ValorTotal')
                ->useClienteQuery()->usePessoaQuery()->endUse()->endUse()->endUse()
                ->filterByCompetencia($competencia)
                ->findOne();

        $lancamentosRecentes = LancamentosBoletosQuery::create()
                ->select(array('Nome', 'ClienteId', 'Vencimento', 'ValorBoleto', 'CobrancaId', 'EstaPago'))
                ->useLancamentosQuery()->withColumn('lancamentos.data_geracao', 'DataGeracao')->endUse()
                ->useBoletoQuery()->withColumn('idboleto', 'CobrancaId')->withColumn('vencimento', 'Vencimento')
                ->withColumn('valor', 'ValorBoleto')->withColumn('esta_pago', 'EstaPago')
                ->useClienteQuery()->withColumn('idcliente', 'ClienteId')
                ->usePessoaQuery()->withColumn('nome', 'Nome')->endUse()
                ->endUse()->endUse()
                ->filterByCompetencia($competencia)
                ->orderBy('Vencimento', Criteria::DESC)->limit(8)
                ->find();


        $faturamentoAnualCompetencia = BoletoQuery::create()
                        ->select(array('MesVencimento', 'AnoVencimento', 'ValorTotal'))
                        ->withColumn('year(vencimento)', 'AnoVencimento')->withColumn('month(vencimento)', 'MesVencimento')
                        ->withColumn('round(sum(valor), 2)', 'ValorTotal')
                        ->where("year(vencimento) = {$competencia->getAno()}")
                        ->groupBy('MesVencimento')->orderBy('MesVencimento')->find();

        $retornoView = array();
        //$retornoView['totalClientesNovos'] = $totalClientesNovos;
        //$retornoView['valorTotalServicosNovos'] = $valorTotalServicosNovos;
        $retornoView['notasRecentes'] = $notasRecentes;
        $retornoView['valorTotalNotasGeradasCompetencia'] = $valorTotalNotasGeradasCompetencia;
        $retornoView['lancamentosRecentes'] = $lancamentosRecentes;
        $retornoView['valorTotalLancamentosCompetencia'] = $valorTotalLancamentosCompetencia;
        $retornoView['faturamentoAnualCompetencia'] = $faturamentoAnualCompetencia->toJSON();

        return $this->render('ImaTelecomBundle:Admin:index.html.twig', $retornoView);
    }

    public function getCompetenciaAtual() {
        $competencia = CompetenciaQuery::create()->findOneByTrabalharPeriodoFuturo(1);
        if ($competencia == null) {
            $competencia = CompetenciaQuery::create()->findOneByAtivo(1);
        } else {
            $competencia = CompetenciaQuery::create()->findPk($competencia->getProximaCompetencia());
        }

        return $competencia;
    }

    public function criarNovaCompetencia($ano, $mes) {
        $mes++;
        if ($mes > 12) {
            $mes = 1;
            $ano++;
        }

        $meses = array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");

        $descricao = "Período referênte à " . $meses[$mes - 1];
        $mes = str_pad($mes, 2, "0", STR_PAD_LEFT);

        $periodoInicial = "$ano-$mes-01";
        $periodoFinal = date("Y-m-t", strtotime($periodoInicial));

        $novaCompetencia = new Competencia();
        $novaCompetencia->setAtivo(0);
        $novaCompetencia->setPeriodoFuturo(1);
        $novaCompetencia->setEncerramento(0);
        $novaCompetencia->setAno($ano);
        $novaCompetencia->setMes($mes);
        $novaCompetencia->setDescricao($descricao);
        $novaCompetencia->setPeriodoInicial($periodoInicial);
        $novaCompetencia->setPeriodoFinal($periodoFinal);
        $novaCompetencia->setProximaCompetencia(0);
        $novaCompetencia->setNumeroNf(0);
        $novaCompetencia->setTrabalharPeriodoFuturo(0);
        $novaCompetencia->setUltimaDataEmissao($periodoInicial);

        return $novaCompetencia;
    }

    public function geracaoArquivoNotaFiscalPdf($nota) {
        $cliente = $nota->cliente();
        $pessoa = $cliente->getPessoa();
        $enderecoCliente = $cliente->getEnderecoCliente();
        $tipoPessoa = $pessoa->getTipoPessoa();
        $telefone = $pessoa->getTelefone();
        $situacao = $nota->getSituacao() == 'normal' ? 'Normal' : 'Cancelada';
        $inscricaoEstadual = $pessoa->getRgInscrest();

        $pdf = new ExtendedFPDF('P', 'cm', 'A4');
        $pdf->SetTopMargin(0);
        $pdf->SetLeftMargin(0);
        $pdf->SetRightMargin(0);
        $pdf->SetAutoPageBreak(true, 0, 0);
        $pdf->AddPage();

        $pdf->Image('repositorio/notas/layout/v04.png', 0, 0, 21, 29);
        //$pdf->Image('repositorio/notas/layout/marca_dagua.png', 0, 0, 21, 27.16);

        $pdf->SetFont('Arial', '', 8);
        $pdf->setXY(13.30, 3.67);
        $pdf->drawTextBox(str_pad($nota->getNumeroNotaFiscal(), 9, "0", STR_PAD_LEFT), 15, 5, 'L', 'T', 0);

        $dataEmissao = $nota->getDataEmissao()->format("d/m/Y"); //date_format(date_create($nota->getDataEmissao()), "d/m/Y");
        $pdf->setXY(16.5, 3.67);
        $pdf->drawTextBox($dataEmissao, 15, 5, 'L', 'T', 0);

        $pdf->setXY(15.29, 4.55);
        if ($tipoPessoa == 'juridica') {
            $pdf->drawTextBox($inscricaoEstadual, 15, 5, 'L', 'T', 0);
        } else {
            $pdf->drawTextBox('XXXX', 15, 5, 'L', 'T', 0);
        }

        $pdf->SetFont('Arial', 'B', 8);
        $pdf->setXY(1.55, 5.35);
        if ($tipoPessoa == 'fisica')
            $pdf->drawTextBox(utf8_decode($pessoa->getNome()), 10, 5, 'L', 'T', 0);
        else
            $pdf->drawTextBox(utf8_decode($pessoa->getRazaoSocial()), 10, 5, 'L', 'T', 0);

        if ($enderecoCliente != null) {
            $cidade = $enderecoCliente->getCidade();
            $endereco1 = $this->remove_accents($enderecoCliente->getRua() . ', ' . $enderecoCliente->getNum());
            $endereco2 = $this->remove_accents($enderecoCliente->getBairro() . ', ' . $cidade->getMunicipio() . '/' . $cidade->getUf() . ', CEP ' . $enderecoCliente->getCep());
        } else {
            $cidade = $pessoa->cidade();
            $endereco1 = $this->remove_accents($pessoa->getRua() . ', ' . $pessoa->getNum());
            $endereco2 = $this->remove_accents($pessoa->getBairro() . ', ' . $cidade->getMunicipio() . '/' . $cidade->getUf() . ', CEP ' . $pessoa->getCep());
        }

        $pdf->SetFont('Arial', '', 8);
        $pdf->setXY(16, 5.45);
        $pdf->drawTextBox($pessoa->getCpfCnpj(), 15, 5, 'L', 'T', 0);

        $pdf->setXY(1.53, 6);
        $pdf->drawTextBox($endereco1, 15, 5, 'L', 'T', 0);

        $pdf->setXY(16, 5.85);
        $pdf->drawTextBox($nota->getServicoClienteImportId(), 15, 5, 'L', 'T', 0);

        $pdf->setXY(1.53, 6.3);
        $pdf->drawTextBox($endereco2, 15, 5, 'L', 'T', 0);

        $pdf->setXY(16, 6.26);
        if ($tipoPessoa == 'fisica') {
            $pdf->drawTextBox('ISENTO', 15, 5, 'L', 'T', 0);
        } else {
            $rg = $pessoa->getRgInscrest();
            if (!empty($rg))
                $pdf->drawTextBox($rg, 15, 5, 'L', 'T', 0);
            else
                $pdf->drawTextBox('ISENTO', 15, 5, 'L', 'T', 0);
        }

        $pdf->setXY(2.40, 6.62);
        $pdf->drawTextBox($pessoa->getEmail(), 15, 5, 'L', 'T', 0);

        $pdf->setXY(16, 6.66);
        $pdf->drawTextBox($telefone->getDdd() . $telefone->getNumero(), 15, 5, 'L', 'T', 0);

        $mensagemCorpo = utf8_decode("Serviço de Internet - Período referênte ");

        $codigoContrato = "";
        $itensNota = $nota->getDadosFiscalItems();
        if ($itensNota->count() > 0) {
            $textoDescricao = "";
            $posY = 1.75;

            foreach ($itensNota as $itemNota) {
                $servicoCliente = $itemNota->getBoletoItem()->getServicoCliente();
                $contrato = $servicoCliente->getContrato();
                $servicoPrestado = $servicoCliente->getServicoPrestado();
                $plano = $servicoPrestado->getPlano();
                //$servicoPrestado = $plano->getServicoPrestado();                
                $codigoServico = str_pad($servicoCliente->getIdservicoCliente(), 12, '0', STR_PAD_LEFT);
                $nomeServico = utf8_decode($plano->getNome());
                //$nomeServico = utf8_decode($servicoPrestado->getNome());
                $codigoContrato = $servicoCliente->getCodCesta();
                $descricaoNota = utf8_decode($servicoCliente->getDescricaoNotaFiscal()) . "\n";

                // pega somente os valores já gerados para o item da nota
                $valorSva = $itemNota->getValorSva();
                $valorScm = $itemNota->getValorScm();
                $temScm = false;
                $temSva = false;

                if ($contrato != null) {
                    /* 					
                      $valorSva = $contrato->getSva() == null ? 0.00 : $contrato->getSva();
                      $valorScm = $contrato->getScm() == null ? 0.00 : $contrato->getScm();
                     */

                    $temScm = $contrato->getTemScm();
                    $temSva = $contrato->getTemSva();
                    $descricaoScm = $contrato->getDescricaoScm();
                    $descricaoSva = $contrato->getDescricaoSva();
                }

                $valorSva = number_format($valorSva, 2, ',', '');
                $valorScm = number_format($valorScm, 2, ',', '');
                $valorItem = number_format($itemNota->getValorItem(), 2, ',', '');

                $tipoPeriodoReferencia = $servicoCliente->getTipoPeriodoReferencia();

                if ($tipoPeriodoReferencia == 'corrente') {
                    $emissao = $nota->getDataEmissao();
                } elseif ($tipoPeriodoReferencia == 'posterior') {
                    $emissao = $nota->getDataEmissao()->modify('+1 month');
                } else {
                    $emissao = $nota->getDataEmissao()->modify('-1 month');
                }

                $dataInicial = $emissao->format('01/m/Y');
                $dataFinal = $emissao->format('t/m/Y');
                $mensagemCorpo2 = utf8_decode("$dataInicial à $dataFinal");
                /*
                  $pdf->setXY(1.55, 5.86 + $posY);
                  $pdf->drawTextBox("$codigoServico - $mensagemCorpo $mensagemCorpo2", 15, 10, 'L', 'T', 0);
                 */

                //$descricaoNota = '';
                if ($temScm) {
                    $descricaoNota .= trim(utf8_decode($descricaoScm));
                }

                if ($temSva) {
                    $descricaoNota .= trim(utf8_decode($descricaoSva));
                }


                if (empty($descricaoNota)) {
                    //$descricaoNota = "$codigoServico - $nomeServico $mensagemCorpo $mensagemCorpo2 \n\n$descricaoNota";
                }

                //$descricaoNota = "$codigoServico - $nomeServico $mensagemCorpo $mensagemCorpo2 \n\n$descricaoNota";
                if ($codigoContrato == 0) {
                    $descricaoNota = "$codigoServico - $nomeServico $mensagemCorpo $mensagemCorpo2 \n\n$descricaoNota";
                } else {
                    $descricaoNota = "$codigoServico - $nomeServico $mensagemCorpo $mensagemCorpo2 - Contrato: $codigoContrato \n\n$descricaoNota";
                }

                $pdf->setXY(1.55, 6 + $posY);
                $pdf->drawTextBox($descricaoNota, 15, 10, 'L', 'T', 0);

                $pdf->setXY(0, 14 + $posY);
                $pdf->drawTextBox($codigoServico, 3.75, 5, 'R', 'T', 0);

                $pdf->setXY(4, 14 + $posY);
                $pdf->drawTextBox($nomeServico, 15, 5, 'L', 'T', 0);
                //$pdf->drawTextBox(utf8_decode($servicoCliente->getImportId() . "  -  " . $servicoPrestado->getNome()), 15, 5, 'L', 'T', 0);                

                $pdf->setXY(10, 14 + $posY);
                $pdf->drawTextBox("R$ 0,00", 1.85, 5, 'R', 'T', 0);

                $pdf->setXY(11.80, 14 + $posY);
                $pdf->drawTextBox("0%", 1.85, 5, 'R', 'T', 0);

                $pdf->setXY(13.70, 14 + $posY);
                $pdf->drawTextBox("R$ $valorSva", 1.85, 5, 'R', 'T', 0);

                $pdf->setXY(15.60, 14 + $posY);
                $pdf->drawTextBox("R$ $valorScm", 1.85, 5, 'R', 'T', 0);

                $pdf->setXY(17.80, 14 + $posY);
                $pdf->drawTextBox("R$ $valorItem", 15, 5, 'L', 'T', 0);

                $posY += 0.33;
            }

            $pdf->SetFont('Arial', '', 8);
            $pdf->setXY(1.55, 9);
            $pdf->drawTextBox($textoDescricao, 17.9, 15, 'L', 'T', 0);
        } else {
            $servicoCliente = $nota->getBoletoItem()->getServicoCliente();
            $servicoPrestado = $servicoCliente->getServicoPrestado();
            $plano = $servicoPrestado->getPlano();
            //$servicoPrestado = $plano->getServicoPrestado();            
            $codigoContrato = $servicoCliente->getCodCesta();
            $codigoServico = str_pad($servicoCliente->getIdservicoCliente(), 8, '0', STR_PAD_LEFT);
            $nomeServico = utf8_decode($plano->getNome());
            //$nomeServico = utf8_decode($servicoPrestado->getNome());

            $pdf->SetFont('Arial', '', 8);
            $pdf->setXY(1.55, 9);
            $pdf->drawTextBox(utf8_decode($servicoCliente->getDescricaoNotaFiscal()), 15, 10, 'L', 'T', 0);

            $pdf->setXY(1.55, 14 + $posY);
            $pdf->drawTextBox($codigoServico, 5, 5, 'L', 'T', 0);

            $pdf->setXY(4, 14 + $posY);
            $pdf->drawTextBox($nomeServico, 15, 5, 'L', 'T', 0);
            //$pdf->drawTextBox(utf8_decode($servicoCliente->getImportId() . "  -  " . $servicoPrestado->getNome()), 15, 5, 'L', 'T', 0);

            $pdf->setXY(9.85, 14);
            $pdf->drawTextBox("R$ " . number_format($nota->getValorNota(), 2, ',', '') . ",00", 15, 5, 'L', 'T', 0);

            $pdf->setXY(13.55, 14);
            $pdf->drawTextBox("R$ 0,00", 15, 5, 'L', 'T', 0);

            $pdf->setXY(17.15, 14);
            $pdf->drawTextBox("0 %", 15, 5, 'L', 'T', 0);
        }
        /*
          if (!empty($codigoContrato)) {
          $pdf->SetFont('Arial', '', 8);
          $pdf->setXY(1.55, 12);
          $pdf->drawTextBox(utf8_decode("Código do contrato - $codigoContrato"), 15, 10, 'L', 'T', 0);
          }
         */
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->setXY(1.65, 24);
        $pdf->drawTextBox(strtoupper($nota->getMestreChaveDigitalMeio()), 15, 5, 'L', 'T', 0);

        $pdf->SetFont('Arial', '', 8);
        $pdf->setXY(2.55, 25.05);
        $pdf->drawTextBox("R$ 0,00", 15, 5, 'L', 'T', 0);

        $pdf->setXY(6.4, 25.05);
        $pdf->drawTextBox("R$ 0,00", 15, 5, 'L', 'T', 0);

        $pdf->setXY(9.90, 25.05);
        $pdf->drawTextBox("R$ 0,00", 15, 5, 'L', 'T', 0);

        $pdf->setXY(13.55, 25.05);
        $pdf->drawTextBox("R$ " . $nota->getValorNota() . ",00", 15, 5, 'L', 'T', 0);

        $pdf->setXY(16.93, 25.05);
        $pdf->drawTextBox("R$ " . $nota->getValorNota() . ",00", 15, 5, 'L', 'T', 0);

        $pdf->setXY(13.58, 24.05);
        $pdf->drawTextBox(ucfirst($situacao), 15, 5, 'L', 'T', 0);

        $pdf->setXY(17.20, 24.05);
        $pdf->drawTextBox($nota->getCfop(), 15, 5, 'L', 'T', 0);

        $filename = "tmp/nota_ima_" . $nota->getNumeroNotaFiscal() . "__$codigoContrato.pdf";

        $pdf->Output($filename, 'F');

        return $filename;
    }

    public function remove_accents($string) {
        $replace = array('&lt;' => '', '&gt;' => '', '\'' => '', '&amp;' => '',
            '&quot;' => '', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'Ae',
            '&Auml;' => 'A', 'Å' => 'A', 'Ā' => 'A', 'Ą' => 'A', 'Ă' => 'A', 'Æ' => 'Ae', 'ª' => 'a',
            'Ç' => 'C', 'Ć' => 'C', 'Č' => 'C', 'Ĉ' => 'C', 'Ċ' => 'C', 'Ď' => 'D', 'Đ' => 'D',
            'Ð' => 'D', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ē' => 'E',
            'Ę' => 'E', 'Ě' => 'E', 'Ĕ' => 'E', 'Ė' => 'E', 'Ĝ' => 'G', 'Ğ' => 'G',
            'Ġ' => 'G', 'Ģ' => 'G', 'Ĥ' => 'H', 'Ħ' => 'H', 'Ì' => 'I', 'Í' => 'I',
            'Î' => 'I', 'Ï' => 'I', 'Ī' => 'I', 'Ĩ' => 'I', 'Ĭ' => 'I', 'Į' => 'I',
            'İ' => 'I', 'Ĳ' => 'IJ', 'Ĵ' => 'J', 'Ķ' => 'K', 'Ł' => 'K', 'Ľ' => 'K',
            'Ĺ' => 'K', 'Ļ' => 'K', 'Ŀ' => 'K', 'Ñ' => 'N', 'Ń' => 'N', 'Ň' => 'N',
            'Ņ' => 'N', 'Ŋ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O',
            'Ö' => 'Oe', '&Ouml;' => 'Oe', 'Ø' => 'O', 'Ō' => 'O', 'Ő' => 'O', 'Ŏ' => 'O',
            'Œ' => 'OE', 'Ŕ' => 'R', 'Ř' => 'R', 'Ŗ' => 'R', 'Ś' => 'S', 'Š' => 'S',
            'Ş' => 'S', 'Ŝ' => 'S', 'Ș' => 'S', 'Ť' => 'T', 'Ţ' => 'T', 'Ŧ' => 'T',
            'Ț' => 'T', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'Ue', 'Ū' => 'U',
            '&Uuml;' => 'Ue', 'Ů' => 'U', 'Ű' => 'U', 'Ŭ' => 'U', 'Ũ' => 'U', 'Ų' => 'U',
            'Ŵ' => 'W', 'Ý' => 'Y', 'Ŷ' => 'Y', 'Ÿ' => 'Y', 'Ź' => 'Z', 'Ž' => 'Z',
            'Ż' => 'Z', 'Þ' => 'T', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a',
            'ä' => 'ae', '&auml;' => 'ae', 'å' => 'a', 'ā' => 'a', 'ą' => 'a', 'ă' => 'a',
            'æ' => 'ae', 'ç' => 'c', 'ć' => 'c', 'č' => 'c', 'ĉ' => 'c', 'ċ' => 'c',
            'ď' => 'd', 'đ' => 'd', 'ð' => 'd', 'è' => 'e', 'é' => 'e', 'ê' => 'e',
            'ë' => 'e', 'ē' => 'e', 'ę' => 'e', 'ě' => 'e', 'ĕ' => 'e', 'ė' => 'e',
            'ƒ' => 'f', 'ĝ' => 'g', 'ğ' => 'g', 'ġ' => 'g', 'ģ' => 'g', 'ĥ' => 'h',
            'ħ' => 'h', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ī' => 'i',
            'ĩ' => 'i', 'ĭ' => 'i', 'į' => 'i', 'ı' => 'i', 'ĳ' => 'ij', 'ĵ' => 'j',
            'ķ' => 'k', 'ĸ' => 'k', 'ł' => 'l', 'ľ' => 'l', 'ĺ' => 'l', 'ļ' => 'l',
            'ŀ' => 'l', 'ñ' => 'n', 'ń' => 'n', 'ň' => 'n', 'ņ' => 'n', 'ŉ' => 'n',
            'ŋ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'oe',
            '&ouml;' => 'oe', 'ø' => 'o', 'ō' => 'o', 'ő' => 'o', 'ŏ' => 'o', 'œ' => 'oe',
            'ŕ' => 'r', 'ř' => 'r', 'ŗ' => 'r', 'š' => 's', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ü' => 'ue', 'ū' => 'u', '&uuml;' => 'ue', 'ů' => 'u', 'ű' => 'u',
            'ŭ' => 'u', 'ũ' => 'u', 'ų' => 'u', 'ŵ' => 'w', 'ý' => 'y', 'ÿ' => 'y',
            'ŷ' => 'y', 'ž' => 'z', 'ż' => 'z', 'ź' => 'z', 'þ' => 't', 'ß' => 'ss',
            'ſ' => 'ss', 'ый' => 'iy', 'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G',
            'Д' => 'D', 'Е' => 'E', 'Ё' => 'YO', 'Ж' => 'ZH', 'З' => 'Z', 'И' => 'I',
            'Й' => 'Y', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
            'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
            'Х' => 'H', 'Ц' => 'C', 'Ч' => 'CH', 'Ш' => 'SH', 'Щ' => 'SCH', 'Ъ' => '',
            'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'YU', 'Я' => 'YA', 'а' => 'a',
            'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
            'ю' => 'yu', 'я' => 'ya'
        );

        $result = str_replace(array_keys($replace), $replace, $string);
        return $result;
    }

    public function adicionarCaracter($string, $caracterEspaco, $caracterDelimitador, $pad) {
        $novaString = "";
        for ($i = 0; $i < ($caracterEspaco - strlen($string)); $i++)
            $novaString .= $caracterDelimitador;

        if ($pad == STR_PAD_LEFT)
            $novaString = $novaString . $string;
        else
            $novaString = $string . $novaString;

        return $novaString;
    }

    public function countAccents($string) {
        $accents = array('á', 'à', 'ã', 'â',
            'é', 'è', 'ê',
            'í', 'ì', 'î',
            'ó', 'ò', 'ô', 'õ',
            'ú', 'ù', 'û'
        );

        $count = 0;
        foreach ($accents as $accent) {
            $countAtual = substr_count($string, "$accent");

            if (in_array($accent, array('ã', 'õ'))) {
                $countAtual *= 2;
            } else if (in_array($accent, array('á', 'é', 'í', 'ó', 'ú'))) {
                $countAtual *= 3;
            }

            $count += $countAtual;
        }

        return $count;
    }

    /* inicio geração de notas fiscais */

    public function criarNotaFiscal($cobrancas, $dataEmissao, $situacao) {
        ini_set('max_execution_time', 600); //600 seconds = 10 minutes

        $retorno = array();
        $retorno['status'] = true;
        $retorno['erros'] = array();

        if (sizeof($cobrancas) == 0) {
            $retorno['status'] = false;
            $retorno['erros'] = array('Não foi encontrada nenhuma cobrança para a geração da Nota Fiscal. Verifique se a mesma já não se encontra gerada ou cancelada.');

            return $retorno;
        }

        $competencia = $this->getCompetenciaAtual();
        $numeroSequencialNotaFiscal = $competencia->getNumeroNf();
        /*
          $fiscal = DadosFiscalQuery::create()->filterByCompetencia($competencia)->orderByNumeroNotaFiscal(Criteria::DESC)->findOne();

          if ($fiscal != null) {
          $ultimoNumeroNotaFiscal = $fiscal->getNumeroNotaFiscal();
          if ($numeroSequencialNotaFiscal < $ultimoNumeroNotaFiscal) {
          $numeroSequencialNotaFiscal = $ultimoNumeroNotaFiscal;
          $competencia->setNumeroNf($numeroSequencialNotaFiscal);
          $competencia->save();
          }
          }
         */
        $codNotaFiscalLotePrevio = CompetenciaQuery::create()->withColumn('SUM(numero_nf)', 'total')
                ->findOne()
                ->getTotal(); // Try this (Don't worry about findOne,  SUM returns one row anyway).
        // Note that getTotal() is generated from reference 'total' of withColumn.
        // The convention is get+Columnname ( camelcase )

        $qtdeItens = DadosFiscalQuery::create()->withColumn('MAX(posicao_ultimo_item)', 'total')->where("competencia_id = " . $competencia->getId())->findOne()->getTotal();
        $inicioFaturaItem = 0;
        $ano = $competencia->getAno();
        $mes = $competencia->getMes();

        $periodoInicialCompetencia = DateTime::createFromFormat('Y-m-d', $competencia->getPeriodoInicial()->format('Y-m-d')); // Hora estava interferindo
        $periodoFinalCompetencia = DateTime::createFromFormat('Y-m-d', $competencia->getPeriodoFinal()->format('Y-m-d')); // Hora estava interferindo
        $dataDaUltimaEmissao = DateTime::createFromFormat('Y-m-d', $competencia->getUltimaDataEmissao());
        $dataDaEmissao = DateTime::createFromFormat('Y-m-d', $dataEmissao);

        if ($dataDaEmissao < $periodoInicialCompetencia || $dataDaEmissao > $periodoFinalCompetencia) {
            $retorno['status'] = false;
            $retorno['erros'][] = 'Data de emissão se encontra fora do Período da Competência Atual.';
        }

        if ($dataDaEmissao < $dataDaUltimaEmissao) {
            $retorno['status'] = false;
            $retorno['erros'][] = 'Data de Emissão não pode ser menor do que a Data da Última Emissão.';
        }

        if (sizeof($cobrancas) == 0) {
            $retorno['status'] = false;
            $retorno['erros'][] = 'Não foi encontrado nenhum boleto para a geração da nota fiscal.';
        }

        if ($retorno['status']) {
            foreach ($cobrancas as $cobranca) {
                $dataAtual = new DateTime('now');
                $itensFiscais = array();
                $numeroSequencialNotaFiscal++;
                $codNotaFiscalLotePrevio++;
                $posicaoPrimeiroItem = 0;


                $retorno['item'] = array();
                $documento = $this->dadosCadastraisMidia_2017($cobranca, $dataEmissao, $numeroSequencialNotaFiscal);

                $valorTotalItens = 0.0;
                $qtdeItem = 1;
                $itemsCobranca = $cobranca->getBoletoItems();

                foreach ($itemsCobranca as $itemCobranca) {
                    $qtdeItens++;

                    $idBoleto = $cobranca->getIdboleto();
                    //$valorItem = $itemCobranca->getValor();
                    $valorItem = $itemCobranca->getBaseCalculoScm();

                    $dadosNota = array();
                    $dadosNota['dataEmissao'] = $dataEmissao;
                    $dadosNota['ano'] = $ano;
                    $dadosNota['mes'] = $mes;
                    $dadosNota['numeroSequencialNotaFiscal'] = $numeroSequencialNotaFiscal;
                    $dadosNota['qtdeItem'] = $qtdeItem;
                    $dadosNota['situacao'] = $situacao;

                    $item = $this->itemDocumentoFiscalMidia_2017($itemCobranca, $dadosNota);
                    $dadoFiscalItem = new DadosFiscalItem();
                    $dadoFiscalItem->setBoleto($cobranca);
                    $dadoFiscalItem->setBoletoItem($itemCobranca);
                    $dadoFiscalItem->setDataCadastro($dataAtual);
                    $dadoFiscalItem->setDataAlterado($dataAtual);
                    $dadoFiscalItem->setPosicaoItem($qtdeItens);
                    $dadoFiscalItem->setItem($item);
                    $dadoFiscalItem->setNumeroItemNotaFiscal($qtdeItem);
                    $dadoFiscalItem->setValorItem($valorItem);
                    $valorTotalItens += $valorItem;

                    $itensFiscais[] = $dadoFiscalItem;

                    if ($inicioFaturaItem != $idBoleto) {// pegar o inicio da posicao do item referente ao boleto no arquivo mestre
                        $inicioFaturaItem = $idBoleto;
                        $posicaoPrimeiroItem = $qtdeItens;
                    }

                    $qtdeItem++;
                    //$retorno['item'][] = $item;
                }

                $dadosNota = array();
                $dadosNota['valor'] = $valorTotalItens;
                $dadosNota['inicioItem'] = $posicaoPrimeiroItem;
                $dadosNota['dataEmissao'] = $dataEmissao;
                $dadosNota['ano'] = $ano;
                $dadosNota['mes'] = $mes;
                $dadosNota['numeroSequencialNotaFiscal'] = $numeroSequencialNotaFiscal;
                $dadosNota['situacao'] = $situacao;

                $mestre = $this->arquivoMestreDocumentoFiscal_2017($cobranca, $dadosNota);

                //$retorno['documento'] = $documento;
                //$retorno['mestre'] = $mestre;

                $dados = $dadosNota;
                $dados['posicaoPrimeiroItem'] = $posicaoPrimeiroItem;
                $dados['qtdeItens'] = $qtdeItens;
                $dados['codNotaFiscalLotePrevio'] = $codNotaFiscalLotePrevio;

                //$this->gravarNotaFiscal($competencia, $cobranca, $documento, $mestre, $itensFiscais, $dados);
                //$retorno = $retornoNota;
                //$retorno['mensagem'] .= $this->gravarNotaFiscal($competencia, $cobranca, $documento, $mestre, $itensFiscais, $dados);

                $retornoGravacaoNota = $this->gravarNotaFiscal($competencia, $cobranca, $documento, $mestre, $itensFiscais, $dados);
                if (!$retornoGravacaoNota['status']) {
                    $retorno['status'] = $retornoGravacaoNota['status'];
                    $retorno['erros'][] = $retornoGravacaoNota['mensagem'];
                    break;
                } else {
                    
                }
            }
        }

        return $retorno;
    }

    public function gravarNotaFiscal($competencia, $cobranca, $documento, $mestre, $items, $dados) {
        $user = $this->getUser();
        $cliente = $cobranca->getCliente();
        $pessoa = $cliente->getPessoa();

        $idBoleto = $cobranca->getIdboleto();
        $tipoPessoa = $pessoa->getTipoPessoa();
        $pessoaImportId = $pessoa->getImportId();

        $dadoFiscal = new DadosFiscal();
        $dadoFiscal->setBoletoId($idBoleto);
        $dadoFiscal->setDocumento($documento);
        $dadoFiscal->setMestre($mestre['linha']);
        $dadoFiscal->setNumeroNotaFiscal($dados['numeroSequencialNotaFiscal']);
        $dadoFiscal->setCompetenciaId($competencia->getId());
        $dadoFiscal->setPosicaoPrimeiroItem($dados['posicaoPrimeiroItem']);
        $dadoFiscal->setPosicaoUltimoItem($dados['qtdeItens']);
        $dadoFiscal->setDataEmissao($dados['dataEmissao']);
        $dadoFiscal->setMestreChaveDigitalMeio($mestre['md5_mestre_chave_digital_meio']);

        if ($tipoPessoa == 'fisica') {
            $dadoFiscal->setCfop("5307");
        } else {
            $dadoFiscal->setCfop("5303");
        }

        $dadoFiscal->setBaseCalculo("0");
        $dadoFiscal->setValorIcms("0");
        $dadoFiscal->setNTributo("0");
        $dadoFiscal->setValorNota($dados['valor']);

        if ($dados['situacao'] == 'N') {
            $dadoFiscal->setSituacao('normal');
        } else if ($dados['situacao'] == 'S') {
            $dadoFiscal->setSituacao('cancelada');
        }

        $dadoFiscal->setServicoClienteImportId('00000');
        $dadoFiscal->setPessoaImportId($pessoaImportId);
        $dadoFiscal->setCodNotaFiscalLotePrevio($dados['codNotaFiscalLotePrevio']);
        $dadoFiscal->setDataCadastro(new DateTime('now'));
        $dadoFiscal->setDataAlterado(new DateTime('now'));
        $dadoFiscal->setUsuarioAlterado($user->getIdusuario());
        $dadoFiscal->setUltimaNotaGerada(true);

        $retorno = array();
        $retorno['status'] = true;
        /*
          $retorno['numeroSequencialNotaFiscal'] = $dados['numeroSequencialNotaFiscal'];
         */
        $ultimaNota = DadosFiscalQuery::create()
                ->filterByBoletoId($idBoleto)
                ->orderById(Criteria::DESC)
                ->findOne();

        $ultimaDataEmissao = $dados['dataEmissao'];

        $con = Propel::getWriteConnection(DadosFiscalTableMap::DATABASE_NAME);
        $con->beginTransaction();

        try {
            if ($ultimaNota != null) {
                $ultimaNota->setUltimaNotaGerada(false);
                $ultimaNota->save();
            }

            $dadoFiscal->save();
            foreach ($items as $itemFiscal) {
                $itemFiscal->setDadosFiscal($dadoFiscal);
                $itemFiscal->save();
            }

            $competencia->setNumeroNf($dados['numeroSequencialNotaFiscal']);
            $competencia->setUltimaDataEmissao($ultimaDataEmissao);
            $competencia->save();

            if ($dados['situacao'] == 'N') {
                $cobranca->setNotaScmGerada(true);
            } else if ($dados['situacao'] == 'S') {
                $cobranca->setNotaScmGerada(false);
            }

            $cobranca->save();

            $con->commit();
            /*
              if ($dadoFiscal->save()) {
              if ($ultimaNota != null) {
              $ultimaNota->setUltimaNotaGerada(false);
              $ultimaNota->save();
              }

              $id = $dadoFiscal->getId();

              $itemFiscalSalvo = true;
              foreach ($items as $itemFiscal) {
              //$itemFiscal->setDadosFiscal($dadoFiscal);
              $itemFiscal->setDadosFiscalId($id);
              try {
              $itemFiscal->save();
              } catch (PropelException $ex) {
              $itemFiscalSalvo = false;
              $retorno['status'] = false;
              $retorno['mensagem'] = $ex->getMessage();
              break;
              }

              if ($itemFiscalSalvo) {
              if ($dados['situacao'] == 'N') {
              $cobranca->setNotaGerada(true);
              } else if ($dados['situacao'] == 'S') {
              $cobranca->setNotaGerada(false);
              }
              } else {
              break;
              }
              }

              if (!$itemFiscalSalvo) {
              foreach ($items as $item) {
              $idDadoFiscalItem = $item->getIddadosFiscalItem();

              if ($idDadoFiscalItem != 0 && $idDadoFiscalItem != null)
              $item->delete();
              }

              $retorno['numeroSequencialNotaFiscal'] = $retorno['numeroSequencialNotaFiscal'] -1;
              $dadoFiscal->delete();
              $retorno['status'] = false;
              $retorno['mensagem'] .= "Não foi possível atualizar a fatura atual enquanto eram gerada(s) a(s) nota(s) fiscal(is).";
              } else {
              $cobranca->save();

              $competencia->setNumeroNf($dados['numeroSequencialNotaFiscal']);
              $ultimaDataEmissao = $dados['dataEmissao']; //"$anoEmissao-$mesEmissao-$diaEmissao";
              $competencia->setUltimaDataEmissao($ultimaDataEmissao);

              if (!$competencia->save()) {
              $dadoFiscal->delete();

              $retorno['status'] = false;
              $retorno['mensagem'] .= "Não foi possível atualizar a competência atual enquanto eram gerada(s) a(s) nota(s) fiscal(is).";
              }
              }
              } else {
              $retorno['status'] = false;
              $retorno['mensagem'] .= "Não foi possível gerar a nota fiscal para a Fatura(Id do Boleto): $idBoleto";
              }
             * 
             */
        } catch (PropelException $ex) {
            $con->rollback();

            $retorno['status'] = false;
            $retorno['mensagem'] = "Não foi possível gerar a nota fiscal para a Fatura(Id do Boleto): $idBoleto";
        }

        return $retorno;
    }

    public function dadosCadastraisMidia_2017($cobranca, $dataEmissao, $numeroSequencialNotaFiscal) {
        $cliente = $cobranca->getCliente();
        $pessoa = $cliente->getPessoa();
        $enderecoCliente = $cliente->getEnderecoCliente();

        if ($enderecoCliente == null) {
            $enderecoCliente = new EnderecoCliente();
            $enderecoCliente->setCliente($cliente);
            $enderecoCliente->setRua($pessoa->getRua());
            $enderecoCliente->setBairro($pessoa->getBairro());
            $enderecoCliente->setNum($pessoa->getNum());
            $enderecoCliente->setComplemento($pessoa->getComplemento());
            $enderecoCliente->setPontoReferencia($pessoa->getPontoReferencia());
            $enderecoCliente->setCidadeId($pessoa->getCidadeId());
            $enderecoCliente->setUf($pessoa->getUf());
            $enderecoCliente->setDataCadastro(new \DateTime('now'));
            $enderecoCliente->setDataAlterado(new \DateTime('now'));
            $enderecoCliente->setUsuarioAlterado(1);
            $enderecoCliente->save();
        }

        $cidade = $enderecoCliente->getCidade();
        $telefones = $pessoa->getTelefones();

        $cpfCnpj = $pessoa->getCpfCnpj();
        $tipoPessoa = $pessoa->getTipoPessoa();
        $nome = $pessoa->getNome();
        $razaoSocial = $pessoa->getRazaoSocial();

        $rua = $enderecoCliente->getRua();
        $numero = $enderecoCliente->getNum();
        $complemento = $enderecoCliente->getComplemento();
        $bairro = $enderecoCliente->getBairro();
        $cep = $enderecoCliente->getCep();
        $cep = str_replace(array('.', '-'), '', $cep);
        $uf = $enderecoCliente->getUf();

        $municipio = $cidade->getMunicipio();
        $codCidade = $cidade->getIdcidade();
        $codigoMunicipio = $cidade->getCodigo();

        $telefoneContato = "";
        foreach ($telefones as $telefone) {
            if (empty($telefoneContato)) {
                $ddd = $telefone->getDdd();
                if (empty($ddd))
                    $ddd = '22';

                $telefoneContato = $ddd . $telefone->getNumero();
                break;
            }
        }

        $servicoContratadoSuc = $cobranca->getIdboleto();

        $erros = array();

        $modelo = "21"; // 21 ou 22
        $serie = "U  ";
        //$data = explode("/", $data_emissao);
        //$dataEmissao = $data[2] . $data[1] . $data[0];
        $dataEmissao = str_replace('-', '', $dataEmissao);

        $linha = "";
        $linha .= substr(str_pad($cpfCnpj, 14, "0", STR_PAD_LEFT), 0, 14); // CPF/CNPJ -> Tamanho 14 (Pos(1, 14), Formato: Numero)
        // Campos 2 e 3
        if ($tipoPessoa == 'fisica') {
            $linha .= substr($this->adicionarCaracter("ISENTO", 14, " ", STR_PAD_RIGHT), 0, 14);
            $linha .= substr($this->adicionarCaracter($this->remove_accents(trim($nome)), 35, " ", STR_PAD_RIGHT), 0, 35);
        } else {
            $rg = trim($pessoa->getRgInscrest());
            if (!empty($rg))
                $linha .= substr($this->adicionarCaracter($this->remove_accents(str_replace(".", "", str_replace("-", "", $rg))), 14, " ", STR_PAD_RIGHT), 0, 14);
            else
                $linha .= substr($this->adicionarCaracter("ISENTO", 14, " ", STR_PAD_RIGHT), 0, 14);

            $linha .= substr($this->adicionarCaracter($this->remove_accents(trim($razaoSocial)), 35, " ", STR_PAD_RIGHT), 0, 35);
        }

        $linha .= substr($this->adicionarCaracter($this->remove_accents(str_replace('°', ' ', ltrim($rua))), 45, " ", STR_PAD_RIGHT), 0, 45); // Campo 4
        $linha .= substr($this->adicionarCaracter($numero, 5, "0", STR_PAD_LEFT), 0, 5); // Campo 5
        $complemento = str_replace('°', '', ltrim($complemento));
        $complemento = str_replace('º', '', $complemento);
        $linha .= substr($this->adicionarCaracter($this->remove_accents($complemento), 15, " ", STR_PAD_RIGHT), 0, 15); // Campo 6  complemento
        $linha .= substr($this->adicionarCaracter($cep, 8, "0", STR_PAD_LEFT), 0, 8); // Campo 7    cep
        $linha .= substr($this->adicionarCaracter($this->remove_accents(ltrim($bairro)), 15, " ", STR_PAD_RIGHT), 0, 15); // Campo 8

        $palavra = "";
        if (in_array($codCidade, array(3192, 2021, 3334))) {
            $palavra = substr($this->adicionarCaracter($municipio, 32, " ", STR_PAD_RIGHT), 0, 32); // Campo 9
            $linha .= substr($this->adicionarCaracter($municipio, 32, " ", STR_PAD_RIGHT), 0, 32); // Campo 9
        } else if (in_array($codCidade, array(3897, 184, 3260))) {
            $linha .= substr($this->adicionarCaracter($municipio, 31, " ", STR_PAD_RIGHT), 0, 31); // Campo 9
            //}// else if ($cliente['codCidade'] == 3334) {
            ///	$linha .= substr($this->adicionarCaracter($cliente['municipio'], 32, " ", STR_PAD_RIGHT), 0, 32); // Campo 9
        } else {
            $countAccents = $this->countAccents($municipio);
            $palavra = substr($this->adicionarCaracter($municipio, 30 + $countAccents, " ", STR_PAD_RIGHT), 0, 30 + $countAccents); // Campo 9
            $linha .= substr($this->adicionarCaracter($municipio, 30 + $countAccents, " ", STR_PAD_RIGHT), 0, 30 + $countAccents); // Campo 9
        }

        //$linha .= strlen($palavra);

        $linha .= substr($this->adicionarCaracter($uf, 2, "RJ", STR_PAD_RIGHT), 0, 2); // Campo 10
        //$linha .= "0" . substr($this->adicionarCaracter($cliente['telefone'], 11, "2", STR_PAD_LEFT), 0, 11); // Campo 11 telefone
        $linha .= str_pad(str_pad($telefoneContato, 11, "2", STR_PAD_LEFT), 12, " ", STR_PAD_RIGHT);
        //substr($this->adicionarCaracter($cliente['telefone'], 12, "2", STR_PAD_LEFT), 0, 12);
        $linha .= substr($this->adicionarCaracter($servicoContratadoSuc, 12, " ", STR_PAD_RIGHT), 0, 12); // Campo 12
        $linha .= str_pad(str_pad($telefoneContato, 11, "2", STR_PAD_LEFT), 12, " ", STR_PAD_RIGHT);
        // substr($this->adicionarCaracter($cliente['telefone'], 12, "2", STR_PAD_LEFT), 0, 12);
        //substr($this->adicionarCaracter($cliente['telefone'], 12, "2", STR_PAD_LEFT), 0, 12); // Campo 13 igual ao mestre
        $linha .= substr($this->adicionarCaracter($uf, 2, "RJ", STR_PAD_RIGHT), 0, 2); // Campo 14
        $linha .= substr($this->adicionarCaracter($dataEmissao, 8, "0", STR_PAD_RIGHT), 0, 8); // Campo 15
        $linha .= substr($this->adicionarCaracter($modelo, 2, "0", STR_PAD_RIGHT), 0, 2); // Campo 16
        $linha .= substr($this->adicionarCaracter($serie, 3, " ", STR_PAD_RIGHT), 0, 3); // Campo 17
        $linha .= substr($this->adicionarCaracter($numeroSequencialNotaFiscal, 9, "0", STR_PAD_LEFT), 0, 9); // Campo 18
        $linha .= substr($this->adicionarCaracter($codigoMunicipio, 7, "0", STR_PAD_LEFT), 0, 7); // Campo 19
        $linha .= substr($this->adicionarCaracter("", 5, " ", STR_PAD_RIGHT), 0, 5); // Campo 20
        $linha .= md5(utf8_decode($linha)); // Campo 21
        $linha .= "\r\n";

        return $linha;
    }

    public function arquivoMestreDocumentoFiscal_2017($cobranca, $dadosNota) {
        $cliente = $cobranca->getCliente();
        $pessoa = $cliente->getPessoa();
        $enderecoCliente = $cliente->getEnderecoCliente();
        $telefones = $pessoa->getTelefones();

        $idBoleto = $cobranca->getIdboleto();

        $cpfCnpj = $pessoa->getCpfCnpj();
        $tipoPessoa = $pessoa->getTipoPessoa();
        $nome = $pessoa->getNome();
        $razaoSocial = $pessoa->getRazaoSocial();

        $uf = $enderecoCliente->getUf();

        $telefoneContato = "";
        foreach ($telefones as $telefone) {
            if (empty($telefoneContato)) {
                $ddd = $telefone->getDdd();
                if (empty($ddd))
                    $ddd = '22';

                $telefoneContato = $ddd . $telefone->getNumero();
                break;
            }
        }

        $servicoContratadoSuc = $cobranca->getIdboleto();

        $erros = array();
        $arrayRetorno = array('linha' => '', 'md5_mestre_chave_digital_meio' => '');
        $cnpjEmitente = "10934273000167";
        $modelo = "21";
        $serie = "U";

        $linha = "";
        $linha .= substr(str_pad($cpfCnpj, 14, "0", STR_PAD_LEFT), 0, 14); // CPF/CNPJ -> Tamanho 14 (Pos(1, 14), Formato: Numero)

        if ($tipoPessoa == 'fisica') {
            $linha .= substr($this->adicionarCaracter($this->remove_accents("ISENTO"), 14, " ", STR_PAD_RIGHT), 0, 14);
            $linha .= substr($this->adicionarCaracter($this->remove_accents(trim($nome)), 35, " ", STR_PAD_RIGHT), 0, 35);
        } else {
            $rg = trim($pessoa->getRgInscrest());
            if (!empty($rg))
                $linha .= substr($this->adicionarCaracter(str_replace(".", "", str_replace("-", "", $rg)), 14, " ", STR_PAD_RIGHT), 0, 14);
            else
                $linha .= $this->adicionarCaracter("ISENTO", 14, " ", STR_PAD_RIGHT);

            $linha .= substr($this->adicionarCaracter($this->remove_accents(trim($razaoSocial)), 35, " ", STR_PAD_RIGHT), 0, 35);
        }

        $linha .= $uf;

        /*
          if ($cliente['tipo_pessoa'] == 'fisica')
          $linha .= "2";
          else
          $linha .= "1";
         */

        $linha .= "0";
        $linha .= "400";
        $linha .= substr($this->adicionarCaracter($servicoContratadoSuc, 12, " ", STR_PAD_RIGHT), 0, 12);

        //$data = explode("/", $data_emissao);
        //$dataEmissao = $data[2] . $data[1] . $data[0];
        $dataEmissao = str_replace('-', '', $dadosNota['dataEmissao']);
        $linha .= $dataEmissao;
        //$linha .= $data[0] . $data[1] . $data[2];

        $linha .= str_pad($modelo, 2, "0", STR_PAD_LEFT);
        $linha .= str_pad($serie, 3, " ", STR_PAD_RIGHT);
        //$linha .= "21U  ";
        $linha .= str_pad($dadosNota['numeroSequencialNotaFiscal'], 9, "0", STR_PAD_LEFT); // Campos 9

        $cpfCnpj = substr(str_pad($cpfCnpj, 14, "0", STR_PAD_LEFT), 0, 14);
        $sequencial = str_pad($dadosNota['numeroSequencialNotaFiscal'], 9, "0", STR_PAD_LEFT);
        //$valorTotal = str_pad(str_pad($cliente['valor'], 10, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
        $valorTotal = str_pad(str_replace([',', '.'], ['', ''], number_format($dadosNota['valor'], 2, '', '.')), 12, "0", STR_PAD_LEFT);

        $bcIcms = str_pad("0", 24, "0", STR_PAD_LEFT);

        $md5 = $cpfCnpj . $sequencial . $valorTotal . $bcIcms . $dataEmissao . $cnpjEmitente;
        $linha .= md5($md5);

        //$linha .= str_pad(str_pad($cliente['valor'], 10, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_replace([',', '.'], ['', ''], number_format($dadosNota['valor'], 2, '', '.')), 12, "0", STR_PAD_LEFT);
        $linha .= str_pad("0", 36, "0", STR_PAD_LEFT);
        //$linha .= str_pad(str_pad($cliente['valor'], 10, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_replace([',', '.'], ['', ''], number_format($dadosNota['valor'], 2, '', '.')), 12, "0", STR_PAD_LEFT);
        //$linha .= 'N';
        $linha .= $dadosNota['situacao'];

        $ano = $dadosNota['ano'][2] . $dadosNota['ano'][3];
        $linha .= str_pad($ano, 2, "0", STR_PAD_LEFT) . str_pad($dadosNota['mes'], 2, "0", STR_PAD_LEFT);
        $linha .= str_pad($dadosNota['inicioItem'], 9, "0", STR_PAD_LEFT);
        $linha .= str_pad(str_pad($telefoneContato, 11, "2", STR_PAD_LEFT), 12, " ", STR_PAD_RIGHT);
        //substr($this->adicionarCaracter($cliente['telefone'], 11, "2", STR_PAD_LEFT), 0, 11);
        //substr($this->adicionarCaracter($cliente['telefone'], 12, "2", STR_PAD_LEFT), 0, 12);

        if ($tipoPessoa == 'juridica') {
            $linha .= "1";
            $linha .= str_pad("99", 2, "0", STR_PAD_LEFT); // a definir ainda
        } else {
            $linha .= "2";
            $linha .= str_pad("03", 2, "0", STR_PAD_LEFT);
        }

        $linha .= substr($this->adicionarCaracter("0", 2, "0", STR_PAD_LEFT), 0, 2);
        $linha .= substr($this->adicionarCaracter("", 12, " ", STR_PAD_LEFT), 0, 12); // 26
        $linha .= substr($this->adicionarCaracter($cnpjEmitente, 14, " ", STR_PAD_LEFT), 0, 14); // 27
        $linha .= substr($this->adicionarCaracter($idBoleto, 20, "0", STR_PAD_LEFT), 0, 20);
        $linha .= $valorTotal; // substr($this->adicionarCaracter($cliente['valor'], 12, "0", STR_PAD_LEFT), 0, 14);
        $linha .= substr($this->adicionarCaracter("0", 8, "0", STR_PAD_LEFT), 0, 8);
        $linha .= substr($this->adicionarCaracter("0", 8, "0", STR_PAD_LEFT), 0, 8);
        $linha .= substr($this->adicionarCaracter("", 50, " ", STR_PAD_LEFT), 0, 50);
        $linha .= substr($this->adicionarCaracter("", 8, "0", STR_PAD_LEFT), 0, 8);

        $informacoesAdicionais = "";
        if ($dadosNota['situacao'] == 'R' || $dadosNota['situacao'] == 'C') {
            // "AAMM_MO_SSS_NNNNNNNNN_AAAAMMDD". Exemplo: "0901_22_A _000001234_20090131", para o documento fiscal da refer�ncia
            // "0901", modelo "22", s�rie "A ", n�mero "000001234", emitido em 31/01/2009
            $informacoesAdicionais = $dadosNota['ano'] . $dadosNota['mes'] . "_" . $modelo . "_" . $serie . "_" . $sequencial . "_" . $dataEmissao;
        }

        $linha .= substr($this->adicionarCaracter($informacoesAdicionais, 30, " ", STR_PAD_LEFT), 0, 30);
        $linha .= substr($this->adicionarCaracter("", 5, " ", STR_PAD_LEFT), 0, 5);
        $linha .= md5($linha);

        $linha .= "\r\n";

        $arrayRetorno['linha'] = $linha;
        $arrayRetorno['md5_mestre_chave_digital_meio'] = md5($md5);

        return $arrayRetorno;
    }

    public function itemDocumentoFiscalMidia_2017($itemCobranca, $dadosNota) {
        $cobranca = $itemCobranca->getBoleto();
        $cliente = $cobranca->getCliente();
        $pessoa = $cliente->getPessoa();
        $enderecoCliente = $cliente->getEnderecoCliente();
        $servicoCliente = $itemCobranca->getServicoCliente();
        $servicoPrestado = $servicoCliente->getServicoPrestado();

        $cpfCnpj = $pessoa->getCpfCnpj();
        $tipoPessoa = $pessoa->getTipoPessoa();
        $uf = $enderecoCliente->getUf();
        $valor = $itemCobranca->getValor();
        $idServicoCliente = $servicoCliente->getIdservicoCliente();
        $idServicoPrestado = $servicoPrestado->getIdservicoPrestado();
        $download = $servicoPrestado->getDownload();

        $erros = array();
        $ano = $dadosNota['ano'][2] . $dadosNota['ano'][3];
        $modelo = "21";
        $serie = "U";

        $linha = "";
        $linha .= str_pad($cpfCnpj, 14, "0", STR_PAD_LEFT); // CPF/CNPJ -> Tamanho 14 (Pos(1, 14), Formato: Numero)
        $linha .= $uf;

        /*
          if ($cliente['tipo_pessoa'] == 'juridica')
          $linha .= "1";
          else
          $linha .= "2";
         */

        $linha .= "0";
        $linha .= "4";
        $linha .= "00";
        //$data = explode("/", $data_emissao);
        //$dataEmissao = $data[2] . $data[1] . $data[0];
        $dataEmissao = str_replace('-', '', $dadosNota['dataEmissao']);
        $linha .= $dataEmissao;
        //$linha .= $data[0] . $data[1] . $data[2];
        $linha .= str_pad($modelo, 2, "0", STR_PAD_LEFT);
        $linha .= str_pad($serie, 3, " ", STR_PAD_RIGHT);
        $linha .= str_pad($dadosNota['numeroSequencialNotaFiscal'], 9, "0", STR_PAD_LEFT);

        if ($tipoPessoa == 'fisica')
            $linha .= "5307";
        else
            $linha .= "5303";

        //$linha .= "001";
        $linha .= str_pad($dadosNota['qtdeItem'], 3, "0", STR_PAD_LEFT);
        $linha .= str_pad($idServicoPrestado, 10, " ", STR_PAD_RIGHT);
        $linha .= str_pad("Servico de Internet ? SCM - Periodo de r", 40, " ", STR_PAD_LEFT);
        $linha .= "0104      ";
        $linha .= str_pad(str_pad("1", 9, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_pad("1", 9, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_replace([',', '.'], ['', ''], number_format($valor, 2, '', '.')), 11, "0", STR_PAD_LEFT);
        //$linha .= str_pad(str_pad($cliente['valor'], 9, "0", STR_PAD_LEFT), 11, "0", STR_PAD_RIGHT); // Campos 18  valores do item separadamente n�o est� somando, só pega o consolidado ainda n�o sei o pq :/
        $linha .= str_pad("0", 11, "0", STR_PAD_LEFT);
        $linha .= str_pad("0", 11, "0", STR_PAD_LEFT);
        $linha .= str_pad("0", 11, "0", STR_PAD_LEFT);
        $linha .= str_pad("0", 11, "0", STR_PAD_LEFT);
        $linha .= str_pad("0", 11, "0", STR_PAD_LEFT);
        //$linha .= str_pad(str_pad($cliente['valor'], 9, "0", STR_PAD_LEFT), 15, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_pad(str_replace([',', '.'], ['', ''], number_format($valor, 2, '', '.')), 11, "0", STR_PAD_LEFT), 15, "0", STR_PAD_RIGHT);
        $linha .= $dadosNota['situacao'];
        $linha .= str_pad($ano, 2, "0", STR_PAD_LEFT) . str_pad($dadosNota['mes'], 2, "0", STR_PAD_LEFT); // Campos 27

        $linha .= str_pad($idServicoCliente, 15, "0", STR_PAD_LEFT);
        $linha .= str_pad(str_pad($download, 8, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_pad("0", 5, "0", STR_PAD_LEFT), 11, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_pad("0", 2, "0", STR_PAD_LEFT), 6, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_pad("0", 9, "0", STR_PAD_LEFT), 11, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_pad("0", 2, "0", STR_PAD_LEFT), 6, "0", STR_PAD_RIGHT);
        $linha .= str_pad(str_pad("0", 9, "0", STR_PAD_LEFT), 11, "0", STR_PAD_RIGHT);
        $linha .= str_pad("", 1, " ", STR_PAD_LEFT);
        $linha .= str_pad("00", 2, "0", STR_PAD_LEFT);


        $linha .= str_pad(" ", 5, " ", STR_PAD_LEFT); // Campos 28
        $linha .= md5($linha); // Campos 29
        $linha .= "\r\n";

        return $linha;
    }

    public function geracaoCobrancas($competencia, $cobrancas, $cobrancaSeparada = false, $vencimento = '') {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();
        $retornoView['logs'] = array();

        $forcarVencimento = false;
        if (!empty($vencimento)) {
            $vencimentoDaCobranca = DateTime::createFromFormat('d/m/Y', $vencimento);
            $vencimentoCobranca = $vencimentoDaCobranca->format('Y-m-d');
            $forcarVencimento = true;
        }

        foreach ($cobrancas as $dadosCobranca) {
            $servicoId = $dadosCobranca['servico'];
            $valorContrato = $dadosCobranca['valor'];

            if ($valorContrato == 0) {
                continue;
            }

            $servicoCliente = ServicoClienteQuery::create()->findPk($servicoId);
            $servicoPrestado = $servicoCliente->getServicoPrestado();
            $plano = $servicoPrestado->getPlano();
            $contrato = $servicoCliente->Contrato();
            $cliente = $servicoCliente->getCliente();
            $clienteId = $cliente->getIdcliente();

            $valorDesconto = $servicoCliente->getValorDesconto();
            $forcarValor = $contrato->getForcarValor();
            $regraFaturamento = $contrato->getRegraFaturamento();

            if (!$forcarVencimento) {
                $vencimento = $competencia->getAno() . '-' . $competencia->getMes() . '-' . $regraFaturamento->getPeriodo1DiaVencimentoCobranca();
                $vencimentoCobranca = date('Y-m-d', strtotime('+1 month', strtotime($vencimento)));
            }

            $nomeServicoPrestado = $servicoPrestado->getNome();
            $codigoCliente = "Cód. do Cliente: $clienteId -> ";

            $existeServicoNaCompetencia = $competencia->hasParcelaPorServicoCliente($servicoCliente);
            if (!$existeServicoNaCompetencia) {
                //if ($forcarValor) {
                $cobrancas = BoletoQuery::create()
                        ->filterByVencimento($vencimentoCobranca)
                        ->filterByCompetencia($competencia)
                        ->filterByEstaPago(false)
                        ->filterByRegistrado(false)
                        ->filterByNotaScmGerada(false)
                        ->filterByNotaScmCancelada(false)
                        ->filterByNotaSvaGerada(false)
                        ->filterByNotaSvaCancelada(false)
                        ->findByClienteId($clienteId);

                $cobranca = null;
                if (!$cobrancaSeparada) {
                    foreach ($cobrancas as $cobrancaDisponivel) {
                        //if (!$cobrancaDisponivel->getRegistrado() && !$cobrancaDisponivel->getEstaPago() && (!$cobrancaDisponivel->getNotaScmGerada() || !$cobrancaDisponivel->getNotaSvaGerada())) {
                        $cobranca = $cobrancaDisponivel;
                        //break;
                        //}
                    }
                }

                $existeServicoNaCobranca = false;
                $podeGerarCobranca = true;
                if ($cobranca != null) {
                    $existeServicoNaCobranca = $cobranca->hasParcelaPorServicoCliente($servicoCliente);
                    $cobrancaEstaResgistrada = $cobranca->getRegistrado();
                    $cobrancaEstaPaga = $cobranca->getEstaPago();

                    if ($existeServicoNaCobranca && ($cobrancaEstaResgistrada || $cobrancaEstaPaga)) {
                        if ($cobrancaEstaResgistrada || $cobrancaEstaPaga) {
                            $podeGerarCobranca = false;
                            $mensagemEstaResgistrada = $codigoCliente . "Este serviço $servicoId: $nomeServicoPrestado já possui uma cobrança registrada.";
                            $mensagemEstaPaga = $codigoCliente . "Este serviço $servicoId: $nomeServicoPrestado já possui uma cobrança paga.";

                            $retornoView['erros'][] = $cobrancaEstaPaga ? $mensagemEstaPaga : $mensagemEstaResgistrada;
                        }
                    }
                }

                if ($cobranca == null || ($cobranca != null && !$existeServicoNaCobranca && !$podeGerarCobranca)) {
                    $cobranca = new Boleto();
                    $cobranca->setValor(0.0);
                    $cobranca->setVencimento($vencimentoCobranca);
                    $cobranca->setClienteId($clienteId);
                    $cobranca->setCompetencia($competencia);
                    $cobranca->setDataCadastro(new DateTime('now'));
                }

                if ($podeGerarCobranca) {
                    $cobranca->setDataAlterado(new DateTime('now'));
                    $cobranca->setUsuarioAlterado($user->getIdusuario());

                    $itemCobranca = $cobranca->getParcelaPorServicoCliente($servicoCliente);
                    if ($itemCobranca == null) {
                        $itemCobranca = new BoletoItem();
                        $itemCobranca->setServicoCliente($servicoCliente);
                        $itemCobranca->setDataCadastro(new DateTime('now'));
                    }

                    $baseCalculoScm = 0.0;
                    $temScm = $contrato->getTemScm();
                    if ($temScm) {
                        $tipoScm = $contrato->getTipoScm();
                        if ($tipoScm == 'valor') {
                            $baseCalculoScm += $contrato->getScm();
                        } else {
                            //$baseCalculoScm += ($contrato->getValor() * $contrato->getScm()) / 100;
                            $baseCalculoScm += ($valorContrato * $contrato->getScm()) / 100;
                        }
                    }

                    $baseCalculoSva = 0.0;
                    $temSva = $contrato->getTemSva();
                    if ($temSva) {
                        $tipoSva = $contrato->getTipoSva();
                        if ($tipoSva == 'valor') {
                            $baseCalculoSva += $contrato->getSva();
                        } else {
                            //$baseCalculoSva += ($contrato->getValor() * $contrato->getSva()) / 100;
                            $baseCalculoSva += ($valorContrato * $contrato->getSva()) / 100;
                        }
                    }

                    if (!$temScm && !$temSva) {
                        $baseCalculoScm = $valorContrato;
                    }

                    $itemCobranca->setBaseCalculoScm($baseCalculoScm);
                    $itemCobranca->setBaseCalculoSva($baseCalculoSva);
                    //$itemCobranca->setValor($valorContrato);
                    $itemCobranca->setValor($baseCalculoScm + $baseCalculoSva);

                    $itemCobranca->setDataAlterado(new DateTime('now'));
                    $itemCobranca->setUsuarioAlterado($user->getIdusuario());

                    $con = Propel::getWriteConnection(BoletoTableMap::DATABASE_NAME);
                    $con->beginTransaction();

                    try {
                        $cobranca->save();
                        $itemCobranca->setBoleto($cobranca);
                        $itemCobranca->save();

                        $valorCobranca = $cobranca->getValorTotalParcelas();
                        $cobranca->setValor($valorCobranca['valorTotal']);
                        $cobranca->setBaseCalculoScm($valorCobranca['valorTotalBaseCalculoScm']);
                        $cobranca->setBaseCalculoSva($valorCobranca['valorTotalBaseCalculoSva']);
                        //$cobranca->setValor($valorCobranca);
                        $cobranca->save();

                        if ($cobranca->getValor() > 0.0) {
                            $con->commit();

                            $retornoView['status'] = true;
                            $retornoView['mensagem'] = $codigoCliente . 'Cobrança gerada/atualizada com sucesso!';
                            $retornoView['logs'][] = $codigoCliente . 'Cobrança gerada/atualizada com sucesso!';
                        } else {

                            $retornoView['erros'][] = $codigoCliente . 'Cobrança não pode ser zerada!';
                            $retornoView['mensagem'] = $codigoCliente . 'Cobrança não pode ser zerada!';
                            //$retornoView['logs'][] = $codigoCliente . 'Cobrança não pode ser zerada! '.$valorContrato;

                            $con->rollBack();
                        }
                    } catch (PropelException $ex) {
                        $con->rollBack();
                        $retornoView['erros'][] = $codigoCliente . $ex->getMessage();
                    }
                }
            } else {
                $retornoView['erros'][] = $codigoCliente . "Este serviço $servicoId: $nomeServicoPrestado já possui uma cobrança nesta Competência.";
            }
        }

        if (sizeof($retornoView['logs']) == 0) {
            $retornoView['logs'][] = 'Não foi encontrado nenhuma cobrança para a geração.';
        }

        $retornoView['logs'] = array_merge($retornoView['logs'], $retornoView['erros']);

        return $retornoView;
    }

    public function getDadosBancarioSantander($bancoId) {
        $sqlDadosCobranca = "select b.nome as nome_banco, b.codigo_febraban, bac.numero_conta as numero_conta_corrente,
bac.digito_verificador as digito_verificador_conta_corrente,
ba.agencia as banco_agencia, ba.digito_verificador as banco_agencia_digito_verificador,
p.razao_social as nome_empresa, p.tipo_pessoa, p.cpf_cnpj, c.idconvenio, c.ultimo_sequencial_arquivo_cobranca,
c.codigo_convenio

from banco b, banco_agencia ba, banco_agencia_conta bac, convenio c, empresa e, pessoa p

where b.idbanco = bac.banco_id and bac.banco_agencia_id = ba.idbanco_agencia
and c.idconvenio = bac.convenio_id and e.idempresa = c.empresa_id and p.id = e.pessoa_id and b.idbanco = $bancoId";

        $con = Propel::getConnection();
        $stmtDadosCobranca = $con->prepare($sqlDadosCobranca);
        $stmtDadosCobranca->execute();
        $dadosCobranca = $stmtDadosCobranca->fetchall();

        if (sizeof($dadosCobranca))
            return $dadosCobranca[0];

        return $dadosCobranca;
    }

    public function getDadosNotaPrefeitura() {
        $config = array();
        $config['ambiente'] = $this->getParameter('nfse_ambiente');
        $config['empresa'] = $this->getParameter('nfse_empresa_nome');
        $config['UF'] = $this->getParameter('nfse_empresa_uf');
        $config['cnpj'] = $this->getParameter('nfse_empresa_cnpj');
        $config['certsDir'] = $this->getParameter('nfse_diretorio_certificado');
        $config['certName'] = $this->getParameter('nfse_nome_certificado');
        $config['keyPass'] = $this->getParameter('nfse_senha_certificado');
        $config['passPhrase'] = $this->getParameter('nfse_senha_decriptacao_chave');
        $config['arquivosDir'] = $this->getParameter('nfse_repositorio');
        $config['arquivoURLxml'] = $this->getParameter('nfse_url_xml');
        $config['baseurl'] = $this->getParameter('nfse_base_url');
        $config['danfeLogo'] = $this->getParameter('nfse_danfe_logo');
        $config['danfeLogoPos'] = $this->getParameter('nfse_danfe_logo_pos');
        $config['danfeFormato'] = $this->getParameter('nfse_danfe_formato');
        $config['danfePapel'] = $this->getParameter('nfse_danfe_papel');
        $config['danfeCanhoto'] = $this->getParameter('nfse_danfe_canhoto');
        $config['danfeFonte'] = $this->getParameter('nfse_danfe_fonte');
        $config['danfePrinter'] = $this->getParameter('nfse_danfe_printer');
        $config['schemes'] = $this->getParameter('nfse_schemes');
        $config['proxyIP'] = $this->getParameter('nfse_proxy_ip');
        $config['mailFROM'] = $this->getParameter('nfse_mail_from');

        return $config;
    }

    public function enviarNotaSva($cobrancas, $dataEmissao) {
        $config = $this->getDadosNotaPrefeitura();
        $nfse = new NFSE($config);

        $user = $this->getUser();
        $competencia = $this->getCompetenciaAtual();
        $empresa = EmpresaQuery::create()->findOne();

        // pegar futuramente os dados do cadastro da empresa, e não de parametros
        // informações do Emitente
        $razaoSocialEmpresa = $this->getParameter('nfse_empresa_nome');
        $nomeFantasiaEmpresa = $this->getParameter('nfse_empresa_nome');
        $cnpjEmpresa = $this->getParameter('nfse_empresa_cnpj');
        $inscricaoMunicipalEmpresa = $this->getParameter('nfse_empresa_inscricao_municipal');

        $dataDaEmissao = DateTime::createFromFormat('Y-m-d', $dataEmissao);
        $dataEmitido = new DateTime('now');

        // Dados para o envio de lotes
        $lotes = array();
        $idLote = $empresa->getUltimoLoteEnviado(); //0;
        $idRps = $empresa->getUltimoRpsEnviado(); //12;
        $NOTAS_POR_LOTE = $empresa->getNotasPorLote(); //12;
        $count = 0;

        $retornoView = array();
        $retornoView['status'] = true;
        $retornoView['erros'] = array();

        foreach ($cobrancas as $cobranca) {
            // Dados da Nota
            /*
              $naturezaOperacao = '1';
              $aliquota = 2;
              $itemListaServico = '1.07';
              $codigoTributacaoMunicipio = '619060100'; */

            $valorNota = $cobranca->getValor();
            $vencimento = $cobranca->getVencimento();

            // Dados do Destinatário
            $cliente = $cobranca->getCliente();
            $pessoa = $cliente->getPessoa();
            $endereco = $cliente->getEnderecoCliente();
            $cidade = $endereco->getCidade();

            $codigoCliente = $cliente->getIdcliente();
            $tipoPessoaCliente = $pessoa->getTipoPessoa();
            $cpfCnpjCliente = $pessoa->getCpfCnpj();
            $nomeCliente = utf8_decode($pessoa->getNome());
            $razaoSocialCliente = utf8_decode($pessoa->getRazaoSocial());
            $emailCliente = $pessoa->getEmail();
            $ruaCliente = utf8_decode($endereco->getRua());
            $numeroCliente = $endereco->getNum();
            $bairroCliente = utf8_decode($endereco->getBairro());
            $complementoCliente = utf8_decode($endereco->getComplemento());
            $cidadeCliente = utf8_decode($cidade->getMunicipio());
            $codigoMunicipioCliente = $cidade->getCodigo();
            $ufCliente = $cidade->getUf();
            $cepCliente = str_replace(['.', '-'], ['', ''], $endereco->getCep());
            $telefoneCliente = '';

            //$nota = new DadosFiscal();
            $nota = DadosFiscalQuery::create()->filterByTipoNota('sva')->findOneByBoletoId($cobranca->getIdboleto());
            if ($nota == null) {
                $nota = new DadosFiscal();
                $nota->setTipoNota('sva');
                $nota->setDataCadastro($dataEmitido);
            }

            $nota->setBoleto($cobranca);
            $nota->setCompetencia($competencia);
            $nota->setDataEmissao($dataDaEmissao);
            $nota->setDataEmitido($dataEmitido);
            $nota->setSituacao('emitida');
            $nota->setValorNota($valorNota);
            $nota->setBaseCalculo(0);
            $nota->setValorIcms(0);
            $nota->setNTributo(0);
            $nota->setPessoa($pessoa);
            $nota->setDataAlterado($dataEmitido);
            $nota->setUsuarioAlterado($user->getIdusuario());

            $discriminacao = "Código do Cliente: $codigoCliente\\n\\n";
            $itensFiscais = array();
            $itensCobranca = $cobranca->getBoletoItems();
            $countItemCobranca = 1;
            foreach ($itensCobranca as $itemCobranca) {
                $valorItem = $itemCobranca->getValor();
                $valorItemSva = $itemCobranca->getBaseCalculoSva();
                $servicoCliente = $itemCobranca->getServicoCliente();
                $contrato = $servicoCliente->getContrato();
                $servicoPrestado = $servicoCliente->getServicoPrestado();
                $plano = $servicoPrestado->getPlano();

                $codigoObra = $contrato->getCodigoObra();
                $art = $contrato->getArt();
                $tipoServicoPrestado = $servicoPrestado->getTipoServicoPrestado();
                $tipo = $tipoServicoPrestado->getTipo();
                $naturezaOperacao = $tipoServicoPrestado->getNaturezaOperacao();
                $aliquota = $tipoServicoPrestado->getAliquota();
                $itemListaServico = $tipoServicoPrestado->getItemListaServico();
                $codigoTributacaoMunicipio = $tipoServicoPrestado->getCodigoTributacao();

                $codigoServico = str_pad($servicoCliente->getIdservicoCliente(), 12, '0', STR_PAD_LEFT);
                $nomeServico = $plano->getNome();
                $discriminacao .= "$codigoServico - $nomeServico\\n";

                if ($tipo != '3' && $tipo != '4') {
                    $tipoPeriodoReferencia = $servicoCliente->getTipoPeriodoReferencia();
                    if ($tipoPeriodoReferencia == 'corrente') {
                        $periodoInicial = $competencia->getPeriodoInicial();
                        $periodoFinal = $competencia->getPeriodoFinal();
                    } elseif ($tipoPeriodoReferencia == 'posterior') {
                        $periodoInicial = $competencia->getPeriodoInicial()->modify('+1 month');
                        $periodoFinal = $competencia->getPeriodoFinal()->modify('+1 month');
                    } else {
                        $periodoInicial = $competencia->getPeriodoInicial()->modify('-1 month');
                        $periodoFinal = $competencia->getPeriodoFinal()->modify('-1 month');
                    }

                    $dataInicial = $periodoInicial->format('d/m/Y');
                    $dataFinal = $periodoFinal->format('d/m/Y');

                    $discriminacao .= "Período de Referência: $dataInicial à $dataFinal\\n";
                    //$discriminacao .= date('d/m/Y', strtotime($servico['periodo_inicial'])) . ' à ' . date('d/m/Y', strtotime($servico['periodo_final'])) . "\\n";
                }

                $discriminacao .= 'Vencimento: ' . $vencimento->format('d/m/Y') . "\\n\\n";

                $dadoFiscalItem = new DadosFiscalItem();
                $dadoFiscalItem->setBoleto($cobranca);
                $dadoFiscalItem->setBoletoItem($itemCobranca);
                $dadoFiscalItem->setDataCadastro($dataEmitido);
                $dadoFiscalItem->setDataAlterado($dataEmitido);
                $dadoFiscalItem->setNumeroItemNotaFiscal($countItemCobranca);
                $dadoFiscalItem->setPosicaoItem(0);
                $dadoFiscalItem->setValorItem($valorItem);
                $dadoFiscalItem->setValorSva($valorItemSva);
                $dadoFiscalItem->setTemSva(true);
                //$valorTotalItens += $valorItem;

                $itensFiscais[] = $dadoFiscalItem;

                $countItemCobranca++;
            }

            $idRps++;
            $oNF = new \NFSePHPGinfesData($idRps);
            $oNF->set('razaoSocial', $razaoSocialEmpresa);
            $oNF->set('nomeFantasia', $nomeFantasiaEmpresa);
            $oNF->set('CNPJ', $cnpjEmpresa);
            $oNF->set('IM', $inscricaoMunicipalEmpresa);

            $oNF->set('tipo', '1');
            $oNF->set('natOperacao', $naturezaOperacao);
            $oNF->set('optanteSimplesNacional', '2');
            $oNF->set('incentivadorCultural', '2');
            $oNF->set('regimeEspecialTributacao', '2');
            $oNF->set('status', '1');
            $oNF->set('cMun', '3301009');

            $oNF->set('DataEmissao', $dataEmissao);

            $oNF->setItem('valorServicos', $valorNota);
            $oNF->setItem('valorDeducoes', 0);
            $oNF->setItem('valorPis', 0);
            $oNF->setItem('valorCofins', 0);
            $oNF->setItem('valorInss', 0);
            $oNF->setItem('valorIr', 0);
            $oNF->setItem('valorCsll', 0);

            $issRetido = '1';
            $valorIssRetido = 0.0;
            $valorIss = 0.0;
            if ($tipoPessoaCliente == 'juridica') {
                if ($naturezaOperacao != '4') {
                    //$oNF->setItem('valorIss', $nota['valorNota']*$nota['aliquota'] / 100);
                    $valorIss = $valorNota * $aliquota / 100;
                    $valorIssRetido = $valorIss;
                }
            } else {
                $issRetido = '2';
            }

            $oNF->setItem('issRetido', $issRetido);
            $oNF->setItem('valorIss', $valorIss);
            $oNF->setItem('valorIssRetido', $valorIssRetido);
            $oNF->setItem('outrasRetencoes', 0);
            $oNF->setItem('aliquota', $aliquota / 100);
            $oNF->setItem('descontoIncondicionado', 0);
            $oNF->setItem('descontoCondicionado', 0);

            //$oNF->setItem('ValorLiquidoNfse', $nota['valorNota']-$valorIssRetido);

            $oNF->setItem('itemListaServico', $itemListaServico);
            $oNF->setItem('codigoTributacaoMunicipio', $codigoTributacaoMunicipio);
            $oNF->setItem('discriminacao', $discriminacao);
            $aux = $idLote + 1;

            if ($tipoPessoaCliente == 'fisica') {
                $oNF->set('tomaCPF', $cpfCnpjCliente);
                $oNF->set('tomaRazaoSocial', $nomeCliente);

                if (!Utils::validarCPF($cpfCnpjCliente)) {
                    $retornoView['erros'][] = "Lote $aux - $nomeCliente - CPF Inválido\n\n";
                }
            } else {
                $oNF->set('tomaCNPJ', $cpfCnpjCliente);
                $oNF->set('tomaRazaoSocial', $razaoSocialCliente);

                if (!Utils::validarCNPJ($cpfCnpjCliente)) {
                    $retornoView['erros'][] = "Lote $aux - $nomeCliente - CNPJ Inválido\n\n";
                }
            }

            $oNF->set('tomaEndLogradouro', $ruaCliente);

            if (!empty($numeroCliente)) {
                $oNF->set('tomaEndNumero', $numeroCliente);
            } else {
                $oNF->set('tomaEndNumero', 's/n');
            }

            $oNF->set('tomaEndComplemento', $complementoCliente);
            $oNF->set('tomaEndBairro', $bairroCliente);
            $oNF->set('tomaEndxMun', $cidadeCliente);
            $oNF->set('tomaEndcMun', $codigoMunicipioCliente);
            $oNF->set('tomaEndUF', $ufCliente);
            $oNF->set('tomaEndCep', $cepCliente);
            $oNF->set('tomaTelefone', $telefoneCliente);

            if ($tipoPessoaCliente == 'juridica') {
                $oNF->set('tomaEmail', $emailCliente);
            } else {
                $oNF->set('tomaEmail', 'notafiscalpf@censanet.com.br');
            }

            $oNF->set('tomaCodigoObra', $codigoObra);
            $oNF->set('tomaArt', $art);

            $oNF->set('tomaIM', '');

            $lotes[] = $oNF;

            $nota->setNumeroRps($idRps);
            $empresa->setUltimoRpsEnviado($idRps);
            $count += 1;
            if ($count % $NOTAS_POR_LOTE == 0) {
                $idLote++;
                $retorno = $this->enviarNotaSvaMontada($idLote, $lotes, $empresa, $nfse, $nota, $itensFiscais, $cobranca, $nomeCliente);
                if (!$retorno['status']) {
                    $retornoView['status'] = $retorno['status'];
                    $retornoView['erros'][] = array_merge($retornoView['erros'], $retorno['erros']);
                }

                $lotes = array();
            }
        }

        if (sizeof($lotes)) {
            $idLote++;
            $retorno = $this->enviarNotaSvaMontada($idLote, $lotes, $empresa, $nfse, $nota, $itensFiscais, $cobranca, $nomeCliente);
            if (!$retorno['status']) {
                $retornoView['status'] = $retorno['status'];
                $retornoView['erros'][] = array_merge($retornoView['erros'], $retorno['erros']);
            }
        }

        if (sizeof($retornoView['erros'])) {
            $retornoView['status'] = false;
        }

        return $retornoView;
    }

    private function enviarNotaSvaMontada($idLote, $lotes, $empresa, $nfse, $nota, $itensFiscais, $cobranca, $nomeCliente = '') {
        $retorno = array();
        $retorno['status'] = false;
        $retorno['erros'] = array();

        $empresa->setUltimoLoteEnviado($idLote);

        $con = Propel::getWriteConnection(EmpresaTableMap::DATABASE_NAME);
        $con->beginTransaction();

        try {
            $empresa->save();

            $xmlLote = $nfse->montarLoteRps($idLote, $lotes);
            $retornoEnvio = $nfse->enviarLoteRps($xmlLote);
            if (!$retornoEnvio) {
                $con->rollBack();
                $retorno['erros'][] = "Lote $idLote - " . $nomeCliente . ' - ' . $nfse->getErro() . "\n\n";
                $nfse->limparErro();
            } else {
                $doc = new \DOMDocument();
                $doc->formatOutput = FALSE;
                $doc->preserveWhiteSpace = FALSE;
                $doc->loadXML($retornoEnvio, LIBXML_NOBLANKS | LIBXML_NOEMPTYTAG);
                $protocolo = $doc->getElementsByTagName("Protocolo")->item(0)->nodeValue;

                $nota->setProtocolo($protocolo);

                $nota->setUltimaNotaGerada(true);
                $nota->save();

                foreach ($itensFiscais as $itemFiscal) {
                    $itemFiscal->setDadosFiscal($nota);
                    $itemFiscal->save();
                }

                $cobranca->setNotaSvaGerada(true);
                $cobranca->save();

                $con->commit();

                $retorno['status'] = true;
            }
        } catch (PropelException $e) {
            $con->rollBack();

            $retorno['erros'][] = $e->getMessage();
        }

        return $retorno;
    }

    public function verificarNotaSva($notas) {
        $cnpjEmpresa = $this->getParameter('nfse_empresa_cnpj');
        $inscricaoMunicipalEmpresa = $this->getParameter('nfse_empresa_inscricao_municipal');

        //$protocolos = array(array(13, '7720679'));
        $config = $this->getDadosNotaPrefeitura();
        $msg = '';
        $nfse = new NFSE($config);
        $filename = 'tmp/verificacao_notas_sva_log_' . time() . '.txt';

        $retornoView = array();
        $retornoView['status'] = true;
        $retornoView['erros'] = array();
        $retornoView['log'] = $filename;

        $arquivo = fopen($filename, "w");
        fwrite($arquivo, '***************************** Log da Verificação de Notas SVA *****************************' . PHP_EOL . PHP_EOL);
        foreach ($notas as $nota) {
            $msg = 'Nota: ' . $nota->getId();

            $numeroRps = $nota->getNumeroRps();
            $retorno = $nfse->consultarNfseRps($numeroRps, 1, 1, $cnpjEmpresa, $inscricaoMunicipalEmpresa);
            if (!$retorno['status']) {
                //$msg .= utf8_decode($nfse->getErro()) . "\n\n";
                $msg .= $nfse->getErro();

                $retornoView['status'] = false;
                $retornoView['erros'][] = $msg;

                if (!(strpos($msg, 'retorno do SOAP') !== false)) {
                    $nota->setSituacao('reprovada');
                    $cobranca = $nota->getBoleto();
                    $cobranca->setNotaSvaGerada(false);
                    $cobranca->save();
                }
            } else {
                $msg .= ' - Nota verificada com sucesso.' . PHP_EOL . PHP_EOL;

                $nota->setNumeroNotaFiscal($retorno['numeroNota']);
                $nota->setSituacao('aprovada');
            }

            $nota->save();

            fwrite($arquivo, $msg);
        }

        fclose($arquivo);

        $retornoView['log'] = $filename;

        return $retornoView;
    }

    public function visualizarNotaSva($nota) {
        $config = $this->getDadosNotaPrefeitura();
        $nfse = new NFSE($config);
        //$name = sprintf("%015s", $numRps) . "-" . sprintf("%08d", $nota);
        $diretorio = $nfse->getDiretorioNfse();
        //$name = 'notaDownload';
        $retorno = '';
        $notaId = $nota->getNumeroNotaFiscal();
        $cnpjEmpresa = $this->getParameter('nfse_empresa_cnpj');
        $inscricaoMunicipalEmpresa = $this->getParameter('nfse_empresa_inscricao_municipal');
        $name = sprintf("%08d", $notaId);

        if (!file_exists($diretorio . "aprovadas/{$name}.xml")) {
            //$retorno = $nfse->consultarNfseRps($numRps, 1, 1, $this->cnpj, $this->im);
            $retorno = $nfse->consultarNfse($notaId, $cnpjEmpresa, $inscricaoMunicipalEmpresa);
            if (!$retorno) {
                return array('status' => false, 'erros' => array("Falha ao visualzar a nota " . utf8_decode($nfse->getErro())));
            } else {
                return $nfse->gerarPDF($diretorio . "aprovadas/{$name}.xml");
            }
        } else {
            return $nfse->gerarPDF($diretorio . "aprovadas/{$name}.xml");
        }
    }

    public function geracaoCobrancasContaAPagarPorFornecedor($dados) {
        $user = $this->getUser();

        $retorno = array();
        $retorno['status'] = false;
        $retorno['logs'] = array();

        $con = Propel::getWriteConnection(BoletoTableMap::DATABASE_NAME);
        $con->beginTransaction();

        $fornecedor = $dados['fornecedor'];
        $cliente = $fornecedor->getCliente();
        $pessoa = $cliente->getPessoa();

        $cobranca = new Boleto();
        $cobranca->setValor($dados['valor']);
        $cobranca->setValorOriginal($dados['valor']);
        $cobranca->setValorJuros(0);
        $cobranca->setValorMulta(0);
        $cobranca->setValorDesconto(0);
        $cobranca->setValorPago(0);
        $cobranca->setVencimento($dados['vencimento']);
        $cobranca->setCliente($cliente);
        $cobranca->setCompetencia($dados['competencia']);
        $cobranca->setFornecedor($fornecedor);
        $cobranca->setTipoConta('a_pagar');
        $cobranca->setDataCadastro(new DateTime('now'));
        $cobranca->setDataAlterado(new DateTime('now'));
        $cobranca->setUsuario($user);

        $valorTotal = $dados['valor'];

        $historicoCobranca = new BoletoBaixaHistorico();
        $historicoCobranca->setBoleto($cobranca);
        $historicoCobranca->setLancamentoId(null);
        $historicoCobranca->setPessoa($pessoa);
        $historicoCobranca->setCompetencia($dados['competencia']);
        $historicoCobranca->setStatus('em_aberto');
        $historicoCobranca->setValorOriginal($cobranca->getValorOriginal());
        $historicoCobranca->setValorMulta($cobranca->getValorMulta());
        $historicoCobranca->setValorJuros($cobranca->getValorJuros());
        $historicoCobranca->setValorDesconto($cobranca->getValorDesconto());
        $historicoCobranca->setValorTotal($valorTotal);
        $historicoCobranca->setDataVencimento($cobranca->getVencimento());
        $historicoCobranca->setDescricaoMovimento($dados['descricaoMovimento']);
        $historicoCobranca->setDataCadastro(new DateTime('now'));
        $historicoCobranca->setDataAlterado(new DateTime('now'));
        $historicoCobranca->setUsuarioAlterado($user->getIdusuario());

        $fornecedorNome = 'Fornecedor ' . $pessoa->getRazaoSocial() . ' - ';
        try {
            $cobranca->save();
            $historicoCobranca->save();

            $con->commit();
            $retorno['status'] = true;
            $retorno['logs'][] = $fornecedorNome . 'Cobrança gerada com sucesso.';
        } catch (PropelException $ex) {
            $con->rollBack();
            $retorno['logs'][] = $fornecedorNome . 'Não foi possível gerar a Cobrança/Histórico.';
            $retorno['logs'][] = $ex->getMessage();
        }

        return $retorno;
    }

}
