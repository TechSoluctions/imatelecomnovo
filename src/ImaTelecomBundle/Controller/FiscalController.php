<?php

namespace ImaTelecomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Propel\Runtime\Propel;
use ImaTelecomBundle\Model\BoletoQuery;
use ImaTelecomBundle\Model\BoletoItemQuery;
use ImaTelecomBundle\Model\CompetenciaQuery;
use ImaTelecomBundle\Model\DadosFiscalQuery;
use ImaTelecomBundle\Model\Sintegra;
use ImaTelecomBundle\Model\SintegraQuery;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Propel\Runtime\ActiveQuery\Criteria;
use ImaTelecomBundle\Lib\ExtendedFPDF;
use \DateTime;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use ImaTelecomBundle\Lib\NFSE\NFSE;
use ImaTelecomBundle\Lib\Utils;

class FiscalController extends AdminController {

    public function indexAction(Request $request) {
        return $this->render('ImaTelecomBundle:Admin:index.html.twig');
    }

    public function notasFiscaisScmNaoGeradasAction(Request $request, $competenciaId = 0) {
        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        if ($competencia == null) {
            $competencia = $this->getCompetenciaAtual();
        }

        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();
        $cobrancas = BoletoQuery::create()
                ->withColumn('pessoa.nome', 'nome')
                ->withColumn('pessoa.tipo_pessoa', 'tipoPessoa')
                ->useClienteQuery()->joinPessoa()->endUse()
                ->filterByCompetencia($competencia)
                ->where('base_calculo_scm > 0')
                ->find();

        $notas = array();
        foreach ($cobrancas as $cobranca) {
            $cobrancaNotaScmGerada = $cobranca->getNotaScmGerada();
            $cobrancaNotaScmCancelada = $cobranca->getNotaScmCancelada();

            if (!$cobrancaNotaScmGerada && !$cobrancaNotaScmCancelada) {
                $notas[] = $cobranca;
            }
        }

        $retornoView = array();
        $retornoView['notas'] = $notas;
        $retornoView['competencias'] = $competencias;
        $retornoView['competenciaAtual'] = $competencia;

        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-scm-notas-nao-geradas.html.twig', $retornoView);
    }

    public function notasFiscaisScmNotasNaoGeradasGeracaoAction(Request $request) {
        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();
        $competencia = $this->getCompetenciaAtual();

        $retornoView = array();
        $retornoView['competencias'] = $competencias;
        $retornoView['competenciaAtual'] = $competencia;

        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-scm-notas-nao-geradas-geracao.html.twig', $retornoView);
    }

    public function notasFiscaisScmNotasNaoGeradasGeracaoGerarAction(Request $request) {
        $tipoGeracao = $request->get('tipoGeracao', 'ambos');
        $competenciaId = $request->get('competencia', '');
        $emissao = $request->get('dataEmissao', '');

        if (empty($emissao)) {
            $competencia = $this->getCompetenciaAtual();
            $dataEmissao = $competencia->getUltimaDataEmissao();
        } else {
            $dataDaEmissao = DateTime::createFromFormat('d/m/Y', $emissao);
            $dataEmissao = $dataDaEmissao->format('Y-m-d');
        }

        $cobrancas = BoletoQuery::create()
                //->joinBoletoItem()
                ->filterByCompetenciaId($competenciaId);

        if ($tipoGeracao != 'ambos') {
            $cobrancas->useClienteQuery()->joinPessoa()->endUse()->where("tipo_pessoa = '$tipoGeracao'");
        }

        $cobrancas->find();

        $notas = array();
        foreach ($cobrancas as $cobranca) {
            $cobrancaNotaScmGerada = $cobranca->getNotaScmGerada();
            $cobrancaNotaScmCancelada = $cobranca->getNotaScmCancelada();

            if (!$cobrancaNotaScmGerada && !$cobrancaNotaScmCancelada) {
                $notas[] = $cobranca;
            }
        }

        // verificar data de emissão se não existir 
        // e pegar a ultima data de emissão da competencia, para casos em que seja apenas regeração da nota
        $situacao = 'N';
        $retorno = $this->criarNotaFiscal($notas, $dataEmissao, $situacao);

        $retornoView = array();
        $retornoView['status'] = $retorno['status'];
        $retornoView['erros'] = $retorno['erros'];

        if ($retornoView['status']) {
            $retornoView['mensagem'] = "Nota gerada com sucesso.";
        }

        return new JsonResponse($retornoView);
    }

    public function notasFiscaisScmNotasNaoGeradasGeracaoNotaAction(Request $request, $cobrancaId) {
        $retornoView = array();

        $competencia = $this->getCompetenciaAtual();
        $cobranca = BoletoQuery::create()->findPk($cobrancaId);
        $itemsCobranca = $cobranca->getBoletoItems();

        $retornoView['cobranca'] = $cobranca;
        $retornoView['itemsCobranca'] = $itemsCobranca;
        $retornoView['competencia'] = $competencia;

        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-scm-notas-nao-geradas-geracao-nota.html.twig', $retornoView);
    }

    public function notasFiscaisScmNotasNaoGeradasGeracaoNotaGerarAction(Request $request, $cobrancaId) {
        $cobranca = BoletoQuery::create()
                //->joinBoletoItem()
                ->where('base_calculo_scm > 0')
                ->findPk($cobrancaId);

        if ($cobranca == null) {
            return new JsonResponse(array('status' => false, 'erros' => array('Não foi possível localizar a cobrança para a geração da Nota SCM. Por favor, verifique se a mesma existe e se é do tipo SCM!')));
        }

        $cobrancaNotaScmGerada = $cobranca->getNotaScmGerada();
        $cobrancaNotaScmCancelada = $cobranca->getNotaScmCancelada();

        $notas = array();
        if (!$cobrancaNotaScmGerada && !$cobrancaNotaScmCancelada) {
            $notas[] = $cobranca;
        }

        $emissao = $request->get('dataEmissao', '');
        if (empty($emissao)) {
            $competencia = $this->getCompetenciaAtual();
            $dataEmissao = $competencia->getUltimaDataEmissao();
        } else {
            $dataDaEmissao = DateTime::createFromFormat('d/m/Y', $emissao);
            $dataEmissao = $dataDaEmissao->format('Y-m-d');
        }

        $situacao = 'N';
        $retorno = $this->criarNotaFiscal($notas, $dataEmissao, $situacao);

        $retornoView = array();
        $retornoView['status'] = $retorno['status'];
        $retornoView['erros'] = $retorno['erros'];

        if ($retornoView['status']) {
            $retornoView['mensagem'] = "Nota gerada com sucesso.";
        }

        return new JsonResponse($retornoView);
    }

    /*
      public function notasFiscaisGeracaoNotaGerarNotaPorCompetenciaAction(Request $request, $competenciaId) {
      $tipoGeracao = $request->get('tipoGeracao', 'ambos');
      $emissao = $request->get('dataEmissao', '');
      if (empty($emissao)) {
      $competencia = $this->getCompetenciaAtual();
      $dataEmissao = $competencia->getUltimaDataEmissao();
      } else {
      $dataDaEmissao = DateTime::createFromFormat('d/m/Y', $emissao);
      $dataEmissao = $dataDaEmissao->format('Y-m-d');
      }

      $cobrancas = BoletoQuery::create()
      ->joinBoletoItem()
      ->filterByCompetenciaId($competenciaId);

      if ($tipoGeracao != 'ambos') {
      $cobrancas->useClienteQuery()->joinPessoa()->endUse()->where("tipo_pessoa = '$tipoGeracao'");
      }

      $cobrancas->find();

      $notas = array();
      foreach ($cobrancas as $cobranca) {
      $cobrancaNotaGerada = $cobranca->getNotaGerada();
      $cobrancaNotaCancelada = $cobranca->getNotaCancelada();

      if (!$cobrancaNotaGerada && !$cobrancaNotaCancelada) {
      $notas[] = $cobranca;
      }
      }

      // verificar data de emissão se não existir
      // e pegar a ultima data de emissão da competencia, para casos em que seja apenas regeração da nota
      $situacao = 'N';
      $retorno = $this->criarNotaFiscal($notas, $dataEmissao, $situacao);

      $retornoView = array();
      $retornoView['status'] = $retorno['status'];
      $retornoView['erros'] = $retorno['erros'];
      $retornoView['mensagem'] = "Nota gerada com sucesso.";

      return new JsonResponse($retornoView);
      } */
    /*
      public function notasFiscaisCancelamentoNotaAction(Request $request, $notaId) {
      $retornoView = array();

      //$competencia = $this->getCompetenciaAtual();
      $nota = DadosFiscalQuery::create()->findPk($notaId);
      //$itemsNota = $nota->getBoletoItems();

      $retornoView['nota'] = $nota;
      //$retornoView['itemsNota'] = $itemsNota;
      //$retornoView['competencia'] = $competencia;
      return $this->render('ImaTelecomBundle:Fiscal:notas_fiscais_cancelamento_notas.html.twig', $retornoView);
      } */

    public function notasFiscaisScmNotasGeradasCancelamentoNotaCancelarNotaAction(Request $request, $notaId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $nota = DadosFiscalQuery::create()->findPk($notaId);
        $tipoNota = $nota->getTipoNota();
        if ($tipoNota == 'scm') {
            $cobranca = $nota->getBoleto();

            //$retornoView['geracao'] = array();
            //$cobrancaEstaRegistrada = $cobranca->getRegistrado();
            $cobrancaNotaScmGerada = $cobranca->getNotaScmGerada();
            $cobrancaNotaScmCancelada = $cobranca->getNotaScmCancelada();

            $cobrancasGeracao = array();
            if ($cobrancaNotaScmGerada && !$cobrancaNotaScmCancelada) {
                $cobrancasGeracao[] = $cobranca;
            }

            if (sizeof($cobrancasGeracao)) {
                $competencia = $this->getCompetenciaAtual();
                $dataEmissao = $competencia->getUltimaDataEmissao();
                $situacao = 'S';
                $retorno = $this->criarNotaFiscal($cobrancasGeracao, $dataEmissao, $situacao);
                $retornoView['status'] = $retorno['status'];
                $retornoView['erros'] = $retorno['erros'];
            } else {
                $retornoView['erros'][] = 'Não foi possível cancelar esta nota, pois a mesma já se encontra cancelada.';
            }

            if ($retornoView['status']) {
                $retornoView['mensagem'] = "Nota Cancelada com sucesso.";
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_fiscal_notas_fiscais_scm_notas_geradas');
            }
        } else {
            $retornoView['erros'][] = 'Não foi possível cancelar esta nota, pois a mesma não é do tipo SCM.';
        }

        return new JsonResponse($retornoView);
    }

    public function notasFiscaisScmNotasNaoGeradasItemNotaAction(Request $request, $boletoId) {
        $cobranca = BoletoQuery::create()->findPk($boletoId);
        $retornoView = array();
        $retornoView['cobranca'] = $cobranca;

        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-scm-notas-nao-geradas-item-nota.html.twig', $retornoView);
    }

    public function sintegrasAction(Request $request) {
        $sintegras = SintegraQuery::create()->find();

        $retornoView = array();
        $retornoView['sintegras'] = $sintegras;
        return $this->render('ImaTelecomBundle:Fiscal:sintegras.html.twig', $retornoView);
    }

    public function sintegrasDownloadAction(Request $request, $sintegraId) {
        $sintegra = SintegraQuery::create()->findPk($sintegraId);
        $documento = $sintegra->getDocumento();
        $filename = 'tmp/sintegra_' . time() . '.txt';

        $arquivoSintegra = fopen($filename, "w");
        fwrite($arquivoSintegra, $documento);
        fclose($arquivoSintegra);

        // Generate response
        $response = new Response();

        // Set headers
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($filename));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
        $response->headers->set('Content-length', filesize($filename));

        // Send headers before outputting anything
        $response->sendHeaders();
        $response->setContent(file_get_contents($filename));

        return $response;
    }

    public function sintegrasExcluirAction(Request $request, $sintegraId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $sintegra = SintegraQuery::create()->findPk($sintegraId);
        if ($sintegra != null) {
            try {
                $sintegra->delete();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = "Arquivo Sintegra excluído com sucesso.";
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_fiscal_sintegras');
            } catch (PropelException $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        } else {
            $retornoView['erros'][] = "Não foi possível localizar este registro.";
        }

        return new JsonResponse($retornoView);
    }

    private function gerarDocumentoSintegra($competencia) {
        $notas = DadosFiscalQuery::create()
                ->withColumn('tipo_pessoa', 'tipoPessoa')->withColumn('cpf_cnpj', 'cpfCnpj')
                ->withColumn('rg_inscrest', 'rgInscrest')->withColumn('pessoa.uf', 'pessoaUf')
                ->useBoletoQuery()->useClienteQuery()->usePessoaQuery()->endUse()->endUse()->endUse()
                ->findByCompetenciaId($competencia->getId());

        $totalLinhas = 0;

        $periodoInicial = str_replace('-', '', $competencia->getPeriodoInicial()->format('Y-m-d')); //str_replace("-", "", date_format($competencia->getPeriodoInicial(), "Y-m-d"));
        $periodoFinal = str_replace('-', '', $competencia->getPeriodoFinal()->format('Y-m-d')); //str_replace("-", "", date_format($competencia->getPeriodoFinal(), "Y-m-d"));
        $QUEBRA_LINHA = "\r\n";

        $header = "";
        $header .= "101093427300016778829257      IMA TELECOM LTDA - ME              Campos dos Goytacazes         RJ0000000000$periodoInicial" . "$periodoFinal" . "331$QUEBRA_LINHA";
        $header .= "11Rua Salvador Corres               00139                      Centro         28035310IMA TELECOM LTDA - ME       222726274700$QUEBRA_LINHA";
        $totalLinhas += 2;

        $body = "";
        $registro76 = '';
        $totalRegistro76 = 0;

        $registro77 = '';
        $totalRegistro77 = 0;
        foreach ($notas as $nota) {
            /* $cliente = $nota->cliente();
              $pessoa = $cliente->getPessoa();
              $cidade = $pessoa->cidade(); */

            $tipoPessoa = $nota->getTipoPessoa();
            $cpfCnpj = $nota->getCpfCnpj();
            $rg = trim($nota->getRgInscrest());
            $dataDaEmissao = $nota->getDataEmissao()->format('Y-m-d');
            $dataEmissao = str_replace("-", "", $dataDaEmissao);
            $numeroNotaFiscal = $nota->getNumeroNotaFiscal();
            $codigoNotaFiscalLotePrevio = $nota->getCodNotaFiscalLotePrevio();
            $valorNota = str_replace([',', '.'], ['', ''], number_format($nota->getValorNota(), 2, '', '.'));
            $baseDeCalculo = str_replace([',', '.'], ['', ''], number_format($nota->getBaseCalculo(), 2, '', '.'));
            $valorIcms = str_replace([',', '.'], ['', ''], number_format($nota->getValorIcms(), 2, '', '.'));
            $cfop = $nota->getCfop();
            $pessoaImportId = $nota->getPessoaImportId();
            $pessoaUf = $nota->getPessoaUf();

            //REGISTRO TIPO 76
            $registro76 .= "76" . substr(str_pad($cpfCnpj, 14, "0", STR_PAD_LEFT), 0, 14);

            if ($tipoPessoa == 'fisica') {
                $registro76 .= substr($this->adicionarCaracter("ISENTO", 14, " ", STR_PAD_RIGHT), 0, 14);
            } else {
                if (!empty($rg)) {
                    $registro76 .= substr($this->adicionarCaracter($this->remove_accents(str_replace(".", "", str_replace("-", "", $rg))), 14, " ", STR_PAD_RIGHT), 0, 14);
                } else {
                    $registro76 .= substr($this->adicionarCaracter("ISENTO", 14, " ", STR_PAD_RIGHT), 0, 14);
                }
            }

            $registro76 .= "21U   ";
            $registro76 .= str_pad($numeroNotaFiscal, 10, "0", STR_PAD_LEFT);
            $registro76 .= $cfop;
            $registro76 .= "1"; // 1-receita propria / 2-Receita de terceiros          
            $registro76 .= $dataEmissao;
            $registro76 .= $pessoaUf;
            $registro76 .= str_pad(str_pad($valorNota, 11, "0", STR_PAD_LEFT), 13, "0", STR_PAD_RIGHT);
            $registro76 .= str_pad(str_pad($baseDeCalculo, 11, "0", STR_PAD_LEFT), 13, "0", STR_PAD_RIGHT);
            $registro76 .= str_pad(str_pad($valorIcms, 10, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
            $registro76 .= str_pad("0", 12, "0", STR_PAD_LEFT);
            $registro76 .= str_pad(str_pad($valorNota, 10, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
            $registro76 .= "00"; // aliquota
            $registro76 .= "N";
            $registro76 .= "$QUEBRA_LINHA";

            $totalRegistro76++;
            $totalLinhas++;

            // REGISTRO TIPO 77
            $registro77 .= "77" . substr(str_pad($cpfCnpj, 14, "0", STR_PAD_LEFT), 0, 14);
            $registro77 .= "21U   ";
            $registro77 .= str_pad($numeroNotaFiscal, 10, "0", STR_PAD_LEFT);
            $registro77 .= $cfop;
            $registro77 .= "1001"; // numero do arquivo item
            $registro77 .= substr($this->adicionarCaracter($codigoNotaFiscalLotePrevio, 11, " ", STR_PAD_RIGHT), 0, 11);
            $registro77 .= str_pad(str_pad("1", 10, "0", STR_PAD_LEFT), 13, "0", STR_PAD_RIGHT);
            $registro77 .= str_pad(str_pad($valorNota, 10, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
            $registro77 .= str_pad("0", 12, "0", STR_PAD_LEFT);
            $registro77 .= str_pad(str_pad($baseDeCalculo, 10, "0", STR_PAD_LEFT), 12, "0", STR_PAD_RIGHT);
            $registro77 .= "00"; // aliquota
            $registro77 .= substr(str_pad($cpfCnpj, 14, "0", STR_PAD_LEFT), 0, 14);
            $registro77 .= substr(str_pad($pessoaImportId, 10, "0", STR_PAD_LEFT), 0, 10);
            $registro77 .= "$QUEBRA_LINHA";

            $totalRegistro77++;
            $totalLinhas++;
        }

        $body = $registro76 . $registro77;
        $totalLinhas++;
        $footer = "90" . "1093427300016778829257      " . "77";
        $footer .= str_pad($totalRegistro77, 8, "0", STR_PAD_LEFT);
        $footer .= "76";
        $footer .= str_pad($totalRegistro76, 8, "0", STR_PAD_LEFT);
        $footer .= "99";
        $footer .= str_pad($totalLinhas, 8, "0", STR_PAD_LEFT);
        $footer .= "                                                                 1";

        return $header . $body . $footer;
    }

    public function sintegrasGeracaoAction(Request $request) {
        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();
        $competencia = $this->getCompetenciaAtual();

        $retornoView = array();
        $retornoView['competencias'] = $competencias;
        $retornoView['competenciaAtual'] = $competencia;

        return $this->render('ImaTelecomBundle:Fiscal:sintegras-geracao.html.twig', $retornoView);
    }

    public function sintegrasGeracaoPorCompetenciaAction(Request $request, $competenciaId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $user = $this->getUser();
        $competencia = CompetenciaQuery::create()->findPk($competenciaId);

        if ($competencia == null) {
            $competencia = $this->getCompetenciaAtual();
        }

        $sintegra = SintegraQuery::create()->findOneByCompetenciaId($competencia->getId());
        if ($sintegra == null) {
            $documento = $this->gerarDocumentoSintegra($competencia);
            $dataGeracao = new \DateTime('now');
            $sintegra = new Sintegra();
            $sintegra->setDataGeracao($dataGeracao);
            $sintegra->setDataCadastro(new \DateTime('now'));
            $sintegra->setDocumento($documento);
            $sintegra->setCompetencia($competencia);
            $sintegra->setDataAlterado(new \DateTime('now'));
            $sintegra->setUsuarioAlterado($user->getIdusuario());

            try {
                $sintegra->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Arquivo gerado com sucesso!';
            } catch (Exception $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        } else {
            $link = $this->generateUrl('ima_telecom_admin_fiscal_sintegras_regerar_arquivo', array('sintegraId' => $sintegra->getId()));
            $retornoView['erros'][] = "O Arquivo Sintegra já existe para esta competência.";
            $retornoView['erros'][] = "Você pode Regerá-lo <a href='' data-url='$link' class='regerar-arquivo'>click aqui</a>, e selecionando a opção <i class='icon fa fa-recycle' aria-hidden='true'></i> (Regerar arquivo Sintegra).";
        }

        return new JsonResponse($retornoView);
    }

    public function sintegrasRegerarArquivoAction(Request $request, $sintegraId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $sintegra = SintegraQuery::create()->findPk($sintegraId);
        if ($sintegra != null) {
            $user = $this->getUser();
            $documento = $this->gerarDocumentoSintegra($sintegra->getCompetencia());

            $sintegra->setDocumento($documento);
            $sintegra->setDataRegeracao(new \DateTime('now'));
            $sintegra->setDataAlterado(new \DateTime('now'));
            $sintegra->setUsuarioAlterado($user->getIdusuario());

            try {
                $sintegra->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Arquivo Sintegra Regerado com sucesso!';
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_fiscal_sintegras');
            } catch (Exception $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        } else {
            $retornoView['erros'][] = "O Arquivo Sintegra não existe para esta competência. Você pode Gerá-lo no menu acima da listagem, selecionando a opção de 'Gerar Arquivo'";
        }

        return new JsonResponse($retornoView);
    }

    public function notasFiscaisScmNotasGeradasAction(Request $request, $competenciaId = 0) {
        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        if ($competencia == null) {
            $competencia = $this->getCompetenciaAtual();
        }

        $notas = DadosFiscalQuery::create()
                ->withColumn('boleto.cliente_id', 'clienteId')
                ->withColumn('pessoa.nome', 'nome')
                ->useBoletoQuery()->useClienteQuery()->joinPessoa()->endUse()->endUse()
                ->filterByCompetencia($competencia)
                ->filterByTipoNota('scm')
                ->find();

        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();

        $retornoView = array();
        $retornoView['notas'] = $notas;
        $retornoView['competencias'] = $competencias;
        $retornoView['competenciaAtual'] = $competencia;

        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-scm-notas-geradas.html.twig', $retornoView);
    }

    public function notasFiscaisScmNotasGeradasMidiaFiscalDownloadPorCompetenciaAction(Request $request, $competenciaId) {
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes

        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        $notas = DadosFiscalQuery::create()
                ->filterByCompetencia($competencia)
                ->filterByTipoNota('scm')
                ->orderByNumeroNotaFiscal()
                ->find();

        $documentos = '';
        $mestres = '';
        $items = '';

        foreach ($notas as $nota) {
            $documentos .= $nota->getDocumento();
            $mestres .= $nota->getMestre();

            $itemsNota = $nota->getDadosFiscalItems();
            foreach ($itemsNota as $itemNota) {
                $items .= $itemNota->getItem();
            }
        }

        $anoMes = str_replace("20", "", $competencia->getAno()) . $competencia->getMes();
        $nomeArquivoNota = '';
        if ($competencia->getAno() == 2016) {
            $uf = "RJ";
            $serie = "U  ";
            $situacao = "N";
            $nomeArquivoNota = $uf . $serie . $anoMes . $situacao;
        } else if ($competencia->getAno() >= 2017) {
            $uf = "RJ";
            $cnpjEmitente = "10934273000167";
            $modelo = "21";
            $serie = "U  ";
            $situacao = "N01";
            $nomeArquivoNota = $uf . $cnpjEmitente . $modelo . $serie . $anoMes . $situacao;
        }

        $caminhoDocumento = 'tmp/' . $nomeArquivoNota . 'D.001';
        $caminhoMestre = 'tmp/' . $nomeArquivoNota . 'M.001';
        $caminhoItems = 'tmp/' . $nomeArquivoNota . 'I.001';

        $this->escreverArquivo($caminhoDocumento, $documentos);
        $this->escreverArquivo($caminhoMestre, $mestres);
        $this->escreverArquivo($caminhoItems, $items);

        $filename = 'midia_fiscal_' . time() . '.zip';
        $filePath = 'tmp/' . $filename;

        $zip = new \ZipArchive();
        if ($zip->open($filePath, \ZIPARCHIVE::CREATE)) {
            $zip->addFile($caminhoDocumento, str_replace('tmp/', '', $caminhoDocumento));
            $zip->addFile($caminhoMestre, str_replace('tmp/', '', $caminhoMestre));
            $zip->addFile($caminhoItems, str_replace('tmp/', '', $caminhoItems));
        }

        $zip->close();

        /*
          // prepare BinaryFileResponse
          $response = new BinaryFileResponse($filePath);
          $response->trustXSendfileTypeHeader();
          $response->setContentDisposition(
          ResponseHeaderBag::DISPOSITION_INLINE,
          $filename,
          iconv('UTF-8', 'ASCII//TRANSLIT', $filename)
          );


          return $response; */
        /*
          // Generate response
          $response = new Response();

          // Set headers
          $response->headers->set('Cache-Control', 'private');
          $response->headers->set('Content-type', mime_content_type($filename));
          $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
          $response->headers->set('Content-length', filesize($filename));

          // Send headers before outputting anything
          $response->sendHeaders();
          $response->setContent(file_get_contents($filename));

          return $response; */

        $retornoView = array();
        $retornoView['status'] = true;
        $retornoView['download_url'] = $request->getBasePath() . '/' . $filePath;

        return new JsonResponse($retornoView);
    }

    public function escreverArquivo($caminho, $conteudo) {
        $arquivo = fopen($caminho, "w");
        fwrite($arquivo, $conteudo);
        fclose($arquivo);
    }

    public function notasFiscaisScmNotasGeradasNotasFiscaisPdfDownloadPorCompetenciaAction(Request $request, $competenciaId) {
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes

        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        $notas = DadosFiscalQuery::create()
                ->filterByCompetencia($competencia)
                ->filterByTipoNota('scm')
                ->orderByNumeroNotaFiscal()
                ->find();


        //$filename = 'tmp/notas_fiscais_' . time() . '.zip';

        $filename = 'notas_fiscais_' . time() . '.zip';
        $filePath = 'tmp/' . $filename;

        $zip = new \ZipArchive();
        if ($zip->open($filePath, \ZIPARCHIVE::CREATE)) {
            foreach ($notas as $nota) {
                $pdf = $this->geracaoArquivoNotaFiscalPdf($nota);
                $arquivo = str_replace('tmp/', '', $pdf);

                $zip->addFile($pdf, $arquivo);
            }
        }

        $zip->close();
        /*
          // Generate response
          $response = new Response();

          // Set headers
          $response->headers->set('Cache-Control', 'private');
          $response->headers->set('Content-type', mime_content_type($filename));
          $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
          $response->headers->set('Content-length', filesize($filename));

          // Send headers before outputting anything
          $response->sendHeaders();
          $response->setContent(file_get_contents($filename));

          return $response; */



        $retornoView = array();
        $retornoView['status'] = true;
        $retornoView['download_url'] = $request->getBasePath() . '/' . $filePath;

        return new JsonResponse($retornoView);
    }

    public function notasFiscaisScmRelacaoDeNotasEmitidasAction(Request $request) {
        $competencia = $this->getCompetenciaAtual();

        $pdf = new ExtendedFPDF('P', 'cm', 'A4');
        $pdf->SetTopMargin(0);
        $pdf->SetLeftMargin(0);
        $pdf->SetRightMargin(0);
        $pdf->SetAutoPageBreak(true, 0, 0);
        $pdf->AddPage('L');

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->setXY(0.30, 0.5);
        $pdf->drawTextBox(utf8_decode("Relação de Notas Fiscais"), 15, 5, 'L', 'T', 0);
        $this->cabecalhoRelatorioIMAPDF($pdf);

        $pdf->SetFont('Arial', 'B', 8);
        $pdf->setXY(0.30, 1.2);
        $pdf->drawTextBox(utf8_decode("Competência:"), 15, 5, 'L', 'T', 0);

        $pdf->setXY(4, 1.2);
        $pdf->drawTextBox($competencia->getId(), 15, 5, 'L', 'T', 0);

        $anoMes = str_replace("20", "", $competencia->getAno()) . $competencia->getMes();

        $pdf->setXY(5, 1.2);
        $pdf->drawTextBox($anoMes, 15, 5, 'L', 'T', 0);

        $pdf->setXY(7.50, 1.2);
        $pdf->drawTextBox("Revenda:", 15, 5, 'L', 'T', 0);

        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetLineWidth(0.05);

        $pdf->setXY(0.30, 1.75);
        $pdf->drawTextBox(utf8_decode("Emissão"), 30, 5, 'L', 'T', 0);

        $pdf->setXY(2.31, 1.75);
        $pdf->drawTextBox("Cliente", 30, 5, 'L', 'T', 0);

        $pdf->setXY(9.05, 1.75);
        $pdf->drawTextBox("Doc. Cliente", 30, 5, 'L', 'T', 0);

        $pdf->setXY(12, 1.75);
        $pdf->drawTextBox("Chave Digital", 30, 5, 'L', 'T', 0);

        $pdf->setXY(18.2, 1.75);
        $pdf->drawTextBox(utf8_decode("Núm. NF."), 30, 5, 'L', 'T', 0);

        $pdf->setXY(19.8, 1.75);
        $pdf->drawTextBox("Cfop", 30, 5, 'L', 'T', 0);

        $pdf->setXY(20.8, 1.75);
        $pdf->drawTextBox("Base", 30, 5, 'L', 'T', 0);

        $pdf->setXY(22.3, 1.75);
        $pdf->drawTextBox("Vlr. Icms", 30, 5, 'L', 'T', 0);

        $pdf->setXY(23.8, 1.75);
        $pdf->drawTextBox("N. Trib", 30, 5, 'L', 'T', 0);

        $pdf->setXY(25.3, 1.75);
        $pdf->drawTextBox("Vlr. NF", 30, 5, 'L', 'T', 0);

        $pdf->setXY(26.9, 1.75);
        $pdf->drawTextBox(utf8_decode("Situação"), 30, 5, 'L', 'T', 0);

        $pdf->setXY(28.4, 1.75);
        $pdf->drawTextBox("Ref.", 30, 5, 'L', 'T', 0);

        $registros = 0;
        $valorTotal = 0;

        $pdf->SetFont('Arial', '', 8);
        $positionY = 2.2;

        $notas = DadosFiscalQuery::create()->withColumn('nome', 'nome')->withColumn('cpf_cnpj', 'cpfCnpj')
                ->useBoletoQuery()->useClienteQuery()->usePessoaQuery()->endUse()->endUse()->endUse()
                ->filterByTipoNota('scm')
                ->findByCompetenciaId($competencia->getId());

        foreach ($notas as $nota) {
            /*
              $pessoa = $nota->getPessoa();
              $nome = $pessoa->getNome();
              $cpfCnpj = $pessoa->getCpfCnpj(); */

            $nome = $nota->getNome();
            $cpfCnpj = $nota->getCpfCnpj();

            $valorNota = $nota->getValorNota();
            $dataDaEmissao = $nota->getDataEmissao();
            $mestreChaveDigitalMeio = $nota->getMestreChaveDigitalMeio();
            $numeroNotaFiscal = $nota->getNumeroNotaFiscal();
            $cfop = $nota->getCfop();
            $baseCalculo = $nota->getBaseCalculo();
            $valorIcms = $nota->getValorIcms();
            $numeroTributo = $nota->getNTributo();
            $situacao = $nota->getSituacao();
            $servicoClienteImportId = $nota->getServicoClienteImportId();

            $registros++;
            $valorTotal += $valorNota;

            //$date = date_create($dataDaEmissao);
            //$dataEmissao = date_format($date, "d/m/Y");
            $dataEmissao = $dataDaEmissao->format('d/m/Y');

            $pdf->setXY(0.30, $positionY);
            $pdf->drawTextBox($dataEmissao, 30, 5, 'L', 'T', 0);

            $cliente = substr($this->adicionarCaracter($this->remove_accents(trim($nome)), 35, " ", STR_PAD_RIGHT), 0, 35);
            $pdf->setXY(2.31, $positionY);
            $pdf->drawTextBox($cliente, 30, 5, 'L', 'T', 0);

            $pdf->setXY(9.05, $positionY);
            $pdf->drawTextBox($cpfCnpj, 30, 5, 'L', 'T', 0);

            $pdf->setXY(12, $positionY);
            $pdf->drawTextBox(strtoupper($mestreChaveDigitalMeio), 30, 5, 'L', 'T', 0);

            $pdf->setXY(18.2, $positionY);
            $pdf->drawTextBox($numeroNotaFiscal, 1.4, 5, 'R', 'T', 0);

            $pdf->setXY(19.8, $positionY);
            $pdf->drawTextBox($cfop, 30, 5, 'L', 'T', 0);

            $pdf->setXY(20.8, $positionY);
            $pdf->drawTextBox("R$ " . number_format($baseCalculo, 2, ',', '.'), 30, 5, 'L', 'T', 0);

            $pdf->setXY(22.3, $positionY);
            $pdf->drawTextBox("R$ " . number_format($valorIcms, 2, ',', '.'), 30, 5, 'L', 'T', 0);

            $pdf->setXY(23.8, $positionY);
            $pdf->drawTextBox("R$ " . number_format($numeroTributo, 2, ',', '.'), 30, 5, 'L', 'T', 0);

            $pdf->setXY(25.3, $positionY);
            $pdf->drawTextBox("R$ " . number_format($valorNota, 2, ',', '.'), 30, 5, 'L', 'T', 0);

            $pdf->setXY(26.9, $positionY);
            $pdf->drawTextBox(ucfirst($situacao), 30, 5, 'L', 'T', 0);

            $pdf->setXY(28.4, $positionY);
            $pdf->drawTextBox($servicoClienteImportId, 30, 5, 'L', 'T', 0);

            $positionY += 0.35;

            // rodapé
            if ($positionY >= 20) {
                $this->rodapeRelatorioIMAPDF($pdf);

                $pdf->AddPage('L');
                $positionY = 1.2;

                $this->cabecalhoRelatorioIMAPDF($pdf, true);

                $pdf->SetFont('Arial', '', 8);
            }
        }

        $graphX = 0;
        $graphY = $positionY;
        $graphW = 30;
        $graphH = 0.01;

        $this->criarLinhaHorizontalPDF($pdf, $graphX, $graphY, $graphW, $graphH);

        $positionY += 0.15;

        $pdf->SetFont('Arial', 'B', 10);
        $pdf->setXY(19.8, $positionY);
        $pdf->drawTextBox("Subtotal:", 30, 5, 'L', 'T', 0);

        $pdf->setXY(22.3, $positionY);
        $pdf->drawTextBox("R$ 0,00", 30, 5, 'L', 'T', 0);

        $pdf->setXY(25.3, $positionY);
        $pdf->drawTextBox("R$ " . number_format($valorTotal, 2, ',', '.'), 30, 5, 'L', 'T', 0);

        $positionY += 0.45;
        $graphY = $positionY;

        $this->criarLinhaHorizontalPDF($pdf, $graphX, $graphY, $graphW, $graphH);

        //$positionY += 0.20;
        $positionY += 0.10;
        $pdf->setXY(0.30, $positionY);
        $pdf->drawTextBox($registros, 30, 5, 'L', 'T', 0);

        $pdf->setXY(2.31, $positionY);
        $pdf->drawTextBox("Registros", 30, 5, 'L', 'T', 0);

        $pdf->setXY(18.2, $positionY);
        $pdf->drawTextBox("Total:", 30, 5, 'L', 'T', 0);

        $pdf->setXY(19.8, $positionY);
        $pdf->drawTextBox("R$ 0,00", 30, 5, 'L', 'T', 0);

        $pdf->setXY(23.8, $positionY);
        $pdf->drawTextBox("R$ " . number_format($valorTotal, 2, ',', '.'), 30, 5, 'L', 'T', 0);

        $this->rodapeRelatorioIMAPDF($pdf);

        $caminhoArquivoSalvo = "repositorio/notas/relatorios/relacao_notas_fiscais_geradas__$anoMes.pdf";
        $pdf->Output($caminhoArquivoSalvo, "F");

        return new BinaryFileResponse($caminhoArquivoSalvo);
    }

    private function cabecalhoRelatorioIMAPDF($pdf, $novaPagina = false) {
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->setXY(0.30, 0.5);
        $pdf->drawTextBox(utf8_decode("Relação de Notas Fiscais"), 15, 5, 'L', 'T', 0);

        // cabe�alho
        $graphX = 0;
        $graphY = 2;
        $graphW = 30;
        $graphH = 0.05;
        if ($novaPagina)
            $graphY = 1;

        $this->criarLinhaHorizontalPDF($pdf, $graphX, $graphY, $graphW, $graphH);

        return $pdf;
    }

    private function rodapeRelatorioIMAPDF($pdf) {
        $graphX = 0;
        $graphY = 20.10;
        $graphW = 30;
        $graphH = 0.01;

        $this->criarLinhaHorizontalPDF($pdf, $graphX, $graphY, $graphW, $graphH);

        $graphY += 0.16;

        $dataAtual = $this->geraDataEmPortugues();
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->setXY(0.30, $graphY);
        $pdf->drawTextBox($dataAtual, 30, 5, 'L', 'T', 0);

        return $pdf;
    }

    private function geraDataEmPortugues() {
        //setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
        $data = strftime('%A, %d de %B de %Y', strtotime('today'));

        // Dias da semana convertidos
        $data = str_replace("Sunday", "Domingo", $data);
        $data = str_replace("Monday", "Segunda-feira", $data);
        $data = str_replace("Tuesday", utf8_decode("Terça-feira"), $data);
        $data = str_replace("Wednesday", "Quarta-feira", $data);
        $data = str_replace("Thursday", "Quinta-feira", $data);
        $data = str_replace("Friday", "Sexta-feira", $data);
        $data = str_replace("Saturday", "Sabado", $data);

        // Meses convertidos
        $data = str_replace("January", "Janeiro", $data);
        $data = str_replace("February", "Fevereiro", $data);
        $data = str_replace("March", utf8_decode("Março"), $data);
        $data = str_replace("April", "Abril", $data);
        $data = str_replace("May", "Maio", $data);
        $data = str_replace("June", "Junho", $data);
        $data = str_replace("July", "Julho", $data);
        $data = str_replace("August", "Agosto", $data);
        $data = str_replace("September", "Setembro", $data);
        $data = str_replace("October", "Outubro", $data);
        $data = str_replace("November", "Novembro", $data);
        $data = str_replace("December", "Dezembro", $data);

        return $data;
    }

    private function criarLinhaHorizontalPDF($pdf, $graphX, $graphY, $graphW, $graphH) {
        $posicaoY = $graphY + $graphH;
        $largura = $graphX + $graphW;
        $altura = $graphY + $graphH;

        return $pdf->Line($graphX, $posicaoY, $largura, $altura);
    }

    public function testeEnviarNotaAction() {
        // Configuração dos dados da empresa

        $config = $this->getDadosNotaPrefeitura();
        $nfse = new NFSE($config);

        // informações do Emitente
        $razaoSocialEmpresa = utf8_decode($this->getParameter('nfse_empresa_nome'));
        $nomeFantasiaEmpresa = utf8_decode($this->getParameter('nfse_empresa_nome'));
        $cnpjEmpresa = $this->getParameter('nfse_empresa_cnpj');
        $inscricaoMunicipalEmpresa = $this->getParameter('nfse_empresa_inscricao_municipal');

        $dataEmissao = '2018-06-18';

        // Dados para o envio de lotes
        $lotes = array();
        $idLote = 0;
        $idRps = 36;
        $msg = '';
        $count = 0;

        $cobrancas = BoletoQuery::create()->filterByIdboleto(111728)->find();

        // loop
        foreach ($cobrancas as $cobranca) {
            // Dados da Nota
            $naturezaOperacao = '1';
            $valorNota = $cobranca->getValor();
            $aliquota = 2;
            $itemListaServico = '1.07';
            $codigoTributacaoMunicipio = '619060100';
            $vencimento = $cobranca->getVencimento();

            // Dados do Destinatário
            $cliente = $cobranca->getCliente();
            $pessoa = $cliente->getPessoa();
            $endereco = $cliente->getEnderecoCliente();
            $cidade = $endereco->getCidade();

            $codigoCliente = $cliente->getIdcliente();
            $tipoPessoaCliente = $pessoa->getTipoPessoa();
            $cpfCnpjCliente = $pessoa->getCpfCnpj();
            $nomeCliente = utf8_decode($pessoa->getNome());
            $razaoSocialCliente = utf8_decode($pessoa->getRazaoSocial());
            $emailCliente = $pessoa->getEmail();
            $ruaCliente = utf8_decode($endereco->getRua());
            $numeroCliente = $endereco->getNum();
            $bairroCliente = utf8_decode($endereco->getBairro());
            $complementoCliente = utf8_decode($endereco->getComplemento());
            $cidadeCliente = utf8_decode($cidade->getMunicipio());
            $codigoMunicipioCliente = $cidade->getCodigo();
            $ufCliente = $cidade->getUf();
            $cepCliente = str_replace(['.', '-'], ['', ''], $endereco->getCep());
            $telefoneCliente = '';

            $servicosNota = array();


            $idRps++;
            $oNF = new \NFSePHPGinfesData($idRps);
            $oNF->set('razaoSocial', $razaoSocialEmpresa);
            $oNF->set('nomeFantasia', $nomeFantasiaEmpresa);
            $oNF->set('CNPJ', $cnpjEmpresa);
            $oNF->set('IM', $inscricaoMunicipalEmpresa);

            $oNF->set('tipo', '1');
            $oNF->set('natOperacao', $naturezaOperacao);
            $oNF->set('optanteSimplesNacional', '2');
            $oNF->set('incentivadorCultural', '2');
            $oNF->set('regimeEspecialTributacao', '2');
            $oNF->set('status', '1');
            $oNF->set('cMun', '3301009');

            $oNF->set('DataEmissao', $dataEmissao);

            $oNF->setItem('valorServicos', $valorNota);
            $oNF->setItem('valorDeducoes', 0);
            $oNF->setItem('valorPis', 0);
            $oNF->setItem('valorCofins', 0);
            $oNF->setItem('valorInss', 0);
            $oNF->setItem('valorIr', 0);
            $oNF->setItem('valorCsll', 0);

            $issRetido = '1';
            $valorIss = 0;
            //$valorIssRetido = 0;

            if ($naturezaOperacao != '4') {
                //$issRetido = '1';
                //$oNF->setItem('valorIss', $nota['valorNota']*$nota['aliquota'] / 100);
                $valorIss = $valorNota * $aliquota / 100;
            }

            $valorIssRetido = $valorIss;
            $oNF->setItem('issRetido', $issRetido);
            $oNF->setItem('valorIss', $valorIss);
            //$oNF->setItem('valorIssRetido', $valorIss);
            $oNF->setItem('valorIssRetido', $valorIssRetido);
            $oNF->setItem('outrasRetencoes', 0);
            $oNF->setItem('aliquota', $aliquota / 100);
            //$oNF->setItem('valorIss', 0);
            $oNF->setItem('descontoIncondicionado', 0);
            $oNF->setItem('descontoCondicionado', 0);

            //$oNF->setItem('ValorLiquidoNfse', $nota['valorNota']-$valorIssRetido);


            $oNF->setItem('itemListaServico', $itemListaServico);
            $oNF->setItem('codigoTributacaoMunicipio', $codigoTributacaoMunicipio);
            //$discriminacao = '';
            $discriminacao = "Código do Cliente: $codigoCliente\n\n";
            foreach ($servicosNota as $servico) {
                $discriminacao .= $servico['nome'] . "\\n";

                if ($servico['tipo'] != '3' and $servico['tipo'] != '4') {
                    $discriminacao .= 'Período de Referência: ';
                    $discriminacao .= date('d/m/Y', strtotime($servico['periodo_inicial'])) . ' à ' . date('d/m/Y', strtotime($servico['periodo_final'])) . "\\n";
                }

                $discriminacao .= 'Vencimento: ' . date('d/m/Y', strtotime($vencimento)) . "\\n\\n";
            }

            $oNF->setItem('discriminacao', $discriminacao);

            if ($tipoPessoaCliente == 'fisica') {
                $oNF->set('tomaCPF', $cpfCnpjCliente);
                $oNF->set('tomaRazaoSocial', utf8_encode($nomeCliente));

                if (!Utils::validarCPF($cpfCnpjCliente)) {
                    $aux = $idLote + 1;
                    $msg .= "Lote $aux - $nomeCliente - CPF Inválido\n\n";
                }
            } else {
                $oNF->set('tomaCNPJ', $cpfCnpjCliente);
                $oNF->set('tomaRazaoSocial', utf8_encode($razaoSocialCliente));

                if (!Utils::validarCNPJ($cpfCnpjCliente)) {
                    $aux = $idLote + 1;
                    $msg .= "Lote $aux - $nomeCliente - CNPJ Inválido\n\n";
                }
            }

            $oNF->set('tomaEndLogradouro', utf8_encode($ruaCliente));

            if (!empty($numeroCliente)) {
                $oNF->set('tomaEndNumero', $numeroCliente);
            } else {
                $oNF->set('tomaEndNumero', 's/n');
            }

            $oNF->set('tomaEndComplemento', utf8_encode($complementoCliente));
            $oNF->set('tomaEndBairro', utf8_encode($bairroCliente));
            $oNF->set('tomaEndxMun', utf8_encode($cidadeCliente));
            $oNF->set('tomaEndcMun', $codigoMunicipioCliente);
            $oNF->set('tomaEndUF', $ufCliente);
            $oNF->set('tomaEndCep', $cepCliente);
            //$oNF->set('tomaEndCep', '');
            $oNF->set('tomaTelefone', $telefoneCliente);
            //$oNF->set('tomaEmail', 'tjamancio@censanet.com.br');

            if ($tipoPessoaCliente == 'juridica') {
                $oNF->set('tomaEmail', $emailCliente);
                //$oNF->set('tomaEmail', 'tjamancio@censanet.com.br');
            } else {
                $oNF->set('tomaEmail', 'notafiscalpf@censanet.com.br');
            }

            //$oNF->set('tomaEmail', 'tjamancio@censanet.com.br');

            $oNF->set('tomaIM', '');
            //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

            $dataEmitido = date('Y-m-d H:i:s');

            echo "RPS: $idRps <br/>";
            echo "Status: Emitida <br/>";
            echo "Data de Emissão: $dataEmissao <br/>";
            echo "Data Emitido: $dataEmitido <br/><br/>";



            /*
              if (!$bd->executar("update {boletos} set nfse_emitida = 1, numero_rps = $idRps, status = 'emitida', data_emissao = '$dataEmissao', data_emitido = '$dataEmitido' where id = {$nota['boleto']}")) {
              $bd->rollback();
              return array(false, $msg . $retorno . "Erro ao Atualizar o Banco de Dados! Nem Todas as Notas foram emitidas!");
              }

              if (!$bd->executar("update {variavel} set valor = $idRps where nome = 'id_rps' and tipo = 6")) {
              $bd->rollback();
              return array(false, $msg . "Falha ao atualizar o n򭥲o do RPS no Banco de Dados! Nem Todas as Notas foram emitidas!");
              } */

            $lotes[] = $oNF;


            $count += 1;
            if ($count % 1 == 0) {
                $idLote++;
                /*
                  if (!$bd->executar("update {variavel} set valor = $idLote where nome = 'id_lote' and tipo = 6")) {
                  $bd->rollback();
                  return array(false, $msg . "Falha ao atualizar o n򭥲o do Lote no Banco de Dados! Nem Todas as Notas foram emitidas!");
                  } */

                $xmlLote = $nfse->montarLoteRps($idLote, $lotes);
                $retorno = $nfse->enviarLoteRps($xmlLote);
                if (!$retorno) {
                    /*
                      $bd->rollback();
                      $bd->startTransaction();
                      $resultado = $variaveis->getPorNome('id_rps');
                      $idRps = $resultado[0][2];
                      $falha = true; */
                    //$msg .= 'Lote ' . $idLote . ' - ' . $nfse->errMsg . "\n\n";
                    $msg .= "Lote $idLote - " . $nomeCliente . ' - ' . $nfse->getErro() . "\n\n";
                    //$log = logger::getInstancia();
                    //$log->info($nfse->getDebug());
                    //$msg .= 'CPF/CNPJ  ' . $nota['cpfCnpj'] . ' - ' . $nfse->errMsg . "\n\n";
                    $nfse->limparErro();

                    echo "Erro: $msg <br/><br/>";
                } else {
                    /*
                      $bd->commit();
                      $bd->startTransaction(); */
                    $doc = new \DOMDocument();
                    $doc->formatOutput = FALSE;
                    $doc->preserveWhiteSpace = FALSE;
                    $doc->loadXML($retorno, LIBXML_NOBLANKS | LIBXML_NOEMPTYTAG);
                    $protocolo = $doc->getElementsByTagName("Protocolo")->item(0)->nodeValue;

                    echo "Protocolo: $protocolo <br/>";
                    echo "Status: Emitida <br/>";

                    /*
                      if (!$bd->executar("update {boletos} set protocolo = $protocolo where nfse_emitida=1 and status = 'emitida' and protocolo is null")) {
                      $bd->rollback();
                      return array(false, $msg . $retorno . "Erro ao Atualizar o Banco de Dados! Protocolo $protocolo n䯠foi salvo -- Nota: {$nota['boleto']}");
                      } */
                }

                //$lotes = array();
            }
        }

        var_dump($lotes);
        /*
          $resp = new Response($lotes);
          $resp->headers->set('Content-type', 'xml');
         */
        return new JsonResponse($lotes);

        /*
          $configJson = [
          "atualizacao" => "2018-10-02 06:01:21",
          "tpAmb" => 2,
          "razaosocial" => "IMA TELECOM LTDA ME",
          "siglaUF" => "RJ",
          "cnpj" => "10934273000167",
          "schemes" => "NFSe",
          "versao" => "3.10",
          "tokenIBPT" => "",
          "CSC" => "",
          "CSCid" => "",
          "aProxyConf" => [
          "proxyIp" => "",
          "proxyPort" => "",
          "proxyUser" => "",
          "proxyPass" => ""
          ]
          ];

          //criando o stdClass a partir de um array
          $xmlArray = [
          'versao' => '3.10',
          'Id' => '',
          'pk_nItem' => null
          ];

          $std = json_decode(json_encode($xmlArray));

          $oNfe = new \stdClass();
          $oNfe->razaoSocial = "";
          $oNfe->nomeFantasia = "";
          $oNfe->CNPJ = "";
          $oNfe->IM = "";

          $oNfe->tipo = '1';
          $oNfe->natOperacao = '4'; // codigo de tributação 619060100
          $oNfe->optanteSimplesNacional = '2';
          $oNfe->incentivadorCultural = '2';
          $oNfe->regimeEspecialTributacao = '2';
          $oNfe->status = '1';
          $oNfe->cMun = '3301009';

          $oNfe->DataEmissao = "";

          $oNfe->valorServicos = "10";
          $oNfe->valorDeducoes = '0';
          $oNfe->valorPis = '0';
          $oNfe->valorCofins = '0';
          $oNfe->valorInss = '0';
          $oNfe->valorIr = '0';
          $oNfe->valorCsll = '0';

          $nfse = new \ImaTelecomBundle\Lib\NFSE\NFSE($pathDir, $filename, $password);
          $nfse->setConfiguracao($configJson);
          //$nfse->createMake($oNfe);
          $xml = $nfse->teste();

          //$xml = '99999999999999999999999999999999999999999999-procNFe.xml';
          $xml = 'nota.xml';
          $docxml = \NFePHP\DA\Legacy\FilesFolders::readFile($xml);

          $danfe = new \NFePHP\DA\NFe\Danfe($docxml, 'P', 'A4', '', 'I', '');
          $id = $danfe->montaDANFE();
          //$teste = $danfe->printDANFE($id . '.pdf', 'F');
          $salva = $danfe->printDocument('teste_nota_aki.pdf', 'F'); //Salva o PDF na pasta

          $response = new BinaryFileResponse($teste);
          $response->trustXSendfileTypeHeader();
          $response->setContentDisposition(
          ResponseHeaderBag::DISPOSITION_INLINE, $filename, iconv('UTF-8', 'ASCII//TRANSLIT', $id . '.pdf')
          );


          return $response;
         */
    }

    public function testeValidarNotaAction() {
        //$protocolo = '7720679';
        $cnpjEmpresa = $this->getParameter('nfse_empresa_cnpj');
        $inscricaoMunicipalEmpresa = $this->getParameter('nfse_empresa_inscricao_municipal');

        $protocolos = array(array(38, '7720679'));
        $config = $this->getDadosNotaPrefeitura();
        $msg = '';
        $count = 0;
        $nfse = new NFSE($config);

        foreach ($protocolos as $protocolo) {
            //$retorno = $nfse->consultarLoteRps($protocolo[0], $this->cnpj, $this->im);
            $retorno = $nfse->consultarNfseRps($protocolo[0], 1, 1, $cnpjEmpresa, $inscricaoMunicipalEmpresa);

            if (!$retorno['status']) {
                //$falha = true;
                //$msg .= 'Protocolo ' . $protocolo[0] . ' - ' . utf8_decode($nfse->errMsg) . "\n\n";
                $msg .= 'Rps ' . $protocolo[0] . ' - ' . utf8_decode($nfse->getErro()) . "\n\n";
                echo utf8_decode("Erro: $msg");
            }

            echo "Finalizado  {$retorno['numeroNota']}";
            $count += 1;
            /*
              //$mensageiro->send_msg("Verificado protocolo " . $protocolo[0] . ' - ' . $count . ' de ' . $qtdeProtocolos);
              $mensageiro->send_msg("Verificado rps " . $protocolo[1] . ' - ' . $count . ' de ' . $qtdeProtocolos);

              $fract = $count / $qtdeProtocolos;
              $mensageiro->send_fracao($fract);
              if ($qtdeProtocolos > $count) {
              //sleep(5);
              } */
        }


        return new Response();
    }

    public function testeVisualizarNFSEAction() {
        $config = $this->getDadosNotaPrefeitura();
        $nfse = new NFSE($config);
        //$name = sprintf("%015s", $numRps) . "-" . sprintf("%08d", $nota);
        $diretorio = $nfse->getDiretorioNfse();
        //$name = 'notaDownload';
        $retorno = '';
        $nota = 187;
        $cnpjEmpresa = $this->getParameter('nfse_empresa_cnpj');
        $inscricaoMunicipalEmpresa = $this->getParameter('nfse_empresa_inscricao_municipal');
        $name = sprintf("%08d", $nota);

        if (!file_exists($diretorio . "aprovadas/{$name}.xml")) {
            //$retorno = $nfse->consultarNfseRps($numRps, 1, 1, $this->cnpj, $this->im);
            $retorno = $nfse->consultarNfse($nota, $cnpjEmpresa, $inscricaoMunicipalEmpresa);
            if (!$retorno) {
                echo "Falha ao gerar a nota " . utf8_decode($nfse->getErro());
            } else {
                $nfse->gerarPDF($diretorio . "aprovadas/{$name}.xml");
            }
        } else {
            $nfse->gerarPDF($diretorio . "aprovadas/{$name}.xml");
        }

        return new JsonResponse($retorno);
    }

    public function notasFiscaisSvaNotasNaoGeradasAction(Request $request, $competenciaId = 0) {
        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        if ($competencia == null) {
            $competencia = $this->getCompetenciaAtual();
        }

        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();
        $cobrancas = BoletoQuery::create()
                ->withColumn('pessoa.nome', 'nome')
                ->withColumn('pessoa.tipo_pessoa', 'tipoPessoa')
                ->useClienteQuery()->joinPessoa()->endUse()
                ->filterByCompetencia($competencia)
                ->filterByNotaSvaGerada(false)
                ->where('base_calculo_sva > 0')                
                ->find();

        $notas = array();
        foreach ($cobrancas as $cobranca) {
            $cobrancaNotaSvaGerada = $cobranca->getNotaSvaGerada();
            $cobrancaNotaSvaCancelada = $cobranca->getNotaSvaCancelada();

            if (!$cobrancaNotaSvaGerada && !$cobrancaNotaSvaCancelada) {
                $notas[] = $cobranca;
            }
        }

        $retornoView = array();
        $retornoView['notas'] = $notas;
        $retornoView['competencias'] = $competencias;
        $retornoView['competenciaAtual'] = $competencia;

        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-sva-notas-nao-geradas.html.twig', $retornoView);
    }

    public function notasFiscaisSvaNotasNaoGeradasGeracaoAction(Request $request) {
        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();
        $competencia = $this->getCompetenciaAtual();

        $retornoView = array();
        $retornoView['competencias'] = $competencias;
        $retornoView['competenciaAtual'] = $competencia;
        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-sva-notas-nao-geradas-geracao.html.twig', $retornoView);
    }

    public function notasFiscaisSvaNotasNaoGeradasGeracaoGerarAction(Request $request) {
        $tipoGeracao = $request->get('tipoGeracao', 'ambos');
        $competenciaId = $request->get('competencia', '');
        $emissao = $request->get('dataEmissao', '');

        $cobranca = BoletoQuery::create()->where('base_calculo_sva > 0')
                ->filterByCompetenciaId($competenciaId)
                ->filterByNotaSvaGerada(false);

        if ($tipoGeracao != 'ambos') {
            $cobranca->useClienteQuery()->joinPessoa()->endUse()->where("tipo_pessoa = '$tipoGeracao'");
        }

        $cobrancas = $cobranca->find();
        if (sizeof($cobrancas) == 0) {
            return new JsonResponse(array('status' => false, 'erros' => array(array('Não foi encontrada cobranças disponíveis para esta geração. Por favor, verifique se já foi gerada à nota SVA para esta cobrança!'))));
        }

        $notas = array();
        foreach ($cobrancas as $cobranca) {
            $cobrancaNotaSvaGerada = $cobranca->getNotaSvaGerada();
            $cobrancaNotaSvaCancelada = $cobranca->getNotaSvaCancelada();

            $notas = array();
            if (!$cobrancaNotaSvaGerada && !$cobrancaNotaSvaCancelada) {
                $notas[] = $cobranca;
            }
        }

        //$emissao = $request->get('dataEmissao', '');
        if (empty($emissao)) {
            $competencia = $this->getCompetenciaAtual();
            $dataEmissao = $competencia->getUltimaDataEmissao();
        } else {
            $dataDaEmissao = DateTime::createFromFormat('d/m/Y', $emissao);
            $dataEmissao = $dataDaEmissao->format('Y-m-d');
        }

        $retorno = $this->enviarNotaSva($notas, $dataEmissao);
        $retornoView = array();
        $retornoView['status'] = $retorno['status'];
        $retornoView['erros'] = $retorno['erros'];

        if ($retornoView['status']) {
            $retornoView['mensagem'] = "Nota emitida com sucesso.";
        }

        return new JsonResponse($retornoView);
    }

    public function notasFiscaisSvaNotasNaoGeradasGeracaoNotaAction(Request $request, $cobrancaId) {
        $retornoView = array();

        $competencia = $this->getCompetenciaAtual();
        $cobranca = BoletoQuery::create()->findPk($cobrancaId);
        $itemsCobranca = $cobranca->getBoletoItems();

        $retornoView['cobranca'] = $cobranca;
        $retornoView['itemsCobranca'] = $itemsCobranca;
        $retornoView['competencia'] = $competencia;

        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-sva-notas-nao-geradas-geracao-nota.html.twig', $retornoView);
    }

    public function notasFiscaisSvaNotasNaoGeradasGeracaoNotaGerarAction(Request $request, $cobrancaId) {
        $cobranca = BoletoQuery::create()->where('base_calculo_sva > 0')
                ->filterByNotaSvaGerada(false)
                ->findPk($cobrancaId); //111728
        if ($cobranca == null) {
            return new JsonResponse(array('status' => false, 'erros' => array(array('Não foi encontrada cobranças disponíveis para esta geração. Por favor, verifique se já foi gerada à nota SVA para esta cobrança!'))));
        }

        $cobrancaNotaSvaGerada = $cobranca->getNotaSvaGerada();
        $cobrancaNotaSvaCancelada = $cobranca->getNotaSvaCancelada();

        $notas = array();
        if (!$cobrancaNotaSvaGerada && !$cobrancaNotaSvaCancelada) {
            $notas[] = $cobranca;
        }

        $emissao = $request->get('dataEmissao', '');
        if (empty($emissao)) {
            $competencia = $this->getCompetenciaAtual();
            $dataEmissao = $competencia->getUltimaDataEmissao();
        } else {
            $dataDaEmissao = DateTime::createFromFormat('d/m/Y', $emissao);
            $dataEmissao = $dataDaEmissao->format('Y-m-d');
        }

        $retorno = $this->enviarNotaSva($notas, $dataEmissao);
        $retornoView = array();
        $retornoView['status'] = $retorno['status'];
        $retornoView['erros'] = $retorno['erros'];
        //$retornoView['erros'][] = json_encode($notas);

        if ($retornoView['status']) {
            $retornoView['mensagem'] = "Nota emitida com sucesso.";
        }

        return new JsonResponse($retornoView);
    }

    public function notasFiscaisSvaNotasNaoVerificadasAction(Request $request) {
        $notas = DadosFiscalQuery::create()
                        ->withColumn('pessoa.nome', 'nome')
                        ->withColumn('pessoa.tipo_pessoa', 'tipoPessoa')
                        ->withColumn('boleto.vencimento', 'vencimento')
                        ->useBoletoQuery()->useClienteQuery()->joinPessoa()->endUse()->endUse()
                        ->filterByTipoNota('sva')
                        ->filterBySituacao('emitida')->find();

        $retornoView = array();
        $retornoView['notas'] = $notas;
        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-sva-notas-nao-verificadas.html.twig', $retornoView);
    }

    public function notasFiscaisSvaNotasNaoVerificadasVerificacaoNotaAction(Request $request, $notaId) {
        $retornoView = array();

        $competencia = $this->getCompetenciaAtual();
        $nota = DadosFiscalQuery::create()->filterByTipoNota('sva')->findPk($notaId);
        $itemsNota = $nota->getDadosFiscalItems();

        $retornoView['nota'] = $nota;
        $retornoView['itemsNota'] = $itemsNota;
        $retornoView['competencia'] = $competencia;
        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-sva-notas-nao-verificadas-verificacao-nota.html.twig', $retornoView);
    }

    // gerar um arquivo de log, parecido com os lançamentos bancários, ao gerar lançamento
    public function notasFiscaisSvaNotasNaoVerificadasVerificacaoNotaVerificarAction(Request $request, $notaId) {
        $notas = DadosFiscalQuery::create()->filterById($notaId)->filterByTipoNota('sva')->filterBySituacao('emitida')->find();
        $retorno = $this->verificarNotaSva($notas);

        $retornoView = array();
        $retornoView['status'] = $retorno['status'];
        $retornoView['erros'] = $retorno['erros'];

        if ($retornoView['status']) {
            $retornoView['mensagem'] = "Nota verificada com sucesso.";
        }

        //return new JsonResponse($retornoView);
        $filename = $retorno['log'];

        // Generate response
        $response = new Response();

        // Set headers
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($filename));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
        $response->headers->set('Content-length', filesize($filename));

        // Send headers before outputting anything
        $response->sendHeaders();
        $response->setContent(file_get_contents($filename));

        return $response;
    }

    public function notasFiscaisSvaNotasNaoVerificadasVerificacaoAction(Request $request) {
        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();
        $competencia = $this->getCompetenciaAtual();

        $retornoView = array();
        $retornoView['competencias'] = $competencias;
        $retornoView['competenciaAtual'] = $competencia;

        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-sva-notas-nao-verificadas-verificacao.html.twig', $retornoView);
    }

    public function notasFiscaisSvaNotasNaoVerificadasVerificacacaoVerificarAction(Request $request) {
        $tipoGeracao = $request->get('tipoGeracao', 'ambos');
        $competenciaId = $request->get('competencia', '');

        $notasGeradas = DadosFiscalQuery::create()
                ->filterByTipoNota('sva')
                ->filterBySituacao('emitida')
                ->filterByCompetenciaId($competenciaId);

        if ($tipoGeracao != 'ambos') {
            $notasGeradas->useBoletoQuery()->useClienteQuery()->joinPessoa()->endUse()->endUse()->where("tipo_pessoa = '$tipoGeracao'");
        }

        $notas = $notasGeradas->find();

        $retornoView = array();
        if (sizeof($notas) > 0) {
            $retorno = $this->verificarNotaSva($notas);
            $retornoView['status'] = $retorno['status'];
            $retornoView['erros'] = $retorno['erros'];

            if ($retornoView['status']) {
                $retornoView['mensagem'] = "Notas verificadas com sucesso.";
            }

            $filename = $retorno['log'];

            // Generate response
            $response = new Response();

            // Set headers
            $response->headers->set('Cache-Control', 'private');
            $response->headers->set('Content-type', mime_content_type($filename));
            $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
            $response->headers->set('Content-length', filesize($filename));

            // Send headers before outputting anything
            $response->sendHeaders();
            $response->setContent(file_get_contents($filename));

            return $response;
        } else {
            $retornoView['status'] = false;
            $retornoView['erros'] = array();
            $retornoView['erros'][] = 'Não existem notas para serem verificadas.';
        }

        return new JsonResponse($retornoView);
    }

    public function notasFiscaisSvaNotasGeradasAction(Request $request, $competenciaId = 0) {
        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        if ($competencia == null) {
            $competencia = $this->getCompetenciaAtual();
        }

        $notas = DadosFiscalQuery::create()
                ->withColumn('boleto.cliente_id', 'clienteId')
                ->withColumn('pessoa.nome', 'nome')
                ->useBoletoQuery()->useClienteQuery()->joinPessoa()->endUse()->endUse()
                ->filterByCompetencia($competencia)
                ->filterByTipoNota('sva')
                ->where('situacao != "emitida"')
                ->find();

        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();

        $retornoView = array();
        $retornoView['notas'] = $notas;
        $retornoView['competencias'] = $competencias;
        $retornoView['competenciaAtual'] = $competencia;
        return $this->render('ImaTelecomBundle:Fiscal:notas-fiscais-sva-notas-geradas.html.twig', $retornoView);
    }

}
