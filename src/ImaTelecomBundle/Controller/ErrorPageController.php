<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ImaTelecomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
/**
 * Description of ErrorPage
 *
 * @author jrodolfo
 */
class ErrorPageController extends Controller {
    //put your code here
    
    public function page404Action(Request $request) {
        $message = $request->getSession()->get('ErrorPageMessage');
        if (empty($message)) {
            $message = 'Parece que nada foi encontrado neste local.';
        }
        
        $request->getSession()->set('ErrorPageMessage', '');
        
        $retornoView = array();
        $retornoView['message'] = $message;
        return $this->render('ImaTelecomBundle:ErrorPage:404.html.twig', $retornoView);
    }
}
