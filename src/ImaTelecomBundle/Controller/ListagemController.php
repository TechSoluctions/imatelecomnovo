<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ImaTelecomBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ImaTelecomBundle\Model\CompetenciaQuery;
use ImaTelecomBundle\Model\BoletoQuery;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use ImaTelecomBundle\Model\Map\PessoaTableMap;
use Propel\Runtime\Map\TableMap;

/**
 * Description of ListagemController
 *
 * @author jrodolfo
 */
class ListagemController extends AdminController {

    public function cobrancasListagemAction(Request $request, $competenciaId, $tipoLancamento) {
        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        if ($competencia == null) {
            $competencia = $this->getCompetenciaAtual();
        }

        $stringContent = json_encode($request->getContent());
        $url = str_replace('"', '', urldecode($stringContent));
        parse_str($url, $data);

        $query = BoletoQuery::create()
                        ->withColumn('pessoa.nome', 'Nome')
                        ->withColumn("concat(competencia.Ano, '/', competencia.Mes)", 'Competencia')
                        ->useClienteQuery()->joinPessoa()->endUse()
                        ->useCompetenciaQuery()->endUse()
                        ->filterByCompetencia($competencia)
                        ->limit($data['length'])->offset($data['start']);

        if ($tipoLancamento != 'todos') {
            $query->filterByTipoConta($tipoLancamento);
        }

        $queryFiltered = BoletoQuery::create()->useClienteQuery()->joinPessoa()->endUse()->filterByCompetencia($competencia);
        if ($tipoLancamento != 'todos') {
            $queryFiltered->filterByTipoConta($tipoLancamento);
        }

        $recordsTotal = $queryFiltered->count();

        foreach ($data['order'] as $order) {
            $column = $data['columns'][$order['column']];
            if ($column['orderable']) {
                $query->orderBy($column['data'], $order['dir']);
            }
        }

        if (!empty($data['search']['value'])) {
            $where = [];

            foreach ($data['columns'] as $column) {
                if ($column['searchable']) {
                    switch ($column['name']) {
                        case 'Boleto':
                            $where[] = BoletoTableMap::translateFieldName($column['data'], TableMap::TYPE_PHPNAME, TableMap::TYPE_COLNAME) . " like '%{$data['search']['value']}%'";
                            break;

                        case 'Pessoa':
                            $where[] = PessoaTableMap::translateFieldName($column['data'], TableMap::TYPE_PHPNAME, TableMap::TYPE_COLNAME) . " like '%{$data['search']['value']}%'";
                            break;

                        default:
                            break;
                    }
                }
            }

            if (sizeof($where)) {
                $query->where('(' . implode(' or ', $where) . ')');
            }

            $cobrancas = $query->find();
            $filtered = $query->count();
        } else {
            $cobrancas = $query->find();
            $filtered = $recordsTotal;
        }

        $output = array(
            'data' => array(),
            'draw' => $data['draw'],
            'recordsFiltered' => $filtered,
            'recordsTotal' => $recordsTotal
        );

        $output['data'] = $cobrancas->toArray();

        return new Response(json_encode($output), 200, ['Content-Type' => 'application/json']);
    }

}
