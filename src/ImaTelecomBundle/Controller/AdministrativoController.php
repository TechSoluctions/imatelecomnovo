<?php

namespace ImaTelecomBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use \DateTime;
use Propel\Runtime\Propel;
use Propel\Runtime\Exception\PropelException;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use ImaTelecomBundle\Model\Map\SiciTableMap;
use ImaTelecomBundle\Model\Map\PessoaTableMap;
use ImaTelecomBundle\Model\Pessoa;
use ImaTelecomBundle\Model\Cliente;
use ImaTelecomBundle\Model\ClienteQuery;
use ImaTelecomBundle\Model\EnderecoCliente;
use ImaTelecomBundle\Model\EnderecoClienteQuery;
use ImaTelecomBundle\Model\BoletoQuery;
use ImaTelecomBundle\Model\BoletoItemQuery;
use ImaTelecomBundle\Model\ServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery;
use ImaTelecomBundle\Model\DadosFiscalQuery;
use ImaTelecomBundle\Model\SiciQuery;
use ImaTelecomBundle\Model\Sici as SiciModel;
use ImaTelecomBundle\Lib\Sici;
use ImaTelecomBundle\Model\Telefone;
use ImaTelecomBundle\Model\TelefoneQuery;
use ImaTelecomBundle\Model\ServicoPrestadoQuery;
use ImaTelecomBundle\Model\ServicoPrestado;
use ImaTelecomBundle\Model\DadosFiscaisGeracaoQuery;
use ImaTelecomBundle\Model\Map\ServicoClienteTableMap;
use ImaTelecomBundle\Model\Map\EnderecoClienteTableMap;
use ImaTelecomBundle\Model\CompetenciaQuery;
use ImaTelecomBundle\Model\CidadeQuery;
use ImaTelecomBundle\Lib\Utils;
use ImaTelecomBundle\Lib\BoletoSantanderCNAB240;
use ImaTelecomBundle\Model\EnderecoServClienteQuery;
use ImaTelecomBundle\Model\EnderecoServCliente;
use ImaTelecomBundle\Model\PlanosQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use ImaTelecomBundle\Model\TipoServicoPrestadoQuery;
use ImaTelecomBundle\Model\FornecedorQuery;
use ImaTelecomBundle\Model\Fornecedor;
use ImaTelecomBundle\Model\BancoAgenciaContaQuery;

class AdministrativoController extends AdminController {

    public function indexAction(Request $request) {
        return $this->render('ImaTelecomBundle:Admin:index.html.twig');
    }

    public function clientesAction(Request $request) {
        $clientes = ClienteQuery::create()
                ->select(array('Idcliente', 'Nome', 'Ativo', 'DataContrato'))
                ->usePessoaQuery()
                ->withColumn('pessoa.nome', 'Nome')
                ->endUse()
                ->find();

        $retornoView = array();
        $retornoView['clientes'] = $clientes;
        return $this->render('ImaTelecomBundle:Administrativo:clientes.html.twig', $retornoView);
    }

    public function clientesClienteAction(Request $request, $clienteId) {
        $cliente = ClienteQuery::create()->withColumn('Pessoa.nome', 'Nome')->join('Pessoa')->findPk($clienteId);

        if ($cliente == null) {
            $pessoa = new Pessoa();
            $cliente = new Cliente();
            $cliente->setPessoa($pessoa);
            $cliente->setIdcliente(0);
        }

        $retornoView = array();
        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $cliente;
        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente.html.twig', $retornoView);
    }

    public function clientesClienteDadosPessoaisAction(Request $request, $clienteId) {
        $cliente = ClienteQuery::create()->withColumn('Pessoa.nome', 'Nome')->join('Pessoa')->findPk($clienteId);

        if ($cliente == null) {
            $pessoa = new Pessoa();
            $cliente = new Cliente();
            $cliente->setPessoa($pessoa);
            $cliente->setIdcliente(0);
        }

        $retornoView = array();
        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $cliente;
        $retornoView['pessoa'] = $cliente->getPessoa();
        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-dados-pessoais.html.twig', $retornoView);
    }

    public function clientesClienteDadosPessoaisSalvarAction(Request $request, $clienteId) {
        $user = $this->getUser();
        $dataAtual = new \DateTime('now');

        $status = false;
        $erros = array();

        $importIdCliente = $request->get('importIdCliente', '');
        $dataContratoCliente = \DateTime::createFromFormat('d/m/Y', $request->get('dataContratoCliente', ''));
        $dataContratadoCliente = $dataContratoCliente <= $dataAtual ? $dataContratoCliente : null;

        $cliente = ClienteQuery::create()->findPk($clienteId);
        if ($cliente == null) {
            $cliente = new Cliente();
            $cliente->setAtivo(true);
            $cliente->setDataCadastro(new \DateTime('now'));
            $cliente->setDataContrato($dataContratadoCliente);
        }

        $ativo = $cliente->getAtivo();
        $clienteDataDoContrato = $cliente->getDataContrato();
        if (($clienteDataDoContrato == null || $clienteDataDoContrato == '0000-00-00') && $ativo) {
            $cliente->setDataContrato($dataContratadoCliente);
        }

        $cliente->setImportId($importIdCliente);
        $cliente->setDataAlterado(new \DateTime('now'));
        $cliente->setUsuarioAlterado($user->getIdusuario());

        if (!empty($importIdCliente)) {
            $verificarImportIdCliente = ClienteQuery::create()->where("idcliente != $clienteId")->findOneByImportId($importIdCliente);
            if ($verificarImportIdCliente != null) {
                $erros[] = 'Código de Importação do cliente já se encontra em uso.';
            }
        }

        if ($dataContratadoCliente == null) {
            $erros[] = 'Data do Contrato não informada ou a data é maior que a data atual.';
        }

        $tipoPessoa = $request->get('tipoPessoa', '');
        $nome = trim($request->get('nome', ''));
        $razaoSocial = trim($request->get('razaoSocial', ''));
        $email = trim($request->get('email', ''));
        $dataDeNascimento = trim($request->get('dataNascimento', ''));
        $dataNascimento = empty($dataDeNascimento) ? null : DateTime::createFromFormat('d/m/Y', $dataDeNascimento);
        $sexo = $request->get('sexo', 'm');
        $cpf = trim($request->get('cpf', ''));
        $rg = trim($request->get('rg', ''));
        $cnpj = trim($request->get('cnpj', ''));
        $inscricaoEstadual = trim($request->get('inscricaoEstadual', ''));
        $cep = trim($request->get('cep', ''));
        $rua = trim($request->get('endereco', ''));
        $numero = trim($request->get('num', ''));
        $complemento = trim($request->get('complemento', ''));
        $bairro = trim($request->get('bairro', ''));
        $cidadeId = trim($request->get('cidade', ''));
        $pontoReferencia = trim($request->get('referencia', ''));
        $observacao = trim($request->get('observacao', ''));
        $importIdPessoa = trim($request->get('importIdPessoa', ''));
        $eFornecedor = $request->get('efornecedor', false);

        if (empty($nome)) {
            $erros[] = 'Nome: Campo não pode ser vazio.';
        }

        if ($tipoPessoa == 'fisica') {
            if (empty($cpf)) {
                $erros[] = 'CPF: Campo não pode ser vazio.';
            } else {
                if (!Utils::validarCPF($cpf)) {
                    $erros[] = 'CPF: Campo inválido.';
                }
            }
        } else {
            if (empty($cnpj)) {
                $erros[] = 'CNPJ: Campo não pode ser vazio.';
            } else {
                if (!Utils::validarCNPJ($cnpj)) {
                    $erros[] = 'CNPJ: Campo inválido.';
                }
            }

            if (empty($razaoSocial)) {
                $erros[] = 'Razão Social: Campo não pode ser vazio.';
            }
        }

        if (empty($cep)) {
            $erros[] = 'CEP: Campo não pode ser vazio.';
        } else {
            // expressao regular para avaliar o cep
            $verificarCep = preg_match("/^[0-9]{5}-[0-9]{3}$/", $cep);
            $verificarCepNumerico = preg_match("/^[0-9]{8}$/", $cep);
            if (!$verificarCep && !$verificarCepNumerico) {
                $erros[] = 'CEP: Campo inválido.';
            }
        }

        $uf = '';
        $cidade = CidadeQuery::create()->findPk($cidadeId);
        if (empty($cidadeId)) {
            $erros[] = 'Cidade: Campo não pode ser vazio.';
        } else {
            if ($cidade == null) {
                $erros[] = "Cidade: Não foi possível localizar, Código do Município: $cidadeId";
            } else {
                $uf = $cidade->getUf();
            }
        }

        if (empty($uf)) {
            $erros[] = 'UF: Campo não pode ser vazio.';
        }

        $cep = str_replace(array('.', '-'), '', $cep);
        $pessoa = $cliente->getPessoa() == null ? new Pessoa() : $cliente->getPessoa();
        $pessoa->setNome($nome);
        $pessoa->setTipoPessoa($tipoPessoa);
        $pessoa->setRazaoSocial($razaoSocial);
        $pessoa->setEmail($email);
        $pessoa->setDataNascimento($dataNascimento);
        $pessoa->setEFornecedor($eFornecedor);

        if ($tipoPessoa == 'fisica') {
            $cpf = str_replace(array('.', '-'), '', $cpf);
            $pessoa->setCpfCnpj($cpf);
            $pessoa->setRgInscrest($rg);
            $pessoa->setSexo($sexo);
        } else {
            $cnpj = str_replace(array('.', '-', '/'), '', $cnpj);
            $pessoa->setCpfCnpj($cnpj);
            $pessoa->setRgInscrest($inscricaoEstadual);
        }

        $pessoa->setCep($cep);
        $pessoa->setRua($rua);
        $pessoa->setNum($numero);
        $pessoa->setComplemento($complemento);
        $pessoa->setBairro($bairro);
        $pessoa->setCidadeId($cidadeId);
        $pessoa->setPontoReferencia($pontoReferencia);
        $pessoa->setObs($observacao);
        $pessoa->setImportId($importIdPessoa);
        $pessoa->setUsuarioAlterado($user->getIdusuario());
        $pessoa->setDataAlterado(new DateTime('now'));

        if ($pessoa->getId() == null) {
            $pessoa->setDataCadastro(new DateTime('now'));
        }

        $validator = $this->get('validator');
        $errosValidacao = $validator->validate($pessoa);

        if (count($errosValidacao) == 0 && sizeof($erros) == 0) {
            // get the PDO connection object from Propel
            $con = Propel::getWriteConnection(PessoaTableMap::DATABASE_NAME);
            $con->beginTransaction();

            try {
                $pessoa->save();
                $cliente->setPessoa($pessoa);
                $cliente->save();
                //$endereco->save();
                
                $clienteId = $cliente->getIdcliente();
                $fornecedor = FornecedorQuery::create()->filterByPessoa($pessoa)->findOneByClienteId($clienteId);
                if ($fornecedor == null) {
                    $fornecedor = new Fornecedor();
                    $fornecedor->setCliente($cliente);
                    $fornecedor->setPessoa($pessoa);
                    $fornecedor->setDataCadastro(new DateTime('now'));
                    $fornecedor->setDataAlterado(new DateTime('now'));
                    $fornecedor->setUsuarioAlterado($user->getIdusuario());
                }

                if (!$eFornecedor) {                    
                    $fornecedor->setAtivo(false);
                    $fornecedor->setDataAlterado(new DateTime('now'));
                    $fornecedor->setUsuarioAlterado($user->getIdusuario());
                }

                $fornecedor->save();

                $con->commit();
                $status = true;
            } catch (PropelException $ex) {
                $con->rollback();

                $erros[] = $ex->getMessage();
            }
        }

        foreach ($errosValidacao as $erro) {
            $erros[] = $erro->getMessage();
        }

        $retornoView = array();
        //$retornoView['cliente'] = $cliente;
        $retornoView['status'] = $status;
        $retornoView['erros'] = $erros;

        if ($retornoView['status']) {
            $retornoView['mensagem'] = "Dados salvos com sucesso.";

            $params = array();
            $params['clienteId'] = $cliente->getIdcliente();

            $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_administrativo_clientes_cliente', $params);
        }

        return new JsonResponse($retornoView);
    }

    public function clientesClienteFornecedorAction(Request $request, $clienteId) {
        $cliente = ClienteQuery::create()->findPk($clienteId);
        $fornecedor = FornecedorQuery::create()->joinCliente()->findOneByClienteId($clienteId);
        if ($fornecedor == null) {
            $pessoa = $cliente->getPessoa();

            $fornecedor = new Fornecedor();
            $fornecedor->setPessoa($pessoa);
            $fornecedor->setCliente($cliente);
        }

        $bancosAgenciaConta = BancoAgenciaContaQuery::create()
                ->useBancoQuery()->withColumn('banco.nome', 'BancoNome')->endUse()
                ->useBancoAgenciaQuery()->withColumn('banco_agencia.agencia', 'BancoAgenciaNome')->endUse()
                ->find();

        $retornoView = array();
        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $cliente;
        $retornoView['fornecedor'] = $fornecedor;
        $retornoView['bancosAgenciaConta'] = $bancosAgenciaConta;

        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-fornecedor.html.twig', $retornoView);
    }

    public function clientesClienteFornecedorSalvarAction(Request $request, $clienteId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $bancoAgenciaContaId = trim($request->get('bancoAgenciaConta', ''));
        $observacao = trim($request->get('observacao', ''));

        $bancoAgenciaConta = BancoAgenciaContaQuery::create()->findPk($bancoAgenciaContaId);

        if ($bancoAgenciaConta == null) {
            $retornoView['erros'][] = "Conta: Campo não pode ser vazio.";
        }

        $fornecedor = FornecedorQuery::create()->findOneByClienteId($clienteId);
        if ($fornecedor == null) {
            $cliente = ClienteQuery::create()->findPk($clienteId);
            $pessoa = $cliente->getPessoa();

            $fornecedor = new Fornecedor();
            $fornecedor->setCliente($cliente);
            $fornecedor->setPessoa($pessoa);
            $fornecedor->setDataCadastro(new DateTime('now'));
        }

        $fornecedor->setBancoAgenciaConta($bancoAgenciaConta);
        $fornecedor->setObservacao($observacao);
        $fornecedor->setDataAlterado(new DateTime('now'));
        $fornecedor->setUsuarioAlterado($user->getIdusuario());

        if (sizeof($retornoView['erros']) == 0) {
            try {
                $fornecedor->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Fornecedor salvo com sucesso.';
            } catch (Exception $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function clientesClienteFornecedorToggleStatusAction(Request $request, $clienteId, $fornecedorId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $fornecedor = FornecedorQuery::create()->filterByClienteId($clienteId)->findPk($fornecedorId);
        if ($fornecedor == null) {
            $retornoView['erros'][] = "Fornecedor: Não foi possível localizar o fornecedor para este cliente.";
        }

        if (sizeof($retornoView['erros']) == 0) {
            $ativo = !$fornecedor->getAtivo();
            $fornecedor->setAtivo($ativo);
            $fornecedor->setDataAlterado(new DateTime('now'));
            $fornecedor->setUsuarioAlterado($user->getIdusuario());

            try {
                $fornecedor->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Fornecedor atualizado com sucesso.';
            } catch (Exception $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function clientesClienteTelefonesAction(Request $request, $clienteId) {
        $retornoView = array();
        $telefones = array();
        $cliente = ClienteQuery::create()->findPk($clienteId);
        if ($cliente != null) {
            $pessoa = $cliente->getPessoa();
            $telefones = $pessoa->getTelefones();
        }

        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $cliente;
        $retornoView['telefones'] = $telefones;

        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-telefones.html.twig', $retornoView);
    }

    public function clientesClienteTelefonesSalvarTelefoneAction(Request $request, $clienteId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $cliente = ClienteQuery::create()->findPk($clienteId);
        if ($cliente == null) {
            $retornoView['erros'][] = 'Cliente inexistente.';
        } else {
            $pessoa = $cliente->getPessoa();
            $telefoneId = $request->get('telefoneId', '0');
            $tipo = $request->get('tipoTelefone', '');
            $dddNumero = trim(str_replace(['(', ')', '-'], ['', '', ''], $request->get('numero', '')));
            $ddd = substr($dddNumero, 0, 2);
            $numero = substr($dddNumero, 3, strlen($dddNumero));

            //$telefone = TelefoneQuery::create()->filterByPessoa($pessoa)->filterByTipo($tipo)->filterByDdd($ddd)->filterByNumero($numero)->findPk($telefoneId);
            $telefone = TelefoneQuery::create()->findPk($telefoneId);
            if ($telefone == null) {
                $telefone = new Telefone();
                $telefone->setPessoa($pessoa);
            }

            $telefone->setTipo($tipo);
            $telefone->setDdd($ddd);
            $telefone->setNumero($numero);
            $telefone->setDataCadastro(new \DateTime('now'));
            $telefone->setDataAlterado(new \DateTime('now'));
            $telefone->setUsuarioAlterado($user->getIdusuario());

            try {
                $telefone->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = "Telefone adicionado com sucesso.";
            } catch (PropelException $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function clientesClienteTelefonesEditarTelefoneAction(Request $request, $clienteId, $telefoneId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $telefone = TelefoneQuery::create()->findPk($telefoneId);
        if ($telefone != null) {
            $retornoView['status'] = true;
            $retornoView['telefone'] = $telefone->toJson();
        } else {
            $retornoView['erros'][] = 'Telefone não encontrado.';
        }

        return new JsonResponse($retornoView);
    }

    public function clientesClienteTelefonesExcluirTelefoneAction(Request $request, $clienteId, $telefoneId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();
        $retornoView['mensagem'] = "Telefone excluído com sucesso.";

        $cliente = ClienteQuery::create()->findPk($clienteId);
        if ($cliente == null) {
            $retornoView['erros'][] = 'Cliente inexistente.';
        } else {
            $pessoa = $cliente->getPessoa();
            $telefone = TelefoneQuery::create()->filterByPessoa($pessoa)->findPk($telefoneId);
            if ($telefone != null) {
                $telefone->delete();
                $retornoView['status'] = true;
            } else {
                $retornoView['erros'][] = 'Telefone inexistente para o cliente: <strong>' . $pessoa->getNome() . '</strong>';
            }
        }

        return new JsonResponse($retornoView);
    }

    public function clientesClienteCancelamentoClienteAction(Request $request, $clienteId) {
        $cliente = ClienteQuery::create()->withColumn('Pessoa.nome', 'Nome')->join('Pessoa')->findPk($clienteId);
        $motivos = array();

        $retornoView = array();
        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $cliente;
        $retornoView['motivos'] = $motivos;
        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-cancelamento-cliente.html.twig', $retornoView);
    }

    public function clientesClienteCancelamentoClienteCancelarAction(Request $request, $clienteId) {

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array('Não implementado ainda!');

        return new JsonResponse($retornoView);
    }

    public function servicosAutocompleteAction(Request $request) {
        $query = $request->get('nome', '');
        $limit = $request->get('limit', '');
        $servicosPrestado = ServicoPrestadoQuery::create()
                        ->filterByAtivo(true)->where("nome like '%$query%'")->orderByNome('desc')->limit($limit)->find();

        return new JsonResponse($servicosPrestado->toArray());
    }

    public function clientesClienteServicosAction(Request $request, $clienteId) {
        $cliente = ClienteQuery::create()->withColumn('Pessoa.nome', 'Nome')->join('Pessoa')->findPk($clienteId);

        $retornoView = array();
        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $cliente;
        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-servicos.html.twig', $retornoView);
    }

    public function clientesClienteServicosServicoAction(Request $request, $clienteId, $servicoId) {
        $servicoCliente = ServicoClienteQuery::create()->filterByClienteId($clienteId)->findPk($servicoId);
        if ($servicoCliente == null || $servicoId == 0) {
            $cliente = ClienteQuery::create()->findPk($clienteId);
            $servicoPrestado = new ServicoPrestado(); //ServicoPrestadoQuery::create()->filterByAtivo(true)->findOne();            

            $servicoCliente = new ServicoCliente();
            $servicoCliente->setCliente($cliente);
            $servicoCliente->setServicoPrestado($servicoPrestado);
        }

        $cidades = CidadeQuery::create()->orderByMunicipio()->find();
        $servicos = ServicoPrestadoQuery::create()->findByAtivo(true);

        $retornoView = array();
        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $servicoCliente->getCliente();
        $retornoView['servico'] = $servicoCliente;
        $retornoView['cidades'] = $cidades;
        $retornoView['servicos'] = $servicos;
        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-servicos-servico.html.twig', $retornoView);
    }

    public function clientesClienteServicosExcluirServicoAction(Request $request, $clienteId, $servicoId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $boletoItem = BoletoItemQuery::create()->findByServicoClienteId($servicoId)->count();
        if ($boletoItem > 0) {
            $retornoView['erros'][] = 'Este serviço não pode ser excluído pois existem cobranças vinculadas a ele.';
        } else {
            $servico = ServicoClienteQuery::create()->findPk($servicoId);
            $enderecoServico = EnderecoServClienteQuery::create()->findByServicoClienteId($servicoId);

            try {
                $enderecoServico->delete();
                $servico->delete();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Serviço excluído com sucesso!';
            } catch (PropelException $ex) {
                //$con->rollBack();
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function servicosServicoPorIdAction(Request $request, $servicoId) {
        $servico = ServicoPrestadoQuery::create()
                ->withColumn('planos.nome', 'planoNome')
                ->withColumn('planos.valor', 'planoValor')
                ->joinPlanos()
                ->findPk($servicoId);
        $retorno = array();
        $retorno['servico'] = $servico->toJson();
        return new JsonResponse($retorno);
    }

    public function clientesClienteEnderecoCobrancaAction(Request $request, $clienteId) {
        $cliente = ClienteQuery::create()->findPk($clienteId);
        $endereco = EnderecoClienteQuery::create()->findOneByClienteId($clienteId);
        if ($endereco == null) {
            $endereco = new EnderecoCliente();
        }

        $retornoView = array();
        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $cliente;
        $retornoView['endereco'] = $endereco;
        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-endereco-cobranca.html.twig', $retornoView);
    }

    public function clientesClienteEnderecoCobrancaSalvarAction(Request $request, $clienteId) {
        $user = $this->getUser();

        // endereço do serviço
        $cep = $request->get('cep', '');
        $rua = $request->get('endereco', '');
        $numero = $request->get('num', '');
        $complemento = $request->get('complemento', '');
        $bairro = $request->get('bairro', '');
        $cidadeId = $request->get('cidade', '');
        $pontoReferencia = $request->get('pontoReferencia', '');

        $cidade = CidadeQuery::create()->findPk($cidadeId);
        $uf = $cidade->getUf();
        $enderecos = EnderecoClienteQuery::create()->findByClienteId($clienteId);

        $endereco = new EnderecoCliente();
        $endereco->setClienteId($clienteId);
        $endereco->setDataCadastro(new DateTime('now'));
        $endereco->setImportId(0);
        $endereco->setRua($rua);
        $endereco->setBairro($bairro);
        $endereco->setNum($numero);
        $endereco->setComplemento($complemento);
        $endereco->setCep($cep);
        $endereco->setUf($uf);
        $endereco->setPontoReferencia($pontoReferencia);
        $endereco->setCidade($cidade);
        $endereco->setDataAlterado(new DateTime('now'));
        $endereco->setUsuarioAlterado($user->getIdusuario());

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $con = Propel::getWriteConnection(EnderecoClienteTableMap::DATABASE_NAME);
        $con->beginTransaction();

        try {
            $enderecos->delete();
            $endereco->save();

            $retornoView['status'] = true;
            $retornoView['mensagem'] = 'Endereço salvo com sucesso!';

            $con->commit();
        } catch (PropelException $ex) {
            $con->rollBack();
            $retornoView['erros'][] = $ex->getMessage();
        }

        return new JsonResponse($retornoView);
    }

    public function clientesClienteServicosServicoSalvarAction(Request $request, $clienteId, $servicoId) {
        $user = $this->getUser();

        // dados do serviço do cliente
        $servicoPrestadoId = $request->get('servicoPrestadoId', '');
        $codigoContrato = $request->get('codCesta', '');
        $importId = $request->get('importId', '');
        $dataContratado = DateTime::createFromFormat('d/m/Y', $request->get('dataContratado', ''));
        $periodoReferencia = $request->get('periodoReferencia', 'corrente');
        /* $cancelado = $request->get('isCancelado', '0');
          $dataCancelado = DateTime::createFromFormat('d/m/Y', $request->get('dataCancelado', '')); */
        $codigoObra = trim($request->get('codigoObra', ''));
        $art = trim($request->get('art', ''));
        $descricaoNotaFiscal = $request->get('descricaoNotaFiscal', '');
        $valorDesconto = (double) str_replace(',', '.', str_replace('.', '', $request->get('valorDesconto', '0')));

        // endereço do serviço
        $cep = $request->get('cep', '');
        $rua = $request->get('endereco', '');
        $numero = $request->get('numero', '');
        $complemento = $request->get('complemento', '');
        $bairro = $request->get('bairro', '');
        $cidadeId = $request->get('cidade', '');
        $referencia = $request->get('pontoReferencia', '');

        // dados do contrato
        $tipoFaturamentoId = trim($request->get('tipoFaturamentoId', ''));
        $regraFaturamentoId = trim($request->get('regraFaturamentoId', ''));
        $temScm = $request->get('temScm', '0');
        $tipoScm = $request->get('tipoScm', null);
        $scm = (double) str_replace(',', '.', str_replace('.', '', $request->get('scm', null)));
        $descricaoScm = $request->get('descricaoScm', '');
        $temSva = $request->get('temSva', '0');
        $tipoSva = $request->get('tipoSva', null);
        $sva = (double) str_replace(',', '.', str_replace('.', '', $request->get('sva', null)));
        $descricaoSva = $request->get('descricaoSva', '');
        $forcarValor = $request->get('forcarValor', '0');
        $valorForcado = (double) str_replace(',', '.', str_replace('.', '', $request->get('valorForcado', null)));
        $gerarCobranca = $request->get('gerarCobranca', '0');
        $temReembolso = $request->get('temReembolso', '1');

        $servicoCliente = ServicoClienteQuery::create()->filterByClienteId($clienteId)->findPk($servicoId);
        if ($servicoCliente == null || $servicoId == 0) {
            $cliente = ClienteQuery::create()->findPk($clienteId);
            $servicoCliente = new ServicoCliente();
            $servicoCliente->setCliente($cliente);
            $servicoCliente->setDataCadastro(new DateTime('now'));
        }

        $servicoPrestado = ServicoPrestadoQuery::create()->findPk($servicoPrestadoId);
        $plano = $servicoPrestado->getPlano();
        $valorServico = $plano->getValor();
        
        if ($valorServico == 0.00) {
            $valorServico = $servicoCliente->getValor();
        }
        
        $tipoServico = $servicoPrestado->getTipo();

        $servicoCliente->setServicoPrestado($servicoPrestado);
        $servicoCliente->setCodCesta($codigoContrato);
        $servicoCliente->setImportId($importId);
        $servicoCliente->setDescricaoNotaFiscal($descricaoNotaFiscal);
        $servicoCliente->setTipoPeriodoReferencia($periodoReferencia);
        $servicoCliente->setDataAlterado(new DateTime('now'));
        $servicoCliente->setUsuarioAlterado($user->getIdusuario());
        $servicoCliente->setValorDesconto($valorDesconto);

        $contrato = $servicoCliente->Contrato();
        $contrato->setTipoFaturamentoId($tipoFaturamentoId);
        $contrato->setRegraFaturamentoId($regraFaturamentoId);
        $contrato->setTemScm($temScm);
        $contrato->setScm($scm);
        $contrato->setDescricaoScm($descricaoScm);
        $contrato->setTipoScm($tipoScm);
        $contrato->setTemSva($temSva);
        $contrato->setSva($sva);
        $contrato->setTipoSva($tipoSva);
        $contrato->setDescricaoSva($descricaoSva);
        $contrato->setForcarValor($forcarValor);
        $contrato->setGerarCobranca($gerarCobranca);
        $contrato->setCodigoObra($codigoObra);
        $contrato->setArt($art);
        $contrato->setTemReembolso($temReembolso);

        if ($dataContratado != null || !empty($dataContratado)) {
            $servicoCliente->setDataContratado($dataContratado);
            $contrato->setDataContratado($dataContratado);
        }

        /*
          if ($cancelado == '1') {
          $servicoCliente->setDataCancelado($dataCancelado);
          $contrato->setDataCancelado($dataCancelado);
          } else {
          $servicoCliente->setDataCancelado(null);
          $contrato->setDataCancelado(null);
          } */

        $retornoView = array();
        $retornoView['cliente'] = $servicoCliente->getCliente();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        if ($tipoSva == true && $tipoServico == 'taxa' && (empty($codigoObra) || empty($art))) {
            $retornoView['erros'][] = 'Por favor, informe o Código de Obra e ART.<br/>';
        }

        // verifica dados do contrato e do serviço
        if ($plano == null) {
            $retornoView['erros'][] = 'Plano não foi selecionado.<br/>';
        }

        if ($servicoPrestado == null) {
            $retornoView['erros'][] = 'Serviço Prestado não foi selecionado.<br/>';
        }

        if ($temScm == '1') {
            if (empty($scm)) {
                $retornoView['erros'][] = 'SCM não pode ser vazio.<br/>';
            }
        } else {
            $scm = 0.0;
            $tipoScm = null;
        }

        if ($temSva == '1') {
            if (empty($sva)) {
                $retornoView['erros'][] = 'SVA não pode ser vazio.<br/>';
            }
        } else {
            $sva = 0.0;
            $tipoSva = null;
        }

        if ($temScm == '0' && $temSva == '0') {
            $retornoView['erros'][] = 'Por favor, informe se o Serviço Prestado possuirá SCM ou SVA.<br/>';
        }

        if ($forcarValor == '1') {
            $valorServico = $valorForcado;
            $contrato->setValor($valorForcado);
        } else {
            $contrato->setValor(null);
        }

        if ($valorDesconto > $valorServico) {
            $retornoView['erros'][] = 'Valor do Desconto não pode ser maior do que o Valor do Serviço.<br/>';
        } elseif ($valorDesconto < 0) {
            $retornoView['erros'][] = 'Valor do Desconto não pode ser menor do que zero.';
        }

        $servicoCliente->setValor($valorServico);
        //$contrato->setValor($valorServico);
        $valorServico -= $valorDesconto;
        if ($temScm == '1' && $temSva == '1') {
            $valor = $scm + $sva;

            if (($tipoScm == 'valor' && $tipoSva == 'percentual') || ($tipoScm == 'percentual' && $tipoSva == 'valor')) {
                $retornoView['erros'][] = 'Os Tipos de SCM e SVA precisam ser iguais.<br/>';
            } elseif (($tipoScm == 'percentual' && $tipoSva == 'percentual') && $valor != 100.0) {
                $retornoView['erros'][] = 'Porcentagem do SCM e SVA não podem ser diferentes de 100%.<br/>';
            } elseif (($tipoScm == 'valor' && $tipoSva == 'valor') && $valor != $valorServico) {
                $retornoView['erros'][] = 'Valor do SCM e SVA devem ser iguais ao Valor do Serviço junto ao Valor do Desconto.<br/>';
            }
        } elseif ($temScm == '1' && $temSva == '0') {
            if ($tipoScm == 'percentual' && $scm != 100.0) {
                $retornoView['erros'][] = 'Porcentagem do SCM não pode ser diferente de 100%.<br/>';
            } elseif ($tipoScm == 'valor' && $scm != $valorServico) {
                $retornoView['erros'][] = 'Valor do SCM deve ser igual ao Valor do Serviço junto ao Valor do Desconto.<br/>';
            }
        } elseif ($temScm == '0' && $temSva == '1') {
            if ($tipoSva == 'percentual' && $sva != 100.0) {
                $retornoView['erros'][] = 'Porcentagem do SVA não pode ser diferente de 100%.<br/>';
            } elseif ($tipoSva == 'valor' && $sva != $valorServico) {
                $retornoView['erros'][] = 'Valor do SVA deve ser igual ao Valor do Serviço junto ao Valor do Desconto.<br/>';
            }
        }

        if ($temScm == '1' && empty($tipoScm)) {
            $retornoView['erros'][] = 'Tipo do SCM não pode ser vazio.<br/>';
        }

        if ($temSva == '1' && empty($tipoSva)) {
            $retornoView['erros'][] = 'Tipo do SVA não pode ser vazio.<br/>';
        }

        if ($forcarValor == '1') {
            if (empty($valorForcado)) {
                $retornoView['erros'][] = 'Valor Forçado não pode ser vazio.<br/>';
            }
        }

        if (empty($tipoFaturamentoId)) {
            $retornoView['erros'][] = 'Tipo do Faturamento não foi selecionado.<br/>';
        }

        if (empty($regraFaturamentoId)) {
            $retornoView['erros'][] = 'Regra de Faturamento não foi selecionado.<br/>';
        }


        // verificar dados do endereço
        if (empty($cep)) {
            $retornoView['erros'][] = 'CEP: Campo não pode ser vazio.<br/>';
        } else {
            // expressao regular para avaliar o cep
            $verificarCep = preg_match("/^[0-9]{5}-[0-9]{3}$/", $cep);
            $verificarCepNumerico = preg_match("/^[0-9]{8}$/", $cep);
            if (!$verificarCep && !$verificarCepNumerico) {
                $retornoView['erros'][] = 'CEP: Campo inválido.<br/>';
            }
        }

        if (empty($rua)) {
            $retornoView['erros'][] = 'Rua: Campo não pode ser vazio.<br/>';
        }

        $uf = '';
        $cidade = CidadeQuery::create()->findPk($cidadeId);
        if (empty($cidadeId)) {
            $retornoView['erros'][] = 'Cidade: Campo não pode ser vazio.<br/>';
        } else {
            if ($cidade == null) {
                $retornoView['erros'][] = "Cidade: Não foi possível localizar, Código do Município: $cidadeId <br/>";
            } else {
                $uf = $cidade->getUf();
            }
        }

        if (empty($uf)) {
            $retornoView['erros'][] = 'UF: Campo não pode ser vazio.<br/>';
        }

        $cep = str_replace(array('.', '-'), '', $cep);
        $endereco = EnderecoServClienteQuery::create()->findOneByServicoClienteId($servicoId);
        if ($endereco == null) {
            $endereco = new EnderecoServCliente();
            $endereco->setDataCadastro(new DateTime('now'));
            $endereco->setImportId(0);
        }

        $endereco->setRua($rua);
        $endereco->setBairro($bairro);
        $endereco->setNum($numero);
        $endereco->setComplemento($complemento);
        $endereco->setCep($cep);
        $endereco->setUf($uf);
        $endereco->setPontoReferencia($referencia);
        $endereco->setCidade($cidade);
        $endereco->setDataAlterado(new DateTime('now'));
        $endereco->setUsuarioAlterado($user->getIdusuario());

        if (sizeof($retornoView['erros']) == 0) {
            $con = Propel::getWriteConnection(ServicoClienteTableMap::DATABASE_NAME);
            $con->beginTransaction();

            try {
                $contrato->save();
                $servicoCliente->setContratoId($contrato->getIdcontrato());
                $servicoCliente->save();

                $endereco->setServicoCliente($servicoCliente);
                $endereco->save();

                $con->commit();
                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Salvo com sucesso! <br/>';
            } catch (Exception $ex) {
                $con->rollBack();
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function clientesClienteNotasFiscaisNotasPorServicoAction(Request $request, $clienteId, $servicoId) {
        $servicoCliente = ServicoClienteQuery::create()->filterByClienteId($clienteId)->findPk($servicoId);

        $notas = array();
        foreach ($servicoCliente->getNotas() as $notasPorServico) {
            foreach ($notasPorServico as $nota) {
                $notas[] = $nota;
            }
        }

        $retornoView = array();
        $retornoView['servicoCliente'] = $servicoCliente;
        $retornoView['cliente'] = $servicoCliente->getCliente();
        $retornoView['notas'] = $notas;

        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-notas-fiscais-notas-por-servico.html.twig', $retornoView);
    }

    public function clientesClienteNotasFiscaisNotasPorServicoGerarNotaFiscalAction(Request $request, $clienteId, $servicoId) {
        $user = $this->getUser();
        $competencia = $this->getCompetenciaAtual();
        $dataDaEmissao = DateTime::createFromFormat('d/m/Y', $request->get('dataEmissao', ''));
        $dataEmissao = $dataDaEmissao->format('Y-m-d');
        // acrescentar coluna na cobranca para verificar se já foi gerada uma nota para a mesma
        // isso irá servir para controlar a geração de parcelas nos boletos que já foram gerados anteriormente

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();
        $retornoView['geracao'] = array();

        $parcelas = BoletoItemQuery::create()
                ->joinBoleto()
                ->filterByServicoClienteId($servicoId)
                ->where("base_calculo_scm > 0 and competencia_id = " . $competencia->getId())
                ->find();

        if (sizeof($parcelas)) {
            $cobrancasGeracao = array();
            foreach ($parcelas as $parcela) {
                $cobranca = $parcela->getBoleto();
                $cobrancaEstaRegistrada = $cobranca->getRegistrado();
                $cobrancaNotaScmGerada = $cobranca->getNotaScmGerada();
                $cobrancaNotaCancelada = $cobranca->getNotaCancelada();

                if (!$cobrancaNotaScmGerada && !$cobrancaNotaScmCancelada) {
                    $cobrancasGeracao[] = $cobranca;
                }

                /*
                  $dadosFiscaisGeracao = $parcela->getDadosFiscaisGeracaos(); //$cobranca->getBoletoItems()

                  $dadoFiscalGeracao = sizeof($dadosFiscaisGeracao) ? $dadosFiscaisGeracao[0] : null;
                  if ($dadoFiscalGeracao == null) {
                  $retornoView['erros'][] = 'Não foi encontrado dados fiscais geração';
                  $dadoFiscalGeracao = new DadosFiscaisGeracao();
                  $dadoFiscalGeracao->setStatus('a gerar');
                  $dadoFiscalGeracao->setBoletoItem($parcela);
                  $dadoFiscalGeracao->setCompetenciaId($competencia->getId());
                  $dadoFiscalGeracao->setDataCadastro(new DateTime('now'));
                  $dadoFiscalGeracao->setDataAlterado(new DateTime('now'));
                  $dadoFiscalGeracao->setUsuarioAlterado($user->getIdusuario());
                  $dadoFiscalGeracao->save();
                  }

                  $statusGeracao = $dadoFiscalGeracao->getStatus();
                  if ($statusGeracao == 'a gerar') {
                  $cobrancasGeracao[] = $cobranca;
                  } */
            }

            $situacao = 'N';
            $retorno = $this->criarNotaFiscal($cobrancasGeracao, $dataEmissao, $situacao);
            $retornoView['status'] = $retorno['status'];
            $retornoView['erros'] = $retorno['erros'];
        } else {
            $retornoView['erros'][] = 'Não foi possível gerar a nota fiscal, pois não existem cobranças para este serviço nesta competência.';
        }

        return new JsonResponse($retornoView);
    }

    /* fim geração de notas fiscais */

    public function clientesClienteNotasFiscaisAction(Request $request, $clienteId) {
        $cliente = ClienteQuery::create()->findPk($clienteId);

        $notas = array();
        foreach ($cliente->getNotas() as $notasCliente) {
            foreach ($notasCliente as $nota) {
                $notas[] = $nota;
            }
        }

        $retornoView = array();
        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $cliente;
        $retornoView['notas'] = $notas;

        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-notas-fiscais.html.twig', $retornoView);
    }

    public function clientesClienteNotasFiscaisGeracaoNotaAction(Request $request, $clienteId) {
        $cliente = ClienteQuery::create()->findPk($clienteId);
        $servicosCliente = ServicoClienteQuery::create()->findByClienteId($clienteId);
        $competencia = $this->getCompetenciaAtual();

        $retornoView = array();
        $retornoView['clienteId'] = $clienteId;
        $retornoView['cliente'] = $cliente;
        $retornoView['servicos'] = $servicosCliente;
        $retornoView['competenciaAtual'] = $competencia;

        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-notas-fiscais-geracao.html.twig', $retornoView);
    }

    public function clientesClienteNotasFiscaisGeracaoNotaCobrancasPorServicoAction(Request $request, $clienteId, $servicoClienteId) {
        $cobrancas = BoletoQuery::create()->useBoletoItemQuery()->filterByServicoClienteId($servicoClienteId)->endUse()
                        ->filterByNotaScmGerada(FALSE)->groupByIdboleto()->find();

        $retornoView = array();
        $retornoView['cobrancas'] = $cobrancas;

        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-notas-fiscais-geracao-cobrancas-por-servico.html.twig', $retornoView);
    }

    public function clientesClienteNotasFiscaisGeracaoNotaGerarAction(Request $request, $clienteId) {
        $cobrancasIds = $request->get('cobrancas', '');
        if (empty($cobrancasIds)) {
            return new JsonResponse(array('status' => false, 'erros' => array('Por favor, selecione uma cobrança para a geração da Nota.')));
        }

        $cobrancasIds = implode(",", $cobrancasIds);
        $cobrancas = BoletoQuery::create()->filterByClienteId($clienteId)->where("base_calculo_scm > 0 and idboleto in ($cobrancasIds)")->find();
        //$cobrancas = BoletoQuery::create()->filterByClienteId($clienteId)->where("idboleto in ($cobrancasIds)")->find();

        if ($cobrancas == null || sizeof($cobrancas) == 0) {
            return new JsonResponse(array('status' => false, 'erros' => array('Por favor, verifique se a cobrança ainda existe, pois não foi possível localiza-la na base de dados.')));
        }

        $emissao = $request->get('dataEmissao', '');
        if (empty($emissao)) {
            $competencia = $this->getCompetenciaAtual();
            $dataEmissao = $competencia->getUltimaDataEmissao();
        } else {
            $dataDaEmissao = DateTime::createFromFormat('d/m/Y', $emissao);
            $dataEmissao = $dataDaEmissao->format('Y-m-d');
        }

        $notas = array();
        foreach ($cobrancas as $cobranca) {
            $cobrancaNotaScmGerada = $cobranca->getNotaScmGerada();
            $cobrancaNotaScmCancelada = $cobranca->getNotaScmCancelada();

            if (!$cobrancaNotaScmGerada && !$cobrancaNotaScmCancelada) {
                $notas[] = $cobranca;
            }
        }

        // verificar data de emissão se não existir 
        // e pegar a ultima data de emissão da competencia, para casos em que seja apenas regeração da nota
        $situacao = 'N';
        $retorno = $this->criarNotaFiscal($notas, $dataEmissao, $situacao);

        $retornoView = array();
        $retornoView['status'] = $retorno['status'];
        $retornoView['erros'] = $retorno['erros'];

        if ($retornoView['status']) {
            $retornoView['mensagem'] = "Nota gerada com sucesso.";
        }

        return new JsonResponse($retornoView);
    }

    public function clienteNotasFiscaisVisualizarNotaScmAction(Request $request, $clienteId, $notaFiscalId) {
        $nota = DadosFiscalQuery::create()->filterByTipoNota('scm')->findPk($notaFiscalId);

        if ($nota == null) {
            $request->getSession()->set('ErrorPageMessage', 'Não foi encontrada à Nota Fiscal de SCM.');

            return $this->redirectToRoute('ima_telecom_admin_error_page_404');
        }

        $filename = $this->geracaoArquivoNotaFiscalPdf($nota);
        return new BinaryFileResponse($filename);
    }

    public function clientesClienteCobrancasAction(Request $request, $clienteId) {
        $cliente = ClienteQuery::create()->findPk($clienteId);

        $retornoView = array();
        $retornoView['cliente'] = $cliente;
        $retornoView['boletos'] = $cliente->getBoletos();
        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-cobrancas.html.twig', $retornoView);
    }

    public function clientesClienteCobrancasGeracaoAction(Request $request, $clienteId) {
        //$cliente = ClienteQuery::create()->withColumn('Pessoa.nome', 'Nome')->join('Pessoa')->findPk($clienteId);
        $servicosCliente = ServicoClienteQuery::create()->findByClienteId($clienteId);

        $retornoView = array();
        $retornoView['servicos'] = $servicosCliente;
        $retornoView['clienteId'] = $clienteId;

        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-cobrancas-geracao.html.twig', $retornoView);
    }

    // refazer
    public function clientesClienteCobrancasGeracaoGerarAction(Request $request, $clienteId) {
        /*
          $competencia = $this->getCompetenciaAtual();
          //$vencimento = DateTime::createFromFormat('d/m/Y', $request->get('vencimento', ''));
          $cobrancas = $request->get('cobrancas', array());
          $retornoView = $this->geracaoCobrancas($competencia, $cobrancas); */

        $retornoView = array('status' => false, 'erros' => array());

        $competencia = $this->getCompetenciaAtual();
        $forcarVencimento = $request->get('forcarVencimento', '0');
        $vencimento = $request->get('vencimento', '');
        $cobrancas = $request->get('cobrancas', array());
        $cobrancaSeparada = (bool) $request->get('cobrancaSeparada', false);
        
        if (sizeof($cobrancas) > 0) {
            if ($forcarVencimento == '1') {
                $retornoView = $this->geracaoCobrancas($competencia, $cobrancas, $cobrancaSeparada, $vencimento);
            } else {
                $retornoView = $this->geracaoCobrancas($competencia, $cobrancas, $cobrancaSeparada);
            }
        } else {
            $retornoView['erros'][] = 'Não foi encontrado nenhuma cobrança para a geração.';
        }

        return new JsonResponse($retornoView);
    }

    public function clientesClienteCobrancasVisualizarCobrancaAction(Request $request, $clienteId, $cobrancaId) {
        $parametros = array();

        // ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
        $parametros["codigo_banco_com_dv"] = "033-7";
        $parametros["local_pagamento"] = "Pagável em qualquer agência bancária.";  // Descrição do local de pagamento do boleto
        $parametros["beneficiario"] = "IMA TELECOM LTDA - ME"; // Nome da Empresa ou Algum endereço web
        $parametros["numero_beneficiario"] = "10.934.273/0001-67"; // CPF ou CNPJ da empresa
        $parametros["agencia_beneficiario"] = "0822"; // Agencia: 0822-2
        $parametros["agencia_beneficiario_dv"] = "2"; // Agencia: 0822-2
        $parametros["codigo_beneficiario"] = "8782750";  // Beneficiário/Convênio/PSK: 8782750
        $parametros["endereco_beneficiario"] = "Rua Salvador Correa, 139, Sala 06, Centro - CEP: 28.035-310 - Campos/RJ";  // Rua, Número, Complemento, CEP, Bairro, Cidade/UF
        $parametros["email_beneficiario"] = "contato@imatelecom.com.br";
        $parametros["telefone_beneficiario"] = "(22) 2726-2747";

        // DADOS DO BOLETO
        $parametros["especie_documento"] = "";  // Espécie do Documento
        $parametros["aceite"] = "Não";  // Não ou Sim
        $parametros["descricao_carteira"] = "COB. SIMPLES - RCR";  // Descrição da Carteira
        $parametros["especie"] = "R$";
        $parametros["quantidade"] = "001";
        $parametros["valor"] = "";
        $parametros["nome_empresa"] = "IMA TELECOM LTDA - ME"; // Nome da empresa

        /*
          $where = "";
          if (!empty($idboleto)) {
          $where = " and idboleto = $idboleto ";
          }

          $boletos = $this->getBoletosLancamentos($where); //$this->getCobrancasSantander($where);
         */
        $cobrancas = BoletoQuery::create()
                ->filterByClienteId($clienteId)
                ->filterByIdboleto($cobrancaId)
                ->filterByRegistrado(true)
                ->find();

        $ambiente = $this->get('kernel')->getEnvironment();
        $bancoId = 20;
        $dadosCobranca = $this->getDadosBancarioSantander($bancoId);
        $boletoCNAB240 = new BoletoSantanderCNAB240($dadosCobranca);
        $boletoCNAB240->setParametrosVisualizacaoBoleto($parametros);
        $retorno = $boletoCNAB240->getBoletoCobranca($cobrancas, $ambiente);

        return $this->render('ImaTelecomBundle:Financeiro:visualizar-cobranca-santander.html.twig', $retorno);
        /*
          echo $bar = $retorno['boletos'][0]['codigo_barras_texto'];
          $generator = new \ImaTelecomBundle\Lib\BarCodeGenerator($bar);
          //var_dump($generator);

          $pdf = new \ImaTelecomBundle\Lib\ExtendedFPDF('P', 'cm', 'A4');
          $pdf->SetTopMargin(0);
          $pdf->SetLeftMargin(0);
          $pdf->SetRightMargin(0);
          $pdf->SetAutoPageBreak(true, 0, 0);
          $pdf->AddPage('L');

          $pdf->setXY(5,5);
          $pdf->Image($this->get('kernel')->getRootDir() . '/../web/barcode.gif');

          //echo '<img src="/ImaTelecomNovo/web/barcode.gif" />';


          $caminhoArquivoSalvo = "tmp_teste.pdf";
          $pdf->Output($caminhoArquivoSalvo, "F");

          return new BinaryFileResponse($caminhoArquivoSalvo); */
    }

    public function clientesClienteCobrancasDetalhesCobrancaAction(Request $request, $clienteId, $cobrancaId) {
        $cobranca = BoletoQuery::create()->filterByClienteId($clienteId)->findPk($cobrancaId);
        $retornoView = array();
        $retornoView['cobranca'] = $cobranca;

        return $this->render('ImaTelecomBundle:Administrativo:clientes-cliente-cobrancas-detalhes-cobranca.html.twig', $retornoView);
    }

    public function clientesClienteCobrancasExcluirCobrancaAction(Request $request, $clienteId, $cobrancaId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $cobranca = BoletoQuery::create()->filterByClienteId($clienteId)->findPk($cobrancaId);
        $parcelas = $cobranca->getBoletoItems();

        $cobrancaEstaRegistrada = $cobranca->getRegistrado();
        $cobrancaEstaPaga = $cobranca->getEstaPago();

        if (!$cobrancaEstaRegistrada && !$cobrancaEstaPaga) {
            $possuiNota = !$cobranca->hasNotaFiscal();
            $possuiLancamento = !$cobranca->hasLancamentos();
            if ($possuiNota && $possuiLancamento) {
                $con = Propel::getWriteConnection(BoletoTableMap::DATABASE_NAME);
                $con->beginTransaction();

                try {
                    foreach ($parcelas as $parcela) {
                        $dadosFiscais = DadosFiscaisGeracaoQuery::create()->findOneByBoletoItemId($parcela->getIdboletoItem());
                        if ($dadosFiscais != null) {
                            $status = $dadosFiscais->getStatus();
                            if ($status == 'a gerar') {
                                $dadosFiscais->delete();
                            } else {
                                $retornoView['erros'][] = 'Esta cobrança possui Notas Fiscais vinculados a ela.';
                            }
                        }
                    }

                    $parcelas->delete();
                    $cobranca->delete();

                    $con->commit();

                    $retornoView['status'] = true;
                    $retornoView['mensagem'] = 'Cobrança excluída com sucesso.';
                    $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_cobrancas');
                } catch (PropelException $ex) {
                    $con->rollback();
                    $retornoView['erros'][] = $ex->getMessage();
                }
            } else {
                $retornoView['erros'][] = !$possuiNota ? 'Esta cobrança possui Notas Fiscais vinculados a ela.' : null;
                $retornoView['erros'][] = !$possuiLancamento ? 'Esta cobrança possui Lançamentos vinculados a ela.' : null;
                $retornoView['erros'] = array_filter($retornoView['erros']);
            }
        } else {
            $retornoView['erros'][] = $cobrancaEstaRegistrada ? 'Esta cobrança se encontrada registrada.' : null;
            $retornoView['erros'][] = $cobrancaEstaPaga ? 'Esta cobrança já se encontrada paga.' : null;
            $retornoView['erros'] = array_filter($retornoView['erros']);
        }

        return new JsonResponse($retornoView);
    }

    public function sicisAction(Request $request) {
        $sicis = SiciQuery::create()->orderByCompetenciaId(Criteria::DESC)->find();

        $retornoView = array();
        $retornoView['sicis'] = $sicis; //->toArray();
        return $this->render('ImaTelecomBundle:Administrativo:sicis.html.twig', $retornoView);
    }

    public function gerarSiciAction(Request $request) {
        $erros = array();
        $retornoView = array();

        $user = $this->getUser();
        $competenciaAtual = $this->getCompetenciaAtual();
        //$competenciaId = $competenciaAtual->getProximaCompetencia();
        $competenciaId = $competenciaAtual->getId(); // trocado o mes e ano pela competencia and year(vencimento) = $ano and month(vencimento) = $mes

        $proximaCompetenciaId = $competenciaAtual->getProximaCompetencia();
        $competencia = CompetenciaQuery::create()->findPk($proximaCompetenciaId);

        $ano = $competencia->getAno();
        $mes = $competencia->getMes();
        $filename = 'SICI_' . $ano . '_' . $mes . '_' . time() . '.xls';
        $diretorioSici = $this->getParameter('repositorio_sicis');
        $caminhoArquivo = "$diretorioSici/$filename";

        if (!file_exists($diretorioSici)) {
            mkdir($diretorioSici, 0777, true);
        }

        $sql = "select a.idservico_cliente as id, download, upload, franquia, garantia_banda as GarantiaBanda,
tecnologia, h.idcidade as codCidade, tipo_pessoa

from servico_cliente a, servico_prestado b, endereco_serv_cliente c, boleto_item d,
boleto e, cliente f, pessoa g, cidade h, endereco_cliente ec, dados_fiscal df
where a.servico_prestado_id = b.idservico_prestado and a.idservico_cliente = c.servico_cliente_id and
a.idservico_cliente = d.servico_cliente_id and d.boleto_id = e.idboleto and a.cliente_id = f.idcliente
and f.pessoa_id = g.id and c.cidade_id = h.idcidade and c.cidade_id in (3188, 3192) and b.tipo = 'internet'
and f.idcliente = ec.cliente_id and df.boleto_id = e.idboleto and g.id not in (1106,9739)
and df.competencia_id = $competenciaId group by idservico_cliente"; // trocado o mes e ano pela competencia and year(vencimento) = $ano and month(vencimento) = $mes

        $con = Propel::getConnection();
        $stmt = $con->prepare($sql);
        $stmt->execute();
        $itens = $stmt->fetchall();

        $sql2 = "select count(distinct idboleto) as boletos, count(distinct idservico_cliente) as pontos, sum(valor) as valor
from (select e.idboleto, a.idservico_cliente, download, upload, franquia, garantia_banda as GarantiaBanda, 
tecnologia, h.idcidade as codCidade, tipo_pessoa, d.valor, g.nome, g.rua, g.num, h.municipio, g.uf, g.cpf_cnpj, g.rg_inscrest, g.bairro
from servico_cliente a, servico_prestado b, endereco_serv_cliente c, boleto_item d,
boleto e, cliente f, pessoa g, cidade h, endereco_cliente ec, dados_fiscal df
where a.servico_prestado_id = b.idservico_prestado and a.idservico_cliente = c.servico_cliente_id and
a.idservico_cliente = d.servico_cliente_id and d.boleto_id = e.idboleto and a.cliente_id = f.idcliente
and f.pessoa_id = g.id and ec.cidade_id = h.idcidade and c.cidade_id in (3188, 3192) and b.tipo = 'internet'
and f.idcliente = ec.cliente_id and df.boleto_id = e.idboleto and g.id not in (1106,9739)
and df.competencia_id = $competenciaId group by d.idboleto_item) as T";

        // Este sql será para pegar o valor do repasse da ima baseado no censanet
        $sqlRepasseIma = "select count(distinct idboleto) as boletos, count(distinct idservico_cliente) as pontos, sum(valor) as valor
from (select e.idboleto, a.idservico_cliente, download, upload, franquia, garantia_banda as GarantiaBanda, 
tecnologia, h.idcidade as codCidade, tipo_pessoa, d.valor, g.nome, g.rua, g.num, h.municipio, g.uf, g.cpf_cnpj, g.rg_inscrest, g.bairro
from servico_cliente a, servico_prestado b, endereco_serv_cliente c, boleto_item d,
boleto e, cliente f, pessoa g, cidade h, endereco_cliente ec, dados_fiscal df
where a.servico_prestado_id = b.idservico_prestado and a.idservico_cliente = c.servico_cliente_id and
a.idservico_cliente = d.servico_cliente_id and d.boleto_id = e.idboleto and a.cliente_id = f.idcliente
and f.pessoa_id = g.id and ec.cidade_id = h.idcidade and c.cidade_id in (3188, 3192) and b.tipo = 'internet'
and f.idcliente = ec.cliente_id and df.boleto_id = e.idboleto and f.import_id != 0
and df.competencia_id = $competenciaId group by d.idboleto_item) as T";


        $stmtInformacoes = $con->prepare($sql2);
        $stmtInformacoes->execute(); //->fetchall();
        $informacoes = $stmtInformacoes->fetchall();

        $siciModel = new SiciModel();
        $siciModel->setNomeArquivo($filename);
        $siciModel->setAno($ano);
        $siciModel->setMes($mes);
        $siciModel->setQtdeNotas($informacoes[0]['boletos']);
        $siciModel->setQtdePontos($informacoes[0]['pontos']);
        $siciModel->setValorTotal($informacoes[0]['valor']);
        $siciModel->setDataCadastro(new \DateTime('now'));
        $siciModel->setDataAlterado(new \DateTime('now'));
        $siciModel->setUsuarioAlterado($user->getIdusuario());
        $siciModel->setCompetenciaId($competenciaAtual->getId());

        try {
            $siciModel->save();
        } catch (PropelException $ex) {
            $erros[] = $ex->getMessage();
        }

        $phpExcel = $this->get('phpexcel');
        $sici = new Sici();
        $excel = $sici->gerarExcel($phpExcel, $itens);

// Creating streamed excel file created with php
// create the writer
        $writer = $this->get('phpexcel')->createWriter($excel, 'Excel5');
        $writer->save($caminhoArquivo);

        /*
          // create the response
          $response = $this->get('phpexcel')->createStreamedResponse($writer);
          // adding headers
          $dispositionHeader = $response->headers->makeDisposition(
          ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename
          );

          $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
          $response->headers->set('Pragma', 'public');
          $response->headers->set('Cache-Control', 'maxage=1');
          $response->headers->set('Content-Disposition', $dispositionHeader);

          return $response;
         * 
         */

        $status = sizeof($erros) ? false : true;
        $retornoView['status'] = $status;
        $retornoView['erros'] = $erros;

        if ($status) {
            $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_administrativo_sicis');
            $retornoView['mensagem'] = 'Arquivo gerado com sucesso.';
        }

        return new JsonResponse($retornoView);
    }

    public function siciDownloadSiciAction(Request $request, $siciId) {
        $sici = SiciQuery::create()->findPk($siciId);

        $filename = $sici->getNomeArquivo();
        $file = $this->getParameter('repositorio_sicis') . '/' . $filename;

        if (file_exists($file) && !empty($filename)) {
            /*
              $phpExcelObject = \PHPExcel_IOFactory::load($file);
              $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');

              $response = $this->get('phpexcel')->createStreamedResponse($writer);
              $dispositionHeader = $response->headers->makeDisposition(
              ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename
              );

              $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
              $response->headers->set('Pragma', 'public');
              $response->headers->set('Cache-Control', 'maxage=1');
              $response->headers->set('Content-Disposition', $dispositionHeader);

              return $response; */
            $filename = str_replace('.xls', '.zip', $filename);
            $filePath = 'tmp/' . $filename;

            $zip = new \ZipArchive();
            if ($zip->open($filePath, \ZIPARCHIVE::CREATE)) {
                $zip->addFile($file, str_replace($this->getParameter('repositorio_sicis') . '/', '', $file));
            }

            $zip->close();

            $retornoView = array();
            $retornoView['status'] = true;
            $retornoView['download_url'] = $request->getBasePath() . '/' . $filePath;

            return new JsonResponse($retornoView);
        } else {
            $retornoView = array();
            $retornoView['status'] = false;
            $retornoView['erros'] = array();
            $retornoView['erros'][] = "O arquivo $filename não foi encontrado em nossos servidores.";

            return new JsonResponse($retornoView);

            //return $this->render('ImaTelecomBundle:Administrativo:sicis.html.twig', $retornoView);
        }
    }

    public function siciExcluirSiciAction(Request $request, $siciId) {
        $erros = array();
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();
        $retornoView['mensagem'] = '';

        $sici = SiciQuery::create()->findPk($siciId);
        if ($sici != null) {
            $filename = $sici->getNomeArquivo();
            $file = $this->getParameter('repositorio_sicis') . '/' . $filename;

            if (file_exists($file) && !empty($filename)) {
                try {
                    unlink($file);
                } catch (Exception $ex) {
                    $retornoView['erros'][] = $ex->getMessage();
                }
            } else {
                $retornoView['mensagem'] .= "O arquivo $filename não foi localizado no repositório.<br/>";
            }

            if (sizeof($retornoView['erros']) == 0) {
                try {
                    $sici->delete();

                    $retornoView['status'] = true;
                    $retornoView['mensagem'] .= "O arquivo $filename foi excluído com sucesso.";
                    $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_administrativo_sicis');
                } catch (PropelException $ex) {
                    $retornoView['erros'][] = $ex->getMessage();
                }
            }
        } else {
            $retornoView['erros'][] = 'Não foi encontrado nenhum sici com este código ' . $siciId;
        }

        return new JsonResponse($retornoView);
    }

    public function servicosPrestadosAction(Request $request) {
        $servicos = ServicoPrestadoQuery::create()
                ->orderByNome(Criteria::DESC)
                ->find();

        $retornoView = array();
        $retornoView['servicos'] = $servicos;
        return $this->render('ImaTelecomBundle:Administrativo:servicos-prestados.html.twig', $retornoView);
    }

    public function servicosPrestadosServicoPrestadoAction(Request $request, $servicoPrestadoId) {
        $servico = ServicoPrestadoQuery::create()->findPk($servicoPrestadoId);
        if ($servico == null) {
            $servico = new ServicoPrestado();
        }

        $planos = PlanosQuery::create()->orderByNome(Criteria::ASC)->find();
        $tipoServicosPrestados = TipoServicoPrestadoQuery::create()->orderByTipo(Criteria::ASC)->find();

        $retornoView = array();
        $retornoView['servico'] = $servico;
        $retornoView['planos'] = $planos;
        $retornoView['tipoServicosPrestados'] = $tipoServicosPrestados;

        return $this->render('ImaTelecomBundle:Administrativo:servico-prestado.html.twig', $retornoView);
    }

    public function servicosPrestadosServicoPrestadoSalvarAction(Request $request, $servicoPrestadoId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $nome = trim(trim($request->get('nome', '')));
        $tipo = trim($request->get('tipo', 'internet'));
        $codigo = trim($request->get('codigo', ''));
        $download = trim($request->get('download', '0'));
        $upload = trim($request->get('upload', '0'));
        $franquia = trim($request->get('franquia', '0'));
        $garantiaBanda = trim($request->get('garantiaBanda', '0'));
        $tecnologia = trim($request->get('tecnologia', 'fibra'));
        $planoId = trim($request->get('planoId', '0'));
        $ativo = trim($request->get('ativo', '0'));
        $prePago = trim($request->get('prePago', '0'));
        $tipoServicoPrestadoId = trim($request->get('tipoServicoPrestadoId', '0'));

        if (empty($nome)) {
            $retornoView['erros'][] = "Nome: Campo não pode ser vazio.";
        }

        if (empty($planoId)) {
            $retornoView['erros'][] = "Plano: Campo não pode ser vazio.";
        }

        if (empty($tipoServicoPrestadoId)) {
            $retornoView['erros'][] = "Tipo Serviço Prestado: Campo não pode ser vazio.";
        }

        $servico = ServicoPrestadoQuery::create()->findPk($servicoPrestadoId);
        if ($servico == null) {
            $servico = new ServicoPrestado();
            $servico->setDataCadastro(new DateTime('now'));
        }

        $servico->setNome($nome);
        $servico->setTecnologia($tecnologia);
        $servico->setCodigo($codigo);
        $servico->setDownload($download);
        $servico->setUpload($upload);
        $servico->setFranquia($franquia);
        $servico->setGarantiaBanda($garantiaBanda);
        $servico->setAtivo($ativo);
        $servico->setPrePago($prePago);
        $servico->setPlanoId($planoId);
        $servico->setTipo($tipo);
        $servico->setTipoServicoPrestadoId($tipoServicoPrestadoId);
        $servico->setDataAlterado(new DateTime('now'));
        $servico->setUsuarioAlterado($user->getIdusuario());

        if (sizeof($retornoView['erros']) == 0) {
            try {
                $servico->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Serviço salvo com sucesso.';
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_administrativo_servicos_prestados');
            } catch (Exception $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function servicosPrestadosServicoPrestadoExcluirAction(Request $request, $servicoPrestadoId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $servico = ServicoPrestadoQuery::create()->findPk($servicoPrestadoId);
        if ($servico != null) {
            $nome = $servico->getNome();
            $temCliente = ServicoClienteQuery::create()->filterByServicoPrestado($servico)->count();
            if ($temCliente == 0) {
                try {
                    $servico->delete();

                    $retornoView['status'] = true;
                    $retornoView['mensagem'] = "$nome foi excluído com sucesso.";
                    $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_administrativo_servicos_prestados');
                } catch (Exception $ex) {
                    $retornoView['erros'][] = $ex->getMessage();
                }
            } else {
                $retornoView['erros'][] = "O serviço $nome, está vinculado a alguns clientes, mude os serviços ou desative o mesmo caso esteja ativado.";
            }
        } else {
            $retornoView['erros'][] = 'Não foi encontrado nenhum serviço com este código ' . $servicoPrestadoId;
        }

        return new JsonResponse($retornoView);
    }

    public function clienteNotasFiscaisVisualizarNotaSvaAction(Request $request, $notaFiscalId) {
        $nota = DadosFiscalQuery::create()->filterBySituacao('aprovada')->filterByTipoNota('sva')->findPk($notaFiscalId);

        if ($nota == null) {
            $request->getSession()->set('ErrorPageMessage', 'Não foi encontrada à Nota Fiscal de SVA.');

            return $this->redirectToRoute('ima_telecom_admin_error_page_404');
        }

        $retorno = $this->visualizarNotaSva($nota);

        return new JsonResponse($retorno);
    }

}
