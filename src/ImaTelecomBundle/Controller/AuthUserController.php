<?php

namespace ImaTelecomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Security;


class AuthUserController extends Controller {

    public function loginAction(Request $request) {
        /*
          $loadFile = "https://www.google.com/recaptcha/api/image?c=03ANcjospBrZGRUrJx4G7iHyv6OowSgBojInPuDoFxTs_gr5-VXGmQr0EWGCCp0U70oa3iby_HGoIXEHJ6RPXRVpHtmVo8gXjjMhMBROVFNAgHpVOZcu_HOVMsPLwC0IrrKoyRM5oC7KoSX7rH2IZ5rlj9c59b5IiebEUFBbeRPuLBLnkzX5Q9-FG177gc91j52RU-JQG1-V6xFi2EUST5Vke0xIs2eiuy1g&th=,X65Xro-wTUlTZ4JNZZe2Gd176ARa7pHwAAAAKqAAAAGlawOYsGiUHR_sjMEvYXa-nB0rdB15qIq1yr_x2hlRNBt0g9Ynv5SdRPSIc1dW-OWXfiobyfnjEpl2h00ncH0qxxYb0jenYZGNig8fjfc2_W8XZzoH4sr1IWREL2XuG2x7Qk1zycUtULo4j_9OPL7va6XczLCuisZTUTFnV0ssXqGgZmTcueALV8HleuItT9TRTfueO5MPhfdjp3nf1t8ivwOY1xlmsVbjC_pFC5MKfh4rlzrfWUvp6aWD9RlR9BtBLr5o8EjN9h2BI7Aw_obsVix0_EPdJzREPDsvJrdKIdb75PwsYGddKx2mOID-3qHCi1SWQBk6X7iG88M9Axxhfp9VWOdY1FZF6sIcDnysTYq2pjThp4oiq5eQCj9JSgE5HD1j0yV91xsFztZmrKflN6m8NKELQ-ZzM8fT6H-TJsSV2fpDFSMCzfykbzZC25E1yd6zLvU0HhkT0UKam-dL4aK-XlplZCKSFGmpEb042cDnPzR-rIBkAgK5Q01qhD-D_eGloda52bGImiQHA9C8MVApWP-A2H8xff1HRSBYXMVKz_NBlNkhVsIml0MiRlXmA48MQALszAyPtDszeiyO1nLb8uK-jJe_atB4H3CX0ExwJq9NvZ1kqTt5dgroTuo-twQMaWuYdbXhRSmoQ0zR_ELHnOe5Vl3y0-G4CpZGGo1_0b_NYyS_XXb_X3e_8EkYQO7Al5JoFv4ZKY-mQJ-GGyVPr0Y8IqSKp__Vkuzo8gD0AXV8qUSANOckuYa_QYrWdU-L6rGO8cpIsI7M1ofyW_VjUICwk61lnH7Qm2uOy6XJ1vFoJpU2E7wFyYxbdOujAcW8OzXruY-4OTrVAWRSEVtcXy062zJyh-FWdK3JJrIeNqGxBTKrpiuPw3WcTAN0QGqTSIB5vsgpXKK945tx8XELthVcTLjeundsR3MN8i1ufX1deR1bFSi31n5TzSLsdPMErQK4-6IgIwRfHrGo-XlOKrR0_lrtTs_QbOwBJaK0_g4-NlkePDxKuAiy7ukNJRnYe7SjfiBhfuvYwrVt35Icp32AgYVl8GjSfZYpk7wgEbl_LRlRsCXAmneQlU5UZojRQzpZ9O2DZB7WwwU1bz5cWkCcrm26PHsFZi33BFqg4if24ONXoaSzdwOskvMVqNWHTVA9vrcqxmsAEXF6yAlRElTXQCYLLpEEbhs41ijAP0X6S5DQ28lM3iTJ_ucyVO19T6x76-E8GMM";
          $im = file_get_contents($loadFile);
          $output = 'tmp/teste_cat.jpg';
          file_put_contents($output, $im); */
/*
        $str = utf8_decode('09324949000111ISENTO        AUTOPISTA FLUMINENSE S/A           RUA XV DE NOVEMBRO                           00004SALA 901       26079800TORRE SUL CENTRNiterói                       RJ22136349401 43221       22136349401 RJ2018030221U  0000086583303302     ');
        print md5($str, false);
        if (hash('md5', $str) === '2745e020283be2f7dd63c27a8068cf69') {
            echo "FOI";
            exit;
        } else {
            
        }*/

        $session = $request->getSession();
        $logado = $session->get('ck_authorized_site_imatelecom');

        if ($logado) {
            return $this->redirect($this->generateUrl('ima_telecom_admin'));
        } else {
            // get the login error if there is one            
            if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
                $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
            } elseif (null !== $session && $session->has(Security::AUTHENTICATION_ERROR)) {
                $error = $session->get(Security::AUTHENTICATION_ERROR);
                $session->remove(Security::AUTHENTICATION_ERROR);
            } else {
                $error = '';
            }

            // last username entered by the user
            $lastUsername = (null === $session) ? '' : $session->get(Security::LAST_USERNAME);

            /*
              $fs = new Filesystem();
              try {
              //$fs->remove($this->getParameter('kernel_cache_dir'));
              $fs->remove($this->container->getParameter('kernel.cache_dir'));
              } catch (Symfony\Component\Security\Acl\Exception\Exception $e) {
              } */
            
            $retornoView = array();
            $retornoView['last_username'] = $lastUsername;
            $retornoView['error'] = $error;
            return $this->render('ImaTelecomBundle:AuthUser:login.html.twig', $retornoView);
        }
    }

}
