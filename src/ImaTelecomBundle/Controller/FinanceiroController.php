<?php

namespace ImaTelecomBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Propel\Runtime\Propel;
use Propel\Runtime\Exception\PropelException;
use ImaTelecomBundle\Model\Map\CompetenciaTableMap;
use ImaTelecomBundle\Model\Competencia;
use ImaTelecomBundle\Model\CompetenciaQuery;
use ImaTelecomBundle\Model\LancamentosQuery;
use ImaTelecomBundle\Model\Lancamentos;
use ImaTelecomBundle\Model\BoletoQuery;
use ImaTelecomBundle\Lib\BoletoSantanderCNAB240;
use ImaTelecomBundle\Model\BoletoBaixaHistorico;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery;
use ImaTelecomBundle\Model\ConvenioQuery;
use ImaTelecomBundle\Model\BancoAgenciaContaQuery;
use \DateTime;
use Propel\Runtime\ActiveQuery\Criteria;
use ImaTelecomBundle\Model\ServicoClienteQuery;
use ImaTelecomBundle\Model\ClienteQuery;
use ImaTelecomBundle\Model\Map\LancamentosTableMap;
use ImaTelecomBundle\Model\LancamentosBoletos;
use ImaTelecomBundle\Model\LancamentosBoletosQuery;
use ImaTelecomBundle\Model\FormaPagamentoQuery;
use ImaTelecomBundle\Model\FormaPagamento;
use ImaTelecomBundle\Model\Baixa;
use ImaTelecomBundle\Model\BaixaMovimento;
use ImaTelecomBundle\Model\Map\BaixaTableMap;
use ImaTelecomBundle\Model\ContaCaixaQuery;
use ImaTelecomBundle\Model\BaixaEstorno;
use ImaTelecomBundle\Model\BancoAgenciaConta;
use ImaTelecomBundle\Model\BancoAgenciaQuery;
use ImaTelecomBundle\Model\BancoQuery;
use ImaTelecomBundle\Model\BancoAgencia;
use ImaTelecomBundle\Model\EnderecoAgencia;
use ImaTelecomBundle\Model\CidadeQuery;
use ImaTelecomBundle\Model\Map\BancoAgenciaTableMap;
use ImaTelecomBundle\Model\Convenio;
use ImaTelecomBundle\Model\Banco;
use ImaTelecomBundle\Model\Empresa;
use ImaTelecomBundle\Model\EmpresaQuery;
use ImaTelecomBundle\Model\Pessoa;
use ImaTelecomBundle\Model\ContaCaixa;
use ImaTelecomBundle\Model\BaixaQuery;
use ImaTelecomBundle\Model\FornecedorQuery;
use ImaTelecomBundle\Model\BaixaMovimentoQuery;
use ImaTelecomBundle\Model\BaixaEstornoQuery;

class FinanceiroController extends AdminController {

    public function indexAction(Request $request) {
        return $this->render('ImaTelecomBundle:Admin:index.html.twig');
    }

    public function competenciasAction(Request $request) {
        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->find();

        $retornoView = array();
        $retornoView['competencias'] = $competencias;
        return $this->render('ImaTelecomBundle:Financeiro:competencias.html.twig', $retornoView);
    }

    public function competenciaAction(Request $request, $competenciaId) {
        $competencia = CompetenciaQuery::create()->findPk($competenciaId);

        if ($competencia == null) {
            $competencia = new Competencia();
        }

        $retornoView = array();
        $retornoView['competencia'] = $competencia;
        return $this->render('ImaTelecomBundle:Financeiro:competencia.html.twig', $retornoView);
    }

    public function competenciaAtivaAction(Request $request) {
        //$competencia = $this->getCompetenciaAtual();
        $competencia = CompetenciaQuery::create()->findOneByAtivo(1);

        $retornoView = array();
        $retornoView['competencia'] = $competencia;
        return $this->render('ImaTelecomBundle:Financeiro:competencia.html.twig', $retornoView);
    }

    public function liberarCompetenciaAtualAction(Request $request) {
        $erros = array();
        $status = true;
        $retornoView = array();
        $con = Propel::getWriteConnection(CompetenciaTableMap::DATABASE_NAME);
        $con->beginTransaction();

        $competenciaAtual = CompetenciaQuery::create()->findOneByAtivo(1);
        if (!$competenciaAtual->hasProximaCompetencia()) {
            $novaCompetencia = $this->criarNovaCompetencia($competenciaAtual->getAno(), $competenciaAtual->getMes());

            try {
                if ($novaCompetencia->save()) {
                    $competenciaAtual->setProximaCompetencia($novaCompetencia->getId());
                    $competenciaAtual->save();
                }
                $con->commit();
            } catch (PropelException $ex) {
                $con->rollback();
                $erros[] = $ex->getMessage();
            }
        }

        if ($competenciaAtual->hasProximaCompetencia()) {
            $competenciaFutura = $competenciaAtual->getCompetenciaFutura();
            if ($competenciaAtual->getAtivo() == 1 && $competenciaAtual->getEncerramento() == 0 && $competenciaAtual->getPeriodoFuturo() == 0) {
                $competenciaAtual->setEncerramento(1);
                $competenciaAtual->setAtivo(0);
                $competenciaAtual->setPeriodoFuturo(0);
                $competenciaAtual->setTrabalharPeriodoFuturo(0);

                $competenciaFutura->setAtivo(1);
                $competenciaFutura->setPeriodoFuturo(0);

                $numeroDaUltimaNotaEmitida = 0;
                $anoCompetenciaAtual = $competenciaAtual->getAno();
                $anoCompetenciaFutura = $competenciaFutura->getAno();
                if ($anoCompetenciaAtual == $anoCompetenciaFutura) {
                    $numeroDaUltimaNotaEmitida = $competenciaAtual->getNumeroNf();
                }
                /*
                  $numeroDaUltimaNotaEmitida = CompetenciaQuery::create()->withColumn('SUM(numero_nf)', 'total')
                  ->filterByAno($ano)
                  ->findOne()
                  ->getTotal(); */
                $competenciaFutura->setNumeroNf($numeroDaUltimaNotaEmitida);

                try {
                    $competenciaAtual->save();
                    $competenciaFutura->save();

                    $con->commit();
                } catch (PropelException $ex) {
                    $con->rollback();
                    $erros[] = $ex->getMessage();
                }
            } else {
                if ($competencia->getAtivo() == 0)
                    $erros[] = "A Competência precisa estar ativa.";

                if ($competencia->getEncerramento() == 1)
                    $erros[] = "A Competência já se encontra encerrada.";

                if ($competencia->getPeriodoFuturo() == 1)
                    $erros[] = "A Competência precisa estar no período atual.";
            }


            if (!$competenciaFutura->hasProximaCompetencia()) {
                $novaCompetencia = $this->criarNovaCompetencia($competenciaFutura->getAno(), $competenciaFutura->getMes());
                try {
                    if ($novaCompetencia->save()) {
                        $competenciaFutura->setProximaCompetencia($novaCompetencia->getId());
                        $competenciaFutura->save();
                    }
                    $con->commit();
                } catch (PropelException $ex) {
                    $con->rollback();
                    $erros[] = $ex->getMessage();
                }
            } else {
                $erros[] = 'A Competência Futura não possui a Próxima Competência. Por favor, tente novamente. Caso o erro permaneça, contacte o Suporte.';
            }
        } else {
            $erros[] = 'Não existe a Próxima Competência. Por favor, tente novamente. Caso o erro permaneça, contacte o Suporte.';
        }

        if (sizeof($erros)) {
            $status = false;
        } else {
            $retornoView['mensagem'] = 'Competência Liberada com sucesso!';
            $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_competencias');
        }

        $retornoView['status'] = $status;
        $retornoView['erros'] = $erros;

        return new JsonResponse($retornoView);
    }

    public function competenciaTrabalharPeriodoFuturoAction(Request $request) {
        $retornoView = array();

        $con = Propel::getWriteConnection(CompetenciaTableMap::DATABASE_NAME);
        $con->beginTransaction();

        $competencia = CompetenciaQuery::create()->findOneByAtivo(1);
        if ($competencia->getTrabalharPeriodoFuturo() == 0) {
            $competencia->setTrabalharPeriodoFuturo(1);

            try {
                if (!$competencia->hasProximaCompetencia()) {
                    $novaCompetencia = $this->criarNovaCompetencia($competencia->getAno(), $competencia->getMes());

                    try {
                        $novaCompetencia->save();
                        $competencia->setProximaCompetencia($novaCompetencia->getId());
                        $competencia->save();
                        $con->commit();

                        $retornoView['mensagem'] = 'Você agora se encontra trabalhando na competência futura!';
                    } catch (PropelException $ex) {
                        $con->rollback();
                        $erros[] = $ex->getMessage();
                    }
                }
            } catch (PropelException $ex) {
                $status = false;
                $erros[] = $ex->getMessage();
            }
        } else {
            $competencia->setTrabalharPeriodoFuturo(0);
            $retornoView['mensagem'] = 'Você agora não se encontra trabalhando na competência futura mais!';
        }

        $status = true;
        $erros = array();

        try {
            $competencia->save();
            $con->commit();
        } catch (PropelException $ex) {
            $con->rollback();
            $status = false;
            $erros[] = $ex->getMessage();
        }

        $retornoView['status'] = true;
        $retornoView['erros'] = $erros;
        return new JsonResponse($retornoView);
    }

    public function faturamentosAction(Request $request) {
        $clientes = ClienteQuery::create()->withColumn('Pessoa.nome', 'Nome')->join('Pessoa')->find();

        $retornoView = array();
        $retornoView['clientes'] = $clientes->toArray();
        return $this->render('ImaTelecomBundle:Administrativo:clientes.html.twig', $retornoView);
    }

    public function lancamentosBancariosAction(Request $request) {
        $lancamentos = LancamentosQuery::create()->orderByCompetenciaId(Criteria::DESC)->orderByIdlancamento(Criteria::DESC)->find();

        $retornoView = array();
        $retornoView['lancamentos'] = $lancamentos;
        return $this->render('ImaTelecomBundle:Financeiro:lancamentos-bancarios.html.twig', $retornoView);
    }

    public function lancamentosBancariosDetalhesLancamentoAction(Request $request, $lancamentoId) {
        $lancamento = LancamentosQuery::create()->findPk($lancamentoId);
        $itensLancamento = LancamentosBoletosQuery::create()
                ->select(array('Idboleto', 'Idcliente', 'Registrado', 'EstaPago', 'Vencimento', 'NomePessoa', 'Valor', 'ImportId'))
                ->useBoletoQuery()
                ->withColumn('boleto.Idboleto', 'Idboleto')
                ->withColumn('boleto.Registrado', 'Registrado')
                ->withColumn('boleto.EstaPago', 'EstaPago')
                ->withColumn('boleto.Vencimento', 'Vencimento')
                ->withColumn('boleto.Valor', 'Valor')
                ->withColumn('boleto.ImportId', 'ImportId')
                ->useClienteQuery()
                ->withColumn('cliente.Idcliente', 'Idcliente')
                ->usePessoaQuery()
                ->withColumn('pessoa.Nome', 'NomePessoa')
                ->endUse()
                ->endUse()
                ->endUse()
                ->filterByLancamentos($lancamento)
                ->find();

        if ($lancamento == null) {
            $lancamento = new Lancamentos();
        }

        $retornoView = array();
        $retornoView['lancamento'] = $lancamento;
        $retornoView['itensLancamento'] = $itensLancamento;
        return $this->render('ImaTelecomBundle:Financeiro:lancamentos-bancario.html.twig', $retornoView);
    }

    public function lancamentosBancariosBaixarLancamentoAction(Request $request, $lancamentoId) {
        $retornoView = array();

        if ($request->isMethod('GET')) {
            $lancamento = LancamentosQuery::create()->findPk($lancamentoId);
            $retornoView['lancamento'] = $lancamento;
            $retornoView['isTab'] = false;
            return $this->render('ImaTelecomBundle:Financeiro:lancamentos-bancario-baixa-arquivo.html.twig', $retornoView);
        } elseif ($request->isMethod('POST')) {
            $retornoView = $this->lancamentosBancarioBaixarLancamento($lancamentoId);
            return new JsonResponse($retornoView);
        }
    }

    public function lancamentosBancarioBaixarLancamento($lancamentoId) {
        $retornoView = array();
        $retornoView['status'] = true;
        $retornoView['mensagem'] = "Lançamento verificado com sucesso!";
        
        $user = $this->getUser();
        $lancamento = LancamentosQuery::create()->findPk($lancamentoId);
        $banco = $lancamento->getConvenio()->BancoAgenciaConta()->getBanco();
        $dadosCobranca = $this->getDadosBancarioSantander($banco->getIdbanco());
        $arquivoRetorno = $_FILES['arquivoRetorno'];
        $tipoArquivo = utf8_encode($_FILES['arquivoRetorno']['type']);

        $boletoCNAB240 = new BoletoSantanderCNAB240($dadosCobranca);
        $retornoCobrancas = $boletoCNAB240->obterArquivoRetorno($arquivoRetorno);

        if ($tipoArquivo == "text/plain") {
            if (sizeof($retornoCobrancas) == 0) {
                $retornoView['status'] = false;
                $retornoView['mensagem'] = "Arquivo Incompatível.";
            }

            foreach ($retornoCobrancas as $boleto => $dadosBoleto) {
                $boletoLancamento = BoletoQuery::create()->findOneByNossoNumero((int) $boleto);
                //$boletoLancamento = BoletoQuery::create()->findOneByNossoNumero(trim($boleto));
                if ($boletoLancamento == null) {
                    continue;
                }

                if ($boletoLancamento->getEstaPago() == 0) {
                    //$idLancamento = trim($dadosBoleto['T']['numeroDocumentoCobranca']);                    
                    //$idLancamento = trim($dadosBoleto['T']['numeroDocumentoCobranca']);
                    //$numeroDocumentoCobranca = trim($dadosBoleto['T']['numeroDocumentoCobranca']);
                    $lancamento = LancamentosQuery::create()->findPk($lancamentoId);
                    if ($lancamento != null) {
                        //$idBoleto = substr_replace($boleto, "", -1);
                        // ------------------------ BOLETO BAIXA HISTORICO ------------------------ \\
                        $boletoBaixaHistorico = new BoletoBaixaHistorico();
                        $boletoBaixaHistorico->setLancamentoId($lancamentoId);
                        //$boletoBaixaHistorico->setBoletoId($dadosBoleto['T']['numeroDocumentoCobranca']);
                        $boletoBaixaHistorico->setBoletoId($boletoLancamento->getIdboleto());

                        $mensagens = implode("\n\r", $dadosBoleto['mensagens']);
                        $boletoBaixaHistorico->setStatus($dadosBoleto['status']);

                        $valorBoleto = $boletoCNAB240->getRoundValor($dadosBoleto['T']['valorNominalTitulo'], 2);
                        $boletoBaixaHistorico->setValorOriginal($valorBoleto);

                        $valorTarifaCusto = $boletoCNAB240->getRoundValor($dadosBoleto['T']['valorTarifaCustos'], 2);
                        $boletoBaixaHistorico->setValorTarifaCusto($valorTarifaCusto);

                        $dataVencimento = $boletoCNAB240->getFormatarData("Y-mm-dd", $dadosBoleto['T']['dataVencimentoTitulo']);
                        $boletoBaixaHistorico->setDataVencimento($dataVencimento);
                        $boletoBaixaHistorico->setNumeroDocumento(trim($dadosBoleto['T']['numeroDocumentoCobranca']));
                        $boletoBaixaHistorico->setCodigoMovimento($dadosBoleto['T']['codigoMovimento']);
                        $boletoBaixaHistorico->setDescricaoMovimento($dadosBoleto['descricaoMovimento']);

                        if ($dadosBoleto['status'] == 'baixado') {
                            //dataOcorrencia  data de Pagamento
                            $dataPagamento = $boletoCNAB240->getFormatarData("dd/mm/aaaa", $dadosBoleto['U']['dataOcorrencia']);
                            $boletoBaixaHistorico->setDataPagamento($dataPagamento);
                            //dataEfetivacaoCredito data de baixa no banco
                            //$dataBaixa = $boletoCNAB240->getFormatarData("dd/mm/aaaa", $dadosBoleto['U']['dataEfetivacaoCredito']);
                            //$boletoBaixaHistorico->setDataBaixa($dataBaixa);

                            // DADOS DO BOLETO PARA ATUALIZAÇÃO
                            $dateTimePagamento = \DateTime::createFromFormat("d/m/Y", $dataPagamento);
                            $boletoLancamento->setRegistrado(1);
                            $boletoLancamento->setEstaPago(1);
                            $boletoLancamento->setDataPagamento($dataPagamento);

                            $jurosMultaEncargos = $boletoCNAB240->getRoundValor($dadosBoleto['U']['jurosMultaEncargos'], 2);
                            $boletoLancamento->setValorJuros($jurosMultaEncargos);

                            $valorDescontoConcedido = $boletoCNAB240->getRoundValor($dadosBoleto['U']['valorDescontoConcedido'], 2);
                            $boletoLancamento->setValorDesconto($valorDescontoConcedido);

                            $valorAbatimentoConcedido = $boletoCNAB240->getRoundValor($dadosBoleto['U']['valorAbatimentoConcedido'], 2);
                            $boletoLancamento->setValorAbatimento($valorAbatimentoConcedido);

                            $valorIORecolhido = $boletoCNAB240->getRoundValor($dadosBoleto['U']['valorIORecolhido'], 2);
                            $boletoLancamento->setValorIof($valorIORecolhido);

                            $valorPagoPagador = $boletoCNAB240->getRoundValor($dadosBoleto['U']['valorPagoPagador'], 2);
                            $boletoLancamento->setValorPago($valorPagoPagador);

                            $valorLiquidoCreditado = $boletoCNAB240->getRoundValor($dadosBoleto['U']['valorLiquidoCreditado'], 2);
                            $boletoLancamento->setValorLiquido($valorLiquidoCreditado);

                            $valorOutrasDespesas = $boletoCNAB240->getRoundValor($dadosBoleto['U']['valorOutrasDespesas'], 2);
                            $boletoLancamento->setValorOutrasDespesas($valorOutrasDespesas);

                            $valorOutrosCreditos = $boletoCNAB240->getRoundValor($dadosBoleto['U']['valorOutrosCreditos'], 2);
                            $boletoLancamento->setValorOutrosCreditos($valorOutrosCreditos);
                        } else {
                            if ($dadosBoleto['status'] == 'registrado') {
                                $boletoLancamento->setRegistrado(1);
                            } else {
                                $boletoLancamento->setRegistrado(0);
                            }

                            $boletoLancamento->setEstaPago(0);
                            $boletoLancamento->setDataPagamento(null);
                            $boletoLancamento->setDataPagamento(null);
                            $boletoLancamento->setValorJuros(null);
                            $boletoLancamento->setValorDesconto(null);
                            $boletoLancamento->setValorPago(null);
                            $boletoLancamento->setValorIof(null);
                            $boletoLancamento->setValorAbatimento(null);
                            $boletoLancamento->setValorLiquido(null);
                            $boletoLancamento->setValorOutrasDespesas(null);
                            $boletoLancamento->setValorOutrosCreditos(null);
                        }

                        $boletoBaixaHistorico->setMensagemRetorno($mensagens);
                        $boletoBaixaHistorico->setDataCadastro(new DateTime('now'));
                        $boletoBaixaHistorico->setDataAlterado(new DateTime('now'));
                        $boletoBaixaHistorico->setUsuario($user);
                        $boletoBaixaHistorico->setCompetenciaId($lancamento->getCompetenciaId());
                        // ------------------------ BOLETO BAIXA HISTORICO ------------------------ \\

                        try {
                            $boletoLancamento->save();
                            $boletoBaixaHistorico->save();
                        } catch (PropelException $propelException) {
                            $retornoView['status'] = false;
                            $retornoView['mensagem'] = "Não foi possível salvar o Histórico de Baixa/Boleto, Nosso Número: $boleto. Descrição do erro gerado: " . $propelException->getMessage();
                        }
                    } else {
                        $retornoView['status'] = false;
                        $retornoView['mensagem'] = "Não foi possível localizar o lançamento: $idLancamento, para o Nosso Número: $boleto";
                    }
                } else {
                    // Boleto já foi pago
                    $retornoView['status'] = true;
                    $retornoView['mensagem'] = "Lançamento verificado com sucesso!";
                }
            }
        } else {
            $retornoView['status'] = false;
            $retornoView['mensagem'] = "Arquivo Incompatível.";
        }

        return $retornoView;
    }

    public function lancamentosBancariosDownloadLancamentoAction(Request $request, $lancamentoId) {
        $lancamento = LancamentosQuery::create()->findPk($lancamentoId);
        $banco = $lancamento->getConvenio()->BancoAgenciaConta()->getBanco();

        $conteudoArquivoSaida = $lancamento->getConteudoArquivoSaida();
        $bancoNome = str_replace(' ', '_', $banco->getNome());
        $anoMesLancamento = $lancamento->getCompetencia()->getAno() . '_' . $lancamento->getCompetencia()->getMes();
        $filename = 'tmp/arquivo_registro_' . $bancoNome . '_' . $anoMesLancamento . '_' . time() . '.txt';

        $arquivoBancario = fopen($filename, "w");
        fwrite($arquivoBancario, $conteudoArquivoSaida);
        fclose($arquivoBancario);

        // Generate response
        $response = new Response();

        // Set headers
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($filename));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
        $response->headers->set('Content-length', filesize($filename));

        // Send headers before outputting anything
        $response->sendHeaders();
        $response->setContent(file_get_contents($filename));

        return $response;
    }

    public function lancamentosBancariosGeracaoLancamentoAction(Request $request) {
        $competenciaAtual = $this->getCompetenciaAtual();
        $cobrancas = BoletoQuery::create()->withColumn('pessoa.nome', 'ClienteNome')
                        ->useClienteQuery()->joinPessoa()->endUse()
                        //->where("tipo_pessoa = 'juridica'")
                        ->filterByRegistrado(false)->findByCompetenciaId($competenciaAtual->getId()); //$this->getCobrancasGeracaoLancamento($competenciaAtual->getId());
        //$convenios = ConvenioQuery::create()->findByAtivo(true);
        $dadosBancarios = BancoAgenciaContaQuery::create()
                ->useConvenioQuery()->filterByAtivo(true)->endUse()
                ->find();

        $retornoView = array();
        $retornoView['cobrancas'] = $cobrancas;
        $retornoView['dadosBancarios'] = $dadosBancarios;
        return $this->render('ImaTelecomBundle:Financeiro:lancamentos-bancario-geracao.html.twig', $retornoView);
    }

    public function lancamentosBancariosGeracaoLancamentoDadosBancarioAction(Request $request, $convenioId) {
        $dadosBancario = BancoAgenciaContaQuery::create()->findByConvenioId($convenioId)->getLast();
        $retornoView = array();
        $retornoView['dadosBancario'] = $dadosBancario;
        return $this->render('ImaTelecomBundle:Financeiro:lancamentos-bancario-geracao-dados-bancario.html.twig', $retornoView);
    }

    public function lancamentosBancariosGeracaoLancamentoGerarAction(Request $request) {
        // selecionar o banco e  depois o convenio
        $tipo = $request->get('tipo', '');
        $convenioId = $request->get('convenio', '');
        $cobrancaIds = $request->get('cobrancas', array());

        $retornoView = array();
        $retornoView['status'] = true;
        $retornoView['erros'] = array();

        $where = '';
        $ids = implode(',', $cobrancaIds);
        if ($tipo == 'todos' && !empty($ids)) {
            $where = "and idboleto not in ($ids)";
        }

        if ($tipo == 'alguns' && !empty($ids)) {
            $where = "and idboleto in ($ids)";
        } elseif ($tipo == 'alguns' && empty($ids)) {
            $retornoView['status'] = false;
            $retornoView['erros'][] = 'Por favor, selecione pelo menos uma cobrança para a geração.';
        }

        if ($retornoView['status']) {
            $competencia = $this->getCompetenciaAtual();
            $competenciaId = $competencia->getId();

            $sql = "SELECT b.*, p.*, sp.nome as nome_servico_prestado, tipo_pessoa,
ec.rua as endereco_cliente, ec.num as numero_cliente, ec.bairro as bairro_cliente,
ec.cep as cep_cliente, ci.municipio as municipio_cliente, ci.uf as uf_cliente

    FROM boleto b, servico_cliente cl, servico_prestado sp,
cliente c left join endereco_cliente ec
          left join cidade ci on ci.idcidade = ec.cidade_id
on ec.cliente_id = c.idcliente,
pessoa p, competencia cp, boleto_item bi

    where c.idcliente = b.cliente_id and p.id = c.pessoa_id
    and cp.id = b.competencia_id and cl.cliente_id = c.idcliente 
    and sp.idservico_prestado = cl.servico_prestado_id and bi.boleto_id = b.idboleto and b.valor > 0
    and cl.idservico_cliente = bi.servico_cliente_id $where and b.esta_pago = 0 and b.competencia_id = $competenciaId
    group by b.idboleto order by ec.cliente_id desc";

            $con = Propel::getConnection();
            $stmt = $con->prepare($sql);
            $stmt->execute();
            $cobrancas = $stmt->fetchall();

            $retornoView = $this->gerarArquivoSaidaCobrancaRegistrada240Santander($competencia, $convenioId, $cobrancas);
            if ($retornoView['status']) {
                $filename = $retornoView['filename'];

                $response = new Response();

                // Set headers
                $response->headers->set('Cache-Control', 'private');
                $response->headers->set('Content-type', mime_content_type($filename));
                $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
                $response->headers->set('Content-length', filesize($filename));

                // Send headers before outputting anything
                $response->sendHeaders();
                $response->setContent(file_get_contents($filename));

                return $response;
            }
        }

        return new JsonResponse($retornoView);
    }

    private function gerarArquivoSaidaCobrancaRegistrada240Santander($competencia, $convenioId, $cobrancas) {
        $user = $this->getUser();

        $retorno = array();
        $retorno['status'] = true;
        $retorno['erros'] = array();

        $bancoAgenciaConta = BancoAgenciaContaQuery::create()->findByConvenioId($convenioId)->getLast();
        if ($bancoAgenciaConta == null) {
            $retorno['status'] = false;
            $retorno['erros'][] = 'O Convênio não foi informado na parametrização da Agência Bancária, verifique as configurações do Convênio antes de prosseguir com a geração.';
        } else {
            $convenio = $bancoAgenciaConta->getConvenio();
            $empresa = $convenio->getEmpresa();
            $pessoa = $empresa->getPessoa();
            $bancoAgencia = $bancoAgenciaConta->getBancoAgencia();
            $banco = $bancoAgenciaConta->getBanco();

            //$competencia = $this->getCompetenciaAtual(); //CompetenciaQuery::create()->findPk($competenciaId);

            $dadosCobranca = array();
            $dadosCobranca['nome_banco'] = $banco->getNome();
            $dadosCobranca['codigo_febraban'] = $banco->getCodigoFebraban();
            $dadosCobranca['tipo_pessoa'] = $pessoa->getTipoPessoa();
            $dadosCobranca['cpf_cnpj'] = $pessoa->getCpfCnpj();
            $dadosCobranca['numero_conta_corrente'] = $bancoAgenciaConta->getNumeroConta();
            $dadosCobranca['digito_verificador_conta_corrente'] = $bancoAgenciaConta->getDigitoVerificador();
            $dadosCobranca['nome_empresa'] = $pessoa->getRazaoSocial();
            $dadosCobranca['banco_agencia'] = $bancoAgencia->getAgencia();
            $dadosCobranca['banco_agencia_digito_verificador'] = $bancoAgencia->getDigitoVerificador();
            $dadosCobranca['versao_layout'] = '030';
            $dadosCobranca['ultimo_sequencial_arquivo_cobranca'] = $convenio->getUltimoSequencialArquivoCobranca();
            $dadosCobranca['codigo_convenio'] = $convenio->getCodigoConvenio();

            $cobrancasLancamento = array();
            $cobrancasLancamento['cobrancas'] = $cobrancas;

            $boletoCNAB240 = new BoletoSantanderCNAB240($dadosCobranca);

            $lancamento = new Lancamentos();
            $lancamento->setCompetencia($competencia);
            $lancamento->setConvenio($convenio);
            $lancamento->setDataCadastro(new DateTime('now'));
            $lancamento->setDataAlterado(new DateTime('now'));
            $lancamento->setUsuarioAlterado($user->getIdusuario());

            $con = Propel::getWriteConnection(LancamentosTableMap::DATABASE_NAME);
            $con->beginTransaction();
            try {
                $lancamento->save();

                $cobrancasLancamento['lancamento'] = $lancamento->getIdlancamento();

                $boletoCNAB240->setCobrancas($cobrancasLancamento);
                $arquivoRemessa = $boletoCNAB240->gerarArquivoRemessa();
                $dataGeracao = $boletoCNAB240->getFormatarData("dd/mm/aaaa", $arquivoRemessa['dataGeracao']);

                $lancamento->setConteudoArquivoSaida($arquivoRemessa['textoArquivo']);
                $lancamento->setDataGeracao($dataGeracao);
                $lancamento->setSequencialArquivoCobranca($arquivoRemessa['ultimoSequencialArquivoCobranca']);
                $lancamento->save();

                $convenio->setUltimoSequencialArquivoCobranca($arquivoRemessa['ultimoSequencialArquivoCobranca']);
                $convenio->save();

                foreach ($arquivoRemessa['dadosCobranca'] as $boletoId => $informacoes) {
                    //$dataGeracao = $boletoCNAB240->getFormatarData("dd/mm/aaaa", $retornoArquivoRemessa['dataGeracao']);
                    $lancamentosBoleto = new LancamentosBoletos();
                    $lancamentosBoleto->setLancamentoId($lancamento->getIdlancamento());
                    $lancamentosBoleto->setBoletoId($boletoId);
                    $lancamentosBoleto->setConteudoLinhasLancamentoBoleto(substr($informacoes['textoCobranca'], 2));
                    $lancamentosBoleto->setDataCadastrado(new DateTime('now'));
                    $lancamentosBoleto->setDataAlterado(new DateTime('now'));
                    $lancamentosBoleto->setDataGeracao($dataGeracao);
                    $lancamentosBoleto->setCompetencia($competencia);
                    $lancamentosBoleto->setConvenio($convenio);
                    $lancamentosBoleto->save();

                    // Preenchendo informações dos boletos
                    $boleto = BoletoQuery::create()->findPk($boletoId);
                    $boleto->setNossoNumero($informacoes['nossoNumero']);
                    $boleto->setNumeroDocumento($informacoes['numeroDocumento']);
                    $boleto->setLinhaDigitavel($informacoes['linhaDigitavel']);
                    $boleto->setRegistrado(0);
                    $boleto->setEstaPago(0);
                    $boleto->setDataPagamento(null);
                    $boleto->setValorJuros(null);
                    $boleto->setValorDesconto(null);
                    $boleto->setValorPago(null);
                    $boleto->setValorIof(null);
                    $boleto->setValorAbatimento(null);
                    $boleto->setValorLiquido(null);
                    $boleto->setValorOutrasDespesas(null);
                    $boleto->setValorOutrosCreditos(null);
                    $boleto->save();
                }

                $filename = 'tmp/arquivo_registro_santander_' . time() . '.txt';
                $arquivo = fopen($filename, "w");
                fwrite($arquivo, $arquivoRemessa['textoArquivo']);
                fclose($arquivo);

                $retorno['filename'] = $filename;

                $con->commit();
            } catch (PropelException $ex) {
                $con->rollBack();

                $retorno['status'] = false;
                $retorno['erros'][] = $ex->getMessage();
            }
        }

        return $retorno;
    }

    // Arquivo Remessa Santander
    private function geracaoAquivoSaidaSantanderCobrancaRegistrada240() {
        $params = array();
        // Santander
        $bancoId = 20; //$this->container->getParameter('banco_santander_id');
        $competenciaId = $request->request->get('competenciaId');

        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        $anoMes = $competencia->getAno() . $competencia->getMes();

        $dadosCobranca = $this->getDadosBancarioSantander($bancoId);

        $lancamento = new Lancamentos();
        $lancamento->setCompetenciaId($competenciaId);
        $lancamento->setDataCadastro(date_format(new \DateTime('now'), 'Y-m-d H:i:s'));
        $lancamento->setDataAlterado(date_format(new \DateTime('now'), 'Y-m-d H:i:s'));
        $lancamento->setUsuarioAlterado(1);
        $lancamento->setConvenioId($dadosCobranca['idconvenio']);
        $lancamento->save();

        //$cobrancas = $this->getCobrancasSantander(" and b.competencia_id = $competenciaId and idboleto = 135892");
        $cobrancas = $this->getCobrancasSantander(" and b.competencia_id = $competenciaId and idboleto = 135892");
        $cobrancasLancamento = array(
            'lancamento' => $lancamento->getIdlancamento(),
            'cobrancas' => $cobrancas);

        $boletoCNAB240 = new BoletoSantanderCNAB240($dadosCobranca);
        $boletoCNAB240->setCobrancas($cobrancasLancamento);
        $retornoArquivoRemessa = $boletoCNAB240->gerarArquivoRemessa();
        $dataGeracao = $boletoCNAB240->getFormatarData("dd/mm/aaaa", $retornoArquivoRemessa['dataGeracao']);

        //$lancamento = new Lancamentos();
        $lancamento->setConteudoArquivoSaida($retornoArquivoRemessa['textoArquivo']);
        //$lancamento->setCompetenciaId($competenciaId);
        $lancamento->setDataGeracao($dataGeracao);
        //$lancamento->setDataCadastro((new \DateTime())->format('Y-m-d H:i:s'));
        //$lancamento->setDataAlterado((new \DateTime())->format('Y-m-d H:i:s'));
        //$lancamento->setUsuarioAlterado(1);
        //$lancamento->setConvenioId($dadosCobranca['idconvenio']);
        $lancamento->setSequencialArquivoCobranca($retornoArquivoRemessa['ultimoSequencialArquivoCobranca']);
        $lancamento->save();

        $convenio = ConvenioQuery::create()->findPk($dadosCobranca['idconvenio']);
        $convenio->setUltimoSequencialArquivoCobranca($retornoArquivoRemessa['ultimoSequencialArquivoCobranca']);
        $convenio->save();

        if ($lancamento != null) {
            foreach ($retornoArquivoRemessa['dadosCobranca'] as $boletoId => $informacoes) {
                //$dataGeracao = $boletoCNAB240->getFormatarData("dd/mm/aaaa", $retornoArquivoRemessa['dataGeracao']);
                $lancamentosBoleto = new LancamentosBoletos();
                $lancamentosBoleto->setLancamentoId($lancamento->getIdlancamento());
                $lancamentosBoleto->setBoletoId($boletoId);
                $lancamentosBoleto->setConteudoLinhasLancamentoBoleto(substr($informacoes['textoCobranca'], 2));
                $lancamentosBoleto->setDataCadastrado(date_format(new \DateTime('now'), 'Y-m-d H:i:s'));
                $lancamentosBoleto->setDataAlterado(date_format(new \DateTime('now'), 'Y-m-d H:i:s'));
                $lancamentosBoleto->setDataGeracao($dataGeracao);
                $lancamentosBoleto->setCompetenciaId($competenciaId);
                $lancamentosBoleto->setConvenioId($dadosCobranca['idconvenio']);
                $lancamentosBoleto->save();

                // Preenchendo informações dos boletos
                $boleto = BoletoQuery::create()->findPk($boletoId);
                $boleto->setNossoNumero($informacoes['nossoNumero']);
                $boleto->setNumeroDocumento($informacoes['numeroDocumento']);
                $boleto->setLinhaDigitavel($informacoes['linhaDigitavel']);
                $boleto->setRegistrado(0);
                $boleto->setEstaPago(0);
                $boleto->setDataPagamento(null);
                $boleto->setValorJuros(null);
                $boleto->setValorDesconto(null);
                $boleto->setValorPago(null);
                $boleto->setValorIof(null);
                $boleto->setValorAbatimento(null);
                $boleto->setValorLiquido(null);
                $boleto->setValorOutrasDespesas(null);
                $boleto->setValorOutrosCreditos(null);

                $boleto->save();
            }
        }

        /*
          // Fim
          $caminhoOrigem = "tmp/";
          $nomeArquivo = "arquivo_saida_santander.txt";
          $pathFilename = $this->get('kernel')->getRootDir() . "/$caminhoOrigem/$nomeArquivo";
          $nomeArquivoZip = "arquivo_saida_$anoMes.zip";

          $arquivoSantander = fopen($caminhoOrigem . $nomeArquivo, "w");
          fwrite($arquivoSantander, $retornoArquivoRemessa['textoArquivo']);
          fclose($arquivoSantander);

          $this->criarArquivoZip($nomeArquivoZip, array($caminhoOrigem . $nomeArquivo));

          if (file_exists("tmp/$nomeArquivoZip")) {
          $params['status'] = true;
          $params['mensagem'] = "Arquivo gerado com sucesso!";
          $params['extra'] = "tmp/$nomeArquivoZip";
          } else {
          $params['status'] = false;
          $params['mensagem'] = "Não foi possível criar o arquivo de saída.";
          } */

        return new JsonResponse($params);
    }

    public function cobrancasAction(Request $request, $competenciaId = 0, $tipoLancamento = 'a_receber') {
        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        if ($competencia == null) {
            $competencia = $this->getCompetenciaAtual();
        }

        //$tipoLancamentoAtual = 'a_receber';
        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();
        $query = BoletoQuery::create()
                ->withColumn('pessoa.nome', 'nome')
                ->useClienteQuery()->joinPessoa()->endUse()
                ->filterByCompetencia($competencia);

        if ($tipoLancamento != 'todos') {
            $query->filterByTipoConta($tipoLancamento);
        }

        $cobrancas = $query->find();

        $tiposLancamento = array();
        $tiposLancamento[] = array('Tipo' => 'a_pagar', 'Nome' => 'A Pagar');
        $tiposLancamento[] = array('Tipo' => 'a_receber', 'Nome' => 'A Receber');
        $tiposLancamento[] = array('Tipo' => 'todos', 'Nome' => 'Todos');

        $retornoView = array();
        $retornoView['cobrancas'] = $cobrancas;
        $retornoView['competenciaAtual'] = $competencia;
        $retornoView['competencias'] = $competencias;
        $retornoView['tipoLancamentoAtual'] = $tipoLancamento;
        $retornoView['tiposLancamento'] = $tiposLancamento;

        return $this->render('ImaTelecomBundle:Financeiro:cobrancas.html.twig', $retornoView);
    }

    public function cobrancasGeracaoCobrancaAction(Request $request) {
        $competencia = $this->getCompetenciaAtual();
        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();
        $clientes = ClienteQuery::create()->usePessoaQuery()->withColumn('pessoa.nome', 'NomePessoa')->endUse()->find();

        $retornoView = array();
        $retornoView['clientes'] = $clientes;
        $retornoView['competenciaAtual'] = $competencia;
        $retornoView['competencias'] = $competencias;
        return $this->render('ImaTelecomBundle:Financeiro:cobrancas-geracao-cobranca.html.twig', $retornoView);
    }

    public function cobrancasGeracaoCobrancaPorCompetenciaAction(Request $request) {
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes

        $tipoCobrancaGeracao = $request->get('tipoCobrancaGeracao', 'todos');
        $clienteId = trim($request->get('clienteId', ''));
        $tipoPessoa = $request->get('tipoPessoa', 'todos');
        $forcarVencimento = $request->get('forcarVencimento', '0');
        $vencimento = $request->get('vencimento', '');
        $competenciaId = $request->get('competencia', '0');
        $cobrancaSeparada = (bool) $request->get('cobrancaSeparada', false);

        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        if ($competencia == null) {
            $competencia = $this->getCompetenciaAtual();
        }

        $cobrancas = array();
        $ano = $competencia->getAno();
        $mes = $competencia->getMes();
        $cobrancasGeradas = " and not exists (select 1 from boleto b, boleto_item bi, competencia c
where bi.boleto_id = b.idboleto and b.competencia_id = c.id
and c.ano = $ano and c.mes = $mes and bi.servico_cliente_id = idservico_cliente)";

        $servicosClientes = ServicoClienteQuery::create()
                ->joinContrato();

        if ($tipoCobrancaGeracao === 'um') {
            $servicosClientes->filterByClienteId($clienteId);
        } else {
            if ($tipoPessoa != 'todos') {
                $servicosClientes->useClienteQuery()->usePessoaQuery()->filterByTipoPessoa($tipoPessoa)->endUse()->endUse();
            }
        }

        $servicosClientes->where("contrato.gerar_cobranca = 1 and (servico_cliente.data_cancelado is null or servico_cliente.data_cancelado = '0000-00-00' or (month(servico_cliente.data_cancelado) = $mes and year(servico_cliente.data_cancelado) = $ano))$cobrancasGeradas")
                //->limit(50000)
                ->groupByIdservicoCliente()
                ->find();

        foreach ($servicosClientes as $servicoCliente) {
            //$contrato = $servicoCliente->Contrato();
            $valorTotal = $servicoCliente->getValor() - $servicoCliente->getValorDesconto();

            $cobranca = array();
            $cobranca['servico'] = $servicoCliente->getIdservicoCliente();
            $cobranca['valor'] = $valorTotal;
            //$cobranca['valor'] = $contrato->getValor();
            $cobrancas[] = $cobranca;
        }

        $retornoView = array('logs' => array());
        if (sizeof($cobrancas) > 0) {
            if ($forcarVencimento == '1') {
                $retornoView = $this->geracaoCobrancas($competencia, $cobrancas, $cobrancaSeparada, $vencimento);
            } else {
                $retornoView = $this->geracaoCobrancas($competencia, $cobrancas, $cobrancaSeparada);
            }
        } else {
            $retornoView['logs'][] = 'Não foi encontrado nenhuma cobrança para a geração.';
        }

        $log = implode(PHP_EOL, $retornoView['logs']);

        $filename = 'tmp/cobrancas_log_' . time() . '.txt';

        $arquivo = fopen($filename, "w");
        fwrite($arquivo, '***************************** Log da Geração de Cobranças *****************************' . PHP_EOL . PHP_EOL);
        fwrite($arquivo, $log);
        fclose($arquivo);

        // Generate response
        $response = new Response();

        // Set headers
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($filename));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
        $response->headers->set('Content-length', filesize($filename));

        // Send headers before outputting anything
        $response->sendHeaders();
        $response->setContent(file_get_contents($filename));

        return $response;
    }

    public function cobrancasGeracaoCobrancaContasAPagarAction(Request $request) {
        $competencia = $this->getCompetenciaAtual();
        $competencias = CompetenciaQuery::create()->orderById(Criteria::DESC)->limit(5)->find();
        $fornecedores = FornecedorQuery::create()
                ->usePessoaQuery()->withColumn('pessoa.razao_social', 'NomePessoa')->filterByEFornecedor(true)->endUse()
                ->findByAtivo(true);

        $retornoView = array();
        $retornoView['competenciaAtual'] = $competencia;
        $retornoView['competencias'] = $competencias;
        $retornoView['fornecedores'] = $fornecedores;
        return $this->render('ImaTelecomBundle:Financeiro:cobrancas-geracao-cobranca-contas-a-pagar.html.twig', $retornoView);
    }

    public function cobrancasGeracaoCobrancaContasAPagarPorFornecedorAction(Request $request) {
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $fornecedorId = $request->get('fornecedorId', '0');
        $competenciaId = $request->get('competencia', '0');
        $vencimento = trim($request->get('vencimento', ''));
        $valor = trim($request->get('valor', ''));
        $descricaoMovimento = trim($request->get('descricaoMovimento', ''));

        $competencia = CompetenciaQuery::create()->findPk($competenciaId);
        if ($competencia == null) {
            $competencia = $this->getCompetenciaAtual();
        }

        $fornecedor = FornecedorQuery::create()
                        ->usePessoaQuery()->filterByEFornecedor(true)->endUse()
                        ->filterByAtivo(true)->findPk($fornecedorId);

        if ($fornecedor == null) {
            $retornoView['erros'][] = "Fornecedor: Campo não pode ser vazio.";
        }

        if (empty($vencimento)) {
            $retornoView['erros'][] = "Vencimento: Campo não pode ser vazio.";
        } else {
            $vencimento = DateTime::createFromFormat('d/m/Y', $vencimento);
        }

        if (empty($valor)) {
            $retornoView['erros'][] = "Valor: Campo não pode ser vazio.";
        } else {
            if (strpos($valor, ',') !== false) {
                $valor = (double) str_replace(',', '.', str_replace('.', '', $valor));
            } else {
                $valor = (double) $valor;
            }
        }

        if (sizeof($retornoView['erros']) == 0) {
            $dados = array();
            $dados['fornecedor'] = $fornecedor;
            $dados['competencia'] = $competencia;
            $dados['vencimento'] = $vencimento;
            $dados['valor'] = $valor;
            $dados['descricaoMovimento'] = $descricaoMovimento;

            $retorno = $this->geracaoCobrancasContaAPagarPorFornecedor($dados);
            $log = implode(PHP_EOL, $retorno['logs']);

            $filename = 'tmp/cobrancas_contas_a_pagar_log_' . time() . '.txt';

            $arquivo = fopen($filename, "w");
            fwrite($arquivo, '***************************** Log da Geração de Cobranças Contas à Pagar *****************************' . PHP_EOL . PHP_EOL);
            fwrite($arquivo, $log);
            fclose($arquivo);

            // Generate response
            $response = new Response();

            // Set headers
            $response->headers->set('Cache-Control', 'private');
            $response->headers->set('Content-type', mime_content_type($filename));
            $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filename) . '";');
            $response->headers->set('Content-length', filesize($filename));

            // Send headers before outputting anything
            $response->sendHeaders();
            $response->setContent(file_get_contents($filename));

            return $response;
        } else {
            return new JsonResponse($retornoView);
        }
    }

    public function cobrancasHistoricoPorCobrancaAction(Request $request, $cobrancaId) {
        $cobranca = BoletoQuery::create()->findPk($cobrancaId);
        $historicos = BoletoBaixaHistoricoQuery::create()->findByBoletoId($cobrancaId);
        $cliente = $cobranca->getCliente();
        $fornecedor = $cobranca->getFornecedor();

        $pessoa = $fornecedor != null ? $fornecedor->getPessoa() : $cliente->getPessoa();
        $tipoPessoa = $pessoa->getTipoPessoa();
        if ($tipoPessoa == 'juridica') {
            $nomeFornecedor = $pessoa->getRazaoSocial();
        } else {
            $nomeFornecedor = $pessoa->getNome();
        }

        $retornoView = array();
        $retornoView['cobranca'] = $cobranca;
        $retornoView['nomeFornecedor'] = $nomeFornecedor;
        $retornoView['historicos'] = $historicos;

        return $this->render('ImaTelecomBundle:Financeiro:cobrancas-historico-por-cobranca.html.twig', $retornoView);
    }

    public function cobrancasHistoricoPorCobrancaDetalhesAction(Request $request, $cobrancaId, $historicoId) {
        $retornoView = array();

        $historico = BoletoBaixaHistoricoQuery::create()->filterByBoletoId($cobrancaId)->findPk($historicoId);
        $lancamento = LancamentosQuery::create()
                ->useConvenioQuery()
                ->withColumn('convenio.nome', 'ConvenioNome')
                ->withColumn('convenio.codigo_convenio', 'ConvenioCodigo')
                ->withColumn('convenio.digito_verificador', 'ConvenioDigitoVerificador')
                ->endUse()
                ->useCompetenciaQuery()->withColumn("concat(competencia.Ano, '/', competencia.Mes)", 'CompetenciaAnoMes')->endUse()
                ->findPk($historico->getLancamentoId());

        if ($lancamento == null) {
            $lancamento = new Lancamentos();
        }

        $pessoa = $historico->getPessoa();
        if ($pessoa == null) {
            $pessoa = new Pessoa();
        }

        $baixa = BaixaQuery::create()
                ->useContaCaixaQuery()->withColumn('conta_caixa.descricao', 'ContaCaixaDescricao')->endUse()
                ->useUsuarioQuery()->withColumn('usuario.login', 'UsuarioBaixaLogin')->endUse()
                ->useCompetenciaQuery()->withColumn("concat(competencia.Ano, '/', competencia.Mes)", 'CompetenciaAnoMes')->endUse()
                ->findPk($historico->getBaixaId());

        if ($baixa == null) {
            $baixa = new Baixa();
            $retornoView['formasPagamento'] = array();
        } else {
            $formasPagamento = BaixaMovimentoQuery::create()
                    ->useFormaPagamentoQuery()->withColumn('forma_pagamento.nome', 'FormaPagamentoNome')->endUse()
                    ->findByBaixaId($baixa->getIdbaixa());

            $retornoView['formasPagamento'] = $formasPagamento->toArray();
        }

        $estorno = BaixaEstornoQuery::create()
                ->useContaCaixaQuery()->withColumn('conta_caixa.descricao', 'ContaCaixaDescricao')->endUse()
                ->useUsuarioQuery()->withColumn('usuario.login', 'UsuarioEstornoLogin')->endUse()
                ->useCompetenciaQuery()->withColumn("concat(competencia.Ano, '/', competencia.Mes)", 'CompetenciaAnoMes')->endUse()
                ->findPk($historico->getBaixaEstornoId());

        if ($estorno == null) {
            $estorno = new BaixaEstorno();
        }


        $retornoView['historico'] = $historico->toArray();
        $retornoView['lancamento'] = $lancamento->toArray();
        $retornoView['cliente'] = $pessoa->toArray();
        $retornoView['baixa'] = $baixa->toArray();
        $retornoView['estorno'] = $estorno->toArray();

        return new JsonResponse($retornoView);
    }

    public function cobrancasRealizacaoBaixaAction(Request $request) {
        $formasPagamento = FormaPagamentoQuery::create()->findByAtivo(true);

        $cobrancaIds = $request->get('lancamentos', array());
        $lancamentos = BoletoQuery::create()->filterByEstaPago(false)->findPks($cobrancaIds); // fazer baixa somente nos que ainda estão em aberto e não foram estornados
        $contasCaixa = ContaCaixaQuery::create()->findByAtivo(true);

        $retornoView = array();
        $retornoView['contasCaixa'] = $contasCaixa;
        $retornoView['formasPagamento'] = $formasPagamento;
        $retornoView['lancamentos'] = $lancamentos;
        return $this->render('ImaTelecomBundle:Financeiro:cobrancas-realizacao-baixa.html.twig', $retornoView);
    }

    public function cobrancasRealizacaoBaixaRealizarBaixaAction(Request $request) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $user = $this->getUser();
        $competencia = $this->getCompetenciaAtual();

        //$dataPagamento = DateTime::createFromFormat('d/m/Y', $request->get('dataPagamento', ''));
        $dataPagamento = new DateTime('now');
        $dataBaixa = DateTime::createFromFormat('d/m/Y', $request->get('dataBaixa', ''));
        $contaCaixaId = trim($request->get('contaCaixa', null));
        $formasPagamento = $request->get('formaPagamento', array());
        $descricaoMovimento = trim($request->get('descricaoMovimento', null));
        $valorConsolidado = $request->get('valorConsolidado', array());
        $valorDescriminado = $request->get('valorDescriminado', array());

        $con = Propel::getWriteConnection(BaixaTableMap::DATABASE_NAME);
        $con->beginTransaction();

        $contaCaixa = ContaCaixaQuery::create()->findPk($contaCaixaId); // verificar de onde irá pegar a conta caixa

        $baixa = new Baixa();
        $baixa->setUsuarioBaixa($user->getIdusuario());
        $baixa->setCompetencia($competencia);
        $baixa->setContaCaixa($contaCaixa);
        $baixa->setDataPagamento($dataPagamento);
        $baixa->setDataBaixa($dataBaixa);
        $baixa->setDescricaoMovimento($descricaoMovimento);
        $baixa->setValorOriginal($valorConsolidado['Original']);
        $baixa->setValorMulta($valorConsolidado['Multa']);
        $baixa->setValorJuros($valorConsolidado['Juros']);
        $baixa->setValorDesconto($valorConsolidado['Desconto']);
        $baixa->setValorTotal($valorConsolidado['Total']);
        $baixa->setDataCadastro(new DateTime('now'));
        $baixa->setDataAlterado(new DateTime('now'));
        $baixa->save();

        foreach ($formasPagamento as $tipoPagamento) {
            $formaPagamento = FormaPagamentoQuery::create()->findPk($tipoPagamento['Tipo']);
            $temNumeroDocumento = $formaPagamento->getTemNumeroDocumento();
            $errosPagamento = array();

            if ($temNumeroDocumento && empty(trim($tipoPagamento['NumeroDocumento']))) {
                $errosPagamento[] = 'Número do Documento não pode ser vazio';
            } elseif (!$temNumeroDocumento && !empty(trim($tipoPagamento['NumeroDocumento']))) {
                $errosPagamento[] = 'Não é possível adicionar o Número do Documento, por favor atualize a Forma de Pagamento.';
            }

            if (!sizeof($errosPagamento)) {
                $baixaMovimento = new BaixaMovimento();
                $baixaMovimento->setBaixa($baixa);
                $baixaMovimento->setUsuarioBaixa($user->getIdusuario());
                $baixaMovimento->setDataBaixa($dataBaixa);
                $baixaMovimento->setFormaPagamento($formaPagamento);
                $baixaMovimento->setValor($tipoPagamento['Valor']);
                $baixaMovimento->setDataCadastro(new DateTime('now'));
                $baixaMovimento->setDataAlterado(new DateTime('now'));

                if ($temNumeroDocumento) {
                    $baixaMovimento->setNumeroDocumento($tipoPagamento['NumeroDocumento']);
                }

                $baixaMovimento->save();
            } else {
                $retornoView['erros'][] = array_merge($errosPagamento);
            }
        }

        foreach ($valorDescriminado as $descriminado) {
            $cobranca = BoletoQuery::create()->findPk($descriminado['Lancamento']);
            if ($cobranca != null) {
                $estaPago = $cobranca->getEstaPago();
                if ($estaPago) {
                    $retornoView['erros'][] = 'O Lançamento: ' . $descriminado['Lancamento'] . ' já se encontra Baixado.';
                }

                $pessoa = $cobranca->getCliente()->getPessoa();

                $cobranca->setEstaPago(true);
                $cobranca->setBaixaEstorno(null);
                $cobranca->setBaixa($baixa);
                $cobranca->setDataPagamento($dataPagamento);
                $cobranca->setValorOriginal($descriminado['Original']);
                $cobranca->setValorMulta($descriminado['Multa']);
                $cobranca->setValorJuros($descriminado['Juros']);
                $cobranca->setValorDesconto($descriminado['Desconto']);
                $cobranca->setValorPago($descriminado['Final']);
                $cobranca->setDataAlterado(new DateTime('now'));
                $cobranca->setUsuarioAlterado($user->getIdusuario());

                $historico = new BoletoBaixaHistorico();
                $historico->setBoleto($cobranca);
                $historico->setLancamentoId(null);
                $historico->setPessoa($pessoa);
                $historico->setCompetencia($competencia);
                $historico->setBaixa($baixa);
                $historico->setStatus('baixado');
                $historico->setValorOriginal($descriminado['Original']);
                $historico->setValorMulta($descriminado['Multa']);
                $historico->setValorJuros($descriminado['Juros']);
                $historico->setValorDesconto($descriminado['Desconto']);
                $historico->setValorTotal($descriminado['Final']);
                $historico->setDataVencimento($cobranca->getVencimento());
                $historico->setDescricaoMovimento($descricaoMovimento);
                $historico->setDataCadastro(new DateTime('now'));
                $historico->setDataAlterado(new DateTime('now'));
                $historico->setUsuarioAlterado($user->getIdusuario());

                try {
                    $cobranca->save();
                    $historico->save();
                } catch (PropelException $ex) {
                    $con->rollBack();
                    $retornoView['erros'][] = 'Não foi possível atualizar as cobranças/histórico para o Lançamento: ' . $descriminado['Lancamento'];
                    $retornoView['erros'][] = $ex->getMessage();
                }
            } else {
                $retornoView['erros'][] = 'Não foi possível localizar o Lançamento: ' . $descriminado['Lancamento'];
            }
        }

        if (sizeof($retornoView['erros'])) {
            $con->rollBack();
        } else {
            $con->commit();
            $retornoView['status'] = true;
            $retornoView['mensagem'] = 'Baixa realizada com sucesso.';
        }

        return new JsonResponse($retornoView);
    }

    public function cobrancasRealizacaoEstornoBaixaAction(Request $request) {
        $cobrancaIds = $request->get('lancamentos', array());
        $lancamentos = BoletoQuery::create()->filterByEstaPago(true)->findPks($cobrancaIds); // fazer o estorno somente nos que forão baixados

        $retornoView = array();
        $retornoView['lancamentos'] = $lancamentos;
        return $this->render('ImaTelecomBundle:Financeiro:cobrancas-realizacao-estorno-baixa.html.twig', $retornoView);
    }

    public function cobrancasRealizacaoEstornoBaixaRealizarEstornoBaixaAction(Request $request) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $user = $this->getUser();
        $competencia = $this->getCompetenciaAtual();

        $dataEstorno = DateTime::createFromFormat('d/m/Y', $request->get('dataEstorno', ''));
        $descricaoMovimento = trim($request->get('descricaoMovimento', null));
        $lancamentos = $request->get('lancamentos', array());

        $lancamentosId = array_keys($lancamentos);

        if (!sizeof($lancamentosId)) {
            $retornoView['erros'][] = "Por favor, selecione ao menos um Lançamento.";
        }

        $con = Propel::getWriteConnection(BaixaTableMap::DATABASE_NAME);
        $con->beginTransaction();

        $lancamentosEstornados = array();
        foreach ($lancamentosId as $lancamentoId) {
            if (!in_array($lancamentoId, $lancamentosEstornados)) {
                $lancamento = BoletoQuery::create()->findPk($lancamentoId);
                if ($lancamento != null) {
                    $baixa = $lancamento->getBaixa();
                    if ($baixa != null) {
                        $contaCaixa = $baixa->getContaCaixaId();
                        $valorOriginal = $baixa->getValorOriginal();
                        $valorMulta = $baixa->getValorMulta();
                        $valorJuros = $baixa->getValorJuros();
                        $valorDesconto = $baixa->getValorDesconto();
                        $valorTotal = $baixa->getValorTotal();
                        $dataBaixa = $baixa->getDataBaixa();

                        if ($dataEstorno < $dataBaixa) {
                            $retornoView['erros'][] = "A Data do Estorno não pode ser menor do que a Data de Pagamento para o Lançamento: $lancamentoId.";
                            continue;
                        }

                        $estorno = new BaixaEstorno();
                        $estorno->setDataCadastro(new DateTime('now'));
                        $estorno->setDataAlterado(new DateTime('now'));
                        $estorno->setBaixa($baixa);
                        $estorno->setContaCaixaId($contaCaixa);
                        $estorno->setCompetencia($competencia);
                        $estorno->setUsuarioEstorno($user->getIdusuario());
                        $estorno->setDataEstorno($dataEstorno);
                        $estorno->setDescricaoMovimento($descricaoMovimento);
                        $estorno->setValorOriginal($valorOriginal);
                        $estorno->setValorMulta($valorMulta);
                        $estorno->setValorJuros($valorJuros);
                        $estorno->setValorDesconto($valorDesconto);
                        $estorno->setValorTotal($valorTotal);

                        $estorno->save();

                        $cobrancasBaixadas = $baixa->getBoletos();
                        foreach ($cobrancasBaixadas as $cobrancaBaixada) {
                            $cobrancaId = $cobrancaBaixada->getIdboleto();

                            if (!in_array($cobrancaId, $lancamentosEstornados)) {
                                $lancamentosEstornados[] = $cobrancaId;
                                $cliente = $cobrancaBaixada->getCliente();
                                $estaPago = $cobrancaBaixada->getEstaPago();

                                if ($estaPago) {
                                    $estornoCobranca = $cobrancaBaixada->getBaixaEstorno();
                                    if ($estornoCobranca == null) {
                                        $valorOriginalCobranca = $cobrancaBaixada->getValorOriginal();
                                        $valorMultaCobranca = $cobrancaBaixada->getValorMulta();
                                        $valorJurosCobranca = $cobrancaBaixada->getValorJuros();
                                        $valorDescontoCobranca = $cobrancaBaixada->getValorDesconto();
                                        $valorTotalCobranca = $cobrancaBaixada->getValorPago();

                                        $historico = new BoletoBaixaHistorico();
                                        $historico->setBoleto($cobrancaBaixada);
                                        $historico->setLancamentoId(null);
                                        $historico->setPessoaId($cliente->getPessoaId());
                                        $historico->setCompetencia($competencia);
                                        $historico->setBaixaEstorno($estorno);
                                        $historico->setStatus('estornado');
                                        $historico->setValorOriginal($valorOriginalCobranca);
                                        $historico->setValorMulta($valorMultaCobranca);
                                        $historico->setValorJuros($valorJurosCobranca);
                                        $historico->setValorDesconto($valorDescontoCobranca);
                                        $historico->setValorTotal($valorTotalCobranca);
                                        $historico->setDataVencimento($cobrancaBaixada->getVencimento());
                                        $historico->setDescricaoMovimento($descricaoMovimento);
                                        $historico->setDataCadastro(new DateTime('now'));
                                        $historico->setDataAlterado(new DateTime('now'));
                                        $historico->setUsuarioAlterado($user->getIdusuario());

                                        $cobrancaBaixada->setBaixa(null);
                                        $cobrancaBaixada->setBaixaEstorno($estorno);
                                        $cobrancaBaixada->setEstaPago(false);
                                        $cobrancaBaixada->setDataPagamento(null);
                                        $cobrancaBaixada->setValorOriginal(null);
                                        $cobrancaBaixada->setValorMulta(null);
                                        $cobrancaBaixada->setValorJuros(null);
                                        $cobrancaBaixada->setValorDesconto(null);
                                        $cobrancaBaixada->setValorPago(null);
                                        $cobrancaBaixada->setDataAlterado(new DateTime('now'));
                                        $cobrancaBaixada->setUsuarioAlterado($user->getIdusuario());

                                        try {
                                            $cobrancaBaixada->save();
                                            $historico->save();
                                        } catch (PropelException $ex) {
                                            $retornoView['erros'][] = 'Não foi possível atualizar as cobranças/histórico para o Lançamento: ' . $cobrancaId;
                                            $retornoView['erros'][] = $ex->getMessage();
                                        }
                                    } else {
                                        $retornoView['erros'][] = "O Lançamento: $cobrancaId se encontra com a Baixa Estornada.";
                                    }
                                } else {
                                    $retornoView['erros'][] = "O Lançamento: $cobrancaId não se encontra Baixado.";
                                }
                            }
                        }
                    } else {
                        $retornoView['erros'][] = "Não foi possível localizar a Baixa para o Lançamento: $lancamentoId.\nVerifique se o mesmo já não foi Estornado.";
                    }
                } else {
                    $retornoView['erros'][] = 'Não foi possível localizar o Lançamento: ' . $lancamentoId;
                }
            }
        }

        if (sizeof($retornoView['erros'])) {
            $con->rollBack();
        } else {
            $con->commit();
            $retornoView['status'] = true;
            $retornoView['mensagem'] = 'Baixa Estornada com sucesso.';
        }

        return new JsonResponse($retornoView);
    }

    public function formasPagamentoAction(Request $request) {
        $formasPagamento = FormaPagamentoQuery::create()->find();

        $retornoView = array();
        $retornoView['formasPagamento'] = $formasPagamento;
        return $this->render('ImaTelecomBundle:Financeiro:formas-pagamento.html.twig', $retornoView);
    }

    public function formasPagamentoFormaPagamentoAction(Request $request, $formaPagamentoId) {
        $formaPagamento = FormaPagamentoQuery::create()->findPk($formaPagamentoId);
        if ($formaPagamento == null) {
            $formaPagamento = new FormaPagamento();
        }

        $retornoView = array();
        $retornoView['formaPagamento'] = $formaPagamento;

        return $this->render('ImaTelecomBundle:Financeiro:forma-pagamento.html.twig', $retornoView);
    }

    public function formasPagamentoFormaPagamentoSalvarAction(Request $request, $formaPagamentoId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $tipo = trim($request->get('tipo', ''));
        $nome = trim($request->get('nome', ''));
        $ativo = trim($request->get('ativo', '0'));
        $temNumeroDocumento = trim($request->get('temNumeroDocumento', '0'));

        if (empty($tipo)) {
            $retornoView['erros'][] = "Tipo: Campo não pode ser vazio.";
        }

        if (empty($nome)) {
            $retornoView['erros'][] = "Nome: Campo não pode ser vazio.";
        }

        $formaPagamento = FormaPagamentoQuery::create()->findPk($formaPagamentoId);
        if ($formaPagamento == null) {
            $formaPagamento = new FormaPagamento();
            $formaPagamento->setDataCadastro(new DateTime('now'));
        }

        $formaPagamento->setTipo($tipo);
        $formaPagamento->setNome($nome);
        $formaPagamento->setAtivo($ativo);
        $formaPagamento->setTemNumeroDocumento($temNumeroDocumento);
        $formaPagamento->setDataAlterado(new DateTime('now'));
        $formaPagamento->setUsuarioAlterado($user->getIdusuario());

        if (sizeof($retornoView['erros']) == 0) {
            try {
                $formaPagamento->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Forma de Pagamento salvo com sucesso.';
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_formas_pagamento');
            } catch (Exception $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function formasPagamentoFormaPagamentoExcluirAction(Request $request, $formaPagamentoId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $formaPagamento = FormaPagamentoQuery::create()->findPk($formaPagamentoId);
        if ($formaPagamento != null) {
            $nome = $formaPagamento->getNome();
            $temPagamento = BoletoBaixaHistoricoQuery::create()->filterByFormaPagamento($formaPagamento)->count();
            if ($temPagamento == 0) {
                try {
                    $formaPagamento->delete();

                    $retornoView['status'] = true;
                    $retornoView['mensagem'] = "$nome foi excluído com sucesso.";
                    $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_formas_pagamento');
                } catch (PropelException $ex) {
                    $retornoView['erros'][] = $ex->getMessage();
                }
            } else {
                $retornoView['erros'][] = "O serviço $nome, está vinculado a algumas baixas, desative a forma de pagamento caso o mesmo esteja ativado.";
            }
        } else {
            $retornoView['erros'][] = 'Não foi encontrado nenhuma forma de pagamento com este código ' . $formaPagamentoId;
        }

        return new JsonResponse($retornoView);
    }

    public function agenciasBancariasAction(Request $request) {
        $agencias = BancoAgenciaQuery::create()->find();

        $retornoView = array();
        $retornoView['agencias'] = $agencias;

        return $this->render('ImaTelecomBundle:Financeiro:agencias-bancarias.html.twig', $retornoView);
    }

    public function agenciasBancariasAgenciaAction(Request $request, $agenciaBancariaId) {
        $agenciaBancaria = BancoAgenciaQuery::create()->findPk($agenciaBancariaId);
        if ($agenciaBancaria == null) {
            $agenciaBancaria = new BancoAgencia();
            $agenciaBancaria->setIdbancoAgencia(0);
            $agenciaBancaria->setEnderecoAgencia(new EnderecoAgencia());
        }

        $retornoView = array();
        $retornoView['agencia'] = $agenciaBancaria;
        $retornoView['enderecoAgencia'] = $agenciaBancaria->getEnderecoAgencia();

        return $this->render('ImaTelecomBundle:Financeiro:agencias-bancarias-agencia.html.twig', $retornoView);
    }

    public function agenciasBancariasAgenciaSalvarAction(Request $request, $agenciaBancariaId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $nome = trim($request->get('nome', ''));
        $numeroAgencia = trim($request->get('numeroAgencia', ''));
        $digitoVerificador = trim($request->get('digitoVerificador', '0'));
        $rua = trim($request->get('rua', ''));
        $numero = trim($request->get('num', ''));
        $bairro = trim($request->get('bairro', ''));
        $cep = trim($request->get('cep', ''));
        $complemento = trim($request->get('complemento', ''));
        $pontoReferencia = trim($request->get('pontoReferencia', ''));
        $cidadeId = trim($request->get('cidadeId', ''));

        $cidade = CidadeQuery::create()->findPk($cidadeId);
        if ($cidade == null) {
            $retornoView['erros'][] = "Cidade: Campo não pode ser vazio.";
        }

        if (empty($nome)) {
            $retornoView['erros'][] = "Nome: Campo não pode ser vazio.";
        }

        if (empty($numeroAgencia)) {
            $retornoView['erros'][] = "Número da Agência: Campo não pode ser vazio.";
        }

        if (empty($rua)) {
            $retornoView['erros'][] = "Rua: Campo não pode ser vazio.";
        }

        if (empty($numero)) {
            $retornoView['erros'][] = "Número: Campo não pode ser vazio.";
        }

        if (empty($bairro)) {
            $retornoView['erros'][] = "Bairro: Campo não pode ser vazio.";
        }

        if (empty($cep)) {
            $retornoView['erros'][] = "CEP: Campo não pode ser vazio.";
        }

        $agencia = BancoAgenciaQuery::create()->findPk($agenciaBancariaId);
        if ($agencia == null) {
            $agencia = new BancoAgencia();
            $agencia->setDataCadastro(new DateTime('now'));
        }

        $enderecoAgencia = $agencia->getEnderecoAgencia();
        if ($enderecoAgencia == null) {
            $enderecoAgencia = new EnderecoAgencia();
            $enderecoAgencia->setDataCadastro(new DateTime('now'));
        }

        if (sizeof($retornoView['erros']) == 0) {
            $con = Propel::getWriteConnection(BancoAgenciaTableMap::DATABASE_NAME);
            $con->beginTransaction();

            $enderecoAgencia->setRua($rua);
            $enderecoAgencia->setBairro($bairro);
            $enderecoAgencia->setNum($numero);
            $enderecoAgencia->setComplemento($complemento);
            $enderecoAgencia->setCep($cep);
            $enderecoAgencia->setPontoReferencia($pontoReferencia);
            $enderecoAgencia->setCidade($cidade);
            $enderecoAgencia->setDataAlterado(new DateTime('now'));
            $enderecoAgencia->setUsuarioAlterado($user->getIdusuario());

            $agencia->setNome($nome);
            $agencia->setAgencia($numeroAgencia);
            $agencia->setDigitoVerificador($digitoVerificador);
            $agencia->setDataAlterado(new DateTime('now'));
            $agencia->setUsuarioAlterado($user->getIdusuario());

            try {
                $enderecoAgencia->save();
                $agencia->setEnderecoAgencia($enderecoAgencia);
                $agencia->save();

                $con->commit();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Agência Bancária salva com sucesso.';
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_agencias_bancarias');
            } catch (PropelException $ex) {
                $con->rollBack();

                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function contasBancariasAction(Request $request) {
        $contas = BancoAgenciaContaQuery::create()->find();

        $retornoView = array();
        $retornoView['contas'] = $contas;

        return $this->render('ImaTelecomBundle:Financeiro:contas-bancarias.html.twig', $retornoView);
    }

    public function contasBancariasContaAction(Request $request, $contaBancariaId) {
        $contaBancaria = BancoAgenciaContaQuery::create()->findPk($contaBancariaId);
        if ($contaBancaria == null) {
            $contaBancaria = new BancoAgenciaConta();
            $contaBancaria->setIdbancoAgenciaConta(0);
        }

        $bancos = BancoQuery::create()->find();
        $agencias = BancoAgenciaQuery::create()->find();
        $convenios = ConvenioQuery::create()->find();

        $retornoView = array();
        $retornoView['bancos'] = $bancos;
        $retornoView['agencias'] = $agencias;
        $retornoView['convenios'] = $convenios;
        $retornoView['contaBancaria'] = $contaBancaria;

        return $this->render('ImaTelecomBundle:Financeiro:contas-bancarias-conta.html.twig', $retornoView);
    }

    public function contasBancariasContaDetalhesAction(Request $request, $contaBancariaId) {
        $contaBancaria = BancoAgenciaContaQuery::create()
                ->useBancoQuery()->withColumn('banco.nome', 'BancoNome')->endUse()
                ->useBancoAgenciaQuery()->withColumn('banco_agencia.nome', 'BancoAgenciaNome')->endUse()
                ->findPk($contaBancariaId);

        if ($contaBancaria == null) {
            $contaBancaria = new BancoAgenciaConta();
            $contaBancaria->setBanco(new Banco());
            $contaBancaria->setBancoAgencia(new BancoAgencia());
            $contaBancaria->setConvenio(new Convenio());
        }

        $retornoView = array();
        $retornoView['contaBancaria'] = $contaBancaria->toArray();

        return new JsonResponse($retornoView);
    }

    public function contasBancariasContaSalvarAction(Request $request, $contaBancariaId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $tipoConta = trim($request->get('tipoConta', ''));
        $numeroConta = trim($request->get('numeroConta', ''));
        $digitoVerificador = trim($request->get('digitoVerificador', '0'));
        $bancoId = trim($request->get('banco', '0'));
        $bancoAgenciaId = trim($request->get('agencia', '0'));
        $convenioId = trim($request->get('convenio', '0'));

        $banco = BancoQuery::create()->findPk($bancoId);
        $bancoAgencia = BancoAgenciaQuery::create()->findPk($bancoAgenciaId);
        $convenio = ConvenioQuery::create()->findPk($convenioId);

        if (empty($tipoConta)) {
            $retornoView['erros'][] = "Tipo da Conta: Campo não pode ser vazio.";
        }

        if (empty($numeroConta)) {
            $retornoView['erros'][] = "Número da Conta: Campo não pode ser vazio.";
        }

        if ($banco == null) {
            $retornoView['erros'][] = "Banco: Campo não pode ser vazio.";
        }

        if ($bancoAgencia == null) {
            $retornoView['erros'][] = "Agência: Campo não pode ser vazio.";
        }

        if ($convenio == null) {
            $retornoView['erros'][] = "Convênio: Campo não pode ser vazio.";
        }

        $bancoAgenciaConta = BancoAgenciaContaQuery::create()->findPk($contaBancariaId);
        if ($bancoAgenciaConta == null) {
            $bancoAgenciaConta = new BancoAgenciaConta();
            $bancoAgenciaConta->setDataCadastro(new DateTime('now'));
        }

        $bancoAgenciaConta->setTipoConta($tipoConta);
        $bancoAgenciaConta->setNumeroConta($numeroConta);
        $bancoAgenciaConta->setDigitoVerificador($digitoVerificador);
        $bancoAgenciaConta->setBanco($banco);
        $bancoAgenciaConta->setBancoAgencia($bancoAgencia);
        $bancoAgenciaConta->setConvenio($convenio);
        $bancoAgenciaConta->setDataAlterado(new DateTime('now'));
        $bancoAgenciaConta->setUsuarioAlterado($user->getIdusuario());

        if (sizeof($retornoView['erros']) == 0) {
            try {
                $bancoAgenciaConta->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Conta Bancária salva com sucesso.';
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_contas_bancarias');
            } catch (PropelException $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function contasBancariasContaExcluirAction(Request $request, $contaBancariaId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $bancoAgenciaConta = BancoAgenciaContaQuery::create()->findPk($contaBancariaId);
        if ($bancoAgenciaConta != null) {
            $tipoConta = $bancoAgenciaConta->getTipoConta() == 'poupanca' ? 'Poupança' : 'Corrente';
            $nome = $bancoAgenciaConta->getNumeroConta() . ' do tipo ' . $tipoConta;

            $temContaCaixa = ContaCaixaQuery::create()->filterByBancoAgenciaConta($bancoAgenciaConta)->count();
            if ($temContaCaixa == 0) {
                try {
                    $bancoAgenciaConta->delete();

                    $retornoView['status'] = true;
                    $retornoView['mensagem'] = "A Conta Bancária <strong>$nome</strong> foi excluída com sucesso.";
                    $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_contas_bancarias');
                } catch (PropelException $ex) {
                    $retornoView['erros'][] = $ex->getMessage();
                }
            } else {
                $retornoView['erros'][] = "A Conta Bancária <strong>$nome</strong>, está vinculada a algumas Contas Caixa, por favor, mude a Conta Bancária dessas Contas Caixa para poder prosseguir com a Exclusão.";
            }
        } else {
            $retornoView['erros'][] = 'Não foi encontrado nenhuma Conta Bancária com este código ' . $contaBancariaId;
        }

        return new JsonResponse($retornoView);
    }

    public function contasBancariasRastrearContasCaixaVinculadasAction(Request $request, $contaBancariaId) {
        $contaBancaria = BancoAgenciaContaQuery::create()
                ->useBancoQuery()->withColumn('banco.nome', 'BancoNome')->endUse()
                ->useBancoAgenciaQuery()->withColumn('banco_agencia.nome', 'BancoAgenciaNome')->endUse()
                ->findPk($contaBancariaId);

        $contasCaixa = $contaBancaria->getContaCaixas();

        $retornoView = array();
        $retornoView['contaBancaria'] = $contaBancaria;
        $retornoView['contasCaixa'] = $contasCaixa;

        return $this->render('ImaTelecomBundle:Financeiro:contas-bancarias-rastrear-contas-caixa-vinculadas.html.twig', $retornoView);
    }

    public function contasBancariasContasCaixaPorContaBancariaAction(Request $request, $contaBancariaId) {
        $contasCaixa = ContaCaixaQuery::create()->findByBancoAgenciaContaId($contaBancariaId);

        $retornoView = array();
        $retornoView['contasCaixa'] = $contasCaixa->toArray();

        return new JsonResponse($retornoView);
    }

    public function bancosAction(Request $request) {
        $bancos = BancoQuery::create()->find();

        $retornoView = array();
        $retornoView['bancos'] = $bancos;

        return $this->render('ImaTelecomBundle:Financeiro:bancos.html.twig', $retornoView);
    }

    public function bancosBancoAction(Request $request, $bancoId) {
        $banco = BancoQuery::create()->findPk($bancoId);
        if ($banco == null) {
            $banco = new Banco();
            $banco->setIdbanco(0);
        }

        $retornoView = array();
        $retornoView['banco'] = $banco;

        return $this->render('ImaTelecomBundle:Financeiro:bancos-banco.html.twig', $retornoView);
    }

    public function bancosBancoSalvarAction(Request $request, $bancoId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $nome = trim($request->get('nome', ''));
        $codigoFebraban = trim($request->get('codigoFebraban', ''));
        $ativo = trim($request->get('ativo', '0'));

        if (empty($nome)) {
            $retornoView['erros'][] = "Nome: Campo não pode ser vazio.";
        }

        if (empty($codigoFebraban)) {
            $retornoView['erros'][] = "Código Febraban: Campo não pode ser vazio.";
        }

        if ($ativo == null) {
            $retornoView['erros'][] = "Ativo: Campo não pode ser vazio.";
        }

        $banco = BancoQuery::create()->findPk($bancoId);
        if ($banco == null) {
            $banco = new Banco();
            $banco->setDataCadastro(new DateTime('now'));
        }

        $banco->setNome($nome);
        $banco->setCodigoFebraban($codigoFebraban);
        $banco->setAtivo($ativo);
        $banco->setDataAlterado(new DateTime('now'));
        $banco->setUsuarioAlterado($user->getIdusuario());

        if (sizeof($retornoView['erros']) == 0) {
            try {
                $banco->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Banco salva com sucesso.';
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_bancos');
            } catch (PropelException $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function conveniosAction(Request $request) {
        $convenios = ConvenioQuery::create()->find();

        $retornoView = array();
        $retornoView['convenios'] = $convenios;

        return $this->render('ImaTelecomBundle:Financeiro:convenios.html.twig', $retornoView);
    }

    public function conveniosConvenioAction(Request $request, $convenioId) {
        $convenio = ConvenioQuery::create()->findPk($convenioId);
        if ($convenio == null) {
            $convenio = new Convenio();
            $convenio->setIdconvenio(0);
        }

        $empresas = EmpresaQuery::create()->findByAtivo(true);
        $empresa = $convenio->getEmpresa();
        if ($empresa == null) {
            $empresa = new Empresa();
            $empresa->setPessoa(new Pessoa());
        }

        $retornoView = array();
        $retornoView['convenio'] = $convenio;
        $retornoView['empresaAtual'] = $empresa;
        $retornoView['empresas'] = $empresas;

        return $this->render('ImaTelecomBundle:Financeiro:convenios-convenio.html.twig', $retornoView);
    }

    public function conveniosConvenioSalvarAction(Request $request, $convenioId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $nome = trim($request->get('nome', ''));
        $codigoConvenio = trim($request->get('codigoConvenio', ''));
        $ativo = trim($request->get('ativo', '0'));
        $digitoVerificador = trim($request->get('digitoVerificador', ''));
        $cnpjConvenio = trim($request->get('cnpjConvenio', ''));
        $tipoCobranca = trim($request->get('tipoCobranca', ''));
        $tipoNossoNumero = trim($request->get('tipoNossoNumero', ''));
        $limiteInferior = trim($request->get('limiteInferior', ''));
        $limiteSuperior = trim($request->get('limiteSuperior', ''));
        $ultimoSequencialArquivoCobranca = trim($request->get('ultimoSequencialArquivoCobranca', ''));
        $valorCodigoBarra = trim($request->get('valorCodigoBarra', ''));
        $carteira = trim($request->get('carteira', ''));
        $tipoCarteira = trim($request->get('tipoCarteira', ''));
        $empresaId = trim($request->get('empresaId', ''));

        $empresa = EmpresaQuery::create()->findPk($empresaId);

        if (empty($nome)) {
            $retornoView['erros'][] = "Nome: Campo não pode ser vazio.";
        }

        if (empty($codigoConvenio)) {
            $retornoView['erros'][] = "Código Convênio: Campo não pode ser vazio.";
        }

        if ($ativo == null) {
            $retornoView['erros'][] = "Ativo: Campo não pode ser vazio.";
        }

        if ($cnpjConvenio == null) {
            $retornoView['erros'][] = "CNPJ: Campo não pode ser vazio.";
        }

        if ($tipoCobranca == null) {
            $retornoView['erros'][] = "Tipo da Cobrança: Campo não pode ser vazio.";
        }

        if ($tipoNossoNumero == null) {
            $retornoView['erros'][] = "Tipo do Nosso Número: Campo não pode ser vazio.";
        }

        if ($limiteInferior == null) {
            $retornoView['erros'][] = "Limite Inferior: Campo não pode ser vazio.";
        }

        if ($limiteSuperior == null) {
            $retornoView['erros'][] = "Limite Superior: Campo não pode ser vazio.";
        }

        if ($ultimoSequencialArquivoCobranca == null) {
            $retornoView['erros'][] = "Último Seq. Arq. Cobrança: Campo não pode ser vazio.";
        }

        if ($valorCodigoBarra == null) {
            $retornoView['erros'][] = "Valor Código de Barra: Campo não pode ser vazio.";
        }

        if ($carteira == null) {
            $retornoView['erros'][] = "Carteira: Campo não pode ser vazio.";
        }

        if ($tipoCarteira == null) {
            $retornoView['erros'][] = "Tipo da Carteira: Campo não pode ser vazio.";
        }

        if ($empresa == null) {
            $retornoView['erros'][] = "Empresa: Campo não pode ser vazio.";
        }

        $convenio = ConvenioQuery::create()->findPk($convenioId);
        if ($convenio == null) {
            $convenio = new Convenio();
            $convenio->setDataCadastro(new DateTime('now'));
        }

        $convenio->setNome($nome);
        $convenio->setCodigoConvenio($codigoConvenio);
        $convenio->setDigitoVerificador($digitoVerificador);
        $convenio->setCarteira($carteira);
        $convenio->setTipoCarteira($tipoCarteira);
        $convenio->setCnpj($cnpjConvenio);
        $convenio->setTipoCobranca($tipoCobranca);
        $convenio->setTipoNossoNumero($tipoNossoNumero);
        $convenio->setLimiteInferior($limiteInferior);
        $convenio->setLimiteSuperior($limiteSuperior);
        $convenio->setValorCodigoBarra($valorCodigoBarra);
        $convenio->setUltimoSequencialArquivoCobranca($ultimoSequencialArquivoCobranca);
        $convenio->setUltimoSequencial(0);
        $convenio->setUltimoSequencialDebitoAutomatico(0);
        $convenio->setEmpresa($empresa);
        $convenio->setAtivo($ativo);
        $convenio->setDataAlterado(new DateTime('now'));
        $convenio->setUsuarioAlterado($user->getIdusuario());

        if (sizeof($retornoView['erros']) == 0) {
            try {
                $convenio->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Convênio salvo com sucesso.';
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_convenios');
            } catch (PropelException $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function conveniosConvenioExcluirAction(Request $request, $convenioId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $convenio = ConvenioQuery::create()->findPk($convenioId);
        if ($convenio != null) {
            $nome = $convenio->getNome();
            $temConvenio = BancoAgenciaContaQuery::create()->filterByConvenio($convenio)->count();
            if ($temConvenio == 0) {
                try {
                    $convenio->delete();

                    $retornoView['status'] = true;
                    $retornoView['mensagem'] = "Convênio <strong>$nome</strong> foi excluído com sucesso.";
                    $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_convenios');
                } catch (PropelException $ex) {
                    $retornoView['erros'][] = $ex->getMessage();
                }
            } else {
                $retornoView['erros'][] = "O Convênio <strong>$nome</strong>, está vinculado a algumas Contas Bancárias, por favor, mude o Convênio dessas Contas ou desative o Convênio caso esteja Ativado.";
            }
        } else {
            $retornoView['erros'][] = 'Não foi encontrado nenhum Convênio com este código ' . $convenioId;
        }

        return new JsonResponse($retornoView);
    }

    public function conveniosRastrearContasBancariasVinculadasAction(Request $request, $convenioId) {
        $convenio = ConvenioQuery::create()->findPk($convenioId);
        $contasBancarias = $convenio->getBancoAgenciaContas();

        $retornoView = array();
        $retornoView['convenio'] = $convenio;
        $retornoView['contasBancarias'] = $contasBancarias;

        return $this->render('ImaTelecomBundle:Financeiro:convenios-rastrear-contas-bancarias-vinculadas.html.twig', $retornoView);
    }

    public function conveniosContasBancariasPorConvenioAction(Request $request, $convenioId) {
        $contasBancarias = BancoAgenciaContaQuery::create()
                ->useBancoQuery()->withColumn('banco.nome', 'NomeBanco')->endUse()
                ->useBancoAgenciaQuery()->withColumn('banco_agencia.nome', 'NomeAgencia')->endUse()
                ->findByConvenioId($convenioId);

        $retornoView = array();
        $retornoView['contasBancarias'] = $contasBancarias->toArray();

        return new JsonResponse($retornoView);
    }

    public function contasCaixaAction(Request $request) {
        $contasCaixa = ContaCaixaQuery::create()->find();

        $retornoView = array();
        $retornoView['contasCaixa'] = $contasCaixa;

        return $this->render('ImaTelecomBundle:Financeiro:contas-caixa.html.twig', $retornoView);
    }

    public function contasCaixaContaAction(Request $request, $contaCaixaId) {
        $contaCaixa = ContaCaixaQuery::create()->findPk($contaCaixaId);
        if ($contaCaixa == null) {
            $contaCaixa = new ContaCaixa();
            $contaCaixa->setIdcontaCaixa(0);
        }

        $contaBancaria = $contaCaixa->getBancoAgenciaConta();
        if ($contaBancaria == null) {
            $contaBancaria = new BancoAgenciaConta();
            $contaBancaria->setBanco(new Banco());
            $contaBancaria->setBancoAgencia(new BancoAgencia());
            $contaBancaria->setConvenio(new Convenio());
        }

        $contasBancarias = BancoAgenciaContaQuery::create()
                ->useBancoQuery()->withColumn('banco.nome', 'BancoNome')->endUse()
                ->useBancoAgenciaQuery()->withColumn('banco_agencia.nome', 'BancoAgenciaNome')->endUse()
                ->find();

        $retornoView = array();
        $retornoView['contaCaixa'] = $contaCaixa;
        $retornoView['contaBancariaAtual'] = $contaBancaria;
        $retornoView['contasBancarias'] = $contasBancarias;

        return $this->render('ImaTelecomBundle:Financeiro:contas-caixa-conta.html.twig', $retornoView);
    }

    public function contasCaixaContaSalvarAction(Request $request, $contaCaixaId) {
        $user = $this->getUser();

        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $descricao = trim($request->get('descricao', ''));
        $ativo = trim($request->get('ativo', '0'));
        $bancoAgenciaContaId = trim($request->get('bancoAgenciaContaId', ''));

        $contaBancaria = BancoAgenciaContaQuery::create()->findPk($bancoAgenciaContaId);

        if (empty($descricao)) {
            $retornoView['erros'][] = "Descrição: Campo não pode ser vazio.";
        }

//        if (empty($ativo)) {
//            $retornoView['erros'][] = "Ativo: Campo não pode ser vazio.";
//        }

        if ($contaBancaria == null) {
            $retornoView['erros'][] = "Conta Bancária: Campo não pode ser vazio.";
        }

        $contaCaixa = ContaCaixaQuery::create()->findPk($contaCaixaId);
        if ($contaCaixa == null) {
            $contaCaixa = new ContaCaixa();
            $contaCaixa->setDataCadastro(new DateTime('now'));
        }

        $contaCaixa->setDescricao($descricao);
        $contaCaixa->setBancoAgenciaConta($contaBancaria);
        $contaCaixa->setAtivo($ativo);
        $contaCaixa->setDataAlterado(new DateTime('now'));
        $contaCaixa->setUsuarioAlterado($user->getIdusuario());

        if (sizeof($retornoView['erros']) == 0) {
            try {
                $contaCaixa->save();

                $retornoView['status'] = true;
                $retornoView['mensagem'] = 'Conta Caixa salva com sucesso.';
                $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_contas_caixa');
            } catch (PropelException $ex) {
                $retornoView['erros'][] = $ex->getMessage();
            }
        }

        return new JsonResponse($retornoView);
    }

    public function contasCaixaContaExcluirAction(Request $request, $contaCaixaId) {
        $retornoView = array();
        $retornoView['status'] = false;
        $retornoView['erros'] = array();

        $contaCaixa = ContaCaixaQuery::create()->findPk($contaCaixaId);
        if ($contaCaixa != null) {
            $nome = $contaCaixa->getDescricao();
            $temBaixa = BaixaQuery::create()->filterByContaCaixa($contaCaixa)->count();
            if ($temBaixa == 0) {
                try {
                    $contaCaixa->delete();

                    $retornoView['status'] = true;
                    $retornoView['mensagem'] = "Conta Caixa <strong>$nome</strong> foi excluída com sucesso.";
                    $retornoView['redirect_url'] = $this->generateUrl('ima_telecom_admin_financeiro_contas_caixa');
                } catch (PropelException $ex) {
                    $retornoView['erros'][] = $ex->getMessage();
                }
            } else {
                $retornoView['erros'][] = "A Conta Caixa <strong>$nome</strong>, está vinculado a algumas Baixas, por motivos de histórico e segurança na integridade do sistema, não podemos excluí-la.";
            }
        } else {
            $retornoView['erros'][] = 'Não foi encontrado nenhuma Conta Caixa com este código ' . $contaCaixaId;
        }

        return new JsonResponse($retornoView);
    }

}
