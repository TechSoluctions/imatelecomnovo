<?php

namespace ImaTelecomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ImaTelecomBundle:Default:index.html.twig', array('name' => $name));
    }
}
