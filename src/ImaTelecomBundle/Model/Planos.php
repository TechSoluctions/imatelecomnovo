<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Planos as BasePlanos;

/**
 * Skeleton subclass for representing a row from the 'planos' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Planos extends BasePlanos
{

}
