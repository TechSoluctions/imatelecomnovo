<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Lancamentos as BaseLancamentos;

/**
 * Skeleton subclass for representing a row from the 'lancamentos' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Lancamentos extends BaseLancamentos {

    public function usuario() {
        $usuario = UsuarioQuery::create()->findPk($this->getUsuarioAlterado());
        if ($usuario == null)
            $usuario = new Usuario();
        return $usuario;
    }

    public function LancamentosBoletos() {
        $itens = LancamentosBoletosQuery::create()->findByLancamentoId($this->idlancamento);
        return $itens;
    }
}
