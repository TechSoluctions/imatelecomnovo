<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Competencia as BaseCompetencia;

/**
 * Skeleton subclass for representing a row from the 'competencia' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Competencia extends BaseCompetencia
{

    public function hasProximaCompetencia() {
        if ($this->getProximaCompetencia() == 0)
            return false;
        else 
            return true;
    }
    
    public function getCompetenciaFutura() {
        $competenciaFutura = new Competencia();
        if ($this->hasProximaCompetencia())
            $competenciaFutura = CompetenciaQuery::create()->findPk($this->getProximaCompetencia());        
        return $competenciaFutura;
    }
    
    public function hasParcelaPorServicoCliente($servicoCliente) {        
        if (!is_numeric($servicoCliente)) {
            $servicoCliente = $servicoCliente->getIdservicoCliente();
        }
        
        $cobranca = BoletoQuery::create()->joinBoletoItem()->where("servico_cliente_id = $servicoCliente")->findOneByCompetenciaId($this->id);        
        return $cobranca != null ? true : false;
    }
}
