<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\EstoqueLancamentoItemQuery as BaseEstoqueLancamentoItemQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'estoque_lancamento_item' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class EstoqueLancamentoItemQuery extends BaseEstoqueLancamentoItemQuery
{

}
