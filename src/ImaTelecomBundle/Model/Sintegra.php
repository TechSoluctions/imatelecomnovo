<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Sintegra as BaseSintegra;

/**
 * Skeleton subclass for representing a row from the 'sintegra' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Sintegra extends BaseSintegra
{

    
    public function usuario() {
        $usuario = UsuarioQuery::create()->findPk($this->getUsuarioAlterado());        
        if ($usuario == null)
            $usuario = new Usuario();
        return $usuario;
    }
}
