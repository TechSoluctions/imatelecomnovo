<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Empresa as ChildEmpresa;
use ImaTelecomBundle\Model\EmpresaQuery as ChildEmpresaQuery;
use ImaTelecomBundle\Model\Map\EmpresaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'empresa' table.
 *
 *
 *
 * @method     ChildEmpresaQuery orderByIdempresa($order = Criteria::ASC) Order by the idempresa column
 * @method     ChildEmpresaQuery orderByPessoaId($order = Criteria::ASC) Order by the pessoa_id column
 * @method     ChildEmpresaQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildEmpresaQuery orderByTipoGeracaoCobranca($order = Criteria::ASC) Order by the tipo_geracao_cobranca column
 * @method     ChildEmpresaQuery orderByUltimoLoteEnviado($order = Criteria::ASC) Order by the ultimo_lote_enviado column
 * @method     ChildEmpresaQuery orderByUltimoRpsEnviado($order = Criteria::ASC) Order by the ultimo_rps_enviado column
 * @method     ChildEmpresaQuery orderByCertificadoDigital($order = Criteria::ASC) Order by the certificado_digital column
 * @method     ChildEmpresaQuery orderByNotasPorLote($order = Criteria::ASC) Order by the notas_por_lote column
 *
 * @method     ChildEmpresaQuery groupByIdempresa() Group by the idempresa column
 * @method     ChildEmpresaQuery groupByPessoaId() Group by the pessoa_id column
 * @method     ChildEmpresaQuery groupByAtivo() Group by the ativo column
 * @method     ChildEmpresaQuery groupByTipoGeracaoCobranca() Group by the tipo_geracao_cobranca column
 * @method     ChildEmpresaQuery groupByUltimoLoteEnviado() Group by the ultimo_lote_enviado column
 * @method     ChildEmpresaQuery groupByUltimoRpsEnviado() Group by the ultimo_rps_enviado column
 * @method     ChildEmpresaQuery groupByCertificadoDigital() Group by the certificado_digital column
 * @method     ChildEmpresaQuery groupByNotasPorLote() Group by the notas_por_lote column
 *
 * @method     ChildEmpresaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEmpresaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEmpresaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEmpresaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEmpresaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEmpresaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEmpresaQuery leftJoinPessoa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pessoa relation
 * @method     ChildEmpresaQuery rightJoinPessoa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pessoa relation
 * @method     ChildEmpresaQuery innerJoinPessoa($relationAlias = null) Adds a INNER JOIN clause to the query using the Pessoa relation
 *
 * @method     ChildEmpresaQuery joinWithPessoa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pessoa relation
 *
 * @method     ChildEmpresaQuery leftJoinWithPessoa() Adds a LEFT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildEmpresaQuery rightJoinWithPessoa() Adds a RIGHT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildEmpresaQuery innerJoinWithPessoa() Adds a INNER JOIN clause and with to the query using the Pessoa relation
 *
 * @method     ChildEmpresaQuery leftJoinConvenio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Convenio relation
 * @method     ChildEmpresaQuery rightJoinConvenio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Convenio relation
 * @method     ChildEmpresaQuery innerJoinConvenio($relationAlias = null) Adds a INNER JOIN clause to the query using the Convenio relation
 *
 * @method     ChildEmpresaQuery joinWithConvenio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Convenio relation
 *
 * @method     ChildEmpresaQuery leftJoinWithConvenio() Adds a LEFT JOIN clause and with to the query using the Convenio relation
 * @method     ChildEmpresaQuery rightJoinWithConvenio() Adds a RIGHT JOIN clause and with to the query using the Convenio relation
 * @method     ChildEmpresaQuery innerJoinWithConvenio() Adds a INNER JOIN clause and with to the query using the Convenio relation
 *
 * @method     \ImaTelecomBundle\Model\PessoaQuery|\ImaTelecomBundle\Model\ConvenioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEmpresa findOne(ConnectionInterface $con = null) Return the first ChildEmpresa matching the query
 * @method     ChildEmpresa findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEmpresa matching the query, or a new ChildEmpresa object populated from the query conditions when no match is found
 *
 * @method     ChildEmpresa findOneByIdempresa(int $idempresa) Return the first ChildEmpresa filtered by the idempresa column
 * @method     ChildEmpresa findOneByPessoaId(int $pessoa_id) Return the first ChildEmpresa filtered by the pessoa_id column
 * @method     ChildEmpresa findOneByAtivo(boolean $ativo) Return the first ChildEmpresa filtered by the ativo column
 * @method     ChildEmpresa findOneByTipoGeracaoCobranca(string $tipo_geracao_cobranca) Return the first ChildEmpresa filtered by the tipo_geracao_cobranca column
 * @method     ChildEmpresa findOneByUltimoLoteEnviado(int $ultimo_lote_enviado) Return the first ChildEmpresa filtered by the ultimo_lote_enviado column
 * @method     ChildEmpresa findOneByUltimoRpsEnviado(int $ultimo_rps_enviado) Return the first ChildEmpresa filtered by the ultimo_rps_enviado column
 * @method     ChildEmpresa findOneByCertificadoDigital(string $certificado_digital) Return the first ChildEmpresa filtered by the certificado_digital column
 * @method     ChildEmpresa findOneByNotasPorLote(int $notas_por_lote) Return the first ChildEmpresa filtered by the notas_por_lote column *

 * @method     ChildEmpresa requirePk($key, ConnectionInterface $con = null) Return the ChildEmpresa by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEmpresa requireOne(ConnectionInterface $con = null) Return the first ChildEmpresa matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEmpresa requireOneByIdempresa(int $idempresa) Return the first ChildEmpresa filtered by the idempresa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEmpresa requireOneByPessoaId(int $pessoa_id) Return the first ChildEmpresa filtered by the pessoa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEmpresa requireOneByAtivo(boolean $ativo) Return the first ChildEmpresa filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEmpresa requireOneByTipoGeracaoCobranca(string $tipo_geracao_cobranca) Return the first ChildEmpresa filtered by the tipo_geracao_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEmpresa requireOneByUltimoLoteEnviado(int $ultimo_lote_enviado) Return the first ChildEmpresa filtered by the ultimo_lote_enviado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEmpresa requireOneByUltimoRpsEnviado(int $ultimo_rps_enviado) Return the first ChildEmpresa filtered by the ultimo_rps_enviado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEmpresa requireOneByCertificadoDigital(string $certificado_digital) Return the first ChildEmpresa filtered by the certificado_digital column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEmpresa requireOneByNotasPorLote(int $notas_por_lote) Return the first ChildEmpresa filtered by the notas_por_lote column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEmpresa[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEmpresa objects based on current ModelCriteria
 * @method     ChildEmpresa[]|ObjectCollection findByIdempresa(int $idempresa) Return ChildEmpresa objects filtered by the idempresa column
 * @method     ChildEmpresa[]|ObjectCollection findByPessoaId(int $pessoa_id) Return ChildEmpresa objects filtered by the pessoa_id column
 * @method     ChildEmpresa[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildEmpresa objects filtered by the ativo column
 * @method     ChildEmpresa[]|ObjectCollection findByTipoGeracaoCobranca(string $tipo_geracao_cobranca) Return ChildEmpresa objects filtered by the tipo_geracao_cobranca column
 * @method     ChildEmpresa[]|ObjectCollection findByUltimoLoteEnviado(int $ultimo_lote_enviado) Return ChildEmpresa objects filtered by the ultimo_lote_enviado column
 * @method     ChildEmpresa[]|ObjectCollection findByUltimoRpsEnviado(int $ultimo_rps_enviado) Return ChildEmpresa objects filtered by the ultimo_rps_enviado column
 * @method     ChildEmpresa[]|ObjectCollection findByCertificadoDigital(string $certificado_digital) Return ChildEmpresa objects filtered by the certificado_digital column
 * @method     ChildEmpresa[]|ObjectCollection findByNotasPorLote(int $notas_por_lote) Return ChildEmpresa objects filtered by the notas_por_lote column
 * @method     ChildEmpresa[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EmpresaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\EmpresaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Empresa', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEmpresaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEmpresaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEmpresaQuery) {
            return $criteria;
        }
        $query = new ChildEmpresaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEmpresa|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EmpresaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EmpresaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEmpresa A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idempresa, pessoa_id, ativo, tipo_geracao_cobranca, ultimo_lote_enviado, ultimo_rps_enviado, certificado_digital, notas_por_lote FROM empresa WHERE idempresa = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEmpresa $obj */
            $obj = new ChildEmpresa();
            $obj->hydrate($row);
            EmpresaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEmpresa|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EmpresaTableMap::COL_IDEMPRESA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EmpresaTableMap::COL_IDEMPRESA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idempresa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdempresa(1234); // WHERE idempresa = 1234
     * $query->filterByIdempresa(array(12, 34)); // WHERE idempresa IN (12, 34)
     * $query->filterByIdempresa(array('min' => 12)); // WHERE idempresa > 12
     * </code>
     *
     * @param     mixed $idempresa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByIdempresa($idempresa = null, $comparison = null)
    {
        if (is_array($idempresa)) {
            $useMinMax = false;
            if (isset($idempresa['min'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_IDEMPRESA, $idempresa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idempresa['max'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_IDEMPRESA, $idempresa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaTableMap::COL_IDEMPRESA, $idempresa, $comparison);
    }

    /**
     * Filter the query on the pessoa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPessoaId(1234); // WHERE pessoa_id = 1234
     * $query->filterByPessoaId(array(12, 34)); // WHERE pessoa_id IN (12, 34)
     * $query->filterByPessoaId(array('min' => 12)); // WHERE pessoa_id > 12
     * </code>
     *
     * @see       filterByPessoa()
     *
     * @param     mixed $pessoaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByPessoaId($pessoaId = null, $comparison = null)
    {
        if (is_array($pessoaId)) {
            $useMinMax = false;
            if (isset($pessoaId['min'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_PESSOA_ID, $pessoaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pessoaId['max'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_PESSOA_ID, $pessoaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaTableMap::COL_PESSOA_ID, $pessoaId, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(EmpresaTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the tipo_geracao_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoGeracaoCobranca('fooValue');   // WHERE tipo_geracao_cobranca = 'fooValue'
     * $query->filterByTipoGeracaoCobranca('%fooValue%', Criteria::LIKE); // WHERE tipo_geracao_cobranca LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoGeracaoCobranca The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByTipoGeracaoCobranca($tipoGeracaoCobranca = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoGeracaoCobranca)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaTableMap::COL_TIPO_GERACAO_COBRANCA, $tipoGeracaoCobranca, $comparison);
    }

    /**
     * Filter the query on the ultimo_lote_enviado column
     *
     * Example usage:
     * <code>
     * $query->filterByUltimoLoteEnviado(1234); // WHERE ultimo_lote_enviado = 1234
     * $query->filterByUltimoLoteEnviado(array(12, 34)); // WHERE ultimo_lote_enviado IN (12, 34)
     * $query->filterByUltimoLoteEnviado(array('min' => 12)); // WHERE ultimo_lote_enviado > 12
     * </code>
     *
     * @param     mixed $ultimoLoteEnviado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByUltimoLoteEnviado($ultimoLoteEnviado = null, $comparison = null)
    {
        if (is_array($ultimoLoteEnviado)) {
            $useMinMax = false;
            if (isset($ultimoLoteEnviado['min'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_ULTIMO_LOTE_ENVIADO, $ultimoLoteEnviado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ultimoLoteEnviado['max'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_ULTIMO_LOTE_ENVIADO, $ultimoLoteEnviado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaTableMap::COL_ULTIMO_LOTE_ENVIADO, $ultimoLoteEnviado, $comparison);
    }

    /**
     * Filter the query on the ultimo_rps_enviado column
     *
     * Example usage:
     * <code>
     * $query->filterByUltimoRpsEnviado(1234); // WHERE ultimo_rps_enviado = 1234
     * $query->filterByUltimoRpsEnviado(array(12, 34)); // WHERE ultimo_rps_enviado IN (12, 34)
     * $query->filterByUltimoRpsEnviado(array('min' => 12)); // WHERE ultimo_rps_enviado > 12
     * </code>
     *
     * @param     mixed $ultimoRpsEnviado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByUltimoRpsEnviado($ultimoRpsEnviado = null, $comparison = null)
    {
        if (is_array($ultimoRpsEnviado)) {
            $useMinMax = false;
            if (isset($ultimoRpsEnviado['min'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_ULTIMO_RPS_ENVIADO, $ultimoRpsEnviado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ultimoRpsEnviado['max'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_ULTIMO_RPS_ENVIADO, $ultimoRpsEnviado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaTableMap::COL_ULTIMO_RPS_ENVIADO, $ultimoRpsEnviado, $comparison);
    }

    /**
     * Filter the query on the certificado_digital column
     *
     * Example usage:
     * <code>
     * $query->filterByCertificadoDigital('fooValue');   // WHERE certificado_digital = 'fooValue'
     * $query->filterByCertificadoDigital('%fooValue%', Criteria::LIKE); // WHERE certificado_digital LIKE '%fooValue%'
     * </code>
     *
     * @param     string $certificadoDigital The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByCertificadoDigital($certificadoDigital = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($certificadoDigital)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaTableMap::COL_CERTIFICADO_DIGITAL, $certificadoDigital, $comparison);
    }

    /**
     * Filter the query on the notas_por_lote column
     *
     * Example usage:
     * <code>
     * $query->filterByNotasPorLote(1234); // WHERE notas_por_lote = 1234
     * $query->filterByNotasPorLote(array(12, 34)); // WHERE notas_por_lote IN (12, 34)
     * $query->filterByNotasPorLote(array('min' => 12)); // WHERE notas_por_lote > 12
     * </code>
     *
     * @param     mixed $notasPorLote The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByNotasPorLote($notasPorLote = null, $comparison = null)
    {
        if (is_array($notasPorLote)) {
            $useMinMax = false;
            if (isset($notasPorLote['min'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_NOTAS_POR_LOTE, $notasPorLote['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($notasPorLote['max'])) {
                $this->addUsingAlias(EmpresaTableMap::COL_NOTAS_POR_LOTE, $notasPorLote['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EmpresaTableMap::COL_NOTAS_POR_LOTE, $notasPorLote, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Pessoa object
     *
     * @param \ImaTelecomBundle\Model\Pessoa|ObjectCollection $pessoa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByPessoa($pessoa, $comparison = null)
    {
        if ($pessoa instanceof \ImaTelecomBundle\Model\Pessoa) {
            return $this
                ->addUsingAlias(EmpresaTableMap::COL_PESSOA_ID, $pessoa->getId(), $comparison);
        } elseif ($pessoa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EmpresaTableMap::COL_PESSOA_ID, $pessoa->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPessoa() only accepts arguments of type \ImaTelecomBundle\Model\Pessoa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pessoa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function joinPessoa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pessoa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pessoa');
        }

        return $this;
    }

    /**
     * Use the Pessoa relation Pessoa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\PessoaQuery A secondary query class using the current class as primary query
     */
    public function usePessoaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPessoa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pessoa', '\ImaTelecomBundle\Model\PessoaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Convenio object
     *
     * @param \ImaTelecomBundle\Model\Convenio|ObjectCollection $convenio the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEmpresaQuery The current query, for fluid interface
     */
    public function filterByConvenio($convenio, $comparison = null)
    {
        if ($convenio instanceof \ImaTelecomBundle\Model\Convenio) {
            return $this
                ->addUsingAlias(EmpresaTableMap::COL_IDEMPRESA, $convenio->getEmpresaId(), $comparison);
        } elseif ($convenio instanceof ObjectCollection) {
            return $this
                ->useConvenioQuery()
                ->filterByPrimaryKeys($convenio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByConvenio() only accepts arguments of type \ImaTelecomBundle\Model\Convenio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Convenio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function joinConvenio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Convenio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Convenio');
        }

        return $this;
    }

    /**
     * Use the Convenio relation Convenio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ConvenioQuery A secondary query class using the current class as primary query
     */
    public function useConvenioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinConvenio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Convenio', '\ImaTelecomBundle\Model\ConvenioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEmpresa $empresa Object to remove from the list of results
     *
     * @return $this|ChildEmpresaQuery The current query, for fluid interface
     */
    public function prune($empresa = null)
    {
        if ($empresa) {
            $this->addUsingAlias(EmpresaTableMap::COL_IDEMPRESA, $empresa->getIdempresa(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the empresa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EmpresaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EmpresaTableMap::clearInstancePool();
            EmpresaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EmpresaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EmpresaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EmpresaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EmpresaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EmpresaQuery
