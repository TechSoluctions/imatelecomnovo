<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\CaixaMovimento as ChildCaixaMovimento;
use ImaTelecomBundle\Model\CaixaMovimentoQuery as ChildCaixaMovimentoQuery;
use ImaTelecomBundle\Model\Map\CaixaMovimentoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'caixa_movimento' table.
 *
 *
 *
 * @method     ChildCaixaMovimentoQuery orderByIdcaixaMovimento($order = Criteria::ASC) Order by the idcaixa_movimento column
 * @method     ChildCaixaMovimentoQuery orderByFormaPagamentoId($order = Criteria::ASC) Order by the forma_pagamento_id column
 * @method     ChildCaixaMovimentoQuery orderByValor($order = Criteria::ASC) Order by the valor column
 * @method     ChildCaixaMovimentoQuery orderByCaixaId($order = Criteria::ASC) Order by the caixa_id column
 * @method     ChildCaixaMovimentoQuery orderByChequeBanco($order = Criteria::ASC) Order by the cheque_banco column
 * @method     ChildCaixaMovimentoQuery orderByChequeConta($order = Criteria::ASC) Order by the cheque_conta column
 * @method     ChildCaixaMovimentoQuery orderByChequeNumero($order = Criteria::ASC) Order by the cheque_numero column
 *
 * @method     ChildCaixaMovimentoQuery groupByIdcaixaMovimento() Group by the idcaixa_movimento column
 * @method     ChildCaixaMovimentoQuery groupByFormaPagamentoId() Group by the forma_pagamento_id column
 * @method     ChildCaixaMovimentoQuery groupByValor() Group by the valor column
 * @method     ChildCaixaMovimentoQuery groupByCaixaId() Group by the caixa_id column
 * @method     ChildCaixaMovimentoQuery groupByChequeBanco() Group by the cheque_banco column
 * @method     ChildCaixaMovimentoQuery groupByChequeConta() Group by the cheque_conta column
 * @method     ChildCaixaMovimentoQuery groupByChequeNumero() Group by the cheque_numero column
 *
 * @method     ChildCaixaMovimentoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCaixaMovimentoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCaixaMovimentoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCaixaMovimentoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCaixaMovimentoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCaixaMovimentoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCaixaMovimentoQuery leftJoinCaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Caixa relation
 * @method     ChildCaixaMovimentoQuery rightJoinCaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Caixa relation
 * @method     ChildCaixaMovimentoQuery innerJoinCaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Caixa relation
 *
 * @method     ChildCaixaMovimentoQuery joinWithCaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Caixa relation
 *
 * @method     ChildCaixaMovimentoQuery leftJoinWithCaixa() Adds a LEFT JOIN clause and with to the query using the Caixa relation
 * @method     ChildCaixaMovimentoQuery rightJoinWithCaixa() Adds a RIGHT JOIN clause and with to the query using the Caixa relation
 * @method     ChildCaixaMovimentoQuery innerJoinWithCaixa() Adds a INNER JOIN clause and with to the query using the Caixa relation
 *
 * @method     ChildCaixaMovimentoQuery leftJoinFormaPagamento($relationAlias = null) Adds a LEFT JOIN clause to the query using the FormaPagamento relation
 * @method     ChildCaixaMovimentoQuery rightJoinFormaPagamento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FormaPagamento relation
 * @method     ChildCaixaMovimentoQuery innerJoinFormaPagamento($relationAlias = null) Adds a INNER JOIN clause to the query using the FormaPagamento relation
 *
 * @method     ChildCaixaMovimentoQuery joinWithFormaPagamento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FormaPagamento relation
 *
 * @method     ChildCaixaMovimentoQuery leftJoinWithFormaPagamento() Adds a LEFT JOIN clause and with to the query using the FormaPagamento relation
 * @method     ChildCaixaMovimentoQuery rightJoinWithFormaPagamento() Adds a RIGHT JOIN clause and with to the query using the FormaPagamento relation
 * @method     ChildCaixaMovimentoQuery innerJoinWithFormaPagamento() Adds a INNER JOIN clause and with to the query using the FormaPagamento relation
 *
 * @method     \ImaTelecomBundle\Model\CaixaQuery|\ImaTelecomBundle\Model\FormaPagamentoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCaixaMovimento findOne(ConnectionInterface $con = null) Return the first ChildCaixaMovimento matching the query
 * @method     ChildCaixaMovimento findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCaixaMovimento matching the query, or a new ChildCaixaMovimento object populated from the query conditions when no match is found
 *
 * @method     ChildCaixaMovimento findOneByIdcaixaMovimento(int $idcaixa_movimento) Return the first ChildCaixaMovimento filtered by the idcaixa_movimento column
 * @method     ChildCaixaMovimento findOneByFormaPagamentoId(int $forma_pagamento_id) Return the first ChildCaixaMovimento filtered by the forma_pagamento_id column
 * @method     ChildCaixaMovimento findOneByValor(string $valor) Return the first ChildCaixaMovimento filtered by the valor column
 * @method     ChildCaixaMovimento findOneByCaixaId(int $caixa_id) Return the first ChildCaixaMovimento filtered by the caixa_id column
 * @method     ChildCaixaMovimento findOneByChequeBanco(string $cheque_banco) Return the first ChildCaixaMovimento filtered by the cheque_banco column
 * @method     ChildCaixaMovimento findOneByChequeConta(string $cheque_conta) Return the first ChildCaixaMovimento filtered by the cheque_conta column
 * @method     ChildCaixaMovimento findOneByChequeNumero(string $cheque_numero) Return the first ChildCaixaMovimento filtered by the cheque_numero column *

 * @method     ChildCaixaMovimento requirePk($key, ConnectionInterface $con = null) Return the ChildCaixaMovimento by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixaMovimento requireOne(ConnectionInterface $con = null) Return the first ChildCaixaMovimento matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCaixaMovimento requireOneByIdcaixaMovimento(int $idcaixa_movimento) Return the first ChildCaixaMovimento filtered by the idcaixa_movimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixaMovimento requireOneByFormaPagamentoId(int $forma_pagamento_id) Return the first ChildCaixaMovimento filtered by the forma_pagamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixaMovimento requireOneByValor(string $valor) Return the first ChildCaixaMovimento filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixaMovimento requireOneByCaixaId(int $caixa_id) Return the first ChildCaixaMovimento filtered by the caixa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixaMovimento requireOneByChequeBanco(string $cheque_banco) Return the first ChildCaixaMovimento filtered by the cheque_banco column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixaMovimento requireOneByChequeConta(string $cheque_conta) Return the first ChildCaixaMovimento filtered by the cheque_conta column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixaMovimento requireOneByChequeNumero(string $cheque_numero) Return the first ChildCaixaMovimento filtered by the cheque_numero column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCaixaMovimento[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCaixaMovimento objects based on current ModelCriteria
 * @method     ChildCaixaMovimento[]|ObjectCollection findByIdcaixaMovimento(int $idcaixa_movimento) Return ChildCaixaMovimento objects filtered by the idcaixa_movimento column
 * @method     ChildCaixaMovimento[]|ObjectCollection findByFormaPagamentoId(int $forma_pagamento_id) Return ChildCaixaMovimento objects filtered by the forma_pagamento_id column
 * @method     ChildCaixaMovimento[]|ObjectCollection findByValor(string $valor) Return ChildCaixaMovimento objects filtered by the valor column
 * @method     ChildCaixaMovimento[]|ObjectCollection findByCaixaId(int $caixa_id) Return ChildCaixaMovimento objects filtered by the caixa_id column
 * @method     ChildCaixaMovimento[]|ObjectCollection findByChequeBanco(string $cheque_banco) Return ChildCaixaMovimento objects filtered by the cheque_banco column
 * @method     ChildCaixaMovimento[]|ObjectCollection findByChequeConta(string $cheque_conta) Return ChildCaixaMovimento objects filtered by the cheque_conta column
 * @method     ChildCaixaMovimento[]|ObjectCollection findByChequeNumero(string $cheque_numero) Return ChildCaixaMovimento objects filtered by the cheque_numero column
 * @method     ChildCaixaMovimento[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CaixaMovimentoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\CaixaMovimentoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\CaixaMovimento', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCaixaMovimentoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCaixaMovimentoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCaixaMovimentoQuery) {
            return $criteria;
        }
        $query = new ChildCaixaMovimentoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCaixaMovimento|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CaixaMovimentoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CaixaMovimentoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCaixaMovimento A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcaixa_movimento, forma_pagamento_id, valor, caixa_id, cheque_banco, cheque_conta, cheque_numero FROM caixa_movimento WHERE idcaixa_movimento = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCaixaMovimento $obj */
            $obj = new ChildCaixaMovimento();
            $obj->hydrate($row);
            CaixaMovimentoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCaixaMovimento|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CaixaMovimentoTableMap::COL_IDCAIXA_MOVIMENTO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CaixaMovimentoTableMap::COL_IDCAIXA_MOVIMENTO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcaixa_movimento column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcaixaMovimento(1234); // WHERE idcaixa_movimento = 1234
     * $query->filterByIdcaixaMovimento(array(12, 34)); // WHERE idcaixa_movimento IN (12, 34)
     * $query->filterByIdcaixaMovimento(array('min' => 12)); // WHERE idcaixa_movimento > 12
     * </code>
     *
     * @param     mixed $idcaixaMovimento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByIdcaixaMovimento($idcaixaMovimento = null, $comparison = null)
    {
        if (is_array($idcaixaMovimento)) {
            $useMinMax = false;
            if (isset($idcaixaMovimento['min'])) {
                $this->addUsingAlias(CaixaMovimentoTableMap::COL_IDCAIXA_MOVIMENTO, $idcaixaMovimento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcaixaMovimento['max'])) {
                $this->addUsingAlias(CaixaMovimentoTableMap::COL_IDCAIXA_MOVIMENTO, $idcaixaMovimento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaMovimentoTableMap::COL_IDCAIXA_MOVIMENTO, $idcaixaMovimento, $comparison);
    }

    /**
     * Filter the query on the forma_pagamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFormaPagamentoId(1234); // WHERE forma_pagamento_id = 1234
     * $query->filterByFormaPagamentoId(array(12, 34)); // WHERE forma_pagamento_id IN (12, 34)
     * $query->filterByFormaPagamentoId(array('min' => 12)); // WHERE forma_pagamento_id > 12
     * </code>
     *
     * @see       filterByFormaPagamento()
     *
     * @param     mixed $formaPagamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByFormaPagamentoId($formaPagamentoId = null, $comparison = null)
    {
        if (is_array($formaPagamentoId)) {
            $useMinMax = false;
            if (isset($formaPagamentoId['min'])) {
                $this->addUsingAlias(CaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($formaPagamentoId['max'])) {
                $this->addUsingAlias(CaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(CaixaMovimentoTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(CaixaMovimentoTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaMovimentoTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query on the caixa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCaixaId(1234); // WHERE caixa_id = 1234
     * $query->filterByCaixaId(array(12, 34)); // WHERE caixa_id IN (12, 34)
     * $query->filterByCaixaId(array('min' => 12)); // WHERE caixa_id > 12
     * </code>
     *
     * @see       filterByCaixa()
     *
     * @param     mixed $caixaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByCaixaId($caixaId = null, $comparison = null)
    {
        if (is_array($caixaId)) {
            $useMinMax = false;
            if (isset($caixaId['min'])) {
                $this->addUsingAlias(CaixaMovimentoTableMap::COL_CAIXA_ID, $caixaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($caixaId['max'])) {
                $this->addUsingAlias(CaixaMovimentoTableMap::COL_CAIXA_ID, $caixaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaMovimentoTableMap::COL_CAIXA_ID, $caixaId, $comparison);
    }

    /**
     * Filter the query on the cheque_banco column
     *
     * Example usage:
     * <code>
     * $query->filterByChequeBanco('fooValue');   // WHERE cheque_banco = 'fooValue'
     * $query->filterByChequeBanco('%fooValue%', Criteria::LIKE); // WHERE cheque_banco LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chequeBanco The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByChequeBanco($chequeBanco = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chequeBanco)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaMovimentoTableMap::COL_CHEQUE_BANCO, $chequeBanco, $comparison);
    }

    /**
     * Filter the query on the cheque_conta column
     *
     * Example usage:
     * <code>
     * $query->filterByChequeConta('fooValue');   // WHERE cheque_conta = 'fooValue'
     * $query->filterByChequeConta('%fooValue%', Criteria::LIKE); // WHERE cheque_conta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chequeConta The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByChequeConta($chequeConta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chequeConta)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaMovimentoTableMap::COL_CHEQUE_CONTA, $chequeConta, $comparison);
    }

    /**
     * Filter the query on the cheque_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByChequeNumero('fooValue');   // WHERE cheque_numero = 'fooValue'
     * $query->filterByChequeNumero('%fooValue%', Criteria::LIKE); // WHERE cheque_numero LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chequeNumero The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByChequeNumero($chequeNumero = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chequeNumero)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaMovimentoTableMap::COL_CHEQUE_NUMERO, $chequeNumero, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Caixa object
     *
     * @param \ImaTelecomBundle\Model\Caixa|ObjectCollection $caixa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByCaixa($caixa, $comparison = null)
    {
        if ($caixa instanceof \ImaTelecomBundle\Model\Caixa) {
            return $this
                ->addUsingAlias(CaixaMovimentoTableMap::COL_CAIXA_ID, $caixa->getIdcaixa(), $comparison);
        } elseif ($caixa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CaixaMovimentoTableMap::COL_CAIXA_ID, $caixa->toKeyValue('PrimaryKey', 'Idcaixa'), $comparison);
        } else {
            throw new PropelException('filterByCaixa() only accepts arguments of type \ImaTelecomBundle\Model\Caixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Caixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function joinCaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Caixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Caixa');
        }

        return $this;
    }

    /**
     * Use the Caixa relation Caixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CaixaQuery A secondary query class using the current class as primary query
     */
    public function useCaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Caixa', '\ImaTelecomBundle\Model\CaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\FormaPagamento object
     *
     * @param \ImaTelecomBundle\Model\FormaPagamento|ObjectCollection $formaPagamento The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByFormaPagamento($formaPagamento, $comparison = null)
    {
        if ($formaPagamento instanceof \ImaTelecomBundle\Model\FormaPagamento) {
            return $this
                ->addUsingAlias(CaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamento->getIdformaPagamento(), $comparison);
        } elseif ($formaPagamento instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamento->toKeyValue('PrimaryKey', 'IdformaPagamento'), $comparison);
        } else {
            throw new PropelException('filterByFormaPagamento() only accepts arguments of type \ImaTelecomBundle\Model\FormaPagamento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FormaPagamento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function joinFormaPagamento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FormaPagamento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FormaPagamento');
        }

        return $this;
    }

    /**
     * Use the FormaPagamento relation FormaPagamento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FormaPagamentoQuery A secondary query class using the current class as primary query
     */
    public function useFormaPagamentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFormaPagamento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FormaPagamento', '\ImaTelecomBundle\Model\FormaPagamentoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCaixaMovimento $caixaMovimento Object to remove from the list of results
     *
     * @return $this|ChildCaixaMovimentoQuery The current query, for fluid interface
     */
    public function prune($caixaMovimento = null)
    {
        if ($caixaMovimento) {
            $this->addUsingAlias(CaixaMovimentoTableMap::COL_IDCAIXA_MOVIMENTO, $caixaMovimento->getIdcaixaMovimento(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the caixa_movimento table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CaixaMovimentoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CaixaMovimentoTableMap::clearInstancePool();
            CaixaMovimentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CaixaMovimentoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CaixaMovimentoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CaixaMovimentoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CaixaMovimentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CaixaMovimentoQuery
