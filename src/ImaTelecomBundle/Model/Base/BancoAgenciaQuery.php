<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BancoAgencia as ChildBancoAgencia;
use ImaTelecomBundle\Model\BancoAgenciaQuery as ChildBancoAgenciaQuery;
use ImaTelecomBundle\Model\Map\BancoAgenciaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'banco_agencia' table.
 *
 *
 *
 * @method     ChildBancoAgenciaQuery orderByIdbancoAgencia($order = Criteria::ASC) Order by the idbanco_agencia column
 * @method     ChildBancoAgenciaQuery orderByAgencia($order = Criteria::ASC) Order by the agencia column
 * @method     ChildBancoAgenciaQuery orderByDigitoVerificador($order = Criteria::ASC) Order by the digito_verificador column
 * @method     ChildBancoAgenciaQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildBancoAgenciaQuery orderByEnderecoAgenciaId($order = Criteria::ASC) Order by the endereco_agencia_id column
 * @method     ChildBancoAgenciaQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildBancoAgenciaQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildBancoAgenciaQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildBancoAgenciaQuery groupByIdbancoAgencia() Group by the idbanco_agencia column
 * @method     ChildBancoAgenciaQuery groupByAgencia() Group by the agencia column
 * @method     ChildBancoAgenciaQuery groupByDigitoVerificador() Group by the digito_verificador column
 * @method     ChildBancoAgenciaQuery groupByNome() Group by the nome column
 * @method     ChildBancoAgenciaQuery groupByEnderecoAgenciaId() Group by the endereco_agencia_id column
 * @method     ChildBancoAgenciaQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildBancoAgenciaQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildBancoAgenciaQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildBancoAgenciaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBancoAgenciaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBancoAgenciaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBancoAgenciaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBancoAgenciaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBancoAgenciaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBancoAgenciaQuery leftJoinEnderecoAgencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoAgencia relation
 * @method     ChildBancoAgenciaQuery rightJoinEnderecoAgencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoAgencia relation
 * @method     ChildBancoAgenciaQuery innerJoinEnderecoAgencia($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoAgencia relation
 *
 * @method     ChildBancoAgenciaQuery joinWithEnderecoAgencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoAgencia relation
 *
 * @method     ChildBancoAgenciaQuery leftJoinWithEnderecoAgencia() Adds a LEFT JOIN clause and with to the query using the EnderecoAgencia relation
 * @method     ChildBancoAgenciaQuery rightJoinWithEnderecoAgencia() Adds a RIGHT JOIN clause and with to the query using the EnderecoAgencia relation
 * @method     ChildBancoAgenciaQuery innerJoinWithEnderecoAgencia() Adds a INNER JOIN clause and with to the query using the EnderecoAgencia relation
 *
 * @method     ChildBancoAgenciaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBancoAgenciaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBancoAgenciaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBancoAgenciaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBancoAgenciaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBancoAgenciaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBancoAgenciaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildBancoAgenciaQuery leftJoinBancoAgenciaConta($relationAlias = null) Adds a LEFT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildBancoAgenciaQuery rightJoinBancoAgenciaConta($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildBancoAgenciaQuery innerJoinBancoAgenciaConta($relationAlias = null) Adds a INNER JOIN clause to the query using the BancoAgenciaConta relation
 *
 * @method     ChildBancoAgenciaQuery joinWithBancoAgenciaConta($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildBancoAgenciaQuery leftJoinWithBancoAgenciaConta() Adds a LEFT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildBancoAgenciaQuery rightJoinWithBancoAgenciaConta() Adds a RIGHT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildBancoAgenciaQuery innerJoinWithBancoAgenciaConta() Adds a INNER JOIN clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     \ImaTelecomBundle\Model\EnderecoAgenciaQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BancoAgenciaContaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBancoAgencia findOne(ConnectionInterface $con = null) Return the first ChildBancoAgencia matching the query
 * @method     ChildBancoAgencia findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBancoAgencia matching the query, or a new ChildBancoAgencia object populated from the query conditions when no match is found
 *
 * @method     ChildBancoAgencia findOneByIdbancoAgencia(int $idbanco_agencia) Return the first ChildBancoAgencia filtered by the idbanco_agencia column
 * @method     ChildBancoAgencia findOneByAgencia(string $agencia) Return the first ChildBancoAgencia filtered by the agencia column
 * @method     ChildBancoAgencia findOneByDigitoVerificador(string $digito_verificador) Return the first ChildBancoAgencia filtered by the digito_verificador column
 * @method     ChildBancoAgencia findOneByNome(string $nome) Return the first ChildBancoAgencia filtered by the nome column
 * @method     ChildBancoAgencia findOneByEnderecoAgenciaId(int $endereco_agencia_id) Return the first ChildBancoAgencia filtered by the endereco_agencia_id column
 * @method     ChildBancoAgencia findOneByDataCadastro(string $data_cadastro) Return the first ChildBancoAgencia filtered by the data_cadastro column
 * @method     ChildBancoAgencia findOneByDataAlterado(string $data_alterado) Return the first ChildBancoAgencia filtered by the data_alterado column
 * @method     ChildBancoAgencia findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBancoAgencia filtered by the usuario_alterado column *

 * @method     ChildBancoAgencia requirePk($key, ConnectionInterface $con = null) Return the ChildBancoAgencia by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgencia requireOne(ConnectionInterface $con = null) Return the first ChildBancoAgencia matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBancoAgencia requireOneByIdbancoAgencia(int $idbanco_agencia) Return the first ChildBancoAgencia filtered by the idbanco_agencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgencia requireOneByAgencia(string $agencia) Return the first ChildBancoAgencia filtered by the agencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgencia requireOneByDigitoVerificador(string $digito_verificador) Return the first ChildBancoAgencia filtered by the digito_verificador column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgencia requireOneByNome(string $nome) Return the first ChildBancoAgencia filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgencia requireOneByEnderecoAgenciaId(int $endereco_agencia_id) Return the first ChildBancoAgencia filtered by the endereco_agencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgencia requireOneByDataCadastro(string $data_cadastro) Return the first ChildBancoAgencia filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgencia requireOneByDataAlterado(string $data_alterado) Return the first ChildBancoAgencia filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgencia requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBancoAgencia filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBancoAgencia[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBancoAgencia objects based on current ModelCriteria
 * @method     ChildBancoAgencia[]|ObjectCollection findByIdbancoAgencia(int $idbanco_agencia) Return ChildBancoAgencia objects filtered by the idbanco_agencia column
 * @method     ChildBancoAgencia[]|ObjectCollection findByAgencia(string $agencia) Return ChildBancoAgencia objects filtered by the agencia column
 * @method     ChildBancoAgencia[]|ObjectCollection findByDigitoVerificador(string $digito_verificador) Return ChildBancoAgencia objects filtered by the digito_verificador column
 * @method     ChildBancoAgencia[]|ObjectCollection findByNome(string $nome) Return ChildBancoAgencia objects filtered by the nome column
 * @method     ChildBancoAgencia[]|ObjectCollection findByEnderecoAgenciaId(int $endereco_agencia_id) Return ChildBancoAgencia objects filtered by the endereco_agencia_id column
 * @method     ChildBancoAgencia[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildBancoAgencia objects filtered by the data_cadastro column
 * @method     ChildBancoAgencia[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildBancoAgencia objects filtered by the data_alterado column
 * @method     ChildBancoAgencia[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildBancoAgencia objects filtered by the usuario_alterado column
 * @method     ChildBancoAgencia[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BancoAgenciaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\BancoAgenciaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\BancoAgencia', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBancoAgenciaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBancoAgenciaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBancoAgenciaQuery) {
            return $criteria;
        }
        $query = new ChildBancoAgenciaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBancoAgencia|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BancoAgenciaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BancoAgenciaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoAgencia A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idbanco_agencia, agencia, digito_verificador, nome, endereco_agencia_id, data_cadastro, data_alterado, usuario_alterado FROM banco_agencia WHERE idbanco_agencia = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBancoAgencia $obj */
            $obj = new ChildBancoAgencia();
            $obj->hydrate($row);
            BancoAgenciaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBancoAgencia|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idbanco_agencia column
     *
     * Example usage:
     * <code>
     * $query->filterByIdbancoAgencia(1234); // WHERE idbanco_agencia = 1234
     * $query->filterByIdbancoAgencia(array(12, 34)); // WHERE idbanco_agencia IN (12, 34)
     * $query->filterByIdbancoAgencia(array('min' => 12)); // WHERE idbanco_agencia > 12
     * </code>
     *
     * @param     mixed $idbancoAgencia The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByIdbancoAgencia($idbancoAgencia = null, $comparison = null)
    {
        if (is_array($idbancoAgencia)) {
            $useMinMax = false;
            if (isset($idbancoAgencia['min'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA, $idbancoAgencia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idbancoAgencia['max'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA, $idbancoAgencia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA, $idbancoAgencia, $comparison);
    }

    /**
     * Filter the query on the agencia column
     *
     * Example usage:
     * <code>
     * $query->filterByAgencia('fooValue');   // WHERE agencia = 'fooValue'
     * $query->filterByAgencia('%fooValue%', Criteria::LIKE); // WHERE agencia LIKE '%fooValue%'
     * </code>
     *
     * @param     string $agencia The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByAgencia($agencia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($agencia)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_AGENCIA, $agencia, $comparison);
    }

    /**
     * Filter the query on the digito_verificador column
     *
     * Example usage:
     * <code>
     * $query->filterByDigitoVerificador('fooValue');   // WHERE digito_verificador = 'fooValue'
     * $query->filterByDigitoVerificador('%fooValue%', Criteria::LIKE); // WHERE digito_verificador LIKE '%fooValue%'
     * </code>
     *
     * @param     string $digitoVerificador The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByDigitoVerificador($digitoVerificador = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($digitoVerificador)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_DIGITO_VERIFICADOR, $digitoVerificador, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the endereco_agencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEnderecoAgenciaId(1234); // WHERE endereco_agencia_id = 1234
     * $query->filterByEnderecoAgenciaId(array(12, 34)); // WHERE endereco_agencia_id IN (12, 34)
     * $query->filterByEnderecoAgenciaId(array('min' => 12)); // WHERE endereco_agencia_id > 12
     * </code>
     *
     * @see       filterByEnderecoAgencia()
     *
     * @param     mixed $enderecoAgenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByEnderecoAgenciaId($enderecoAgenciaId = null, $comparison = null)
    {
        if (is_array($enderecoAgenciaId)) {
            $useMinMax = false;
            if (isset($enderecoAgenciaId['min'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_ENDERECO_AGENCIA_ID, $enderecoAgenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enderecoAgenciaId['max'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_ENDERECO_AGENCIA_ID, $enderecoAgenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_ENDERECO_AGENCIA_ID, $enderecoAgenciaId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(BancoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoAgencia object
     *
     * @param \ImaTelecomBundle\Model\EnderecoAgencia|ObjectCollection $enderecoAgencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByEnderecoAgencia($enderecoAgencia, $comparison = null)
    {
        if ($enderecoAgencia instanceof \ImaTelecomBundle\Model\EnderecoAgencia) {
            return $this
                ->addUsingAlias(BancoAgenciaTableMap::COL_ENDERECO_AGENCIA_ID, $enderecoAgencia->getIdenderecoAgencia(), $comparison);
        } elseif ($enderecoAgencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BancoAgenciaTableMap::COL_ENDERECO_AGENCIA_ID, $enderecoAgencia->toKeyValue('PrimaryKey', 'IdenderecoAgencia'), $comparison);
        } else {
            throw new PropelException('filterByEnderecoAgencia() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoAgencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoAgencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function joinEnderecoAgencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoAgencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoAgencia');
        }

        return $this;
    }

    /**
     * Use the EnderecoAgencia relation EnderecoAgencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoAgenciaQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoAgenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEnderecoAgencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoAgencia', '\ImaTelecomBundle\Model\EnderecoAgenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(BancoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BancoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BancoAgenciaConta object
     *
     * @param \ImaTelecomBundle\Model\BancoAgenciaConta|ObjectCollection $bancoAgenciaConta the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function filterByBancoAgenciaConta($bancoAgenciaConta, $comparison = null)
    {
        if ($bancoAgenciaConta instanceof \ImaTelecomBundle\Model\BancoAgenciaConta) {
            return $this
                ->addUsingAlias(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA, $bancoAgenciaConta->getBancoAgenciaId(), $comparison);
        } elseif ($bancoAgenciaConta instanceof ObjectCollection) {
            return $this
                ->useBancoAgenciaContaQuery()
                ->filterByPrimaryKeys($bancoAgenciaConta->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBancoAgenciaConta() only accepts arguments of type \ImaTelecomBundle\Model\BancoAgenciaConta or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BancoAgenciaConta relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function joinBancoAgenciaConta($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BancoAgenciaConta');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BancoAgenciaConta');
        }

        return $this;
    }

    /**
     * Use the BancoAgenciaConta relation BancoAgenciaConta object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoAgenciaContaQuery A secondary query class using the current class as primary query
     */
    public function useBancoAgenciaContaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBancoAgenciaConta($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BancoAgenciaConta', '\ImaTelecomBundle\Model\BancoAgenciaContaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBancoAgencia $bancoAgencia Object to remove from the list of results
     *
     * @return $this|ChildBancoAgenciaQuery The current query, for fluid interface
     */
    public function prune($bancoAgencia = null)
    {
        if ($bancoAgencia) {
            $this->addUsingAlias(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA, $bancoAgencia->getIdbancoAgencia(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the banco_agencia table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BancoAgenciaTableMap::clearInstancePool();
            BancoAgenciaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BancoAgenciaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BancoAgenciaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BancoAgenciaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BancoAgenciaQuery
