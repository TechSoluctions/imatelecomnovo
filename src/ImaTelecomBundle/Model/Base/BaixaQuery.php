<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Baixa as ChildBaixa;
use ImaTelecomBundle\Model\BaixaQuery as ChildBaixaQuery;
use ImaTelecomBundle\Model\Map\BaixaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'baixa' table.
 *
 *
 *
 * @method     ChildBaixaQuery orderByIdbaixa($order = Criteria::ASC) Order by the idbaixa column
 * @method     ChildBaixaQuery orderByContaCaixaId($order = Criteria::ASC) Order by the conta_caixa_id column
 * @method     ChildBaixaQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildBaixaQuery orderByUsuarioBaixa($order = Criteria::ASC) Order by the usuario_baixa column
 * @method     ChildBaixaQuery orderByDataBaixa($order = Criteria::ASC) Order by the data_baixa column
 * @method     ChildBaixaQuery orderByDataPagamento($order = Criteria::ASC) Order by the data_pagamento column
 * @method     ChildBaixaQuery orderByDescricaoMovimento($order = Criteria::ASC) Order by the descricao_movimento column
 * @method     ChildBaixaQuery orderByValorOriginal($order = Criteria::ASC) Order by the valor_original column
 * @method     ChildBaixaQuery orderByValorMulta($order = Criteria::ASC) Order by the valor_multa column
 * @method     ChildBaixaQuery orderByValorJuros($order = Criteria::ASC) Order by the valor_juros column
 * @method     ChildBaixaQuery orderByValorDesconto($order = Criteria::ASC) Order by the valor_desconto column
 * @method     ChildBaixaQuery orderByValorTotal($order = Criteria::ASC) Order by the valor_total column
 * @method     ChildBaixaQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildBaixaQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 *
 * @method     ChildBaixaQuery groupByIdbaixa() Group by the idbaixa column
 * @method     ChildBaixaQuery groupByContaCaixaId() Group by the conta_caixa_id column
 * @method     ChildBaixaQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildBaixaQuery groupByUsuarioBaixa() Group by the usuario_baixa column
 * @method     ChildBaixaQuery groupByDataBaixa() Group by the data_baixa column
 * @method     ChildBaixaQuery groupByDataPagamento() Group by the data_pagamento column
 * @method     ChildBaixaQuery groupByDescricaoMovimento() Group by the descricao_movimento column
 * @method     ChildBaixaQuery groupByValorOriginal() Group by the valor_original column
 * @method     ChildBaixaQuery groupByValorMulta() Group by the valor_multa column
 * @method     ChildBaixaQuery groupByValorJuros() Group by the valor_juros column
 * @method     ChildBaixaQuery groupByValorDesconto() Group by the valor_desconto column
 * @method     ChildBaixaQuery groupByValorTotal() Group by the valor_total column
 * @method     ChildBaixaQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildBaixaQuery groupByDataAlterado() Group by the data_alterado column
 *
 * @method     ChildBaixaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBaixaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBaixaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBaixaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBaixaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBaixaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBaixaQuery leftJoinCompetencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Competencia relation
 * @method     ChildBaixaQuery rightJoinCompetencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Competencia relation
 * @method     ChildBaixaQuery innerJoinCompetencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Competencia relation
 *
 * @method     ChildBaixaQuery joinWithCompetencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Competencia relation
 *
 * @method     ChildBaixaQuery leftJoinWithCompetencia() Adds a LEFT JOIN clause and with to the query using the Competencia relation
 * @method     ChildBaixaQuery rightJoinWithCompetencia() Adds a RIGHT JOIN clause and with to the query using the Competencia relation
 * @method     ChildBaixaQuery innerJoinWithCompetencia() Adds a INNER JOIN clause and with to the query using the Competencia relation
 *
 * @method     ChildBaixaQuery leftJoinContaCaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContaCaixa relation
 * @method     ChildBaixaQuery rightJoinContaCaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContaCaixa relation
 * @method     ChildBaixaQuery innerJoinContaCaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the ContaCaixa relation
 *
 * @method     ChildBaixaQuery joinWithContaCaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContaCaixa relation
 *
 * @method     ChildBaixaQuery leftJoinWithContaCaixa() Adds a LEFT JOIN clause and with to the query using the ContaCaixa relation
 * @method     ChildBaixaQuery rightJoinWithContaCaixa() Adds a RIGHT JOIN clause and with to the query using the ContaCaixa relation
 * @method     ChildBaixaQuery innerJoinWithContaCaixa() Adds a INNER JOIN clause and with to the query using the ContaCaixa relation
 *
 * @method     ChildBaixaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBaixaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBaixaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBaixaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBaixaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBaixaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBaixaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildBaixaQuery leftJoinBaixaEstorno($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildBaixaQuery rightJoinBaixaEstorno($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildBaixaQuery innerJoinBaixaEstorno($relationAlias = null) Adds a INNER JOIN clause to the query using the BaixaEstorno relation
 *
 * @method     ChildBaixaQuery joinWithBaixaEstorno($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildBaixaQuery leftJoinWithBaixaEstorno() Adds a LEFT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildBaixaQuery rightJoinWithBaixaEstorno() Adds a RIGHT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildBaixaQuery innerJoinWithBaixaEstorno() Adds a INNER JOIN clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildBaixaQuery leftJoinBaixaMovimento($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaixaMovimento relation
 * @method     ChildBaixaQuery rightJoinBaixaMovimento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaixaMovimento relation
 * @method     ChildBaixaQuery innerJoinBaixaMovimento($relationAlias = null) Adds a INNER JOIN clause to the query using the BaixaMovimento relation
 *
 * @method     ChildBaixaQuery joinWithBaixaMovimento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaixaMovimento relation
 *
 * @method     ChildBaixaQuery leftJoinWithBaixaMovimento() Adds a LEFT JOIN clause and with to the query using the BaixaMovimento relation
 * @method     ChildBaixaQuery rightJoinWithBaixaMovimento() Adds a RIGHT JOIN clause and with to the query using the BaixaMovimento relation
 * @method     ChildBaixaQuery innerJoinWithBaixaMovimento() Adds a INNER JOIN clause and with to the query using the BaixaMovimento relation
 *
 * @method     ChildBaixaQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildBaixaQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildBaixaQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildBaixaQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildBaixaQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildBaixaQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildBaixaQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildBaixaQuery leftJoinBoletoBaixaHistorico($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildBaixaQuery rightJoinBoletoBaixaHistorico($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildBaixaQuery innerJoinBoletoBaixaHistorico($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildBaixaQuery joinWithBoletoBaixaHistorico($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildBaixaQuery leftJoinWithBoletoBaixaHistorico() Adds a LEFT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildBaixaQuery rightJoinWithBoletoBaixaHistorico() Adds a RIGHT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildBaixaQuery innerJoinWithBoletoBaixaHistorico() Adds a INNER JOIN clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     \ImaTelecomBundle\Model\CompetenciaQuery|\ImaTelecomBundle\Model\ContaCaixaQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BaixaEstornoQuery|\ImaTelecomBundle\Model\BaixaMovimentoQuery|\ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBaixa findOne(ConnectionInterface $con = null) Return the first ChildBaixa matching the query
 * @method     ChildBaixa findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBaixa matching the query, or a new ChildBaixa object populated from the query conditions when no match is found
 *
 * @method     ChildBaixa findOneByIdbaixa(int $idbaixa) Return the first ChildBaixa filtered by the idbaixa column
 * @method     ChildBaixa findOneByContaCaixaId(int $conta_caixa_id) Return the first ChildBaixa filtered by the conta_caixa_id column
 * @method     ChildBaixa findOneByCompetenciaId(int $competencia_id) Return the first ChildBaixa filtered by the competencia_id column
 * @method     ChildBaixa findOneByUsuarioBaixa(int $usuario_baixa) Return the first ChildBaixa filtered by the usuario_baixa column
 * @method     ChildBaixa findOneByDataBaixa(string $data_baixa) Return the first ChildBaixa filtered by the data_baixa column
 * @method     ChildBaixa findOneByDataPagamento(string $data_pagamento) Return the first ChildBaixa filtered by the data_pagamento column
 * @method     ChildBaixa findOneByDescricaoMovimento(string $descricao_movimento) Return the first ChildBaixa filtered by the descricao_movimento column
 * @method     ChildBaixa findOneByValorOriginal(string $valor_original) Return the first ChildBaixa filtered by the valor_original column
 * @method     ChildBaixa findOneByValorMulta(string $valor_multa) Return the first ChildBaixa filtered by the valor_multa column
 * @method     ChildBaixa findOneByValorJuros(string $valor_juros) Return the first ChildBaixa filtered by the valor_juros column
 * @method     ChildBaixa findOneByValorDesconto(string $valor_desconto) Return the first ChildBaixa filtered by the valor_desconto column
 * @method     ChildBaixa findOneByValorTotal(string $valor_total) Return the first ChildBaixa filtered by the valor_total column
 * @method     ChildBaixa findOneByDataCadastro(string $data_cadastro) Return the first ChildBaixa filtered by the data_cadastro column
 * @method     ChildBaixa findOneByDataAlterado(string $data_alterado) Return the first ChildBaixa filtered by the data_alterado column *

 * @method     ChildBaixa requirePk($key, ConnectionInterface $con = null) Return the ChildBaixa by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOne(ConnectionInterface $con = null) Return the first ChildBaixa matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBaixa requireOneByIdbaixa(int $idbaixa) Return the first ChildBaixa filtered by the idbaixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByContaCaixaId(int $conta_caixa_id) Return the first ChildBaixa filtered by the conta_caixa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByCompetenciaId(int $competencia_id) Return the first ChildBaixa filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByUsuarioBaixa(int $usuario_baixa) Return the first ChildBaixa filtered by the usuario_baixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByDataBaixa(string $data_baixa) Return the first ChildBaixa filtered by the data_baixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByDataPagamento(string $data_pagamento) Return the first ChildBaixa filtered by the data_pagamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByDescricaoMovimento(string $descricao_movimento) Return the first ChildBaixa filtered by the descricao_movimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByValorOriginal(string $valor_original) Return the first ChildBaixa filtered by the valor_original column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByValorMulta(string $valor_multa) Return the first ChildBaixa filtered by the valor_multa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByValorJuros(string $valor_juros) Return the first ChildBaixa filtered by the valor_juros column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByValorDesconto(string $valor_desconto) Return the first ChildBaixa filtered by the valor_desconto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByValorTotal(string $valor_total) Return the first ChildBaixa filtered by the valor_total column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByDataCadastro(string $data_cadastro) Return the first ChildBaixa filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixa requireOneByDataAlterado(string $data_alterado) Return the first ChildBaixa filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBaixa[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBaixa objects based on current ModelCriteria
 * @method     ChildBaixa[]|ObjectCollection findByIdbaixa(int $idbaixa) Return ChildBaixa objects filtered by the idbaixa column
 * @method     ChildBaixa[]|ObjectCollection findByContaCaixaId(int $conta_caixa_id) Return ChildBaixa objects filtered by the conta_caixa_id column
 * @method     ChildBaixa[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildBaixa objects filtered by the competencia_id column
 * @method     ChildBaixa[]|ObjectCollection findByUsuarioBaixa(int $usuario_baixa) Return ChildBaixa objects filtered by the usuario_baixa column
 * @method     ChildBaixa[]|ObjectCollection findByDataBaixa(string $data_baixa) Return ChildBaixa objects filtered by the data_baixa column
 * @method     ChildBaixa[]|ObjectCollection findByDataPagamento(string $data_pagamento) Return ChildBaixa objects filtered by the data_pagamento column
 * @method     ChildBaixa[]|ObjectCollection findByDescricaoMovimento(string $descricao_movimento) Return ChildBaixa objects filtered by the descricao_movimento column
 * @method     ChildBaixa[]|ObjectCollection findByValorOriginal(string $valor_original) Return ChildBaixa objects filtered by the valor_original column
 * @method     ChildBaixa[]|ObjectCollection findByValorMulta(string $valor_multa) Return ChildBaixa objects filtered by the valor_multa column
 * @method     ChildBaixa[]|ObjectCollection findByValorJuros(string $valor_juros) Return ChildBaixa objects filtered by the valor_juros column
 * @method     ChildBaixa[]|ObjectCollection findByValorDesconto(string $valor_desconto) Return ChildBaixa objects filtered by the valor_desconto column
 * @method     ChildBaixa[]|ObjectCollection findByValorTotal(string $valor_total) Return ChildBaixa objects filtered by the valor_total column
 * @method     ChildBaixa[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildBaixa objects filtered by the data_cadastro column
 * @method     ChildBaixa[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildBaixa objects filtered by the data_alterado column
 * @method     ChildBaixa[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BaixaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\BaixaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Baixa', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBaixaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBaixaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBaixaQuery) {
            return $criteria;
        }
        $query = new ChildBaixaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBaixa|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BaixaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BaixaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixa A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idbaixa, conta_caixa_id, competencia_id, usuario_baixa, data_baixa, data_pagamento, descricao_movimento, valor_original, valor_multa, valor_juros, valor_desconto, valor_total, data_cadastro, data_alterado FROM baixa WHERE idbaixa = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBaixa $obj */
            $obj = new ChildBaixa();
            $obj->hydrate($row);
            BaixaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBaixa|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idbaixa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdbaixa(1234); // WHERE idbaixa = 1234
     * $query->filterByIdbaixa(array(12, 34)); // WHERE idbaixa IN (12, 34)
     * $query->filterByIdbaixa(array('min' => 12)); // WHERE idbaixa > 12
     * </code>
     *
     * @param     mixed $idbaixa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByIdbaixa($idbaixa = null, $comparison = null)
    {
        if (is_array($idbaixa)) {
            $useMinMax = false;
            if (isset($idbaixa['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $idbaixa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idbaixa['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $idbaixa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $idbaixa, $comparison);
    }

    /**
     * Filter the query on the conta_caixa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContaCaixaId(1234); // WHERE conta_caixa_id = 1234
     * $query->filterByContaCaixaId(array(12, 34)); // WHERE conta_caixa_id IN (12, 34)
     * $query->filterByContaCaixaId(array('min' => 12)); // WHERE conta_caixa_id > 12
     * </code>
     *
     * @see       filterByContaCaixa()
     *
     * @param     mixed $contaCaixaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByContaCaixaId($contaCaixaId = null, $comparison = null)
    {
        if (is_array($contaCaixaId)) {
            $useMinMax = false;
            if (isset($contaCaixaId['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_CONTA_CAIXA_ID, $contaCaixaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contaCaixaId['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_CONTA_CAIXA_ID, $contaCaixaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_CONTA_CAIXA_ID, $contaCaixaId, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @see       filterByCompetencia()
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the usuario_baixa column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioBaixa(1234); // WHERE usuario_baixa = 1234
     * $query->filterByUsuarioBaixa(array(12, 34)); // WHERE usuario_baixa IN (12, 34)
     * $query->filterByUsuarioBaixa(array('min' => 12)); // WHERE usuario_baixa > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioBaixa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByUsuarioBaixa($usuarioBaixa = null, $comparison = null)
    {
        if (is_array($usuarioBaixa)) {
            $useMinMax = false;
            if (isset($usuarioBaixa['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_USUARIO_BAIXA, $usuarioBaixa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioBaixa['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_USUARIO_BAIXA, $usuarioBaixa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_USUARIO_BAIXA, $usuarioBaixa, $comparison);
    }

    /**
     * Filter the query on the data_baixa column
     *
     * Example usage:
     * <code>
     * $query->filterByDataBaixa('2011-03-14'); // WHERE data_baixa = '2011-03-14'
     * $query->filterByDataBaixa('now'); // WHERE data_baixa = '2011-03-14'
     * $query->filterByDataBaixa(array('max' => 'yesterday')); // WHERE data_baixa > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataBaixa The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByDataBaixa($dataBaixa = null, $comparison = null)
    {
        if (is_array($dataBaixa)) {
            $useMinMax = false;
            if (isset($dataBaixa['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_DATA_BAIXA, $dataBaixa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataBaixa['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_DATA_BAIXA, $dataBaixa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_DATA_BAIXA, $dataBaixa, $comparison);
    }

    /**
     * Filter the query on the data_pagamento column
     *
     * Example usage:
     * <code>
     * $query->filterByDataPagamento('2011-03-14'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByDataPagamento('now'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByDataPagamento(array('max' => 'yesterday')); // WHERE data_pagamento > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataPagamento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByDataPagamento($dataPagamento = null, $comparison = null)
    {
        if (is_array($dataPagamento)) {
            $useMinMax = false;
            if (isset($dataPagamento['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_DATA_PAGAMENTO, $dataPagamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataPagamento['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_DATA_PAGAMENTO, $dataPagamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_DATA_PAGAMENTO, $dataPagamento, $comparison);
    }

    /**
     * Filter the query on the descricao_movimento column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricaoMovimento('fooValue');   // WHERE descricao_movimento = 'fooValue'
     * $query->filterByDescricaoMovimento('%fooValue%', Criteria::LIKE); // WHERE descricao_movimento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricaoMovimento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByDescricaoMovimento($descricaoMovimento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricaoMovimento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_DESCRICAO_MOVIMENTO, $descricaoMovimento, $comparison);
    }

    /**
     * Filter the query on the valor_original column
     *
     * Example usage:
     * <code>
     * $query->filterByValorOriginal(1234); // WHERE valor_original = 1234
     * $query->filterByValorOriginal(array(12, 34)); // WHERE valor_original IN (12, 34)
     * $query->filterByValorOriginal(array('min' => 12)); // WHERE valor_original > 12
     * </code>
     *
     * @param     mixed $valorOriginal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByValorOriginal($valorOriginal = null, $comparison = null)
    {
        if (is_array($valorOriginal)) {
            $useMinMax = false;
            if (isset($valorOriginal['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_ORIGINAL, $valorOriginal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorOriginal['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_ORIGINAL, $valorOriginal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_VALOR_ORIGINAL, $valorOriginal, $comparison);
    }

    /**
     * Filter the query on the valor_multa column
     *
     * Example usage:
     * <code>
     * $query->filterByValorMulta(1234); // WHERE valor_multa = 1234
     * $query->filterByValorMulta(array(12, 34)); // WHERE valor_multa IN (12, 34)
     * $query->filterByValorMulta(array('min' => 12)); // WHERE valor_multa > 12
     * </code>
     *
     * @param     mixed $valorMulta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByValorMulta($valorMulta = null, $comparison = null)
    {
        if (is_array($valorMulta)) {
            $useMinMax = false;
            if (isset($valorMulta['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_MULTA, $valorMulta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorMulta['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_MULTA, $valorMulta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_VALOR_MULTA, $valorMulta, $comparison);
    }

    /**
     * Filter the query on the valor_juros column
     *
     * Example usage:
     * <code>
     * $query->filterByValorJuros(1234); // WHERE valor_juros = 1234
     * $query->filterByValorJuros(array(12, 34)); // WHERE valor_juros IN (12, 34)
     * $query->filterByValorJuros(array('min' => 12)); // WHERE valor_juros > 12
     * </code>
     *
     * @param     mixed $valorJuros The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByValorJuros($valorJuros = null, $comparison = null)
    {
        if (is_array($valorJuros)) {
            $useMinMax = false;
            if (isset($valorJuros['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_JUROS, $valorJuros['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorJuros['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_JUROS, $valorJuros['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_VALOR_JUROS, $valorJuros, $comparison);
    }

    /**
     * Filter the query on the valor_desconto column
     *
     * Example usage:
     * <code>
     * $query->filterByValorDesconto(1234); // WHERE valor_desconto = 1234
     * $query->filterByValorDesconto(array(12, 34)); // WHERE valor_desconto IN (12, 34)
     * $query->filterByValorDesconto(array('min' => 12)); // WHERE valor_desconto > 12
     * </code>
     *
     * @param     mixed $valorDesconto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByValorDesconto($valorDesconto = null, $comparison = null)
    {
        if (is_array($valorDesconto)) {
            $useMinMax = false;
            if (isset($valorDesconto['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_DESCONTO, $valorDesconto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorDesconto['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_DESCONTO, $valorDesconto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_VALOR_DESCONTO, $valorDesconto, $comparison);
    }

    /**
     * Filter the query on the valor_total column
     *
     * Example usage:
     * <code>
     * $query->filterByValorTotal(1234); // WHERE valor_total = 1234
     * $query->filterByValorTotal(array(12, 34)); // WHERE valor_total IN (12, 34)
     * $query->filterByValorTotal(array('min' => 12)); // WHERE valor_total > 12
     * </code>
     *
     * @param     mixed $valorTotal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByValorTotal($valorTotal = null, $comparison = null)
    {
        if (is_array($valorTotal)) {
            $useMinMax = false;
            if (isset($valorTotal['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_TOTAL, $valorTotal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorTotal['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_VALOR_TOTAL, $valorTotal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_VALOR_TOTAL, $valorTotal, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(BaixaTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(BaixaTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Competencia object
     *
     * @param \ImaTelecomBundle\Model\Competencia|ObjectCollection $competencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByCompetencia($competencia, $comparison = null)
    {
        if ($competencia instanceof \ImaTelecomBundle\Model\Competencia) {
            return $this
                ->addUsingAlias(BaixaTableMap::COL_COMPETENCIA_ID, $competencia->getId(), $comparison);
        } elseif ($competencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaTableMap::COL_COMPETENCIA_ID, $competencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompetencia() only accepts arguments of type \ImaTelecomBundle\Model\Competencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Competencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function joinCompetencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Competencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Competencia');
        }

        return $this;
    }

    /**
     * Use the Competencia relation Competencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CompetenciaQuery A secondary query class using the current class as primary query
     */
    public function useCompetenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompetencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Competencia', '\ImaTelecomBundle\Model\CompetenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContaCaixa object
     *
     * @param \ImaTelecomBundle\Model\ContaCaixa|ObjectCollection $contaCaixa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByContaCaixa($contaCaixa, $comparison = null)
    {
        if ($contaCaixa instanceof \ImaTelecomBundle\Model\ContaCaixa) {
            return $this
                ->addUsingAlias(BaixaTableMap::COL_CONTA_CAIXA_ID, $contaCaixa->getIdcontaCaixa(), $comparison);
        } elseif ($contaCaixa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaTableMap::COL_CONTA_CAIXA_ID, $contaCaixa->toKeyValue('PrimaryKey', 'IdcontaCaixa'), $comparison);
        } else {
            throw new PropelException('filterByContaCaixa() only accepts arguments of type \ImaTelecomBundle\Model\ContaCaixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContaCaixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function joinContaCaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContaCaixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContaCaixa');
        }

        return $this;
    }

    /**
     * Use the ContaCaixa relation ContaCaixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContaCaixaQuery A secondary query class using the current class as primary query
     */
    public function useContaCaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContaCaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContaCaixa', '\ImaTelecomBundle\Model\ContaCaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(BaixaTableMap::COL_USUARIO_BAIXA, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaTableMap::COL_USUARIO_BAIXA, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BaixaEstorno object
     *
     * @param \ImaTelecomBundle\Model\BaixaEstorno|ObjectCollection $baixaEstorno the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByBaixaEstorno($baixaEstorno, $comparison = null)
    {
        if ($baixaEstorno instanceof \ImaTelecomBundle\Model\BaixaEstorno) {
            return $this
                ->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $baixaEstorno->getBaixaId(), $comparison);
        } elseif ($baixaEstorno instanceof ObjectCollection) {
            return $this
                ->useBaixaEstornoQuery()
                ->filterByPrimaryKeys($baixaEstorno->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixaEstorno() only accepts arguments of type \ImaTelecomBundle\Model\BaixaEstorno or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaixaEstorno relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function joinBaixaEstorno($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaixaEstorno');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaixaEstorno');
        }

        return $this;
    }

    /**
     * Use the BaixaEstorno relation BaixaEstorno object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaEstornoQuery A secondary query class using the current class as primary query
     */
    public function useBaixaEstornoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixaEstorno($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaixaEstorno', '\ImaTelecomBundle\Model\BaixaEstornoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BaixaMovimento object
     *
     * @param \ImaTelecomBundle\Model\BaixaMovimento|ObjectCollection $baixaMovimento the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByBaixaMovimento($baixaMovimento, $comparison = null)
    {
        if ($baixaMovimento instanceof \ImaTelecomBundle\Model\BaixaMovimento) {
            return $this
                ->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $baixaMovimento->getBaixaId(), $comparison);
        } elseif ($baixaMovimento instanceof ObjectCollection) {
            return $this
                ->useBaixaMovimentoQuery()
                ->filterByPrimaryKeys($baixaMovimento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixaMovimento() only accepts arguments of type \ImaTelecomBundle\Model\BaixaMovimento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaixaMovimento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function joinBaixaMovimento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaixaMovimento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaixaMovimento');
        }

        return $this;
    }

    /**
     * Use the BaixaMovimento relation BaixaMovimento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaMovimentoQuery A secondary query class using the current class as primary query
     */
    public function useBaixaMovimentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixaMovimento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaixaMovimento', '\ImaTelecomBundle\Model\BaixaMovimentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $boleto->getBaixaId(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            return $this
                ->useBoletoQuery()
                ->filterByPrimaryKeys($boleto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoBaixaHistorico object
     *
     * @param \ImaTelecomBundle\Model\BoletoBaixaHistorico|ObjectCollection $boletoBaixaHistorico the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBaixaQuery The current query, for fluid interface
     */
    public function filterByBoletoBaixaHistorico($boletoBaixaHistorico, $comparison = null)
    {
        if ($boletoBaixaHistorico instanceof \ImaTelecomBundle\Model\BoletoBaixaHistorico) {
            return $this
                ->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $boletoBaixaHistorico->getBaixaId(), $comparison);
        } elseif ($boletoBaixaHistorico instanceof ObjectCollection) {
            return $this
                ->useBoletoBaixaHistoricoQuery()
                ->filterByPrimaryKeys($boletoBaixaHistorico->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoBaixaHistorico() only accepts arguments of type \ImaTelecomBundle\Model\BoletoBaixaHistorico or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoBaixaHistorico relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function joinBoletoBaixaHistorico($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoBaixaHistorico');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoBaixaHistorico');
        }

        return $this;
    }

    /**
     * Use the BoletoBaixaHistorico relation BoletoBaixaHistorico object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoBaixaHistoricoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBoletoBaixaHistorico($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoBaixaHistorico', '\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBaixa $baixa Object to remove from the list of results
     *
     * @return $this|ChildBaixaQuery The current query, for fluid interface
     */
    public function prune($baixa = null)
    {
        if ($baixa) {
            $this->addUsingAlias(BaixaTableMap::COL_IDBAIXA, $baixa->getIdbaixa(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the baixa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BaixaTableMap::clearInstancePool();
            BaixaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BaixaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BaixaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BaixaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BaixaQuery
