<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Cidade as ChildCidade;
use ImaTelecomBundle\Model\CidadeQuery as ChildCidadeQuery;
use ImaTelecomBundle\Model\Map\CidadeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'cidade' table.
 *
 *
 *
 * @method     ChildCidadeQuery orderByIdcidade($order = Criteria::ASC) Order by the idcidade column
 * @method     ChildCidadeQuery orderByUf($order = Criteria::ASC) Order by the uf column
 * @method     ChildCidadeQuery orderByMunicipio($order = Criteria::ASC) Order by the municipio column
 * @method     ChildCidadeQuery orderByCodigo($order = Criteria::ASC) Order by the codigo column
 *
 * @method     ChildCidadeQuery groupByIdcidade() Group by the idcidade column
 * @method     ChildCidadeQuery groupByUf() Group by the uf column
 * @method     ChildCidadeQuery groupByMunicipio() Group by the municipio column
 * @method     ChildCidadeQuery groupByCodigo() Group by the codigo column
 *
 * @method     ChildCidadeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCidadeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCidadeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCidadeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCidadeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCidadeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCidadeQuery leftJoinEnderecoAgencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoAgencia relation
 * @method     ChildCidadeQuery rightJoinEnderecoAgencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoAgencia relation
 * @method     ChildCidadeQuery innerJoinEnderecoAgencia($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoAgencia relation
 *
 * @method     ChildCidadeQuery joinWithEnderecoAgencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoAgencia relation
 *
 * @method     ChildCidadeQuery leftJoinWithEnderecoAgencia() Adds a LEFT JOIN clause and with to the query using the EnderecoAgencia relation
 * @method     ChildCidadeQuery rightJoinWithEnderecoAgencia() Adds a RIGHT JOIN clause and with to the query using the EnderecoAgencia relation
 * @method     ChildCidadeQuery innerJoinWithEnderecoAgencia() Adds a INNER JOIN clause and with to the query using the EnderecoAgencia relation
 *
 * @method     ChildCidadeQuery leftJoinEnderecoCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoCliente relation
 * @method     ChildCidadeQuery rightJoinEnderecoCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoCliente relation
 * @method     ChildCidadeQuery innerJoinEnderecoCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoCliente relation
 *
 * @method     ChildCidadeQuery joinWithEnderecoCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoCliente relation
 *
 * @method     ChildCidadeQuery leftJoinWithEnderecoCliente() Adds a LEFT JOIN clause and with to the query using the EnderecoCliente relation
 * @method     ChildCidadeQuery rightJoinWithEnderecoCliente() Adds a RIGHT JOIN clause and with to the query using the EnderecoCliente relation
 * @method     ChildCidadeQuery innerJoinWithEnderecoCliente() Adds a INNER JOIN clause and with to the query using the EnderecoCliente relation
 *
 * @method     ChildCidadeQuery leftJoinEnderecoServCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoServCliente relation
 * @method     ChildCidadeQuery rightJoinEnderecoServCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoServCliente relation
 * @method     ChildCidadeQuery innerJoinEnderecoServCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoServCliente relation
 *
 * @method     ChildCidadeQuery joinWithEnderecoServCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoServCliente relation
 *
 * @method     ChildCidadeQuery leftJoinWithEnderecoServCliente() Adds a LEFT JOIN clause and with to the query using the EnderecoServCliente relation
 * @method     ChildCidadeQuery rightJoinWithEnderecoServCliente() Adds a RIGHT JOIN clause and with to the query using the EnderecoServCliente relation
 * @method     ChildCidadeQuery innerJoinWithEnderecoServCliente() Adds a INNER JOIN clause and with to the query using the EnderecoServCliente relation
 *
 * @method     \ImaTelecomBundle\Model\EnderecoAgenciaQuery|\ImaTelecomBundle\Model\EnderecoClienteQuery|\ImaTelecomBundle\Model\EnderecoServClienteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCidade findOne(ConnectionInterface $con = null) Return the first ChildCidade matching the query
 * @method     ChildCidade findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCidade matching the query, or a new ChildCidade object populated from the query conditions when no match is found
 *
 * @method     ChildCidade findOneByIdcidade(int $idcidade) Return the first ChildCidade filtered by the idcidade column
 * @method     ChildCidade findOneByUf(string $uf) Return the first ChildCidade filtered by the uf column
 * @method     ChildCidade findOneByMunicipio(string $municipio) Return the first ChildCidade filtered by the municipio column
 * @method     ChildCidade findOneByCodigo(string $codigo) Return the first ChildCidade filtered by the codigo column *

 * @method     ChildCidade requirePk($key, ConnectionInterface $con = null) Return the ChildCidade by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCidade requireOne(ConnectionInterface $con = null) Return the first ChildCidade matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCidade requireOneByIdcidade(int $idcidade) Return the first ChildCidade filtered by the idcidade column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCidade requireOneByUf(string $uf) Return the first ChildCidade filtered by the uf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCidade requireOneByMunicipio(string $municipio) Return the first ChildCidade filtered by the municipio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCidade requireOneByCodigo(string $codigo) Return the first ChildCidade filtered by the codigo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCidade[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCidade objects based on current ModelCriteria
 * @method     ChildCidade[]|ObjectCollection findByIdcidade(int $idcidade) Return ChildCidade objects filtered by the idcidade column
 * @method     ChildCidade[]|ObjectCollection findByUf(string $uf) Return ChildCidade objects filtered by the uf column
 * @method     ChildCidade[]|ObjectCollection findByMunicipio(string $municipio) Return ChildCidade objects filtered by the municipio column
 * @method     ChildCidade[]|ObjectCollection findByCodigo(string $codigo) Return ChildCidade objects filtered by the codigo column
 * @method     ChildCidade[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CidadeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\CidadeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Cidade', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCidadeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCidadeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCidadeQuery) {
            return $criteria;
        }
        $query = new ChildCidadeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCidade|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CidadeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CidadeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCidade A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcidade, uf, municipio, codigo FROM cidade WHERE idcidade = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCidade $obj */
            $obj = new ChildCidade();
            $obj->hydrate($row);
            CidadeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCidade|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CidadeTableMap::COL_IDCIDADE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CidadeTableMap::COL_IDCIDADE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcidade column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcidade(1234); // WHERE idcidade = 1234
     * $query->filterByIdcidade(array(12, 34)); // WHERE idcidade IN (12, 34)
     * $query->filterByIdcidade(array('min' => 12)); // WHERE idcidade > 12
     * </code>
     *
     * @param     mixed $idcidade The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function filterByIdcidade($idcidade = null, $comparison = null)
    {
        if (is_array($idcidade)) {
            $useMinMax = false;
            if (isset($idcidade['min'])) {
                $this->addUsingAlias(CidadeTableMap::COL_IDCIDADE, $idcidade['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcidade['max'])) {
                $this->addUsingAlias(CidadeTableMap::COL_IDCIDADE, $idcidade['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CidadeTableMap::COL_IDCIDADE, $idcidade, $comparison);
    }

    /**
     * Filter the query on the uf column
     *
     * Example usage:
     * <code>
     * $query->filterByUf('fooValue');   // WHERE uf = 'fooValue'
     * $query->filterByUf('%fooValue%', Criteria::LIKE); // WHERE uf LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uf The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function filterByUf($uf = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uf)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CidadeTableMap::COL_UF, $uf, $comparison);
    }

    /**
     * Filter the query on the municipio column
     *
     * Example usage:
     * <code>
     * $query->filterByMunicipio('fooValue');   // WHERE municipio = 'fooValue'
     * $query->filterByMunicipio('%fooValue%', Criteria::LIKE); // WHERE municipio LIKE '%fooValue%'
     * </code>
     *
     * @param     string $municipio The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function filterByMunicipio($municipio = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($municipio)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CidadeTableMap::COL_MUNICIPIO, $municipio, $comparison);
    }

    /**
     * Filter the query on the codigo column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigo('fooValue');   // WHERE codigo = 'fooValue'
     * $query->filterByCodigo('%fooValue%', Criteria::LIKE); // WHERE codigo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function filterByCodigo($codigo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CidadeTableMap::COL_CODIGO, $codigo, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoAgencia object
     *
     * @param \ImaTelecomBundle\Model\EnderecoAgencia|ObjectCollection $enderecoAgencia the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCidadeQuery The current query, for fluid interface
     */
    public function filterByEnderecoAgencia($enderecoAgencia, $comparison = null)
    {
        if ($enderecoAgencia instanceof \ImaTelecomBundle\Model\EnderecoAgencia) {
            return $this
                ->addUsingAlias(CidadeTableMap::COL_IDCIDADE, $enderecoAgencia->getCidadeId(), $comparison);
        } elseif ($enderecoAgencia instanceof ObjectCollection) {
            return $this
                ->useEnderecoAgenciaQuery()
                ->filterByPrimaryKeys($enderecoAgencia->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEnderecoAgencia() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoAgencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoAgencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function joinEnderecoAgencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoAgencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoAgencia');
        }

        return $this;
    }

    /**
     * Use the EnderecoAgencia relation EnderecoAgencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoAgenciaQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoAgenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEnderecoAgencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoAgencia', '\ImaTelecomBundle\Model\EnderecoAgenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoCliente object
     *
     * @param \ImaTelecomBundle\Model\EnderecoCliente|ObjectCollection $enderecoCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCidadeQuery The current query, for fluid interface
     */
    public function filterByEnderecoCliente($enderecoCliente, $comparison = null)
    {
        if ($enderecoCliente instanceof \ImaTelecomBundle\Model\EnderecoCliente) {
            return $this
                ->addUsingAlias(CidadeTableMap::COL_IDCIDADE, $enderecoCliente->getCidadeId(), $comparison);
        } elseif ($enderecoCliente instanceof ObjectCollection) {
            return $this
                ->useEnderecoClienteQuery()
                ->filterByPrimaryKeys($enderecoCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEnderecoCliente() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function joinEnderecoCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoCliente');
        }

        return $this;
    }

    /**
     * Use the EnderecoCliente relation EnderecoCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoClienteQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEnderecoCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoCliente', '\ImaTelecomBundle\Model\EnderecoClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoServCliente object
     *
     * @param \ImaTelecomBundle\Model\EnderecoServCliente|ObjectCollection $enderecoServCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCidadeQuery The current query, for fluid interface
     */
    public function filterByEnderecoServCliente($enderecoServCliente, $comparison = null)
    {
        if ($enderecoServCliente instanceof \ImaTelecomBundle\Model\EnderecoServCliente) {
            return $this
                ->addUsingAlias(CidadeTableMap::COL_IDCIDADE, $enderecoServCliente->getCidadeId(), $comparison);
        } elseif ($enderecoServCliente instanceof ObjectCollection) {
            return $this
                ->useEnderecoServClienteQuery()
                ->filterByPrimaryKeys($enderecoServCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEnderecoServCliente() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoServCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoServCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function joinEnderecoServCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoServCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoServCliente');
        }

        return $this;
    }

    /**
     * Use the EnderecoServCliente relation EnderecoServCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoServClienteQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoServClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEnderecoServCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoServCliente', '\ImaTelecomBundle\Model\EnderecoServClienteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCidade $cidade Object to remove from the list of results
     *
     * @return $this|ChildCidadeQuery The current query, for fluid interface
     */
    public function prune($cidade = null)
    {
        if ($cidade) {
            $this->addUsingAlias(CidadeTableMap::COL_IDCIDADE, $cidade->getIdcidade(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the cidade table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CidadeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CidadeTableMap::clearInstancePool();
            CidadeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CidadeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CidadeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CidadeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CidadeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CidadeQuery
