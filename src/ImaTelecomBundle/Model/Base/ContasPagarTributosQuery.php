<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ContasPagarTributos as ChildContasPagarTributos;
use ImaTelecomBundle\Model\ContasPagarTributosQuery as ChildContasPagarTributosQuery;
use ImaTelecomBundle\Model\Map\ContasPagarTributosTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'contas_pagar_tributos' table.
 *
 *
 *
 * @method     ChildContasPagarTributosQuery orderByIdcontasPagarTributos($order = Criteria::ASC) Order by the idcontas_pagar_tributos column
 * @method     ChildContasPagarTributosQuery orderByTributoId($order = Criteria::ASC) Order by the tributo_id column
 * @method     ChildContasPagarTributosQuery orderByPercentual($order = Criteria::ASC) Order by the percentual column
 * @method     ChildContasPagarTributosQuery orderByValor($order = Criteria::ASC) Order by the valor column
 * @method     ChildContasPagarTributosQuery orderByContaPagarId($order = Criteria::ASC) Order by the conta_pagar_id column
 * @method     ChildContasPagarTributosQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildContasPagarTributosQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildContasPagarTributosQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildContasPagarTributosQuery groupByIdcontasPagarTributos() Group by the idcontas_pagar_tributos column
 * @method     ChildContasPagarTributosQuery groupByTributoId() Group by the tributo_id column
 * @method     ChildContasPagarTributosQuery groupByPercentual() Group by the percentual column
 * @method     ChildContasPagarTributosQuery groupByValor() Group by the valor column
 * @method     ChildContasPagarTributosQuery groupByContaPagarId() Group by the conta_pagar_id column
 * @method     ChildContasPagarTributosQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildContasPagarTributosQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildContasPagarTributosQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildContasPagarTributosQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildContasPagarTributosQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildContasPagarTributosQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildContasPagarTributosQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildContasPagarTributosQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildContasPagarTributosQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildContasPagarTributosQuery leftJoinContasPagar($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagar relation
 * @method     ChildContasPagarTributosQuery rightJoinContasPagar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagar relation
 * @method     ChildContasPagarTributosQuery innerJoinContasPagar($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagar relation
 *
 * @method     ChildContasPagarTributosQuery joinWithContasPagar($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagar relation
 *
 * @method     ChildContasPagarTributosQuery leftJoinWithContasPagar() Adds a LEFT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildContasPagarTributosQuery rightJoinWithContasPagar() Adds a RIGHT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildContasPagarTributosQuery innerJoinWithContasPagar() Adds a INNER JOIN clause and with to the query using the ContasPagar relation
 *
 * @method     ChildContasPagarTributosQuery leftJoinTributo($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tributo relation
 * @method     ChildContasPagarTributosQuery rightJoinTributo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tributo relation
 * @method     ChildContasPagarTributosQuery innerJoinTributo($relationAlias = null) Adds a INNER JOIN clause to the query using the Tributo relation
 *
 * @method     ChildContasPagarTributosQuery joinWithTributo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tributo relation
 *
 * @method     ChildContasPagarTributosQuery leftJoinWithTributo() Adds a LEFT JOIN clause and with to the query using the Tributo relation
 * @method     ChildContasPagarTributosQuery rightJoinWithTributo() Adds a RIGHT JOIN clause and with to the query using the Tributo relation
 * @method     ChildContasPagarTributosQuery innerJoinWithTributo() Adds a INNER JOIN clause and with to the query using the Tributo relation
 *
 * @method     ChildContasPagarTributosQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildContasPagarTributosQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildContasPagarTributosQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildContasPagarTributosQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildContasPagarTributosQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildContasPagarTributosQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildContasPagarTributosQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \ImaTelecomBundle\Model\ContasPagarQuery|\ImaTelecomBundle\Model\TributoQuery|\ImaTelecomBundle\Model\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildContasPagarTributos findOne(ConnectionInterface $con = null) Return the first ChildContasPagarTributos matching the query
 * @method     ChildContasPagarTributos findOneOrCreate(ConnectionInterface $con = null) Return the first ChildContasPagarTributos matching the query, or a new ChildContasPagarTributos object populated from the query conditions when no match is found
 *
 * @method     ChildContasPagarTributos findOneByIdcontasPagarTributos(int $idcontas_pagar_tributos) Return the first ChildContasPagarTributos filtered by the idcontas_pagar_tributos column
 * @method     ChildContasPagarTributos findOneByTributoId(int $tributo_id) Return the first ChildContasPagarTributos filtered by the tributo_id column
 * @method     ChildContasPagarTributos findOneByPercentual(string $percentual) Return the first ChildContasPagarTributos filtered by the percentual column
 * @method     ChildContasPagarTributos findOneByValor(string $valor) Return the first ChildContasPagarTributos filtered by the valor column
 * @method     ChildContasPagarTributos findOneByContaPagarId(int $conta_pagar_id) Return the first ChildContasPagarTributos filtered by the conta_pagar_id column
 * @method     ChildContasPagarTributos findOneByDataCadastro(string $data_cadastro) Return the first ChildContasPagarTributos filtered by the data_cadastro column
 * @method     ChildContasPagarTributos findOneByDataAlterado(string $data_alterado) Return the first ChildContasPagarTributos filtered by the data_alterado column
 * @method     ChildContasPagarTributos findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildContasPagarTributos filtered by the usuario_alterado column *

 * @method     ChildContasPagarTributos requirePk($key, ConnectionInterface $con = null) Return the ChildContasPagarTributos by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarTributos requireOne(ConnectionInterface $con = null) Return the first ChildContasPagarTributos matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContasPagarTributos requireOneByIdcontasPagarTributos(int $idcontas_pagar_tributos) Return the first ChildContasPagarTributos filtered by the idcontas_pagar_tributos column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarTributos requireOneByTributoId(int $tributo_id) Return the first ChildContasPagarTributos filtered by the tributo_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarTributos requireOneByPercentual(string $percentual) Return the first ChildContasPagarTributos filtered by the percentual column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarTributos requireOneByValor(string $valor) Return the first ChildContasPagarTributos filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarTributos requireOneByContaPagarId(int $conta_pagar_id) Return the first ChildContasPagarTributos filtered by the conta_pagar_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarTributos requireOneByDataCadastro(string $data_cadastro) Return the first ChildContasPagarTributos filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarTributos requireOneByDataAlterado(string $data_alterado) Return the first ChildContasPagarTributos filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarTributos requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildContasPagarTributos filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContasPagarTributos[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildContasPagarTributos objects based on current ModelCriteria
 * @method     ChildContasPagarTributos[]|ObjectCollection findByIdcontasPagarTributos(int $idcontas_pagar_tributos) Return ChildContasPagarTributos objects filtered by the idcontas_pagar_tributos column
 * @method     ChildContasPagarTributos[]|ObjectCollection findByTributoId(int $tributo_id) Return ChildContasPagarTributos objects filtered by the tributo_id column
 * @method     ChildContasPagarTributos[]|ObjectCollection findByPercentual(string $percentual) Return ChildContasPagarTributos objects filtered by the percentual column
 * @method     ChildContasPagarTributos[]|ObjectCollection findByValor(string $valor) Return ChildContasPagarTributos objects filtered by the valor column
 * @method     ChildContasPagarTributos[]|ObjectCollection findByContaPagarId(int $conta_pagar_id) Return ChildContasPagarTributos objects filtered by the conta_pagar_id column
 * @method     ChildContasPagarTributos[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildContasPagarTributos objects filtered by the data_cadastro column
 * @method     ChildContasPagarTributos[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildContasPagarTributos objects filtered by the data_alterado column
 * @method     ChildContasPagarTributos[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildContasPagarTributos objects filtered by the usuario_alterado column
 * @method     ChildContasPagarTributos[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ContasPagarTributosQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ContasPagarTributosQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ContasPagarTributos', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildContasPagarTributosQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildContasPagarTributosQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildContasPagarTributosQuery) {
            return $criteria;
        }
        $query = new ChildContasPagarTributosQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildContasPagarTributos|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContasPagarTributosTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ContasPagarTributosTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagarTributos A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcontas_pagar_tributos, tributo_id, percentual, valor, conta_pagar_id, data_cadastro, data_alterado, usuario_alterado FROM contas_pagar_tributos WHERE idcontas_pagar_tributos = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildContasPagarTributos $obj */
            $obj = new ChildContasPagarTributos();
            $obj->hydrate($row);
            ContasPagarTributosTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildContasPagarTributos|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcontas_pagar_tributos column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcontasPagarTributos(1234); // WHERE idcontas_pagar_tributos = 1234
     * $query->filterByIdcontasPagarTributos(array(12, 34)); // WHERE idcontas_pagar_tributos IN (12, 34)
     * $query->filterByIdcontasPagarTributos(array('min' => 12)); // WHERE idcontas_pagar_tributos > 12
     * </code>
     *
     * @param     mixed $idcontasPagarTributos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByIdcontasPagarTributos($idcontasPagarTributos = null, $comparison = null)
    {
        if (is_array($idcontasPagarTributos)) {
            $useMinMax = false;
            if (isset($idcontasPagarTributos['min'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS, $idcontasPagarTributos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcontasPagarTributos['max'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS, $idcontasPagarTributos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS, $idcontasPagarTributos, $comparison);
    }

    /**
     * Filter the query on the tributo_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTributoId(1234); // WHERE tributo_id = 1234
     * $query->filterByTributoId(array(12, 34)); // WHERE tributo_id IN (12, 34)
     * $query->filterByTributoId(array('min' => 12)); // WHERE tributo_id > 12
     * </code>
     *
     * @see       filterByTributo()
     *
     * @param     mixed $tributoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByTributoId($tributoId = null, $comparison = null)
    {
        if (is_array($tributoId)) {
            $useMinMax = false;
            if (isset($tributoId['min'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_TRIBUTO_ID, $tributoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tributoId['max'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_TRIBUTO_ID, $tributoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_TRIBUTO_ID, $tributoId, $comparison);
    }

    /**
     * Filter the query on the percentual column
     *
     * Example usage:
     * <code>
     * $query->filterByPercentual(1234); // WHERE percentual = 1234
     * $query->filterByPercentual(array(12, 34)); // WHERE percentual IN (12, 34)
     * $query->filterByPercentual(array('min' => 12)); // WHERE percentual > 12
     * </code>
     *
     * @param     mixed $percentual The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByPercentual($percentual = null, $comparison = null)
    {
        if (is_array($percentual)) {
            $useMinMax = false;
            if (isset($percentual['min'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_PERCENTUAL, $percentual['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($percentual['max'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_PERCENTUAL, $percentual['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_PERCENTUAL, $percentual, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query on the conta_pagar_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContaPagarId(1234); // WHERE conta_pagar_id = 1234
     * $query->filterByContaPagarId(array(12, 34)); // WHERE conta_pagar_id IN (12, 34)
     * $query->filterByContaPagarId(array('min' => 12)); // WHERE conta_pagar_id > 12
     * </code>
     *
     * @see       filterByContasPagar()
     *
     * @param     mixed $contaPagarId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByContaPagarId($contaPagarId = null, $comparison = null)
    {
        if (is_array($contaPagarId)) {
            $useMinMax = false;
            if (isset($contaPagarId['min'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_CONTA_PAGAR_ID, $contaPagarId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contaPagarId['max'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_CONTA_PAGAR_ID, $contaPagarId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_CONTA_PAGAR_ID, $contaPagarId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ContasPagarTributosTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTributosTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagar object
     *
     * @param \ImaTelecomBundle\Model\ContasPagar|ObjectCollection $contasPagar The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByContasPagar($contasPagar, $comparison = null)
    {
        if ($contasPagar instanceof \ImaTelecomBundle\Model\ContasPagar) {
            return $this
                ->addUsingAlias(ContasPagarTributosTableMap::COL_CONTA_PAGAR_ID, $contasPagar->getIdcontasPagar(), $comparison);
        } elseif ($contasPagar instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContasPagarTributosTableMap::COL_CONTA_PAGAR_ID, $contasPagar->toKeyValue('PrimaryKey', 'IdcontasPagar'), $comparison);
        } else {
            throw new PropelException('filterByContasPagar() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function joinContasPagar($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagar');
        }

        return $this;
    }

    /**
     * Use the ContasPagar relation ContasPagar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContasPagar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagar', '\ImaTelecomBundle\Model\ContasPagarQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Tributo object
     *
     * @param \ImaTelecomBundle\Model\Tributo|ObjectCollection $tributo The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByTributo($tributo, $comparison = null)
    {
        if ($tributo instanceof \ImaTelecomBundle\Model\Tributo) {
            return $this
                ->addUsingAlias(ContasPagarTributosTableMap::COL_TRIBUTO_ID, $tributo->getIdtributo(), $comparison);
        } elseif ($tributo instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContasPagarTributosTableMap::COL_TRIBUTO_ID, $tributo->toKeyValue('PrimaryKey', 'Idtributo'), $comparison);
        } else {
            throw new PropelException('filterByTributo() only accepts arguments of type \ImaTelecomBundle\Model\Tributo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tributo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function joinTributo($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tributo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tributo');
        }

        return $this;
    }

    /**
     * Use the Tributo relation Tributo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\TributoQuery A secondary query class using the current class as primary query
     */
    public function useTributoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTributo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tributo', '\ImaTelecomBundle\Model\TributoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ContasPagarTributosTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContasPagarTributosTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildContasPagarTributos $contasPagarTributos Object to remove from the list of results
     *
     * @return $this|ChildContasPagarTributosQuery The current query, for fluid interface
     */
    public function prune($contasPagarTributos = null)
    {
        if ($contasPagarTributos) {
            $this->addUsingAlias(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS, $contasPagarTributos->getIdcontasPagarTributos(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the contas_pagar_tributos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTributosTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ContasPagarTributosTableMap::clearInstancePool();
            ContasPagarTributosTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTributosTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ContasPagarTributosTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ContasPagarTributosTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ContasPagarTributosTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ContasPagarTributosQuery
