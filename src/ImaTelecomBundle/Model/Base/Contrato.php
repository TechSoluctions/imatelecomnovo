<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Contrato as ChildContrato;
use ImaTelecomBundle\Model\ContratoQuery as ChildContratoQuery;
use ImaTelecomBundle\Model\EnderecoCliente as ChildEnderecoCliente;
use ImaTelecomBundle\Model\EnderecoClienteQuery as ChildEnderecoClienteQuery;
use ImaTelecomBundle\Model\RegraFaturamento as ChildRegraFaturamento;
use ImaTelecomBundle\Model\RegraFaturamentoQuery as ChildRegraFaturamentoQuery;
use ImaTelecomBundle\Model\ServicoCliente as ChildServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery as ChildServicoClienteQuery;
use ImaTelecomBundle\Model\Map\ContratoTableMap;
use ImaTelecomBundle\Model\Map\ServicoClienteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'contrato' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Contrato implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\ContratoTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idcontrato field.
     *
     * @var        int
     */
    protected $idcontrato;

    /**
     * The value for the descricao field.
     *
     * @var        string
     */
    protected $descricao;

    /**
     * The value for the valor field.
     *
     * @var        string
     */
    protected $valor;

    /**
     * The value for the endereco_cliente_id field.
     *
     * @var        int
     */
    protected $endereco_cliente_id;

    /**
     * The value for the data_contratado field.
     *
     * @var        DateTime
     */
    protected $data_contratado;

    /**
     * The value for the data_cancelado field.
     *
     * @var        DateTime
     */
    protected $data_cancelado;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the cancelado field.
     *
     * @var        boolean
     */
    protected $cancelado;

    /**
     * The value for the cliente_id field.
     *
     * @var        int
     */
    protected $cliente_id;

    /**
     * The value for the forma_pagamento_id field.
     *
     * @var        int
     */
    protected $forma_pagamento_id;

    /**
     * The value for the profile_pagamento_id field.
     *
     * @var        int
     */
    protected $profile_pagamento_id;

    /**
     * The value for the vendedor_id field.
     *
     * @var        int
     */
    protected $vendedor_id;

    /**
     * The value for the renovado_ate field.
     *
     * @var        DateTime
     */
    protected $renovado_ate;

    /**
     * The value for the primeiro_vencimento field.
     *
     * @var        DateTime
     */
    protected $primeiro_vencimento;

    /**
     * The value for the percentual_multa field.
     *
     * @var        int
     */
    protected $percentual_multa;

    /**
     * The value for the percentual_acrescimo field.
     *
     * @var        int
     */
    protected $percentual_acrescimo;

    /**
     * The value for the usuario_cancelamento field.
     *
     * @var        int
     */
    protected $usuario_cancelamento;

    /**
     * The value for the motivo_cancelamento field.
     *
     * @var        int
     */
    protected $motivo_cancelamento;

    /**
     * The value for the agencia_debito field.
     *
     * @var        string
     */
    protected $agencia_debito;

    /**
     * The value for the id_cliente_banco field.
     *
     * @var        string
     */
    protected $id_cliente_banco;

    /**
     * The value for the emissao_nf_21 field.
     *
     * @var        boolean
     */
    protected $emissao_nf_21;

    /**
     * The value for the emissao_nf_pre field.
     *
     * @var        boolean
     */
    protected $emissao_nf_pre;

    /**
     * The value for the ignora_renovacao_automatica field.
     *
     * @var        boolean
     */
    protected $ignora_renovacao_automatica;

    /**
     * The value for the comodato field.
     *
     * @var        int
     */
    protected $comodato;

    /**
     * The value for the empresa_id field.
     *
     * @var        int
     */
    protected $empresa_id;

    /**
     * The value for the descricao_comodato field.
     *
     * @var        string
     */
    protected $descricao_comodato;

    /**
     * The value for the data_comodato field.
     *
     * @var        DateTime
     */
    protected $data_comodato;

    /**
     * The value for the obs_comodato field.
     *
     * @var        string
     */
    protected $obs_comodato;

    /**
     * The value for the centrocusto field.
     *
     * @var        int
     */
    protected $centrocusto;

    /**
     * The value for the unidade_negocio field.
     *
     * @var        int
     */
    protected $unidade_negocio;

    /**
     * The value for the valor_renovacao field.
     *
     * @var        string
     */
    protected $valor_renovacao;

    /**
     * The value for the modelo_nf field.
     *
     * @var        int
     */
    protected $modelo_nf;

    /**
     * The value for the emissao_nfse field.
     *
     * @var        boolean
     */
    protected $emissao_nfse;

    /**
     * The value for the suspenso field.
     *
     * @var        boolean
     */
    protected $suspenso;

    /**
     * The value for the data_suspensao field.
     *
     * @var        DateTime
     */
    protected $data_suspensao;

    /**
     * The value for the usuario_suspensao field.
     *
     * @var        int
     */
    protected $usuario_suspensao;

    /**
     * The value for the motivo_suspensao field.
     *
     * @var        int
     */
    protected $motivo_suspensao;

    /**
     * The value for the versao field.
     *
     * @var        int
     */
    protected $versao;

    /**
     * The value for the faturamento_automatico field.
     *
     * @var        boolean
     */
    protected $faturamento_automatico;

    /**
     * The value for the regra_faturamento_id field.
     *
     * @var        int
     */
    protected $regra_faturamento_id;

    /**
     * The value for the endereco_nf_id field.
     *
     * @var        int
     */
    protected $endereco_nf_id;

    /**
     * The value for the endereco_nfse_id field.
     *
     * @var        int
     */
    protected $endereco_nfse_id;

    /**
     * The value for the obs_nf field.
     *
     * @var        string
     */
    protected $obs_nf;

    /**
     * The value for the tipo_faturamento_id field.
     *
     * @var        int
     */
    protected $tipo_faturamento_id;

    /**
     * The value for the tem_scm field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $tem_scm;

    /**
     * The value for the scm field.
     *
     * @var        string
     */
    protected $scm;

    /**
     * The value for the tem_sva field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $tem_sva;

    /**
     * The value for the sva field.
     *
     * @var        string
     */
    protected $sva;

    /**
     * The value for the forcar_valor field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $forcar_valor;

    /**
     * The value for the descricao_scm field.
     *
     * @var        string
     */
    protected $descricao_scm;

    /**
     * The value for the descricao_sva field.
     *
     * @var        string
     */
    protected $descricao_sva;

    /**
     * The value for the tipo_scm field.
     *
     * @var        string
     */
    protected $tipo_scm;

    /**
     * The value for the tipo_sva field.
     *
     * @var        string
     */
    protected $tipo_sva;

    /**
     * The value for the gerar_cobranca field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $gerar_cobranca;

    /**
     * The value for the codigo_obra field.
     *
     * @var        string
     */
    protected $codigo_obra;

    /**
     * The value for the art field.
     *
     * @var        string
     */
    protected $art;

    /**
     * The value for the tem_reembolso field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $tem_reembolso;

    /**
     * @var        ChildEnderecoCliente
     */
    protected $aEnderecoCliente;

    /**
     * @var        ChildRegraFaturamento
     */
    protected $aRegraFaturamento;

    /**
     * @var        ObjectCollection|ChildServicoCliente[] Collection to store aggregation of ChildServicoCliente objects.
     */
    protected $collServicoClientes;
    protected $collServicoClientesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicoCliente[]
     */
    protected $servicoClientesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->tem_scm = false;
        $this->tem_sva = false;
        $this->forcar_valor = false;
        $this->gerar_cobranca = false;
        $this->tem_reembolso = true;
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Contrato object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Contrato</code> instance.  If
     * <code>obj</code> is an instance of <code>Contrato</code>, delegates to
     * <code>equals(Contrato)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Contrato The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idcontrato] column value.
     *
     * @return int
     */
    public function getIdcontrato()
    {
        return $this->idcontrato;
    }

    /**
     * Get the [descricao] column value.
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Get the [valor] column value.
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Get the [endereco_cliente_id] column value.
     *
     * @return int
     */
    public function getEnderecoClienteId()
    {
        return $this->endereco_cliente_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_contratado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataContratado($format = NULL)
    {
        if ($format === null) {
            return $this->data_contratado;
        } else {
            return $this->data_contratado instanceof \DateTimeInterface ? $this->data_contratado->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_cancelado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCancelado($format = NULL)
    {
        if ($format === null) {
            return $this->data_cancelado;
        } else {
            return $this->data_cancelado instanceof \DateTimeInterface ? $this->data_cancelado->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [cancelado] column value.
     *
     * @return boolean
     */
    public function getCancelado()
    {
        return $this->cancelado;
    }

    /**
     * Get the [cancelado] column value.
     *
     * @return boolean
     */
    public function isCancelado()
    {
        return $this->getCancelado();
    }

    /**
     * Get the [cliente_id] column value.
     *
     * @return int
     */
    public function getClienteId()
    {
        return $this->cliente_id;
    }

    /**
     * Get the [forma_pagamento_id] column value.
     *
     * @return int
     */
    public function getFormaPagamentoId()
    {
        return $this->forma_pagamento_id;
    }

    /**
     * Get the [profile_pagamento_id] column value.
     *
     * @return int
     */
    public function getProfilePagamentoId()
    {
        return $this->profile_pagamento_id;
    }

    /**
     * Get the [vendedor_id] column value.
     *
     * @return int
     */
    public function getVendedorId()
    {
        return $this->vendedor_id;
    }

    /**
     * Get the [optionally formatted] temporal [renovado_ate] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getRenovadoAte($format = NULL)
    {
        if ($format === null) {
            return $this->renovado_ate;
        } else {
            return $this->renovado_ate instanceof \DateTimeInterface ? $this->renovado_ate->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [primeiro_vencimento] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPrimeiroVencimento($format = NULL)
    {
        if ($format === null) {
            return $this->primeiro_vencimento;
        } else {
            return $this->primeiro_vencimento instanceof \DateTimeInterface ? $this->primeiro_vencimento->format($format) : null;
        }
    }

    /**
     * Get the [percentual_multa] column value.
     *
     * @return int
     */
    public function getPercentualMulta()
    {
        return $this->percentual_multa;
    }

    /**
     * Get the [percentual_acrescimo] column value.
     *
     * @return int
     */
    public function getPercentualAcrescimo()
    {
        return $this->percentual_acrescimo;
    }

    /**
     * Get the [usuario_cancelamento] column value.
     *
     * @return int
     */
    public function getUsuarioCancelamento()
    {
        return $this->usuario_cancelamento;
    }

    /**
     * Get the [motivo_cancelamento] column value.
     *
     * @return int
     */
    public function getMotivoCancelamento()
    {
        return $this->motivo_cancelamento;
    }

    /**
     * Get the [agencia_debito] column value.
     *
     * @return string
     */
    public function getAgenciaDebito()
    {
        return $this->agencia_debito;
    }

    /**
     * Get the [id_cliente_banco] column value.
     *
     * @return string
     */
    public function getIdClienteBanco()
    {
        return $this->id_cliente_banco;
    }

    /**
     * Get the [emissao_nf_21] column value.
     *
     * @return boolean
     */
    public function getEmissaoNf21()
    {
        return $this->emissao_nf_21;
    }

    /**
     * Get the [emissao_nf_21] column value.
     *
     * @return boolean
     */
    public function isEmissaoNf21()
    {
        return $this->getEmissaoNf21();
    }

    /**
     * Get the [emissao_nf_pre] column value.
     *
     * @return boolean
     */
    public function getEmissaoNfPre()
    {
        return $this->emissao_nf_pre;
    }

    /**
     * Get the [emissao_nf_pre] column value.
     *
     * @return boolean
     */
    public function isEmissaoNfPre()
    {
        return $this->getEmissaoNfPre();
    }

    /**
     * Get the [ignora_renovacao_automatica] column value.
     *
     * @return boolean
     */
    public function getIgnoraRenovacaoAutomatica()
    {
        return $this->ignora_renovacao_automatica;
    }

    /**
     * Get the [ignora_renovacao_automatica] column value.
     *
     * @return boolean
     */
    public function isIgnoraRenovacaoAutomatica()
    {
        return $this->getIgnoraRenovacaoAutomatica();
    }

    /**
     * Get the [comodato] column value.
     *
     * @return int
     */
    public function getComodato()
    {
        return $this->comodato;
    }

    /**
     * Get the [empresa_id] column value.
     *
     * @return int
     */
    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    /**
     * Get the [descricao_comodato] column value.
     *
     * @return string
     */
    public function getDescricaoComodato()
    {
        return $this->descricao_comodato;
    }

    /**
     * Get the [optionally formatted] temporal [data_comodato] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataComodato($format = NULL)
    {
        if ($format === null) {
            return $this->data_comodato;
        } else {
            return $this->data_comodato instanceof \DateTimeInterface ? $this->data_comodato->format($format) : null;
        }
    }

    /**
     * Get the [obs_comodato] column value.
     *
     * @return string
     */
    public function getObsComodato()
    {
        return $this->obs_comodato;
    }

    /**
     * Get the [centrocusto] column value.
     *
     * @return int
     */
    public function getCentrocusto()
    {
        return $this->centrocusto;
    }

    /**
     * Get the [unidade_negocio] column value.
     *
     * @return int
     */
    public function getUnidadeNegocio()
    {
        return $this->unidade_negocio;
    }

    /**
     * Get the [valor_renovacao] column value.
     *
     * @return string
     */
    public function getValorRenovacao()
    {
        return $this->valor_renovacao;
    }

    /**
     * Get the [modelo_nf] column value.
     *
     * @return int
     */
    public function getModeloNf()
    {
        return $this->modelo_nf;
    }

    /**
     * Get the [emissao_nfse] column value.
     *
     * @return boolean
     */
    public function getEmissaoNfse()
    {
        return $this->emissao_nfse;
    }

    /**
     * Get the [emissao_nfse] column value.
     *
     * @return boolean
     */
    public function isEmissaoNfse()
    {
        return $this->getEmissaoNfse();
    }

    /**
     * Get the [suspenso] column value.
     *
     * @return boolean
     */
    public function getSuspenso()
    {
        return $this->suspenso;
    }

    /**
     * Get the [suspenso] column value.
     *
     * @return boolean
     */
    public function isSuspenso()
    {
        return $this->getSuspenso();
    }

    /**
     * Get the [optionally formatted] temporal [data_suspensao] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataSuspensao($format = NULL)
    {
        if ($format === null) {
            return $this->data_suspensao;
        } else {
            return $this->data_suspensao instanceof \DateTimeInterface ? $this->data_suspensao->format($format) : null;
        }
    }

    /**
     * Get the [usuario_suspensao] column value.
     *
     * @return int
     */
    public function getUsuarioSuspensao()
    {
        return $this->usuario_suspensao;
    }

    /**
     * Get the [motivo_suspensao] column value.
     *
     * @return int
     */
    public function getMotivoSuspensao()
    {
        return $this->motivo_suspensao;
    }

    /**
     * Get the [versao] column value.
     *
     * @return int
     */
    public function getVersao()
    {
        return $this->versao;
    }

    /**
     * Get the [faturamento_automatico] column value.
     *
     * @return boolean
     */
    public function getFaturamentoAutomatico()
    {
        return $this->faturamento_automatico;
    }

    /**
     * Get the [faturamento_automatico] column value.
     *
     * @return boolean
     */
    public function isFaturamentoAutomatico()
    {
        return $this->getFaturamentoAutomatico();
    }

    /**
     * Get the [regra_faturamento_id] column value.
     *
     * @return int
     */
    public function getRegraFaturamentoId()
    {
        return $this->regra_faturamento_id;
    }

    /**
     * Get the [endereco_nf_id] column value.
     *
     * @return int
     */
    public function getEnderecoNfId()
    {
        return $this->endereco_nf_id;
    }

    /**
     * Get the [endereco_nfse_id] column value.
     *
     * @return int
     */
    public function getEnderecoNfseId()
    {
        return $this->endereco_nfse_id;
    }

    /**
     * Get the [obs_nf] column value.
     *
     * @return string
     */
    public function getObsNf()
    {
        return $this->obs_nf;
    }

    /**
     * Get the [tipo_faturamento_id] column value.
     *
     * @return int
     */
    public function getTipoFaturamentoId()
    {
        return $this->tipo_faturamento_id;
    }

    /**
     * Get the [tem_scm] column value.
     *
     * @return boolean
     */
    public function getTemScm()
    {
        return $this->tem_scm;
    }

    /**
     * Get the [tem_scm] column value.
     *
     * @return boolean
     */
    public function isTemScm()
    {
        return $this->getTemScm();
    }

    /**
     * Get the [scm] column value.
     *
     * @return string
     */
    public function getScm()
    {
        return $this->scm;
    }

    /**
     * Get the [tem_sva] column value.
     *
     * @return boolean
     */
    public function getTemSva()
    {
        return $this->tem_sva;
    }

    /**
     * Get the [tem_sva] column value.
     *
     * @return boolean
     */
    public function isTemSva()
    {
        return $this->getTemSva();
    }

    /**
     * Get the [sva] column value.
     *
     * @return string
     */
    public function getSva()
    {
        return $this->sva;
    }

    /**
     * Get the [forcar_valor] column value.
     *
     * @return boolean
     */
    public function getForcarValor()
    {
        return $this->forcar_valor;
    }

    /**
     * Get the [forcar_valor] column value.
     *
     * @return boolean
     */
    public function isForcarValor()
    {
        return $this->getForcarValor();
    }

    /**
     * Get the [descricao_scm] column value.
     *
     * @return string
     */
    public function getDescricaoScm()
    {
        return $this->descricao_scm;
    }

    /**
     * Get the [descricao_sva] column value.
     *
     * @return string
     */
    public function getDescricaoSva()
    {
        return $this->descricao_sva;
    }

    /**
     * Get the [tipo_scm] column value.
     *
     * @return string
     */
    public function getTipoScm()
    {
        return $this->tipo_scm;
    }

    /**
     * Get the [tipo_sva] column value.
     *
     * @return string
     */
    public function getTipoSva()
    {
        return $this->tipo_sva;
    }

    /**
     * Get the [gerar_cobranca] column value.
     *
     * @return boolean
     */
    public function getGerarCobranca()
    {
        return $this->gerar_cobranca;
    }

    /**
     * Get the [gerar_cobranca] column value.
     *
     * @return boolean
     */
    public function isGerarCobranca()
    {
        return $this->getGerarCobranca();
    }

    /**
     * Get the [codigo_obra] column value.
     *
     * @return string
     */
    public function getCodigoObra()
    {
        return $this->codigo_obra;
    }

    /**
     * Get the [art] column value.
     *
     * @return string
     */
    public function getArt()
    {
        return $this->art;
    }

    /**
     * Get the [tem_reembolso] column value.
     *
     * @return boolean
     */
    public function getTemReembolso()
    {
        return $this->tem_reembolso;
    }

    /**
     * Get the [tem_reembolso] column value.
     *
     * @return boolean
     */
    public function isTemReembolso()
    {
        return $this->getTemReembolso();
    }

    /**
     * Set the value of [idcontrato] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setIdcontrato($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idcontrato !== $v) {
            $this->idcontrato = $v;
            $this->modifiedColumns[ContratoTableMap::COL_IDCONTRATO] = true;
        }

        return $this;
    } // setIdcontrato()

    /**
     * Set the value of [descricao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDescricao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao !== $v) {
            $this->descricao = $v;
            $this->modifiedColumns[ContratoTableMap::COL_DESCRICAO] = true;
        }

        return $this;
    } // setDescricao()

    /**
     * Set the value of [valor] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setValor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor !== $v) {
            $this->valor = $v;
            $this->modifiedColumns[ContratoTableMap::COL_VALOR] = true;
        }

        return $this;
    } // setValor()

    /**
     * Set the value of [endereco_cliente_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setEnderecoClienteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->endereco_cliente_id !== $v) {
            $this->endereco_cliente_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_ENDERECO_CLIENTE_ID] = true;
        }

        if ($this->aEnderecoCliente !== null && $this->aEnderecoCliente->getIdenderecoCliente() !== $v) {
            $this->aEnderecoCliente = null;
        }

        return $this;
    } // setEnderecoClienteId()

    /**
     * Sets the value of [data_contratado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDataContratado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_contratado !== null || $dt !== null) {
            if ($this->data_contratado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_contratado->format("Y-m-d H:i:s.u")) {
                $this->data_contratado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContratoTableMap::COL_DATA_CONTRATADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataContratado()

    /**
     * Sets the value of [data_cancelado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDataCancelado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cancelado !== null || $dt !== null) {
            if ($this->data_cancelado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cancelado->format("Y-m-d H:i:s.u")) {
                $this->data_cancelado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContratoTableMap::COL_DATA_CANCELADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCancelado()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContratoTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContratoTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[ContratoTableMap::COL_USUARIO_ALTERADO] = true;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Sets the value of the [cancelado] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setCancelado($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cancelado !== $v) {
            $this->cancelado = $v;
            $this->modifiedColumns[ContratoTableMap::COL_CANCELADO] = true;
        }

        return $this;
    } // setCancelado()

    /**
     * Set the value of [cliente_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setClienteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cliente_id !== $v) {
            $this->cliente_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_CLIENTE_ID] = true;
        }

        return $this;
    } // setClienteId()

    /**
     * Set the value of [forma_pagamento_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setFormaPagamentoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->forma_pagamento_id !== $v) {
            $this->forma_pagamento_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_FORMA_PAGAMENTO_ID] = true;
        }

        return $this;
    } // setFormaPagamentoId()

    /**
     * Set the value of [profile_pagamento_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setProfilePagamentoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->profile_pagamento_id !== $v) {
            $this->profile_pagamento_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_PROFILE_PAGAMENTO_ID] = true;
        }

        return $this;
    } // setProfilePagamentoId()

    /**
     * Set the value of [vendedor_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setVendedorId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vendedor_id !== $v) {
            $this->vendedor_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_VENDEDOR_ID] = true;
        }

        return $this;
    } // setVendedorId()

    /**
     * Sets the value of [renovado_ate] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setRenovadoAte($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->renovado_ate !== null || $dt !== null) {
            if ($this->renovado_ate === null || $dt === null || $dt->format("Y-m-d") !== $this->renovado_ate->format("Y-m-d")) {
                $this->renovado_ate = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContratoTableMap::COL_RENOVADO_ATE] = true;
            }
        } // if either are not null

        return $this;
    } // setRenovadoAte()

    /**
     * Sets the value of [primeiro_vencimento] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setPrimeiroVencimento($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->primeiro_vencimento !== null || $dt !== null) {
            if ($this->primeiro_vencimento === null || $dt === null || $dt->format("Y-m-d") !== $this->primeiro_vencimento->format("Y-m-d")) {
                $this->primeiro_vencimento = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContratoTableMap::COL_PRIMEIRO_VENCIMENTO] = true;
            }
        } // if either are not null

        return $this;
    } // setPrimeiroVencimento()

    /**
     * Set the value of [percentual_multa] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setPercentualMulta($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->percentual_multa !== $v) {
            $this->percentual_multa = $v;
            $this->modifiedColumns[ContratoTableMap::COL_PERCENTUAL_MULTA] = true;
        }

        return $this;
    } // setPercentualMulta()

    /**
     * Set the value of [percentual_acrescimo] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setPercentualAcrescimo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->percentual_acrescimo !== $v) {
            $this->percentual_acrescimo = $v;
            $this->modifiedColumns[ContratoTableMap::COL_PERCENTUAL_ACRESCIMO] = true;
        }

        return $this;
    } // setPercentualAcrescimo()

    /**
     * Set the value of [usuario_cancelamento] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setUsuarioCancelamento($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_cancelamento !== $v) {
            $this->usuario_cancelamento = $v;
            $this->modifiedColumns[ContratoTableMap::COL_USUARIO_CANCELAMENTO] = true;
        }

        return $this;
    } // setUsuarioCancelamento()

    /**
     * Set the value of [motivo_cancelamento] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setMotivoCancelamento($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->motivo_cancelamento !== $v) {
            $this->motivo_cancelamento = $v;
            $this->modifiedColumns[ContratoTableMap::COL_MOTIVO_CANCELAMENTO] = true;
        }

        return $this;
    } // setMotivoCancelamento()

    /**
     * Set the value of [agencia_debito] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setAgenciaDebito($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->agencia_debito !== $v) {
            $this->agencia_debito = $v;
            $this->modifiedColumns[ContratoTableMap::COL_AGENCIA_DEBITO] = true;
        }

        return $this;
    } // setAgenciaDebito()

    /**
     * Set the value of [id_cliente_banco] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setIdClienteBanco($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->id_cliente_banco !== $v) {
            $this->id_cliente_banco = $v;
            $this->modifiedColumns[ContratoTableMap::COL_ID_CLIENTE_BANCO] = true;
        }

        return $this;
    } // setIdClienteBanco()

    /**
     * Sets the value of the [emissao_nf_21] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setEmissaoNf21($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->emissao_nf_21 !== $v) {
            $this->emissao_nf_21 = $v;
            $this->modifiedColumns[ContratoTableMap::COL_EMISSAO_NF_21] = true;
        }

        return $this;
    } // setEmissaoNf21()

    /**
     * Sets the value of the [emissao_nf_pre] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setEmissaoNfPre($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->emissao_nf_pre !== $v) {
            $this->emissao_nf_pre = $v;
            $this->modifiedColumns[ContratoTableMap::COL_EMISSAO_NF_PRE] = true;
        }

        return $this;
    } // setEmissaoNfPre()

    /**
     * Sets the value of the [ignora_renovacao_automatica] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setIgnoraRenovacaoAutomatica($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ignora_renovacao_automatica !== $v) {
            $this->ignora_renovacao_automatica = $v;
            $this->modifiedColumns[ContratoTableMap::COL_IGNORA_RENOVACAO_AUTOMATICA] = true;
        }

        return $this;
    } // setIgnoraRenovacaoAutomatica()

    /**
     * Set the value of [comodato] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setComodato($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->comodato !== $v) {
            $this->comodato = $v;
            $this->modifiedColumns[ContratoTableMap::COL_COMODATO] = true;
        }

        return $this;
    } // setComodato()

    /**
     * Set the value of [empresa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setEmpresaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->empresa_id !== $v) {
            $this->empresa_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_EMPRESA_ID] = true;
        }

        return $this;
    } // setEmpresaId()

    /**
     * Set the value of [descricao_comodato] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDescricaoComodato($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao_comodato !== $v) {
            $this->descricao_comodato = $v;
            $this->modifiedColumns[ContratoTableMap::COL_DESCRICAO_COMODATO] = true;
        }

        return $this;
    } // setDescricaoComodato()

    /**
     * Sets the value of [data_comodato] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDataComodato($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_comodato !== null || $dt !== null) {
            if ($this->data_comodato === null || $dt === null || $dt->format("Y-m-d") !== $this->data_comodato->format("Y-m-d")) {
                $this->data_comodato = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContratoTableMap::COL_DATA_COMODATO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataComodato()

    /**
     * Set the value of [obs_comodato] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setObsComodato($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->obs_comodato !== $v) {
            $this->obs_comodato = $v;
            $this->modifiedColumns[ContratoTableMap::COL_OBS_COMODATO] = true;
        }

        return $this;
    } // setObsComodato()

    /**
     * Set the value of [centrocusto] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setCentrocusto($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->centrocusto !== $v) {
            $this->centrocusto = $v;
            $this->modifiedColumns[ContratoTableMap::COL_CENTROCUSTO] = true;
        }

        return $this;
    } // setCentrocusto()

    /**
     * Set the value of [unidade_negocio] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setUnidadeNegocio($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->unidade_negocio !== $v) {
            $this->unidade_negocio = $v;
            $this->modifiedColumns[ContratoTableMap::COL_UNIDADE_NEGOCIO] = true;
        }

        return $this;
    } // setUnidadeNegocio()

    /**
     * Set the value of [valor_renovacao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setValorRenovacao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_renovacao !== $v) {
            $this->valor_renovacao = $v;
            $this->modifiedColumns[ContratoTableMap::COL_VALOR_RENOVACAO] = true;
        }

        return $this;
    } // setValorRenovacao()

    /**
     * Set the value of [modelo_nf] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setModeloNf($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->modelo_nf !== $v) {
            $this->modelo_nf = $v;
            $this->modifiedColumns[ContratoTableMap::COL_MODELO_NF] = true;
        }

        return $this;
    } // setModeloNf()

    /**
     * Sets the value of the [emissao_nfse] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setEmissaoNfse($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->emissao_nfse !== $v) {
            $this->emissao_nfse = $v;
            $this->modifiedColumns[ContratoTableMap::COL_EMISSAO_NFSE] = true;
        }

        return $this;
    } // setEmissaoNfse()

    /**
     * Sets the value of the [suspenso] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setSuspenso($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->suspenso !== $v) {
            $this->suspenso = $v;
            $this->modifiedColumns[ContratoTableMap::COL_SUSPENSO] = true;
        }

        return $this;
    } // setSuspenso()

    /**
     * Sets the value of [data_suspensao] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDataSuspensao($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_suspensao !== null || $dt !== null) {
            if ($this->data_suspensao === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_suspensao->format("Y-m-d H:i:s.u")) {
                $this->data_suspensao = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContratoTableMap::COL_DATA_SUSPENSAO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataSuspensao()

    /**
     * Set the value of [usuario_suspensao] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setUsuarioSuspensao($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_suspensao !== $v) {
            $this->usuario_suspensao = $v;
            $this->modifiedColumns[ContratoTableMap::COL_USUARIO_SUSPENSAO] = true;
        }

        return $this;
    } // setUsuarioSuspensao()

    /**
     * Set the value of [motivo_suspensao] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setMotivoSuspensao($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->motivo_suspensao !== $v) {
            $this->motivo_suspensao = $v;
            $this->modifiedColumns[ContratoTableMap::COL_MOTIVO_SUSPENSAO] = true;
        }

        return $this;
    } // setMotivoSuspensao()

    /**
     * Set the value of [versao] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setVersao($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->versao !== $v) {
            $this->versao = $v;
            $this->modifiedColumns[ContratoTableMap::COL_VERSAO] = true;
        }

        return $this;
    } // setVersao()

    /**
     * Sets the value of the [faturamento_automatico] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setFaturamentoAutomatico($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->faturamento_automatico !== $v) {
            $this->faturamento_automatico = $v;
            $this->modifiedColumns[ContratoTableMap::COL_FATURAMENTO_AUTOMATICO] = true;
        }

        return $this;
    } // setFaturamentoAutomatico()

    /**
     * Set the value of [regra_faturamento_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setRegraFaturamentoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->regra_faturamento_id !== $v) {
            $this->regra_faturamento_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_REGRA_FATURAMENTO_ID] = true;
        }

        if ($this->aRegraFaturamento !== null && $this->aRegraFaturamento->getIdregraFaturamento() !== $v) {
            $this->aRegraFaturamento = null;
        }

        return $this;
    } // setRegraFaturamentoId()

    /**
     * Set the value of [endereco_nf_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setEnderecoNfId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->endereco_nf_id !== $v) {
            $this->endereco_nf_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_ENDERECO_NF_ID] = true;
        }

        return $this;
    } // setEnderecoNfId()

    /**
     * Set the value of [endereco_nfse_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setEnderecoNfseId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->endereco_nfse_id !== $v) {
            $this->endereco_nfse_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_ENDERECO_NFSE_ID] = true;
        }

        return $this;
    } // setEnderecoNfseId()

    /**
     * Set the value of [obs_nf] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setObsNf($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->obs_nf !== $v) {
            $this->obs_nf = $v;
            $this->modifiedColumns[ContratoTableMap::COL_OBS_NF] = true;
        }

        return $this;
    } // setObsNf()

    /**
     * Set the value of [tipo_faturamento_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setTipoFaturamentoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->tipo_faturamento_id !== $v) {
            $this->tipo_faturamento_id = $v;
            $this->modifiedColumns[ContratoTableMap::COL_TIPO_FATURAMENTO_ID] = true;
        }

        return $this;
    } // setTipoFaturamentoId()

    /**
     * Sets the value of the [tem_scm] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setTemScm($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->tem_scm !== $v) {
            $this->tem_scm = $v;
            $this->modifiedColumns[ContratoTableMap::COL_TEM_SCM] = true;
        }

        return $this;
    } // setTemScm()

    /**
     * Set the value of [scm] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setScm($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->scm !== $v) {
            $this->scm = $v;
            $this->modifiedColumns[ContratoTableMap::COL_SCM] = true;
        }

        return $this;
    } // setScm()

    /**
     * Sets the value of the [tem_sva] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setTemSva($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->tem_sva !== $v) {
            $this->tem_sva = $v;
            $this->modifiedColumns[ContratoTableMap::COL_TEM_SVA] = true;
        }

        return $this;
    } // setTemSva()

    /**
     * Set the value of [sva] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setSva($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sva !== $v) {
            $this->sva = $v;
            $this->modifiedColumns[ContratoTableMap::COL_SVA] = true;
        }

        return $this;
    } // setSva()

    /**
     * Sets the value of the [forcar_valor] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setForcarValor($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->forcar_valor !== $v) {
            $this->forcar_valor = $v;
            $this->modifiedColumns[ContratoTableMap::COL_FORCAR_VALOR] = true;
        }

        return $this;
    } // setForcarValor()

    /**
     * Set the value of [descricao_scm] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDescricaoScm($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao_scm !== $v) {
            $this->descricao_scm = $v;
            $this->modifiedColumns[ContratoTableMap::COL_DESCRICAO_SCM] = true;
        }

        return $this;
    } // setDescricaoScm()

    /**
     * Set the value of [descricao_sva] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setDescricaoSva($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao_sva !== $v) {
            $this->descricao_sva = $v;
            $this->modifiedColumns[ContratoTableMap::COL_DESCRICAO_SVA] = true;
        }

        return $this;
    } // setDescricaoSva()

    /**
     * Set the value of [tipo_scm] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setTipoScm($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_scm !== $v) {
            $this->tipo_scm = $v;
            $this->modifiedColumns[ContratoTableMap::COL_TIPO_SCM] = true;
        }

        return $this;
    } // setTipoScm()

    /**
     * Set the value of [tipo_sva] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setTipoSva($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_sva !== $v) {
            $this->tipo_sva = $v;
            $this->modifiedColumns[ContratoTableMap::COL_TIPO_SVA] = true;
        }

        return $this;
    } // setTipoSva()

    /**
     * Sets the value of the [gerar_cobranca] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setGerarCobranca($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->gerar_cobranca !== $v) {
            $this->gerar_cobranca = $v;
            $this->modifiedColumns[ContratoTableMap::COL_GERAR_COBRANCA] = true;
        }

        return $this;
    } // setGerarCobranca()

    /**
     * Set the value of [codigo_obra] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setCodigoObra($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo_obra !== $v) {
            $this->codigo_obra = $v;
            $this->modifiedColumns[ContratoTableMap::COL_CODIGO_OBRA] = true;
        }

        return $this;
    } // setCodigoObra()

    /**
     * Set the value of [art] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setArt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->art !== $v) {
            $this->art = $v;
            $this->modifiedColumns[ContratoTableMap::COL_ART] = true;
        }

        return $this;
    } // setArt()

    /**
     * Sets the value of the [tem_reembolso] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function setTemReembolso($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->tem_reembolso !== $v) {
            $this->tem_reembolso = $v;
            $this->modifiedColumns[ContratoTableMap::COL_TEM_REEMBOLSO] = true;
        }

        return $this;
    } // setTemReembolso()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->tem_scm !== false) {
                return false;
            }

            if ($this->tem_sva !== false) {
                return false;
            }

            if ($this->forcar_valor !== false) {
                return false;
            }

            if ($this->gerar_cobranca !== false) {
                return false;
            }

            if ($this->tem_reembolso !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ContratoTableMap::translateFieldName('Idcontrato', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idcontrato = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ContratoTableMap::translateFieldName('Descricao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ContratoTableMap::translateFieldName('Valor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ContratoTableMap::translateFieldName('EnderecoClienteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->endereco_cliente_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ContratoTableMap::translateFieldName('DataContratado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_contratado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ContratoTableMap::translateFieldName('DataCancelado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cancelado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ContratoTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ContratoTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ContratoTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ContratoTableMap::translateFieldName('Cancelado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cancelado = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ContratoTableMap::translateFieldName('ClienteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cliente_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ContratoTableMap::translateFieldName('FormaPagamentoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->forma_pagamento_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ContratoTableMap::translateFieldName('ProfilePagamentoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->profile_pagamento_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ContratoTableMap::translateFieldName('VendedorId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vendedor_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ContratoTableMap::translateFieldName('RenovadoAte', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->renovado_ate = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ContratoTableMap::translateFieldName('PrimeiroVencimento', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->primeiro_vencimento = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ContratoTableMap::translateFieldName('PercentualMulta', TableMap::TYPE_PHPNAME, $indexType)];
            $this->percentual_multa = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ContratoTableMap::translateFieldName('PercentualAcrescimo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->percentual_acrescimo = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ContratoTableMap::translateFieldName('UsuarioCancelamento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_cancelamento = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ContratoTableMap::translateFieldName('MotivoCancelamento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->motivo_cancelamento = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ContratoTableMap::translateFieldName('AgenciaDebito', TableMap::TYPE_PHPNAME, $indexType)];
            $this->agencia_debito = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ContratoTableMap::translateFieldName('IdClienteBanco', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_cliente_banco = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : ContratoTableMap::translateFieldName('EmissaoNf21', TableMap::TYPE_PHPNAME, $indexType)];
            $this->emissao_nf_21 = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : ContratoTableMap::translateFieldName('EmissaoNfPre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->emissao_nf_pre = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : ContratoTableMap::translateFieldName('IgnoraRenovacaoAutomatica', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ignora_renovacao_automatica = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : ContratoTableMap::translateFieldName('Comodato', TableMap::TYPE_PHPNAME, $indexType)];
            $this->comodato = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : ContratoTableMap::translateFieldName('EmpresaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->empresa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : ContratoTableMap::translateFieldName('DescricaoComodato', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao_comodato = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : ContratoTableMap::translateFieldName('DataComodato', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->data_comodato = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : ContratoTableMap::translateFieldName('ObsComodato', TableMap::TYPE_PHPNAME, $indexType)];
            $this->obs_comodato = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : ContratoTableMap::translateFieldName('Centrocusto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->centrocusto = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : ContratoTableMap::translateFieldName('UnidadeNegocio', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unidade_negocio = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : ContratoTableMap::translateFieldName('ValorRenovacao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_renovacao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : ContratoTableMap::translateFieldName('ModeloNf', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modelo_nf = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : ContratoTableMap::translateFieldName('EmissaoNfse', TableMap::TYPE_PHPNAME, $indexType)];
            $this->emissao_nfse = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 35 + $startcol : ContratoTableMap::translateFieldName('Suspenso', TableMap::TYPE_PHPNAME, $indexType)];
            $this->suspenso = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 36 + $startcol : ContratoTableMap::translateFieldName('DataSuspensao', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_suspensao = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 37 + $startcol : ContratoTableMap::translateFieldName('UsuarioSuspensao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_suspensao = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 38 + $startcol : ContratoTableMap::translateFieldName('MotivoSuspensao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->motivo_suspensao = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 39 + $startcol : ContratoTableMap::translateFieldName('Versao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->versao = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 40 + $startcol : ContratoTableMap::translateFieldName('FaturamentoAutomatico', TableMap::TYPE_PHPNAME, $indexType)];
            $this->faturamento_automatico = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 41 + $startcol : ContratoTableMap::translateFieldName('RegraFaturamentoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->regra_faturamento_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 42 + $startcol : ContratoTableMap::translateFieldName('EnderecoNfId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->endereco_nf_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 43 + $startcol : ContratoTableMap::translateFieldName('EnderecoNfseId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->endereco_nfse_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 44 + $startcol : ContratoTableMap::translateFieldName('ObsNf', TableMap::TYPE_PHPNAME, $indexType)];
            $this->obs_nf = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 45 + $startcol : ContratoTableMap::translateFieldName('TipoFaturamentoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_faturamento_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 46 + $startcol : ContratoTableMap::translateFieldName('TemScm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tem_scm = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 47 + $startcol : ContratoTableMap::translateFieldName('Scm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->scm = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 48 + $startcol : ContratoTableMap::translateFieldName('TemSva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tem_sva = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 49 + $startcol : ContratoTableMap::translateFieldName('Sva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sva = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 50 + $startcol : ContratoTableMap::translateFieldName('ForcarValor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->forcar_valor = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 51 + $startcol : ContratoTableMap::translateFieldName('DescricaoScm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao_scm = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 52 + $startcol : ContratoTableMap::translateFieldName('DescricaoSva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao_sva = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 53 + $startcol : ContratoTableMap::translateFieldName('TipoScm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_scm = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 54 + $startcol : ContratoTableMap::translateFieldName('TipoSva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_sva = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 55 + $startcol : ContratoTableMap::translateFieldName('GerarCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gerar_cobranca = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 56 + $startcol : ContratoTableMap::translateFieldName('CodigoObra', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codigo_obra = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 57 + $startcol : ContratoTableMap::translateFieldName('Art', TableMap::TYPE_PHPNAME, $indexType)];
            $this->art = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 58 + $startcol : ContratoTableMap::translateFieldName('TemReembolso', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tem_reembolso = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 59; // 59 = ContratoTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Contrato'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aEnderecoCliente !== null && $this->endereco_cliente_id !== $this->aEnderecoCliente->getIdenderecoCliente()) {
            $this->aEnderecoCliente = null;
        }
        if ($this->aRegraFaturamento !== null && $this->regra_faturamento_id !== $this->aRegraFaturamento->getIdregraFaturamento()) {
            $this->aRegraFaturamento = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContratoTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildContratoQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEnderecoCliente = null;
            $this->aRegraFaturamento = null;
            $this->collServicoClientes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Contrato::setDeleted()
     * @see Contrato::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratoTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildContratoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratoTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ContratoTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEnderecoCliente !== null) {
                if ($this->aEnderecoCliente->isModified() || $this->aEnderecoCliente->isNew()) {
                    $affectedRows += $this->aEnderecoCliente->save($con);
                }
                $this->setEnderecoCliente($this->aEnderecoCliente);
            }

            if ($this->aRegraFaturamento !== null) {
                if ($this->aRegraFaturamento->isModified() || $this->aRegraFaturamento->isNew()) {
                    $affectedRows += $this->aRegraFaturamento->save($con);
                }
                $this->setRegraFaturamento($this->aRegraFaturamento);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->servicoClientesScheduledForDeletion !== null) {
                if (!$this->servicoClientesScheduledForDeletion->isEmpty()) {
                    foreach ($this->servicoClientesScheduledForDeletion as $servicoCliente) {
                        // need to save related object because we set the relation to null
                        $servicoCliente->save($con);
                    }
                    $this->servicoClientesScheduledForDeletion = null;
                }
            }

            if ($this->collServicoClientes !== null) {
                foreach ($this->collServicoClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ContratoTableMap::COL_IDCONTRATO] = true;
        if (null !== $this->idcontrato) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ContratoTableMap::COL_IDCONTRATO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ContratoTableMap::COL_IDCONTRATO)) {
            $modifiedColumns[':p' . $index++]  = 'idcontrato';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DESCRICAO)) {
            $modifiedColumns[':p' . $index++]  = 'descricao';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_VALOR)) {
            $modifiedColumns[':p' . $index++]  = 'valor';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ENDERECO_CLIENTE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'endereco_cliente_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_CONTRATADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_contratado';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_CANCELADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cancelado';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_CANCELADO)) {
            $modifiedColumns[':p' . $index++]  = 'cancelado';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_CLIENTE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cliente_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_FORMA_PAGAMENTO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'forma_pagamento_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_PROFILE_PAGAMENTO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'profile_pagamento_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_VENDEDOR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vendedor_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_RENOVADO_ATE)) {
            $modifiedColumns[':p' . $index++]  = 'renovado_ate';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_PRIMEIRO_VENCIMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'primeiro_vencimento';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_PERCENTUAL_MULTA)) {
            $modifiedColumns[':p' . $index++]  = 'percentual_multa';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_PERCENTUAL_ACRESCIMO)) {
            $modifiedColumns[':p' . $index++]  = 'percentual_acrescimo';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_USUARIO_CANCELAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_cancelamento';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_MOTIVO_CANCELAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'motivo_cancelamento';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_AGENCIA_DEBITO)) {
            $modifiedColumns[':p' . $index++]  = 'agencia_debito';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ID_CLIENTE_BANCO)) {
            $modifiedColumns[':p' . $index++]  = 'id_cliente_banco';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_EMISSAO_NF_21)) {
            $modifiedColumns[':p' . $index++]  = 'emissao_nf_21';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_EMISSAO_NF_PRE)) {
            $modifiedColumns[':p' . $index++]  = 'emissao_nf_pre';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_IGNORA_RENOVACAO_AUTOMATICA)) {
            $modifiedColumns[':p' . $index++]  = 'ignora_renovacao_automatica';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_COMODATO)) {
            $modifiedColumns[':p' . $index++]  = 'comodato';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_EMPRESA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'empresa_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DESCRICAO_COMODATO)) {
            $modifiedColumns[':p' . $index++]  = 'descricao_comodato';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_COMODATO)) {
            $modifiedColumns[':p' . $index++]  = 'data_comodato';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_OBS_COMODATO)) {
            $modifiedColumns[':p' . $index++]  = 'obs_comodato';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_CENTROCUSTO)) {
            $modifiedColumns[':p' . $index++]  = 'centrocusto';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_UNIDADE_NEGOCIO)) {
            $modifiedColumns[':p' . $index++]  = 'unidade_negocio';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_VALOR_RENOVACAO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_renovacao';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_MODELO_NF)) {
            $modifiedColumns[':p' . $index++]  = 'modelo_nf';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_EMISSAO_NFSE)) {
            $modifiedColumns[':p' . $index++]  = 'emissao_nfse';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_SUSPENSO)) {
            $modifiedColumns[':p' . $index++]  = 'suspenso';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_SUSPENSAO)) {
            $modifiedColumns[':p' . $index++]  = 'data_suspensao';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_USUARIO_SUSPENSAO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_suspensao';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_MOTIVO_SUSPENSAO)) {
            $modifiedColumns[':p' . $index++]  = 'motivo_suspensao';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_VERSAO)) {
            $modifiedColumns[':p' . $index++]  = 'versao';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_FATURAMENTO_AUTOMATICO)) {
            $modifiedColumns[':p' . $index++]  = 'faturamento_automatico';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_REGRA_FATURAMENTO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'regra_faturamento_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ENDERECO_NF_ID)) {
            $modifiedColumns[':p' . $index++]  = 'endereco_nf_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ENDERECO_NFSE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'endereco_nfse_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_OBS_NF)) {
            $modifiedColumns[':p' . $index++]  = 'obs_nf';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TIPO_FATURAMENTO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_faturamento_id';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TEM_SCM)) {
            $modifiedColumns[':p' . $index++]  = 'tem_scm';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_SCM)) {
            $modifiedColumns[':p' . $index++]  = 'scm';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TEM_SVA)) {
            $modifiedColumns[':p' . $index++]  = 'tem_sva';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_SVA)) {
            $modifiedColumns[':p' . $index++]  = 'sva';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_FORCAR_VALOR)) {
            $modifiedColumns[':p' . $index++]  = 'forcar_valor';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DESCRICAO_SCM)) {
            $modifiedColumns[':p' . $index++]  = 'descricao_scm';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DESCRICAO_SVA)) {
            $modifiedColumns[':p' . $index++]  = 'descricao_sva';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TIPO_SCM)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_scm';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TIPO_SVA)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_sva';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_GERAR_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'gerar_cobranca';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_CODIGO_OBRA)) {
            $modifiedColumns[':p' . $index++]  = 'codigo_obra';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ART)) {
            $modifiedColumns[':p' . $index++]  = 'art';
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TEM_REEMBOLSO)) {
            $modifiedColumns[':p' . $index++]  = 'tem_reembolso';
        }

        $sql = sprintf(
            'INSERT INTO contrato (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idcontrato':
                        $stmt->bindValue($identifier, $this->idcontrato, PDO::PARAM_INT);
                        break;
                    case 'descricao':
                        $stmt->bindValue($identifier, $this->descricao, PDO::PARAM_STR);
                        break;
                    case 'valor':
                        $stmt->bindValue($identifier, $this->valor, PDO::PARAM_STR);
                        break;
                    case 'endereco_cliente_id':
                        $stmt->bindValue($identifier, $this->endereco_cliente_id, PDO::PARAM_INT);
                        break;
                    case 'data_contratado':
                        $stmt->bindValue($identifier, $this->data_contratado ? $this->data_contratado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_cancelado':
                        $stmt->bindValue($identifier, $this->data_cancelado ? $this->data_cancelado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'cancelado':
                        $stmt->bindValue($identifier, (int) $this->cancelado, PDO::PARAM_INT);
                        break;
                    case 'cliente_id':
                        $stmt->bindValue($identifier, $this->cliente_id, PDO::PARAM_INT);
                        break;
                    case 'forma_pagamento_id':
                        $stmt->bindValue($identifier, $this->forma_pagamento_id, PDO::PARAM_INT);
                        break;
                    case 'profile_pagamento_id':
                        $stmt->bindValue($identifier, $this->profile_pagamento_id, PDO::PARAM_INT);
                        break;
                    case 'vendedor_id':
                        $stmt->bindValue($identifier, $this->vendedor_id, PDO::PARAM_INT);
                        break;
                    case 'renovado_ate':
                        $stmt->bindValue($identifier, $this->renovado_ate ? $this->renovado_ate->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'primeiro_vencimento':
                        $stmt->bindValue($identifier, $this->primeiro_vencimento ? $this->primeiro_vencimento->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'percentual_multa':
                        $stmt->bindValue($identifier, $this->percentual_multa, PDO::PARAM_INT);
                        break;
                    case 'percentual_acrescimo':
                        $stmt->bindValue($identifier, $this->percentual_acrescimo, PDO::PARAM_INT);
                        break;
                    case 'usuario_cancelamento':
                        $stmt->bindValue($identifier, $this->usuario_cancelamento, PDO::PARAM_INT);
                        break;
                    case 'motivo_cancelamento':
                        $stmt->bindValue($identifier, $this->motivo_cancelamento, PDO::PARAM_INT);
                        break;
                    case 'agencia_debito':
                        $stmt->bindValue($identifier, $this->agencia_debito, PDO::PARAM_STR);
                        break;
                    case 'id_cliente_banco':
                        $stmt->bindValue($identifier, $this->id_cliente_banco, PDO::PARAM_STR);
                        break;
                    case 'emissao_nf_21':
                        $stmt->bindValue($identifier, (int) $this->emissao_nf_21, PDO::PARAM_INT);
                        break;
                    case 'emissao_nf_pre':
                        $stmt->bindValue($identifier, (int) $this->emissao_nf_pre, PDO::PARAM_INT);
                        break;
                    case 'ignora_renovacao_automatica':
                        $stmt->bindValue($identifier, (int) $this->ignora_renovacao_automatica, PDO::PARAM_INT);
                        break;
                    case 'comodato':
                        $stmt->bindValue($identifier, $this->comodato, PDO::PARAM_INT);
                        break;
                    case 'empresa_id':
                        $stmt->bindValue($identifier, $this->empresa_id, PDO::PARAM_INT);
                        break;
                    case 'descricao_comodato':
                        $stmt->bindValue($identifier, $this->descricao_comodato, PDO::PARAM_STR);
                        break;
                    case 'data_comodato':
                        $stmt->bindValue($identifier, $this->data_comodato ? $this->data_comodato->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'obs_comodato':
                        $stmt->bindValue($identifier, $this->obs_comodato, PDO::PARAM_STR);
                        break;
                    case 'centrocusto':
                        $stmt->bindValue($identifier, $this->centrocusto, PDO::PARAM_INT);
                        break;
                    case 'unidade_negocio':
                        $stmt->bindValue($identifier, $this->unidade_negocio, PDO::PARAM_INT);
                        break;
                    case 'valor_renovacao':
                        $stmt->bindValue($identifier, $this->valor_renovacao, PDO::PARAM_STR);
                        break;
                    case 'modelo_nf':
                        $stmt->bindValue($identifier, $this->modelo_nf, PDO::PARAM_INT);
                        break;
                    case 'emissao_nfse':
                        $stmt->bindValue($identifier, (int) $this->emissao_nfse, PDO::PARAM_INT);
                        break;
                    case 'suspenso':
                        $stmt->bindValue($identifier, (int) $this->suspenso, PDO::PARAM_INT);
                        break;
                    case 'data_suspensao':
                        $stmt->bindValue($identifier, $this->data_suspensao ? $this->data_suspensao->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_suspensao':
                        $stmt->bindValue($identifier, $this->usuario_suspensao, PDO::PARAM_INT);
                        break;
                    case 'motivo_suspensao':
                        $stmt->bindValue($identifier, $this->motivo_suspensao, PDO::PARAM_INT);
                        break;
                    case 'versao':
                        $stmt->bindValue($identifier, $this->versao, PDO::PARAM_INT);
                        break;
                    case 'faturamento_automatico':
                        $stmt->bindValue($identifier, (int) $this->faturamento_automatico, PDO::PARAM_INT);
                        break;
                    case 'regra_faturamento_id':
                        $stmt->bindValue($identifier, $this->regra_faturamento_id, PDO::PARAM_INT);
                        break;
                    case 'endereco_nf_id':
                        $stmt->bindValue($identifier, $this->endereco_nf_id, PDO::PARAM_INT);
                        break;
                    case 'endereco_nfse_id':
                        $stmt->bindValue($identifier, $this->endereco_nfse_id, PDO::PARAM_INT);
                        break;
                    case 'obs_nf':
                        $stmt->bindValue($identifier, $this->obs_nf, PDO::PARAM_STR);
                        break;
                    case 'tipo_faturamento_id':
                        $stmt->bindValue($identifier, $this->tipo_faturamento_id, PDO::PARAM_INT);
                        break;
                    case 'tem_scm':
                        $stmt->bindValue($identifier, (int) $this->tem_scm, PDO::PARAM_INT);
                        break;
                    case 'scm':
                        $stmt->bindValue($identifier, $this->scm, PDO::PARAM_STR);
                        break;
                    case 'tem_sva':
                        $stmt->bindValue($identifier, (int) $this->tem_sva, PDO::PARAM_INT);
                        break;
                    case 'sva':
                        $stmt->bindValue($identifier, $this->sva, PDO::PARAM_STR);
                        break;
                    case 'forcar_valor':
                        $stmt->bindValue($identifier, (int) $this->forcar_valor, PDO::PARAM_INT);
                        break;
                    case 'descricao_scm':
                        $stmt->bindValue($identifier, $this->descricao_scm, PDO::PARAM_STR);
                        break;
                    case 'descricao_sva':
                        $stmt->bindValue($identifier, $this->descricao_sva, PDO::PARAM_STR);
                        break;
                    case 'tipo_scm':
                        $stmt->bindValue($identifier, $this->tipo_scm, PDO::PARAM_STR);
                        break;
                    case 'tipo_sva':
                        $stmt->bindValue($identifier, $this->tipo_sva, PDO::PARAM_STR);
                        break;
                    case 'gerar_cobranca':
                        $stmt->bindValue($identifier, (int) $this->gerar_cobranca, PDO::PARAM_INT);
                        break;
                    case 'codigo_obra':
                        $stmt->bindValue($identifier, $this->codigo_obra, PDO::PARAM_STR);
                        break;
                    case 'art':
                        $stmt->bindValue($identifier, $this->art, PDO::PARAM_STR);
                        break;
                    case 'tem_reembolso':
                        $stmt->bindValue($identifier, (int) $this->tem_reembolso, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdcontrato($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContratoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdcontrato();
                break;
            case 1:
                return $this->getDescricao();
                break;
            case 2:
                return $this->getValor();
                break;
            case 3:
                return $this->getEnderecoClienteId();
                break;
            case 4:
                return $this->getDataContratado();
                break;
            case 5:
                return $this->getDataCancelado();
                break;
            case 6:
                return $this->getDataCadastro();
                break;
            case 7:
                return $this->getDataAlterado();
                break;
            case 8:
                return $this->getUsuarioAlterado();
                break;
            case 9:
                return $this->getCancelado();
                break;
            case 10:
                return $this->getClienteId();
                break;
            case 11:
                return $this->getFormaPagamentoId();
                break;
            case 12:
                return $this->getProfilePagamentoId();
                break;
            case 13:
                return $this->getVendedorId();
                break;
            case 14:
                return $this->getRenovadoAte();
                break;
            case 15:
                return $this->getPrimeiroVencimento();
                break;
            case 16:
                return $this->getPercentualMulta();
                break;
            case 17:
                return $this->getPercentualAcrescimo();
                break;
            case 18:
                return $this->getUsuarioCancelamento();
                break;
            case 19:
                return $this->getMotivoCancelamento();
                break;
            case 20:
                return $this->getAgenciaDebito();
                break;
            case 21:
                return $this->getIdClienteBanco();
                break;
            case 22:
                return $this->getEmissaoNf21();
                break;
            case 23:
                return $this->getEmissaoNfPre();
                break;
            case 24:
                return $this->getIgnoraRenovacaoAutomatica();
                break;
            case 25:
                return $this->getComodato();
                break;
            case 26:
                return $this->getEmpresaId();
                break;
            case 27:
                return $this->getDescricaoComodato();
                break;
            case 28:
                return $this->getDataComodato();
                break;
            case 29:
                return $this->getObsComodato();
                break;
            case 30:
                return $this->getCentrocusto();
                break;
            case 31:
                return $this->getUnidadeNegocio();
                break;
            case 32:
                return $this->getValorRenovacao();
                break;
            case 33:
                return $this->getModeloNf();
                break;
            case 34:
                return $this->getEmissaoNfse();
                break;
            case 35:
                return $this->getSuspenso();
                break;
            case 36:
                return $this->getDataSuspensao();
                break;
            case 37:
                return $this->getUsuarioSuspensao();
                break;
            case 38:
                return $this->getMotivoSuspensao();
                break;
            case 39:
                return $this->getVersao();
                break;
            case 40:
                return $this->getFaturamentoAutomatico();
                break;
            case 41:
                return $this->getRegraFaturamentoId();
                break;
            case 42:
                return $this->getEnderecoNfId();
                break;
            case 43:
                return $this->getEnderecoNfseId();
                break;
            case 44:
                return $this->getObsNf();
                break;
            case 45:
                return $this->getTipoFaturamentoId();
                break;
            case 46:
                return $this->getTemScm();
                break;
            case 47:
                return $this->getScm();
                break;
            case 48:
                return $this->getTemSva();
                break;
            case 49:
                return $this->getSva();
                break;
            case 50:
                return $this->getForcarValor();
                break;
            case 51:
                return $this->getDescricaoScm();
                break;
            case 52:
                return $this->getDescricaoSva();
                break;
            case 53:
                return $this->getTipoScm();
                break;
            case 54:
                return $this->getTipoSva();
                break;
            case 55:
                return $this->getGerarCobranca();
                break;
            case 56:
                return $this->getCodigoObra();
                break;
            case 57:
                return $this->getArt();
                break;
            case 58:
                return $this->getTemReembolso();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Contrato'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Contrato'][$this->hashCode()] = true;
        $keys = ContratoTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdcontrato(),
            $keys[1] => $this->getDescricao(),
            $keys[2] => $this->getValor(),
            $keys[3] => $this->getEnderecoClienteId(),
            $keys[4] => $this->getDataContratado(),
            $keys[5] => $this->getDataCancelado(),
            $keys[6] => $this->getDataCadastro(),
            $keys[7] => $this->getDataAlterado(),
            $keys[8] => $this->getUsuarioAlterado(),
            $keys[9] => $this->getCancelado(),
            $keys[10] => $this->getClienteId(),
            $keys[11] => $this->getFormaPagamentoId(),
            $keys[12] => $this->getProfilePagamentoId(),
            $keys[13] => $this->getVendedorId(),
            $keys[14] => $this->getRenovadoAte(),
            $keys[15] => $this->getPrimeiroVencimento(),
            $keys[16] => $this->getPercentualMulta(),
            $keys[17] => $this->getPercentualAcrescimo(),
            $keys[18] => $this->getUsuarioCancelamento(),
            $keys[19] => $this->getMotivoCancelamento(),
            $keys[20] => $this->getAgenciaDebito(),
            $keys[21] => $this->getIdClienteBanco(),
            $keys[22] => $this->getEmissaoNf21(),
            $keys[23] => $this->getEmissaoNfPre(),
            $keys[24] => $this->getIgnoraRenovacaoAutomatica(),
            $keys[25] => $this->getComodato(),
            $keys[26] => $this->getEmpresaId(),
            $keys[27] => $this->getDescricaoComodato(),
            $keys[28] => $this->getDataComodato(),
            $keys[29] => $this->getObsComodato(),
            $keys[30] => $this->getCentrocusto(),
            $keys[31] => $this->getUnidadeNegocio(),
            $keys[32] => $this->getValorRenovacao(),
            $keys[33] => $this->getModeloNf(),
            $keys[34] => $this->getEmissaoNfse(),
            $keys[35] => $this->getSuspenso(),
            $keys[36] => $this->getDataSuspensao(),
            $keys[37] => $this->getUsuarioSuspensao(),
            $keys[38] => $this->getMotivoSuspensao(),
            $keys[39] => $this->getVersao(),
            $keys[40] => $this->getFaturamentoAutomatico(),
            $keys[41] => $this->getRegraFaturamentoId(),
            $keys[42] => $this->getEnderecoNfId(),
            $keys[43] => $this->getEnderecoNfseId(),
            $keys[44] => $this->getObsNf(),
            $keys[45] => $this->getTipoFaturamentoId(),
            $keys[46] => $this->getTemScm(),
            $keys[47] => $this->getScm(),
            $keys[48] => $this->getTemSva(),
            $keys[49] => $this->getSva(),
            $keys[50] => $this->getForcarValor(),
            $keys[51] => $this->getDescricaoScm(),
            $keys[52] => $this->getDescricaoSva(),
            $keys[53] => $this->getTipoScm(),
            $keys[54] => $this->getTipoSva(),
            $keys[55] => $this->getGerarCobranca(),
            $keys[56] => $this->getCodigoObra(),
            $keys[57] => $this->getArt(),
            $keys[58] => $this->getTemReembolso(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTime) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        if ($result[$keys[28]] instanceof \DateTime) {
            $result[$keys[28]] = $result[$keys[28]]->format('c');
        }

        if ($result[$keys[36]] instanceof \DateTime) {
            $result[$keys[36]] = $result[$keys[36]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEnderecoCliente) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'enderecoCliente';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'endereco_cliente';
                        break;
                    default:
                        $key = 'EnderecoCliente';
                }

                $result[$key] = $this->aEnderecoCliente->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aRegraFaturamento) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'regraFaturamento';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'regra_faturamento';
                        break;
                    default:
                        $key = 'RegraFaturamento';
                }

                $result[$key] = $this->aRegraFaturamento->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collServicoClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicoClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'servico_clientes';
                        break;
                    default:
                        $key = 'ServicoClientes';
                }

                $result[$key] = $this->collServicoClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Contrato
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContratoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Contrato
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdcontrato($value);
                break;
            case 1:
                $this->setDescricao($value);
                break;
            case 2:
                $this->setValor($value);
                break;
            case 3:
                $this->setEnderecoClienteId($value);
                break;
            case 4:
                $this->setDataContratado($value);
                break;
            case 5:
                $this->setDataCancelado($value);
                break;
            case 6:
                $this->setDataCadastro($value);
                break;
            case 7:
                $this->setDataAlterado($value);
                break;
            case 8:
                $this->setUsuarioAlterado($value);
                break;
            case 9:
                $this->setCancelado($value);
                break;
            case 10:
                $this->setClienteId($value);
                break;
            case 11:
                $this->setFormaPagamentoId($value);
                break;
            case 12:
                $this->setProfilePagamentoId($value);
                break;
            case 13:
                $this->setVendedorId($value);
                break;
            case 14:
                $this->setRenovadoAte($value);
                break;
            case 15:
                $this->setPrimeiroVencimento($value);
                break;
            case 16:
                $this->setPercentualMulta($value);
                break;
            case 17:
                $this->setPercentualAcrescimo($value);
                break;
            case 18:
                $this->setUsuarioCancelamento($value);
                break;
            case 19:
                $this->setMotivoCancelamento($value);
                break;
            case 20:
                $this->setAgenciaDebito($value);
                break;
            case 21:
                $this->setIdClienteBanco($value);
                break;
            case 22:
                $this->setEmissaoNf21($value);
                break;
            case 23:
                $this->setEmissaoNfPre($value);
                break;
            case 24:
                $this->setIgnoraRenovacaoAutomatica($value);
                break;
            case 25:
                $this->setComodato($value);
                break;
            case 26:
                $this->setEmpresaId($value);
                break;
            case 27:
                $this->setDescricaoComodato($value);
                break;
            case 28:
                $this->setDataComodato($value);
                break;
            case 29:
                $this->setObsComodato($value);
                break;
            case 30:
                $this->setCentrocusto($value);
                break;
            case 31:
                $this->setUnidadeNegocio($value);
                break;
            case 32:
                $this->setValorRenovacao($value);
                break;
            case 33:
                $this->setModeloNf($value);
                break;
            case 34:
                $this->setEmissaoNfse($value);
                break;
            case 35:
                $this->setSuspenso($value);
                break;
            case 36:
                $this->setDataSuspensao($value);
                break;
            case 37:
                $this->setUsuarioSuspensao($value);
                break;
            case 38:
                $this->setMotivoSuspensao($value);
                break;
            case 39:
                $this->setVersao($value);
                break;
            case 40:
                $this->setFaturamentoAutomatico($value);
                break;
            case 41:
                $this->setRegraFaturamentoId($value);
                break;
            case 42:
                $this->setEnderecoNfId($value);
                break;
            case 43:
                $this->setEnderecoNfseId($value);
                break;
            case 44:
                $this->setObsNf($value);
                break;
            case 45:
                $this->setTipoFaturamentoId($value);
                break;
            case 46:
                $this->setTemScm($value);
                break;
            case 47:
                $this->setScm($value);
                break;
            case 48:
                $this->setTemSva($value);
                break;
            case 49:
                $this->setSva($value);
                break;
            case 50:
                $this->setForcarValor($value);
                break;
            case 51:
                $this->setDescricaoScm($value);
                break;
            case 52:
                $this->setDescricaoSva($value);
                break;
            case 53:
                $this->setTipoScm($value);
                break;
            case 54:
                $this->setTipoSva($value);
                break;
            case 55:
                $this->setGerarCobranca($value);
                break;
            case 56:
                $this->setCodigoObra($value);
                break;
            case 57:
                $this->setArt($value);
                break;
            case 58:
                $this->setTemReembolso($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ContratoTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdcontrato($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDescricao($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setValor($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setEnderecoClienteId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDataContratado($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDataCancelado($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDataCadastro($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDataAlterado($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setCancelado($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setClienteId($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setFormaPagamentoId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setProfilePagamentoId($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setVendedorId($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setRenovadoAte($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setPrimeiroVencimento($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setPercentualMulta($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setPercentualAcrescimo($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setUsuarioCancelamento($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setMotivoCancelamento($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setAgenciaDebito($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setIdClienteBanco($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setEmissaoNf21($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setEmissaoNfPre($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setIgnoraRenovacaoAutomatica($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setComodato($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setEmpresaId($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setDescricaoComodato($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setDataComodato($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setObsComodato($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setCentrocusto($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setUnidadeNegocio($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setValorRenovacao($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setModeloNf($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setEmissaoNfse($arr[$keys[34]]);
        }
        if (array_key_exists($keys[35], $arr)) {
            $this->setSuspenso($arr[$keys[35]]);
        }
        if (array_key_exists($keys[36], $arr)) {
            $this->setDataSuspensao($arr[$keys[36]]);
        }
        if (array_key_exists($keys[37], $arr)) {
            $this->setUsuarioSuspensao($arr[$keys[37]]);
        }
        if (array_key_exists($keys[38], $arr)) {
            $this->setMotivoSuspensao($arr[$keys[38]]);
        }
        if (array_key_exists($keys[39], $arr)) {
            $this->setVersao($arr[$keys[39]]);
        }
        if (array_key_exists($keys[40], $arr)) {
            $this->setFaturamentoAutomatico($arr[$keys[40]]);
        }
        if (array_key_exists($keys[41], $arr)) {
            $this->setRegraFaturamentoId($arr[$keys[41]]);
        }
        if (array_key_exists($keys[42], $arr)) {
            $this->setEnderecoNfId($arr[$keys[42]]);
        }
        if (array_key_exists($keys[43], $arr)) {
            $this->setEnderecoNfseId($arr[$keys[43]]);
        }
        if (array_key_exists($keys[44], $arr)) {
            $this->setObsNf($arr[$keys[44]]);
        }
        if (array_key_exists($keys[45], $arr)) {
            $this->setTipoFaturamentoId($arr[$keys[45]]);
        }
        if (array_key_exists($keys[46], $arr)) {
            $this->setTemScm($arr[$keys[46]]);
        }
        if (array_key_exists($keys[47], $arr)) {
            $this->setScm($arr[$keys[47]]);
        }
        if (array_key_exists($keys[48], $arr)) {
            $this->setTemSva($arr[$keys[48]]);
        }
        if (array_key_exists($keys[49], $arr)) {
            $this->setSva($arr[$keys[49]]);
        }
        if (array_key_exists($keys[50], $arr)) {
            $this->setForcarValor($arr[$keys[50]]);
        }
        if (array_key_exists($keys[51], $arr)) {
            $this->setDescricaoScm($arr[$keys[51]]);
        }
        if (array_key_exists($keys[52], $arr)) {
            $this->setDescricaoSva($arr[$keys[52]]);
        }
        if (array_key_exists($keys[53], $arr)) {
            $this->setTipoScm($arr[$keys[53]]);
        }
        if (array_key_exists($keys[54], $arr)) {
            $this->setTipoSva($arr[$keys[54]]);
        }
        if (array_key_exists($keys[55], $arr)) {
            $this->setGerarCobranca($arr[$keys[55]]);
        }
        if (array_key_exists($keys[56], $arr)) {
            $this->setCodigoObra($arr[$keys[56]]);
        }
        if (array_key_exists($keys[57], $arr)) {
            $this->setArt($arr[$keys[57]]);
        }
        if (array_key_exists($keys[58], $arr)) {
            $this->setTemReembolso($arr[$keys[58]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ContratoTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ContratoTableMap::COL_IDCONTRATO)) {
            $criteria->add(ContratoTableMap::COL_IDCONTRATO, $this->idcontrato);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DESCRICAO)) {
            $criteria->add(ContratoTableMap::COL_DESCRICAO, $this->descricao);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_VALOR)) {
            $criteria->add(ContratoTableMap::COL_VALOR, $this->valor);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ENDERECO_CLIENTE_ID)) {
            $criteria->add(ContratoTableMap::COL_ENDERECO_CLIENTE_ID, $this->endereco_cliente_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_CONTRATADO)) {
            $criteria->add(ContratoTableMap::COL_DATA_CONTRATADO, $this->data_contratado);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_CANCELADO)) {
            $criteria->add(ContratoTableMap::COL_DATA_CANCELADO, $this->data_cancelado);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(ContratoTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(ContratoTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(ContratoTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_CANCELADO)) {
            $criteria->add(ContratoTableMap::COL_CANCELADO, $this->cancelado);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_CLIENTE_ID)) {
            $criteria->add(ContratoTableMap::COL_CLIENTE_ID, $this->cliente_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_FORMA_PAGAMENTO_ID)) {
            $criteria->add(ContratoTableMap::COL_FORMA_PAGAMENTO_ID, $this->forma_pagamento_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_PROFILE_PAGAMENTO_ID)) {
            $criteria->add(ContratoTableMap::COL_PROFILE_PAGAMENTO_ID, $this->profile_pagamento_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_VENDEDOR_ID)) {
            $criteria->add(ContratoTableMap::COL_VENDEDOR_ID, $this->vendedor_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_RENOVADO_ATE)) {
            $criteria->add(ContratoTableMap::COL_RENOVADO_ATE, $this->renovado_ate);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_PRIMEIRO_VENCIMENTO)) {
            $criteria->add(ContratoTableMap::COL_PRIMEIRO_VENCIMENTO, $this->primeiro_vencimento);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_PERCENTUAL_MULTA)) {
            $criteria->add(ContratoTableMap::COL_PERCENTUAL_MULTA, $this->percentual_multa);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_PERCENTUAL_ACRESCIMO)) {
            $criteria->add(ContratoTableMap::COL_PERCENTUAL_ACRESCIMO, $this->percentual_acrescimo);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_USUARIO_CANCELAMENTO)) {
            $criteria->add(ContratoTableMap::COL_USUARIO_CANCELAMENTO, $this->usuario_cancelamento);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_MOTIVO_CANCELAMENTO)) {
            $criteria->add(ContratoTableMap::COL_MOTIVO_CANCELAMENTO, $this->motivo_cancelamento);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_AGENCIA_DEBITO)) {
            $criteria->add(ContratoTableMap::COL_AGENCIA_DEBITO, $this->agencia_debito);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ID_CLIENTE_BANCO)) {
            $criteria->add(ContratoTableMap::COL_ID_CLIENTE_BANCO, $this->id_cliente_banco);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_EMISSAO_NF_21)) {
            $criteria->add(ContratoTableMap::COL_EMISSAO_NF_21, $this->emissao_nf_21);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_EMISSAO_NF_PRE)) {
            $criteria->add(ContratoTableMap::COL_EMISSAO_NF_PRE, $this->emissao_nf_pre);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_IGNORA_RENOVACAO_AUTOMATICA)) {
            $criteria->add(ContratoTableMap::COL_IGNORA_RENOVACAO_AUTOMATICA, $this->ignora_renovacao_automatica);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_COMODATO)) {
            $criteria->add(ContratoTableMap::COL_COMODATO, $this->comodato);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_EMPRESA_ID)) {
            $criteria->add(ContratoTableMap::COL_EMPRESA_ID, $this->empresa_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DESCRICAO_COMODATO)) {
            $criteria->add(ContratoTableMap::COL_DESCRICAO_COMODATO, $this->descricao_comodato);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_COMODATO)) {
            $criteria->add(ContratoTableMap::COL_DATA_COMODATO, $this->data_comodato);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_OBS_COMODATO)) {
            $criteria->add(ContratoTableMap::COL_OBS_COMODATO, $this->obs_comodato);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_CENTROCUSTO)) {
            $criteria->add(ContratoTableMap::COL_CENTROCUSTO, $this->centrocusto);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_UNIDADE_NEGOCIO)) {
            $criteria->add(ContratoTableMap::COL_UNIDADE_NEGOCIO, $this->unidade_negocio);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_VALOR_RENOVACAO)) {
            $criteria->add(ContratoTableMap::COL_VALOR_RENOVACAO, $this->valor_renovacao);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_MODELO_NF)) {
            $criteria->add(ContratoTableMap::COL_MODELO_NF, $this->modelo_nf);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_EMISSAO_NFSE)) {
            $criteria->add(ContratoTableMap::COL_EMISSAO_NFSE, $this->emissao_nfse);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_SUSPENSO)) {
            $criteria->add(ContratoTableMap::COL_SUSPENSO, $this->suspenso);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DATA_SUSPENSAO)) {
            $criteria->add(ContratoTableMap::COL_DATA_SUSPENSAO, $this->data_suspensao);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_USUARIO_SUSPENSAO)) {
            $criteria->add(ContratoTableMap::COL_USUARIO_SUSPENSAO, $this->usuario_suspensao);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_MOTIVO_SUSPENSAO)) {
            $criteria->add(ContratoTableMap::COL_MOTIVO_SUSPENSAO, $this->motivo_suspensao);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_VERSAO)) {
            $criteria->add(ContratoTableMap::COL_VERSAO, $this->versao);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_FATURAMENTO_AUTOMATICO)) {
            $criteria->add(ContratoTableMap::COL_FATURAMENTO_AUTOMATICO, $this->faturamento_automatico);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_REGRA_FATURAMENTO_ID)) {
            $criteria->add(ContratoTableMap::COL_REGRA_FATURAMENTO_ID, $this->regra_faturamento_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ENDERECO_NF_ID)) {
            $criteria->add(ContratoTableMap::COL_ENDERECO_NF_ID, $this->endereco_nf_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ENDERECO_NFSE_ID)) {
            $criteria->add(ContratoTableMap::COL_ENDERECO_NFSE_ID, $this->endereco_nfse_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_OBS_NF)) {
            $criteria->add(ContratoTableMap::COL_OBS_NF, $this->obs_nf);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TIPO_FATURAMENTO_ID)) {
            $criteria->add(ContratoTableMap::COL_TIPO_FATURAMENTO_ID, $this->tipo_faturamento_id);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TEM_SCM)) {
            $criteria->add(ContratoTableMap::COL_TEM_SCM, $this->tem_scm);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_SCM)) {
            $criteria->add(ContratoTableMap::COL_SCM, $this->scm);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TEM_SVA)) {
            $criteria->add(ContratoTableMap::COL_TEM_SVA, $this->tem_sva);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_SVA)) {
            $criteria->add(ContratoTableMap::COL_SVA, $this->sva);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_FORCAR_VALOR)) {
            $criteria->add(ContratoTableMap::COL_FORCAR_VALOR, $this->forcar_valor);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DESCRICAO_SCM)) {
            $criteria->add(ContratoTableMap::COL_DESCRICAO_SCM, $this->descricao_scm);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_DESCRICAO_SVA)) {
            $criteria->add(ContratoTableMap::COL_DESCRICAO_SVA, $this->descricao_sva);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TIPO_SCM)) {
            $criteria->add(ContratoTableMap::COL_TIPO_SCM, $this->tipo_scm);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TIPO_SVA)) {
            $criteria->add(ContratoTableMap::COL_TIPO_SVA, $this->tipo_sva);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_GERAR_COBRANCA)) {
            $criteria->add(ContratoTableMap::COL_GERAR_COBRANCA, $this->gerar_cobranca);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_CODIGO_OBRA)) {
            $criteria->add(ContratoTableMap::COL_CODIGO_OBRA, $this->codigo_obra);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_ART)) {
            $criteria->add(ContratoTableMap::COL_ART, $this->art);
        }
        if ($this->isColumnModified(ContratoTableMap::COL_TEM_REEMBOLSO)) {
            $criteria->add(ContratoTableMap::COL_TEM_REEMBOLSO, $this->tem_reembolso);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildContratoQuery::create();
        $criteria->add(ContratoTableMap::COL_IDCONTRATO, $this->idcontrato);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdcontrato();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdcontrato();
    }

    /**
     * Generic method to set the primary key (idcontrato column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdcontrato($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdcontrato();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Contrato (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDescricao($this->getDescricao());
        $copyObj->setValor($this->getValor());
        $copyObj->setEnderecoClienteId($this->getEnderecoClienteId());
        $copyObj->setDataContratado($this->getDataContratado());
        $copyObj->setDataCancelado($this->getDataCancelado());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setCancelado($this->getCancelado());
        $copyObj->setClienteId($this->getClienteId());
        $copyObj->setFormaPagamentoId($this->getFormaPagamentoId());
        $copyObj->setProfilePagamentoId($this->getProfilePagamentoId());
        $copyObj->setVendedorId($this->getVendedorId());
        $copyObj->setRenovadoAte($this->getRenovadoAte());
        $copyObj->setPrimeiroVencimento($this->getPrimeiroVencimento());
        $copyObj->setPercentualMulta($this->getPercentualMulta());
        $copyObj->setPercentualAcrescimo($this->getPercentualAcrescimo());
        $copyObj->setUsuarioCancelamento($this->getUsuarioCancelamento());
        $copyObj->setMotivoCancelamento($this->getMotivoCancelamento());
        $copyObj->setAgenciaDebito($this->getAgenciaDebito());
        $copyObj->setIdClienteBanco($this->getIdClienteBanco());
        $copyObj->setEmissaoNf21($this->getEmissaoNf21());
        $copyObj->setEmissaoNfPre($this->getEmissaoNfPre());
        $copyObj->setIgnoraRenovacaoAutomatica($this->getIgnoraRenovacaoAutomatica());
        $copyObj->setComodato($this->getComodato());
        $copyObj->setEmpresaId($this->getEmpresaId());
        $copyObj->setDescricaoComodato($this->getDescricaoComodato());
        $copyObj->setDataComodato($this->getDataComodato());
        $copyObj->setObsComodato($this->getObsComodato());
        $copyObj->setCentrocusto($this->getCentrocusto());
        $copyObj->setUnidadeNegocio($this->getUnidadeNegocio());
        $copyObj->setValorRenovacao($this->getValorRenovacao());
        $copyObj->setModeloNf($this->getModeloNf());
        $copyObj->setEmissaoNfse($this->getEmissaoNfse());
        $copyObj->setSuspenso($this->getSuspenso());
        $copyObj->setDataSuspensao($this->getDataSuspensao());
        $copyObj->setUsuarioSuspensao($this->getUsuarioSuspensao());
        $copyObj->setMotivoSuspensao($this->getMotivoSuspensao());
        $copyObj->setVersao($this->getVersao());
        $copyObj->setFaturamentoAutomatico($this->getFaturamentoAutomatico());
        $copyObj->setRegraFaturamentoId($this->getRegraFaturamentoId());
        $copyObj->setEnderecoNfId($this->getEnderecoNfId());
        $copyObj->setEnderecoNfseId($this->getEnderecoNfseId());
        $copyObj->setObsNf($this->getObsNf());
        $copyObj->setTipoFaturamentoId($this->getTipoFaturamentoId());
        $copyObj->setTemScm($this->getTemScm());
        $copyObj->setScm($this->getScm());
        $copyObj->setTemSva($this->getTemSva());
        $copyObj->setSva($this->getSva());
        $copyObj->setForcarValor($this->getForcarValor());
        $copyObj->setDescricaoScm($this->getDescricaoScm());
        $copyObj->setDescricaoSva($this->getDescricaoSva());
        $copyObj->setTipoScm($this->getTipoScm());
        $copyObj->setTipoSva($this->getTipoSva());
        $copyObj->setGerarCobranca($this->getGerarCobranca());
        $copyObj->setCodigoObra($this->getCodigoObra());
        $copyObj->setArt($this->getArt());
        $copyObj->setTemReembolso($this->getTemReembolso());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getServicoClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicoCliente($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdcontrato(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Contrato Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEnderecoCliente object.
     *
     * @param  ChildEnderecoCliente $v
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEnderecoCliente(ChildEnderecoCliente $v = null)
    {
        if ($v === null) {
            $this->setEnderecoClienteId(NULL);
        } else {
            $this->setEnderecoClienteId($v->getIdenderecoCliente());
        }

        $this->aEnderecoCliente = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEnderecoCliente object, it will not be re-added.
        if ($v !== null) {
            $v->addContrato($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEnderecoCliente object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEnderecoCliente The associated ChildEnderecoCliente object.
     * @throws PropelException
     */
    public function getEnderecoCliente(ConnectionInterface $con = null)
    {
        if ($this->aEnderecoCliente === null && ($this->endereco_cliente_id !== null)) {
            $this->aEnderecoCliente = ChildEnderecoClienteQuery::create()->findPk($this->endereco_cliente_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEnderecoCliente->addContratos($this);
             */
        }

        return $this->aEnderecoCliente;
    }

    /**
     * Declares an association between this object and a ChildRegraFaturamento object.
     *
     * @param  ChildRegraFaturamento $v
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRegraFaturamento(ChildRegraFaturamento $v = null)
    {
        if ($v === null) {
            $this->setRegraFaturamentoId(NULL);
        } else {
            $this->setRegraFaturamentoId($v->getIdregraFaturamento());
        }

        $this->aRegraFaturamento = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildRegraFaturamento object, it will not be re-added.
        if ($v !== null) {
            $v->addContrato($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildRegraFaturamento object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildRegraFaturamento The associated ChildRegraFaturamento object.
     * @throws PropelException
     */
    public function getRegraFaturamento(ConnectionInterface $con = null)
    {
        if ($this->aRegraFaturamento === null && ($this->regra_faturamento_id !== null)) {
            $this->aRegraFaturamento = ChildRegraFaturamentoQuery::create()->findPk($this->regra_faturamento_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRegraFaturamento->addContratos($this);
             */
        }

        return $this->aRegraFaturamento;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ServicoCliente' == $relationName) {
            return $this->initServicoClientes();
        }
    }

    /**
     * Clears out the collServicoClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicoClientes()
     */
    public function clearServicoClientes()
    {
        $this->collServicoClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicoClientes collection loaded partially.
     */
    public function resetPartialServicoClientes($v = true)
    {
        $this->collServicoClientesPartial = $v;
    }

    /**
     * Initializes the collServicoClientes collection.
     *
     * By default this just sets the collServicoClientes collection to an empty array (like clearcollServicoClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicoClientes($overrideExisting = true)
    {
        if (null !== $this->collServicoClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicoClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collServicoClientes = new $collectionClassName;
        $this->collServicoClientes->setModel('\ImaTelecomBundle\Model\ServicoCliente');
    }

    /**
     * Gets an array of ChildServicoCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildContrato is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     * @throws PropelException
     */
    public function getServicoClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoClientesPartial && !$this->isNew();
        if (null === $this->collServicoClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicoClientes) {
                // return empty collection
                $this->initServicoClientes();
            } else {
                $collServicoClientes = ChildServicoClienteQuery::create(null, $criteria)
                    ->filterByContrato($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicoClientesPartial && count($collServicoClientes)) {
                        $this->initServicoClientes(false);

                        foreach ($collServicoClientes as $obj) {
                            if (false == $this->collServicoClientes->contains($obj)) {
                                $this->collServicoClientes->append($obj);
                            }
                        }

                        $this->collServicoClientesPartial = true;
                    }

                    return $collServicoClientes;
                }

                if ($partial && $this->collServicoClientes) {
                    foreach ($this->collServicoClientes as $obj) {
                        if ($obj->isNew()) {
                            $collServicoClientes[] = $obj;
                        }
                    }
                }

                $this->collServicoClientes = $collServicoClientes;
                $this->collServicoClientesPartial = false;
            }
        }

        return $this->collServicoClientes;
    }

    /**
     * Sets a collection of ChildServicoCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicoClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildContrato The current object (for fluent API support)
     */
    public function setServicoClientes(Collection $servicoClientes, ConnectionInterface $con = null)
    {
        /** @var ChildServicoCliente[] $servicoClientesToDelete */
        $servicoClientesToDelete = $this->getServicoClientes(new Criteria(), $con)->diff($servicoClientes);


        $this->servicoClientesScheduledForDeletion = $servicoClientesToDelete;

        foreach ($servicoClientesToDelete as $servicoClienteRemoved) {
            $servicoClienteRemoved->setContrato(null);
        }

        $this->collServicoClientes = null;
        foreach ($servicoClientes as $servicoCliente) {
            $this->addServicoCliente($servicoCliente);
        }

        $this->collServicoClientes = $servicoClientes;
        $this->collServicoClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ServicoCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ServicoCliente objects.
     * @throws PropelException
     */
    public function countServicoClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoClientesPartial && !$this->isNew();
        if (null === $this->collServicoClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicoClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicoClientes());
            }

            $query = ChildServicoClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByContrato($this)
                ->count($con);
        }

        return count($this->collServicoClientes);
    }

    /**
     * Method called to associate a ChildServicoCliente object to this object
     * through the ChildServicoCliente foreign key attribute.
     *
     * @param  ChildServicoCliente $l ChildServicoCliente
     * @return $this|\ImaTelecomBundle\Model\Contrato The current object (for fluent API support)
     */
    public function addServicoCliente(ChildServicoCliente $l)
    {
        if ($this->collServicoClientes === null) {
            $this->initServicoClientes();
            $this->collServicoClientesPartial = true;
        }

        if (!$this->collServicoClientes->contains($l)) {
            $this->doAddServicoCliente($l);

            if ($this->servicoClientesScheduledForDeletion and $this->servicoClientesScheduledForDeletion->contains($l)) {
                $this->servicoClientesScheduledForDeletion->remove($this->servicoClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicoCliente $servicoCliente The ChildServicoCliente object to add.
     */
    protected function doAddServicoCliente(ChildServicoCliente $servicoCliente)
    {
        $this->collServicoClientes[]= $servicoCliente;
        $servicoCliente->setContrato($this);
    }

    /**
     * @param  ChildServicoCliente $servicoCliente The ChildServicoCliente object to remove.
     * @return $this|ChildContrato The current object (for fluent API support)
     */
    public function removeServicoCliente(ChildServicoCliente $servicoCliente)
    {
        if ($this->getServicoClientes()->contains($servicoCliente)) {
            $pos = $this->collServicoClientes->search($servicoCliente);
            $this->collServicoClientes->remove($pos);
            if (null === $this->servicoClientesScheduledForDeletion) {
                $this->servicoClientesScheduledForDeletion = clone $this->collServicoClientes;
                $this->servicoClientesScheduledForDeletion->clear();
            }
            $this->servicoClientesScheduledForDeletion[]= $servicoCliente;
            $servicoCliente->setContrato(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Contrato is new, it will return
     * an empty collection; or if this Contrato has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Contrato.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Contrato is new, it will return
     * an empty collection; or if this Contrato has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Contrato.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinServicoPrestado(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('ServicoPrestado', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Contrato is new, it will return
     * an empty collection; or if this Contrato has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Contrato.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEnderecoCliente) {
            $this->aEnderecoCliente->removeContrato($this);
        }
        if (null !== $this->aRegraFaturamento) {
            $this->aRegraFaturamento->removeContrato($this);
        }
        $this->idcontrato = null;
        $this->descricao = null;
        $this->valor = null;
        $this->endereco_cliente_id = null;
        $this->data_contratado = null;
        $this->data_cancelado = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->cancelado = null;
        $this->cliente_id = null;
        $this->forma_pagamento_id = null;
        $this->profile_pagamento_id = null;
        $this->vendedor_id = null;
        $this->renovado_ate = null;
        $this->primeiro_vencimento = null;
        $this->percentual_multa = null;
        $this->percentual_acrescimo = null;
        $this->usuario_cancelamento = null;
        $this->motivo_cancelamento = null;
        $this->agencia_debito = null;
        $this->id_cliente_banco = null;
        $this->emissao_nf_21 = null;
        $this->emissao_nf_pre = null;
        $this->ignora_renovacao_automatica = null;
        $this->comodato = null;
        $this->empresa_id = null;
        $this->descricao_comodato = null;
        $this->data_comodato = null;
        $this->obs_comodato = null;
        $this->centrocusto = null;
        $this->unidade_negocio = null;
        $this->valor_renovacao = null;
        $this->modelo_nf = null;
        $this->emissao_nfse = null;
        $this->suspenso = null;
        $this->data_suspensao = null;
        $this->usuario_suspensao = null;
        $this->motivo_suspensao = null;
        $this->versao = null;
        $this->faturamento_automatico = null;
        $this->regra_faturamento_id = null;
        $this->endereco_nf_id = null;
        $this->endereco_nfse_id = null;
        $this->obs_nf = null;
        $this->tipo_faturamento_id = null;
        $this->tem_scm = null;
        $this->scm = null;
        $this->tem_sva = null;
        $this->sva = null;
        $this->forcar_valor = null;
        $this->descricao_scm = null;
        $this->descricao_sva = null;
        $this->tipo_scm = null;
        $this->tipo_sva = null;
        $this->gerar_cobranca = null;
        $this->codigo_obra = null;
        $this->art = null;
        $this->tem_reembolso = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collServicoClientes) {
                foreach ($this->collServicoClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collServicoClientes = null;
        $this->aEnderecoCliente = null;
        $this->aRegraFaturamento = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ContratoTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
