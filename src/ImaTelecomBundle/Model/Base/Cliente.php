<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Cliente as ChildCliente;
use ImaTelecomBundle\Model\ClienteEstoqueItem as ChildClienteEstoqueItem;
use ImaTelecomBundle\Model\ClienteEstoqueItemQuery as ChildClienteEstoqueItemQuery;
use ImaTelecomBundle\Model\ClienteQuery as ChildClienteQuery;
use ImaTelecomBundle\Model\EnderecoCliente as ChildEnderecoCliente;
use ImaTelecomBundle\Model\EnderecoClienteQuery as ChildEnderecoClienteQuery;
use ImaTelecomBundle\Model\EstoqueLancamento as ChildEstoqueLancamento;
use ImaTelecomBundle\Model\EstoqueLancamentoQuery as ChildEstoqueLancamentoQuery;
use ImaTelecomBundle\Model\Fornecedor as ChildFornecedor;
use ImaTelecomBundle\Model\FornecedorQuery as ChildFornecedorQuery;
use ImaTelecomBundle\Model\Pessoa as ChildPessoa;
use ImaTelecomBundle\Model\PessoaQuery as ChildPessoaQuery;
use ImaTelecomBundle\Model\ServicoCliente as ChildServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery as ChildServicoClienteQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use ImaTelecomBundle\Model\Map\ClienteEstoqueItemTableMap;
use ImaTelecomBundle\Model\Map\ClienteTableMap;
use ImaTelecomBundle\Model\Map\EnderecoClienteTableMap;
use ImaTelecomBundle\Model\Map\EstoqueLancamentoTableMap;
use ImaTelecomBundle\Model\Map\FornecedorTableMap;
use ImaTelecomBundle\Model\Map\ServicoClienteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'cliente' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Cliente implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\ClienteTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idcliente field.
     *
     * @var        int
     */
    protected $idcliente;

    /**
     * The value for the pessoa_id field.
     *
     * @var        int
     */
    protected $pessoa_id;

    /**
     * The value for the ativo field.
     *
     * @var        boolean
     */
    protected $ativo;

    /**
     * The value for the data_contrato field.
     *
     * @var        DateTime
     */
    protected $data_contrato;

    /**
     * The value for the data_cancelado field.
     *
     * @var        DateTime
     */
    protected $data_cancelado;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the import_id field.
     *
     * @var        int
     */
    protected $import_id;

    /**
     * @var        ChildPessoa
     */
    protected $aPessoa;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildBoleto[] Collection to store aggregation of ChildBoleto objects.
     */
    protected $collBoletos;
    protected $collBoletosPartial;

    /**
     * @var        ObjectCollection|ChildClienteEstoqueItem[] Collection to store aggregation of ChildClienteEstoqueItem objects.
     */
    protected $collClienteEstoqueItems;
    protected $collClienteEstoqueItemsPartial;

    /**
     * @var        ObjectCollection|ChildEnderecoCliente[] Collection to store aggregation of ChildEnderecoCliente objects.
     */
    protected $collEnderecoClientes;
    protected $collEnderecoClientesPartial;

    /**
     * @var        ObjectCollection|ChildEstoqueLancamento[] Collection to store aggregation of ChildEstoqueLancamento objects.
     */
    protected $collEstoqueLancamentos;
    protected $collEstoqueLancamentosPartial;

    /**
     * @var        ObjectCollection|ChildFornecedor[] Collection to store aggregation of ChildFornecedor objects.
     */
    protected $collFornecedors;
    protected $collFornecedorsPartial;

    /**
     * @var        ObjectCollection|ChildServicoCliente[] Collection to store aggregation of ChildServicoCliente objects.
     */
    protected $collServicoClientes;
    protected $collServicoClientesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoleto[]
     */
    protected $boletosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildClienteEstoqueItem[]
     */
    protected $clienteEstoqueItemsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEnderecoCliente[]
     */
    protected $enderecoClientesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEstoqueLancamento[]
     */
    protected $estoqueLancamentosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFornecedor[]
     */
    protected $fornecedorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicoCliente[]
     */
    protected $servicoClientesScheduledForDeletion = null;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Cliente object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Cliente</code> instance.  If
     * <code>obj</code> is an instance of <code>Cliente</code>, delegates to
     * <code>equals(Cliente)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Cliente The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idcliente] column value.
     *
     * @return int
     */
    public function getIdcliente()
    {
        return $this->idcliente;
    }

    /**
     * Get the [pessoa_id] column value.
     *
     * @return int
     */
    public function getPessoaId()
    {
        return $this->pessoa_id;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->getAtivo();
    }

    /**
     * Get the [optionally formatted] temporal [data_contrato] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataContrato($format = NULL)
    {
        if ($format === null) {
            return $this->data_contrato;
        } else {
            return $this->data_contrato instanceof \DateTimeInterface ? $this->data_contrato->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_cancelado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCancelado($format = NULL)
    {
        if ($format === null) {
            return $this->data_cancelado;
        } else {
            return $this->data_cancelado instanceof \DateTimeInterface ? $this->data_cancelado->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [import_id] column value.
     *
     * @return int
     */
    public function getImportId()
    {
        return $this->import_id;
    }

    /**
     * Set the value of [idcliente] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function setIdcliente($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idcliente !== $v) {
            $this->idcliente = $v;
            $this->modifiedColumns[ClienteTableMap::COL_IDCLIENTE] = true;
        }

        return $this;
    } // setIdcliente()

    /**
     * Set the value of [pessoa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function setPessoaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pessoa_id !== $v) {
            $this->pessoa_id = $v;
            $this->modifiedColumns[ClienteTableMap::COL_PESSOA_ID] = true;
        }

        if ($this->aPessoa !== null && $this->aPessoa->getId() !== $v) {
            $this->aPessoa = null;
        }

        return $this;
    } // setPessoaId()

    /**
     * Sets the value of the [ativo] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function setAtivo($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ativo !== $v) {
            $this->ativo = $v;
            $this->modifiedColumns[ClienteTableMap::COL_ATIVO] = true;
        }

        return $this;
    } // setAtivo()

    /**
     * Sets the value of [data_contrato] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function setDataContrato($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_contrato !== null || $dt !== null) {
            if ($this->data_contrato === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_contrato->format("Y-m-d H:i:s.u")) {
                $this->data_contrato = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ClienteTableMap::COL_DATA_CONTRATO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataContrato()

    /**
     * Sets the value of [data_cancelado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function setDataCancelado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cancelado !== null || $dt !== null) {
            if ($this->data_cancelado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cancelado->format("Y-m-d H:i:s.u")) {
                $this->data_cancelado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ClienteTableMap::COL_DATA_CANCELADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCancelado()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ClienteTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ClienteTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[ClienteTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Set the value of [import_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function setImportId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->import_id !== $v) {
            $this->import_id = $v;
            $this->modifiedColumns[ClienteTableMap::COL_IMPORT_ID] = true;
        }

        return $this;
    } // setImportId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ClienteTableMap::translateFieldName('Idcliente', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idcliente = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ClienteTableMap::translateFieldName('PessoaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pessoa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ClienteTableMap::translateFieldName('Ativo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ativo = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ClienteTableMap::translateFieldName('DataContrato', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_contrato = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ClienteTableMap::translateFieldName('DataCancelado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cancelado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ClienteTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ClienteTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ClienteTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ClienteTableMap::translateFieldName('ImportId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->import_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = ClienteTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Cliente'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aPessoa !== null && $this->pessoa_id !== $this->aPessoa->getId()) {
            $this->aPessoa = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ClienteTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildClienteQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPessoa = null;
            $this->aUsuario = null;
            $this->collBoletos = null;

            $this->collClienteEstoqueItems = null;

            $this->collEnderecoClientes = null;

            $this->collEstoqueLancamentos = null;

            $this->collFornecedors = null;

            $this->collServicoClientes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Cliente::setDeleted()
     * @see Cliente::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClienteTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildClienteQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClienteTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ClienteTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPessoa !== null) {
                if ($this->aPessoa->isModified() || $this->aPessoa->isNew()) {
                    $affectedRows += $this->aPessoa->save($con);
                }
                $this->setPessoa($this->aPessoa);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->boletosScheduledForDeletion !== null) {
                if (!$this->boletosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BoletoQuery::create()
                        ->filterByPrimaryKeys($this->boletosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->boletosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletos !== null) {
                foreach ($this->collBoletos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->clienteEstoqueItemsScheduledForDeletion !== null) {
                if (!$this->clienteEstoqueItemsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ClienteEstoqueItemQuery::create()
                        ->filterByPrimaryKeys($this->clienteEstoqueItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->clienteEstoqueItemsScheduledForDeletion = null;
                }
            }

            if ($this->collClienteEstoqueItems !== null) {
                foreach ($this->collClienteEstoqueItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->enderecoClientesScheduledForDeletion !== null) {
                if (!$this->enderecoClientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EnderecoClienteQuery::create()
                        ->filterByPrimaryKeys($this->enderecoClientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->enderecoClientesScheduledForDeletion = null;
                }
            }

            if ($this->collEnderecoClientes !== null) {
                foreach ($this->collEnderecoClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->estoqueLancamentosScheduledForDeletion !== null) {
                if (!$this->estoqueLancamentosScheduledForDeletion->isEmpty()) {
                    foreach ($this->estoqueLancamentosScheduledForDeletion as $estoqueLancamento) {
                        // need to save related object because we set the relation to null
                        $estoqueLancamento->save($con);
                    }
                    $this->estoqueLancamentosScheduledForDeletion = null;
                }
            }

            if ($this->collEstoqueLancamentos !== null) {
                foreach ($this->collEstoqueLancamentos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->fornecedorsScheduledForDeletion !== null) {
                if (!$this->fornecedorsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\FornecedorQuery::create()
                        ->filterByPrimaryKeys($this->fornecedorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->fornecedorsScheduledForDeletion = null;
                }
            }

            if ($this->collFornecedors !== null) {
                foreach ($this->collFornecedors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->servicoClientesScheduledForDeletion !== null) {
                if (!$this->servicoClientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ServicoClienteQuery::create()
                        ->filterByPrimaryKeys($this->servicoClientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicoClientesScheduledForDeletion = null;
                }
            }

            if ($this->collServicoClientes !== null) {
                foreach ($this->collServicoClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ClienteTableMap::COL_IDCLIENTE] = true;
        if (null !== $this->idcliente) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ClienteTableMap::COL_IDCLIENTE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ClienteTableMap::COL_IDCLIENTE)) {
            $modifiedColumns[':p' . $index++]  = 'idcliente';
        }
        if ($this->isColumnModified(ClienteTableMap::COL_PESSOA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pessoa_id';
        }
        if ($this->isColumnModified(ClienteTableMap::COL_ATIVO)) {
            $modifiedColumns[':p' . $index++]  = 'ativo';
        }
        if ($this->isColumnModified(ClienteTableMap::COL_DATA_CONTRATO)) {
            $modifiedColumns[':p' . $index++]  = 'data_contrato';
        }
        if ($this->isColumnModified(ClienteTableMap::COL_DATA_CANCELADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cancelado';
        }
        if ($this->isColumnModified(ClienteTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(ClienteTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(ClienteTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(ClienteTableMap::COL_IMPORT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'import_id';
        }

        $sql = sprintf(
            'INSERT INTO cliente (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idcliente':
                        $stmt->bindValue($identifier, $this->idcliente, PDO::PARAM_INT);
                        break;
                    case 'pessoa_id':
                        $stmt->bindValue($identifier, $this->pessoa_id, PDO::PARAM_INT);
                        break;
                    case 'ativo':
                        $stmt->bindValue($identifier, (int) $this->ativo, PDO::PARAM_INT);
                        break;
                    case 'data_contrato':
                        $stmt->bindValue($identifier, $this->data_contrato ? $this->data_contrato->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_cancelado':
                        $stmt->bindValue($identifier, $this->data_cancelado ? $this->data_cancelado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'import_id':
                        $stmt->bindValue($identifier, $this->import_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdcliente($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ClienteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdcliente();
                break;
            case 1:
                return $this->getPessoaId();
                break;
            case 2:
                return $this->getAtivo();
                break;
            case 3:
                return $this->getDataContrato();
                break;
            case 4:
                return $this->getDataCancelado();
                break;
            case 5:
                return $this->getDataCadastro();
                break;
            case 6:
                return $this->getDataAlterado();
                break;
            case 7:
                return $this->getUsuarioAlterado();
                break;
            case 8:
                return $this->getImportId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Cliente'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Cliente'][$this->hashCode()] = true;
        $keys = ClienteTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdcliente(),
            $keys[1] => $this->getPessoaId(),
            $keys[2] => $this->getAtivo(),
            $keys[3] => $this->getDataContrato(),
            $keys[4] => $this->getDataCancelado(),
            $keys[5] => $this->getDataCadastro(),
            $keys[6] => $this->getDataAlterado(),
            $keys[7] => $this->getUsuarioAlterado(),
            $keys[8] => $this->getImportId(),
        );
        if ($result[$keys[3]] instanceof \DateTime) {
            $result[$keys[3]] = $result[$keys[3]]->format('c');
        }

        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPessoa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pessoa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'pessoa';
                        break;
                    default:
                        $key = 'Pessoa';
                }

                $result[$key] = $this->aPessoa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBoletos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boletos';
                        break;
                    default:
                        $key = 'Boletos';
                }

                $result[$key] = $this->collBoletos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collClienteEstoqueItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'clienteEstoqueItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cliente_estoque_items';
                        break;
                    default:
                        $key = 'ClienteEstoqueItems';
                }

                $result[$key] = $this->collClienteEstoqueItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEnderecoClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'enderecoClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'endereco_clientes';
                        break;
                    default:
                        $key = 'EnderecoClientes';
                }

                $result[$key] = $this->collEnderecoClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEstoqueLancamentos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'estoqueLancamentos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'estoque_lancamentos';
                        break;
                    default:
                        $key = 'EstoqueLancamentos';
                }

                $result[$key] = $this->collEstoqueLancamentos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFornecedors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'fornecedors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'fornecedors';
                        break;
                    default:
                        $key = 'Fornecedors';
                }

                $result[$key] = $this->collFornecedors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collServicoClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicoClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'servico_clientes';
                        break;
                    default:
                        $key = 'ServicoClientes';
                }

                $result[$key] = $this->collServicoClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Cliente
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ClienteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Cliente
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdcliente($value);
                break;
            case 1:
                $this->setPessoaId($value);
                break;
            case 2:
                $this->setAtivo($value);
                break;
            case 3:
                $this->setDataContrato($value);
                break;
            case 4:
                $this->setDataCancelado($value);
                break;
            case 5:
                $this->setDataCadastro($value);
                break;
            case 6:
                $this->setDataAlterado($value);
                break;
            case 7:
                $this->setUsuarioAlterado($value);
                break;
            case 8:
                $this->setImportId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ClienteTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdcliente($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPessoaId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAtivo($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDataContrato($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDataCancelado($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDataCadastro($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDataAlterado($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setImportId($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ClienteTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ClienteTableMap::COL_IDCLIENTE)) {
            $criteria->add(ClienteTableMap::COL_IDCLIENTE, $this->idcliente);
        }
        if ($this->isColumnModified(ClienteTableMap::COL_PESSOA_ID)) {
            $criteria->add(ClienteTableMap::COL_PESSOA_ID, $this->pessoa_id);
        }
        if ($this->isColumnModified(ClienteTableMap::COL_ATIVO)) {
            $criteria->add(ClienteTableMap::COL_ATIVO, $this->ativo);
        }
        if ($this->isColumnModified(ClienteTableMap::COL_DATA_CONTRATO)) {
            $criteria->add(ClienteTableMap::COL_DATA_CONTRATO, $this->data_contrato);
        }
        if ($this->isColumnModified(ClienteTableMap::COL_DATA_CANCELADO)) {
            $criteria->add(ClienteTableMap::COL_DATA_CANCELADO, $this->data_cancelado);
        }
        if ($this->isColumnModified(ClienteTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(ClienteTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(ClienteTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(ClienteTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(ClienteTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(ClienteTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(ClienteTableMap::COL_IMPORT_ID)) {
            $criteria->add(ClienteTableMap::COL_IMPORT_ID, $this->import_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildClienteQuery::create();
        $criteria->add(ClienteTableMap::COL_IDCLIENTE, $this->idcliente);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdcliente();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdcliente();
    }

    /**
     * Generic method to set the primary key (idcliente column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdcliente($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdcliente();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Cliente (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPessoaId($this->getPessoaId());
        $copyObj->setAtivo($this->getAtivo());
        $copyObj->setDataContrato($this->getDataContrato());
        $copyObj->setDataCancelado($this->getDataCancelado());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setImportId($this->getImportId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBoletos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoleto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getClienteEstoqueItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addClienteEstoqueItem($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEnderecoClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEnderecoCliente($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEstoqueLancamentos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEstoqueLancamento($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFornecedors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFornecedor($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getServicoClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicoCliente($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdcliente(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Cliente Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildPessoa object.
     *
     * @param  ChildPessoa $v
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPessoa(ChildPessoa $v = null)
    {
        if ($v === null) {
            $this->setPessoaId(NULL);
        } else {
            $this->setPessoaId($v->getId());
        }

        $this->aPessoa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPessoa object, it will not be re-added.
        if ($v !== null) {
            $v->addCliente($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPessoa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPessoa The associated ChildPessoa object.
     * @throws PropelException
     */
    public function getPessoa(ConnectionInterface $con = null)
    {
        if ($this->aPessoa === null && ($this->pessoa_id !== null)) {
            $this->aPessoa = ChildPessoaQuery::create()->findPk($this->pessoa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPessoa->addClientes($this);
             */
        }

        return $this->aPessoa;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addCliente($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addClientes($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Boleto' == $relationName) {
            return $this->initBoletos();
        }
        if ('ClienteEstoqueItem' == $relationName) {
            return $this->initClienteEstoqueItems();
        }
        if ('EnderecoCliente' == $relationName) {
            return $this->initEnderecoClientes();
        }
        if ('EstoqueLancamento' == $relationName) {
            return $this->initEstoqueLancamentos();
        }
        if ('Fornecedor' == $relationName) {
            return $this->initFornecedors();
        }
        if ('ServicoCliente' == $relationName) {
            return $this->initServicoClientes();
        }
    }

    /**
     * Clears out the collBoletos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletos()
     */
    public function clearBoletos()
    {
        $this->collBoletos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletos collection loaded partially.
     */
    public function resetPartialBoletos($v = true)
    {
        $this->collBoletosPartial = $v;
    }

    /**
     * Initializes the collBoletos collection.
     *
     * By default this just sets the collBoletos collection to an empty array (like clearcollBoletos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletos($overrideExisting = true)
    {
        if (null !== $this->collBoletos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletos = new $collectionClassName;
        $this->collBoletos->setModel('\ImaTelecomBundle\Model\Boleto');
    }

    /**
     * Gets an array of ChildBoleto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCliente is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     * @throws PropelException
     */
    public function getBoletos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                // return empty collection
                $this->initBoletos();
            } else {
                $collBoletos = ChildBoletoQuery::create(null, $criteria)
                    ->filterByCliente($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletosPartial && count($collBoletos)) {
                        $this->initBoletos(false);

                        foreach ($collBoletos as $obj) {
                            if (false == $this->collBoletos->contains($obj)) {
                                $this->collBoletos->append($obj);
                            }
                        }

                        $this->collBoletosPartial = true;
                    }

                    return $collBoletos;
                }

                if ($partial && $this->collBoletos) {
                    foreach ($this->collBoletos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletos[] = $obj;
                        }
                    }
                }

                $this->collBoletos = $collBoletos;
                $this->collBoletosPartial = false;
            }
        }

        return $this->collBoletos;
    }

    /**
     * Sets a collection of ChildBoleto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function setBoletos(Collection $boletos, ConnectionInterface $con = null)
    {
        /** @var ChildBoleto[] $boletosToDelete */
        $boletosToDelete = $this->getBoletos(new Criteria(), $con)->diff($boletos);


        $this->boletosScheduledForDeletion = $boletosToDelete;

        foreach ($boletosToDelete as $boletoRemoved) {
            $boletoRemoved->setCliente(null);
        }

        $this->collBoletos = null;
        foreach ($boletos as $boleto) {
            $this->addBoleto($boleto);
        }

        $this->collBoletos = $boletos;
        $this->collBoletosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Boleto objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Boleto objects.
     * @throws PropelException
     */
    public function countBoletos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletos());
            }

            $query = ChildBoletoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCliente($this)
                ->count($con);
        }

        return count($this->collBoletos);
    }

    /**
     * Method called to associate a ChildBoleto object to this object
     * through the ChildBoleto foreign key attribute.
     *
     * @param  ChildBoleto $l ChildBoleto
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function addBoleto(ChildBoleto $l)
    {
        if ($this->collBoletos === null) {
            $this->initBoletos();
            $this->collBoletosPartial = true;
        }

        if (!$this->collBoletos->contains($l)) {
            $this->doAddBoleto($l);

            if ($this->boletosScheduledForDeletion and $this->boletosScheduledForDeletion->contains($l)) {
                $this->boletosScheduledForDeletion->remove($this->boletosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoleto $boleto The ChildBoleto object to add.
     */
    protected function doAddBoleto(ChildBoleto $boleto)
    {
        $this->collBoletos[]= $boleto;
        $boleto->setCliente($this);
    }

    /**
     * @param  ChildBoleto $boleto The ChildBoleto object to remove.
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function removeBoleto(ChildBoleto $boleto)
    {
        if ($this->getBoletos()->contains($boleto)) {
            $pos = $this->collBoletos->search($boleto);
            $this->collBoletos->remove($pos);
            if (null === $this->boletosScheduledForDeletion) {
                $this->boletosScheduledForDeletion = clone $this->collBoletos;
                $this->boletosScheduledForDeletion->clear();
            }
            $this->boletosScheduledForDeletion[]= clone $boleto;
            $boleto->setCliente(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinBaixaEstorno(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('BaixaEstorno', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinFornecedor(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Fornecedor', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletos($query, $con);
    }

    /**
     * Clears out the collClienteEstoqueItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addClienteEstoqueItems()
     */
    public function clearClienteEstoqueItems()
    {
        $this->collClienteEstoqueItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collClienteEstoqueItems collection loaded partially.
     */
    public function resetPartialClienteEstoqueItems($v = true)
    {
        $this->collClienteEstoqueItemsPartial = $v;
    }

    /**
     * Initializes the collClienteEstoqueItems collection.
     *
     * By default this just sets the collClienteEstoqueItems collection to an empty array (like clearcollClienteEstoqueItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initClienteEstoqueItems($overrideExisting = true)
    {
        if (null !== $this->collClienteEstoqueItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = ClienteEstoqueItemTableMap::getTableMap()->getCollectionClassName();

        $this->collClienteEstoqueItems = new $collectionClassName;
        $this->collClienteEstoqueItems->setModel('\ImaTelecomBundle\Model\ClienteEstoqueItem');
    }

    /**
     * Gets an array of ChildClienteEstoqueItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCliente is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildClienteEstoqueItem[] List of ChildClienteEstoqueItem objects
     * @throws PropelException
     */
    public function getClienteEstoqueItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collClienteEstoqueItemsPartial && !$this->isNew();
        if (null === $this->collClienteEstoqueItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collClienteEstoqueItems) {
                // return empty collection
                $this->initClienteEstoqueItems();
            } else {
                $collClienteEstoqueItems = ChildClienteEstoqueItemQuery::create(null, $criteria)
                    ->filterByCliente($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collClienteEstoqueItemsPartial && count($collClienteEstoqueItems)) {
                        $this->initClienteEstoqueItems(false);

                        foreach ($collClienteEstoqueItems as $obj) {
                            if (false == $this->collClienteEstoqueItems->contains($obj)) {
                                $this->collClienteEstoqueItems->append($obj);
                            }
                        }

                        $this->collClienteEstoqueItemsPartial = true;
                    }

                    return $collClienteEstoqueItems;
                }

                if ($partial && $this->collClienteEstoqueItems) {
                    foreach ($this->collClienteEstoqueItems as $obj) {
                        if ($obj->isNew()) {
                            $collClienteEstoqueItems[] = $obj;
                        }
                    }
                }

                $this->collClienteEstoqueItems = $collClienteEstoqueItems;
                $this->collClienteEstoqueItemsPartial = false;
            }
        }

        return $this->collClienteEstoqueItems;
    }

    /**
     * Sets a collection of ChildClienteEstoqueItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $clienteEstoqueItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function setClienteEstoqueItems(Collection $clienteEstoqueItems, ConnectionInterface $con = null)
    {
        /** @var ChildClienteEstoqueItem[] $clienteEstoqueItemsToDelete */
        $clienteEstoqueItemsToDelete = $this->getClienteEstoqueItems(new Criteria(), $con)->diff($clienteEstoqueItems);


        $this->clienteEstoqueItemsScheduledForDeletion = $clienteEstoqueItemsToDelete;

        foreach ($clienteEstoqueItemsToDelete as $clienteEstoqueItemRemoved) {
            $clienteEstoqueItemRemoved->setCliente(null);
        }

        $this->collClienteEstoqueItems = null;
        foreach ($clienteEstoqueItems as $clienteEstoqueItem) {
            $this->addClienteEstoqueItem($clienteEstoqueItem);
        }

        $this->collClienteEstoqueItems = $clienteEstoqueItems;
        $this->collClienteEstoqueItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ClienteEstoqueItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ClienteEstoqueItem objects.
     * @throws PropelException
     */
    public function countClienteEstoqueItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collClienteEstoqueItemsPartial && !$this->isNew();
        if (null === $this->collClienteEstoqueItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collClienteEstoqueItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getClienteEstoqueItems());
            }

            $query = ChildClienteEstoqueItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCliente($this)
                ->count($con);
        }

        return count($this->collClienteEstoqueItems);
    }

    /**
     * Method called to associate a ChildClienteEstoqueItem object to this object
     * through the ChildClienteEstoqueItem foreign key attribute.
     *
     * @param  ChildClienteEstoqueItem $l ChildClienteEstoqueItem
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function addClienteEstoqueItem(ChildClienteEstoqueItem $l)
    {
        if ($this->collClienteEstoqueItems === null) {
            $this->initClienteEstoqueItems();
            $this->collClienteEstoqueItemsPartial = true;
        }

        if (!$this->collClienteEstoqueItems->contains($l)) {
            $this->doAddClienteEstoqueItem($l);

            if ($this->clienteEstoqueItemsScheduledForDeletion and $this->clienteEstoqueItemsScheduledForDeletion->contains($l)) {
                $this->clienteEstoqueItemsScheduledForDeletion->remove($this->clienteEstoqueItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildClienteEstoqueItem $clienteEstoqueItem The ChildClienteEstoqueItem object to add.
     */
    protected function doAddClienteEstoqueItem(ChildClienteEstoqueItem $clienteEstoqueItem)
    {
        $this->collClienteEstoqueItems[]= $clienteEstoqueItem;
        $clienteEstoqueItem->setCliente($this);
    }

    /**
     * @param  ChildClienteEstoqueItem $clienteEstoqueItem The ChildClienteEstoqueItem object to remove.
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function removeClienteEstoqueItem(ChildClienteEstoqueItem $clienteEstoqueItem)
    {
        if ($this->getClienteEstoqueItems()->contains($clienteEstoqueItem)) {
            $pos = $this->collClienteEstoqueItems->search($clienteEstoqueItem);
            $this->collClienteEstoqueItems->remove($pos);
            if (null === $this->clienteEstoqueItemsScheduledForDeletion) {
                $this->clienteEstoqueItemsScheduledForDeletion = clone $this->collClienteEstoqueItems;
                $this->clienteEstoqueItemsScheduledForDeletion->clear();
            }
            $this->clienteEstoqueItemsScheduledForDeletion[]= clone $clienteEstoqueItem;
            $clienteEstoqueItem->setCliente(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related ClienteEstoqueItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildClienteEstoqueItem[] List of ChildClienteEstoqueItem objects
     */
    public function getClienteEstoqueItemsJoinEstoque(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildClienteEstoqueItemQuery::create(null, $criteria);
        $query->joinWith('Estoque', $joinBehavior);

        return $this->getClienteEstoqueItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related ClienteEstoqueItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildClienteEstoqueItem[] List of ChildClienteEstoqueItem objects
     */
    public function getClienteEstoqueItemsJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildClienteEstoqueItemQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getClienteEstoqueItems($query, $con);
    }

    /**
     * Clears out the collEnderecoClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEnderecoClientes()
     */
    public function clearEnderecoClientes()
    {
        $this->collEnderecoClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEnderecoClientes collection loaded partially.
     */
    public function resetPartialEnderecoClientes($v = true)
    {
        $this->collEnderecoClientesPartial = $v;
    }

    /**
     * Initializes the collEnderecoClientes collection.
     *
     * By default this just sets the collEnderecoClientes collection to an empty array (like clearcollEnderecoClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEnderecoClientes($overrideExisting = true)
    {
        if (null !== $this->collEnderecoClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = EnderecoClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collEnderecoClientes = new $collectionClassName;
        $this->collEnderecoClientes->setModel('\ImaTelecomBundle\Model\EnderecoCliente');
    }

    /**
     * Gets an array of ChildEnderecoCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCliente is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEnderecoCliente[] List of ChildEnderecoCliente objects
     * @throws PropelException
     */
    public function getEnderecoClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEnderecoClientes) {
                // return empty collection
                $this->initEnderecoClientes();
            } else {
                $collEnderecoClientes = ChildEnderecoClienteQuery::create(null, $criteria)
                    ->filterByCliente($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEnderecoClientesPartial && count($collEnderecoClientes)) {
                        $this->initEnderecoClientes(false);

                        foreach ($collEnderecoClientes as $obj) {
                            if (false == $this->collEnderecoClientes->contains($obj)) {
                                $this->collEnderecoClientes->append($obj);
                            }
                        }

                        $this->collEnderecoClientesPartial = true;
                    }

                    return $collEnderecoClientes;
                }

                if ($partial && $this->collEnderecoClientes) {
                    foreach ($this->collEnderecoClientes as $obj) {
                        if ($obj->isNew()) {
                            $collEnderecoClientes[] = $obj;
                        }
                    }
                }

                $this->collEnderecoClientes = $collEnderecoClientes;
                $this->collEnderecoClientesPartial = false;
            }
        }

        return $this->collEnderecoClientes;
    }

    /**
     * Sets a collection of ChildEnderecoCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $enderecoClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function setEnderecoClientes(Collection $enderecoClientes, ConnectionInterface $con = null)
    {
        /** @var ChildEnderecoCliente[] $enderecoClientesToDelete */
        $enderecoClientesToDelete = $this->getEnderecoClientes(new Criteria(), $con)->diff($enderecoClientes);


        $this->enderecoClientesScheduledForDeletion = $enderecoClientesToDelete;

        foreach ($enderecoClientesToDelete as $enderecoClienteRemoved) {
            $enderecoClienteRemoved->setCliente(null);
        }

        $this->collEnderecoClientes = null;
        foreach ($enderecoClientes as $enderecoCliente) {
            $this->addEnderecoCliente($enderecoCliente);
        }

        $this->collEnderecoClientes = $enderecoClientes;
        $this->collEnderecoClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EnderecoCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EnderecoCliente objects.
     * @throws PropelException
     */
    public function countEnderecoClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEnderecoClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEnderecoClientes());
            }

            $query = ChildEnderecoClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCliente($this)
                ->count($con);
        }

        return count($this->collEnderecoClientes);
    }

    /**
     * Method called to associate a ChildEnderecoCliente object to this object
     * through the ChildEnderecoCliente foreign key attribute.
     *
     * @param  ChildEnderecoCliente $l ChildEnderecoCliente
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function addEnderecoCliente(ChildEnderecoCliente $l)
    {
        if ($this->collEnderecoClientes === null) {
            $this->initEnderecoClientes();
            $this->collEnderecoClientesPartial = true;
        }

        if (!$this->collEnderecoClientes->contains($l)) {
            $this->doAddEnderecoCliente($l);

            if ($this->enderecoClientesScheduledForDeletion and $this->enderecoClientesScheduledForDeletion->contains($l)) {
                $this->enderecoClientesScheduledForDeletion->remove($this->enderecoClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEnderecoCliente $enderecoCliente The ChildEnderecoCliente object to add.
     */
    protected function doAddEnderecoCliente(ChildEnderecoCliente $enderecoCliente)
    {
        $this->collEnderecoClientes[]= $enderecoCliente;
        $enderecoCliente->setCliente($this);
    }

    /**
     * @param  ChildEnderecoCliente $enderecoCliente The ChildEnderecoCliente object to remove.
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function removeEnderecoCliente(ChildEnderecoCliente $enderecoCliente)
    {
        if ($this->getEnderecoClientes()->contains($enderecoCliente)) {
            $pos = $this->collEnderecoClientes->search($enderecoCliente);
            $this->collEnderecoClientes->remove($pos);
            if (null === $this->enderecoClientesScheduledForDeletion) {
                $this->enderecoClientesScheduledForDeletion = clone $this->collEnderecoClientes;
                $this->enderecoClientesScheduledForDeletion->clear();
            }
            $this->enderecoClientesScheduledForDeletion[]= clone $enderecoCliente;
            $enderecoCliente->setCliente(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related EnderecoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoCliente[] List of ChildEnderecoCliente objects
     */
    public function getEnderecoClientesJoinCidade(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoClienteQuery::create(null, $criteria);
        $query->joinWith('Cidade', $joinBehavior);

        return $this->getEnderecoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related EnderecoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoCliente[] List of ChildEnderecoCliente objects
     */
    public function getEnderecoClientesJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoClienteQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getEnderecoClientes($query, $con);
    }

    /**
     * Clears out the collEstoqueLancamentos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEstoqueLancamentos()
     */
    public function clearEstoqueLancamentos()
    {
        $this->collEstoqueLancamentos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEstoqueLancamentos collection loaded partially.
     */
    public function resetPartialEstoqueLancamentos($v = true)
    {
        $this->collEstoqueLancamentosPartial = $v;
    }

    /**
     * Initializes the collEstoqueLancamentos collection.
     *
     * By default this just sets the collEstoqueLancamentos collection to an empty array (like clearcollEstoqueLancamentos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEstoqueLancamentos($overrideExisting = true)
    {
        if (null !== $this->collEstoqueLancamentos && !$overrideExisting) {
            return;
        }

        $collectionClassName = EstoqueLancamentoTableMap::getTableMap()->getCollectionClassName();

        $this->collEstoqueLancamentos = new $collectionClassName;
        $this->collEstoqueLancamentos->setModel('\ImaTelecomBundle\Model\EstoqueLancamento');
    }

    /**
     * Gets an array of ChildEstoqueLancamento objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCliente is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEstoqueLancamento[] List of ChildEstoqueLancamento objects
     * @throws PropelException
     */
    public function getEstoqueLancamentos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEstoqueLancamentosPartial && !$this->isNew();
        if (null === $this->collEstoqueLancamentos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEstoqueLancamentos) {
                // return empty collection
                $this->initEstoqueLancamentos();
            } else {
                $collEstoqueLancamentos = ChildEstoqueLancamentoQuery::create(null, $criteria)
                    ->filterByCliente($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEstoqueLancamentosPartial && count($collEstoqueLancamentos)) {
                        $this->initEstoqueLancamentos(false);

                        foreach ($collEstoqueLancamentos as $obj) {
                            if (false == $this->collEstoqueLancamentos->contains($obj)) {
                                $this->collEstoqueLancamentos->append($obj);
                            }
                        }

                        $this->collEstoqueLancamentosPartial = true;
                    }

                    return $collEstoqueLancamentos;
                }

                if ($partial && $this->collEstoqueLancamentos) {
                    foreach ($this->collEstoqueLancamentos as $obj) {
                        if ($obj->isNew()) {
                            $collEstoqueLancamentos[] = $obj;
                        }
                    }
                }

                $this->collEstoqueLancamentos = $collEstoqueLancamentos;
                $this->collEstoqueLancamentosPartial = false;
            }
        }

        return $this->collEstoqueLancamentos;
    }

    /**
     * Sets a collection of ChildEstoqueLancamento objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $estoqueLancamentos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function setEstoqueLancamentos(Collection $estoqueLancamentos, ConnectionInterface $con = null)
    {
        /** @var ChildEstoqueLancamento[] $estoqueLancamentosToDelete */
        $estoqueLancamentosToDelete = $this->getEstoqueLancamentos(new Criteria(), $con)->diff($estoqueLancamentos);


        $this->estoqueLancamentosScheduledForDeletion = $estoqueLancamentosToDelete;

        foreach ($estoqueLancamentosToDelete as $estoqueLancamentoRemoved) {
            $estoqueLancamentoRemoved->setCliente(null);
        }

        $this->collEstoqueLancamentos = null;
        foreach ($estoqueLancamentos as $estoqueLancamento) {
            $this->addEstoqueLancamento($estoqueLancamento);
        }

        $this->collEstoqueLancamentos = $estoqueLancamentos;
        $this->collEstoqueLancamentosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EstoqueLancamento objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EstoqueLancamento objects.
     * @throws PropelException
     */
    public function countEstoqueLancamentos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEstoqueLancamentosPartial && !$this->isNew();
        if (null === $this->collEstoqueLancamentos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEstoqueLancamentos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEstoqueLancamentos());
            }

            $query = ChildEstoqueLancamentoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCliente($this)
                ->count($con);
        }

        return count($this->collEstoqueLancamentos);
    }

    /**
     * Method called to associate a ChildEstoqueLancamento object to this object
     * through the ChildEstoqueLancamento foreign key attribute.
     *
     * @param  ChildEstoqueLancamento $l ChildEstoqueLancamento
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function addEstoqueLancamento(ChildEstoqueLancamento $l)
    {
        if ($this->collEstoqueLancamentos === null) {
            $this->initEstoqueLancamentos();
            $this->collEstoqueLancamentosPartial = true;
        }

        if (!$this->collEstoqueLancamentos->contains($l)) {
            $this->doAddEstoqueLancamento($l);

            if ($this->estoqueLancamentosScheduledForDeletion and $this->estoqueLancamentosScheduledForDeletion->contains($l)) {
                $this->estoqueLancamentosScheduledForDeletion->remove($this->estoqueLancamentosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEstoqueLancamento $estoqueLancamento The ChildEstoqueLancamento object to add.
     */
    protected function doAddEstoqueLancamento(ChildEstoqueLancamento $estoqueLancamento)
    {
        $this->collEstoqueLancamentos[]= $estoqueLancamento;
        $estoqueLancamento->setCliente($this);
    }

    /**
     * @param  ChildEstoqueLancamento $estoqueLancamento The ChildEstoqueLancamento object to remove.
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function removeEstoqueLancamento(ChildEstoqueLancamento $estoqueLancamento)
    {
        if ($this->getEstoqueLancamentos()->contains($estoqueLancamento)) {
            $pos = $this->collEstoqueLancamentos->search($estoqueLancamento);
            $this->collEstoqueLancamentos->remove($pos);
            if (null === $this->estoqueLancamentosScheduledForDeletion) {
                $this->estoqueLancamentosScheduledForDeletion = clone $this->collEstoqueLancamentos;
                $this->estoqueLancamentosScheduledForDeletion->clear();
            }
            $this->estoqueLancamentosScheduledForDeletion[]= $estoqueLancamento;
            $estoqueLancamento->setCliente(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related EstoqueLancamentos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEstoqueLancamento[] List of ChildEstoqueLancamento objects
     */
    public function getEstoqueLancamentosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEstoqueLancamentoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getEstoqueLancamentos($query, $con);
    }

    /**
     * Clears out the collFornecedors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFornecedors()
     */
    public function clearFornecedors()
    {
        $this->collFornecedors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFornecedors collection loaded partially.
     */
    public function resetPartialFornecedors($v = true)
    {
        $this->collFornecedorsPartial = $v;
    }

    /**
     * Initializes the collFornecedors collection.
     *
     * By default this just sets the collFornecedors collection to an empty array (like clearcollFornecedors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFornecedors($overrideExisting = true)
    {
        if (null !== $this->collFornecedors && !$overrideExisting) {
            return;
        }

        $collectionClassName = FornecedorTableMap::getTableMap()->getCollectionClassName();

        $this->collFornecedors = new $collectionClassName;
        $this->collFornecedors->setModel('\ImaTelecomBundle\Model\Fornecedor');
    }

    /**
     * Gets an array of ChildFornecedor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCliente is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     * @throws PropelException
     */
    public function getFornecedors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFornecedorsPartial && !$this->isNew();
        if (null === $this->collFornecedors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFornecedors) {
                // return empty collection
                $this->initFornecedors();
            } else {
                $collFornecedors = ChildFornecedorQuery::create(null, $criteria)
                    ->filterByCliente($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFornecedorsPartial && count($collFornecedors)) {
                        $this->initFornecedors(false);

                        foreach ($collFornecedors as $obj) {
                            if (false == $this->collFornecedors->contains($obj)) {
                                $this->collFornecedors->append($obj);
                            }
                        }

                        $this->collFornecedorsPartial = true;
                    }

                    return $collFornecedors;
                }

                if ($partial && $this->collFornecedors) {
                    foreach ($this->collFornecedors as $obj) {
                        if ($obj->isNew()) {
                            $collFornecedors[] = $obj;
                        }
                    }
                }

                $this->collFornecedors = $collFornecedors;
                $this->collFornecedorsPartial = false;
            }
        }

        return $this->collFornecedors;
    }

    /**
     * Sets a collection of ChildFornecedor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $fornecedors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function setFornecedors(Collection $fornecedors, ConnectionInterface $con = null)
    {
        /** @var ChildFornecedor[] $fornecedorsToDelete */
        $fornecedorsToDelete = $this->getFornecedors(new Criteria(), $con)->diff($fornecedors);


        $this->fornecedorsScheduledForDeletion = $fornecedorsToDelete;

        foreach ($fornecedorsToDelete as $fornecedorRemoved) {
            $fornecedorRemoved->setCliente(null);
        }

        $this->collFornecedors = null;
        foreach ($fornecedors as $fornecedor) {
            $this->addFornecedor($fornecedor);
        }

        $this->collFornecedors = $fornecedors;
        $this->collFornecedorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Fornecedor objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Fornecedor objects.
     * @throws PropelException
     */
    public function countFornecedors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFornecedorsPartial && !$this->isNew();
        if (null === $this->collFornecedors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFornecedors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFornecedors());
            }

            $query = ChildFornecedorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCliente($this)
                ->count($con);
        }

        return count($this->collFornecedors);
    }

    /**
     * Method called to associate a ChildFornecedor object to this object
     * through the ChildFornecedor foreign key attribute.
     *
     * @param  ChildFornecedor $l ChildFornecedor
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function addFornecedor(ChildFornecedor $l)
    {
        if ($this->collFornecedors === null) {
            $this->initFornecedors();
            $this->collFornecedorsPartial = true;
        }

        if (!$this->collFornecedors->contains($l)) {
            $this->doAddFornecedor($l);

            if ($this->fornecedorsScheduledForDeletion and $this->fornecedorsScheduledForDeletion->contains($l)) {
                $this->fornecedorsScheduledForDeletion->remove($this->fornecedorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFornecedor $fornecedor The ChildFornecedor object to add.
     */
    protected function doAddFornecedor(ChildFornecedor $fornecedor)
    {
        $this->collFornecedors[]= $fornecedor;
        $fornecedor->setCliente($this);
    }

    /**
     * @param  ChildFornecedor $fornecedor The ChildFornecedor object to remove.
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function removeFornecedor(ChildFornecedor $fornecedor)
    {
        if ($this->getFornecedors()->contains($fornecedor)) {
            $pos = $this->collFornecedors->search($fornecedor);
            $this->collFornecedors->remove($pos);
            if (null === $this->fornecedorsScheduledForDeletion) {
                $this->fornecedorsScheduledForDeletion = clone $this->collFornecedors;
                $this->fornecedorsScheduledForDeletion->clear();
            }
            $this->fornecedorsScheduledForDeletion[]= clone $fornecedor;
            $fornecedor->setCliente(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinBancoAgenciaConta(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('BancoAgenciaConta', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }

    /**
     * Clears out the collServicoClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicoClientes()
     */
    public function clearServicoClientes()
    {
        $this->collServicoClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicoClientes collection loaded partially.
     */
    public function resetPartialServicoClientes($v = true)
    {
        $this->collServicoClientesPartial = $v;
    }

    /**
     * Initializes the collServicoClientes collection.
     *
     * By default this just sets the collServicoClientes collection to an empty array (like clearcollServicoClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicoClientes($overrideExisting = true)
    {
        if (null !== $this->collServicoClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicoClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collServicoClientes = new $collectionClassName;
        $this->collServicoClientes->setModel('\ImaTelecomBundle\Model\ServicoCliente');
    }

    /**
     * Gets an array of ChildServicoCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCliente is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     * @throws PropelException
     */
    public function getServicoClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoClientesPartial && !$this->isNew();
        if (null === $this->collServicoClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicoClientes) {
                // return empty collection
                $this->initServicoClientes();
            } else {
                $collServicoClientes = ChildServicoClienteQuery::create(null, $criteria)
                    ->filterByCliente($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicoClientesPartial && count($collServicoClientes)) {
                        $this->initServicoClientes(false);

                        foreach ($collServicoClientes as $obj) {
                            if (false == $this->collServicoClientes->contains($obj)) {
                                $this->collServicoClientes->append($obj);
                            }
                        }

                        $this->collServicoClientesPartial = true;
                    }

                    return $collServicoClientes;
                }

                if ($partial && $this->collServicoClientes) {
                    foreach ($this->collServicoClientes as $obj) {
                        if ($obj->isNew()) {
                            $collServicoClientes[] = $obj;
                        }
                    }
                }

                $this->collServicoClientes = $collServicoClientes;
                $this->collServicoClientesPartial = false;
            }
        }

        return $this->collServicoClientes;
    }

    /**
     * Sets a collection of ChildServicoCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicoClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function setServicoClientes(Collection $servicoClientes, ConnectionInterface $con = null)
    {
        /** @var ChildServicoCliente[] $servicoClientesToDelete */
        $servicoClientesToDelete = $this->getServicoClientes(new Criteria(), $con)->diff($servicoClientes);


        $this->servicoClientesScheduledForDeletion = $servicoClientesToDelete;

        foreach ($servicoClientesToDelete as $servicoClienteRemoved) {
            $servicoClienteRemoved->setCliente(null);
        }

        $this->collServicoClientes = null;
        foreach ($servicoClientes as $servicoCliente) {
            $this->addServicoCliente($servicoCliente);
        }

        $this->collServicoClientes = $servicoClientes;
        $this->collServicoClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ServicoCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ServicoCliente objects.
     * @throws PropelException
     */
    public function countServicoClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoClientesPartial && !$this->isNew();
        if (null === $this->collServicoClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicoClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicoClientes());
            }

            $query = ChildServicoClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCliente($this)
                ->count($con);
        }

        return count($this->collServicoClientes);
    }

    /**
     * Method called to associate a ChildServicoCliente object to this object
     * through the ChildServicoCliente foreign key attribute.
     *
     * @param  ChildServicoCliente $l ChildServicoCliente
     * @return $this|\ImaTelecomBundle\Model\Cliente The current object (for fluent API support)
     */
    public function addServicoCliente(ChildServicoCliente $l)
    {
        if ($this->collServicoClientes === null) {
            $this->initServicoClientes();
            $this->collServicoClientesPartial = true;
        }

        if (!$this->collServicoClientes->contains($l)) {
            $this->doAddServicoCliente($l);

            if ($this->servicoClientesScheduledForDeletion and $this->servicoClientesScheduledForDeletion->contains($l)) {
                $this->servicoClientesScheduledForDeletion->remove($this->servicoClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicoCliente $servicoCliente The ChildServicoCliente object to add.
     */
    protected function doAddServicoCliente(ChildServicoCliente $servicoCliente)
    {
        $this->collServicoClientes[]= $servicoCliente;
        $servicoCliente->setCliente($this);
    }

    /**
     * @param  ChildServicoCliente $servicoCliente The ChildServicoCliente object to remove.
     * @return $this|ChildCliente The current object (for fluent API support)
     */
    public function removeServicoCliente(ChildServicoCliente $servicoCliente)
    {
        if ($this->getServicoClientes()->contains($servicoCliente)) {
            $pos = $this->collServicoClientes->search($servicoCliente);
            $this->collServicoClientes->remove($pos);
            if (null === $this->servicoClientesScheduledForDeletion) {
                $this->servicoClientesScheduledForDeletion = clone $this->collServicoClientes;
                $this->servicoClientesScheduledForDeletion->clear();
            }
            $this->servicoClientesScheduledForDeletion[]= clone $servicoCliente;
            $servicoCliente->setCliente(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinContrato(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('Contrato', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinServicoPrestado(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('ServicoPrestado', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cliente is new, it will return
     * an empty collection; or if this Cliente has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aPessoa) {
            $this->aPessoa->removeCliente($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeCliente($this);
        }
        $this->idcliente = null;
        $this->pessoa_id = null;
        $this->ativo = null;
        $this->data_contrato = null;
        $this->data_cancelado = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->import_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBoletos) {
                foreach ($this->collBoletos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collClienteEstoqueItems) {
                foreach ($this->collClienteEstoqueItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEnderecoClientes) {
                foreach ($this->collEnderecoClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEstoqueLancamentos) {
                foreach ($this->collEstoqueLancamentos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFornecedors) {
                foreach ($this->collFornecedors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicoClientes) {
                foreach ($this->collServicoClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBoletos = null;
        $this->collClienteEstoqueItems = null;
        $this->collEnderecoClientes = null;
        $this->collEstoqueLancamentos = null;
        $this->collFornecedors = null;
        $this->collServicoClientes = null;
        $this->aPessoa = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ClienteTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
