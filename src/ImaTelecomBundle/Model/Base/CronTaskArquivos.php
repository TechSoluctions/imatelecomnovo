<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\CronTask as ChildCronTask;
use ImaTelecomBundle\Model\CronTaskArquivosQuery as ChildCronTaskArquivosQuery;
use ImaTelecomBundle\Model\CronTaskQuery as ChildCronTaskQuery;
use ImaTelecomBundle\Model\Map\CronTaskArquivosTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'cron_task_arquivos' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class CronTaskArquivos implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\CronTaskArquivosTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the arquivo field.
     *
     * @var        string
     */
    protected $arquivo;

    /**
     * The value for the cron_task_id field.
     *
     * @var        int
     */
    protected $cron_task_id;

    /**
     * The value for the executar field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $executar;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the ordem_execucao field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ordem_execucao;

    /**
     * The value for the diretorio_filho field.
     *
     * @var        string
     */
    protected $diretorio_filho;

    /**
     * The value for the conteudo_script field.
     *
     * @var        string
     */
    protected $conteudo_script;

    /**
     * The value for the tipo_arquivo field.
     *
     * Note: this column has a database default value of: 'unico'
     * @var        string
     */
    protected $tipo_arquivo;

    /**
     * @var        ChildCronTask
     */
    protected $aCronTask;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->executar = false;
        $this->ordem_execucao = 0;
        $this->tipo_arquivo = 'unico';
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\CronTaskArquivos object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>CronTaskArquivos</code> instance.  If
     * <code>obj</code> is an instance of <code>CronTaskArquivos</code>, delegates to
     * <code>equals(CronTaskArquivos)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|CronTaskArquivos The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [arquivo] column value.
     *
     * @return string
     */
    public function getArquivo()
    {
        return $this->arquivo;
    }

    /**
     * Get the [cron_task_id] column value.
     *
     * @return int
     */
    public function getCronTaskId()
    {
        return $this->cron_task_id;
    }

    /**
     * Get the [executar] column value.
     *
     * @return boolean
     */
    public function getExecutar()
    {
        return $this->executar;
    }

    /**
     * Get the [executar] column value.
     *
     * @return boolean
     */
    public function isExecutar()
    {
        return $this->getExecutar();
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [ordem_execucao] column value.
     *
     * @return int
     */
    public function getOrdemExecucao()
    {
        return $this->ordem_execucao;
    }

    /**
     * Get the [diretorio_filho] column value.
     *
     * @return string
     */
    public function getDiretorioFilho()
    {
        return $this->diretorio_filho;
    }

    /**
     * Get the [conteudo_script] column value.
     *
     * @return string
     */
    public function getConteudoScript()
    {
        return $this->conteudo_script;
    }

    /**
     * Get the [tipo_arquivo] column value.
     *
     * @return string
     */
    public function getTipoArquivo()
    {
        return $this->tipo_arquivo;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CronTaskArquivosTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [arquivo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setArquivo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->arquivo !== $v) {
            $this->arquivo = $v;
            $this->modifiedColumns[CronTaskArquivosTableMap::COL_ARQUIVO] = true;
        }

        return $this;
    } // setArquivo()

    /**
     * Set the value of [cron_task_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setCronTaskId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cron_task_id !== $v) {
            $this->cron_task_id = $v;
            $this->modifiedColumns[CronTaskArquivosTableMap::COL_CRON_TASK_ID] = true;
        }

        if ($this->aCronTask !== null && $this->aCronTask->getId() !== $v) {
            $this->aCronTask = null;
        }

        return $this;
    } // setCronTaskId()

    /**
     * Sets the value of the [executar] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setExecutar($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->executar !== $v) {
            $this->executar = $v;
            $this->modifiedColumns[CronTaskArquivosTableMap::COL_EXECUTAR] = true;
        }

        return $this;
    } // setExecutar()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CronTaskArquivosTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CronTaskArquivosTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [ordem_execucao] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setOrdemExecucao($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ordem_execucao !== $v) {
            $this->ordem_execucao = $v;
            $this->modifiedColumns[CronTaskArquivosTableMap::COL_ORDEM_EXECUCAO] = true;
        }

        return $this;
    } // setOrdemExecucao()

    /**
     * Set the value of [diretorio_filho] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setDiretorioFilho($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->diretorio_filho !== $v) {
            $this->diretorio_filho = $v;
            $this->modifiedColumns[CronTaskArquivosTableMap::COL_DIRETORIO_FILHO] = true;
        }

        return $this;
    } // setDiretorioFilho()

    /**
     * Set the value of [conteudo_script] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setConteudoScript($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->conteudo_script !== $v) {
            $this->conteudo_script = $v;
            $this->modifiedColumns[CronTaskArquivosTableMap::COL_CONTEUDO_SCRIPT] = true;
        }

        return $this;
    } // setConteudoScript()

    /**
     * Set the value of [tipo_arquivo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     */
    public function setTipoArquivo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_arquivo !== $v) {
            $this->tipo_arquivo = $v;
            $this->modifiedColumns[CronTaskArquivosTableMap::COL_TIPO_ARQUIVO] = true;
        }

        return $this;
    } // setTipoArquivo()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->executar !== false) {
                return false;
            }

            if ($this->ordem_execucao !== 0) {
                return false;
            }

            if ($this->tipo_arquivo !== 'unico') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CronTaskArquivosTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CronTaskArquivosTableMap::translateFieldName('Arquivo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->arquivo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CronTaskArquivosTableMap::translateFieldName('CronTaskId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cron_task_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CronTaskArquivosTableMap::translateFieldName('Executar', TableMap::TYPE_PHPNAME, $indexType)];
            $this->executar = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CronTaskArquivosTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CronTaskArquivosTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CronTaskArquivosTableMap::translateFieldName('OrdemExecucao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ordem_execucao = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CronTaskArquivosTableMap::translateFieldName('DiretorioFilho', TableMap::TYPE_PHPNAME, $indexType)];
            $this->diretorio_filho = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CronTaskArquivosTableMap::translateFieldName('ConteudoScript', TableMap::TYPE_PHPNAME, $indexType)];
            $this->conteudo_script = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CronTaskArquivosTableMap::translateFieldName('TipoArquivo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_arquivo = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = CronTaskArquivosTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\CronTaskArquivos'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCronTask !== null && $this->cron_task_id !== $this->aCronTask->getId()) {
            $this->aCronTask = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CronTaskArquivosTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCronTaskArquivosQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCronTask = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see CronTaskArquivos::setDeleted()
     * @see CronTaskArquivos::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskArquivosTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCronTaskArquivosQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskArquivosTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CronTaskArquivosTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCronTask !== null) {
                if ($this->aCronTask->isModified() || $this->aCronTask->isNew()) {
                    $affectedRows += $this->aCronTask->save($con);
                }
                $this->setCronTask($this->aCronTask);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CronTaskArquivosTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CronTaskArquivosTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_ARQUIVO)) {
            $modifiedColumns[':p' . $index++]  = 'arquivo';
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_CRON_TASK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cron_task_id';
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_EXECUTAR)) {
            $modifiedColumns[':p' . $index++]  = 'executar';
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_ORDEM_EXECUCAO)) {
            $modifiedColumns[':p' . $index++]  = 'ordem_execucao';
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_DIRETORIO_FILHO)) {
            $modifiedColumns[':p' . $index++]  = 'diretorio_filho';
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_CONTEUDO_SCRIPT)) {
            $modifiedColumns[':p' . $index++]  = 'conteudo_script';
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_TIPO_ARQUIVO)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_arquivo';
        }

        $sql = sprintf(
            'INSERT INTO cron_task_arquivos (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'arquivo':
                        $stmt->bindValue($identifier, $this->arquivo, PDO::PARAM_STR);
                        break;
                    case 'cron_task_id':
                        $stmt->bindValue($identifier, $this->cron_task_id, PDO::PARAM_INT);
                        break;
                    case 'executar':
                        $stmt->bindValue($identifier, (int) $this->executar, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'ordem_execucao':
                        $stmt->bindValue($identifier, $this->ordem_execucao, PDO::PARAM_INT);
                        break;
                    case 'diretorio_filho':
                        $stmt->bindValue($identifier, $this->diretorio_filho, PDO::PARAM_STR);
                        break;
                    case 'conteudo_script':
                        $stmt->bindValue($identifier, $this->conteudo_script, PDO::PARAM_STR);
                        break;
                    case 'tipo_arquivo':
                        $stmt->bindValue($identifier, $this->tipo_arquivo, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CronTaskArquivosTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getArquivo();
                break;
            case 2:
                return $this->getCronTaskId();
                break;
            case 3:
                return $this->getExecutar();
                break;
            case 4:
                return $this->getDataCadastro();
                break;
            case 5:
                return $this->getDataAlterado();
                break;
            case 6:
                return $this->getOrdemExecucao();
                break;
            case 7:
                return $this->getDiretorioFilho();
                break;
            case 8:
                return $this->getConteudoScript();
                break;
            case 9:
                return $this->getTipoArquivo();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['CronTaskArquivos'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CronTaskArquivos'][$this->hashCode()] = true;
        $keys = CronTaskArquivosTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getArquivo(),
            $keys[2] => $this->getCronTaskId(),
            $keys[3] => $this->getExecutar(),
            $keys[4] => $this->getDataCadastro(),
            $keys[5] => $this->getDataAlterado(),
            $keys[6] => $this->getOrdemExecucao(),
            $keys[7] => $this->getDiretorioFilho(),
            $keys[8] => $this->getConteudoScript(),
            $keys[9] => $this->getTipoArquivo(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCronTask) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'cronTask';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cron_task';
                        break;
                    default:
                        $key = 'CronTask';
                }

                $result[$key] = $this->aCronTask->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CronTaskArquivosTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setArquivo($value);
                break;
            case 2:
                $this->setCronTaskId($value);
                break;
            case 3:
                $this->setExecutar($value);
                break;
            case 4:
                $this->setDataCadastro($value);
                break;
            case 5:
                $this->setDataAlterado($value);
                break;
            case 6:
                $this->setOrdemExecucao($value);
                break;
            case 7:
                $this->setDiretorioFilho($value);
                break;
            case 8:
                $this->setConteudoScript($value);
                break;
            case 9:
                $this->setTipoArquivo($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CronTaskArquivosTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setArquivo($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCronTaskId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setExecutar($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDataCadastro($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDataAlterado($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setOrdemExecucao($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDiretorioFilho($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setConteudoScript($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTipoArquivo($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CronTaskArquivosTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_ID)) {
            $criteria->add(CronTaskArquivosTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_ARQUIVO)) {
            $criteria->add(CronTaskArquivosTableMap::COL_ARQUIVO, $this->arquivo);
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_CRON_TASK_ID)) {
            $criteria->add(CronTaskArquivosTableMap::COL_CRON_TASK_ID, $this->cron_task_id);
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_EXECUTAR)) {
            $criteria->add(CronTaskArquivosTableMap::COL_EXECUTAR, $this->executar);
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(CronTaskArquivosTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(CronTaskArquivosTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_ORDEM_EXECUCAO)) {
            $criteria->add(CronTaskArquivosTableMap::COL_ORDEM_EXECUCAO, $this->ordem_execucao);
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_DIRETORIO_FILHO)) {
            $criteria->add(CronTaskArquivosTableMap::COL_DIRETORIO_FILHO, $this->diretorio_filho);
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_CONTEUDO_SCRIPT)) {
            $criteria->add(CronTaskArquivosTableMap::COL_CONTEUDO_SCRIPT, $this->conteudo_script);
        }
        if ($this->isColumnModified(CronTaskArquivosTableMap::COL_TIPO_ARQUIVO)) {
            $criteria->add(CronTaskArquivosTableMap::COL_TIPO_ARQUIVO, $this->tipo_arquivo);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCronTaskArquivosQuery::create();
        $criteria->add(CronTaskArquivosTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\CronTaskArquivos (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setArquivo($this->getArquivo());
        $copyObj->setCronTaskId($this->getCronTaskId());
        $copyObj->setExecutar($this->getExecutar());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setOrdemExecucao($this->getOrdemExecucao());
        $copyObj->setDiretorioFilho($this->getDiretorioFilho());
        $copyObj->setConteudoScript($this->getConteudoScript());
        $copyObj->setTipoArquivo($this->getTipoArquivo());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\CronTaskArquivos Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCronTask object.
     *
     * @param  ChildCronTask $v
     * @return $this|\ImaTelecomBundle\Model\CronTaskArquivos The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCronTask(ChildCronTask $v = null)
    {
        if ($v === null) {
            $this->setCronTaskId(NULL);
        } else {
            $this->setCronTaskId($v->getId());
        }

        $this->aCronTask = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCronTask object, it will not be re-added.
        if ($v !== null) {
            $v->addCronTaskArquivos($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCronTask object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCronTask The associated ChildCronTask object.
     * @throws PropelException
     */
    public function getCronTask(ConnectionInterface $con = null)
    {
        if ($this->aCronTask === null && ($this->cron_task_id !== null)) {
            $this->aCronTask = ChildCronTaskQuery::create()->findPk($this->cron_task_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCronTask->addCronTaskArquivoss($this);
             */
        }

        return $this->aCronTask;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCronTask) {
            $this->aCronTask->removeCronTaskArquivos($this);
        }
        $this->id = null;
        $this->arquivo = null;
        $this->cron_task_id = null;
        $this->executar = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->ordem_execucao = null;
        $this->diretorio_filho = null;
        $this->conteudo_script = null;
        $this->tipo_arquivo = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aCronTask = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CronTaskArquivosTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
