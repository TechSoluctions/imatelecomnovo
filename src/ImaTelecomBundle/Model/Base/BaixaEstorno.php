<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Baixa as ChildBaixa;
use ImaTelecomBundle\Model\BaixaEstorno as ChildBaixaEstorno;
use ImaTelecomBundle\Model\BaixaEstornoQuery as ChildBaixaEstornoQuery;
use ImaTelecomBundle\Model\BaixaQuery as ChildBaixaQuery;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoBaixaHistorico as ChildBoletoBaixaHistorico;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery as ChildBoletoBaixaHistoricoQuery;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Competencia as ChildCompetencia;
use ImaTelecomBundle\Model\CompetenciaQuery as ChildCompetenciaQuery;
use ImaTelecomBundle\Model\ContaCaixa as ChildContaCaixa;
use ImaTelecomBundle\Model\ContaCaixaQuery as ChildContaCaixaQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BaixaEstornoTableMap;
use ImaTelecomBundle\Model\Map\BoletoBaixaHistoricoTableMap;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'baixa_estorno' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class BaixaEstorno implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\BaixaEstornoTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idbaixa_estorno field.
     *
     * @var        int
     */
    protected $idbaixa_estorno;

    /**
     * The value for the baixa_id field.
     *
     * @var        int
     */
    protected $baixa_id;

    /**
     * The value for the conta_caixa_id field.
     *
     * @var        int
     */
    protected $conta_caixa_id;

    /**
     * The value for the competencia_id field.
     *
     * @var        int
     */
    protected $competencia_id;

    /**
     * The value for the usuario_estorno field.
     *
     * @var        int
     */
    protected $usuario_estorno;

    /**
     * The value for the data_estorno field.
     *
     * @var        DateTime
     */
    protected $data_estorno;

    /**
     * The value for the descricao_movimento field.
     *
     * @var        string
     */
    protected $descricao_movimento;

    /**
     * The value for the valor_original field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_original;

    /**
     * The value for the valor_multa field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_multa;

    /**
     * The value for the valor_juros field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_juros;

    /**
     * The value for the valor_desconto field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_desconto;

    /**
     * The value for the valor_total field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_total;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * @var        ChildBaixa
     */
    protected $aBaixa;

    /**
     * @var        ChildCompetencia
     */
    protected $aCompetencia;

    /**
     * @var        ChildContaCaixa
     */
    protected $aContaCaixa;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildBoleto[] Collection to store aggregation of ChildBoleto objects.
     */
    protected $collBoletos;
    protected $collBoletosPartial;

    /**
     * @var        ObjectCollection|ChildBoletoBaixaHistorico[] Collection to store aggregation of ChildBoletoBaixaHistorico objects.
     */
    protected $collBoletoBaixaHistoricos;
    protected $collBoletoBaixaHistoricosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoleto[]
     */
    protected $boletosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoletoBaixaHistorico[]
     */
    protected $boletoBaixaHistoricosScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->valor_original = '0.00';
        $this->valor_multa = '0.00';
        $this->valor_juros = '0.00';
        $this->valor_desconto = '0.00';
        $this->valor_total = '0.00';
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\BaixaEstorno object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>BaixaEstorno</code> instance.  If
     * <code>obj</code> is an instance of <code>BaixaEstorno</code>, delegates to
     * <code>equals(BaixaEstorno)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|BaixaEstorno The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idbaixa_estorno] column value.
     *
     * @return int
     */
    public function getIdbaixaEstorno()
    {
        return $this->idbaixa_estorno;
    }

    /**
     * Get the [baixa_id] column value.
     *
     * @return int
     */
    public function getBaixaId()
    {
        return $this->baixa_id;
    }

    /**
     * Get the [conta_caixa_id] column value.
     *
     * @return int
     */
    public function getContaCaixaId()
    {
        return $this->conta_caixa_id;
    }

    /**
     * Get the [competencia_id] column value.
     *
     * @return int
     */
    public function getCompetenciaId()
    {
        return $this->competencia_id;
    }

    /**
     * Get the [usuario_estorno] column value.
     *
     * @return int
     */
    public function getUsuarioEstorno()
    {
        return $this->usuario_estorno;
    }

    /**
     * Get the [optionally formatted] temporal [data_estorno] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataEstorno($format = NULL)
    {
        if ($format === null) {
            return $this->data_estorno;
        } else {
            return $this->data_estorno instanceof \DateTimeInterface ? $this->data_estorno->format($format) : null;
        }
    }

    /**
     * Get the [descricao_movimento] column value.
     *
     * @return string
     */
    public function getDescricaoMovimento()
    {
        return $this->descricao_movimento;
    }

    /**
     * Get the [valor_original] column value.
     *
     * @return string
     */
    public function getValorOriginal()
    {
        return $this->valor_original;
    }

    /**
     * Get the [valor_multa] column value.
     *
     * @return string
     */
    public function getValorMulta()
    {
        return $this->valor_multa;
    }

    /**
     * Get the [valor_juros] column value.
     *
     * @return string
     */
    public function getValorJuros()
    {
        return $this->valor_juros;
    }

    /**
     * Get the [valor_desconto] column value.
     *
     * @return string
     */
    public function getValorDesconto()
    {
        return $this->valor_desconto;
    }

    /**
     * Get the [valor_total] column value.
     *
     * @return string
     */
    public function getValorTotal()
    {
        return $this->valor_total;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Set the value of [idbaixa_estorno] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setIdbaixaEstorno($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idbaixa_estorno !== $v) {
            $this->idbaixa_estorno = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO] = true;
        }

        return $this;
    } // setIdbaixaEstorno()

    /**
     * Set the value of [baixa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setBaixaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->baixa_id !== $v) {
            $this->baixa_id = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_BAIXA_ID] = true;
        }

        if ($this->aBaixa !== null && $this->aBaixa->getIdbaixa() !== $v) {
            $this->aBaixa = null;
        }

        return $this;
    } // setBaixaId()

    /**
     * Set the value of [conta_caixa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setContaCaixaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->conta_caixa_id !== $v) {
            $this->conta_caixa_id = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_CONTA_CAIXA_ID] = true;
        }

        if ($this->aContaCaixa !== null && $this->aContaCaixa->getIdcontaCaixa() !== $v) {
            $this->aContaCaixa = null;
        }

        return $this;
    } // setContaCaixaId()

    /**
     * Set the value of [competencia_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setCompetenciaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->competencia_id !== $v) {
            $this->competencia_id = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_COMPETENCIA_ID] = true;
        }

        if ($this->aCompetencia !== null && $this->aCompetencia->getId() !== $v) {
            $this->aCompetencia = null;
        }

        return $this;
    } // setCompetenciaId()

    /**
     * Set the value of [usuario_estorno] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setUsuarioEstorno($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_estorno !== $v) {
            $this->usuario_estorno = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_USUARIO_ESTORNO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioEstorno()

    /**
     * Sets the value of [data_estorno] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setDataEstorno($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_estorno !== null || $dt !== null) {
            if ($this->data_estorno === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_estorno->format("Y-m-d H:i:s.u")) {
                $this->data_estorno = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BaixaEstornoTableMap::COL_DATA_ESTORNO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataEstorno()

    /**
     * Set the value of [descricao_movimento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setDescricaoMovimento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao_movimento !== $v) {
            $this->descricao_movimento = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_DESCRICAO_MOVIMENTO] = true;
        }

        return $this;
    } // setDescricaoMovimento()

    /**
     * Set the value of [valor_original] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setValorOriginal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_original !== $v) {
            $this->valor_original = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_VALOR_ORIGINAL] = true;
        }

        return $this;
    } // setValorOriginal()

    /**
     * Set the value of [valor_multa] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setValorMulta($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_multa !== $v) {
            $this->valor_multa = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_VALOR_MULTA] = true;
        }

        return $this;
    } // setValorMulta()

    /**
     * Set the value of [valor_juros] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setValorJuros($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_juros !== $v) {
            $this->valor_juros = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_VALOR_JUROS] = true;
        }

        return $this;
    } // setValorJuros()

    /**
     * Set the value of [valor_desconto] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setValorDesconto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_desconto !== $v) {
            $this->valor_desconto = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_VALOR_DESCONTO] = true;
        }

        return $this;
    } // setValorDesconto()

    /**
     * Set the value of [valor_total] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setValorTotal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_total !== $v) {
            $this->valor_total = $v;
            $this->modifiedColumns[BaixaEstornoTableMap::COL_VALOR_TOTAL] = true;
        }

        return $this;
    } // setValorTotal()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BaixaEstornoTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BaixaEstornoTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->valor_original !== '0.00') {
                return false;
            }

            if ($this->valor_multa !== '0.00') {
                return false;
            }

            if ($this->valor_juros !== '0.00') {
                return false;
            }

            if ($this->valor_desconto !== '0.00') {
                return false;
            }

            if ($this->valor_total !== '0.00') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : BaixaEstornoTableMap::translateFieldName('IdbaixaEstorno', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idbaixa_estorno = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : BaixaEstornoTableMap::translateFieldName('BaixaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->baixa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : BaixaEstornoTableMap::translateFieldName('ContaCaixaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->conta_caixa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : BaixaEstornoTableMap::translateFieldName('CompetenciaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->competencia_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : BaixaEstornoTableMap::translateFieldName('UsuarioEstorno', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_estorno = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : BaixaEstornoTableMap::translateFieldName('DataEstorno', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_estorno = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : BaixaEstornoTableMap::translateFieldName('DescricaoMovimento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao_movimento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : BaixaEstornoTableMap::translateFieldName('ValorOriginal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_original = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : BaixaEstornoTableMap::translateFieldName('ValorMulta', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_multa = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : BaixaEstornoTableMap::translateFieldName('ValorJuros', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_juros = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : BaixaEstornoTableMap::translateFieldName('ValorDesconto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_desconto = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : BaixaEstornoTableMap::translateFieldName('ValorTotal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_total = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : BaixaEstornoTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : BaixaEstornoTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = BaixaEstornoTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\BaixaEstorno'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aBaixa !== null && $this->baixa_id !== $this->aBaixa->getIdbaixa()) {
            $this->aBaixa = null;
        }
        if ($this->aContaCaixa !== null && $this->conta_caixa_id !== $this->aContaCaixa->getIdcontaCaixa()) {
            $this->aContaCaixa = null;
        }
        if ($this->aCompetencia !== null && $this->competencia_id !== $this->aCompetencia->getId()) {
            $this->aCompetencia = null;
        }
        if ($this->aUsuario !== null && $this->usuario_estorno !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BaixaEstornoTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildBaixaEstornoQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBaixa = null;
            $this->aCompetencia = null;
            $this->aContaCaixa = null;
            $this->aUsuario = null;
            $this->collBoletos = null;

            $this->collBoletoBaixaHistoricos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see BaixaEstorno::setDeleted()
     * @see BaixaEstorno::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaEstornoTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildBaixaEstornoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaEstornoTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BaixaEstornoTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBaixa !== null) {
                if ($this->aBaixa->isModified() || $this->aBaixa->isNew()) {
                    $affectedRows += $this->aBaixa->save($con);
                }
                $this->setBaixa($this->aBaixa);
            }

            if ($this->aCompetencia !== null) {
                if ($this->aCompetencia->isModified() || $this->aCompetencia->isNew()) {
                    $affectedRows += $this->aCompetencia->save($con);
                }
                $this->setCompetencia($this->aCompetencia);
            }

            if ($this->aContaCaixa !== null) {
                if ($this->aContaCaixa->isModified() || $this->aContaCaixa->isNew()) {
                    $affectedRows += $this->aContaCaixa->save($con);
                }
                $this->setContaCaixa($this->aContaCaixa);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->boletosScheduledForDeletion !== null) {
                if (!$this->boletosScheduledForDeletion->isEmpty()) {
                    foreach ($this->boletosScheduledForDeletion as $boleto) {
                        // need to save related object because we set the relation to null
                        $boleto->save($con);
                    }
                    $this->boletosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletos !== null) {
                foreach ($this->collBoletos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->boletoBaixaHistoricosScheduledForDeletion !== null) {
                if (!$this->boletoBaixaHistoricosScheduledForDeletion->isEmpty()) {
                    foreach ($this->boletoBaixaHistoricosScheduledForDeletion as $boletoBaixaHistorico) {
                        // need to save related object because we set the relation to null
                        $boletoBaixaHistorico->save($con);
                    }
                    $this->boletoBaixaHistoricosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletoBaixaHistoricos !== null) {
                foreach ($this->collBoletoBaixaHistoricos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO] = true;
        if (null !== $this->idbaixa_estorno) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO)) {
            $modifiedColumns[':p' . $index++]  = 'idbaixa_estorno';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_BAIXA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'baixa_id';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_CONTA_CAIXA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'conta_caixa_id';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_COMPETENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'competencia_id';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_USUARIO_ESTORNO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_estorno';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_DATA_ESTORNO)) {
            $modifiedColumns[':p' . $index++]  = 'data_estorno';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_DESCRICAO_MOVIMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'descricao_movimento';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_ORIGINAL)) {
            $modifiedColumns[':p' . $index++]  = 'valor_original';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_MULTA)) {
            $modifiedColumns[':p' . $index++]  = 'valor_multa';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_JUROS)) {
            $modifiedColumns[':p' . $index++]  = 'valor_juros';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_DESCONTO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_desconto';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_TOTAL)) {
            $modifiedColumns[':p' . $index++]  = 'valor_total';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }

        $sql = sprintf(
            'INSERT INTO baixa_estorno (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idbaixa_estorno':
                        $stmt->bindValue($identifier, $this->idbaixa_estorno, PDO::PARAM_INT);
                        break;
                    case 'baixa_id':
                        $stmt->bindValue($identifier, $this->baixa_id, PDO::PARAM_INT);
                        break;
                    case 'conta_caixa_id':
                        $stmt->bindValue($identifier, $this->conta_caixa_id, PDO::PARAM_INT);
                        break;
                    case 'competencia_id':
                        $stmt->bindValue($identifier, $this->competencia_id, PDO::PARAM_INT);
                        break;
                    case 'usuario_estorno':
                        $stmt->bindValue($identifier, $this->usuario_estorno, PDO::PARAM_INT);
                        break;
                    case 'data_estorno':
                        $stmt->bindValue($identifier, $this->data_estorno ? $this->data_estorno->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'descricao_movimento':
                        $stmt->bindValue($identifier, $this->descricao_movimento, PDO::PARAM_STR);
                        break;
                    case 'valor_original':
                        $stmt->bindValue($identifier, $this->valor_original, PDO::PARAM_STR);
                        break;
                    case 'valor_multa':
                        $stmt->bindValue($identifier, $this->valor_multa, PDO::PARAM_STR);
                        break;
                    case 'valor_juros':
                        $stmt->bindValue($identifier, $this->valor_juros, PDO::PARAM_STR);
                        break;
                    case 'valor_desconto':
                        $stmt->bindValue($identifier, $this->valor_desconto, PDO::PARAM_STR);
                        break;
                    case 'valor_total':
                        $stmt->bindValue($identifier, $this->valor_total, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdbaixaEstorno($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BaixaEstornoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdbaixaEstorno();
                break;
            case 1:
                return $this->getBaixaId();
                break;
            case 2:
                return $this->getContaCaixaId();
                break;
            case 3:
                return $this->getCompetenciaId();
                break;
            case 4:
                return $this->getUsuarioEstorno();
                break;
            case 5:
                return $this->getDataEstorno();
                break;
            case 6:
                return $this->getDescricaoMovimento();
                break;
            case 7:
                return $this->getValorOriginal();
                break;
            case 8:
                return $this->getValorMulta();
                break;
            case 9:
                return $this->getValorJuros();
                break;
            case 10:
                return $this->getValorDesconto();
                break;
            case 11:
                return $this->getValorTotal();
                break;
            case 12:
                return $this->getDataCadastro();
                break;
            case 13:
                return $this->getDataAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['BaixaEstorno'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['BaixaEstorno'][$this->hashCode()] = true;
        $keys = BaixaEstornoTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdbaixaEstorno(),
            $keys[1] => $this->getBaixaId(),
            $keys[2] => $this->getContaCaixaId(),
            $keys[3] => $this->getCompetenciaId(),
            $keys[4] => $this->getUsuarioEstorno(),
            $keys[5] => $this->getDataEstorno(),
            $keys[6] => $this->getDescricaoMovimento(),
            $keys[7] => $this->getValorOriginal(),
            $keys[8] => $this->getValorMulta(),
            $keys[9] => $this->getValorJuros(),
            $keys[10] => $this->getValorDesconto(),
            $keys[11] => $this->getValorTotal(),
            $keys[12] => $this->getDataCadastro(),
            $keys[13] => $this->getDataAlterado(),
        );
        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[12]] instanceof \DateTime) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBaixa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixa';
                        break;
                    default:
                        $key = 'Baixa';
                }

                $result[$key] = $this->aBaixa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCompetencia) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'competencia';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'competencia';
                        break;
                    default:
                        $key = 'Competencia';
                }

                $result[$key] = $this->aCompetencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aContaCaixa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contaCaixa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'conta_caixa';
                        break;
                    default:
                        $key = 'ContaCaixa';
                }

                $result[$key] = $this->aContaCaixa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBoletos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boletos';
                        break;
                    default:
                        $key = 'Boletos';
                }

                $result[$key] = $this->collBoletos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBoletoBaixaHistoricos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoBaixaHistoricos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_baixa_historicos';
                        break;
                    default:
                        $key = 'BoletoBaixaHistoricos';
                }

                $result[$key] = $this->collBoletoBaixaHistoricos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BaixaEstornoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdbaixaEstorno($value);
                break;
            case 1:
                $this->setBaixaId($value);
                break;
            case 2:
                $this->setContaCaixaId($value);
                break;
            case 3:
                $this->setCompetenciaId($value);
                break;
            case 4:
                $this->setUsuarioEstorno($value);
                break;
            case 5:
                $this->setDataEstorno($value);
                break;
            case 6:
                $this->setDescricaoMovimento($value);
                break;
            case 7:
                $this->setValorOriginal($value);
                break;
            case 8:
                $this->setValorMulta($value);
                break;
            case 9:
                $this->setValorJuros($value);
                break;
            case 10:
                $this->setValorDesconto($value);
                break;
            case 11:
                $this->setValorTotal($value);
                break;
            case 12:
                $this->setDataCadastro($value);
                break;
            case 13:
                $this->setDataAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = BaixaEstornoTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdbaixaEstorno($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBaixaId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setContaCaixaId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCompetenciaId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUsuarioEstorno($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDataEstorno($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDescricaoMovimento($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setValorOriginal($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setValorMulta($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setValorJuros($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setValorDesconto($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setValorTotal($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setDataCadastro($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setDataAlterado($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BaixaEstornoTableMap::DATABASE_NAME);

        if ($this->isColumnModified(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO)) {
            $criteria->add(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $this->idbaixa_estorno);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_BAIXA_ID)) {
            $criteria->add(BaixaEstornoTableMap::COL_BAIXA_ID, $this->baixa_id);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_CONTA_CAIXA_ID)) {
            $criteria->add(BaixaEstornoTableMap::COL_CONTA_CAIXA_ID, $this->conta_caixa_id);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_COMPETENCIA_ID)) {
            $criteria->add(BaixaEstornoTableMap::COL_COMPETENCIA_ID, $this->competencia_id);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_USUARIO_ESTORNO)) {
            $criteria->add(BaixaEstornoTableMap::COL_USUARIO_ESTORNO, $this->usuario_estorno);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_DATA_ESTORNO)) {
            $criteria->add(BaixaEstornoTableMap::COL_DATA_ESTORNO, $this->data_estorno);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_DESCRICAO_MOVIMENTO)) {
            $criteria->add(BaixaEstornoTableMap::COL_DESCRICAO_MOVIMENTO, $this->descricao_movimento);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_ORIGINAL)) {
            $criteria->add(BaixaEstornoTableMap::COL_VALOR_ORIGINAL, $this->valor_original);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_MULTA)) {
            $criteria->add(BaixaEstornoTableMap::COL_VALOR_MULTA, $this->valor_multa);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_JUROS)) {
            $criteria->add(BaixaEstornoTableMap::COL_VALOR_JUROS, $this->valor_juros);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_DESCONTO)) {
            $criteria->add(BaixaEstornoTableMap::COL_VALOR_DESCONTO, $this->valor_desconto);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_VALOR_TOTAL)) {
            $criteria->add(BaixaEstornoTableMap::COL_VALOR_TOTAL, $this->valor_total);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(BaixaEstornoTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(BaixaEstornoTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(BaixaEstornoTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildBaixaEstornoQuery::create();
        $criteria->add(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $this->idbaixa_estorno);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdbaixaEstorno();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdbaixaEstorno();
    }

    /**
     * Generic method to set the primary key (idbaixa_estorno column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdbaixaEstorno($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdbaixaEstorno();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\BaixaEstorno (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBaixaId($this->getBaixaId());
        $copyObj->setContaCaixaId($this->getContaCaixaId());
        $copyObj->setCompetenciaId($this->getCompetenciaId());
        $copyObj->setUsuarioEstorno($this->getUsuarioEstorno());
        $copyObj->setDataEstorno($this->getDataEstorno());
        $copyObj->setDescricaoMovimento($this->getDescricaoMovimento());
        $copyObj->setValorOriginal($this->getValorOriginal());
        $copyObj->setValorMulta($this->getValorMulta());
        $copyObj->setValorJuros($this->getValorJuros());
        $copyObj->setValorDesconto($this->getValorDesconto());
        $copyObj->setValorTotal($this->getValorTotal());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBoletos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoleto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBoletoBaixaHistoricos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoletoBaixaHistorico($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdbaixaEstorno(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\BaixaEstorno Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBaixa object.
     *
     * @param  ChildBaixa $v
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBaixa(ChildBaixa $v = null)
    {
        if ($v === null) {
            $this->setBaixaId(NULL);
        } else {
            $this->setBaixaId($v->getIdbaixa());
        }

        $this->aBaixa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBaixa object, it will not be re-added.
        if ($v !== null) {
            $v->addBaixaEstorno($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBaixa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBaixa The associated ChildBaixa object.
     * @throws PropelException
     */
    public function getBaixa(ConnectionInterface $con = null)
    {
        if ($this->aBaixa === null && ($this->baixa_id !== null)) {
            $this->aBaixa = ChildBaixaQuery::create()->findPk($this->baixa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBaixa->addBaixaEstornos($this);
             */
        }

        return $this->aBaixa;
    }

    /**
     * Declares an association between this object and a ChildCompetencia object.
     *
     * @param  ChildCompetencia $v
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCompetencia(ChildCompetencia $v = null)
    {
        if ($v === null) {
            $this->setCompetenciaId(NULL);
        } else {
            $this->setCompetenciaId($v->getId());
        }

        $this->aCompetencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCompetencia object, it will not be re-added.
        if ($v !== null) {
            $v->addBaixaEstorno($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCompetencia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCompetencia The associated ChildCompetencia object.
     * @throws PropelException
     */
    public function getCompetencia(ConnectionInterface $con = null)
    {
        if ($this->aCompetencia === null && ($this->competencia_id !== null)) {
            $this->aCompetencia = ChildCompetenciaQuery::create()->findPk($this->competencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCompetencia->addBaixaEstornos($this);
             */
        }

        return $this->aCompetencia;
    }

    /**
     * Declares an association between this object and a ChildContaCaixa object.
     *
     * @param  ChildContaCaixa $v
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     * @throws PropelException
     */
    public function setContaCaixa(ChildContaCaixa $v = null)
    {
        if ($v === null) {
            $this->setContaCaixaId(NULL);
        } else {
            $this->setContaCaixaId($v->getIdcontaCaixa());
        }

        $this->aContaCaixa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildContaCaixa object, it will not be re-added.
        if ($v !== null) {
            $v->addBaixaEstorno($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildContaCaixa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildContaCaixa The associated ChildContaCaixa object.
     * @throws PropelException
     */
    public function getContaCaixa(ConnectionInterface $con = null)
    {
        if ($this->aContaCaixa === null && ($this->conta_caixa_id !== null)) {
            $this->aContaCaixa = ChildContaCaixaQuery::create()->findPk($this->conta_caixa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aContaCaixa->addBaixaEstornos($this);
             */
        }

        return $this->aContaCaixa;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioEstorno(NULL);
        } else {
            $this->setUsuarioEstorno($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addBaixaEstorno($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_estorno !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_estorno, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addBaixaEstornos($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Boleto' == $relationName) {
            return $this->initBoletos();
        }
        if ('BoletoBaixaHistorico' == $relationName) {
            return $this->initBoletoBaixaHistoricos();
        }
    }

    /**
     * Clears out the collBoletos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletos()
     */
    public function clearBoletos()
    {
        $this->collBoletos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletos collection loaded partially.
     */
    public function resetPartialBoletos($v = true)
    {
        $this->collBoletosPartial = $v;
    }

    /**
     * Initializes the collBoletos collection.
     *
     * By default this just sets the collBoletos collection to an empty array (like clearcollBoletos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletos($overrideExisting = true)
    {
        if (null !== $this->collBoletos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletos = new $collectionClassName;
        $this->collBoletos->setModel('\ImaTelecomBundle\Model\Boleto');
    }

    /**
     * Gets an array of ChildBoleto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBaixaEstorno is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     * @throws PropelException
     */
    public function getBoletos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                // return empty collection
                $this->initBoletos();
            } else {
                $collBoletos = ChildBoletoQuery::create(null, $criteria)
                    ->filterByBaixaEstorno($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletosPartial && count($collBoletos)) {
                        $this->initBoletos(false);

                        foreach ($collBoletos as $obj) {
                            if (false == $this->collBoletos->contains($obj)) {
                                $this->collBoletos->append($obj);
                            }
                        }

                        $this->collBoletosPartial = true;
                    }

                    return $collBoletos;
                }

                if ($partial && $this->collBoletos) {
                    foreach ($this->collBoletos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletos[] = $obj;
                        }
                    }
                }

                $this->collBoletos = $collBoletos;
                $this->collBoletosPartial = false;
            }
        }

        return $this->collBoletos;
    }

    /**
     * Sets a collection of ChildBoleto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBaixaEstorno The current object (for fluent API support)
     */
    public function setBoletos(Collection $boletos, ConnectionInterface $con = null)
    {
        /** @var ChildBoleto[] $boletosToDelete */
        $boletosToDelete = $this->getBoletos(new Criteria(), $con)->diff($boletos);


        $this->boletosScheduledForDeletion = $boletosToDelete;

        foreach ($boletosToDelete as $boletoRemoved) {
            $boletoRemoved->setBaixaEstorno(null);
        }

        $this->collBoletos = null;
        foreach ($boletos as $boleto) {
            $this->addBoleto($boleto);
        }

        $this->collBoletos = $boletos;
        $this->collBoletosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Boleto objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Boleto objects.
     * @throws PropelException
     */
    public function countBoletos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletos());
            }

            $query = ChildBoletoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBaixaEstorno($this)
                ->count($con);
        }

        return count($this->collBoletos);
    }

    /**
     * Method called to associate a ChildBoleto object to this object
     * through the ChildBoleto foreign key attribute.
     *
     * @param  ChildBoleto $l ChildBoleto
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function addBoleto(ChildBoleto $l)
    {
        if ($this->collBoletos === null) {
            $this->initBoletos();
            $this->collBoletosPartial = true;
        }

        if (!$this->collBoletos->contains($l)) {
            $this->doAddBoleto($l);

            if ($this->boletosScheduledForDeletion and $this->boletosScheduledForDeletion->contains($l)) {
                $this->boletosScheduledForDeletion->remove($this->boletosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoleto $boleto The ChildBoleto object to add.
     */
    protected function doAddBoleto(ChildBoleto $boleto)
    {
        $this->collBoletos[]= $boleto;
        $boleto->setBaixaEstorno($this);
    }

    /**
     * @param  ChildBoleto $boleto The ChildBoleto object to remove.
     * @return $this|ChildBaixaEstorno The current object (for fluent API support)
     */
    public function removeBoleto(ChildBoleto $boleto)
    {
        if ($this->getBoletos()->contains($boleto)) {
            $pos = $this->collBoletos->search($boleto);
            $this->collBoletos->remove($pos);
            if (null === $this->boletosScheduledForDeletion) {
                $this->boletosScheduledForDeletion = clone $this->collBoletos;
                $this->boletosScheduledForDeletion->clear();
            }
            $this->boletosScheduledForDeletion[]= $boleto;
            $boleto->setBaixaEstorno(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinFornecedor(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Fornecedor', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletos($query, $con);
    }

    /**
     * Clears out the collBoletoBaixaHistoricos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletoBaixaHistoricos()
     */
    public function clearBoletoBaixaHistoricos()
    {
        $this->collBoletoBaixaHistoricos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletoBaixaHistoricos collection loaded partially.
     */
    public function resetPartialBoletoBaixaHistoricos($v = true)
    {
        $this->collBoletoBaixaHistoricosPartial = $v;
    }

    /**
     * Initializes the collBoletoBaixaHistoricos collection.
     *
     * By default this just sets the collBoletoBaixaHistoricos collection to an empty array (like clearcollBoletoBaixaHistoricos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletoBaixaHistoricos($overrideExisting = true)
    {
        if (null !== $this->collBoletoBaixaHistoricos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoBaixaHistoricoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletoBaixaHistoricos = new $collectionClassName;
        $this->collBoletoBaixaHistoricos->setModel('\ImaTelecomBundle\Model\BoletoBaixaHistorico');
    }

    /**
     * Gets an array of ChildBoletoBaixaHistorico objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBaixaEstorno is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     * @throws PropelException
     */
    public function getBoletoBaixaHistoricos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                // return empty collection
                $this->initBoletoBaixaHistoricos();
            } else {
                $collBoletoBaixaHistoricos = ChildBoletoBaixaHistoricoQuery::create(null, $criteria)
                    ->filterByBaixaEstorno($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletoBaixaHistoricosPartial && count($collBoletoBaixaHistoricos)) {
                        $this->initBoletoBaixaHistoricos(false);

                        foreach ($collBoletoBaixaHistoricos as $obj) {
                            if (false == $this->collBoletoBaixaHistoricos->contains($obj)) {
                                $this->collBoletoBaixaHistoricos->append($obj);
                            }
                        }

                        $this->collBoletoBaixaHistoricosPartial = true;
                    }

                    return $collBoletoBaixaHistoricos;
                }

                if ($partial && $this->collBoletoBaixaHistoricos) {
                    foreach ($this->collBoletoBaixaHistoricos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletoBaixaHistoricos[] = $obj;
                        }
                    }
                }

                $this->collBoletoBaixaHistoricos = $collBoletoBaixaHistoricos;
                $this->collBoletoBaixaHistoricosPartial = false;
            }
        }

        return $this->collBoletoBaixaHistoricos;
    }

    /**
     * Sets a collection of ChildBoletoBaixaHistorico objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletoBaixaHistoricos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBaixaEstorno The current object (for fluent API support)
     */
    public function setBoletoBaixaHistoricos(Collection $boletoBaixaHistoricos, ConnectionInterface $con = null)
    {
        /** @var ChildBoletoBaixaHistorico[] $boletoBaixaHistoricosToDelete */
        $boletoBaixaHistoricosToDelete = $this->getBoletoBaixaHistoricos(new Criteria(), $con)->diff($boletoBaixaHistoricos);


        $this->boletoBaixaHistoricosScheduledForDeletion = $boletoBaixaHistoricosToDelete;

        foreach ($boletoBaixaHistoricosToDelete as $boletoBaixaHistoricoRemoved) {
            $boletoBaixaHistoricoRemoved->setBaixaEstorno(null);
        }

        $this->collBoletoBaixaHistoricos = null;
        foreach ($boletoBaixaHistoricos as $boletoBaixaHistorico) {
            $this->addBoletoBaixaHistorico($boletoBaixaHistorico);
        }

        $this->collBoletoBaixaHistoricos = $boletoBaixaHistoricos;
        $this->collBoletoBaixaHistoricosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BoletoBaixaHistorico objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BoletoBaixaHistorico objects.
     * @throws PropelException
     */
    public function countBoletoBaixaHistoricos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletoBaixaHistoricos());
            }

            $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBaixaEstorno($this)
                ->count($con);
        }

        return count($this->collBoletoBaixaHistoricos);
    }

    /**
     * Method called to associate a ChildBoletoBaixaHistorico object to this object
     * through the ChildBoletoBaixaHistorico foreign key attribute.
     *
     * @param  ChildBoletoBaixaHistorico $l ChildBoletoBaixaHistorico
     * @return $this|\ImaTelecomBundle\Model\BaixaEstorno The current object (for fluent API support)
     */
    public function addBoletoBaixaHistorico(ChildBoletoBaixaHistorico $l)
    {
        if ($this->collBoletoBaixaHistoricos === null) {
            $this->initBoletoBaixaHistoricos();
            $this->collBoletoBaixaHistoricosPartial = true;
        }

        if (!$this->collBoletoBaixaHistoricos->contains($l)) {
            $this->doAddBoletoBaixaHistorico($l);

            if ($this->boletoBaixaHistoricosScheduledForDeletion and $this->boletoBaixaHistoricosScheduledForDeletion->contains($l)) {
                $this->boletoBaixaHistoricosScheduledForDeletion->remove($this->boletoBaixaHistoricosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to add.
     */
    protected function doAddBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        $this->collBoletoBaixaHistoricos[]= $boletoBaixaHistorico;
        $boletoBaixaHistorico->setBaixaEstorno($this);
    }

    /**
     * @param  ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to remove.
     * @return $this|ChildBaixaEstorno The current object (for fluent API support)
     */
    public function removeBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        if ($this->getBoletoBaixaHistoricos()->contains($boletoBaixaHistorico)) {
            $pos = $this->collBoletoBaixaHistoricos->search($boletoBaixaHistorico);
            $this->collBoletoBaixaHistoricos->remove($pos);
            if (null === $this->boletoBaixaHistoricosScheduledForDeletion) {
                $this->boletoBaixaHistoricosScheduledForDeletion = clone $this->collBoletoBaixaHistoricos;
                $this->boletoBaixaHistoricosScheduledForDeletion->clear();
            }
            $this->boletoBaixaHistoricosScheduledForDeletion[]= $boletoBaixaHistorico;
            $boletoBaixaHistorico->setBaixaEstorno(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinLancamentos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Lancamentos', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BaixaEstorno is new, it will return
     * an empty collection; or if this BaixaEstorno has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BaixaEstorno.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBaixa) {
            $this->aBaixa->removeBaixaEstorno($this);
        }
        if (null !== $this->aCompetencia) {
            $this->aCompetencia->removeBaixaEstorno($this);
        }
        if (null !== $this->aContaCaixa) {
            $this->aContaCaixa->removeBaixaEstorno($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeBaixaEstorno($this);
        }
        $this->idbaixa_estorno = null;
        $this->baixa_id = null;
        $this->conta_caixa_id = null;
        $this->competencia_id = null;
        $this->usuario_estorno = null;
        $this->data_estorno = null;
        $this->descricao_movimento = null;
        $this->valor_original = null;
        $this->valor_multa = null;
        $this->valor_juros = null;
        $this->valor_desconto = null;
        $this->valor_total = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBoletos) {
                foreach ($this->collBoletos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBoletoBaixaHistoricos) {
                foreach ($this->collBoletoBaixaHistoricos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBoletos = null;
        $this->collBoletoBaixaHistoricos = null;
        $this->aBaixa = null;
        $this->aCompetencia = null;
        $this->aContaCaixa = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BaixaEstornoTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
