<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ContasPagar as ChildContasPagar;
use ImaTelecomBundle\Model\ContasPagarQuery as ChildContasPagarQuery;
use ImaTelecomBundle\Model\Map\ContasPagarTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'contas_pagar' table.
 *
 *
 *
 * @method     ChildContasPagarQuery orderByIdcontasPagar($order = Criteria::ASC) Order by the idcontas_pagar column
 * @method     ChildContasPagarQuery orderByValor($order = Criteria::ASC) Order by the valor column
 * @method     ChildContasPagarQuery orderByValorFrete($order = Criteria::ASC) Order by the valor_frete column
 * @method     ChildContasPagarQuery orderByFornecedorId($order = Criteria::ASC) Order by the fornecedor_id column
 * @method     ChildContasPagarQuery orderByNumDocumento($order = Criteria::ASC) Order by the num_documento column
 * @method     ChildContasPagarQuery orderByJuros($order = Criteria::ASC) Order by the juros column
 * @method     ChildContasPagarQuery orderByMulta($order = Criteria::ASC) Order by the multa column
 * @method     ChildContasPagarQuery orderByDataEmissao($order = Criteria::ASC) Order by the data_emissao column
 * @method     ChildContasPagarQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method     ChildContasPagarQuery orderByEspecie($order = Criteria::ASC) Order by the especie column
 * @method     ChildContasPagarQuery orderByArquivo($order = Criteria::ASC) Order by the arquivo column
 * @method     ChildContasPagarQuery orderByObs($order = Criteria::ASC) Order by the obs column
 * @method     ChildContasPagarQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildContasPagarQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildContasPagarQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildContasPagarQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildContasPagarQuery groupByIdcontasPagar() Group by the idcontas_pagar column
 * @method     ChildContasPagarQuery groupByValor() Group by the valor column
 * @method     ChildContasPagarQuery groupByValorFrete() Group by the valor_frete column
 * @method     ChildContasPagarQuery groupByFornecedorId() Group by the fornecedor_id column
 * @method     ChildContasPagarQuery groupByNumDocumento() Group by the num_documento column
 * @method     ChildContasPagarQuery groupByJuros() Group by the juros column
 * @method     ChildContasPagarQuery groupByMulta() Group by the multa column
 * @method     ChildContasPagarQuery groupByDataEmissao() Group by the data_emissao column
 * @method     ChildContasPagarQuery groupByTipo() Group by the tipo column
 * @method     ChildContasPagarQuery groupByEspecie() Group by the especie column
 * @method     ChildContasPagarQuery groupByArquivo() Group by the arquivo column
 * @method     ChildContasPagarQuery groupByObs() Group by the obs column
 * @method     ChildContasPagarQuery groupByAtivo() Group by the ativo column
 * @method     ChildContasPagarQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildContasPagarQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildContasPagarQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildContasPagarQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildContasPagarQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildContasPagarQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildContasPagarQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildContasPagarQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildContasPagarQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildContasPagarQuery leftJoinFornecedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fornecedor relation
 * @method     ChildContasPagarQuery rightJoinFornecedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fornecedor relation
 * @method     ChildContasPagarQuery innerJoinFornecedor($relationAlias = null) Adds a INNER JOIN clause to the query using the Fornecedor relation
 *
 * @method     ChildContasPagarQuery joinWithFornecedor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fornecedor relation
 *
 * @method     ChildContasPagarQuery leftJoinWithFornecedor() Adds a LEFT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildContasPagarQuery rightJoinWithFornecedor() Adds a RIGHT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildContasPagarQuery innerJoinWithFornecedor() Adds a INNER JOIN clause and with to the query using the Fornecedor relation
 *
 * @method     ChildContasPagarQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildContasPagarQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildContasPagarQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildContasPagarQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildContasPagarQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildContasPagarQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildContasPagarQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildContasPagarQuery leftJoinContasPagarParcelas($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagarParcelas relation
 * @method     ChildContasPagarQuery rightJoinContasPagarParcelas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagarParcelas relation
 * @method     ChildContasPagarQuery innerJoinContasPagarParcelas($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagarParcelas relation
 *
 * @method     ChildContasPagarQuery joinWithContasPagarParcelas($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagarParcelas relation
 *
 * @method     ChildContasPagarQuery leftJoinWithContasPagarParcelas() Adds a LEFT JOIN clause and with to the query using the ContasPagarParcelas relation
 * @method     ChildContasPagarQuery rightJoinWithContasPagarParcelas() Adds a RIGHT JOIN clause and with to the query using the ContasPagarParcelas relation
 * @method     ChildContasPagarQuery innerJoinWithContasPagarParcelas() Adds a INNER JOIN clause and with to the query using the ContasPagarParcelas relation
 *
 * @method     ChildContasPagarQuery leftJoinContasPagarTributos($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagarTributos relation
 * @method     ChildContasPagarQuery rightJoinContasPagarTributos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagarTributos relation
 * @method     ChildContasPagarQuery innerJoinContasPagarTributos($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagarTributos relation
 *
 * @method     ChildContasPagarQuery joinWithContasPagarTributos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagarTributos relation
 *
 * @method     ChildContasPagarQuery leftJoinWithContasPagarTributos() Adds a LEFT JOIN clause and with to the query using the ContasPagarTributos relation
 * @method     ChildContasPagarQuery rightJoinWithContasPagarTributos() Adds a RIGHT JOIN clause and with to the query using the ContasPagarTributos relation
 * @method     ChildContasPagarQuery innerJoinWithContasPagarTributos() Adds a INNER JOIN clause and with to the query using the ContasPagarTributos relation
 *
 * @method     ChildContasPagarQuery leftJoinItemComprado($relationAlias = null) Adds a LEFT JOIN clause to the query using the ItemComprado relation
 * @method     ChildContasPagarQuery rightJoinItemComprado($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ItemComprado relation
 * @method     ChildContasPagarQuery innerJoinItemComprado($relationAlias = null) Adds a INNER JOIN clause to the query using the ItemComprado relation
 *
 * @method     ChildContasPagarQuery joinWithItemComprado($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ItemComprado relation
 *
 * @method     ChildContasPagarQuery leftJoinWithItemComprado() Adds a LEFT JOIN clause and with to the query using the ItemComprado relation
 * @method     ChildContasPagarQuery rightJoinWithItemComprado() Adds a RIGHT JOIN clause and with to the query using the ItemComprado relation
 * @method     ChildContasPagarQuery innerJoinWithItemComprado() Adds a INNER JOIN clause and with to the query using the ItemComprado relation
 *
 * @method     \ImaTelecomBundle\Model\FornecedorQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\ContasPagarParcelasQuery|\ImaTelecomBundle\Model\ContasPagarTributosQuery|\ImaTelecomBundle\Model\ItemCompradoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildContasPagar findOne(ConnectionInterface $con = null) Return the first ChildContasPagar matching the query
 * @method     ChildContasPagar findOneOrCreate(ConnectionInterface $con = null) Return the first ChildContasPagar matching the query, or a new ChildContasPagar object populated from the query conditions when no match is found
 *
 * @method     ChildContasPagar findOneByIdcontasPagar(int $idcontas_pagar) Return the first ChildContasPagar filtered by the idcontas_pagar column
 * @method     ChildContasPagar findOneByValor(double $valor) Return the first ChildContasPagar filtered by the valor column
 * @method     ChildContasPagar findOneByValorFrete(double $valor_frete) Return the first ChildContasPagar filtered by the valor_frete column
 * @method     ChildContasPagar findOneByFornecedorId(int $fornecedor_id) Return the first ChildContasPagar filtered by the fornecedor_id column
 * @method     ChildContasPagar findOneByNumDocumento(string $num_documento) Return the first ChildContasPagar filtered by the num_documento column
 * @method     ChildContasPagar findOneByJuros(double $juros) Return the first ChildContasPagar filtered by the juros column
 * @method     ChildContasPagar findOneByMulta(double $multa) Return the first ChildContasPagar filtered by the multa column
 * @method     ChildContasPagar findOneByDataEmissao(string $data_emissao) Return the first ChildContasPagar filtered by the data_emissao column
 * @method     ChildContasPagar findOneByTipo(string $tipo) Return the first ChildContasPagar filtered by the tipo column
 * @method     ChildContasPagar findOneByEspecie(string $especie) Return the first ChildContasPagar filtered by the especie column
 * @method     ChildContasPagar findOneByArquivo(string $arquivo) Return the first ChildContasPagar filtered by the arquivo column
 * @method     ChildContasPagar findOneByObs(string $obs) Return the first ChildContasPagar filtered by the obs column
 * @method     ChildContasPagar findOneByAtivo(boolean $ativo) Return the first ChildContasPagar filtered by the ativo column
 * @method     ChildContasPagar findOneByDataCadastro(string $data_cadastro) Return the first ChildContasPagar filtered by the data_cadastro column
 * @method     ChildContasPagar findOneByDataAlterado(string $data_alterado) Return the first ChildContasPagar filtered by the data_alterado column
 * @method     ChildContasPagar findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildContasPagar filtered by the usuario_alterado column *

 * @method     ChildContasPagar requirePk($key, ConnectionInterface $con = null) Return the ChildContasPagar by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOne(ConnectionInterface $con = null) Return the first ChildContasPagar matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContasPagar requireOneByIdcontasPagar(int $idcontas_pagar) Return the first ChildContasPagar filtered by the idcontas_pagar column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByValor(double $valor) Return the first ChildContasPagar filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByValorFrete(double $valor_frete) Return the first ChildContasPagar filtered by the valor_frete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByFornecedorId(int $fornecedor_id) Return the first ChildContasPagar filtered by the fornecedor_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByNumDocumento(string $num_documento) Return the first ChildContasPagar filtered by the num_documento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByJuros(double $juros) Return the first ChildContasPagar filtered by the juros column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByMulta(double $multa) Return the first ChildContasPagar filtered by the multa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByDataEmissao(string $data_emissao) Return the first ChildContasPagar filtered by the data_emissao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByTipo(string $tipo) Return the first ChildContasPagar filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByEspecie(string $especie) Return the first ChildContasPagar filtered by the especie column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByArquivo(string $arquivo) Return the first ChildContasPagar filtered by the arquivo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByObs(string $obs) Return the first ChildContasPagar filtered by the obs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByAtivo(boolean $ativo) Return the first ChildContasPagar filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByDataCadastro(string $data_cadastro) Return the first ChildContasPagar filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByDataAlterado(string $data_alterado) Return the first ChildContasPagar filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagar requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildContasPagar filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContasPagar[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildContasPagar objects based on current ModelCriteria
 * @method     ChildContasPagar[]|ObjectCollection findByIdcontasPagar(int $idcontas_pagar) Return ChildContasPagar objects filtered by the idcontas_pagar column
 * @method     ChildContasPagar[]|ObjectCollection findByValor(double $valor) Return ChildContasPagar objects filtered by the valor column
 * @method     ChildContasPagar[]|ObjectCollection findByValorFrete(double $valor_frete) Return ChildContasPagar objects filtered by the valor_frete column
 * @method     ChildContasPagar[]|ObjectCollection findByFornecedorId(int $fornecedor_id) Return ChildContasPagar objects filtered by the fornecedor_id column
 * @method     ChildContasPagar[]|ObjectCollection findByNumDocumento(string $num_documento) Return ChildContasPagar objects filtered by the num_documento column
 * @method     ChildContasPagar[]|ObjectCollection findByJuros(double $juros) Return ChildContasPagar objects filtered by the juros column
 * @method     ChildContasPagar[]|ObjectCollection findByMulta(double $multa) Return ChildContasPagar objects filtered by the multa column
 * @method     ChildContasPagar[]|ObjectCollection findByDataEmissao(string $data_emissao) Return ChildContasPagar objects filtered by the data_emissao column
 * @method     ChildContasPagar[]|ObjectCollection findByTipo(string $tipo) Return ChildContasPagar objects filtered by the tipo column
 * @method     ChildContasPagar[]|ObjectCollection findByEspecie(string $especie) Return ChildContasPagar objects filtered by the especie column
 * @method     ChildContasPagar[]|ObjectCollection findByArquivo(string $arquivo) Return ChildContasPagar objects filtered by the arquivo column
 * @method     ChildContasPagar[]|ObjectCollection findByObs(string $obs) Return ChildContasPagar objects filtered by the obs column
 * @method     ChildContasPagar[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildContasPagar objects filtered by the ativo column
 * @method     ChildContasPagar[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildContasPagar objects filtered by the data_cadastro column
 * @method     ChildContasPagar[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildContasPagar objects filtered by the data_alterado column
 * @method     ChildContasPagar[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildContasPagar objects filtered by the usuario_alterado column
 * @method     ChildContasPagar[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ContasPagarQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ContasPagarQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ContasPagar', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildContasPagarQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildContasPagarQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildContasPagarQuery) {
            return $criteria;
        }
        $query = new ChildContasPagarQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildContasPagar|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContasPagarTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ContasPagarTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagar A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcontas_pagar, valor, valor_frete, fornecedor_id, num_documento, juros, multa, data_emissao, tipo, especie, arquivo, obs, ativo, data_cadastro, data_alterado, usuario_alterado FROM contas_pagar WHERE idcontas_pagar = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildContasPagar $obj */
            $obj = new ChildContasPagar();
            $obj->hydrate($row);
            ContasPagarTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildContasPagar|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcontas_pagar column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcontasPagar(1234); // WHERE idcontas_pagar = 1234
     * $query->filterByIdcontasPagar(array(12, 34)); // WHERE idcontas_pagar IN (12, 34)
     * $query->filterByIdcontasPagar(array('min' => 12)); // WHERE idcontas_pagar > 12
     * </code>
     *
     * @param     mixed $idcontasPagar The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByIdcontasPagar($idcontasPagar = null, $comparison = null)
    {
        if (is_array($idcontasPagar)) {
            $useMinMax = false;
            if (isset($idcontasPagar['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $idcontasPagar['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcontasPagar['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $idcontasPagar['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $idcontasPagar, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query on the valor_frete column
     *
     * Example usage:
     * <code>
     * $query->filterByValorFrete(1234); // WHERE valor_frete = 1234
     * $query->filterByValorFrete(array(12, 34)); // WHERE valor_frete IN (12, 34)
     * $query->filterByValorFrete(array('min' => 12)); // WHERE valor_frete > 12
     * </code>
     *
     * @param     mixed $valorFrete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByValorFrete($valorFrete = null, $comparison = null)
    {
        if (is_array($valorFrete)) {
            $useMinMax = false;
            if (isset($valorFrete['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_VALOR_FRETE, $valorFrete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorFrete['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_VALOR_FRETE, $valorFrete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_VALOR_FRETE, $valorFrete, $comparison);
    }

    /**
     * Filter the query on the fornecedor_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFornecedorId(1234); // WHERE fornecedor_id = 1234
     * $query->filterByFornecedorId(array(12, 34)); // WHERE fornecedor_id IN (12, 34)
     * $query->filterByFornecedorId(array('min' => 12)); // WHERE fornecedor_id > 12
     * </code>
     *
     * @see       filterByFornecedor()
     *
     * @param     mixed $fornecedorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByFornecedorId($fornecedorId = null, $comparison = null)
    {
        if (is_array($fornecedorId)) {
            $useMinMax = false;
            if (isset($fornecedorId['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_FORNECEDOR_ID, $fornecedorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fornecedorId['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_FORNECEDOR_ID, $fornecedorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_FORNECEDOR_ID, $fornecedorId, $comparison);
    }

    /**
     * Filter the query on the num_documento column
     *
     * Example usage:
     * <code>
     * $query->filterByNumDocumento('fooValue');   // WHERE num_documento = 'fooValue'
     * $query->filterByNumDocumento('%fooValue%', Criteria::LIKE); // WHERE num_documento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numDocumento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByNumDocumento($numDocumento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numDocumento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_NUM_DOCUMENTO, $numDocumento, $comparison);
    }

    /**
     * Filter the query on the juros column
     *
     * Example usage:
     * <code>
     * $query->filterByJuros(1234); // WHERE juros = 1234
     * $query->filterByJuros(array(12, 34)); // WHERE juros IN (12, 34)
     * $query->filterByJuros(array('min' => 12)); // WHERE juros > 12
     * </code>
     *
     * @param     mixed $juros The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByJuros($juros = null, $comparison = null)
    {
        if (is_array($juros)) {
            $useMinMax = false;
            if (isset($juros['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_JUROS, $juros['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($juros['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_JUROS, $juros['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_JUROS, $juros, $comparison);
    }

    /**
     * Filter the query on the multa column
     *
     * Example usage:
     * <code>
     * $query->filterByMulta(1234); // WHERE multa = 1234
     * $query->filterByMulta(array(12, 34)); // WHERE multa IN (12, 34)
     * $query->filterByMulta(array('min' => 12)); // WHERE multa > 12
     * </code>
     *
     * @param     mixed $multa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByMulta($multa = null, $comparison = null)
    {
        if (is_array($multa)) {
            $useMinMax = false;
            if (isset($multa['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_MULTA, $multa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($multa['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_MULTA, $multa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_MULTA, $multa, $comparison);
    }

    /**
     * Filter the query on the data_emissao column
     *
     * Example usage:
     * <code>
     * $query->filterByDataEmissao('2011-03-14'); // WHERE data_emissao = '2011-03-14'
     * $query->filterByDataEmissao('now'); // WHERE data_emissao = '2011-03-14'
     * $query->filterByDataEmissao(array('max' => 'yesterday')); // WHERE data_emissao > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataEmissao The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByDataEmissao($dataEmissao = null, $comparison = null)
    {
        if (is_array($dataEmissao)) {
            $useMinMax = false;
            if (isset($dataEmissao['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_DATA_EMISSAO, $dataEmissao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataEmissao['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_DATA_EMISSAO, $dataEmissao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_DATA_EMISSAO, $dataEmissao, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the especie column
     *
     * Example usage:
     * <code>
     * $query->filterByEspecie('fooValue');   // WHERE especie = 'fooValue'
     * $query->filterByEspecie('%fooValue%', Criteria::LIKE); // WHERE especie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $especie The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByEspecie($especie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($especie)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_ESPECIE, $especie, $comparison);
    }

    /**
     * Filter the query on the arquivo column
     *
     * Example usage:
     * <code>
     * $query->filterByArquivo('fooValue');   // WHERE arquivo = 'fooValue'
     * $query->filterByArquivo('%fooValue%', Criteria::LIKE); // WHERE arquivo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $arquivo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByArquivo($arquivo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($arquivo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_ARQUIVO, $arquivo, $comparison);
    }

    /**
     * Filter the query on the obs column
     *
     * Example usage:
     * <code>
     * $query->filterByObs('fooValue');   // WHERE obs = 'fooValue'
     * $query->filterByObs('%fooValue%', Criteria::LIKE); // WHERE obs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $obs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByObs($obs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($obs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_OBS, $obs, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ContasPagarTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Fornecedor object
     *
     * @param \ImaTelecomBundle\Model\Fornecedor|ObjectCollection $fornecedor The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByFornecedor($fornecedor, $comparison = null)
    {
        if ($fornecedor instanceof \ImaTelecomBundle\Model\Fornecedor) {
            return $this
                ->addUsingAlias(ContasPagarTableMap::COL_FORNECEDOR_ID, $fornecedor->getIdfornecedor(), $comparison);
        } elseif ($fornecedor instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContasPagarTableMap::COL_FORNECEDOR_ID, $fornecedor->toKeyValue('PrimaryKey', 'Idfornecedor'), $comparison);
        } else {
            throw new PropelException('filterByFornecedor() only accepts arguments of type \ImaTelecomBundle\Model\Fornecedor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fornecedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function joinFornecedor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fornecedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fornecedor');
        }

        return $this;
    }

    /**
     * Use the Fornecedor relation Fornecedor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FornecedorQuery A secondary query class using the current class as primary query
     */
    public function useFornecedorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFornecedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fornecedor', '\ImaTelecomBundle\Model\FornecedorQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ContasPagarTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContasPagarTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagarParcelas object
     *
     * @param \ImaTelecomBundle\Model\ContasPagarParcelas|ObjectCollection $contasPagarParcelas the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByContasPagarParcelas($contasPagarParcelas, $comparison = null)
    {
        if ($contasPagarParcelas instanceof \ImaTelecomBundle\Model\ContasPagarParcelas) {
            return $this
                ->addUsingAlias(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $contasPagarParcelas->getContaPagarId(), $comparison);
        } elseif ($contasPagarParcelas instanceof ObjectCollection) {
            return $this
                ->useContasPagarParcelasQuery()
                ->filterByPrimaryKeys($contasPagarParcelas->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContasPagarParcelas() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagarParcelas or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagarParcelas relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function joinContasPagarParcelas($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagarParcelas');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagarParcelas');
        }

        return $this;
    }

    /**
     * Use the ContasPagarParcelas relation ContasPagarParcelas object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarParcelasQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarParcelasQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContasPagarParcelas($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagarParcelas', '\ImaTelecomBundle\Model\ContasPagarParcelasQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagarTributos object
     *
     * @param \ImaTelecomBundle\Model\ContasPagarTributos|ObjectCollection $contasPagarTributos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByContasPagarTributos($contasPagarTributos, $comparison = null)
    {
        if ($contasPagarTributos instanceof \ImaTelecomBundle\Model\ContasPagarTributos) {
            return $this
                ->addUsingAlias(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $contasPagarTributos->getContaPagarId(), $comparison);
        } elseif ($contasPagarTributos instanceof ObjectCollection) {
            return $this
                ->useContasPagarTributosQuery()
                ->filterByPrimaryKeys($contasPagarTributos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContasPagarTributos() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagarTributos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagarTributos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function joinContasPagarTributos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagarTributos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagarTributos');
        }

        return $this;
    }

    /**
     * Use the ContasPagarTributos relation ContasPagarTributos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarTributosQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarTributosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContasPagarTributos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagarTributos', '\ImaTelecomBundle\Model\ContasPagarTributosQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ItemComprado object
     *
     * @param \ImaTelecomBundle\Model\ItemComprado|ObjectCollection $itemComprado the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildContasPagarQuery The current query, for fluid interface
     */
    public function filterByItemComprado($itemComprado, $comparison = null)
    {
        if ($itemComprado instanceof \ImaTelecomBundle\Model\ItemComprado) {
            return $this
                ->addUsingAlias(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $itemComprado->getContasPagarId(), $comparison);
        } elseif ($itemComprado instanceof ObjectCollection) {
            return $this
                ->useItemCompradoQuery()
                ->filterByPrimaryKeys($itemComprado->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByItemComprado() only accepts arguments of type \ImaTelecomBundle\Model\ItemComprado or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ItemComprado relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function joinItemComprado($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ItemComprado');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ItemComprado');
        }

        return $this;
    }

    /**
     * Use the ItemComprado relation ItemComprado object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ItemCompradoQuery A secondary query class using the current class as primary query
     */
    public function useItemCompradoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinItemComprado($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ItemComprado', '\ImaTelecomBundle\Model\ItemCompradoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildContasPagar $contasPagar Object to remove from the list of results
     *
     * @return $this|ChildContasPagarQuery The current query, for fluid interface
     */
    public function prune($contasPagar = null)
    {
        if ($contasPagar) {
            $this->addUsingAlias(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $contasPagar->getIdcontasPagar(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the contas_pagar table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ContasPagarTableMap::clearInstancePool();
            ContasPagarTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ContasPagarTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ContasPagarTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ContasPagarTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ContasPagarQuery
