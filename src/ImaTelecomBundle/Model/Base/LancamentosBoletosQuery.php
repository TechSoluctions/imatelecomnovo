<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\LancamentosBoletos as ChildLancamentosBoletos;
use ImaTelecomBundle\Model\LancamentosBoletosQuery as ChildLancamentosBoletosQuery;
use ImaTelecomBundle\Model\Map\LancamentosBoletosTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'lancamentos_boletos' table.
 *
 *
 *
 * @method     ChildLancamentosBoletosQuery orderByIdlancamentosBoletos($order = Criteria::ASC) Order by the idlancamentos_boletos column
 * @method     ChildLancamentosBoletosQuery orderByLancamentoId($order = Criteria::ASC) Order by the lancamento_id column
 * @method     ChildLancamentosBoletosQuery orderByBoletoId($order = Criteria::ASC) Order by the boleto_id column
 * @method     ChildLancamentosBoletosQuery orderByConteudoLinhasLancamentoBoleto($order = Criteria::ASC) Order by the conteudo_linhas_lancamento_boleto column
 * @method     ChildLancamentosBoletosQuery orderByDataCadastrado($order = Criteria::ASC) Order by the data_cadastrado column
 * @method     ChildLancamentosBoletosQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildLancamentosBoletosQuery orderByDataGeracao($order = Criteria::ASC) Order by the data_geracao column
 * @method     ChildLancamentosBoletosQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildLancamentosBoletosQuery orderByConvenioId($order = Criteria::ASC) Order by the convenio_id column
 *
 * @method     ChildLancamentosBoletosQuery groupByIdlancamentosBoletos() Group by the idlancamentos_boletos column
 * @method     ChildLancamentosBoletosQuery groupByLancamentoId() Group by the lancamento_id column
 * @method     ChildLancamentosBoletosQuery groupByBoletoId() Group by the boleto_id column
 * @method     ChildLancamentosBoletosQuery groupByConteudoLinhasLancamentoBoleto() Group by the conteudo_linhas_lancamento_boleto column
 * @method     ChildLancamentosBoletosQuery groupByDataCadastrado() Group by the data_cadastrado column
 * @method     ChildLancamentosBoletosQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildLancamentosBoletosQuery groupByDataGeracao() Group by the data_geracao column
 * @method     ChildLancamentosBoletosQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildLancamentosBoletosQuery groupByConvenioId() Group by the convenio_id column
 *
 * @method     ChildLancamentosBoletosQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLancamentosBoletosQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLancamentosBoletosQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLancamentosBoletosQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLancamentosBoletosQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLancamentosBoletosQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLancamentosBoletosQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildLancamentosBoletosQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildLancamentosBoletosQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildLancamentosBoletosQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildLancamentosBoletosQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildLancamentosBoletosQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildLancamentosBoletosQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildLancamentosBoletosQuery leftJoinCompetencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Competencia relation
 * @method     ChildLancamentosBoletosQuery rightJoinCompetencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Competencia relation
 * @method     ChildLancamentosBoletosQuery innerJoinCompetencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Competencia relation
 *
 * @method     ChildLancamentosBoletosQuery joinWithCompetencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Competencia relation
 *
 * @method     ChildLancamentosBoletosQuery leftJoinWithCompetencia() Adds a LEFT JOIN clause and with to the query using the Competencia relation
 * @method     ChildLancamentosBoletosQuery rightJoinWithCompetencia() Adds a RIGHT JOIN clause and with to the query using the Competencia relation
 * @method     ChildLancamentosBoletosQuery innerJoinWithCompetencia() Adds a INNER JOIN clause and with to the query using the Competencia relation
 *
 * @method     ChildLancamentosBoletosQuery leftJoinConvenio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Convenio relation
 * @method     ChildLancamentosBoletosQuery rightJoinConvenio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Convenio relation
 * @method     ChildLancamentosBoletosQuery innerJoinConvenio($relationAlias = null) Adds a INNER JOIN clause to the query using the Convenio relation
 *
 * @method     ChildLancamentosBoletosQuery joinWithConvenio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Convenio relation
 *
 * @method     ChildLancamentosBoletosQuery leftJoinWithConvenio() Adds a LEFT JOIN clause and with to the query using the Convenio relation
 * @method     ChildLancamentosBoletosQuery rightJoinWithConvenio() Adds a RIGHT JOIN clause and with to the query using the Convenio relation
 * @method     ChildLancamentosBoletosQuery innerJoinWithConvenio() Adds a INNER JOIN clause and with to the query using the Convenio relation
 *
 * @method     ChildLancamentosBoletosQuery leftJoinLancamentos($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lancamentos relation
 * @method     ChildLancamentosBoletosQuery rightJoinLancamentos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lancamentos relation
 * @method     ChildLancamentosBoletosQuery innerJoinLancamentos($relationAlias = null) Adds a INNER JOIN clause to the query using the Lancamentos relation
 *
 * @method     ChildLancamentosBoletosQuery joinWithLancamentos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Lancamentos relation
 *
 * @method     ChildLancamentosBoletosQuery leftJoinWithLancamentos() Adds a LEFT JOIN clause and with to the query using the Lancamentos relation
 * @method     ChildLancamentosBoletosQuery rightJoinWithLancamentos() Adds a RIGHT JOIN clause and with to the query using the Lancamentos relation
 * @method     ChildLancamentosBoletosQuery innerJoinWithLancamentos() Adds a INNER JOIN clause and with to the query using the Lancamentos relation
 *
 * @method     \ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\CompetenciaQuery|\ImaTelecomBundle\Model\ConvenioQuery|\ImaTelecomBundle\Model\LancamentosQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLancamentosBoletos findOne(ConnectionInterface $con = null) Return the first ChildLancamentosBoletos matching the query
 * @method     ChildLancamentosBoletos findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLancamentosBoletos matching the query, or a new ChildLancamentosBoletos object populated from the query conditions when no match is found
 *
 * @method     ChildLancamentosBoletos findOneByIdlancamentosBoletos(int $idlancamentos_boletos) Return the first ChildLancamentosBoletos filtered by the idlancamentos_boletos column
 * @method     ChildLancamentosBoletos findOneByLancamentoId(int $lancamento_id) Return the first ChildLancamentosBoletos filtered by the lancamento_id column
 * @method     ChildLancamentosBoletos findOneByBoletoId(int $boleto_id) Return the first ChildLancamentosBoletos filtered by the boleto_id column
 * @method     ChildLancamentosBoletos findOneByConteudoLinhasLancamentoBoleto(string $conteudo_linhas_lancamento_boleto) Return the first ChildLancamentosBoletos filtered by the conteudo_linhas_lancamento_boleto column
 * @method     ChildLancamentosBoletos findOneByDataCadastrado(string $data_cadastrado) Return the first ChildLancamentosBoletos filtered by the data_cadastrado column
 * @method     ChildLancamentosBoletos findOneByDataAlterado(string $data_alterado) Return the first ChildLancamentosBoletos filtered by the data_alterado column
 * @method     ChildLancamentosBoletos findOneByDataGeracao(string $data_geracao) Return the first ChildLancamentosBoletos filtered by the data_geracao column
 * @method     ChildLancamentosBoletos findOneByCompetenciaId(int $competencia_id) Return the first ChildLancamentosBoletos filtered by the competencia_id column
 * @method     ChildLancamentosBoletos findOneByConvenioId(int $convenio_id) Return the first ChildLancamentosBoletos filtered by the convenio_id column *

 * @method     ChildLancamentosBoletos requirePk($key, ConnectionInterface $con = null) Return the ChildLancamentosBoletos by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentosBoletos requireOne(ConnectionInterface $con = null) Return the first ChildLancamentosBoletos matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLancamentosBoletos requireOneByIdlancamentosBoletos(int $idlancamentos_boletos) Return the first ChildLancamentosBoletos filtered by the idlancamentos_boletos column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentosBoletos requireOneByLancamentoId(int $lancamento_id) Return the first ChildLancamentosBoletos filtered by the lancamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentosBoletos requireOneByBoletoId(int $boleto_id) Return the first ChildLancamentosBoletos filtered by the boleto_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentosBoletos requireOneByConteudoLinhasLancamentoBoleto(string $conteudo_linhas_lancamento_boleto) Return the first ChildLancamentosBoletos filtered by the conteudo_linhas_lancamento_boleto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentosBoletos requireOneByDataCadastrado(string $data_cadastrado) Return the first ChildLancamentosBoletos filtered by the data_cadastrado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentosBoletos requireOneByDataAlterado(string $data_alterado) Return the first ChildLancamentosBoletos filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentosBoletos requireOneByDataGeracao(string $data_geracao) Return the first ChildLancamentosBoletos filtered by the data_geracao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentosBoletos requireOneByCompetenciaId(int $competencia_id) Return the first ChildLancamentosBoletos filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentosBoletos requireOneByConvenioId(int $convenio_id) Return the first ChildLancamentosBoletos filtered by the convenio_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLancamentosBoletos[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLancamentosBoletos objects based on current ModelCriteria
 * @method     ChildLancamentosBoletos[]|ObjectCollection findByIdlancamentosBoletos(int $idlancamentos_boletos) Return ChildLancamentosBoletos objects filtered by the idlancamentos_boletos column
 * @method     ChildLancamentosBoletos[]|ObjectCollection findByLancamentoId(int $lancamento_id) Return ChildLancamentosBoletos objects filtered by the lancamento_id column
 * @method     ChildLancamentosBoletos[]|ObjectCollection findByBoletoId(int $boleto_id) Return ChildLancamentosBoletos objects filtered by the boleto_id column
 * @method     ChildLancamentosBoletos[]|ObjectCollection findByConteudoLinhasLancamentoBoleto(string $conteudo_linhas_lancamento_boleto) Return ChildLancamentosBoletos objects filtered by the conteudo_linhas_lancamento_boleto column
 * @method     ChildLancamentosBoletos[]|ObjectCollection findByDataCadastrado(string $data_cadastrado) Return ChildLancamentosBoletos objects filtered by the data_cadastrado column
 * @method     ChildLancamentosBoletos[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildLancamentosBoletos objects filtered by the data_alterado column
 * @method     ChildLancamentosBoletos[]|ObjectCollection findByDataGeracao(string $data_geracao) Return ChildLancamentosBoletos objects filtered by the data_geracao column
 * @method     ChildLancamentosBoletos[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildLancamentosBoletos objects filtered by the competencia_id column
 * @method     ChildLancamentosBoletos[]|ObjectCollection findByConvenioId(int $convenio_id) Return ChildLancamentosBoletos objects filtered by the convenio_id column
 * @method     ChildLancamentosBoletos[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LancamentosBoletosQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\LancamentosBoletosQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\LancamentosBoletos', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLancamentosBoletosQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLancamentosBoletosQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLancamentosBoletosQuery) {
            return $criteria;
        }
        $query = new ChildLancamentosBoletosQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLancamentosBoletos|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LancamentosBoletosTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LancamentosBoletosTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLancamentosBoletos A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idlancamentos_boletos, lancamento_id, boleto_id, conteudo_linhas_lancamento_boleto, data_cadastrado, data_alterado, data_geracao, competencia_id, convenio_id FROM lancamentos_boletos WHERE idlancamentos_boletos = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLancamentosBoletos $obj */
            $obj = new ChildLancamentosBoletos();
            $obj->hydrate($row);
            LancamentosBoletosTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLancamentosBoletos|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idlancamentos_boletos column
     *
     * Example usage:
     * <code>
     * $query->filterByIdlancamentosBoletos(1234); // WHERE idlancamentos_boletos = 1234
     * $query->filterByIdlancamentosBoletos(array(12, 34)); // WHERE idlancamentos_boletos IN (12, 34)
     * $query->filterByIdlancamentosBoletos(array('min' => 12)); // WHERE idlancamentos_boletos > 12
     * </code>
     *
     * @param     mixed $idlancamentosBoletos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByIdlancamentosBoletos($idlancamentosBoletos = null, $comparison = null)
    {
        if (is_array($idlancamentosBoletos)) {
            $useMinMax = false;
            if (isset($idlancamentosBoletos['min'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, $idlancamentosBoletos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idlancamentosBoletos['max'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, $idlancamentosBoletos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, $idlancamentosBoletos, $comparison);
    }

    /**
     * Filter the query on the lancamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLancamentoId(1234); // WHERE lancamento_id = 1234
     * $query->filterByLancamentoId(array(12, 34)); // WHERE lancamento_id IN (12, 34)
     * $query->filterByLancamentoId(array('min' => 12)); // WHERE lancamento_id > 12
     * </code>
     *
     * @see       filterByLancamentos()
     *
     * @param     mixed $lancamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByLancamentoId($lancamentoId = null, $comparison = null)
    {
        if (is_array($lancamentoId)) {
            $useMinMax = false;
            if (isset($lancamentoId['min'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_LANCAMENTO_ID, $lancamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lancamentoId['max'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_LANCAMENTO_ID, $lancamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_LANCAMENTO_ID, $lancamentoId, $comparison);
    }

    /**
     * Filter the query on the boleto_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBoletoId(1234); // WHERE boleto_id = 1234
     * $query->filterByBoletoId(array(12, 34)); // WHERE boleto_id IN (12, 34)
     * $query->filterByBoletoId(array('min' => 12)); // WHERE boleto_id > 12
     * </code>
     *
     * @see       filterByBoleto()
     *
     * @param     mixed $boletoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByBoletoId($boletoId = null, $comparison = null)
    {
        if (is_array($boletoId)) {
            $useMinMax = false;
            if (isset($boletoId['min'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_BOLETO_ID, $boletoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($boletoId['max'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_BOLETO_ID, $boletoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_BOLETO_ID, $boletoId, $comparison);
    }

    /**
     * Filter the query on the conteudo_linhas_lancamento_boleto column
     *
     * Example usage:
     * <code>
     * $query->filterByConteudoLinhasLancamentoBoleto('fooValue');   // WHERE conteudo_linhas_lancamento_boleto = 'fooValue'
     * $query->filterByConteudoLinhasLancamentoBoleto('%fooValue%', Criteria::LIKE); // WHERE conteudo_linhas_lancamento_boleto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $conteudoLinhasLancamentoBoleto The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByConteudoLinhasLancamentoBoleto($conteudoLinhasLancamentoBoleto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($conteudoLinhasLancamentoBoleto)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_CONTEUDO_LINHAS_LANCAMENTO_BOLETO, $conteudoLinhasLancamentoBoleto, $comparison);
    }

    /**
     * Filter the query on the data_cadastrado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastrado('2011-03-14'); // WHERE data_cadastrado = '2011-03-14'
     * $query->filterByDataCadastrado('now'); // WHERE data_cadastrado = '2011-03-14'
     * $query->filterByDataCadastrado(array('max' => 'yesterday')); // WHERE data_cadastrado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastrado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByDataCadastrado($dataCadastrado = null, $comparison = null)
    {
        if (is_array($dataCadastrado)) {
            $useMinMax = false;
            if (isset($dataCadastrado['min'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_DATA_CADASTRADO, $dataCadastrado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastrado['max'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_DATA_CADASTRADO, $dataCadastrado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_DATA_CADASTRADO, $dataCadastrado, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the data_geracao column
     *
     * Example usage:
     * <code>
     * $query->filterByDataGeracao('fooValue');   // WHERE data_geracao = 'fooValue'
     * $query->filterByDataGeracao('%fooValue%', Criteria::LIKE); // WHERE data_geracao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dataGeracao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByDataGeracao($dataGeracao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dataGeracao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_DATA_GERACAO, $dataGeracao, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @see       filterByCompetencia()
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the convenio_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConvenioId(1234); // WHERE convenio_id = 1234
     * $query->filterByConvenioId(array(12, 34)); // WHERE convenio_id IN (12, 34)
     * $query->filterByConvenioId(array('min' => 12)); // WHERE convenio_id > 12
     * </code>
     *
     * @see       filterByConvenio()
     *
     * @param     mixed $convenioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByConvenioId($convenioId = null, $comparison = null)
    {
        if (is_array($convenioId)) {
            $useMinMax = false;
            if (isset($convenioId['min'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_CONVENIO_ID, $convenioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($convenioId['max'])) {
                $this->addUsingAlias(LancamentosBoletosTableMap::COL_CONVENIO_ID, $convenioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosBoletosTableMap::COL_CONVENIO_ID, $convenioId, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(LancamentosBoletosTableMap::COL_BOLETO_ID, $boleto->getIdboleto(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LancamentosBoletosTableMap::COL_BOLETO_ID, $boleto->toKeyValue('PrimaryKey', 'Idboleto'), $comparison);
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Competencia object
     *
     * @param \ImaTelecomBundle\Model\Competencia|ObjectCollection $competencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByCompetencia($competencia, $comparison = null)
    {
        if ($competencia instanceof \ImaTelecomBundle\Model\Competencia) {
            return $this
                ->addUsingAlias(LancamentosBoletosTableMap::COL_COMPETENCIA_ID, $competencia->getId(), $comparison);
        } elseif ($competencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LancamentosBoletosTableMap::COL_COMPETENCIA_ID, $competencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompetencia() only accepts arguments of type \ImaTelecomBundle\Model\Competencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Competencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function joinCompetencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Competencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Competencia');
        }

        return $this;
    }

    /**
     * Use the Competencia relation Competencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CompetenciaQuery A secondary query class using the current class as primary query
     */
    public function useCompetenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompetencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Competencia', '\ImaTelecomBundle\Model\CompetenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Convenio object
     *
     * @param \ImaTelecomBundle\Model\Convenio|ObjectCollection $convenio The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByConvenio($convenio, $comparison = null)
    {
        if ($convenio instanceof \ImaTelecomBundle\Model\Convenio) {
            return $this
                ->addUsingAlias(LancamentosBoletosTableMap::COL_CONVENIO_ID, $convenio->getIdconvenio(), $comparison);
        } elseif ($convenio instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LancamentosBoletosTableMap::COL_CONVENIO_ID, $convenio->toKeyValue('PrimaryKey', 'Idconvenio'), $comparison);
        } else {
            throw new PropelException('filterByConvenio() only accepts arguments of type \ImaTelecomBundle\Model\Convenio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Convenio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function joinConvenio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Convenio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Convenio');
        }

        return $this;
    }

    /**
     * Use the Convenio relation Convenio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ConvenioQuery A secondary query class using the current class as primary query
     */
    public function useConvenioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinConvenio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Convenio', '\ImaTelecomBundle\Model\ConvenioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Lancamentos object
     *
     * @param \ImaTelecomBundle\Model\Lancamentos|ObjectCollection $lancamentos The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function filterByLancamentos($lancamentos, $comparison = null)
    {
        if ($lancamentos instanceof \ImaTelecomBundle\Model\Lancamentos) {
            return $this
                ->addUsingAlias(LancamentosBoletosTableMap::COL_LANCAMENTO_ID, $lancamentos->getIdlancamento(), $comparison);
        } elseif ($lancamentos instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LancamentosBoletosTableMap::COL_LANCAMENTO_ID, $lancamentos->toKeyValue('PrimaryKey', 'Idlancamento'), $comparison);
        } else {
            throw new PropelException('filterByLancamentos() only accepts arguments of type \ImaTelecomBundle\Model\Lancamentos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lancamentos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function joinLancamentos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lancamentos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lancamentos');
        }

        return $this;
    }

    /**
     * Use the Lancamentos relation Lancamentos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\LancamentosQuery A secondary query class using the current class as primary query
     */
    public function useLancamentosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLancamentos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lancamentos', '\ImaTelecomBundle\Model\LancamentosQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLancamentosBoletos $lancamentosBoletos Object to remove from the list of results
     *
     * @return $this|ChildLancamentosBoletosQuery The current query, for fluid interface
     */
    public function prune($lancamentosBoletos = null)
    {
        if ($lancamentosBoletos) {
            $this->addUsingAlias(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, $lancamentosBoletos->getIdlancamentosBoletos(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the lancamentos_boletos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosBoletosTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LancamentosBoletosTableMap::clearInstancePool();
            LancamentosBoletosTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosBoletosTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LancamentosBoletosTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LancamentosBoletosTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LancamentosBoletosTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LancamentosBoletosQuery
