<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoItem as ChildBoletoItem;
use ImaTelecomBundle\Model\BoletoItemQuery as ChildBoletoItemQuery;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\DadosFiscaisGeracao as ChildDadosFiscaisGeracao;
use ImaTelecomBundle\Model\DadosFiscaisGeracaoQuery as ChildDadosFiscaisGeracaoQuery;
use ImaTelecomBundle\Model\DadosFiscalItem as ChildDadosFiscalItem;
use ImaTelecomBundle\Model\DadosFiscalItemQuery as ChildDadosFiscalItemQuery;
use ImaTelecomBundle\Model\ServicoCliente as ChildServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery as ChildServicoClienteQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BoletoItemTableMap;
use ImaTelecomBundle\Model\Map\DadosFiscaisGeracaoTableMap;
use ImaTelecomBundle\Model\Map\DadosFiscalItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'boleto_item' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class BoletoItem implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\BoletoItemTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idboleto_item field.
     *
     * @var        int
     */
    protected $idboleto_item;

    /**
     * The value for the valor field.
     *
     * @var        string
     */
    protected $valor;

    /**
     * The value for the boleto_id field.
     *
     * @var        int
     */
    protected $boleto_id;

    /**
     * The value for the servico_cliente_id field.
     *
     * @var        int
     */
    protected $servico_cliente_id;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the import_id field.
     *
     * @var        int
     */
    protected $import_id;

    /**
     * The value for the base_calculo_scm field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $base_calculo_scm;

    /**
     * The value for the base_calculo_sva field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $base_calculo_sva;

    /**
     * @var        ChildBoleto
     */
    protected $aBoleto;

    /**
     * @var        ChildServicoCliente
     */
    protected $aServicoCliente;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildDadosFiscaisGeracao[] Collection to store aggregation of ChildDadosFiscaisGeracao objects.
     */
    protected $collDadosFiscaisGeracaos;
    protected $collDadosFiscaisGeracaosPartial;

    /**
     * @var        ObjectCollection|ChildDadosFiscalItem[] Collection to store aggregation of ChildDadosFiscalItem objects.
     */
    protected $collDadosFiscalItems;
    protected $collDadosFiscalItemsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDadosFiscaisGeracao[]
     */
    protected $dadosFiscaisGeracaosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDadosFiscalItem[]
     */
    protected $dadosFiscalItemsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->base_calculo_scm = '0.00';
        $this->base_calculo_sva = '0.00';
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\BoletoItem object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>BoletoItem</code> instance.  If
     * <code>obj</code> is an instance of <code>BoletoItem</code>, delegates to
     * <code>equals(BoletoItem)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|BoletoItem The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idboleto_item] column value.
     *
     * @return int
     */
    public function getIdboletoItem()
    {
        return $this->idboleto_item;
    }

    /**
     * Get the [valor] column value.
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Get the [boleto_id] column value.
     *
     * @return int
     */
    public function getBoletoId()
    {
        return $this->boleto_id;
    }

    /**
     * Get the [servico_cliente_id] column value.
     *
     * @return int
     */
    public function getServicoClienteId()
    {
        return $this->servico_cliente_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [import_id] column value.
     *
     * @return int
     */
    public function getImportId()
    {
        return $this->import_id;
    }

    /**
     * Get the [base_calculo_scm] column value.
     *
     * @return string
     */
    public function getBaseCalculoScm()
    {
        return $this->base_calculo_scm;
    }

    /**
     * Get the [base_calculo_sva] column value.
     *
     * @return string
     */
    public function getBaseCalculoSva()
    {
        return $this->base_calculo_sva;
    }

    /**
     * Set the value of [idboleto_item] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setIdboletoItem($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idboleto_item !== $v) {
            $this->idboleto_item = $v;
            $this->modifiedColumns[BoletoItemTableMap::COL_IDBOLETO_ITEM] = true;
        }

        return $this;
    } // setIdboletoItem()

    /**
     * Set the value of [valor] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setValor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor !== $v) {
            $this->valor = $v;
            $this->modifiedColumns[BoletoItemTableMap::COL_VALOR] = true;
        }

        return $this;
    } // setValor()

    /**
     * Set the value of [boleto_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setBoletoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->boleto_id !== $v) {
            $this->boleto_id = $v;
            $this->modifiedColumns[BoletoItemTableMap::COL_BOLETO_ID] = true;
        }

        if ($this->aBoleto !== null && $this->aBoleto->getIdboleto() !== $v) {
            $this->aBoleto = null;
        }

        return $this;
    } // setBoletoId()

    /**
     * Set the value of [servico_cliente_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setServicoClienteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->servico_cliente_id !== $v) {
            $this->servico_cliente_id = $v;
            $this->modifiedColumns[BoletoItemTableMap::COL_SERVICO_CLIENTE_ID] = true;
        }

        if ($this->aServicoCliente !== null && $this->aServicoCliente->getIdservicoCliente() !== $v) {
            $this->aServicoCliente = null;
        }

        return $this;
    } // setServicoClienteId()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BoletoItemTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BoletoItemTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[BoletoItemTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Set the value of [import_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setImportId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->import_id !== $v) {
            $this->import_id = $v;
            $this->modifiedColumns[BoletoItemTableMap::COL_IMPORT_ID] = true;
        }

        return $this;
    } // setImportId()

    /**
     * Set the value of [base_calculo_scm] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setBaseCalculoScm($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->base_calculo_scm !== $v) {
            $this->base_calculo_scm = $v;
            $this->modifiedColumns[BoletoItemTableMap::COL_BASE_CALCULO_SCM] = true;
        }

        return $this;
    } // setBaseCalculoScm()

    /**
     * Set the value of [base_calculo_sva] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function setBaseCalculoSva($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->base_calculo_sva !== $v) {
            $this->base_calculo_sva = $v;
            $this->modifiedColumns[BoletoItemTableMap::COL_BASE_CALCULO_SVA] = true;
        }

        return $this;
    } // setBaseCalculoSva()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->base_calculo_scm !== '0.00') {
                return false;
            }

            if ($this->base_calculo_sva !== '0.00') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : BoletoItemTableMap::translateFieldName('IdboletoItem', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idboleto_item = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : BoletoItemTableMap::translateFieldName('Valor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : BoletoItemTableMap::translateFieldName('BoletoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->boleto_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : BoletoItemTableMap::translateFieldName('ServicoClienteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->servico_cliente_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : BoletoItemTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : BoletoItemTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : BoletoItemTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : BoletoItemTableMap::translateFieldName('ImportId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->import_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : BoletoItemTableMap::translateFieldName('BaseCalculoScm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->base_calculo_scm = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : BoletoItemTableMap::translateFieldName('BaseCalculoSva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->base_calculo_sva = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = BoletoItemTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\BoletoItem'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aBoleto !== null && $this->boleto_id !== $this->aBoleto->getIdboleto()) {
            $this->aBoleto = null;
        }
        if ($this->aServicoCliente !== null && $this->servico_cliente_id !== $this->aServicoCliente->getIdservicoCliente()) {
            $this->aServicoCliente = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BoletoItemTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildBoletoItemQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBoleto = null;
            $this->aServicoCliente = null;
            $this->aUsuario = null;
            $this->collDadosFiscaisGeracaos = null;

            $this->collDadosFiscalItems = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see BoletoItem::setDeleted()
     * @see BoletoItem::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoItemTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildBoletoItemQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoItemTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BoletoItemTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBoleto !== null) {
                if ($this->aBoleto->isModified() || $this->aBoleto->isNew()) {
                    $affectedRows += $this->aBoleto->save($con);
                }
                $this->setBoleto($this->aBoleto);
            }

            if ($this->aServicoCliente !== null) {
                if ($this->aServicoCliente->isModified() || $this->aServicoCliente->isNew()) {
                    $affectedRows += $this->aServicoCliente->save($con);
                }
                $this->setServicoCliente($this->aServicoCliente);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->dadosFiscaisGeracaosScheduledForDeletion !== null) {
                if (!$this->dadosFiscaisGeracaosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\DadosFiscaisGeracaoQuery::create()
                        ->filterByPrimaryKeys($this->dadosFiscaisGeracaosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dadosFiscaisGeracaosScheduledForDeletion = null;
                }
            }

            if ($this->collDadosFiscaisGeracaos !== null) {
                foreach ($this->collDadosFiscaisGeracaos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dadosFiscalItemsScheduledForDeletion !== null) {
                if (!$this->dadosFiscalItemsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\DadosFiscalItemQuery::create()
                        ->filterByPrimaryKeys($this->dadosFiscalItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dadosFiscalItemsScheduledForDeletion = null;
                }
            }

            if ($this->collDadosFiscalItems !== null) {
                foreach ($this->collDadosFiscalItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[BoletoItemTableMap::COL_IDBOLETO_ITEM] = true;
        if (null !== $this->idboleto_item) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BoletoItemTableMap::COL_IDBOLETO_ITEM . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BoletoItemTableMap::COL_IDBOLETO_ITEM)) {
            $modifiedColumns[':p' . $index++]  = 'idboleto_item';
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_VALOR)) {
            $modifiedColumns[':p' . $index++]  = 'valor';
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_BOLETO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'boleto_id';
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_SERVICO_CLIENTE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'servico_cliente_id';
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_IMPORT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'import_id';
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_BASE_CALCULO_SCM)) {
            $modifiedColumns[':p' . $index++]  = 'base_calculo_scm';
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_BASE_CALCULO_SVA)) {
            $modifiedColumns[':p' . $index++]  = 'base_calculo_sva';
        }

        $sql = sprintf(
            'INSERT INTO boleto_item (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idboleto_item':
                        $stmt->bindValue($identifier, $this->idboleto_item, PDO::PARAM_INT);
                        break;
                    case 'valor':
                        $stmt->bindValue($identifier, $this->valor, PDO::PARAM_STR);
                        break;
                    case 'boleto_id':
                        $stmt->bindValue($identifier, $this->boleto_id, PDO::PARAM_INT);
                        break;
                    case 'servico_cliente_id':
                        $stmt->bindValue($identifier, $this->servico_cliente_id, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'import_id':
                        $stmt->bindValue($identifier, $this->import_id, PDO::PARAM_INT);
                        break;
                    case 'base_calculo_scm':
                        $stmt->bindValue($identifier, $this->base_calculo_scm, PDO::PARAM_STR);
                        break;
                    case 'base_calculo_sva':
                        $stmt->bindValue($identifier, $this->base_calculo_sva, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdboletoItem($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BoletoItemTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdboletoItem();
                break;
            case 1:
                return $this->getValor();
                break;
            case 2:
                return $this->getBoletoId();
                break;
            case 3:
                return $this->getServicoClienteId();
                break;
            case 4:
                return $this->getDataCadastro();
                break;
            case 5:
                return $this->getDataAlterado();
                break;
            case 6:
                return $this->getUsuarioAlterado();
                break;
            case 7:
                return $this->getImportId();
                break;
            case 8:
                return $this->getBaseCalculoScm();
                break;
            case 9:
                return $this->getBaseCalculoSva();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['BoletoItem'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['BoletoItem'][$this->hashCode()] = true;
        $keys = BoletoItemTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdboletoItem(),
            $keys[1] => $this->getValor(),
            $keys[2] => $this->getBoletoId(),
            $keys[3] => $this->getServicoClienteId(),
            $keys[4] => $this->getDataCadastro(),
            $keys[5] => $this->getDataAlterado(),
            $keys[6] => $this->getUsuarioAlterado(),
            $keys[7] => $this->getImportId(),
            $keys[8] => $this->getBaseCalculoScm(),
            $keys[9] => $this->getBaseCalculoSva(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBoleto) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boleto';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto';
                        break;
                    default:
                        $key = 'Boleto';
                }

                $result[$key] = $this->aBoleto->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aServicoCliente) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicoCliente';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'servico_cliente';
                        break;
                    default:
                        $key = 'ServicoCliente';
                }

                $result[$key] = $this->aServicoCliente->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collDadosFiscaisGeracaos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dadosFiscaisGeracaos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'dados_fiscais_geracaos';
                        break;
                    default:
                        $key = 'DadosFiscaisGeracaos';
                }

                $result[$key] = $this->collDadosFiscaisGeracaos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDadosFiscalItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dadosFiscalItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'dados_fiscal_items';
                        break;
                    default:
                        $key = 'DadosFiscalItems';
                }

                $result[$key] = $this->collDadosFiscalItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\BoletoItem
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BoletoItemTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\BoletoItem
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdboletoItem($value);
                break;
            case 1:
                $this->setValor($value);
                break;
            case 2:
                $this->setBoletoId($value);
                break;
            case 3:
                $this->setServicoClienteId($value);
                break;
            case 4:
                $this->setDataCadastro($value);
                break;
            case 5:
                $this->setDataAlterado($value);
                break;
            case 6:
                $this->setUsuarioAlterado($value);
                break;
            case 7:
                $this->setImportId($value);
                break;
            case 8:
                $this->setBaseCalculoScm($value);
                break;
            case 9:
                $this->setBaseCalculoSva($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = BoletoItemTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdboletoItem($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setValor($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setBoletoId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setServicoClienteId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDataCadastro($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDataAlterado($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setImportId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setBaseCalculoScm($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setBaseCalculoSva($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BoletoItemTableMap::DATABASE_NAME);

        if ($this->isColumnModified(BoletoItemTableMap::COL_IDBOLETO_ITEM)) {
            $criteria->add(BoletoItemTableMap::COL_IDBOLETO_ITEM, $this->idboleto_item);
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_VALOR)) {
            $criteria->add(BoletoItemTableMap::COL_VALOR, $this->valor);
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_BOLETO_ID)) {
            $criteria->add(BoletoItemTableMap::COL_BOLETO_ID, $this->boleto_id);
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_SERVICO_CLIENTE_ID)) {
            $criteria->add(BoletoItemTableMap::COL_SERVICO_CLIENTE_ID, $this->servico_cliente_id);
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(BoletoItemTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(BoletoItemTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(BoletoItemTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_IMPORT_ID)) {
            $criteria->add(BoletoItemTableMap::COL_IMPORT_ID, $this->import_id);
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_BASE_CALCULO_SCM)) {
            $criteria->add(BoletoItemTableMap::COL_BASE_CALCULO_SCM, $this->base_calculo_scm);
        }
        if ($this->isColumnModified(BoletoItemTableMap::COL_BASE_CALCULO_SVA)) {
            $criteria->add(BoletoItemTableMap::COL_BASE_CALCULO_SVA, $this->base_calculo_sva);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildBoletoItemQuery::create();
        $criteria->add(BoletoItemTableMap::COL_IDBOLETO_ITEM, $this->idboleto_item);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdboletoItem();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdboletoItem();
    }

    /**
     * Generic method to set the primary key (idboleto_item column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdboletoItem($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdboletoItem();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\BoletoItem (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setValor($this->getValor());
        $copyObj->setBoletoId($this->getBoletoId());
        $copyObj->setServicoClienteId($this->getServicoClienteId());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setImportId($this->getImportId());
        $copyObj->setBaseCalculoScm($this->getBaseCalculoScm());
        $copyObj->setBaseCalculoSva($this->getBaseCalculoSva());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getDadosFiscaisGeracaos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDadosFiscaisGeracao($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDadosFiscalItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDadosFiscalItem($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdboletoItem(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\BoletoItem Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBoleto object.
     *
     * @param  ChildBoleto $v
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBoleto(ChildBoleto $v = null)
    {
        if ($v === null) {
            $this->setBoletoId(NULL);
        } else {
            $this->setBoletoId($v->getIdboleto());
        }

        $this->aBoleto = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBoleto object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoItem($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBoleto object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBoleto The associated ChildBoleto object.
     * @throws PropelException
     */
    public function getBoleto(ConnectionInterface $con = null)
    {
        if ($this->aBoleto === null && ($this->boleto_id !== null)) {
            $this->aBoleto = ChildBoletoQuery::create()->findPk($this->boleto_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBoleto->addBoletoItems($this);
             */
        }

        return $this->aBoleto;
    }

    /**
     * Declares an association between this object and a ChildServicoCliente object.
     *
     * @param  ChildServicoCliente $v
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setServicoCliente(ChildServicoCliente $v = null)
    {
        if ($v === null) {
            $this->setServicoClienteId(NULL);
        } else {
            $this->setServicoClienteId($v->getIdservicoCliente());
        }

        $this->aServicoCliente = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildServicoCliente object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoItem($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildServicoCliente object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildServicoCliente The associated ChildServicoCliente object.
     * @throws PropelException
     */
    public function getServicoCliente(ConnectionInterface $con = null)
    {
        if ($this->aServicoCliente === null && ($this->servico_cliente_id !== null)) {
            $this->aServicoCliente = ChildServicoClienteQuery::create()->findPk($this->servico_cliente_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aServicoCliente->addBoletoItems($this);
             */
        }

        return $this->aServicoCliente;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoItem($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addBoletoItems($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('DadosFiscaisGeracao' == $relationName) {
            return $this->initDadosFiscaisGeracaos();
        }
        if ('DadosFiscalItem' == $relationName) {
            return $this->initDadosFiscalItems();
        }
    }

    /**
     * Clears out the collDadosFiscaisGeracaos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDadosFiscaisGeracaos()
     */
    public function clearDadosFiscaisGeracaos()
    {
        $this->collDadosFiscaisGeracaos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDadosFiscaisGeracaos collection loaded partially.
     */
    public function resetPartialDadosFiscaisGeracaos($v = true)
    {
        $this->collDadosFiscaisGeracaosPartial = $v;
    }

    /**
     * Initializes the collDadosFiscaisGeracaos collection.
     *
     * By default this just sets the collDadosFiscaisGeracaos collection to an empty array (like clearcollDadosFiscaisGeracaos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDadosFiscaisGeracaos($overrideExisting = true)
    {
        if (null !== $this->collDadosFiscaisGeracaos && !$overrideExisting) {
            return;
        }

        $collectionClassName = DadosFiscaisGeracaoTableMap::getTableMap()->getCollectionClassName();

        $this->collDadosFiscaisGeracaos = new $collectionClassName;
        $this->collDadosFiscaisGeracaos->setModel('\ImaTelecomBundle\Model\DadosFiscaisGeracao');
    }

    /**
     * Gets an array of ChildDadosFiscaisGeracao objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBoletoItem is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDadosFiscaisGeracao[] List of ChildDadosFiscaisGeracao objects
     * @throws PropelException
     */
    public function getDadosFiscaisGeracaos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscaisGeracaosPartial && !$this->isNew();
        if (null === $this->collDadosFiscaisGeracaos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscaisGeracaos) {
                // return empty collection
                $this->initDadosFiscaisGeracaos();
            } else {
                $collDadosFiscaisGeracaos = ChildDadosFiscaisGeracaoQuery::create(null, $criteria)
                    ->filterByBoletoItem($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDadosFiscaisGeracaosPartial && count($collDadosFiscaisGeracaos)) {
                        $this->initDadosFiscaisGeracaos(false);

                        foreach ($collDadosFiscaisGeracaos as $obj) {
                            if (false == $this->collDadosFiscaisGeracaos->contains($obj)) {
                                $this->collDadosFiscaisGeracaos->append($obj);
                            }
                        }

                        $this->collDadosFiscaisGeracaosPartial = true;
                    }

                    return $collDadosFiscaisGeracaos;
                }

                if ($partial && $this->collDadosFiscaisGeracaos) {
                    foreach ($this->collDadosFiscaisGeracaos as $obj) {
                        if ($obj->isNew()) {
                            $collDadosFiscaisGeracaos[] = $obj;
                        }
                    }
                }

                $this->collDadosFiscaisGeracaos = $collDadosFiscaisGeracaos;
                $this->collDadosFiscaisGeracaosPartial = false;
            }
        }

        return $this->collDadosFiscaisGeracaos;
    }

    /**
     * Sets a collection of ChildDadosFiscaisGeracao objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $dadosFiscaisGeracaos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBoletoItem The current object (for fluent API support)
     */
    public function setDadosFiscaisGeracaos(Collection $dadosFiscaisGeracaos, ConnectionInterface $con = null)
    {
        /** @var ChildDadosFiscaisGeracao[] $dadosFiscaisGeracaosToDelete */
        $dadosFiscaisGeracaosToDelete = $this->getDadosFiscaisGeracaos(new Criteria(), $con)->diff($dadosFiscaisGeracaos);


        $this->dadosFiscaisGeracaosScheduledForDeletion = $dadosFiscaisGeracaosToDelete;

        foreach ($dadosFiscaisGeracaosToDelete as $dadosFiscaisGeracaoRemoved) {
            $dadosFiscaisGeracaoRemoved->setBoletoItem(null);
        }

        $this->collDadosFiscaisGeracaos = null;
        foreach ($dadosFiscaisGeracaos as $dadosFiscaisGeracao) {
            $this->addDadosFiscaisGeracao($dadosFiscaisGeracao);
        }

        $this->collDadosFiscaisGeracaos = $dadosFiscaisGeracaos;
        $this->collDadosFiscaisGeracaosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DadosFiscaisGeracao objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related DadosFiscaisGeracao objects.
     * @throws PropelException
     */
    public function countDadosFiscaisGeracaos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscaisGeracaosPartial && !$this->isNew();
        if (null === $this->collDadosFiscaisGeracaos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscaisGeracaos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDadosFiscaisGeracaos());
            }

            $query = ChildDadosFiscaisGeracaoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBoletoItem($this)
                ->count($con);
        }

        return count($this->collDadosFiscaisGeracaos);
    }

    /**
     * Method called to associate a ChildDadosFiscaisGeracao object to this object
     * through the ChildDadosFiscaisGeracao foreign key attribute.
     *
     * @param  ChildDadosFiscaisGeracao $l ChildDadosFiscaisGeracao
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function addDadosFiscaisGeracao(ChildDadosFiscaisGeracao $l)
    {
        if ($this->collDadosFiscaisGeracaos === null) {
            $this->initDadosFiscaisGeracaos();
            $this->collDadosFiscaisGeracaosPartial = true;
        }

        if (!$this->collDadosFiscaisGeracaos->contains($l)) {
            $this->doAddDadosFiscaisGeracao($l);

            if ($this->dadosFiscaisGeracaosScheduledForDeletion and $this->dadosFiscaisGeracaosScheduledForDeletion->contains($l)) {
                $this->dadosFiscaisGeracaosScheduledForDeletion->remove($this->dadosFiscaisGeracaosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDadosFiscaisGeracao $dadosFiscaisGeracao The ChildDadosFiscaisGeracao object to add.
     */
    protected function doAddDadosFiscaisGeracao(ChildDadosFiscaisGeracao $dadosFiscaisGeracao)
    {
        $this->collDadosFiscaisGeracaos[]= $dadosFiscaisGeracao;
        $dadosFiscaisGeracao->setBoletoItem($this);
    }

    /**
     * @param  ChildDadosFiscaisGeracao $dadosFiscaisGeracao The ChildDadosFiscaisGeracao object to remove.
     * @return $this|ChildBoletoItem The current object (for fluent API support)
     */
    public function removeDadosFiscaisGeracao(ChildDadosFiscaisGeracao $dadosFiscaisGeracao)
    {
        if ($this->getDadosFiscaisGeracaos()->contains($dadosFiscaisGeracao)) {
            $pos = $this->collDadosFiscaisGeracaos->search($dadosFiscaisGeracao);
            $this->collDadosFiscaisGeracaos->remove($pos);
            if (null === $this->dadosFiscaisGeracaosScheduledForDeletion) {
                $this->dadosFiscaisGeracaosScheduledForDeletion = clone $this->collDadosFiscaisGeracaos;
                $this->dadosFiscaisGeracaosScheduledForDeletion->clear();
            }
            $this->dadosFiscaisGeracaosScheduledForDeletion[]= clone $dadosFiscaisGeracao;
            $dadosFiscaisGeracao->setBoletoItem(null);
        }

        return $this;
    }

    /**
     * Clears out the collDadosFiscalItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDadosFiscalItems()
     */
    public function clearDadosFiscalItems()
    {
        $this->collDadosFiscalItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDadosFiscalItems collection loaded partially.
     */
    public function resetPartialDadosFiscalItems($v = true)
    {
        $this->collDadosFiscalItemsPartial = $v;
    }

    /**
     * Initializes the collDadosFiscalItems collection.
     *
     * By default this just sets the collDadosFiscalItems collection to an empty array (like clearcollDadosFiscalItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDadosFiscalItems($overrideExisting = true)
    {
        if (null !== $this->collDadosFiscalItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = DadosFiscalItemTableMap::getTableMap()->getCollectionClassName();

        $this->collDadosFiscalItems = new $collectionClassName;
        $this->collDadosFiscalItems->setModel('\ImaTelecomBundle\Model\DadosFiscalItem');
    }

    /**
     * Gets an array of ChildDadosFiscalItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBoletoItem is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDadosFiscalItem[] List of ChildDadosFiscalItem objects
     * @throws PropelException
     */
    public function getDadosFiscalItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalItemsPartial && !$this->isNew();
        if (null === $this->collDadosFiscalItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscalItems) {
                // return empty collection
                $this->initDadosFiscalItems();
            } else {
                $collDadosFiscalItems = ChildDadosFiscalItemQuery::create(null, $criteria)
                    ->filterByBoletoItem($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDadosFiscalItemsPartial && count($collDadosFiscalItems)) {
                        $this->initDadosFiscalItems(false);

                        foreach ($collDadosFiscalItems as $obj) {
                            if (false == $this->collDadosFiscalItems->contains($obj)) {
                                $this->collDadosFiscalItems->append($obj);
                            }
                        }

                        $this->collDadosFiscalItemsPartial = true;
                    }

                    return $collDadosFiscalItems;
                }

                if ($partial && $this->collDadosFiscalItems) {
                    foreach ($this->collDadosFiscalItems as $obj) {
                        if ($obj->isNew()) {
                            $collDadosFiscalItems[] = $obj;
                        }
                    }
                }

                $this->collDadosFiscalItems = $collDadosFiscalItems;
                $this->collDadosFiscalItemsPartial = false;
            }
        }

        return $this->collDadosFiscalItems;
    }

    /**
     * Sets a collection of ChildDadosFiscalItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $dadosFiscalItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBoletoItem The current object (for fluent API support)
     */
    public function setDadosFiscalItems(Collection $dadosFiscalItems, ConnectionInterface $con = null)
    {
        /** @var ChildDadosFiscalItem[] $dadosFiscalItemsToDelete */
        $dadosFiscalItemsToDelete = $this->getDadosFiscalItems(new Criteria(), $con)->diff($dadosFiscalItems);


        $this->dadosFiscalItemsScheduledForDeletion = $dadosFiscalItemsToDelete;

        foreach ($dadosFiscalItemsToDelete as $dadosFiscalItemRemoved) {
            $dadosFiscalItemRemoved->setBoletoItem(null);
        }

        $this->collDadosFiscalItems = null;
        foreach ($dadosFiscalItems as $dadosFiscalItem) {
            $this->addDadosFiscalItem($dadosFiscalItem);
        }

        $this->collDadosFiscalItems = $dadosFiscalItems;
        $this->collDadosFiscalItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DadosFiscalItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related DadosFiscalItem objects.
     * @throws PropelException
     */
    public function countDadosFiscalItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalItemsPartial && !$this->isNew();
        if (null === $this->collDadosFiscalItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscalItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDadosFiscalItems());
            }

            $query = ChildDadosFiscalItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBoletoItem($this)
                ->count($con);
        }

        return count($this->collDadosFiscalItems);
    }

    /**
     * Method called to associate a ChildDadosFiscalItem object to this object
     * through the ChildDadosFiscalItem foreign key attribute.
     *
     * @param  ChildDadosFiscalItem $l ChildDadosFiscalItem
     * @return $this|\ImaTelecomBundle\Model\BoletoItem The current object (for fluent API support)
     */
    public function addDadosFiscalItem(ChildDadosFiscalItem $l)
    {
        if ($this->collDadosFiscalItems === null) {
            $this->initDadosFiscalItems();
            $this->collDadosFiscalItemsPartial = true;
        }

        if (!$this->collDadosFiscalItems->contains($l)) {
            $this->doAddDadosFiscalItem($l);

            if ($this->dadosFiscalItemsScheduledForDeletion and $this->dadosFiscalItemsScheduledForDeletion->contains($l)) {
                $this->dadosFiscalItemsScheduledForDeletion->remove($this->dadosFiscalItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDadosFiscalItem $dadosFiscalItem The ChildDadosFiscalItem object to add.
     */
    protected function doAddDadosFiscalItem(ChildDadosFiscalItem $dadosFiscalItem)
    {
        $this->collDadosFiscalItems[]= $dadosFiscalItem;
        $dadosFiscalItem->setBoletoItem($this);
    }

    /**
     * @param  ChildDadosFiscalItem $dadosFiscalItem The ChildDadosFiscalItem object to remove.
     * @return $this|ChildBoletoItem The current object (for fluent API support)
     */
    public function removeDadosFiscalItem(ChildDadosFiscalItem $dadosFiscalItem)
    {
        if ($this->getDadosFiscalItems()->contains($dadosFiscalItem)) {
            $pos = $this->collDadosFiscalItems->search($dadosFiscalItem);
            $this->collDadosFiscalItems->remove($pos);
            if (null === $this->dadosFiscalItemsScheduledForDeletion) {
                $this->dadosFiscalItemsScheduledForDeletion = clone $this->collDadosFiscalItems;
                $this->dadosFiscalItemsScheduledForDeletion->clear();
            }
            $this->dadosFiscalItemsScheduledForDeletion[]= clone $dadosFiscalItem;
            $dadosFiscalItem->setBoletoItem(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BoletoItem is new, it will return
     * an empty collection; or if this BoletoItem has previously
     * been saved, it will retrieve related DadosFiscalItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BoletoItem.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscalItem[] List of ChildDadosFiscalItem objects
     */
    public function getDadosFiscalItemsJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalItemQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getDadosFiscalItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BoletoItem is new, it will return
     * an empty collection; or if this BoletoItem has previously
     * been saved, it will retrieve related DadosFiscalItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BoletoItem.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscalItem[] List of ChildDadosFiscalItem objects
     */
    public function getDadosFiscalItemsJoinDadosFiscal(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalItemQuery::create(null, $criteria);
        $query->joinWith('DadosFiscal', $joinBehavior);

        return $this->getDadosFiscalItems($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBoleto) {
            $this->aBoleto->removeBoletoItem($this);
        }
        if (null !== $this->aServicoCliente) {
            $this->aServicoCliente->removeBoletoItem($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeBoletoItem($this);
        }
        $this->idboleto_item = null;
        $this->valor = null;
        $this->boleto_id = null;
        $this->servico_cliente_id = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->import_id = null;
        $this->base_calculo_scm = null;
        $this->base_calculo_sva = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collDadosFiscaisGeracaos) {
                foreach ($this->collDadosFiscaisGeracaos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDadosFiscalItems) {
                foreach ($this->collDadosFiscalItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collDadosFiscaisGeracaos = null;
        $this->collDadosFiscalItems = null;
        $this->aBoleto = null;
        $this->aServicoCliente = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BoletoItemTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
