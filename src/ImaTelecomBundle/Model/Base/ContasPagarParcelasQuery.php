<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ContasPagarParcelas as ChildContasPagarParcelas;
use ImaTelecomBundle\Model\ContasPagarParcelasQuery as ChildContasPagarParcelasQuery;
use ImaTelecomBundle\Model\Map\ContasPagarParcelasTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'contas_pagar_parcelas' table.
 *
 *
 *
 * @method     ChildContasPagarParcelasQuery orderByIdcontasPagarParcelas($order = Criteria::ASC) Order by the idcontas_pagar_parcelas column
 * @method     ChildContasPagarParcelasQuery orderByValor($order = Criteria::ASC) Order by the valor column
 * @method     ChildContasPagarParcelasQuery orderByVencimento($order = Criteria::ASC) Order by the vencimento column
 * @method     ChildContasPagarParcelasQuery orderByContaPagarId($order = Criteria::ASC) Order by the conta_pagar_id column
 * @method     ChildContasPagarParcelasQuery orderByNumParcela($order = Criteria::ASC) Order by the num_parcela column
 * @method     ChildContasPagarParcelasQuery orderByCaixaId($order = Criteria::ASC) Order by the caixa_id column
 * @method     ChildContasPagarParcelasQuery orderByEstaPago($order = Criteria::ASC) Order by the esta_pago column
 *
 * @method     ChildContasPagarParcelasQuery groupByIdcontasPagarParcelas() Group by the idcontas_pagar_parcelas column
 * @method     ChildContasPagarParcelasQuery groupByValor() Group by the valor column
 * @method     ChildContasPagarParcelasQuery groupByVencimento() Group by the vencimento column
 * @method     ChildContasPagarParcelasQuery groupByContaPagarId() Group by the conta_pagar_id column
 * @method     ChildContasPagarParcelasQuery groupByNumParcela() Group by the num_parcela column
 * @method     ChildContasPagarParcelasQuery groupByCaixaId() Group by the caixa_id column
 * @method     ChildContasPagarParcelasQuery groupByEstaPago() Group by the esta_pago column
 *
 * @method     ChildContasPagarParcelasQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildContasPagarParcelasQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildContasPagarParcelasQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildContasPagarParcelasQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildContasPagarParcelasQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildContasPagarParcelasQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildContasPagarParcelasQuery leftJoinCaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Caixa relation
 * @method     ChildContasPagarParcelasQuery rightJoinCaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Caixa relation
 * @method     ChildContasPagarParcelasQuery innerJoinCaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Caixa relation
 *
 * @method     ChildContasPagarParcelasQuery joinWithCaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Caixa relation
 *
 * @method     ChildContasPagarParcelasQuery leftJoinWithCaixa() Adds a LEFT JOIN clause and with to the query using the Caixa relation
 * @method     ChildContasPagarParcelasQuery rightJoinWithCaixa() Adds a RIGHT JOIN clause and with to the query using the Caixa relation
 * @method     ChildContasPagarParcelasQuery innerJoinWithCaixa() Adds a INNER JOIN clause and with to the query using the Caixa relation
 *
 * @method     ChildContasPagarParcelasQuery leftJoinContasPagar($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagar relation
 * @method     ChildContasPagarParcelasQuery rightJoinContasPagar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagar relation
 * @method     ChildContasPagarParcelasQuery innerJoinContasPagar($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagar relation
 *
 * @method     ChildContasPagarParcelasQuery joinWithContasPagar($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagar relation
 *
 * @method     ChildContasPagarParcelasQuery leftJoinWithContasPagar() Adds a LEFT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildContasPagarParcelasQuery rightJoinWithContasPagar() Adds a RIGHT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildContasPagarParcelasQuery innerJoinWithContasPagar() Adds a INNER JOIN clause and with to the query using the ContasPagar relation
 *
 * @method     \ImaTelecomBundle\Model\CaixaQuery|\ImaTelecomBundle\Model\ContasPagarQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildContasPagarParcelas findOne(ConnectionInterface $con = null) Return the first ChildContasPagarParcelas matching the query
 * @method     ChildContasPagarParcelas findOneOrCreate(ConnectionInterface $con = null) Return the first ChildContasPagarParcelas matching the query, or a new ChildContasPagarParcelas object populated from the query conditions when no match is found
 *
 * @method     ChildContasPagarParcelas findOneByIdcontasPagarParcelas(int $idcontas_pagar_parcelas) Return the first ChildContasPagarParcelas filtered by the idcontas_pagar_parcelas column
 * @method     ChildContasPagarParcelas findOneByValor(string $valor) Return the first ChildContasPagarParcelas filtered by the valor column
 * @method     ChildContasPagarParcelas findOneByVencimento(string $vencimento) Return the first ChildContasPagarParcelas filtered by the vencimento column
 * @method     ChildContasPagarParcelas findOneByContaPagarId(int $conta_pagar_id) Return the first ChildContasPagarParcelas filtered by the conta_pagar_id column
 * @method     ChildContasPagarParcelas findOneByNumParcela(int $num_parcela) Return the first ChildContasPagarParcelas filtered by the num_parcela column
 * @method     ChildContasPagarParcelas findOneByCaixaId(int $caixa_id) Return the first ChildContasPagarParcelas filtered by the caixa_id column
 * @method     ChildContasPagarParcelas findOneByEstaPago(boolean $esta_pago) Return the first ChildContasPagarParcelas filtered by the esta_pago column *

 * @method     ChildContasPagarParcelas requirePk($key, ConnectionInterface $con = null) Return the ChildContasPagarParcelas by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarParcelas requireOne(ConnectionInterface $con = null) Return the first ChildContasPagarParcelas matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContasPagarParcelas requireOneByIdcontasPagarParcelas(int $idcontas_pagar_parcelas) Return the first ChildContasPagarParcelas filtered by the idcontas_pagar_parcelas column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarParcelas requireOneByValor(string $valor) Return the first ChildContasPagarParcelas filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarParcelas requireOneByVencimento(string $vencimento) Return the first ChildContasPagarParcelas filtered by the vencimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarParcelas requireOneByContaPagarId(int $conta_pagar_id) Return the first ChildContasPagarParcelas filtered by the conta_pagar_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarParcelas requireOneByNumParcela(int $num_parcela) Return the first ChildContasPagarParcelas filtered by the num_parcela column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarParcelas requireOneByCaixaId(int $caixa_id) Return the first ChildContasPagarParcelas filtered by the caixa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContasPagarParcelas requireOneByEstaPago(boolean $esta_pago) Return the first ChildContasPagarParcelas filtered by the esta_pago column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContasPagarParcelas[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildContasPagarParcelas objects based on current ModelCriteria
 * @method     ChildContasPagarParcelas[]|ObjectCollection findByIdcontasPagarParcelas(int $idcontas_pagar_parcelas) Return ChildContasPagarParcelas objects filtered by the idcontas_pagar_parcelas column
 * @method     ChildContasPagarParcelas[]|ObjectCollection findByValor(string $valor) Return ChildContasPagarParcelas objects filtered by the valor column
 * @method     ChildContasPagarParcelas[]|ObjectCollection findByVencimento(string $vencimento) Return ChildContasPagarParcelas objects filtered by the vencimento column
 * @method     ChildContasPagarParcelas[]|ObjectCollection findByContaPagarId(int $conta_pagar_id) Return ChildContasPagarParcelas objects filtered by the conta_pagar_id column
 * @method     ChildContasPagarParcelas[]|ObjectCollection findByNumParcela(int $num_parcela) Return ChildContasPagarParcelas objects filtered by the num_parcela column
 * @method     ChildContasPagarParcelas[]|ObjectCollection findByCaixaId(int $caixa_id) Return ChildContasPagarParcelas objects filtered by the caixa_id column
 * @method     ChildContasPagarParcelas[]|ObjectCollection findByEstaPago(boolean $esta_pago) Return ChildContasPagarParcelas objects filtered by the esta_pago column
 * @method     ChildContasPagarParcelas[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ContasPagarParcelasQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ContasPagarParcelasQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ContasPagarParcelas', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildContasPagarParcelasQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildContasPagarParcelasQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildContasPagarParcelasQuery) {
            return $criteria;
        }
        $query = new ChildContasPagarParcelasQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildContasPagarParcelas|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContasPagarParcelasTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ContasPagarParcelasTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagarParcelas A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcontas_pagar_parcelas, valor, vencimento, conta_pagar_id, num_parcela, caixa_id, esta_pago FROM contas_pagar_parcelas WHERE idcontas_pagar_parcelas = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildContasPagarParcelas $obj */
            $obj = new ChildContasPagarParcelas();
            $obj->hydrate($row);
            ContasPagarParcelasTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildContasPagarParcelas|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcontas_pagar_parcelas column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcontasPagarParcelas(1234); // WHERE idcontas_pagar_parcelas = 1234
     * $query->filterByIdcontasPagarParcelas(array(12, 34)); // WHERE idcontas_pagar_parcelas IN (12, 34)
     * $query->filterByIdcontasPagarParcelas(array('min' => 12)); // WHERE idcontas_pagar_parcelas > 12
     * </code>
     *
     * @param     mixed $idcontasPagarParcelas The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByIdcontasPagarParcelas($idcontasPagarParcelas = null, $comparison = null)
    {
        if (is_array($idcontasPagarParcelas)) {
            $useMinMax = false;
            if (isset($idcontasPagarParcelas['min'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, $idcontasPagarParcelas['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcontasPagarParcelas['max'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, $idcontasPagarParcelas['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, $idcontasPagarParcelas, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarParcelasTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query on the vencimento column
     *
     * Example usage:
     * <code>
     * $query->filterByVencimento('2011-03-14'); // WHERE vencimento = '2011-03-14'
     * $query->filterByVencimento('now'); // WHERE vencimento = '2011-03-14'
     * $query->filterByVencimento(array('max' => 'yesterday')); // WHERE vencimento > '2011-03-13'
     * </code>
     *
     * @param     mixed $vencimento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByVencimento($vencimento = null, $comparison = null)
    {
        if (is_array($vencimento)) {
            $useMinMax = false;
            if (isset($vencimento['min'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_VENCIMENTO, $vencimento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vencimento['max'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_VENCIMENTO, $vencimento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarParcelasTableMap::COL_VENCIMENTO, $vencimento, $comparison);
    }

    /**
     * Filter the query on the conta_pagar_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContaPagarId(1234); // WHERE conta_pagar_id = 1234
     * $query->filterByContaPagarId(array(12, 34)); // WHERE conta_pagar_id IN (12, 34)
     * $query->filterByContaPagarId(array('min' => 12)); // WHERE conta_pagar_id > 12
     * </code>
     *
     * @see       filterByContasPagar()
     *
     * @param     mixed $contaPagarId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByContaPagarId($contaPagarId = null, $comparison = null)
    {
        if (is_array($contaPagarId)) {
            $useMinMax = false;
            if (isset($contaPagarId['min'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID, $contaPagarId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contaPagarId['max'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID, $contaPagarId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID, $contaPagarId, $comparison);
    }

    /**
     * Filter the query on the num_parcela column
     *
     * Example usage:
     * <code>
     * $query->filterByNumParcela(1234); // WHERE num_parcela = 1234
     * $query->filterByNumParcela(array(12, 34)); // WHERE num_parcela IN (12, 34)
     * $query->filterByNumParcela(array('min' => 12)); // WHERE num_parcela > 12
     * </code>
     *
     * @param     mixed $numParcela The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByNumParcela($numParcela = null, $comparison = null)
    {
        if (is_array($numParcela)) {
            $useMinMax = false;
            if (isset($numParcela['min'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_NUM_PARCELA, $numParcela['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numParcela['max'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_NUM_PARCELA, $numParcela['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarParcelasTableMap::COL_NUM_PARCELA, $numParcela, $comparison);
    }

    /**
     * Filter the query on the caixa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCaixaId(1234); // WHERE caixa_id = 1234
     * $query->filterByCaixaId(array(12, 34)); // WHERE caixa_id IN (12, 34)
     * $query->filterByCaixaId(array('min' => 12)); // WHERE caixa_id > 12
     * </code>
     *
     * @see       filterByCaixa()
     *
     * @param     mixed $caixaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByCaixaId($caixaId = null, $comparison = null)
    {
        if (is_array($caixaId)) {
            $useMinMax = false;
            if (isset($caixaId['min'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_CAIXA_ID, $caixaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($caixaId['max'])) {
                $this->addUsingAlias(ContasPagarParcelasTableMap::COL_CAIXA_ID, $caixaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContasPagarParcelasTableMap::COL_CAIXA_ID, $caixaId, $comparison);
    }

    /**
     * Filter the query on the esta_pago column
     *
     * Example usage:
     * <code>
     * $query->filterByEstaPago(true); // WHERE esta_pago = true
     * $query->filterByEstaPago('yes'); // WHERE esta_pago = true
     * </code>
     *
     * @param     boolean|string $estaPago The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByEstaPago($estaPago = null, $comparison = null)
    {
        if (is_string($estaPago)) {
            $estaPago = in_array(strtolower($estaPago), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContasPagarParcelasTableMap::COL_ESTA_PAGO, $estaPago, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Caixa object
     *
     * @param \ImaTelecomBundle\Model\Caixa|ObjectCollection $caixa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByCaixa($caixa, $comparison = null)
    {
        if ($caixa instanceof \ImaTelecomBundle\Model\Caixa) {
            return $this
                ->addUsingAlias(ContasPagarParcelasTableMap::COL_CAIXA_ID, $caixa->getIdcaixa(), $comparison);
        } elseif ($caixa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContasPagarParcelasTableMap::COL_CAIXA_ID, $caixa->toKeyValue('PrimaryKey', 'Idcaixa'), $comparison);
        } else {
            throw new PropelException('filterByCaixa() only accepts arguments of type \ImaTelecomBundle\Model\Caixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Caixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function joinCaixa($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Caixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Caixa');
        }

        return $this;
    }

    /**
     * Use the Caixa relation Caixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CaixaQuery A secondary query class using the current class as primary query
     */
    public function useCaixaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Caixa', '\ImaTelecomBundle\Model\CaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagar object
     *
     * @param \ImaTelecomBundle\Model\ContasPagar|ObjectCollection $contasPagar The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function filterByContasPagar($contasPagar, $comparison = null)
    {
        if ($contasPagar instanceof \ImaTelecomBundle\Model\ContasPagar) {
            return $this
                ->addUsingAlias(ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID, $contasPagar->getIdcontasPagar(), $comparison);
        } elseif ($contasPagar instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID, $contasPagar->toKeyValue('PrimaryKey', 'IdcontasPagar'), $comparison);
        } else {
            throw new PropelException('filterByContasPagar() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function joinContasPagar($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagar');
        }

        return $this;
    }

    /**
     * Use the ContasPagar relation ContasPagar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContasPagar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagar', '\ImaTelecomBundle\Model\ContasPagarQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildContasPagarParcelas $contasPagarParcelas Object to remove from the list of results
     *
     * @return $this|ChildContasPagarParcelasQuery The current query, for fluid interface
     */
    public function prune($contasPagarParcelas = null)
    {
        if ($contasPagarParcelas) {
            $this->addUsingAlias(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, $contasPagarParcelas->getIdcontasPagarParcelas(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the contas_pagar_parcelas table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarParcelasTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ContasPagarParcelasTableMap::clearInstancePool();
            ContasPagarParcelasTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarParcelasTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ContasPagarParcelasTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ContasPagarParcelasTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ContasPagarParcelasTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ContasPagarParcelasQuery
