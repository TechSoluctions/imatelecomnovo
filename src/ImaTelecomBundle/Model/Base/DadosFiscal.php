<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Competencia as ChildCompetencia;
use ImaTelecomBundle\Model\CompetenciaQuery as ChildCompetenciaQuery;
use ImaTelecomBundle\Model\DadosFiscal as ChildDadosFiscal;
use ImaTelecomBundle\Model\DadosFiscalItem as ChildDadosFiscalItem;
use ImaTelecomBundle\Model\DadosFiscalItemQuery as ChildDadosFiscalItemQuery;
use ImaTelecomBundle\Model\DadosFiscalQuery as ChildDadosFiscalQuery;
use ImaTelecomBundle\Model\Pessoa as ChildPessoa;
use ImaTelecomBundle\Model\PessoaQuery as ChildPessoaQuery;
use ImaTelecomBundle\Model\Map\DadosFiscalItemTableMap;
use ImaTelecomBundle\Model\Map\DadosFiscalTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'dados_fiscal' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class DadosFiscal implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\DadosFiscalTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the boleto_item_id field.
     *
     * @var        int
     */
    protected $boleto_item_id;

    /**
     * The value for the documento field.
     *
     * @var        string
     */
    protected $documento;

    /**
     * The value for the item field.
     *
     * @var        string
     */
    protected $item;

    /**
     * The value for the mestre field.
     *
     * @var        string
     */
    protected $mestre;

    /**
     * The value for the numero_nota_fiscal field.
     *
     * @var        int
     */
    protected $numero_nota_fiscal;

    /**
     * The value for the competencia_id field.
     *
     * @var        int
     */
    protected $competencia_id;

    /**
     * The value for the data_emissao field.
     *
     * @var        DateTime
     */
    protected $data_emissao;

    /**
     * The value for the mestre_chave_digital_meio field.
     *
     * @var        string
     */
    protected $mestre_chave_digital_meio;

    /**
     * The value for the cfop field.
     *
     * @var        string
     */
    protected $cfop;

    /**
     * The value for the base_calculo field.
     *
     * @var        string
     */
    protected $base_calculo;

    /**
     * The value for the valor_icms field.
     *
     * @var        string
     */
    protected $valor_icms;

    /**
     * The value for the n_tributo field.
     *
     * @var        string
     */
    protected $n_tributo;

    /**
     * The value for the valor_nota field.
     *
     * @var        string
     */
    protected $valor_nota;

    /**
     * The value for the situacao field.
     *
     * @var        string
     */
    protected $situacao;

    /**
     * The value for the servico_cliente_import_id field.
     *
     * @var        int
     */
    protected $servico_cliente_import_id;

    /**
     * The value for the cod_nota_fiscal_lote_previo field.
     *
     * @var        int
     */
    protected $cod_nota_fiscal_lote_previo;

    /**
     * The value for the pessoa_import_id field.
     *
     * @var        int
     */
    protected $pessoa_import_id;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the boleto_id field.
     *
     * @var        int
     */
    protected $boleto_id;

    /**
     * The value for the posicao_primeiro_item field.
     *
     * @var        int
     */
    protected $posicao_primeiro_item;

    /**
     * The value for the posicao_ultimo_item field.
     *
     * @var        int
     */
    protected $posicao_ultimo_item;

    /**
     * The value for the ultima_nota_gerada field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $ultima_nota_gerada;

    /**
     * The value for the tipo_nota field.
     *
     * Note: this column has a database default value of: 'scm'
     * @var        string
     */
    protected $tipo_nota;

    /**
     * The value for the protocolo field.
     *
     * @var        int
     */
    protected $protocolo;

    /**
     * The value for the data_emitido field.
     *
     * @var        DateTime
     */
    protected $data_emitido;

    /**
     * The value for the numero_rps field.
     *
     * @var        int
     */
    protected $numero_rps;

    /**
     * @var        ChildBoleto
     */
    protected $aBoleto;

    /**
     * @var        ChildCompetencia
     */
    protected $aCompetencia;

    /**
     * @var        ChildPessoa
     */
    protected $aPessoa;

    /**
     * @var        ObjectCollection|ChildDadosFiscalItem[] Collection to store aggregation of ChildDadosFiscalItem objects.
     */
    protected $collDadosFiscalItems;
    protected $collDadosFiscalItemsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDadosFiscalItem[]
     */
    protected $dadosFiscalItemsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->ultima_nota_gerada = true;
        $this->tipo_nota = 'scm';
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\DadosFiscal object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>DadosFiscal</code> instance.  If
     * <code>obj</code> is an instance of <code>DadosFiscal</code>, delegates to
     * <code>equals(DadosFiscal)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|DadosFiscal The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [boleto_item_id] column value.
     *
     * @return int
     */
    public function getBoletoItemId()
    {
        return $this->boleto_item_id;
    }

    /**
     * Get the [documento] column value.
     *
     * @return string
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Get the [item] column value.
     *
     * @return string
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Get the [mestre] column value.
     *
     * @return string
     */
    public function getMestre()
    {
        return $this->mestre;
    }

    /**
     * Get the [numero_nota_fiscal] column value.
     *
     * @return int
     */
    public function getNumeroNotaFiscal()
    {
        return $this->numero_nota_fiscal;
    }

    /**
     * Get the [competencia_id] column value.
     *
     * @return int
     */
    public function getCompetenciaId()
    {
        return $this->competencia_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_emissao] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataEmissao($format = NULL)
    {
        if ($format === null) {
            return $this->data_emissao;
        } else {
            return $this->data_emissao instanceof \DateTimeInterface ? $this->data_emissao->format($format) : null;
        }
    }

    /**
     * Get the [mestre_chave_digital_meio] column value.
     *
     * @return string
     */
    public function getMestreChaveDigitalMeio()
    {
        return $this->mestre_chave_digital_meio;
    }

    /**
     * Get the [cfop] column value.
     *
     * @return string
     */
    public function getCfop()
    {
        return $this->cfop;
    }

    /**
     * Get the [base_calculo] column value.
     *
     * @return string
     */
    public function getBaseCalculo()
    {
        return $this->base_calculo;
    }

    /**
     * Get the [valor_icms] column value.
     *
     * @return string
     */
    public function getValorIcms()
    {
        return $this->valor_icms;
    }

    /**
     * Get the [n_tributo] column value.
     *
     * @return string
     */
    public function getNTributo()
    {
        return $this->n_tributo;
    }

    /**
     * Get the [valor_nota] column value.
     *
     * @return string
     */
    public function getValorNota()
    {
        return $this->valor_nota;
    }

    /**
     * Get the [situacao] column value.
     *
     * @return string
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * Get the [servico_cliente_import_id] column value.
     *
     * @return int
     */
    public function getServicoClienteImportId()
    {
        return $this->servico_cliente_import_id;
    }

    /**
     * Get the [cod_nota_fiscal_lote_previo] column value.
     *
     * @return int
     */
    public function getCodNotaFiscalLotePrevio()
    {
        return $this->cod_nota_fiscal_lote_previo;
    }

    /**
     * Get the [pessoa_import_id] column value.
     *
     * @return int
     */
    public function getPessoaImportId()
    {
        return $this->pessoa_import_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [boleto_id] column value.
     *
     * @return int
     */
    public function getBoletoId()
    {
        return $this->boleto_id;
    }

    /**
     * Get the [posicao_primeiro_item] column value.
     *
     * @return int
     */
    public function getPosicaoPrimeiroItem()
    {
        return $this->posicao_primeiro_item;
    }

    /**
     * Get the [posicao_ultimo_item] column value.
     *
     * @return int
     */
    public function getPosicaoUltimoItem()
    {
        return $this->posicao_ultimo_item;
    }

    /**
     * Get the [ultima_nota_gerada] column value.
     *
     * @return boolean
     */
    public function getUltimaNotaGerada()
    {
        return $this->ultima_nota_gerada;
    }

    /**
     * Get the [ultima_nota_gerada] column value.
     *
     * @return boolean
     */
    public function isUltimaNotaGerada()
    {
        return $this->getUltimaNotaGerada();
    }

    /**
     * Get the [tipo_nota] column value.
     *
     * @return string
     */
    public function getTipoNota()
    {
        return $this->tipo_nota;
    }

    /**
     * Get the [protocolo] column value.
     *
     * @return int
     */
    public function getProtocolo()
    {
        return $this->protocolo;
    }

    /**
     * Get the [optionally formatted] temporal [data_emitido] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataEmitido($format = NULL)
    {
        if ($format === null) {
            return $this->data_emitido;
        } else {
            return $this->data_emitido instanceof \DateTimeInterface ? $this->data_emitido->format($format) : null;
        }
    }

    /**
     * Get the [numero_rps] column value.
     *
     * @return int
     */
    public function getNumeroRps()
    {
        return $this->numero_rps;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [boleto_item_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setBoletoItemId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->boleto_item_id !== $v) {
            $this->boleto_item_id = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_BOLETO_ITEM_ID] = true;
        }

        return $this;
    } // setBoletoItemId()

    /**
     * Set the value of [documento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setDocumento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->documento !== $v) {
            $this->documento = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_DOCUMENTO] = true;
        }

        return $this;
    } // setDocumento()

    /**
     * Set the value of [item] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setItem($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->item !== $v) {
            $this->item = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_ITEM] = true;
        }

        return $this;
    } // setItem()

    /**
     * Set the value of [mestre] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setMestre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mestre !== $v) {
            $this->mestre = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_MESTRE] = true;
        }

        return $this;
    } // setMestre()

    /**
     * Set the value of [numero_nota_fiscal] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setNumeroNotaFiscal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->numero_nota_fiscal !== $v) {
            $this->numero_nota_fiscal = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL] = true;
        }

        return $this;
    } // setNumeroNotaFiscal()

    /**
     * Set the value of [competencia_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setCompetenciaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->competencia_id !== $v) {
            $this->competencia_id = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_COMPETENCIA_ID] = true;
        }

        if ($this->aCompetencia !== null && $this->aCompetencia->getId() !== $v) {
            $this->aCompetencia = null;
        }

        return $this;
    } // setCompetenciaId()

    /**
     * Sets the value of [data_emissao] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setDataEmissao($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_emissao !== null || $dt !== null) {
            if ($this->data_emissao === null || $dt === null || $dt->format("Y-m-d") !== $this->data_emissao->format("Y-m-d")) {
                $this->data_emissao = $dt === null ? null : clone $dt;
                $this->modifiedColumns[DadosFiscalTableMap::COL_DATA_EMISSAO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataEmissao()

    /**
     * Set the value of [mestre_chave_digital_meio] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setMestreChaveDigitalMeio($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mestre_chave_digital_meio !== $v) {
            $this->mestre_chave_digital_meio = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_MESTRE_CHAVE_DIGITAL_MEIO] = true;
        }

        return $this;
    } // setMestreChaveDigitalMeio()

    /**
     * Set the value of [cfop] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setCfop($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cfop !== $v) {
            $this->cfop = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_CFOP] = true;
        }

        return $this;
    } // setCfop()

    /**
     * Set the value of [base_calculo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setBaseCalculo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->base_calculo !== $v) {
            $this->base_calculo = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_BASE_CALCULO] = true;
        }

        return $this;
    } // setBaseCalculo()

    /**
     * Set the value of [valor_icms] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setValorIcms($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_icms !== $v) {
            $this->valor_icms = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_VALOR_ICMS] = true;
        }

        return $this;
    } // setValorIcms()

    /**
     * Set the value of [n_tributo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setNTributo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->n_tributo !== $v) {
            $this->n_tributo = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_N_TRIBUTO] = true;
        }

        return $this;
    } // setNTributo()

    /**
     * Set the value of [valor_nota] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setValorNota($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_nota !== $v) {
            $this->valor_nota = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_VALOR_NOTA] = true;
        }

        return $this;
    } // setValorNota()

    /**
     * Set the value of [situacao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setSituacao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->situacao !== $v) {
            $this->situacao = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_SITUACAO] = true;
        }

        return $this;
    } // setSituacao()

    /**
     * Set the value of [servico_cliente_import_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setServicoClienteImportId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->servico_cliente_import_id !== $v) {
            $this->servico_cliente_import_id = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID] = true;
        }

        return $this;
    } // setServicoClienteImportId()

    /**
     * Set the value of [cod_nota_fiscal_lote_previo] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setCodNotaFiscalLotePrevio($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cod_nota_fiscal_lote_previo !== $v) {
            $this->cod_nota_fiscal_lote_previo = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO] = true;
        }

        return $this;
    } // setCodNotaFiscalLotePrevio()

    /**
     * Set the value of [pessoa_import_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setPessoaImportId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pessoa_import_id !== $v) {
            $this->pessoa_import_id = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_PESSOA_IMPORT_ID] = true;
        }

        if ($this->aPessoa !== null && $this->aPessoa->getImportId() !== $v) {
            $this->aPessoa = null;
        }

        return $this;
    } // setPessoaImportId()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[DadosFiscalTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[DadosFiscalTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_USUARIO_ALTERADO] = true;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Set the value of [boleto_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setBoletoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->boleto_id !== $v) {
            $this->boleto_id = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_BOLETO_ID] = true;
        }

        if ($this->aBoleto !== null && $this->aBoleto->getIdboleto() !== $v) {
            $this->aBoleto = null;
        }

        return $this;
    } // setBoletoId()

    /**
     * Set the value of [posicao_primeiro_item] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setPosicaoPrimeiroItem($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_primeiro_item !== $v) {
            $this->posicao_primeiro_item = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM] = true;
        }

        return $this;
    } // setPosicaoPrimeiroItem()

    /**
     * Set the value of [posicao_ultimo_item] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setPosicaoUltimoItem($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_ultimo_item !== $v) {
            $this->posicao_ultimo_item = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM] = true;
        }

        return $this;
    } // setPosicaoUltimoItem()

    /**
     * Sets the value of the [ultima_nota_gerada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setUltimaNotaGerada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ultima_nota_gerada !== $v) {
            $this->ultima_nota_gerada = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_ULTIMA_NOTA_GERADA] = true;
        }

        return $this;
    } // setUltimaNotaGerada()

    /**
     * Set the value of [tipo_nota] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setTipoNota($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_nota !== $v) {
            $this->tipo_nota = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_TIPO_NOTA] = true;
        }

        return $this;
    } // setTipoNota()

    /**
     * Set the value of [protocolo] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setProtocolo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->protocolo !== $v) {
            $this->protocolo = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_PROTOCOLO] = true;
        }

        return $this;
    } // setProtocolo()

    /**
     * Sets the value of [data_emitido] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setDataEmitido($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_emitido !== null || $dt !== null) {
            if ($this->data_emitido === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_emitido->format("Y-m-d H:i:s.u")) {
                $this->data_emitido = $dt === null ? null : clone $dt;
                $this->modifiedColumns[DadosFiscalTableMap::COL_DATA_EMITIDO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataEmitido()

    /**
     * Set the value of [numero_rps] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function setNumeroRps($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->numero_rps !== $v) {
            $this->numero_rps = $v;
            $this->modifiedColumns[DadosFiscalTableMap::COL_NUMERO_RPS] = true;
        }

        return $this;
    } // setNumeroRps()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->ultima_nota_gerada !== true) {
                return false;
            }

            if ($this->tipo_nota !== 'scm') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : DadosFiscalTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : DadosFiscalTableMap::translateFieldName('BoletoItemId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->boleto_item_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : DadosFiscalTableMap::translateFieldName('Documento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->documento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : DadosFiscalTableMap::translateFieldName('Item', TableMap::TYPE_PHPNAME, $indexType)];
            $this->item = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : DadosFiscalTableMap::translateFieldName('Mestre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mestre = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : DadosFiscalTableMap::translateFieldName('NumeroNotaFiscal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->numero_nota_fiscal = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : DadosFiscalTableMap::translateFieldName('CompetenciaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->competencia_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : DadosFiscalTableMap::translateFieldName('DataEmissao', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->data_emissao = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : DadosFiscalTableMap::translateFieldName('MestreChaveDigitalMeio', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mestre_chave_digital_meio = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : DadosFiscalTableMap::translateFieldName('Cfop', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cfop = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : DadosFiscalTableMap::translateFieldName('BaseCalculo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->base_calculo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : DadosFiscalTableMap::translateFieldName('ValorIcms', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_icms = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : DadosFiscalTableMap::translateFieldName('NTributo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->n_tributo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : DadosFiscalTableMap::translateFieldName('ValorNota', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_nota = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : DadosFiscalTableMap::translateFieldName('Situacao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->situacao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : DadosFiscalTableMap::translateFieldName('ServicoClienteImportId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->servico_cliente_import_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : DadosFiscalTableMap::translateFieldName('CodNotaFiscalLotePrevio', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cod_nota_fiscal_lote_previo = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : DadosFiscalTableMap::translateFieldName('PessoaImportId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pessoa_import_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : DadosFiscalTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : DadosFiscalTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : DadosFiscalTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : DadosFiscalTableMap::translateFieldName('BoletoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->boleto_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : DadosFiscalTableMap::translateFieldName('PosicaoPrimeiroItem', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_primeiro_item = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : DadosFiscalTableMap::translateFieldName('PosicaoUltimoItem', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_ultimo_item = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : DadosFiscalTableMap::translateFieldName('UltimaNotaGerada', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ultima_nota_gerada = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : DadosFiscalTableMap::translateFieldName('TipoNota', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_nota = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : DadosFiscalTableMap::translateFieldName('Protocolo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->protocolo = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : DadosFiscalTableMap::translateFieldName('DataEmitido', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_emitido = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : DadosFiscalTableMap::translateFieldName('NumeroRps', TableMap::TYPE_PHPNAME, $indexType)];
            $this->numero_rps = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 29; // 29 = DadosFiscalTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\DadosFiscal'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCompetencia !== null && $this->competencia_id !== $this->aCompetencia->getId()) {
            $this->aCompetencia = null;
        }
        if ($this->aPessoa !== null && $this->pessoa_import_id !== $this->aPessoa->getImportId()) {
            $this->aPessoa = null;
        }
        if ($this->aBoleto !== null && $this->boleto_id !== $this->aBoleto->getIdboleto()) {
            $this->aBoleto = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DadosFiscalTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildDadosFiscalQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBoleto = null;
            $this->aCompetencia = null;
            $this->aPessoa = null;
            $this->collDadosFiscalItems = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see DadosFiscal::setDeleted()
     * @see DadosFiscal::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildDadosFiscalQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DadosFiscalTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBoleto !== null) {
                if ($this->aBoleto->isModified() || $this->aBoleto->isNew()) {
                    $affectedRows += $this->aBoleto->save($con);
                }
                $this->setBoleto($this->aBoleto);
            }

            if ($this->aCompetencia !== null) {
                if ($this->aCompetencia->isModified() || $this->aCompetencia->isNew()) {
                    $affectedRows += $this->aCompetencia->save($con);
                }
                $this->setCompetencia($this->aCompetencia);
            }

            if ($this->aPessoa !== null) {
                if ($this->aPessoa->isModified() || $this->aPessoa->isNew()) {
                    $affectedRows += $this->aPessoa->save($con);
                }
                $this->setPessoa($this->aPessoa);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->dadosFiscalItemsScheduledForDeletion !== null) {
                if (!$this->dadosFiscalItemsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\DadosFiscalItemQuery::create()
                        ->filterByPrimaryKeys($this->dadosFiscalItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dadosFiscalItemsScheduledForDeletion = null;
                }
            }

            if ($this->collDadosFiscalItems !== null) {
                foreach ($this->collDadosFiscalItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[DadosFiscalTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . DadosFiscalTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(DadosFiscalTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_BOLETO_ITEM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'boleto_item_id';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DOCUMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'documento';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_ITEM)) {
            $modifiedColumns[':p' . $index++]  = 'item';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_MESTRE)) {
            $modifiedColumns[':p' . $index++]  = 'mestre';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL)) {
            $modifiedColumns[':p' . $index++]  = 'numero_nota_fiscal';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_COMPETENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'competencia_id';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DATA_EMISSAO)) {
            $modifiedColumns[':p' . $index++]  = 'data_emissao';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_MESTRE_CHAVE_DIGITAL_MEIO)) {
            $modifiedColumns[':p' . $index++]  = 'mestre_chave_digital_meio';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_CFOP)) {
            $modifiedColumns[':p' . $index++]  = 'cfop';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_BASE_CALCULO)) {
            $modifiedColumns[':p' . $index++]  = 'base_calculo';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_VALOR_ICMS)) {
            $modifiedColumns[':p' . $index++]  = 'valor_icms';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_N_TRIBUTO)) {
            $modifiedColumns[':p' . $index++]  = 'n_tributo';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_VALOR_NOTA)) {
            $modifiedColumns[':p' . $index++]  = 'valor_nota';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_SITUACAO)) {
            $modifiedColumns[':p' . $index++]  = 'situacao';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'servico_cliente_import_id';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO)) {
            $modifiedColumns[':p' . $index++]  = 'cod_nota_fiscal_lote_previo';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_PESSOA_IMPORT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pessoa_import_id';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_BOLETO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'boleto_id';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_primeiro_item';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_ultimo_item';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_ULTIMA_NOTA_GERADA)) {
            $modifiedColumns[':p' . $index++]  = 'ultima_nota_gerada';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_TIPO_NOTA)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_nota';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_PROTOCOLO)) {
            $modifiedColumns[':p' . $index++]  = 'protocolo';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DATA_EMITIDO)) {
            $modifiedColumns[':p' . $index++]  = 'data_emitido';
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_NUMERO_RPS)) {
            $modifiedColumns[':p' . $index++]  = 'numero_rps';
        }

        $sql = sprintf(
            'INSERT INTO dados_fiscal (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'boleto_item_id':
                        $stmt->bindValue($identifier, $this->boleto_item_id, PDO::PARAM_INT);
                        break;
                    case 'documento':
                        $stmt->bindValue($identifier, $this->documento, PDO::PARAM_STR);
                        break;
                    case 'item':
                        $stmt->bindValue($identifier, $this->item, PDO::PARAM_STR);
                        break;
                    case 'mestre':
                        $stmt->bindValue($identifier, $this->mestre, PDO::PARAM_STR);
                        break;
                    case 'numero_nota_fiscal':
                        $stmt->bindValue($identifier, $this->numero_nota_fiscal, PDO::PARAM_INT);
                        break;
                    case 'competencia_id':
                        $stmt->bindValue($identifier, $this->competencia_id, PDO::PARAM_INT);
                        break;
                    case 'data_emissao':
                        $stmt->bindValue($identifier, $this->data_emissao ? $this->data_emissao->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'mestre_chave_digital_meio':
                        $stmt->bindValue($identifier, $this->mestre_chave_digital_meio, PDO::PARAM_STR);
                        break;
                    case 'cfop':
                        $stmt->bindValue($identifier, $this->cfop, PDO::PARAM_STR);
                        break;
                    case 'base_calculo':
                        $stmt->bindValue($identifier, $this->base_calculo, PDO::PARAM_STR);
                        break;
                    case 'valor_icms':
                        $stmt->bindValue($identifier, $this->valor_icms, PDO::PARAM_STR);
                        break;
                    case 'n_tributo':
                        $stmt->bindValue($identifier, $this->n_tributo, PDO::PARAM_STR);
                        break;
                    case 'valor_nota':
                        $stmt->bindValue($identifier, $this->valor_nota, PDO::PARAM_STR);
                        break;
                    case 'situacao':
                        $stmt->bindValue($identifier, $this->situacao, PDO::PARAM_STR);
                        break;
                    case 'servico_cliente_import_id':
                        $stmt->bindValue($identifier, $this->servico_cliente_import_id, PDO::PARAM_INT);
                        break;
                    case 'cod_nota_fiscal_lote_previo':
                        $stmt->bindValue($identifier, $this->cod_nota_fiscal_lote_previo, PDO::PARAM_INT);
                        break;
                    case 'pessoa_import_id':
                        $stmt->bindValue($identifier, $this->pessoa_import_id, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'boleto_id':
                        $stmt->bindValue($identifier, $this->boleto_id, PDO::PARAM_INT);
                        break;
                    case 'posicao_primeiro_item':
                        $stmt->bindValue($identifier, $this->posicao_primeiro_item, PDO::PARAM_INT);
                        break;
                    case 'posicao_ultimo_item':
                        $stmt->bindValue($identifier, $this->posicao_ultimo_item, PDO::PARAM_INT);
                        break;
                    case 'ultima_nota_gerada':
                        $stmt->bindValue($identifier, (int) $this->ultima_nota_gerada, PDO::PARAM_INT);
                        break;
                    case 'tipo_nota':
                        $stmt->bindValue($identifier, $this->tipo_nota, PDO::PARAM_STR);
                        break;
                    case 'protocolo':
                        $stmt->bindValue($identifier, $this->protocolo, PDO::PARAM_INT);
                        break;
                    case 'data_emitido':
                        $stmt->bindValue($identifier, $this->data_emitido ? $this->data_emitido->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'numero_rps':
                        $stmt->bindValue($identifier, $this->numero_rps, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DadosFiscalTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getBoletoItemId();
                break;
            case 2:
                return $this->getDocumento();
                break;
            case 3:
                return $this->getItem();
                break;
            case 4:
                return $this->getMestre();
                break;
            case 5:
                return $this->getNumeroNotaFiscal();
                break;
            case 6:
                return $this->getCompetenciaId();
                break;
            case 7:
                return $this->getDataEmissao();
                break;
            case 8:
                return $this->getMestreChaveDigitalMeio();
                break;
            case 9:
                return $this->getCfop();
                break;
            case 10:
                return $this->getBaseCalculo();
                break;
            case 11:
                return $this->getValorIcms();
                break;
            case 12:
                return $this->getNTributo();
                break;
            case 13:
                return $this->getValorNota();
                break;
            case 14:
                return $this->getSituacao();
                break;
            case 15:
                return $this->getServicoClienteImportId();
                break;
            case 16:
                return $this->getCodNotaFiscalLotePrevio();
                break;
            case 17:
                return $this->getPessoaImportId();
                break;
            case 18:
                return $this->getDataCadastro();
                break;
            case 19:
                return $this->getDataAlterado();
                break;
            case 20:
                return $this->getUsuarioAlterado();
                break;
            case 21:
                return $this->getBoletoId();
                break;
            case 22:
                return $this->getPosicaoPrimeiroItem();
                break;
            case 23:
                return $this->getPosicaoUltimoItem();
                break;
            case 24:
                return $this->getUltimaNotaGerada();
                break;
            case 25:
                return $this->getTipoNota();
                break;
            case 26:
                return $this->getProtocolo();
                break;
            case 27:
                return $this->getDataEmitido();
                break;
            case 28:
                return $this->getNumeroRps();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['DadosFiscal'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['DadosFiscal'][$this->hashCode()] = true;
        $keys = DadosFiscalTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getBoletoItemId(),
            $keys[2] => $this->getDocumento(),
            $keys[3] => $this->getItem(),
            $keys[4] => $this->getMestre(),
            $keys[5] => $this->getNumeroNotaFiscal(),
            $keys[6] => $this->getCompetenciaId(),
            $keys[7] => $this->getDataEmissao(),
            $keys[8] => $this->getMestreChaveDigitalMeio(),
            $keys[9] => $this->getCfop(),
            $keys[10] => $this->getBaseCalculo(),
            $keys[11] => $this->getValorIcms(),
            $keys[12] => $this->getNTributo(),
            $keys[13] => $this->getValorNota(),
            $keys[14] => $this->getSituacao(),
            $keys[15] => $this->getServicoClienteImportId(),
            $keys[16] => $this->getCodNotaFiscalLotePrevio(),
            $keys[17] => $this->getPessoaImportId(),
            $keys[18] => $this->getDataCadastro(),
            $keys[19] => $this->getDataAlterado(),
            $keys[20] => $this->getUsuarioAlterado(),
            $keys[21] => $this->getBoletoId(),
            $keys[22] => $this->getPosicaoPrimeiroItem(),
            $keys[23] => $this->getPosicaoUltimoItem(),
            $keys[24] => $this->getUltimaNotaGerada(),
            $keys[25] => $this->getTipoNota(),
            $keys[26] => $this->getProtocolo(),
            $keys[27] => $this->getDataEmitido(),
            $keys[28] => $this->getNumeroRps(),
        );
        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[19]] instanceof \DateTime) {
            $result[$keys[19]] = $result[$keys[19]]->format('c');
        }

        if ($result[$keys[27]] instanceof \DateTime) {
            $result[$keys[27]] = $result[$keys[27]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBoleto) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boleto';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto';
                        break;
                    default:
                        $key = 'Boleto';
                }

                $result[$key] = $this->aBoleto->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCompetencia) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'competencia';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'competencia';
                        break;
                    default:
                        $key = 'Competencia';
                }

                $result[$key] = $this->aCompetencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPessoa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pessoa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'pessoa';
                        break;
                    default:
                        $key = 'Pessoa';
                }

                $result[$key] = $this->aPessoa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collDadosFiscalItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dadosFiscalItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'dados_fiscal_items';
                        break;
                    default:
                        $key = 'DadosFiscalItems';
                }

                $result[$key] = $this->collDadosFiscalItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DadosFiscalTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setBoletoItemId($value);
                break;
            case 2:
                $this->setDocumento($value);
                break;
            case 3:
                $this->setItem($value);
                break;
            case 4:
                $this->setMestre($value);
                break;
            case 5:
                $this->setNumeroNotaFiscal($value);
                break;
            case 6:
                $this->setCompetenciaId($value);
                break;
            case 7:
                $this->setDataEmissao($value);
                break;
            case 8:
                $this->setMestreChaveDigitalMeio($value);
                break;
            case 9:
                $this->setCfop($value);
                break;
            case 10:
                $this->setBaseCalculo($value);
                break;
            case 11:
                $this->setValorIcms($value);
                break;
            case 12:
                $this->setNTributo($value);
                break;
            case 13:
                $this->setValorNota($value);
                break;
            case 14:
                $this->setSituacao($value);
                break;
            case 15:
                $this->setServicoClienteImportId($value);
                break;
            case 16:
                $this->setCodNotaFiscalLotePrevio($value);
                break;
            case 17:
                $this->setPessoaImportId($value);
                break;
            case 18:
                $this->setDataCadastro($value);
                break;
            case 19:
                $this->setDataAlterado($value);
                break;
            case 20:
                $this->setUsuarioAlterado($value);
                break;
            case 21:
                $this->setBoletoId($value);
                break;
            case 22:
                $this->setPosicaoPrimeiroItem($value);
                break;
            case 23:
                $this->setPosicaoUltimoItem($value);
                break;
            case 24:
                $this->setUltimaNotaGerada($value);
                break;
            case 25:
                $this->setTipoNota($value);
                break;
            case 26:
                $this->setProtocolo($value);
                break;
            case 27:
                $this->setDataEmitido($value);
                break;
            case 28:
                $this->setNumeroRps($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = DadosFiscalTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBoletoItemId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDocumento($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setItem($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setMestre($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setNumeroNotaFiscal($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCompetenciaId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDataEmissao($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setMestreChaveDigitalMeio($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setCfop($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setBaseCalculo($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setValorIcms($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setNTributo($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setValorNota($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setSituacao($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setServicoClienteImportId($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCodNotaFiscalLotePrevio($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setPessoaImportId($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setDataCadastro($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setDataAlterado($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setBoletoId($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setPosicaoPrimeiroItem($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setPosicaoUltimoItem($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setUltimaNotaGerada($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setTipoNota($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setProtocolo($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setDataEmitido($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setNumeroRps($arr[$keys[28]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DadosFiscalTableMap::DATABASE_NAME);

        if ($this->isColumnModified(DadosFiscalTableMap::COL_ID)) {
            $criteria->add(DadosFiscalTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_BOLETO_ITEM_ID)) {
            $criteria->add(DadosFiscalTableMap::COL_BOLETO_ITEM_ID, $this->boleto_item_id);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DOCUMENTO)) {
            $criteria->add(DadosFiscalTableMap::COL_DOCUMENTO, $this->documento);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_ITEM)) {
            $criteria->add(DadosFiscalTableMap::COL_ITEM, $this->item);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_MESTRE)) {
            $criteria->add(DadosFiscalTableMap::COL_MESTRE, $this->mestre);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL)) {
            $criteria->add(DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL, $this->numero_nota_fiscal);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_COMPETENCIA_ID)) {
            $criteria->add(DadosFiscalTableMap::COL_COMPETENCIA_ID, $this->competencia_id);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DATA_EMISSAO)) {
            $criteria->add(DadosFiscalTableMap::COL_DATA_EMISSAO, $this->data_emissao);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_MESTRE_CHAVE_DIGITAL_MEIO)) {
            $criteria->add(DadosFiscalTableMap::COL_MESTRE_CHAVE_DIGITAL_MEIO, $this->mestre_chave_digital_meio);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_CFOP)) {
            $criteria->add(DadosFiscalTableMap::COL_CFOP, $this->cfop);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_BASE_CALCULO)) {
            $criteria->add(DadosFiscalTableMap::COL_BASE_CALCULO, $this->base_calculo);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_VALOR_ICMS)) {
            $criteria->add(DadosFiscalTableMap::COL_VALOR_ICMS, $this->valor_icms);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_N_TRIBUTO)) {
            $criteria->add(DadosFiscalTableMap::COL_N_TRIBUTO, $this->n_tributo);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_VALOR_NOTA)) {
            $criteria->add(DadosFiscalTableMap::COL_VALOR_NOTA, $this->valor_nota);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_SITUACAO)) {
            $criteria->add(DadosFiscalTableMap::COL_SITUACAO, $this->situacao);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID)) {
            $criteria->add(DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID, $this->servico_cliente_import_id);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO)) {
            $criteria->add(DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO, $this->cod_nota_fiscal_lote_previo);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_PESSOA_IMPORT_ID)) {
            $criteria->add(DadosFiscalTableMap::COL_PESSOA_IMPORT_ID, $this->pessoa_import_id);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(DadosFiscalTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(DadosFiscalTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(DadosFiscalTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_BOLETO_ID)) {
            $criteria->add(DadosFiscalTableMap::COL_BOLETO_ID, $this->boleto_id);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM)) {
            $criteria->add(DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM, $this->posicao_primeiro_item);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM)) {
            $criteria->add(DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM, $this->posicao_ultimo_item);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_ULTIMA_NOTA_GERADA)) {
            $criteria->add(DadosFiscalTableMap::COL_ULTIMA_NOTA_GERADA, $this->ultima_nota_gerada);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_TIPO_NOTA)) {
            $criteria->add(DadosFiscalTableMap::COL_TIPO_NOTA, $this->tipo_nota);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_PROTOCOLO)) {
            $criteria->add(DadosFiscalTableMap::COL_PROTOCOLO, $this->protocolo);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_DATA_EMITIDO)) {
            $criteria->add(DadosFiscalTableMap::COL_DATA_EMITIDO, $this->data_emitido);
        }
        if ($this->isColumnModified(DadosFiscalTableMap::COL_NUMERO_RPS)) {
            $criteria->add(DadosFiscalTableMap::COL_NUMERO_RPS, $this->numero_rps);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildDadosFiscalQuery::create();
        $criteria->add(DadosFiscalTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\DadosFiscal (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBoletoItemId($this->getBoletoItemId());
        $copyObj->setDocumento($this->getDocumento());
        $copyObj->setItem($this->getItem());
        $copyObj->setMestre($this->getMestre());
        $copyObj->setNumeroNotaFiscal($this->getNumeroNotaFiscal());
        $copyObj->setCompetenciaId($this->getCompetenciaId());
        $copyObj->setDataEmissao($this->getDataEmissao());
        $copyObj->setMestreChaveDigitalMeio($this->getMestreChaveDigitalMeio());
        $copyObj->setCfop($this->getCfop());
        $copyObj->setBaseCalculo($this->getBaseCalculo());
        $copyObj->setValorIcms($this->getValorIcms());
        $copyObj->setNTributo($this->getNTributo());
        $copyObj->setValorNota($this->getValorNota());
        $copyObj->setSituacao($this->getSituacao());
        $copyObj->setServicoClienteImportId($this->getServicoClienteImportId());
        $copyObj->setCodNotaFiscalLotePrevio($this->getCodNotaFiscalLotePrevio());
        $copyObj->setPessoaImportId($this->getPessoaImportId());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setBoletoId($this->getBoletoId());
        $copyObj->setPosicaoPrimeiroItem($this->getPosicaoPrimeiroItem());
        $copyObj->setPosicaoUltimoItem($this->getPosicaoUltimoItem());
        $copyObj->setUltimaNotaGerada($this->getUltimaNotaGerada());
        $copyObj->setTipoNota($this->getTipoNota());
        $copyObj->setProtocolo($this->getProtocolo());
        $copyObj->setDataEmitido($this->getDataEmitido());
        $copyObj->setNumeroRps($this->getNumeroRps());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getDadosFiscalItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDadosFiscalItem($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\DadosFiscal Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBoleto object.
     *
     * @param  ChildBoleto $v
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBoleto(ChildBoleto $v = null)
    {
        if ($v === null) {
            $this->setBoletoId(NULL);
        } else {
            $this->setBoletoId($v->getIdboleto());
        }

        $this->aBoleto = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBoleto object, it will not be re-added.
        if ($v !== null) {
            $v->addDadosFiscal($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBoleto object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBoleto The associated ChildBoleto object.
     * @throws PropelException
     */
    public function getBoleto(ConnectionInterface $con = null)
    {
        if ($this->aBoleto === null && ($this->boleto_id !== null)) {
            $this->aBoleto = ChildBoletoQuery::create()->findPk($this->boleto_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBoleto->addDadosFiscals($this);
             */
        }

        return $this->aBoleto;
    }

    /**
     * Declares an association between this object and a ChildCompetencia object.
     *
     * @param  ChildCompetencia $v
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCompetencia(ChildCompetencia $v = null)
    {
        if ($v === null) {
            $this->setCompetenciaId(NULL);
        } else {
            $this->setCompetenciaId($v->getId());
        }

        $this->aCompetencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCompetencia object, it will not be re-added.
        if ($v !== null) {
            $v->addDadosFiscal($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCompetencia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCompetencia The associated ChildCompetencia object.
     * @throws PropelException
     */
    public function getCompetencia(ConnectionInterface $con = null)
    {
        if ($this->aCompetencia === null && ($this->competencia_id !== null)) {
            $this->aCompetencia = ChildCompetenciaQuery::create()->findPk($this->competencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCompetencia->addDadosFiscals($this);
             */
        }

        return $this->aCompetencia;
    }

    /**
     * Declares an association between this object and a ChildPessoa object.
     *
     * @param  ChildPessoa $v
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPessoa(ChildPessoa $v = null)
    {
        if ($v === null) {
            $this->setPessoaImportId(NULL);
        } else {
            $this->setPessoaImportId($v->getImportId());
        }

        $this->aPessoa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPessoa object, it will not be re-added.
        if ($v !== null) {
            $v->addDadosFiscal($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPessoa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPessoa The associated ChildPessoa object.
     * @throws PropelException
     */
    public function getPessoa(ConnectionInterface $con = null)
    {
        if ($this->aPessoa === null && ($this->pessoa_import_id !== null)) {
            $this->aPessoa = ChildPessoaQuery::create()
                ->filterByDadosFiscal($this) // here
                ->findOne($con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPessoa->addDadosFiscals($this);
             */
        }

        return $this->aPessoa;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('DadosFiscalItem' == $relationName) {
            return $this->initDadosFiscalItems();
        }
    }

    /**
     * Clears out the collDadosFiscalItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDadosFiscalItems()
     */
    public function clearDadosFiscalItems()
    {
        $this->collDadosFiscalItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDadosFiscalItems collection loaded partially.
     */
    public function resetPartialDadosFiscalItems($v = true)
    {
        $this->collDadosFiscalItemsPartial = $v;
    }

    /**
     * Initializes the collDadosFiscalItems collection.
     *
     * By default this just sets the collDadosFiscalItems collection to an empty array (like clearcollDadosFiscalItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDadosFiscalItems($overrideExisting = true)
    {
        if (null !== $this->collDadosFiscalItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = DadosFiscalItemTableMap::getTableMap()->getCollectionClassName();

        $this->collDadosFiscalItems = new $collectionClassName;
        $this->collDadosFiscalItems->setModel('\ImaTelecomBundle\Model\DadosFiscalItem');
    }

    /**
     * Gets an array of ChildDadosFiscalItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDadosFiscal is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDadosFiscalItem[] List of ChildDadosFiscalItem objects
     * @throws PropelException
     */
    public function getDadosFiscalItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalItemsPartial && !$this->isNew();
        if (null === $this->collDadosFiscalItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscalItems) {
                // return empty collection
                $this->initDadosFiscalItems();
            } else {
                $collDadosFiscalItems = ChildDadosFiscalItemQuery::create(null, $criteria)
                    ->filterByDadosFiscal($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDadosFiscalItemsPartial && count($collDadosFiscalItems)) {
                        $this->initDadosFiscalItems(false);

                        foreach ($collDadosFiscalItems as $obj) {
                            if (false == $this->collDadosFiscalItems->contains($obj)) {
                                $this->collDadosFiscalItems->append($obj);
                            }
                        }

                        $this->collDadosFiscalItemsPartial = true;
                    }

                    return $collDadosFiscalItems;
                }

                if ($partial && $this->collDadosFiscalItems) {
                    foreach ($this->collDadosFiscalItems as $obj) {
                        if ($obj->isNew()) {
                            $collDadosFiscalItems[] = $obj;
                        }
                    }
                }

                $this->collDadosFiscalItems = $collDadosFiscalItems;
                $this->collDadosFiscalItemsPartial = false;
            }
        }

        return $this->collDadosFiscalItems;
    }

    /**
     * Sets a collection of ChildDadosFiscalItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $dadosFiscalItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDadosFiscal The current object (for fluent API support)
     */
    public function setDadosFiscalItems(Collection $dadosFiscalItems, ConnectionInterface $con = null)
    {
        /** @var ChildDadosFiscalItem[] $dadosFiscalItemsToDelete */
        $dadosFiscalItemsToDelete = $this->getDadosFiscalItems(new Criteria(), $con)->diff($dadosFiscalItems);


        $this->dadosFiscalItemsScheduledForDeletion = $dadosFiscalItemsToDelete;

        foreach ($dadosFiscalItemsToDelete as $dadosFiscalItemRemoved) {
            $dadosFiscalItemRemoved->setDadosFiscal(null);
        }

        $this->collDadosFiscalItems = null;
        foreach ($dadosFiscalItems as $dadosFiscalItem) {
            $this->addDadosFiscalItem($dadosFiscalItem);
        }

        $this->collDadosFiscalItems = $dadosFiscalItems;
        $this->collDadosFiscalItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DadosFiscalItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related DadosFiscalItem objects.
     * @throws PropelException
     */
    public function countDadosFiscalItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalItemsPartial && !$this->isNew();
        if (null === $this->collDadosFiscalItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscalItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDadosFiscalItems());
            }

            $query = ChildDadosFiscalItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDadosFiscal($this)
                ->count($con);
        }

        return count($this->collDadosFiscalItems);
    }

    /**
     * Method called to associate a ChildDadosFiscalItem object to this object
     * through the ChildDadosFiscalItem foreign key attribute.
     *
     * @param  ChildDadosFiscalItem $l ChildDadosFiscalItem
     * @return $this|\ImaTelecomBundle\Model\DadosFiscal The current object (for fluent API support)
     */
    public function addDadosFiscalItem(ChildDadosFiscalItem $l)
    {
        if ($this->collDadosFiscalItems === null) {
            $this->initDadosFiscalItems();
            $this->collDadosFiscalItemsPartial = true;
        }

        if (!$this->collDadosFiscalItems->contains($l)) {
            $this->doAddDadosFiscalItem($l);

            if ($this->dadosFiscalItemsScheduledForDeletion and $this->dadosFiscalItemsScheduledForDeletion->contains($l)) {
                $this->dadosFiscalItemsScheduledForDeletion->remove($this->dadosFiscalItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDadosFiscalItem $dadosFiscalItem The ChildDadosFiscalItem object to add.
     */
    protected function doAddDadosFiscalItem(ChildDadosFiscalItem $dadosFiscalItem)
    {
        $this->collDadosFiscalItems[]= $dadosFiscalItem;
        $dadosFiscalItem->setDadosFiscal($this);
    }

    /**
     * @param  ChildDadosFiscalItem $dadosFiscalItem The ChildDadosFiscalItem object to remove.
     * @return $this|ChildDadosFiscal The current object (for fluent API support)
     */
    public function removeDadosFiscalItem(ChildDadosFiscalItem $dadosFiscalItem)
    {
        if ($this->getDadosFiscalItems()->contains($dadosFiscalItem)) {
            $pos = $this->collDadosFiscalItems->search($dadosFiscalItem);
            $this->collDadosFiscalItems->remove($pos);
            if (null === $this->dadosFiscalItemsScheduledForDeletion) {
                $this->dadosFiscalItemsScheduledForDeletion = clone $this->collDadosFiscalItems;
                $this->dadosFiscalItemsScheduledForDeletion->clear();
            }
            $this->dadosFiscalItemsScheduledForDeletion[]= clone $dadosFiscalItem;
            $dadosFiscalItem->setDadosFiscal(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this DadosFiscal is new, it will return
     * an empty collection; or if this DadosFiscal has previously
     * been saved, it will retrieve related DadosFiscalItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in DadosFiscal.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscalItem[] List of ChildDadosFiscalItem objects
     */
    public function getDadosFiscalItemsJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalItemQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getDadosFiscalItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this DadosFiscal is new, it will return
     * an empty collection; or if this DadosFiscal has previously
     * been saved, it will retrieve related DadosFiscalItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in DadosFiscal.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscalItem[] List of ChildDadosFiscalItem objects
     */
    public function getDadosFiscalItemsJoinBoletoItem(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalItemQuery::create(null, $criteria);
        $query->joinWith('BoletoItem', $joinBehavior);

        return $this->getDadosFiscalItems($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBoleto) {
            $this->aBoleto->removeDadosFiscal($this);
        }
        if (null !== $this->aCompetencia) {
            $this->aCompetencia->removeDadosFiscal($this);
        }
        if (null !== $this->aPessoa) {
            $this->aPessoa->removeDadosFiscal($this);
        }
        $this->id = null;
        $this->boleto_item_id = null;
        $this->documento = null;
        $this->item = null;
        $this->mestre = null;
        $this->numero_nota_fiscal = null;
        $this->competencia_id = null;
        $this->data_emissao = null;
        $this->mestre_chave_digital_meio = null;
        $this->cfop = null;
        $this->base_calculo = null;
        $this->valor_icms = null;
        $this->n_tributo = null;
        $this->valor_nota = null;
        $this->situacao = null;
        $this->servico_cliente_import_id = null;
        $this->cod_nota_fiscal_lote_previo = null;
        $this->pessoa_import_id = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->boleto_id = null;
        $this->posicao_primeiro_item = null;
        $this->posicao_ultimo_item = null;
        $this->ultima_nota_gerada = null;
        $this->tipo_nota = null;
        $this->protocolo = null;
        $this->data_emitido = null;
        $this->numero_rps = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collDadosFiscalItems) {
                foreach ($this->collDadosFiscalItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collDadosFiscalItems = null;
        $this->aBoleto = null;
        $this->aCompetencia = null;
        $this->aPessoa = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DadosFiscalTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
