<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BoletoBaixaHistorico as ChildBoletoBaixaHistorico;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery as ChildBoletoBaixaHistoricoQuery;
use ImaTelecomBundle\Model\Map\BoletoBaixaHistoricoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'boleto_baixa_historico' table.
 *
 *
 *
 * @method     ChildBoletoBaixaHistoricoQuery orderByIdboletoBaixaHistorico($order = Criteria::ASC) Order by the idboleto_baixa_historico column
 * @method     ChildBoletoBaixaHistoricoQuery orderByBoletoId($order = Criteria::ASC) Order by the boleto_id column
 * @method     ChildBoletoBaixaHistoricoQuery orderByLancamentoId($order = Criteria::ASC) Order by the lancamento_id column
 * @method     ChildBoletoBaixaHistoricoQuery orderByPessoaId($order = Criteria::ASC) Order by the pessoa_id column
 * @method     ChildBoletoBaixaHistoricoQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildBoletoBaixaHistoricoQuery orderByBaixaId($order = Criteria::ASC) Order by the baixa_id column
 * @method     ChildBoletoBaixaHistoricoQuery orderByBaixaEstornoId($order = Criteria::ASC) Order by the baixa_estorno_id column
 * @method     ChildBoletoBaixaHistoricoQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildBoletoBaixaHistoricoQuery orderByMensagemRetorno($order = Criteria::ASC) Order by the mensagem_retorno column
 * @method     ChildBoletoBaixaHistoricoQuery orderByDataPagamento($order = Criteria::ASC) Order by the data_pagamento column
 * @method     ChildBoletoBaixaHistoricoQuery orderByDataRecusa($order = Criteria::ASC) Order by the data_recusa column
 * @method     ChildBoletoBaixaHistoricoQuery orderByValorTarifaCusto($order = Criteria::ASC) Order by the valor_tarifa_custo column
 * @method     ChildBoletoBaixaHistoricoQuery orderByValorOriginal($order = Criteria::ASC) Order by the valor_original column
 * @method     ChildBoletoBaixaHistoricoQuery orderByValorMulta($order = Criteria::ASC) Order by the valor_multa column
 * @method     ChildBoletoBaixaHistoricoQuery orderByValorJuros($order = Criteria::ASC) Order by the valor_juros column
 * @method     ChildBoletoBaixaHistoricoQuery orderByValorDesconto($order = Criteria::ASC) Order by the valor_desconto column
 * @method     ChildBoletoBaixaHistoricoQuery orderByValorTotal($order = Criteria::ASC) Order by the valor_total column
 * @method     ChildBoletoBaixaHistoricoQuery orderByDataVencimento($order = Criteria::ASC) Order by the data_vencimento column
 * @method     ChildBoletoBaixaHistoricoQuery orderByNumeroDocumento($order = Criteria::ASC) Order by the numero_documento column
 * @method     ChildBoletoBaixaHistoricoQuery orderByCodigoMovimento($order = Criteria::ASC) Order by the codigo_movimento column
 * @method     ChildBoletoBaixaHistoricoQuery orderByDescricaoMovimento($order = Criteria::ASC) Order by the descricao_movimento column
 * @method     ChildBoletoBaixaHistoricoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildBoletoBaixaHistoricoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildBoletoBaixaHistoricoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildBoletoBaixaHistoricoQuery groupByIdboletoBaixaHistorico() Group by the idboleto_baixa_historico column
 * @method     ChildBoletoBaixaHistoricoQuery groupByBoletoId() Group by the boleto_id column
 * @method     ChildBoletoBaixaHistoricoQuery groupByLancamentoId() Group by the lancamento_id column
 * @method     ChildBoletoBaixaHistoricoQuery groupByPessoaId() Group by the pessoa_id column
 * @method     ChildBoletoBaixaHistoricoQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildBoletoBaixaHistoricoQuery groupByBaixaId() Group by the baixa_id column
 * @method     ChildBoletoBaixaHistoricoQuery groupByBaixaEstornoId() Group by the baixa_estorno_id column
 * @method     ChildBoletoBaixaHistoricoQuery groupByStatus() Group by the status column
 * @method     ChildBoletoBaixaHistoricoQuery groupByMensagemRetorno() Group by the mensagem_retorno column
 * @method     ChildBoletoBaixaHistoricoQuery groupByDataPagamento() Group by the data_pagamento column
 * @method     ChildBoletoBaixaHistoricoQuery groupByDataRecusa() Group by the data_recusa column
 * @method     ChildBoletoBaixaHistoricoQuery groupByValorTarifaCusto() Group by the valor_tarifa_custo column
 * @method     ChildBoletoBaixaHistoricoQuery groupByValorOriginal() Group by the valor_original column
 * @method     ChildBoletoBaixaHistoricoQuery groupByValorMulta() Group by the valor_multa column
 * @method     ChildBoletoBaixaHistoricoQuery groupByValorJuros() Group by the valor_juros column
 * @method     ChildBoletoBaixaHistoricoQuery groupByValorDesconto() Group by the valor_desconto column
 * @method     ChildBoletoBaixaHistoricoQuery groupByValorTotal() Group by the valor_total column
 * @method     ChildBoletoBaixaHistoricoQuery groupByDataVencimento() Group by the data_vencimento column
 * @method     ChildBoletoBaixaHistoricoQuery groupByNumeroDocumento() Group by the numero_documento column
 * @method     ChildBoletoBaixaHistoricoQuery groupByCodigoMovimento() Group by the codigo_movimento column
 * @method     ChildBoletoBaixaHistoricoQuery groupByDescricaoMovimento() Group by the descricao_movimento column
 * @method     ChildBoletoBaixaHistoricoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildBoletoBaixaHistoricoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildBoletoBaixaHistoricoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBoletoBaixaHistoricoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBoletoBaixaHistoricoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinBaixaEstorno($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinBaixaEstorno($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinBaixaEstorno($relationAlias = null) Adds a INNER JOIN clause to the query using the BaixaEstorno relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery joinWithBaixaEstorno($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinWithBaixaEstorno() Adds a LEFT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinWithBaixaEstorno() Adds a RIGHT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinWithBaixaEstorno() Adds a INNER JOIN clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinBaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Baixa relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinBaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Baixa relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinBaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Baixa relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery joinWithBaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Baixa relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinWithBaixa() Adds a LEFT JOIN clause and with to the query using the Baixa relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinWithBaixa() Adds a RIGHT JOIN clause and with to the query using the Baixa relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinWithBaixa() Adds a INNER JOIN clause and with to the query using the Baixa relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinCompetencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Competencia relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinCompetencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Competencia relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinCompetencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Competencia relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery joinWithCompetencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Competencia relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinWithCompetencia() Adds a LEFT JOIN clause and with to the query using the Competencia relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinWithCompetencia() Adds a RIGHT JOIN clause and with to the query using the Competencia relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinWithCompetencia() Adds a INNER JOIN clause and with to the query using the Competencia relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinLancamentos($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lancamentos relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinLancamentos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lancamentos relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinLancamentos($relationAlias = null) Adds a INNER JOIN clause to the query using the Lancamentos relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery joinWithLancamentos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Lancamentos relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinWithLancamentos() Adds a LEFT JOIN clause and with to the query using the Lancamentos relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinWithLancamentos() Adds a RIGHT JOIN clause and with to the query using the Lancamentos relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinWithLancamentos() Adds a INNER JOIN clause and with to the query using the Lancamentos relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinPessoa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pessoa relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinPessoa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pessoa relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinPessoa($relationAlias = null) Adds a INNER JOIN clause to the query using the Pessoa relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery joinWithPessoa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pessoa relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinWithPessoa() Adds a LEFT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinWithPessoa() Adds a RIGHT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinWithPessoa() Adds a INNER JOIN clause and with to the query using the Pessoa relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBoletoBaixaHistoricoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBoletoBaixaHistoricoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBoletoBaixaHistoricoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \ImaTelecomBundle\Model\BaixaEstornoQuery|\ImaTelecomBundle\Model\BaixaQuery|\ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\CompetenciaQuery|\ImaTelecomBundle\Model\LancamentosQuery|\ImaTelecomBundle\Model\PessoaQuery|\ImaTelecomBundle\Model\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBoletoBaixaHistorico findOne(ConnectionInterface $con = null) Return the first ChildBoletoBaixaHistorico matching the query
 * @method     ChildBoletoBaixaHistorico findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBoletoBaixaHistorico matching the query, or a new ChildBoletoBaixaHistorico object populated from the query conditions when no match is found
 *
 * @method     ChildBoletoBaixaHistorico findOneByIdboletoBaixaHistorico(int $idboleto_baixa_historico) Return the first ChildBoletoBaixaHistorico filtered by the idboleto_baixa_historico column
 * @method     ChildBoletoBaixaHistorico findOneByBoletoId(int $boleto_id) Return the first ChildBoletoBaixaHistorico filtered by the boleto_id column
 * @method     ChildBoletoBaixaHistorico findOneByLancamentoId(int $lancamento_id) Return the first ChildBoletoBaixaHistorico filtered by the lancamento_id column
 * @method     ChildBoletoBaixaHistorico findOneByPessoaId(int $pessoa_id) Return the first ChildBoletoBaixaHistorico filtered by the pessoa_id column
 * @method     ChildBoletoBaixaHistorico findOneByCompetenciaId(int $competencia_id) Return the first ChildBoletoBaixaHistorico filtered by the competencia_id column
 * @method     ChildBoletoBaixaHistorico findOneByBaixaId(int $baixa_id) Return the first ChildBoletoBaixaHistorico filtered by the baixa_id column
 * @method     ChildBoletoBaixaHistorico findOneByBaixaEstornoId(int $baixa_estorno_id) Return the first ChildBoletoBaixaHistorico filtered by the baixa_estorno_id column
 * @method     ChildBoletoBaixaHistorico findOneByStatus(string $status) Return the first ChildBoletoBaixaHistorico filtered by the status column
 * @method     ChildBoletoBaixaHistorico findOneByMensagemRetorno(string $mensagem_retorno) Return the first ChildBoletoBaixaHistorico filtered by the mensagem_retorno column
 * @method     ChildBoletoBaixaHistorico findOneByDataPagamento(string $data_pagamento) Return the first ChildBoletoBaixaHistorico filtered by the data_pagamento column
 * @method     ChildBoletoBaixaHistorico findOneByDataRecusa(string $data_recusa) Return the first ChildBoletoBaixaHistorico filtered by the data_recusa column
 * @method     ChildBoletoBaixaHistorico findOneByValorTarifaCusto(string $valor_tarifa_custo) Return the first ChildBoletoBaixaHistorico filtered by the valor_tarifa_custo column
 * @method     ChildBoletoBaixaHistorico findOneByValorOriginal(string $valor_original) Return the first ChildBoletoBaixaHistorico filtered by the valor_original column
 * @method     ChildBoletoBaixaHistorico findOneByValorMulta(string $valor_multa) Return the first ChildBoletoBaixaHistorico filtered by the valor_multa column
 * @method     ChildBoletoBaixaHistorico findOneByValorJuros(string $valor_juros) Return the first ChildBoletoBaixaHistorico filtered by the valor_juros column
 * @method     ChildBoletoBaixaHistorico findOneByValorDesconto(string $valor_desconto) Return the first ChildBoletoBaixaHistorico filtered by the valor_desconto column
 * @method     ChildBoletoBaixaHistorico findOneByValorTotal(string $valor_total) Return the first ChildBoletoBaixaHistorico filtered by the valor_total column
 * @method     ChildBoletoBaixaHistorico findOneByDataVencimento(string $data_vencimento) Return the first ChildBoletoBaixaHistorico filtered by the data_vencimento column
 * @method     ChildBoletoBaixaHistorico findOneByNumeroDocumento(string $numero_documento) Return the first ChildBoletoBaixaHistorico filtered by the numero_documento column
 * @method     ChildBoletoBaixaHistorico findOneByCodigoMovimento(string $codigo_movimento) Return the first ChildBoletoBaixaHistorico filtered by the codigo_movimento column
 * @method     ChildBoletoBaixaHistorico findOneByDescricaoMovimento(string $descricao_movimento) Return the first ChildBoletoBaixaHistorico filtered by the descricao_movimento column
 * @method     ChildBoletoBaixaHistorico findOneByDataCadastro(string $data_cadastro) Return the first ChildBoletoBaixaHistorico filtered by the data_cadastro column
 * @method     ChildBoletoBaixaHistorico findOneByDataAlterado(string $data_alterado) Return the first ChildBoletoBaixaHistorico filtered by the data_alterado column
 * @method     ChildBoletoBaixaHistorico findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBoletoBaixaHistorico filtered by the usuario_alterado column *

 * @method     ChildBoletoBaixaHistorico requirePk($key, ConnectionInterface $con = null) Return the ChildBoletoBaixaHistorico by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOne(ConnectionInterface $con = null) Return the first ChildBoletoBaixaHistorico matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBoletoBaixaHistorico requireOneByIdboletoBaixaHistorico(int $idboleto_baixa_historico) Return the first ChildBoletoBaixaHistorico filtered by the idboleto_baixa_historico column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByBoletoId(int $boleto_id) Return the first ChildBoletoBaixaHistorico filtered by the boleto_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByLancamentoId(int $lancamento_id) Return the first ChildBoletoBaixaHistorico filtered by the lancamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByPessoaId(int $pessoa_id) Return the first ChildBoletoBaixaHistorico filtered by the pessoa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByCompetenciaId(int $competencia_id) Return the first ChildBoletoBaixaHistorico filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByBaixaId(int $baixa_id) Return the first ChildBoletoBaixaHistorico filtered by the baixa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByBaixaEstornoId(int $baixa_estorno_id) Return the first ChildBoletoBaixaHistorico filtered by the baixa_estorno_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByStatus(string $status) Return the first ChildBoletoBaixaHistorico filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByMensagemRetorno(string $mensagem_retorno) Return the first ChildBoletoBaixaHistorico filtered by the mensagem_retorno column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByDataPagamento(string $data_pagamento) Return the first ChildBoletoBaixaHistorico filtered by the data_pagamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByDataRecusa(string $data_recusa) Return the first ChildBoletoBaixaHistorico filtered by the data_recusa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByValorTarifaCusto(string $valor_tarifa_custo) Return the first ChildBoletoBaixaHistorico filtered by the valor_tarifa_custo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByValorOriginal(string $valor_original) Return the first ChildBoletoBaixaHistorico filtered by the valor_original column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByValorMulta(string $valor_multa) Return the first ChildBoletoBaixaHistorico filtered by the valor_multa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByValorJuros(string $valor_juros) Return the first ChildBoletoBaixaHistorico filtered by the valor_juros column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByValorDesconto(string $valor_desconto) Return the first ChildBoletoBaixaHistorico filtered by the valor_desconto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByValorTotal(string $valor_total) Return the first ChildBoletoBaixaHistorico filtered by the valor_total column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByDataVencimento(string $data_vencimento) Return the first ChildBoletoBaixaHistorico filtered by the data_vencimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByNumeroDocumento(string $numero_documento) Return the first ChildBoletoBaixaHistorico filtered by the numero_documento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByCodigoMovimento(string $codigo_movimento) Return the first ChildBoletoBaixaHistorico filtered by the codigo_movimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByDescricaoMovimento(string $descricao_movimento) Return the first ChildBoletoBaixaHistorico filtered by the descricao_movimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByDataCadastro(string $data_cadastro) Return the first ChildBoletoBaixaHistorico filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByDataAlterado(string $data_alterado) Return the first ChildBoletoBaixaHistorico filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoBaixaHistorico requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBoletoBaixaHistorico filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBoletoBaixaHistorico objects based on current ModelCriteria
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByIdboletoBaixaHistorico(int $idboleto_baixa_historico) Return ChildBoletoBaixaHistorico objects filtered by the idboleto_baixa_historico column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByBoletoId(int $boleto_id) Return ChildBoletoBaixaHistorico objects filtered by the boleto_id column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByLancamentoId(int $lancamento_id) Return ChildBoletoBaixaHistorico objects filtered by the lancamento_id column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByPessoaId(int $pessoa_id) Return ChildBoletoBaixaHistorico objects filtered by the pessoa_id column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildBoletoBaixaHistorico objects filtered by the competencia_id column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByBaixaId(int $baixa_id) Return ChildBoletoBaixaHistorico objects filtered by the baixa_id column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByBaixaEstornoId(int $baixa_estorno_id) Return ChildBoletoBaixaHistorico objects filtered by the baixa_estorno_id column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByStatus(string $status) Return ChildBoletoBaixaHistorico objects filtered by the status column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByMensagemRetorno(string $mensagem_retorno) Return ChildBoletoBaixaHistorico objects filtered by the mensagem_retorno column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByDataPagamento(string $data_pagamento) Return ChildBoletoBaixaHistorico objects filtered by the data_pagamento column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByDataRecusa(string $data_recusa) Return ChildBoletoBaixaHistorico objects filtered by the data_recusa column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByValorTarifaCusto(string $valor_tarifa_custo) Return ChildBoletoBaixaHistorico objects filtered by the valor_tarifa_custo column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByValorOriginal(string $valor_original) Return ChildBoletoBaixaHistorico objects filtered by the valor_original column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByValorMulta(string $valor_multa) Return ChildBoletoBaixaHistorico objects filtered by the valor_multa column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByValorJuros(string $valor_juros) Return ChildBoletoBaixaHistorico objects filtered by the valor_juros column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByValorDesconto(string $valor_desconto) Return ChildBoletoBaixaHistorico objects filtered by the valor_desconto column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByValorTotal(string $valor_total) Return ChildBoletoBaixaHistorico objects filtered by the valor_total column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByDataVencimento(string $data_vencimento) Return ChildBoletoBaixaHistorico objects filtered by the data_vencimento column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByNumeroDocumento(string $numero_documento) Return ChildBoletoBaixaHistorico objects filtered by the numero_documento column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByCodigoMovimento(string $codigo_movimento) Return ChildBoletoBaixaHistorico objects filtered by the codigo_movimento column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByDescricaoMovimento(string $descricao_movimento) Return ChildBoletoBaixaHistorico objects filtered by the descricao_movimento column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildBoletoBaixaHistorico objects filtered by the data_cadastro column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildBoletoBaixaHistorico objects filtered by the data_alterado column
 * @method     ChildBoletoBaixaHistorico[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildBoletoBaixaHistorico objects filtered by the usuario_alterado column
 * @method     ChildBoletoBaixaHistorico[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BoletoBaixaHistoricoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\BoletoBaixaHistoricoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBoletoBaixaHistoricoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBoletoBaixaHistoricoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBoletoBaixaHistoricoQuery) {
            return $criteria;
        }
        $query = new ChildBoletoBaixaHistoricoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBoletoBaixaHistorico|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BoletoBaixaHistoricoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoBaixaHistorico A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idboleto_baixa_historico, boleto_id, lancamento_id, pessoa_id, competencia_id, baixa_id, baixa_estorno_id, status, mensagem_retorno, data_pagamento, data_recusa, valor_tarifa_custo, valor_original, valor_multa, valor_juros, valor_desconto, valor_total, data_vencimento, numero_documento, codigo_movimento, descricao_movimento, data_cadastro, data_alterado, usuario_alterado FROM boleto_baixa_historico WHERE idboleto_baixa_historico = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBoletoBaixaHistorico $obj */
            $obj = new ChildBoletoBaixaHistorico();
            $obj->hydrate($row);
            BoletoBaixaHistoricoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBoletoBaixaHistorico|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idboleto_baixa_historico column
     *
     * Example usage:
     * <code>
     * $query->filterByIdboletoBaixaHistorico(1234); // WHERE idboleto_baixa_historico = 1234
     * $query->filterByIdboletoBaixaHistorico(array(12, 34)); // WHERE idboleto_baixa_historico IN (12, 34)
     * $query->filterByIdboletoBaixaHistorico(array('min' => 12)); // WHERE idboleto_baixa_historico > 12
     * </code>
     *
     * @param     mixed $idboletoBaixaHistorico The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByIdboletoBaixaHistorico($idboletoBaixaHistorico = null, $comparison = null)
    {
        if (is_array($idboletoBaixaHistorico)) {
            $useMinMax = false;
            if (isset($idboletoBaixaHistorico['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, $idboletoBaixaHistorico['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idboletoBaixaHistorico['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, $idboletoBaixaHistorico['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, $idboletoBaixaHistorico, $comparison);
    }

    /**
     * Filter the query on the boleto_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBoletoId(1234); // WHERE boleto_id = 1234
     * $query->filterByBoletoId(array(12, 34)); // WHERE boleto_id IN (12, 34)
     * $query->filterByBoletoId(array('min' => 12)); // WHERE boleto_id > 12
     * </code>
     *
     * @see       filterByBoleto()
     *
     * @param     mixed $boletoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByBoletoId($boletoId = null, $comparison = null)
    {
        if (is_array($boletoId)) {
            $useMinMax = false;
            if (isset($boletoId['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BOLETO_ID, $boletoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($boletoId['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BOLETO_ID, $boletoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BOLETO_ID, $boletoId, $comparison);
    }

    /**
     * Filter the query on the lancamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLancamentoId(1234); // WHERE lancamento_id = 1234
     * $query->filterByLancamentoId(array(12, 34)); // WHERE lancamento_id IN (12, 34)
     * $query->filterByLancamentoId(array('min' => 12)); // WHERE lancamento_id > 12
     * </code>
     *
     * @see       filterByLancamentos()
     *
     * @param     mixed $lancamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByLancamentoId($lancamentoId = null, $comparison = null)
    {
        if (is_array($lancamentoId)) {
            $useMinMax = false;
            if (isset($lancamentoId['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID, $lancamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lancamentoId['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID, $lancamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID, $lancamentoId, $comparison);
    }

    /**
     * Filter the query on the pessoa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPessoaId(1234); // WHERE pessoa_id = 1234
     * $query->filterByPessoaId(array(12, 34)); // WHERE pessoa_id IN (12, 34)
     * $query->filterByPessoaId(array('min' => 12)); // WHERE pessoa_id > 12
     * </code>
     *
     * @see       filterByPessoa()
     *
     * @param     mixed $pessoaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByPessoaId($pessoaId = null, $comparison = null)
    {
        if (is_array($pessoaId)) {
            $useMinMax = false;
            if (isset($pessoaId['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_PESSOA_ID, $pessoaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pessoaId['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_PESSOA_ID, $pessoaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_PESSOA_ID, $pessoaId, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @see       filterByCompetencia()
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the baixa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBaixaId(1234); // WHERE baixa_id = 1234
     * $query->filterByBaixaId(array(12, 34)); // WHERE baixa_id IN (12, 34)
     * $query->filterByBaixaId(array('min' => 12)); // WHERE baixa_id > 12
     * </code>
     *
     * @see       filterByBaixa()
     *
     * @param     mixed $baixaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByBaixaId($baixaId = null, $comparison = null)
    {
        if (is_array($baixaId)) {
            $useMinMax = false;
            if (isset($baixaId['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ID, $baixaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baixaId['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ID, $baixaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ID, $baixaId, $comparison);
    }

    /**
     * Filter the query on the baixa_estorno_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBaixaEstornoId(1234); // WHERE baixa_estorno_id = 1234
     * $query->filterByBaixaEstornoId(array(12, 34)); // WHERE baixa_estorno_id IN (12, 34)
     * $query->filterByBaixaEstornoId(array('min' => 12)); // WHERE baixa_estorno_id > 12
     * </code>
     *
     * @see       filterByBaixaEstorno()
     *
     * @param     mixed $baixaEstornoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByBaixaEstornoId($baixaEstornoId = null, $comparison = null)
    {
        if (is_array($baixaEstornoId)) {
            $useMinMax = false;
            if (isset($baixaEstornoId['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstornoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baixaEstornoId['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstornoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstornoId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the mensagem_retorno column
     *
     * Example usage:
     * <code>
     * $query->filterByMensagemRetorno('fooValue');   // WHERE mensagem_retorno = 'fooValue'
     * $query->filterByMensagemRetorno('%fooValue%', Criteria::LIKE); // WHERE mensagem_retorno LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mensagemRetorno The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByMensagemRetorno($mensagemRetorno = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mensagemRetorno)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_MENSAGEM_RETORNO, $mensagemRetorno, $comparison);
    }

    /**
     * Filter the query on the data_pagamento column
     *
     * Example usage:
     * <code>
     * $query->filterByDataPagamento('fooValue');   // WHERE data_pagamento = 'fooValue'
     * $query->filterByDataPagamento('%fooValue%', Criteria::LIKE); // WHERE data_pagamento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dataPagamento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByDataPagamento($dataPagamento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dataPagamento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_PAGAMENTO, $dataPagamento, $comparison);
    }

    /**
     * Filter the query on the data_recusa column
     *
     * Example usage:
     * <code>
     * $query->filterByDataRecusa('fooValue');   // WHERE data_recusa = 'fooValue'
     * $query->filterByDataRecusa('%fooValue%', Criteria::LIKE); // WHERE data_recusa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dataRecusa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByDataRecusa($dataRecusa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dataRecusa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_RECUSA, $dataRecusa, $comparison);
    }

    /**
     * Filter the query on the valor_tarifa_custo column
     *
     * Example usage:
     * <code>
     * $query->filterByValorTarifaCusto(1234); // WHERE valor_tarifa_custo = 1234
     * $query->filterByValorTarifaCusto(array(12, 34)); // WHERE valor_tarifa_custo IN (12, 34)
     * $query->filterByValorTarifaCusto(array('min' => 12)); // WHERE valor_tarifa_custo > 12
     * </code>
     *
     * @param     mixed $valorTarifaCusto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByValorTarifaCusto($valorTarifaCusto = null, $comparison = null)
    {
        if (is_array($valorTarifaCusto)) {
            $useMinMax = false;
            if (isset($valorTarifaCusto['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO, $valorTarifaCusto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorTarifaCusto['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO, $valorTarifaCusto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO, $valorTarifaCusto, $comparison);
    }

    /**
     * Filter the query on the valor_original column
     *
     * Example usage:
     * <code>
     * $query->filterByValorOriginal(1234); // WHERE valor_original = 1234
     * $query->filterByValorOriginal(array(12, 34)); // WHERE valor_original IN (12, 34)
     * $query->filterByValorOriginal(array('min' => 12)); // WHERE valor_original > 12
     * </code>
     *
     * @param     mixed $valorOriginal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByValorOriginal($valorOriginal = null, $comparison = null)
    {
        if (is_array($valorOriginal)) {
            $useMinMax = false;
            if (isset($valorOriginal['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL, $valorOriginal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorOriginal['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL, $valorOriginal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL, $valorOriginal, $comparison);
    }

    /**
     * Filter the query on the valor_multa column
     *
     * Example usage:
     * <code>
     * $query->filterByValorMulta(1234); // WHERE valor_multa = 1234
     * $query->filterByValorMulta(array(12, 34)); // WHERE valor_multa IN (12, 34)
     * $query->filterByValorMulta(array('min' => 12)); // WHERE valor_multa > 12
     * </code>
     *
     * @param     mixed $valorMulta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByValorMulta($valorMulta = null, $comparison = null)
    {
        if (is_array($valorMulta)) {
            $useMinMax = false;
            if (isset($valorMulta['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA, $valorMulta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorMulta['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA, $valorMulta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA, $valorMulta, $comparison);
    }

    /**
     * Filter the query on the valor_juros column
     *
     * Example usage:
     * <code>
     * $query->filterByValorJuros(1234); // WHERE valor_juros = 1234
     * $query->filterByValorJuros(array(12, 34)); // WHERE valor_juros IN (12, 34)
     * $query->filterByValorJuros(array('min' => 12)); // WHERE valor_juros > 12
     * </code>
     *
     * @param     mixed $valorJuros The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByValorJuros($valorJuros = null, $comparison = null)
    {
        if (is_array($valorJuros)) {
            $useMinMax = false;
            if (isset($valorJuros['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS, $valorJuros['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorJuros['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS, $valorJuros['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS, $valorJuros, $comparison);
    }

    /**
     * Filter the query on the valor_desconto column
     *
     * Example usage:
     * <code>
     * $query->filterByValorDesconto(1234); // WHERE valor_desconto = 1234
     * $query->filterByValorDesconto(array(12, 34)); // WHERE valor_desconto IN (12, 34)
     * $query->filterByValorDesconto(array('min' => 12)); // WHERE valor_desconto > 12
     * </code>
     *
     * @param     mixed $valorDesconto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByValorDesconto($valorDesconto = null, $comparison = null)
    {
        if (is_array($valorDesconto)) {
            $useMinMax = false;
            if (isset($valorDesconto['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO, $valorDesconto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorDesconto['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO, $valorDesconto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO, $valorDesconto, $comparison);
    }

    /**
     * Filter the query on the valor_total column
     *
     * Example usage:
     * <code>
     * $query->filterByValorTotal(1234); // WHERE valor_total = 1234
     * $query->filterByValorTotal(array(12, 34)); // WHERE valor_total IN (12, 34)
     * $query->filterByValorTotal(array('min' => 12)); // WHERE valor_total > 12
     * </code>
     *
     * @param     mixed $valorTotal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByValorTotal($valorTotal = null, $comparison = null)
    {
        if (is_array($valorTotal)) {
            $useMinMax = false;
            if (isset($valorTotal['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL, $valorTotal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorTotal['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL, $valorTotal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL, $valorTotal, $comparison);
    }

    /**
     * Filter the query on the data_vencimento column
     *
     * Example usage:
     * <code>
     * $query->filterByDataVencimento('2011-03-14'); // WHERE data_vencimento = '2011-03-14'
     * $query->filterByDataVencimento('now'); // WHERE data_vencimento = '2011-03-14'
     * $query->filterByDataVencimento(array('max' => 'yesterday')); // WHERE data_vencimento > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataVencimento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByDataVencimento($dataVencimento = null, $comparison = null)
    {
        if (is_array($dataVencimento)) {
            $useMinMax = false;
            if (isset($dataVencimento['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO, $dataVencimento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataVencimento['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO, $dataVencimento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO, $dataVencimento, $comparison);
    }

    /**
     * Filter the query on the numero_documento column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroDocumento('fooValue');   // WHERE numero_documento = 'fooValue'
     * $query->filterByNumeroDocumento('%fooValue%', Criteria::LIKE); // WHERE numero_documento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numeroDocumento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByNumeroDocumento($numeroDocumento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numeroDocumento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_NUMERO_DOCUMENTO, $numeroDocumento, $comparison);
    }

    /**
     * Filter the query on the codigo_movimento column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigoMovimento('fooValue');   // WHERE codigo_movimento = 'fooValue'
     * $query->filterByCodigoMovimento('%fooValue%', Criteria::LIKE); // WHERE codigo_movimento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigoMovimento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByCodigoMovimento($codigoMovimento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigoMovimento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_CODIGO_MOVIMENTO, $codigoMovimento, $comparison);
    }

    /**
     * Filter the query on the descricao_movimento column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricaoMovimento('fooValue');   // WHERE descricao_movimento = 'fooValue'
     * $query->filterByDescricaoMovimento('%fooValue%', Criteria::LIKE); // WHERE descricao_movimento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricaoMovimento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByDescricaoMovimento($descricaoMovimento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricaoMovimento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DESCRICAO_MOVIMENTO, $descricaoMovimento, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BaixaEstorno object
     *
     * @param \ImaTelecomBundle\Model\BaixaEstorno|ObjectCollection $baixaEstorno The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByBaixaEstorno($baixaEstorno, $comparison = null)
    {
        if ($baixaEstorno instanceof \ImaTelecomBundle\Model\BaixaEstorno) {
            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstorno->getIdbaixaEstorno(), $comparison);
        } elseif ($baixaEstorno instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstorno->toKeyValue('PrimaryKey', 'IdbaixaEstorno'), $comparison);
        } else {
            throw new PropelException('filterByBaixaEstorno() only accepts arguments of type \ImaTelecomBundle\Model\BaixaEstorno or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaixaEstorno relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function joinBaixaEstorno($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaixaEstorno');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaixaEstorno');
        }

        return $this;
    }

    /**
     * Use the BaixaEstorno relation BaixaEstorno object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaEstornoQuery A secondary query class using the current class as primary query
     */
    public function useBaixaEstornoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBaixaEstorno($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaixaEstorno', '\ImaTelecomBundle\Model\BaixaEstornoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Baixa object
     *
     * @param \ImaTelecomBundle\Model\Baixa|ObjectCollection $baixa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByBaixa($baixa, $comparison = null)
    {
        if ($baixa instanceof \ImaTelecomBundle\Model\Baixa) {
            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ID, $baixa->getIdbaixa(), $comparison);
        } elseif ($baixa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BAIXA_ID, $baixa->toKeyValue('PrimaryKey', 'Idbaixa'), $comparison);
        } else {
            throw new PropelException('filterByBaixa() only accepts arguments of type \ImaTelecomBundle\Model\Baixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Baixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function joinBaixa($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Baixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Baixa');
        }

        return $this;
    }

    /**
     * Use the Baixa relation Baixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaQuery A secondary query class using the current class as primary query
     */
    public function useBaixaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Baixa', '\ImaTelecomBundle\Model\BaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BOLETO_ID, $boleto->getIdboleto(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_BOLETO_ID, $boleto->toKeyValue('PrimaryKey', 'Idboleto'), $comparison);
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Competencia object
     *
     * @param \ImaTelecomBundle\Model\Competencia|ObjectCollection $competencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByCompetencia($competencia, $comparison = null)
    {
        if ($competencia instanceof \ImaTelecomBundle\Model\Competencia) {
            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID, $competencia->getId(), $comparison);
        } elseif ($competencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID, $competencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompetencia() only accepts arguments of type \ImaTelecomBundle\Model\Competencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Competencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function joinCompetencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Competencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Competencia');
        }

        return $this;
    }

    /**
     * Use the Competencia relation Competencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CompetenciaQuery A secondary query class using the current class as primary query
     */
    public function useCompetenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompetencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Competencia', '\ImaTelecomBundle\Model\CompetenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Lancamentos object
     *
     * @param \ImaTelecomBundle\Model\Lancamentos|ObjectCollection $lancamentos The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByLancamentos($lancamentos, $comparison = null)
    {
        if ($lancamentos instanceof \ImaTelecomBundle\Model\Lancamentos) {
            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID, $lancamentos->getIdlancamento(), $comparison);
        } elseif ($lancamentos instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID, $lancamentos->toKeyValue('PrimaryKey', 'Idlancamento'), $comparison);
        } else {
            throw new PropelException('filterByLancamentos() only accepts arguments of type \ImaTelecomBundle\Model\Lancamentos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lancamentos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function joinLancamentos($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lancamentos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lancamentos');
        }

        return $this;
    }

    /**
     * Use the Lancamentos relation Lancamentos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\LancamentosQuery A secondary query class using the current class as primary query
     */
    public function useLancamentosQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLancamentos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lancamentos', '\ImaTelecomBundle\Model\LancamentosQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Pessoa object
     *
     * @param \ImaTelecomBundle\Model\Pessoa|ObjectCollection $pessoa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByPessoa($pessoa, $comparison = null)
    {
        if ($pessoa instanceof \ImaTelecomBundle\Model\Pessoa) {
            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_PESSOA_ID, $pessoa->getId(), $comparison);
        } elseif ($pessoa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_PESSOA_ID, $pessoa->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPessoa() only accepts arguments of type \ImaTelecomBundle\Model\Pessoa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pessoa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function joinPessoa($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pessoa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pessoa');
        }

        return $this;
    }

    /**
     * Use the Pessoa relation Pessoa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\PessoaQuery A secondary query class using the current class as primary query
     */
    public function usePessoaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPessoa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pessoa', '\ImaTelecomBundle\Model\PessoaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBoletoBaixaHistorico $boletoBaixaHistorico Object to remove from the list of results
     *
     * @return $this|ChildBoletoBaixaHistoricoQuery The current query, for fluid interface
     */
    public function prune($boletoBaixaHistorico = null)
    {
        if ($boletoBaixaHistorico) {
            $this->addUsingAlias(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, $boletoBaixaHistorico->getIdboletoBaixaHistorico(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the boleto_baixa_historico table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BoletoBaixaHistoricoTableMap::clearInstancePool();
            BoletoBaixaHistoricoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BoletoBaixaHistoricoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BoletoBaixaHistoricoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BoletoBaixaHistoricoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BoletoBaixaHistoricoQuery
