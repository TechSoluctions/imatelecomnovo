<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ServicoCliente as ChildServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery as ChildServicoClienteQuery;
use ImaTelecomBundle\Model\Map\ServicoClienteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'servico_cliente' table.
 *
 *
 *
 * @method     ChildServicoClienteQuery orderByIdservicoCliente($order = Criteria::ASC) Order by the idservico_cliente column
 * @method     ChildServicoClienteQuery orderByServicoPrestadoId($order = Criteria::ASC) Order by the servico_prestado_id column
 * @method     ChildServicoClienteQuery orderByClienteId($order = Criteria::ASC) Order by the cliente_id column
 * @method     ChildServicoClienteQuery orderByCodCesta($order = Criteria::ASC) Order by the cod_cesta column
 * @method     ChildServicoClienteQuery orderByDataContratado($order = Criteria::ASC) Order by the data_contratado column
 * @method     ChildServicoClienteQuery orderByDataCancelado($order = Criteria::ASC) Order by the data_cancelado column
 * @method     ChildServicoClienteQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildServicoClienteQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildServicoClienteQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildServicoClienteQuery orderByImportId($order = Criteria::ASC) Order by the import_id column
 * @method     ChildServicoClienteQuery orderByDescricaoNotaFiscal($order = Criteria::ASC) Order by the descricao_nota_fiscal column
 * @method     ChildServicoClienteQuery orderByContratoId($order = Criteria::ASC) Order by the contrato_id column
 * @method     ChildServicoClienteQuery orderByTipoPeriodoReferencia($order = Criteria::ASC) Order by the tipo_periodo_referencia column
 * @method     ChildServicoClienteQuery orderByValorDesconto($order = Criteria::ASC) Order by the valor_desconto column
 * @method     ChildServicoClienteQuery orderByValor($order = Criteria::ASC) Order by the valor column
 *
 * @method     ChildServicoClienteQuery groupByIdservicoCliente() Group by the idservico_cliente column
 * @method     ChildServicoClienteQuery groupByServicoPrestadoId() Group by the servico_prestado_id column
 * @method     ChildServicoClienteQuery groupByClienteId() Group by the cliente_id column
 * @method     ChildServicoClienteQuery groupByCodCesta() Group by the cod_cesta column
 * @method     ChildServicoClienteQuery groupByDataContratado() Group by the data_contratado column
 * @method     ChildServicoClienteQuery groupByDataCancelado() Group by the data_cancelado column
 * @method     ChildServicoClienteQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildServicoClienteQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildServicoClienteQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildServicoClienteQuery groupByImportId() Group by the import_id column
 * @method     ChildServicoClienteQuery groupByDescricaoNotaFiscal() Group by the descricao_nota_fiscal column
 * @method     ChildServicoClienteQuery groupByContratoId() Group by the contrato_id column
 * @method     ChildServicoClienteQuery groupByTipoPeriodoReferencia() Group by the tipo_periodo_referencia column
 * @method     ChildServicoClienteQuery groupByValorDesconto() Group by the valor_desconto column
 * @method     ChildServicoClienteQuery groupByValor() Group by the valor column
 *
 * @method     ChildServicoClienteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildServicoClienteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildServicoClienteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildServicoClienteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildServicoClienteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildServicoClienteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildServicoClienteQuery leftJoinCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cliente relation
 * @method     ChildServicoClienteQuery rightJoinCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cliente relation
 * @method     ChildServicoClienteQuery innerJoinCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the Cliente relation
 *
 * @method     ChildServicoClienteQuery joinWithCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cliente relation
 *
 * @method     ChildServicoClienteQuery leftJoinWithCliente() Adds a LEFT JOIN clause and with to the query using the Cliente relation
 * @method     ChildServicoClienteQuery rightJoinWithCliente() Adds a RIGHT JOIN clause and with to the query using the Cliente relation
 * @method     ChildServicoClienteQuery innerJoinWithCliente() Adds a INNER JOIN clause and with to the query using the Cliente relation
 *
 * @method     ChildServicoClienteQuery leftJoinContrato($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contrato relation
 * @method     ChildServicoClienteQuery rightJoinContrato($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contrato relation
 * @method     ChildServicoClienteQuery innerJoinContrato($relationAlias = null) Adds a INNER JOIN clause to the query using the Contrato relation
 *
 * @method     ChildServicoClienteQuery joinWithContrato($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Contrato relation
 *
 * @method     ChildServicoClienteQuery leftJoinWithContrato() Adds a LEFT JOIN clause and with to the query using the Contrato relation
 * @method     ChildServicoClienteQuery rightJoinWithContrato() Adds a RIGHT JOIN clause and with to the query using the Contrato relation
 * @method     ChildServicoClienteQuery innerJoinWithContrato() Adds a INNER JOIN clause and with to the query using the Contrato relation
 *
 * @method     ChildServicoClienteQuery leftJoinServicoPrestado($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicoPrestado relation
 * @method     ChildServicoClienteQuery rightJoinServicoPrestado($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicoPrestado relation
 * @method     ChildServicoClienteQuery innerJoinServicoPrestado($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicoPrestado relation
 *
 * @method     ChildServicoClienteQuery joinWithServicoPrestado($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicoPrestado relation
 *
 * @method     ChildServicoClienteQuery leftJoinWithServicoPrestado() Adds a LEFT JOIN clause and with to the query using the ServicoPrestado relation
 * @method     ChildServicoClienteQuery rightJoinWithServicoPrestado() Adds a RIGHT JOIN clause and with to the query using the ServicoPrestado relation
 * @method     ChildServicoClienteQuery innerJoinWithServicoPrestado() Adds a INNER JOIN clause and with to the query using the ServicoPrestado relation
 *
 * @method     ChildServicoClienteQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildServicoClienteQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildServicoClienteQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildServicoClienteQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildServicoClienteQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildServicoClienteQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildServicoClienteQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildServicoClienteQuery leftJoinBoletoItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoItem relation
 * @method     ChildServicoClienteQuery rightJoinBoletoItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoItem relation
 * @method     ChildServicoClienteQuery innerJoinBoletoItem($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoItem relation
 *
 * @method     ChildServicoClienteQuery joinWithBoletoItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoItem relation
 *
 * @method     ChildServicoClienteQuery leftJoinWithBoletoItem() Adds a LEFT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildServicoClienteQuery rightJoinWithBoletoItem() Adds a RIGHT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildServicoClienteQuery innerJoinWithBoletoItem() Adds a INNER JOIN clause and with to the query using the BoletoItem relation
 *
 * @method     ChildServicoClienteQuery leftJoinEnderecoServCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoServCliente relation
 * @method     ChildServicoClienteQuery rightJoinEnderecoServCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoServCliente relation
 * @method     ChildServicoClienteQuery innerJoinEnderecoServCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoServCliente relation
 *
 * @method     ChildServicoClienteQuery joinWithEnderecoServCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoServCliente relation
 *
 * @method     ChildServicoClienteQuery leftJoinWithEnderecoServCliente() Adds a LEFT JOIN clause and with to the query using the EnderecoServCliente relation
 * @method     ChildServicoClienteQuery rightJoinWithEnderecoServCliente() Adds a RIGHT JOIN clause and with to the query using the EnderecoServCliente relation
 * @method     ChildServicoClienteQuery innerJoinWithEnderecoServCliente() Adds a INNER JOIN clause and with to the query using the EnderecoServCliente relation
 *
 * @method     \ImaTelecomBundle\Model\ClienteQuery|\ImaTelecomBundle\Model\ContratoQuery|\ImaTelecomBundle\Model\ServicoPrestadoQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BoletoItemQuery|\ImaTelecomBundle\Model\EnderecoServClienteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildServicoCliente findOne(ConnectionInterface $con = null) Return the first ChildServicoCliente matching the query
 * @method     ChildServicoCliente findOneOrCreate(ConnectionInterface $con = null) Return the first ChildServicoCliente matching the query, or a new ChildServicoCliente object populated from the query conditions when no match is found
 *
 * @method     ChildServicoCliente findOneByIdservicoCliente(int $idservico_cliente) Return the first ChildServicoCliente filtered by the idservico_cliente column
 * @method     ChildServicoCliente findOneByServicoPrestadoId(int $servico_prestado_id) Return the first ChildServicoCliente filtered by the servico_prestado_id column
 * @method     ChildServicoCliente findOneByClienteId(int $cliente_id) Return the first ChildServicoCliente filtered by the cliente_id column
 * @method     ChildServicoCliente findOneByCodCesta(int $cod_cesta) Return the first ChildServicoCliente filtered by the cod_cesta column
 * @method     ChildServicoCliente findOneByDataContratado(string $data_contratado) Return the first ChildServicoCliente filtered by the data_contratado column
 * @method     ChildServicoCliente findOneByDataCancelado(string $data_cancelado) Return the first ChildServicoCliente filtered by the data_cancelado column
 * @method     ChildServicoCliente findOneByDataCadastro(string $data_cadastro) Return the first ChildServicoCliente filtered by the data_cadastro column
 * @method     ChildServicoCliente findOneByDataAlterado(string $data_alterado) Return the first ChildServicoCliente filtered by the data_alterado column
 * @method     ChildServicoCliente findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildServicoCliente filtered by the usuario_alterado column
 * @method     ChildServicoCliente findOneByImportId(int $import_id) Return the first ChildServicoCliente filtered by the import_id column
 * @method     ChildServicoCliente findOneByDescricaoNotaFiscal(string $descricao_nota_fiscal) Return the first ChildServicoCliente filtered by the descricao_nota_fiscal column
 * @method     ChildServicoCliente findOneByContratoId(int $contrato_id) Return the first ChildServicoCliente filtered by the contrato_id column
 * @method     ChildServicoCliente findOneByTipoPeriodoReferencia(string $tipo_periodo_referencia) Return the first ChildServicoCliente filtered by the tipo_periodo_referencia column
 * @method     ChildServicoCliente findOneByValorDesconto(string $valor_desconto) Return the first ChildServicoCliente filtered by the valor_desconto column
 * @method     ChildServicoCliente findOneByValor(string $valor) Return the first ChildServicoCliente filtered by the valor column *

 * @method     ChildServicoCliente requirePk($key, ConnectionInterface $con = null) Return the ChildServicoCliente by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOne(ConnectionInterface $con = null) Return the first ChildServicoCliente matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicoCliente requireOneByIdservicoCliente(int $idservico_cliente) Return the first ChildServicoCliente filtered by the idservico_cliente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByServicoPrestadoId(int $servico_prestado_id) Return the first ChildServicoCliente filtered by the servico_prestado_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByClienteId(int $cliente_id) Return the first ChildServicoCliente filtered by the cliente_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByCodCesta(int $cod_cesta) Return the first ChildServicoCliente filtered by the cod_cesta column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByDataContratado(string $data_contratado) Return the first ChildServicoCliente filtered by the data_contratado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByDataCancelado(string $data_cancelado) Return the first ChildServicoCliente filtered by the data_cancelado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByDataCadastro(string $data_cadastro) Return the first ChildServicoCliente filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByDataAlterado(string $data_alterado) Return the first ChildServicoCliente filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildServicoCliente filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByImportId(int $import_id) Return the first ChildServicoCliente filtered by the import_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByDescricaoNotaFiscal(string $descricao_nota_fiscal) Return the first ChildServicoCliente filtered by the descricao_nota_fiscal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByContratoId(int $contrato_id) Return the first ChildServicoCliente filtered by the contrato_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByTipoPeriodoReferencia(string $tipo_periodo_referencia) Return the first ChildServicoCliente filtered by the tipo_periodo_referencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByValorDesconto(string $valor_desconto) Return the first ChildServicoCliente filtered by the valor_desconto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoCliente requireOneByValor(string $valor) Return the first ChildServicoCliente filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicoCliente[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildServicoCliente objects based on current ModelCriteria
 * @method     ChildServicoCliente[]|ObjectCollection findByIdservicoCliente(int $idservico_cliente) Return ChildServicoCliente objects filtered by the idservico_cliente column
 * @method     ChildServicoCliente[]|ObjectCollection findByServicoPrestadoId(int $servico_prestado_id) Return ChildServicoCliente objects filtered by the servico_prestado_id column
 * @method     ChildServicoCliente[]|ObjectCollection findByClienteId(int $cliente_id) Return ChildServicoCliente objects filtered by the cliente_id column
 * @method     ChildServicoCliente[]|ObjectCollection findByCodCesta(int $cod_cesta) Return ChildServicoCliente objects filtered by the cod_cesta column
 * @method     ChildServicoCliente[]|ObjectCollection findByDataContratado(string $data_contratado) Return ChildServicoCliente objects filtered by the data_contratado column
 * @method     ChildServicoCliente[]|ObjectCollection findByDataCancelado(string $data_cancelado) Return ChildServicoCliente objects filtered by the data_cancelado column
 * @method     ChildServicoCliente[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildServicoCliente objects filtered by the data_cadastro column
 * @method     ChildServicoCliente[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildServicoCliente objects filtered by the data_alterado column
 * @method     ChildServicoCliente[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildServicoCliente objects filtered by the usuario_alterado column
 * @method     ChildServicoCliente[]|ObjectCollection findByImportId(int $import_id) Return ChildServicoCliente objects filtered by the import_id column
 * @method     ChildServicoCliente[]|ObjectCollection findByDescricaoNotaFiscal(string $descricao_nota_fiscal) Return ChildServicoCliente objects filtered by the descricao_nota_fiscal column
 * @method     ChildServicoCliente[]|ObjectCollection findByContratoId(int $contrato_id) Return ChildServicoCliente objects filtered by the contrato_id column
 * @method     ChildServicoCliente[]|ObjectCollection findByTipoPeriodoReferencia(string $tipo_periodo_referencia) Return ChildServicoCliente objects filtered by the tipo_periodo_referencia column
 * @method     ChildServicoCliente[]|ObjectCollection findByValorDesconto(string $valor_desconto) Return ChildServicoCliente objects filtered by the valor_desconto column
 * @method     ChildServicoCliente[]|ObjectCollection findByValor(string $valor) Return ChildServicoCliente objects filtered by the valor column
 * @method     ChildServicoCliente[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ServicoClienteQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ServicoClienteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ServicoCliente', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildServicoClienteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildServicoClienteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildServicoClienteQuery) {
            return $criteria;
        }
        $query = new ChildServicoClienteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildServicoCliente|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ServicoClienteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ServicoClienteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicoCliente A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idservico_cliente, servico_prestado_id, cliente_id, cod_cesta, data_contratado, data_cancelado, data_cadastro, data_alterado, usuario_alterado, import_id, descricao_nota_fiscal, contrato_id, tipo_periodo_referencia, valor_desconto, valor FROM servico_cliente WHERE idservico_cliente = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildServicoCliente $obj */
            $obj = new ChildServicoCliente();
            $obj->hydrate($row);
            ServicoClienteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildServicoCliente|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idservico_cliente column
     *
     * Example usage:
     * <code>
     * $query->filterByIdservicoCliente(1234); // WHERE idservico_cliente = 1234
     * $query->filterByIdservicoCliente(array(12, 34)); // WHERE idservico_cliente IN (12, 34)
     * $query->filterByIdservicoCliente(array('min' => 12)); // WHERE idservico_cliente > 12
     * </code>
     *
     * @param     mixed $idservicoCliente The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByIdservicoCliente($idservicoCliente = null, $comparison = null)
    {
        if (is_array($idservicoCliente)) {
            $useMinMax = false;
            if (isset($idservicoCliente['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $idservicoCliente['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idservicoCliente['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $idservicoCliente['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $idservicoCliente, $comparison);
    }

    /**
     * Filter the query on the servico_prestado_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServicoPrestadoId(1234); // WHERE servico_prestado_id = 1234
     * $query->filterByServicoPrestadoId(array(12, 34)); // WHERE servico_prestado_id IN (12, 34)
     * $query->filterByServicoPrestadoId(array('min' => 12)); // WHERE servico_prestado_id > 12
     * </code>
     *
     * @see       filterByServicoPrestado()
     *
     * @param     mixed $servicoPrestadoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByServicoPrestadoId($servicoPrestadoId = null, $comparison = null)
    {
        if (is_array($servicoPrestadoId)) {
            $useMinMax = false;
            if (isset($servicoPrestadoId['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID, $servicoPrestadoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($servicoPrestadoId['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID, $servicoPrestadoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID, $servicoPrestadoId, $comparison);
    }

    /**
     * Filter the query on the cliente_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClienteId(1234); // WHERE cliente_id = 1234
     * $query->filterByClienteId(array(12, 34)); // WHERE cliente_id IN (12, 34)
     * $query->filterByClienteId(array('min' => 12)); // WHERE cliente_id > 12
     * </code>
     *
     * @see       filterByCliente()
     *
     * @param     mixed $clienteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByClienteId($clienteId = null, $comparison = null)
    {
        if (is_array($clienteId)) {
            $useMinMax = false;
            if (isset($clienteId['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_CLIENTE_ID, $clienteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clienteId['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_CLIENTE_ID, $clienteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_CLIENTE_ID, $clienteId, $comparison);
    }

    /**
     * Filter the query on the cod_cesta column
     *
     * Example usage:
     * <code>
     * $query->filterByCodCesta(1234); // WHERE cod_cesta = 1234
     * $query->filterByCodCesta(array(12, 34)); // WHERE cod_cesta IN (12, 34)
     * $query->filterByCodCesta(array('min' => 12)); // WHERE cod_cesta > 12
     * </code>
     *
     * @param     mixed $codCesta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByCodCesta($codCesta = null, $comparison = null)
    {
        if (is_array($codCesta)) {
            $useMinMax = false;
            if (isset($codCesta['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_COD_CESTA, $codCesta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($codCesta['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_COD_CESTA, $codCesta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_COD_CESTA, $codCesta, $comparison);
    }

    /**
     * Filter the query on the data_contratado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataContratado('2011-03-14'); // WHERE data_contratado = '2011-03-14'
     * $query->filterByDataContratado('now'); // WHERE data_contratado = '2011-03-14'
     * $query->filterByDataContratado(array('max' => 'yesterday')); // WHERE data_contratado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataContratado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByDataContratado($dataContratado = null, $comparison = null)
    {
        if (is_array($dataContratado)) {
            $useMinMax = false;
            if (isset($dataContratado['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_CONTRATADO, $dataContratado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataContratado['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_CONTRATADO, $dataContratado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_CONTRATADO, $dataContratado, $comparison);
    }

    /**
     * Filter the query on the data_cancelado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCancelado('2011-03-14'); // WHERE data_cancelado = '2011-03-14'
     * $query->filterByDataCancelado('now'); // WHERE data_cancelado = '2011-03-14'
     * $query->filterByDataCancelado(array('max' => 'yesterday')); // WHERE data_cancelado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCancelado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByDataCancelado($dataCancelado = null, $comparison = null)
    {
        if (is_array($dataCancelado)) {
            $useMinMax = false;
            if (isset($dataCancelado['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_CANCELADO, $dataCancelado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCancelado['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_CANCELADO, $dataCancelado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_CANCELADO, $dataCancelado, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the import_id column
     *
     * Example usage:
     * <code>
     * $query->filterByImportId(1234); // WHERE import_id = 1234
     * $query->filterByImportId(array(12, 34)); // WHERE import_id IN (12, 34)
     * $query->filterByImportId(array('min' => 12)); // WHERE import_id > 12
     * </code>
     *
     * @param     mixed $importId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByImportId($importId = null, $comparison = null)
    {
        if (is_array($importId)) {
            $useMinMax = false;
            if (isset($importId['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_IMPORT_ID, $importId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importId['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_IMPORT_ID, $importId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_IMPORT_ID, $importId, $comparison);
    }

    /**
     * Filter the query on the descricao_nota_fiscal column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricaoNotaFiscal('fooValue');   // WHERE descricao_nota_fiscal = 'fooValue'
     * $query->filterByDescricaoNotaFiscal('%fooValue%', Criteria::LIKE); // WHERE descricao_nota_fiscal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricaoNotaFiscal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByDescricaoNotaFiscal($descricaoNotaFiscal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricaoNotaFiscal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_DESCRICAO_NOTA_FISCAL, $descricaoNotaFiscal, $comparison);
    }

    /**
     * Filter the query on the contrato_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContratoId(1234); // WHERE contrato_id = 1234
     * $query->filterByContratoId(array(12, 34)); // WHERE contrato_id IN (12, 34)
     * $query->filterByContratoId(array('min' => 12)); // WHERE contrato_id > 12
     * </code>
     *
     * @see       filterByContrato()
     *
     * @param     mixed $contratoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByContratoId($contratoId = null, $comparison = null)
    {
        if (is_array($contratoId)) {
            $useMinMax = false;
            if (isset($contratoId['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_CONTRATO_ID, $contratoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contratoId['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_CONTRATO_ID, $contratoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_CONTRATO_ID, $contratoId, $comparison);
    }

    /**
     * Filter the query on the tipo_periodo_referencia column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoPeriodoReferencia('fooValue');   // WHERE tipo_periodo_referencia = 'fooValue'
     * $query->filterByTipoPeriodoReferencia('%fooValue%', Criteria::LIKE); // WHERE tipo_periodo_referencia LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoPeriodoReferencia The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByTipoPeriodoReferencia($tipoPeriodoReferencia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoPeriodoReferencia)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_TIPO_PERIODO_REFERENCIA, $tipoPeriodoReferencia, $comparison);
    }

    /**
     * Filter the query on the valor_desconto column
     *
     * Example usage:
     * <code>
     * $query->filterByValorDesconto(1234); // WHERE valor_desconto = 1234
     * $query->filterByValorDesconto(array(12, 34)); // WHERE valor_desconto IN (12, 34)
     * $query->filterByValorDesconto(array('min' => 12)); // WHERE valor_desconto > 12
     * </code>
     *
     * @param     mixed $valorDesconto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByValorDesconto($valorDesconto = null, $comparison = null)
    {
        if (is_array($valorDesconto)) {
            $useMinMax = false;
            if (isset($valorDesconto['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_VALOR_DESCONTO, $valorDesconto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorDesconto['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_VALOR_DESCONTO, $valorDesconto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_VALOR_DESCONTO, $valorDesconto, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(ServicoClienteTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoClienteTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cliente object
     *
     * @param \ImaTelecomBundle\Model\Cliente|ObjectCollection $cliente The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByCliente($cliente, $comparison = null)
    {
        if ($cliente instanceof \ImaTelecomBundle\Model\Cliente) {
            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_CLIENTE_ID, $cliente->getIdcliente(), $comparison);
        } elseif ($cliente instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_CLIENTE_ID, $cliente->toKeyValue('PrimaryKey', 'Idcliente'), $comparison);
        } else {
            throw new PropelException('filterByCliente() only accepts arguments of type \ImaTelecomBundle\Model\Cliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function joinCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cliente');
        }

        return $this;
    }

    /**
     * Use the Cliente relation Cliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteQuery A secondary query class using the current class as primary query
     */
    public function useClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cliente', '\ImaTelecomBundle\Model\ClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Contrato object
     *
     * @param \ImaTelecomBundle\Model\Contrato|ObjectCollection $contrato The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByContrato($contrato, $comparison = null)
    {
        if ($contrato instanceof \ImaTelecomBundle\Model\Contrato) {
            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_CONTRATO_ID, $contrato->getIdcontrato(), $comparison);
        } elseif ($contrato instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_CONTRATO_ID, $contrato->toKeyValue('PrimaryKey', 'Idcontrato'), $comparison);
        } else {
            throw new PropelException('filterByContrato() only accepts arguments of type \ImaTelecomBundle\Model\Contrato or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contrato relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function joinContrato($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contrato');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contrato');
        }

        return $this;
    }

    /**
     * Use the Contrato relation Contrato object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContratoQuery A secondary query class using the current class as primary query
     */
    public function useContratoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContrato($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contrato', '\ImaTelecomBundle\Model\ContratoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ServicoPrestado object
     *
     * @param \ImaTelecomBundle\Model\ServicoPrestado|ObjectCollection $servicoPrestado The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByServicoPrestado($servicoPrestado, $comparison = null)
    {
        if ($servicoPrestado instanceof \ImaTelecomBundle\Model\ServicoPrestado) {
            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID, $servicoPrestado->getIdservicoPrestado(), $comparison);
        } elseif ($servicoPrestado instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID, $servicoPrestado->toKeyValue('PrimaryKey', 'IdservicoPrestado'), $comparison);
        } else {
            throw new PropelException('filterByServicoPrestado() only accepts arguments of type \ImaTelecomBundle\Model\ServicoPrestado or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicoPrestado relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function joinServicoPrestado($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicoPrestado');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicoPrestado');
        }

        return $this;
    }

    /**
     * Use the ServicoPrestado relation ServicoPrestado object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ServicoPrestadoQuery A secondary query class using the current class as primary query
     */
    public function useServicoPrestadoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicoPrestado($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicoPrestado', '\ImaTelecomBundle\Model\ServicoPrestadoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoItem object
     *
     * @param \ImaTelecomBundle\Model\BoletoItem|ObjectCollection $boletoItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByBoletoItem($boletoItem, $comparison = null)
    {
        if ($boletoItem instanceof \ImaTelecomBundle\Model\BoletoItem) {
            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $boletoItem->getServicoClienteId(), $comparison);
        } elseif ($boletoItem instanceof ObjectCollection) {
            return $this
                ->useBoletoItemQuery()
                ->filterByPrimaryKeys($boletoItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoItem() only accepts arguments of type \ImaTelecomBundle\Model\BoletoItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function joinBoletoItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoItem');
        }

        return $this;
    }

    /**
     * Use the BoletoItem relation BoletoItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoItemQuery A secondary query class using the current class as primary query
     */
    public function useBoletoItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoletoItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoItem', '\ImaTelecomBundle\Model\BoletoItemQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoServCliente object
     *
     * @param \ImaTelecomBundle\Model\EnderecoServCliente|ObjectCollection $enderecoServCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildServicoClienteQuery The current query, for fluid interface
     */
    public function filterByEnderecoServCliente($enderecoServCliente, $comparison = null)
    {
        if ($enderecoServCliente instanceof \ImaTelecomBundle\Model\EnderecoServCliente) {
            return $this
                ->addUsingAlias(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $enderecoServCliente->getServicoClienteId(), $comparison);
        } elseif ($enderecoServCliente instanceof ObjectCollection) {
            return $this
                ->useEnderecoServClienteQuery()
                ->filterByPrimaryKeys($enderecoServCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEnderecoServCliente() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoServCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoServCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function joinEnderecoServCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoServCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoServCliente');
        }

        return $this;
    }

    /**
     * Use the EnderecoServCliente relation EnderecoServCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoServClienteQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoServClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEnderecoServCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoServCliente', '\ImaTelecomBundle\Model\EnderecoServClienteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildServicoCliente $servicoCliente Object to remove from the list of results
     *
     * @return $this|ChildServicoClienteQuery The current query, for fluid interface
     */
    public function prune($servicoCliente = null)
    {
        if ($servicoCliente) {
            $this->addUsingAlias(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $servicoCliente->getIdservicoCliente(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the servico_cliente table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoClienteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ServicoClienteTableMap::clearInstancePool();
            ServicoClienteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoClienteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ServicoClienteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ServicoClienteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ServicoClienteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ServicoClienteQuery
