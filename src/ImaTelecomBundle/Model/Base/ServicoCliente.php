<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BoletoItem as ChildBoletoItem;
use ImaTelecomBundle\Model\BoletoItemQuery as ChildBoletoItemQuery;
use ImaTelecomBundle\Model\Cliente as ChildCliente;
use ImaTelecomBundle\Model\ClienteQuery as ChildClienteQuery;
use ImaTelecomBundle\Model\Contrato as ChildContrato;
use ImaTelecomBundle\Model\ContratoQuery as ChildContratoQuery;
use ImaTelecomBundle\Model\EnderecoServCliente as ChildEnderecoServCliente;
use ImaTelecomBundle\Model\EnderecoServClienteQuery as ChildEnderecoServClienteQuery;
use ImaTelecomBundle\Model\ServicoCliente as ChildServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery as ChildServicoClienteQuery;
use ImaTelecomBundle\Model\ServicoPrestado as ChildServicoPrestado;
use ImaTelecomBundle\Model\ServicoPrestadoQuery as ChildServicoPrestadoQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BoletoItemTableMap;
use ImaTelecomBundle\Model\Map\EnderecoServClienteTableMap;
use ImaTelecomBundle\Model\Map\ServicoClienteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'servico_cliente' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class ServicoCliente implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\ServicoClienteTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idservico_cliente field.
     *
     * @var        int
     */
    protected $idservico_cliente;

    /**
     * The value for the servico_prestado_id field.
     *
     * @var        int
     */
    protected $servico_prestado_id;

    /**
     * The value for the cliente_id field.
     *
     * @var        int
     */
    protected $cliente_id;

    /**
     * The value for the cod_cesta field.
     *
     * @var        int
     */
    protected $cod_cesta;

    /**
     * The value for the data_contratado field.
     *
     * @var        DateTime
     */
    protected $data_contratado;

    /**
     * The value for the data_cancelado field.
     *
     * @var        DateTime
     */
    protected $data_cancelado;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the import_id field.
     *
     * @var        int
     */
    protected $import_id;

    /**
     * The value for the descricao_nota_fiscal field.
     *
     * @var        string
     */
    protected $descricao_nota_fiscal;

    /**
     * The value for the contrato_id field.
     *
     * @var        int
     */
    protected $contrato_id;

    /**
     * The value for the tipo_periodo_referencia field.
     *
     * Note: this column has a database default value of: 'corrente'
     * @var        string
     */
    protected $tipo_periodo_referencia;

    /**
     * The value for the valor_desconto field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_desconto;

    /**
     * The value for the valor field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor;

    /**
     * @var        ChildCliente
     */
    protected $aCliente;

    /**
     * @var        ChildContrato
     */
    protected $aContrato;

    /**
     * @var        ChildServicoPrestado
     */
    protected $aServicoPrestado;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildBoletoItem[] Collection to store aggregation of ChildBoletoItem objects.
     */
    protected $collBoletoItems;
    protected $collBoletoItemsPartial;

    /**
     * @var        ObjectCollection|ChildEnderecoServCliente[] Collection to store aggregation of ChildEnderecoServCliente objects.
     */
    protected $collEnderecoServClientes;
    protected $collEnderecoServClientesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoletoItem[]
     */
    protected $boletoItemsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEnderecoServCliente[]
     */
    protected $enderecoServClientesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->tipo_periodo_referencia = 'corrente';
        $this->valor_desconto = '0.00';
        $this->valor = '0.00';
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\ServicoCliente object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ServicoCliente</code> instance.  If
     * <code>obj</code> is an instance of <code>ServicoCliente</code>, delegates to
     * <code>equals(ServicoCliente)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ServicoCliente The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idservico_cliente] column value.
     *
     * @return int
     */
    public function getIdservicoCliente()
    {
        return $this->idservico_cliente;
    }

    /**
     * Get the [servico_prestado_id] column value.
     *
     * @return int
     */
    public function getServicoPrestadoId()
    {
        return $this->servico_prestado_id;
    }

    /**
     * Get the [cliente_id] column value.
     *
     * @return int
     */
    public function getClienteId()
    {
        return $this->cliente_id;
    }

    /**
     * Get the [cod_cesta] column value.
     *
     * @return int
     */
    public function getCodCesta()
    {
        return $this->cod_cesta;
    }

    /**
     * Get the [optionally formatted] temporal [data_contratado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataContratado($format = NULL)
    {
        if ($format === null) {
            return $this->data_contratado;
        } else {
            return $this->data_contratado instanceof \DateTimeInterface ? $this->data_contratado->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_cancelado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCancelado($format = NULL)
    {
        if ($format === null) {
            return $this->data_cancelado;
        } else {
            return $this->data_cancelado instanceof \DateTimeInterface ? $this->data_cancelado->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [import_id] column value.
     *
     * @return int
     */
    public function getImportId()
    {
        return $this->import_id;
    }

    /**
     * Get the [descricao_nota_fiscal] column value.
     *
     * @return string
     */
    public function getDescricaoNotaFiscal()
    {
        return $this->descricao_nota_fiscal;
    }

    /**
     * Get the [contrato_id] column value.
     *
     * @return int
     */
    public function getContratoId()
    {
        return $this->contrato_id;
    }

    /**
     * Get the [tipo_periodo_referencia] column value.
     *
     * @return string
     */
    public function getTipoPeriodoReferencia()
    {
        return $this->tipo_periodo_referencia;
    }

    /**
     * Get the [valor_desconto] column value.
     *
     * @return string
     */
    public function getValorDesconto()
    {
        return $this->valor_desconto;
    }

    /**
     * Get the [valor] column value.
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set the value of [idservico_cliente] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setIdservicoCliente($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idservico_cliente !== $v) {
            $this->idservico_cliente = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_IDSERVICO_CLIENTE] = true;
        }

        return $this;
    } // setIdservicoCliente()

    /**
     * Set the value of [servico_prestado_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setServicoPrestadoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->servico_prestado_id !== $v) {
            $this->servico_prestado_id = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID] = true;
        }

        if ($this->aServicoPrestado !== null && $this->aServicoPrestado->getIdservicoPrestado() !== $v) {
            $this->aServicoPrestado = null;
        }

        return $this;
    } // setServicoPrestadoId()

    /**
     * Set the value of [cliente_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setClienteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cliente_id !== $v) {
            $this->cliente_id = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_CLIENTE_ID] = true;
        }

        if ($this->aCliente !== null && $this->aCliente->getIdcliente() !== $v) {
            $this->aCliente = null;
        }

        return $this;
    } // setClienteId()

    /**
     * Set the value of [cod_cesta] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setCodCesta($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cod_cesta !== $v) {
            $this->cod_cesta = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_COD_CESTA] = true;
        }

        return $this;
    } // setCodCesta()

    /**
     * Sets the value of [data_contratado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setDataContratado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_contratado !== null || $dt !== null) {
            if ($this->data_contratado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_contratado->format("Y-m-d H:i:s.u")) {
                $this->data_contratado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicoClienteTableMap::COL_DATA_CONTRATADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataContratado()

    /**
     * Sets the value of [data_cancelado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setDataCancelado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cancelado !== null || $dt !== null) {
            if ($this->data_cancelado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cancelado->format("Y-m-d H:i:s.u")) {
                $this->data_cancelado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicoClienteTableMap::COL_DATA_CANCELADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCancelado()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicoClienteTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicoClienteTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Set the value of [import_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setImportId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->import_id !== $v) {
            $this->import_id = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_IMPORT_ID] = true;
        }

        return $this;
    } // setImportId()

    /**
     * Set the value of [descricao_nota_fiscal] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setDescricaoNotaFiscal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao_nota_fiscal !== $v) {
            $this->descricao_nota_fiscal = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_DESCRICAO_NOTA_FISCAL] = true;
        }

        return $this;
    } // setDescricaoNotaFiscal()

    /**
     * Set the value of [contrato_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setContratoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->contrato_id !== $v) {
            $this->contrato_id = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_CONTRATO_ID] = true;
        }

        if ($this->aContrato !== null && $this->aContrato->getIdcontrato() !== $v) {
            $this->aContrato = null;
        }

        return $this;
    } // setContratoId()

    /**
     * Set the value of [tipo_periodo_referencia] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setTipoPeriodoReferencia($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_periodo_referencia !== $v) {
            $this->tipo_periodo_referencia = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_TIPO_PERIODO_REFERENCIA] = true;
        }

        return $this;
    } // setTipoPeriodoReferencia()

    /**
     * Set the value of [valor_desconto] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setValorDesconto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_desconto !== $v) {
            $this->valor_desconto = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_VALOR_DESCONTO] = true;
        }

        return $this;
    } // setValorDesconto()

    /**
     * Set the value of [valor] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function setValor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor !== $v) {
            $this->valor = $v;
            $this->modifiedColumns[ServicoClienteTableMap::COL_VALOR] = true;
        }

        return $this;
    } // setValor()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->tipo_periodo_referencia !== 'corrente') {
                return false;
            }

            if ($this->valor_desconto !== '0.00') {
                return false;
            }

            if ($this->valor !== '0.00') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ServicoClienteTableMap::translateFieldName('IdservicoCliente', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idservico_cliente = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ServicoClienteTableMap::translateFieldName('ServicoPrestadoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->servico_prestado_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ServicoClienteTableMap::translateFieldName('ClienteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cliente_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ServicoClienteTableMap::translateFieldName('CodCesta', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cod_cesta = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ServicoClienteTableMap::translateFieldName('DataContratado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_contratado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ServicoClienteTableMap::translateFieldName('DataCancelado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cancelado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ServicoClienteTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ServicoClienteTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ServicoClienteTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ServicoClienteTableMap::translateFieldName('ImportId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->import_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ServicoClienteTableMap::translateFieldName('DescricaoNotaFiscal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao_nota_fiscal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ServicoClienteTableMap::translateFieldName('ContratoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->contrato_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ServicoClienteTableMap::translateFieldName('TipoPeriodoReferencia', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_periodo_referencia = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ServicoClienteTableMap::translateFieldName('ValorDesconto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_desconto = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ServicoClienteTableMap::translateFieldName('Valor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 15; // 15 = ServicoClienteTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\ServicoCliente'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aServicoPrestado !== null && $this->servico_prestado_id !== $this->aServicoPrestado->getIdservicoPrestado()) {
            $this->aServicoPrestado = null;
        }
        if ($this->aCliente !== null && $this->cliente_id !== $this->aCliente->getIdcliente()) {
            $this->aCliente = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
        if ($this->aContrato !== null && $this->contrato_id !== $this->aContrato->getIdcontrato()) {
            $this->aContrato = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ServicoClienteTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildServicoClienteQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCliente = null;
            $this->aContrato = null;
            $this->aServicoPrestado = null;
            $this->aUsuario = null;
            $this->collBoletoItems = null;

            $this->collEnderecoServClientes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ServicoCliente::setDeleted()
     * @see ServicoCliente::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoClienteTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildServicoClienteQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoClienteTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ServicoClienteTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCliente !== null) {
                if ($this->aCliente->isModified() || $this->aCliente->isNew()) {
                    $affectedRows += $this->aCliente->save($con);
                }
                $this->setCliente($this->aCliente);
            }

            if ($this->aContrato !== null) {
                if ($this->aContrato->isModified() || $this->aContrato->isNew()) {
                    $affectedRows += $this->aContrato->save($con);
                }
                $this->setContrato($this->aContrato);
            }

            if ($this->aServicoPrestado !== null) {
                if ($this->aServicoPrestado->isModified() || $this->aServicoPrestado->isNew()) {
                    $affectedRows += $this->aServicoPrestado->save($con);
                }
                $this->setServicoPrestado($this->aServicoPrestado);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->boletoItemsScheduledForDeletion !== null) {
                if (!$this->boletoItemsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BoletoItemQuery::create()
                        ->filterByPrimaryKeys($this->boletoItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->boletoItemsScheduledForDeletion = null;
                }
            }

            if ($this->collBoletoItems !== null) {
                foreach ($this->collBoletoItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->enderecoServClientesScheduledForDeletion !== null) {
                if (!$this->enderecoServClientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EnderecoServClienteQuery::create()
                        ->filterByPrimaryKeys($this->enderecoServClientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->enderecoServClientesScheduledForDeletion = null;
                }
            }

            if ($this->collEnderecoServClientes !== null) {
                foreach ($this->collEnderecoServClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ServicoClienteTableMap::COL_IDSERVICO_CLIENTE] = true;
        if (null !== $this->idservico_cliente) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ServicoClienteTableMap::COL_IDSERVICO_CLIENTE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE)) {
            $modifiedColumns[':p' . $index++]  = 'idservico_cliente';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'servico_prestado_id';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_CLIENTE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cliente_id';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_COD_CESTA)) {
            $modifiedColumns[':p' . $index++]  = 'cod_cesta';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DATA_CONTRATADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_contratado';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DATA_CANCELADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cancelado';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_IMPORT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'import_id';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DESCRICAO_NOTA_FISCAL)) {
            $modifiedColumns[':p' . $index++]  = 'descricao_nota_fiscal';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_CONTRATO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'contrato_id';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_TIPO_PERIODO_REFERENCIA)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_periodo_referencia';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_VALOR_DESCONTO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_desconto';
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_VALOR)) {
            $modifiedColumns[':p' . $index++]  = 'valor';
        }

        $sql = sprintf(
            'INSERT INTO servico_cliente (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idservico_cliente':
                        $stmt->bindValue($identifier, $this->idservico_cliente, PDO::PARAM_INT);
                        break;
                    case 'servico_prestado_id':
                        $stmt->bindValue($identifier, $this->servico_prestado_id, PDO::PARAM_INT);
                        break;
                    case 'cliente_id':
                        $stmt->bindValue($identifier, $this->cliente_id, PDO::PARAM_INT);
                        break;
                    case 'cod_cesta':
                        $stmt->bindValue($identifier, $this->cod_cesta, PDO::PARAM_INT);
                        break;
                    case 'data_contratado':
                        $stmt->bindValue($identifier, $this->data_contratado ? $this->data_contratado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_cancelado':
                        $stmt->bindValue($identifier, $this->data_cancelado ? $this->data_cancelado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'import_id':
                        $stmt->bindValue($identifier, $this->import_id, PDO::PARAM_INT);
                        break;
                    case 'descricao_nota_fiscal':
                        $stmt->bindValue($identifier, $this->descricao_nota_fiscal, PDO::PARAM_STR);
                        break;
                    case 'contrato_id':
                        $stmt->bindValue($identifier, $this->contrato_id, PDO::PARAM_INT);
                        break;
                    case 'tipo_periodo_referencia':
                        $stmt->bindValue($identifier, $this->tipo_periodo_referencia, PDO::PARAM_STR);
                        break;
                    case 'valor_desconto':
                        $stmt->bindValue($identifier, $this->valor_desconto, PDO::PARAM_STR);
                        break;
                    case 'valor':
                        $stmt->bindValue($identifier, $this->valor, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdservicoCliente($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ServicoClienteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdservicoCliente();
                break;
            case 1:
                return $this->getServicoPrestadoId();
                break;
            case 2:
                return $this->getClienteId();
                break;
            case 3:
                return $this->getCodCesta();
                break;
            case 4:
                return $this->getDataContratado();
                break;
            case 5:
                return $this->getDataCancelado();
                break;
            case 6:
                return $this->getDataCadastro();
                break;
            case 7:
                return $this->getDataAlterado();
                break;
            case 8:
                return $this->getUsuarioAlterado();
                break;
            case 9:
                return $this->getImportId();
                break;
            case 10:
                return $this->getDescricaoNotaFiscal();
                break;
            case 11:
                return $this->getContratoId();
                break;
            case 12:
                return $this->getTipoPeriodoReferencia();
                break;
            case 13:
                return $this->getValorDesconto();
                break;
            case 14:
                return $this->getValor();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ServicoCliente'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ServicoCliente'][$this->hashCode()] = true;
        $keys = ServicoClienteTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdservicoCliente(),
            $keys[1] => $this->getServicoPrestadoId(),
            $keys[2] => $this->getClienteId(),
            $keys[3] => $this->getCodCesta(),
            $keys[4] => $this->getDataContratado(),
            $keys[5] => $this->getDataCancelado(),
            $keys[6] => $this->getDataCadastro(),
            $keys[7] => $this->getDataAlterado(),
            $keys[8] => $this->getUsuarioAlterado(),
            $keys[9] => $this->getImportId(),
            $keys[10] => $this->getDescricaoNotaFiscal(),
            $keys[11] => $this->getContratoId(),
            $keys[12] => $this->getTipoPeriodoReferencia(),
            $keys[13] => $this->getValorDesconto(),
            $keys[14] => $this->getValor(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCliente) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'cliente';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cliente';
                        break;
                    default:
                        $key = 'Cliente';
                }

                $result[$key] = $this->aCliente->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aContrato) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contrato';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contrato';
                        break;
                    default:
                        $key = 'Contrato';
                }

                $result[$key] = $this->aContrato->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aServicoPrestado) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicoPrestado';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'servico_prestado';
                        break;
                    default:
                        $key = 'ServicoPrestado';
                }

                $result[$key] = $this->aServicoPrestado->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBoletoItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_items';
                        break;
                    default:
                        $key = 'BoletoItems';
                }

                $result[$key] = $this->collBoletoItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEnderecoServClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'enderecoServClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'endereco_serv_clientes';
                        break;
                    default:
                        $key = 'EnderecoServClientes';
                }

                $result[$key] = $this->collEnderecoServClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ServicoClienteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdservicoCliente($value);
                break;
            case 1:
                $this->setServicoPrestadoId($value);
                break;
            case 2:
                $this->setClienteId($value);
                break;
            case 3:
                $this->setCodCesta($value);
                break;
            case 4:
                $this->setDataContratado($value);
                break;
            case 5:
                $this->setDataCancelado($value);
                break;
            case 6:
                $this->setDataCadastro($value);
                break;
            case 7:
                $this->setDataAlterado($value);
                break;
            case 8:
                $this->setUsuarioAlterado($value);
                break;
            case 9:
                $this->setImportId($value);
                break;
            case 10:
                $this->setDescricaoNotaFiscal($value);
                break;
            case 11:
                $this->setContratoId($value);
                break;
            case 12:
                $this->setTipoPeriodoReferencia($value);
                break;
            case 13:
                $this->setValorDesconto($value);
                break;
            case 14:
                $this->setValor($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ServicoClienteTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdservicoCliente($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setServicoPrestadoId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setClienteId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCodCesta($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDataContratado($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDataCancelado($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDataCadastro($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDataAlterado($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setImportId($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDescricaoNotaFiscal($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setContratoId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setTipoPeriodoReferencia($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setValorDesconto($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setValor($arr[$keys[14]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ServicoClienteTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE)) {
            $criteria->add(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $this->idservico_cliente);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID)) {
            $criteria->add(ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID, $this->servico_prestado_id);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_CLIENTE_ID)) {
            $criteria->add(ServicoClienteTableMap::COL_CLIENTE_ID, $this->cliente_id);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_COD_CESTA)) {
            $criteria->add(ServicoClienteTableMap::COL_COD_CESTA, $this->cod_cesta);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DATA_CONTRATADO)) {
            $criteria->add(ServicoClienteTableMap::COL_DATA_CONTRATADO, $this->data_contratado);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DATA_CANCELADO)) {
            $criteria->add(ServicoClienteTableMap::COL_DATA_CANCELADO, $this->data_cancelado);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(ServicoClienteTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(ServicoClienteTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(ServicoClienteTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_IMPORT_ID)) {
            $criteria->add(ServicoClienteTableMap::COL_IMPORT_ID, $this->import_id);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_DESCRICAO_NOTA_FISCAL)) {
            $criteria->add(ServicoClienteTableMap::COL_DESCRICAO_NOTA_FISCAL, $this->descricao_nota_fiscal);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_CONTRATO_ID)) {
            $criteria->add(ServicoClienteTableMap::COL_CONTRATO_ID, $this->contrato_id);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_TIPO_PERIODO_REFERENCIA)) {
            $criteria->add(ServicoClienteTableMap::COL_TIPO_PERIODO_REFERENCIA, $this->tipo_periodo_referencia);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_VALOR_DESCONTO)) {
            $criteria->add(ServicoClienteTableMap::COL_VALOR_DESCONTO, $this->valor_desconto);
        }
        if ($this->isColumnModified(ServicoClienteTableMap::COL_VALOR)) {
            $criteria->add(ServicoClienteTableMap::COL_VALOR, $this->valor);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildServicoClienteQuery::create();
        $criteria->add(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, $this->idservico_cliente);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdservicoCliente();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdservicoCliente();
    }

    /**
     * Generic method to set the primary key (idservico_cliente column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdservicoCliente($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdservicoCliente();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\ServicoCliente (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setServicoPrestadoId($this->getServicoPrestadoId());
        $copyObj->setClienteId($this->getClienteId());
        $copyObj->setCodCesta($this->getCodCesta());
        $copyObj->setDataContratado($this->getDataContratado());
        $copyObj->setDataCancelado($this->getDataCancelado());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setImportId($this->getImportId());
        $copyObj->setDescricaoNotaFiscal($this->getDescricaoNotaFiscal());
        $copyObj->setContratoId($this->getContratoId());
        $copyObj->setTipoPeriodoReferencia($this->getTipoPeriodoReferencia());
        $copyObj->setValorDesconto($this->getValorDesconto());
        $copyObj->setValor($this->getValor());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBoletoItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoletoItem($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEnderecoServClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEnderecoServCliente($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdservicoCliente(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\ServicoCliente Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCliente object.
     *
     * @param  ChildCliente $v
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCliente(ChildCliente $v = null)
    {
        if ($v === null) {
            $this->setClienteId(NULL);
        } else {
            $this->setClienteId($v->getIdcliente());
        }

        $this->aCliente = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCliente object, it will not be re-added.
        if ($v !== null) {
            $v->addServicoCliente($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCliente object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCliente The associated ChildCliente object.
     * @throws PropelException
     */
    public function getCliente(ConnectionInterface $con = null)
    {
        if ($this->aCliente === null && ($this->cliente_id !== null)) {
            $this->aCliente = ChildClienteQuery::create()->findPk($this->cliente_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCliente->addServicoClientes($this);
             */
        }

        return $this->aCliente;
    }

    /**
     * Declares an association between this object and a ChildContrato object.
     *
     * @param  ChildContrato $v
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     * @throws PropelException
     */
    public function setContrato(ChildContrato $v = null)
    {
        if ($v === null) {
            $this->setContratoId(NULL);
        } else {
            $this->setContratoId($v->getIdcontrato());
        }

        $this->aContrato = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildContrato object, it will not be re-added.
        if ($v !== null) {
            $v->addServicoCliente($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildContrato object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildContrato The associated ChildContrato object.
     * @throws PropelException
     */
    public function getContrato(ConnectionInterface $con = null)
    {
        if ($this->aContrato === null && ($this->contrato_id !== null)) {
            $this->aContrato = ChildContratoQuery::create()->findPk($this->contrato_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aContrato->addServicoClientes($this);
             */
        }

        return $this->aContrato;
    }

    /**
     * Declares an association between this object and a ChildServicoPrestado object.
     *
     * @param  ChildServicoPrestado $v
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     * @throws PropelException
     */
    public function setServicoPrestado(ChildServicoPrestado $v = null)
    {
        if ($v === null) {
            $this->setServicoPrestadoId(NULL);
        } else {
            $this->setServicoPrestadoId($v->getIdservicoPrestado());
        }

        $this->aServicoPrestado = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildServicoPrestado object, it will not be re-added.
        if ($v !== null) {
            $v->addServicoCliente($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildServicoPrestado object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildServicoPrestado The associated ChildServicoPrestado object.
     * @throws PropelException
     */
    public function getServicoPrestado(ConnectionInterface $con = null)
    {
        if ($this->aServicoPrestado === null && ($this->servico_prestado_id !== null)) {
            $this->aServicoPrestado = ChildServicoPrestadoQuery::create()->findPk($this->servico_prestado_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aServicoPrestado->addServicoClientes($this);
             */
        }

        return $this->aServicoPrestado;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addServicoCliente($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addServicoClientes($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('BoletoItem' == $relationName) {
            return $this->initBoletoItems();
        }
        if ('EnderecoServCliente' == $relationName) {
            return $this->initEnderecoServClientes();
        }
    }

    /**
     * Clears out the collBoletoItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletoItems()
     */
    public function clearBoletoItems()
    {
        $this->collBoletoItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletoItems collection loaded partially.
     */
    public function resetPartialBoletoItems($v = true)
    {
        $this->collBoletoItemsPartial = $v;
    }

    /**
     * Initializes the collBoletoItems collection.
     *
     * By default this just sets the collBoletoItems collection to an empty array (like clearcollBoletoItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletoItems($overrideExisting = true)
    {
        if (null !== $this->collBoletoItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoItemTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletoItems = new $collectionClassName;
        $this->collBoletoItems->setModel('\ImaTelecomBundle\Model\BoletoItem');
    }

    /**
     * Gets an array of ChildBoletoItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildServicoCliente is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoletoItem[] List of ChildBoletoItem objects
     * @throws PropelException
     */
    public function getBoletoItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoItemsPartial && !$this->isNew();
        if (null === $this->collBoletoItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletoItems) {
                // return empty collection
                $this->initBoletoItems();
            } else {
                $collBoletoItems = ChildBoletoItemQuery::create(null, $criteria)
                    ->filterByServicoCliente($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletoItemsPartial && count($collBoletoItems)) {
                        $this->initBoletoItems(false);

                        foreach ($collBoletoItems as $obj) {
                            if (false == $this->collBoletoItems->contains($obj)) {
                                $this->collBoletoItems->append($obj);
                            }
                        }

                        $this->collBoletoItemsPartial = true;
                    }

                    return $collBoletoItems;
                }

                if ($partial && $this->collBoletoItems) {
                    foreach ($this->collBoletoItems as $obj) {
                        if ($obj->isNew()) {
                            $collBoletoItems[] = $obj;
                        }
                    }
                }

                $this->collBoletoItems = $collBoletoItems;
                $this->collBoletoItemsPartial = false;
            }
        }

        return $this->collBoletoItems;
    }

    /**
     * Sets a collection of ChildBoletoItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletoItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildServicoCliente The current object (for fluent API support)
     */
    public function setBoletoItems(Collection $boletoItems, ConnectionInterface $con = null)
    {
        /** @var ChildBoletoItem[] $boletoItemsToDelete */
        $boletoItemsToDelete = $this->getBoletoItems(new Criteria(), $con)->diff($boletoItems);


        $this->boletoItemsScheduledForDeletion = $boletoItemsToDelete;

        foreach ($boletoItemsToDelete as $boletoItemRemoved) {
            $boletoItemRemoved->setServicoCliente(null);
        }

        $this->collBoletoItems = null;
        foreach ($boletoItems as $boletoItem) {
            $this->addBoletoItem($boletoItem);
        }

        $this->collBoletoItems = $boletoItems;
        $this->collBoletoItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BoletoItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BoletoItem objects.
     * @throws PropelException
     */
    public function countBoletoItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoItemsPartial && !$this->isNew();
        if (null === $this->collBoletoItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletoItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletoItems());
            }

            $query = ChildBoletoItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByServicoCliente($this)
                ->count($con);
        }

        return count($this->collBoletoItems);
    }

    /**
     * Method called to associate a ChildBoletoItem object to this object
     * through the ChildBoletoItem foreign key attribute.
     *
     * @param  ChildBoletoItem $l ChildBoletoItem
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function addBoletoItem(ChildBoletoItem $l)
    {
        if ($this->collBoletoItems === null) {
            $this->initBoletoItems();
            $this->collBoletoItemsPartial = true;
        }

        if (!$this->collBoletoItems->contains($l)) {
            $this->doAddBoletoItem($l);

            if ($this->boletoItemsScheduledForDeletion and $this->boletoItemsScheduledForDeletion->contains($l)) {
                $this->boletoItemsScheduledForDeletion->remove($this->boletoItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoletoItem $boletoItem The ChildBoletoItem object to add.
     */
    protected function doAddBoletoItem(ChildBoletoItem $boletoItem)
    {
        $this->collBoletoItems[]= $boletoItem;
        $boletoItem->setServicoCliente($this);
    }

    /**
     * @param  ChildBoletoItem $boletoItem The ChildBoletoItem object to remove.
     * @return $this|ChildServicoCliente The current object (for fluent API support)
     */
    public function removeBoletoItem(ChildBoletoItem $boletoItem)
    {
        if ($this->getBoletoItems()->contains($boletoItem)) {
            $pos = $this->collBoletoItems->search($boletoItem);
            $this->collBoletoItems->remove($pos);
            if (null === $this->boletoItemsScheduledForDeletion) {
                $this->boletoItemsScheduledForDeletion = clone $this->collBoletoItems;
                $this->boletoItemsScheduledForDeletion->clear();
            }
            $this->boletoItemsScheduledForDeletion[]= clone $boletoItem;
            $boletoItem->setServicoCliente(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ServicoCliente is new, it will return
     * an empty collection; or if this ServicoCliente has previously
     * been saved, it will retrieve related BoletoItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ServicoCliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoItem[] List of ChildBoletoItem objects
     */
    public function getBoletoItemsJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoItemQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getBoletoItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ServicoCliente is new, it will return
     * an empty collection; or if this ServicoCliente has previously
     * been saved, it will retrieve related BoletoItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ServicoCliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoItem[] List of ChildBoletoItem objects
     */
    public function getBoletoItemsJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoItemQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletoItems($query, $con);
    }

    /**
     * Clears out the collEnderecoServClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEnderecoServClientes()
     */
    public function clearEnderecoServClientes()
    {
        $this->collEnderecoServClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEnderecoServClientes collection loaded partially.
     */
    public function resetPartialEnderecoServClientes($v = true)
    {
        $this->collEnderecoServClientesPartial = $v;
    }

    /**
     * Initializes the collEnderecoServClientes collection.
     *
     * By default this just sets the collEnderecoServClientes collection to an empty array (like clearcollEnderecoServClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEnderecoServClientes($overrideExisting = true)
    {
        if (null !== $this->collEnderecoServClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = EnderecoServClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collEnderecoServClientes = new $collectionClassName;
        $this->collEnderecoServClientes->setModel('\ImaTelecomBundle\Model\EnderecoServCliente');
    }

    /**
     * Gets an array of ChildEnderecoServCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildServicoCliente is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEnderecoServCliente[] List of ChildEnderecoServCliente objects
     * @throws PropelException
     */
    public function getEnderecoServClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoServClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoServClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEnderecoServClientes) {
                // return empty collection
                $this->initEnderecoServClientes();
            } else {
                $collEnderecoServClientes = ChildEnderecoServClienteQuery::create(null, $criteria)
                    ->filterByServicoCliente($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEnderecoServClientesPartial && count($collEnderecoServClientes)) {
                        $this->initEnderecoServClientes(false);

                        foreach ($collEnderecoServClientes as $obj) {
                            if (false == $this->collEnderecoServClientes->contains($obj)) {
                                $this->collEnderecoServClientes->append($obj);
                            }
                        }

                        $this->collEnderecoServClientesPartial = true;
                    }

                    return $collEnderecoServClientes;
                }

                if ($partial && $this->collEnderecoServClientes) {
                    foreach ($this->collEnderecoServClientes as $obj) {
                        if ($obj->isNew()) {
                            $collEnderecoServClientes[] = $obj;
                        }
                    }
                }

                $this->collEnderecoServClientes = $collEnderecoServClientes;
                $this->collEnderecoServClientesPartial = false;
            }
        }

        return $this->collEnderecoServClientes;
    }

    /**
     * Sets a collection of ChildEnderecoServCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $enderecoServClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildServicoCliente The current object (for fluent API support)
     */
    public function setEnderecoServClientes(Collection $enderecoServClientes, ConnectionInterface $con = null)
    {
        /** @var ChildEnderecoServCliente[] $enderecoServClientesToDelete */
        $enderecoServClientesToDelete = $this->getEnderecoServClientes(new Criteria(), $con)->diff($enderecoServClientes);


        $this->enderecoServClientesScheduledForDeletion = $enderecoServClientesToDelete;

        foreach ($enderecoServClientesToDelete as $enderecoServClienteRemoved) {
            $enderecoServClienteRemoved->setServicoCliente(null);
        }

        $this->collEnderecoServClientes = null;
        foreach ($enderecoServClientes as $enderecoServCliente) {
            $this->addEnderecoServCliente($enderecoServCliente);
        }

        $this->collEnderecoServClientes = $enderecoServClientes;
        $this->collEnderecoServClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EnderecoServCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EnderecoServCliente objects.
     * @throws PropelException
     */
    public function countEnderecoServClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoServClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoServClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEnderecoServClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEnderecoServClientes());
            }

            $query = ChildEnderecoServClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByServicoCliente($this)
                ->count($con);
        }

        return count($this->collEnderecoServClientes);
    }

    /**
     * Method called to associate a ChildEnderecoServCliente object to this object
     * through the ChildEnderecoServCliente foreign key attribute.
     *
     * @param  ChildEnderecoServCliente $l ChildEnderecoServCliente
     * @return $this|\ImaTelecomBundle\Model\ServicoCliente The current object (for fluent API support)
     */
    public function addEnderecoServCliente(ChildEnderecoServCliente $l)
    {
        if ($this->collEnderecoServClientes === null) {
            $this->initEnderecoServClientes();
            $this->collEnderecoServClientesPartial = true;
        }

        if (!$this->collEnderecoServClientes->contains($l)) {
            $this->doAddEnderecoServCliente($l);

            if ($this->enderecoServClientesScheduledForDeletion and $this->enderecoServClientesScheduledForDeletion->contains($l)) {
                $this->enderecoServClientesScheduledForDeletion->remove($this->enderecoServClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEnderecoServCliente $enderecoServCliente The ChildEnderecoServCliente object to add.
     */
    protected function doAddEnderecoServCliente(ChildEnderecoServCliente $enderecoServCliente)
    {
        $this->collEnderecoServClientes[]= $enderecoServCliente;
        $enderecoServCliente->setServicoCliente($this);
    }

    /**
     * @param  ChildEnderecoServCliente $enderecoServCliente The ChildEnderecoServCliente object to remove.
     * @return $this|ChildServicoCliente The current object (for fluent API support)
     */
    public function removeEnderecoServCliente(ChildEnderecoServCliente $enderecoServCliente)
    {
        if ($this->getEnderecoServClientes()->contains($enderecoServCliente)) {
            $pos = $this->collEnderecoServClientes->search($enderecoServCliente);
            $this->collEnderecoServClientes->remove($pos);
            if (null === $this->enderecoServClientesScheduledForDeletion) {
                $this->enderecoServClientesScheduledForDeletion = clone $this->collEnderecoServClientes;
                $this->enderecoServClientesScheduledForDeletion->clear();
            }
            $this->enderecoServClientesScheduledForDeletion[]= clone $enderecoServCliente;
            $enderecoServCliente->setServicoCliente(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ServicoCliente is new, it will return
     * an empty collection; or if this ServicoCliente has previously
     * been saved, it will retrieve related EnderecoServClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ServicoCliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoServCliente[] List of ChildEnderecoServCliente objects
     */
    public function getEnderecoServClientesJoinCidade(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoServClienteQuery::create(null, $criteria);
        $query->joinWith('Cidade', $joinBehavior);

        return $this->getEnderecoServClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ServicoCliente is new, it will return
     * an empty collection; or if this ServicoCliente has previously
     * been saved, it will retrieve related EnderecoServClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ServicoCliente.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoServCliente[] List of ChildEnderecoServCliente objects
     */
    public function getEnderecoServClientesJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoServClienteQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getEnderecoServClientes($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCliente) {
            $this->aCliente->removeServicoCliente($this);
        }
        if (null !== $this->aContrato) {
            $this->aContrato->removeServicoCliente($this);
        }
        if (null !== $this->aServicoPrestado) {
            $this->aServicoPrestado->removeServicoCliente($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeServicoCliente($this);
        }
        $this->idservico_cliente = null;
        $this->servico_prestado_id = null;
        $this->cliente_id = null;
        $this->cod_cesta = null;
        $this->data_contratado = null;
        $this->data_cancelado = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->import_id = null;
        $this->descricao_nota_fiscal = null;
        $this->contrato_id = null;
        $this->tipo_periodo_referencia = null;
        $this->valor_desconto = null;
        $this->valor = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBoletoItems) {
                foreach ($this->collBoletoItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEnderecoServClientes) {
                foreach ($this->collEnderecoServClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBoletoItems = null;
        $this->collEnderecoServClientes = null;
        $this->aCliente = null;
        $this->aContrato = null;
        $this->aServicoPrestado = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ServicoClienteTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
