<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\SysPerfil as ChildSysPerfil;
use ImaTelecomBundle\Model\SysPerfilQuery as ChildSysPerfilQuery;
use ImaTelecomBundle\Model\Map\SysPerfilTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sys_perfil' table.
 *
 *
 *
 * @method     ChildSysPerfilQuery orderByIdsysPerfil($order = Criteria::ASC) Order by the idsys_perfil column
 * @method     ChildSysPerfilQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildSysPerfilQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildSysPerfilQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildSysPerfilQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildSysPerfilQuery groupByIdsysPerfil() Group by the idsys_perfil column
 * @method     ChildSysPerfilQuery groupByNome() Group by the nome column
 * @method     ChildSysPerfilQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildSysPerfilQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildSysPerfilQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildSysPerfilQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSysPerfilQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSysPerfilQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSysPerfilQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSysPerfilQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSysPerfilQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSysPerfilQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildSysPerfilQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildSysPerfilQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildSysPerfilQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildSysPerfilQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildSysPerfilQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildSysPerfilQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildSysPerfilQuery leftJoinSysPerfilProcesso($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysPerfilProcesso relation
 * @method     ChildSysPerfilQuery rightJoinSysPerfilProcesso($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysPerfilProcesso relation
 * @method     ChildSysPerfilQuery innerJoinSysPerfilProcesso($relationAlias = null) Adds a INNER JOIN clause to the query using the SysPerfilProcesso relation
 *
 * @method     ChildSysPerfilQuery joinWithSysPerfilProcesso($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SysPerfilProcesso relation
 *
 * @method     ChildSysPerfilQuery leftJoinWithSysPerfilProcesso() Adds a LEFT JOIN clause and with to the query using the SysPerfilProcesso relation
 * @method     ChildSysPerfilQuery rightJoinWithSysPerfilProcesso() Adds a RIGHT JOIN clause and with to the query using the SysPerfilProcesso relation
 * @method     ChildSysPerfilQuery innerJoinWithSysPerfilProcesso() Adds a INNER JOIN clause and with to the query using the SysPerfilProcesso relation
 *
 * @method     ChildSysPerfilQuery leftJoinSysUsuarioPerfil($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsuarioPerfil relation
 * @method     ChildSysPerfilQuery rightJoinSysUsuarioPerfil($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsuarioPerfil relation
 * @method     ChildSysPerfilQuery innerJoinSysUsuarioPerfil($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsuarioPerfil relation
 *
 * @method     ChildSysPerfilQuery joinWithSysUsuarioPerfil($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SysUsuarioPerfil relation
 *
 * @method     ChildSysPerfilQuery leftJoinWithSysUsuarioPerfil() Adds a LEFT JOIN clause and with to the query using the SysUsuarioPerfil relation
 * @method     ChildSysPerfilQuery rightJoinWithSysUsuarioPerfil() Adds a RIGHT JOIN clause and with to the query using the SysUsuarioPerfil relation
 * @method     ChildSysPerfilQuery innerJoinWithSysUsuarioPerfil() Adds a INNER JOIN clause and with to the query using the SysUsuarioPerfil relation
 *
 * @method     \ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\SysPerfilProcessoQuery|\ImaTelecomBundle\Model\SysUsuarioPerfilQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSysPerfil findOne(ConnectionInterface $con = null) Return the first ChildSysPerfil matching the query
 * @method     ChildSysPerfil findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSysPerfil matching the query, or a new ChildSysPerfil object populated from the query conditions when no match is found
 *
 * @method     ChildSysPerfil findOneByIdsysPerfil(int $idsys_perfil) Return the first ChildSysPerfil filtered by the idsys_perfil column
 * @method     ChildSysPerfil findOneByNome(string $nome) Return the first ChildSysPerfil filtered by the nome column
 * @method     ChildSysPerfil findOneByDataCadastro(string $data_cadastro) Return the first ChildSysPerfil filtered by the data_cadastro column
 * @method     ChildSysPerfil findOneByDataAlterado(string $data_alterado) Return the first ChildSysPerfil filtered by the data_alterado column
 * @method     ChildSysPerfil findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildSysPerfil filtered by the usuario_alterado column *

 * @method     ChildSysPerfil requirePk($key, ConnectionInterface $con = null) Return the ChildSysPerfil by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSysPerfil requireOne(ConnectionInterface $con = null) Return the first ChildSysPerfil matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSysPerfil requireOneByIdsysPerfil(int $idsys_perfil) Return the first ChildSysPerfil filtered by the idsys_perfil column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSysPerfil requireOneByNome(string $nome) Return the first ChildSysPerfil filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSysPerfil requireOneByDataCadastro(string $data_cadastro) Return the first ChildSysPerfil filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSysPerfil requireOneByDataAlterado(string $data_alterado) Return the first ChildSysPerfil filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSysPerfil requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildSysPerfil filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSysPerfil[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSysPerfil objects based on current ModelCriteria
 * @method     ChildSysPerfil[]|ObjectCollection findByIdsysPerfil(int $idsys_perfil) Return ChildSysPerfil objects filtered by the idsys_perfil column
 * @method     ChildSysPerfil[]|ObjectCollection findByNome(string $nome) Return ChildSysPerfil objects filtered by the nome column
 * @method     ChildSysPerfil[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildSysPerfil objects filtered by the data_cadastro column
 * @method     ChildSysPerfil[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildSysPerfil objects filtered by the data_alterado column
 * @method     ChildSysPerfil[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildSysPerfil objects filtered by the usuario_alterado column
 * @method     ChildSysPerfil[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SysPerfilQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\SysPerfilQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\SysPerfil', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSysPerfilQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSysPerfilQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSysPerfilQuery) {
            return $criteria;
        }
        $query = new ChildSysPerfilQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSysPerfil|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SysPerfilTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SysPerfilTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSysPerfil A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idsys_perfil, nome, data_cadastro, data_alterado, usuario_alterado FROM sys_perfil WHERE idsys_perfil = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSysPerfil $obj */
            $obj = new ChildSysPerfil();
            $obj->hydrate($row);
            SysPerfilTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSysPerfil|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SysPerfilTableMap::COL_IDSYS_PERFIL, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SysPerfilTableMap::COL_IDSYS_PERFIL, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idsys_perfil column
     *
     * Example usage:
     * <code>
     * $query->filterByIdsysPerfil(1234); // WHERE idsys_perfil = 1234
     * $query->filterByIdsysPerfil(array(12, 34)); // WHERE idsys_perfil IN (12, 34)
     * $query->filterByIdsysPerfil(array('min' => 12)); // WHERE idsys_perfil > 12
     * </code>
     *
     * @param     mixed $idsysPerfil The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterByIdsysPerfil($idsysPerfil = null, $comparison = null)
    {
        if (is_array($idsysPerfil)) {
            $useMinMax = false;
            if (isset($idsysPerfil['min'])) {
                $this->addUsingAlias(SysPerfilTableMap::COL_IDSYS_PERFIL, $idsysPerfil['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idsysPerfil['max'])) {
                $this->addUsingAlias(SysPerfilTableMap::COL_IDSYS_PERFIL, $idsysPerfil['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysPerfilTableMap::COL_IDSYS_PERFIL, $idsysPerfil, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysPerfilTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(SysPerfilTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(SysPerfilTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysPerfilTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(SysPerfilTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(SysPerfilTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysPerfilTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(SysPerfilTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(SysPerfilTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SysPerfilTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(SysPerfilTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SysPerfilTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\SysPerfilProcesso object
     *
     * @param \ImaTelecomBundle\Model\SysPerfilProcesso|ObjectCollection $sysPerfilProcesso the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterBySysPerfilProcesso($sysPerfilProcesso, $comparison = null)
    {
        if ($sysPerfilProcesso instanceof \ImaTelecomBundle\Model\SysPerfilProcesso) {
            return $this
                ->addUsingAlias(SysPerfilTableMap::COL_IDSYS_PERFIL, $sysPerfilProcesso->getPerfilId(), $comparison);
        } elseif ($sysPerfilProcesso instanceof ObjectCollection) {
            return $this
                ->useSysPerfilProcessoQuery()
                ->filterByPrimaryKeys($sysPerfilProcesso->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysPerfilProcesso() only accepts arguments of type \ImaTelecomBundle\Model\SysPerfilProcesso or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysPerfilProcesso relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function joinSysPerfilProcesso($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysPerfilProcesso');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysPerfilProcesso');
        }

        return $this;
    }

    /**
     * Use the SysPerfilProcesso relation SysPerfilProcesso object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\SysPerfilProcessoQuery A secondary query class using the current class as primary query
     */
    public function useSysPerfilProcessoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSysPerfilProcesso($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysPerfilProcesso', '\ImaTelecomBundle\Model\SysPerfilProcessoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\SysUsuarioPerfil object
     *
     * @param \ImaTelecomBundle\Model\SysUsuarioPerfil|ObjectCollection $sysUsuarioPerfil the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSysPerfilQuery The current query, for fluid interface
     */
    public function filterBySysUsuarioPerfil($sysUsuarioPerfil, $comparison = null)
    {
        if ($sysUsuarioPerfil instanceof \ImaTelecomBundle\Model\SysUsuarioPerfil) {
            return $this
                ->addUsingAlias(SysPerfilTableMap::COL_IDSYS_PERFIL, $sysUsuarioPerfil->getPerfilId(), $comparison);
        } elseif ($sysUsuarioPerfil instanceof ObjectCollection) {
            return $this
                ->useSysUsuarioPerfilQuery()
                ->filterByPrimaryKeys($sysUsuarioPerfil->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysUsuarioPerfil() only accepts arguments of type \ImaTelecomBundle\Model\SysUsuarioPerfil or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsuarioPerfil relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function joinSysUsuarioPerfil($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsuarioPerfil');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsuarioPerfil');
        }

        return $this;
    }

    /**
     * Use the SysUsuarioPerfil relation SysUsuarioPerfil object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\SysUsuarioPerfilQuery A secondary query class using the current class as primary query
     */
    public function useSysUsuarioPerfilQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSysUsuarioPerfil($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsuarioPerfil', '\ImaTelecomBundle\Model\SysUsuarioPerfilQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSysPerfil $sysPerfil Object to remove from the list of results
     *
     * @return $this|ChildSysPerfilQuery The current query, for fluid interface
     */
    public function prune($sysPerfil = null)
    {
        if ($sysPerfil) {
            $this->addUsingAlias(SysPerfilTableMap::COL_IDSYS_PERFIL, $sysPerfil->getIdsysPerfil(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sys_perfil table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SysPerfilTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SysPerfilTableMap::clearInstancePool();
            SysPerfilTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SysPerfilTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SysPerfilTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SysPerfilTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SysPerfilTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SysPerfilQuery
