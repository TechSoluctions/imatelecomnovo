<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ContasPagar as ChildContasPagar;
use ImaTelecomBundle\Model\ContasPagarParcelas as ChildContasPagarParcelas;
use ImaTelecomBundle\Model\ContasPagarParcelasQuery as ChildContasPagarParcelasQuery;
use ImaTelecomBundle\Model\ContasPagarQuery as ChildContasPagarQuery;
use ImaTelecomBundle\Model\ContasPagarTributos as ChildContasPagarTributos;
use ImaTelecomBundle\Model\ContasPagarTributosQuery as ChildContasPagarTributosQuery;
use ImaTelecomBundle\Model\Fornecedor as ChildFornecedor;
use ImaTelecomBundle\Model\FornecedorQuery as ChildFornecedorQuery;
use ImaTelecomBundle\Model\ItemComprado as ChildItemComprado;
use ImaTelecomBundle\Model\ItemCompradoQuery as ChildItemCompradoQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\ContasPagarParcelasTableMap;
use ImaTelecomBundle\Model\Map\ContasPagarTableMap;
use ImaTelecomBundle\Model\Map\ContasPagarTributosTableMap;
use ImaTelecomBundle\Model\Map\ItemCompradoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'contas_pagar' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class ContasPagar implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\ContasPagarTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idcontas_pagar field.
     *
     * @var        int
     */
    protected $idcontas_pagar;

    /**
     * The value for the valor field.
     *
     * @var        double
     */
    protected $valor;

    /**
     * The value for the valor_frete field.
     *
     * Note: this column has a database default value of: 0.0
     * @var        double
     */
    protected $valor_frete;

    /**
     * The value for the fornecedor_id field.
     *
     * @var        int
     */
    protected $fornecedor_id;

    /**
     * The value for the num_documento field.
     *
     * @var        string
     */
    protected $num_documento;

    /**
     * The value for the juros field.
     *
     * @var        double
     */
    protected $juros;

    /**
     * The value for the multa field.
     *
     * @var        double
     */
    protected $multa;

    /**
     * The value for the data_emissao field.
     *
     * @var        DateTime
     */
    protected $data_emissao;

    /**
     * The value for the tipo field.
     *
     * @var        string
     */
    protected $tipo;

    /**
     * The value for the especie field.
     *
     * @var        string
     */
    protected $especie;

    /**
     * The value for the arquivo field.
     *
     * @var        string
     */
    protected $arquivo;

    /**
     * The value for the obs field.
     *
     * @var        string
     */
    protected $obs;

    /**
     * The value for the ativo field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $ativo;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * @var        ChildFornecedor
     */
    protected $aFornecedor;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildContasPagarParcelas[] Collection to store aggregation of ChildContasPagarParcelas objects.
     */
    protected $collContasPagarParcelass;
    protected $collContasPagarParcelassPartial;

    /**
     * @var        ObjectCollection|ChildContasPagarTributos[] Collection to store aggregation of ChildContasPagarTributos objects.
     */
    protected $collContasPagarTributoss;
    protected $collContasPagarTributossPartial;

    /**
     * @var        ObjectCollection|ChildItemComprado[] Collection to store aggregation of ChildItemComprado objects.
     */
    protected $collItemComprados;
    protected $collItemCompradosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContasPagarParcelas[]
     */
    protected $contasPagarParcelassScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContasPagarTributos[]
     */
    protected $contasPagarTributossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildItemComprado[]
     */
    protected $itemCompradosScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->valor_frete = 0.0;
        $this->ativo = true;
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\ContasPagar object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ContasPagar</code> instance.  If
     * <code>obj</code> is an instance of <code>ContasPagar</code>, delegates to
     * <code>equals(ContasPagar)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ContasPagar The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idcontas_pagar] column value.
     *
     * @return int
     */
    public function getIdcontasPagar()
    {
        return $this->idcontas_pagar;
    }

    /**
     * Get the [valor] column value.
     *
     * @return double
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Get the [valor_frete] column value.
     *
     * @return double
     */
    public function getValorFrete()
    {
        return $this->valor_frete;
    }

    /**
     * Get the [fornecedor_id] column value.
     *
     * @return int
     */
    public function getFornecedorId()
    {
        return $this->fornecedor_id;
    }

    /**
     * Get the [num_documento] column value.
     *
     * @return string
     */
    public function getNumDocumento()
    {
        return $this->num_documento;
    }

    /**
     * Get the [juros] column value.
     *
     * @return double
     */
    public function getJuros()
    {
        return $this->juros;
    }

    /**
     * Get the [multa] column value.
     *
     * @return double
     */
    public function getMulta()
    {
        return $this->multa;
    }

    /**
     * Get the [optionally formatted] temporal [data_emissao] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataEmissao($format = NULL)
    {
        if ($format === null) {
            return $this->data_emissao;
        } else {
            return $this->data_emissao instanceof \DateTimeInterface ? $this->data_emissao->format($format) : null;
        }
    }

    /**
     * Get the [tipo] column value.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Get the [especie] column value.
     *
     * @return string
     */
    public function getEspecie()
    {
        return $this->especie;
    }

    /**
     * Get the [arquivo] column value.
     *
     * @return string
     */
    public function getArquivo()
    {
        return $this->arquivo;
    }

    /**
     * Get the [obs] column value.
     *
     * @return string
     */
    public function getObs()
    {
        return $this->obs;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->getAtivo();
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Set the value of [idcontas_pagar] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setIdcontasPagar($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idcontas_pagar !== $v) {
            $this->idcontas_pagar = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_IDCONTAS_PAGAR] = true;
        }

        return $this;
    } // setIdcontasPagar()

    /**
     * Set the value of [valor] column.
     *
     * @param double $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setValor($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->valor !== $v) {
            $this->valor = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_VALOR] = true;
        }

        return $this;
    } // setValor()

    /**
     * Set the value of [valor_frete] column.
     *
     * @param double $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setValorFrete($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->valor_frete !== $v) {
            $this->valor_frete = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_VALOR_FRETE] = true;
        }

        return $this;
    } // setValorFrete()

    /**
     * Set the value of [fornecedor_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setFornecedorId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->fornecedor_id !== $v) {
            $this->fornecedor_id = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_FORNECEDOR_ID] = true;
        }

        if ($this->aFornecedor !== null && $this->aFornecedor->getIdfornecedor() !== $v) {
            $this->aFornecedor = null;
        }

        return $this;
    } // setFornecedorId()

    /**
     * Set the value of [num_documento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setNumDocumento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->num_documento !== $v) {
            $this->num_documento = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_NUM_DOCUMENTO] = true;
        }

        return $this;
    } // setNumDocumento()

    /**
     * Set the value of [juros] column.
     *
     * @param double $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setJuros($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->juros !== $v) {
            $this->juros = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_JUROS] = true;
        }

        return $this;
    } // setJuros()

    /**
     * Set the value of [multa] column.
     *
     * @param double $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setMulta($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->multa !== $v) {
            $this->multa = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_MULTA] = true;
        }

        return $this;
    } // setMulta()

    /**
     * Sets the value of [data_emissao] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setDataEmissao($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_emissao !== null || $dt !== null) {
            if ($this->data_emissao === null || $dt === null || $dt->format("Y-m-d") !== $this->data_emissao->format("Y-m-d")) {
                $this->data_emissao = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContasPagarTableMap::COL_DATA_EMISSAO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataEmissao()

    /**
     * Set the value of [tipo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setTipo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo !== $v) {
            $this->tipo = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_TIPO] = true;
        }

        return $this;
    } // setTipo()

    /**
     * Set the value of [especie] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setEspecie($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->especie !== $v) {
            $this->especie = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_ESPECIE] = true;
        }

        return $this;
    } // setEspecie()

    /**
     * Set the value of [arquivo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setArquivo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->arquivo !== $v) {
            $this->arquivo = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_ARQUIVO] = true;
        }

        return $this;
    } // setArquivo()

    /**
     * Set the value of [obs] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setObs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->obs !== $v) {
            $this->obs = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_OBS] = true;
        }

        return $this;
    } // setObs()

    /**
     * Sets the value of the [ativo] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setAtivo($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ativo !== $v) {
            $this->ativo = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_ATIVO] = true;
        }

        return $this;
    } // setAtivo()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContasPagarTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContasPagarTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[ContasPagarTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->valor_frete !== 0.0) {
                return false;
            }

            if ($this->ativo !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ContasPagarTableMap::translateFieldName('IdcontasPagar', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idcontas_pagar = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ContasPagarTableMap::translateFieldName('Valor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ContasPagarTableMap::translateFieldName('ValorFrete', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_frete = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ContasPagarTableMap::translateFieldName('FornecedorId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fornecedor_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ContasPagarTableMap::translateFieldName('NumDocumento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->num_documento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ContasPagarTableMap::translateFieldName('Juros', TableMap::TYPE_PHPNAME, $indexType)];
            $this->juros = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ContasPagarTableMap::translateFieldName('Multa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->multa = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ContasPagarTableMap::translateFieldName('DataEmissao', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->data_emissao = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ContasPagarTableMap::translateFieldName('Tipo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ContasPagarTableMap::translateFieldName('Especie', TableMap::TYPE_PHPNAME, $indexType)];
            $this->especie = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ContasPagarTableMap::translateFieldName('Arquivo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->arquivo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ContasPagarTableMap::translateFieldName('Obs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->obs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ContasPagarTableMap::translateFieldName('Ativo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ativo = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ContasPagarTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ContasPagarTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ContasPagarTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = ContasPagarTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\ContasPagar'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aFornecedor !== null && $this->fornecedor_id !== $this->aFornecedor->getIdfornecedor()) {
            $this->aFornecedor = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContasPagarTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildContasPagarQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aFornecedor = null;
            $this->aUsuario = null;
            $this->collContasPagarParcelass = null;

            $this->collContasPagarTributoss = null;

            $this->collItemComprados = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ContasPagar::setDeleted()
     * @see ContasPagar::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildContasPagarQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ContasPagarTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFornecedor !== null) {
                if ($this->aFornecedor->isModified() || $this->aFornecedor->isNew()) {
                    $affectedRows += $this->aFornecedor->save($con);
                }
                $this->setFornecedor($this->aFornecedor);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->contasPagarParcelassScheduledForDeletion !== null) {
                if (!$this->contasPagarParcelassScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ContasPagarParcelasQuery::create()
                        ->filterByPrimaryKeys($this->contasPagarParcelassScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contasPagarParcelassScheduledForDeletion = null;
                }
            }

            if ($this->collContasPagarParcelass !== null) {
                foreach ($this->collContasPagarParcelass as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->contasPagarTributossScheduledForDeletion !== null) {
                if (!$this->contasPagarTributossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ContasPagarTributosQuery::create()
                        ->filterByPrimaryKeys($this->contasPagarTributossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contasPagarTributossScheduledForDeletion = null;
                }
            }

            if ($this->collContasPagarTributoss !== null) {
                foreach ($this->collContasPagarTributoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->itemCompradosScheduledForDeletion !== null) {
                if (!$this->itemCompradosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ItemCompradoQuery::create()
                        ->filterByPrimaryKeys($this->itemCompradosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->itemCompradosScheduledForDeletion = null;
                }
            }

            if ($this->collItemComprados !== null) {
                foreach ($this->collItemComprados as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ContasPagarTableMap::COL_IDCONTAS_PAGAR] = true;
        if (null !== $this->idcontas_pagar) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ContasPagarTableMap::COL_IDCONTAS_PAGAR . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ContasPagarTableMap::COL_IDCONTAS_PAGAR)) {
            $modifiedColumns[':p' . $index++]  = 'idcontas_pagar';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_VALOR)) {
            $modifiedColumns[':p' . $index++]  = 'valor';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_VALOR_FRETE)) {
            $modifiedColumns[':p' . $index++]  = 'valor_frete';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_FORNECEDOR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'fornecedor_id';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_NUM_DOCUMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'num_documento';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_JUROS)) {
            $modifiedColumns[':p' . $index++]  = 'juros';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_MULTA)) {
            $modifiedColumns[':p' . $index++]  = 'multa';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_DATA_EMISSAO)) {
            $modifiedColumns[':p' . $index++]  = 'data_emissao';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_TIPO)) {
            $modifiedColumns[':p' . $index++]  = 'tipo';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_ESPECIE)) {
            $modifiedColumns[':p' . $index++]  = 'especie';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_ARQUIVO)) {
            $modifiedColumns[':p' . $index++]  = 'arquivo';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_OBS)) {
            $modifiedColumns[':p' . $index++]  = 'obs';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_ATIVO)) {
            $modifiedColumns[':p' . $index++]  = 'ativo';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }

        $sql = sprintf(
            'INSERT INTO contas_pagar (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idcontas_pagar':
                        $stmt->bindValue($identifier, $this->idcontas_pagar, PDO::PARAM_INT);
                        break;
                    case 'valor':
                        $stmt->bindValue($identifier, $this->valor, PDO::PARAM_STR);
                        break;
                    case 'valor_frete':
                        $stmt->bindValue($identifier, $this->valor_frete, PDO::PARAM_STR);
                        break;
                    case 'fornecedor_id':
                        $stmt->bindValue($identifier, $this->fornecedor_id, PDO::PARAM_INT);
                        break;
                    case 'num_documento':
                        $stmt->bindValue($identifier, $this->num_documento, PDO::PARAM_STR);
                        break;
                    case 'juros':
                        $stmt->bindValue($identifier, $this->juros, PDO::PARAM_STR);
                        break;
                    case 'multa':
                        $stmt->bindValue($identifier, $this->multa, PDO::PARAM_STR);
                        break;
                    case 'data_emissao':
                        $stmt->bindValue($identifier, $this->data_emissao ? $this->data_emissao->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'tipo':
                        $stmt->bindValue($identifier, $this->tipo, PDO::PARAM_STR);
                        break;
                    case 'especie':
                        $stmt->bindValue($identifier, $this->especie, PDO::PARAM_STR);
                        break;
                    case 'arquivo':
                        $stmt->bindValue($identifier, $this->arquivo, PDO::PARAM_STR);
                        break;
                    case 'obs':
                        $stmt->bindValue($identifier, $this->obs, PDO::PARAM_STR);
                        break;
                    case 'ativo':
                        $stmt->bindValue($identifier, (int) $this->ativo, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdcontasPagar($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContasPagarTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdcontasPagar();
                break;
            case 1:
                return $this->getValor();
                break;
            case 2:
                return $this->getValorFrete();
                break;
            case 3:
                return $this->getFornecedorId();
                break;
            case 4:
                return $this->getNumDocumento();
                break;
            case 5:
                return $this->getJuros();
                break;
            case 6:
                return $this->getMulta();
                break;
            case 7:
                return $this->getDataEmissao();
                break;
            case 8:
                return $this->getTipo();
                break;
            case 9:
                return $this->getEspecie();
                break;
            case 10:
                return $this->getArquivo();
                break;
            case 11:
                return $this->getObs();
                break;
            case 12:
                return $this->getAtivo();
                break;
            case 13:
                return $this->getDataCadastro();
                break;
            case 14:
                return $this->getDataAlterado();
                break;
            case 15:
                return $this->getUsuarioAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ContasPagar'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ContasPagar'][$this->hashCode()] = true;
        $keys = ContasPagarTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdcontasPagar(),
            $keys[1] => $this->getValor(),
            $keys[2] => $this->getValorFrete(),
            $keys[3] => $this->getFornecedorId(),
            $keys[4] => $this->getNumDocumento(),
            $keys[5] => $this->getJuros(),
            $keys[6] => $this->getMulta(),
            $keys[7] => $this->getDataEmissao(),
            $keys[8] => $this->getTipo(),
            $keys[9] => $this->getEspecie(),
            $keys[10] => $this->getArquivo(),
            $keys[11] => $this->getObs(),
            $keys[12] => $this->getAtivo(),
            $keys[13] => $this->getDataCadastro(),
            $keys[14] => $this->getDataAlterado(),
            $keys[15] => $this->getUsuarioAlterado(),
        );
        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aFornecedor) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'fornecedor';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'fornecedor';
                        break;
                    default:
                        $key = 'Fornecedor';
                }

                $result[$key] = $this->aFornecedor->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collContasPagarParcelass) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contasPagarParcelass';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contas_pagar_parcelass';
                        break;
                    default:
                        $key = 'ContasPagarParcelass';
                }

                $result[$key] = $this->collContasPagarParcelass->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collContasPagarTributoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contasPagarTributoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contas_pagar_tributoss';
                        break;
                    default:
                        $key = 'ContasPagarTributoss';
                }

                $result[$key] = $this->collContasPagarTributoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collItemComprados) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'itemComprados';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'item_comprados';
                        break;
                    default:
                        $key = 'ItemComprados';
                }

                $result[$key] = $this->collItemComprados->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\ContasPagar
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContasPagarTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\ContasPagar
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdcontasPagar($value);
                break;
            case 1:
                $this->setValor($value);
                break;
            case 2:
                $this->setValorFrete($value);
                break;
            case 3:
                $this->setFornecedorId($value);
                break;
            case 4:
                $this->setNumDocumento($value);
                break;
            case 5:
                $this->setJuros($value);
                break;
            case 6:
                $this->setMulta($value);
                break;
            case 7:
                $this->setDataEmissao($value);
                break;
            case 8:
                $this->setTipo($value);
                break;
            case 9:
                $this->setEspecie($value);
                break;
            case 10:
                $this->setArquivo($value);
                break;
            case 11:
                $this->setObs($value);
                break;
            case 12:
                $this->setAtivo($value);
                break;
            case 13:
                $this->setDataCadastro($value);
                break;
            case 14:
                $this->setDataAlterado($value);
                break;
            case 15:
                $this->setUsuarioAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ContasPagarTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdcontasPagar($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setValor($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setValorFrete($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setFornecedorId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setNumDocumento($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setJuros($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setMulta($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDataEmissao($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTipo($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setEspecie($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setArquivo($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setObs($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setAtivo($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setDataCadastro($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setDataAlterado($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[15]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ContasPagarTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ContasPagarTableMap::COL_IDCONTAS_PAGAR)) {
            $criteria->add(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $this->idcontas_pagar);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_VALOR)) {
            $criteria->add(ContasPagarTableMap::COL_VALOR, $this->valor);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_VALOR_FRETE)) {
            $criteria->add(ContasPagarTableMap::COL_VALOR_FRETE, $this->valor_frete);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_FORNECEDOR_ID)) {
            $criteria->add(ContasPagarTableMap::COL_FORNECEDOR_ID, $this->fornecedor_id);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_NUM_DOCUMENTO)) {
            $criteria->add(ContasPagarTableMap::COL_NUM_DOCUMENTO, $this->num_documento);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_JUROS)) {
            $criteria->add(ContasPagarTableMap::COL_JUROS, $this->juros);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_MULTA)) {
            $criteria->add(ContasPagarTableMap::COL_MULTA, $this->multa);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_DATA_EMISSAO)) {
            $criteria->add(ContasPagarTableMap::COL_DATA_EMISSAO, $this->data_emissao);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_TIPO)) {
            $criteria->add(ContasPagarTableMap::COL_TIPO, $this->tipo);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_ESPECIE)) {
            $criteria->add(ContasPagarTableMap::COL_ESPECIE, $this->especie);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_ARQUIVO)) {
            $criteria->add(ContasPagarTableMap::COL_ARQUIVO, $this->arquivo);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_OBS)) {
            $criteria->add(ContasPagarTableMap::COL_OBS, $this->obs);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_ATIVO)) {
            $criteria->add(ContasPagarTableMap::COL_ATIVO, $this->ativo);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(ContasPagarTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(ContasPagarTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(ContasPagarTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(ContasPagarTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildContasPagarQuery::create();
        $criteria->add(ContasPagarTableMap::COL_IDCONTAS_PAGAR, $this->idcontas_pagar);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdcontasPagar();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdcontasPagar();
    }

    /**
     * Generic method to set the primary key (idcontas_pagar column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdcontasPagar($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdcontasPagar();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\ContasPagar (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setValor($this->getValor());
        $copyObj->setValorFrete($this->getValorFrete());
        $copyObj->setFornecedorId($this->getFornecedorId());
        $copyObj->setNumDocumento($this->getNumDocumento());
        $copyObj->setJuros($this->getJuros());
        $copyObj->setMulta($this->getMulta());
        $copyObj->setDataEmissao($this->getDataEmissao());
        $copyObj->setTipo($this->getTipo());
        $copyObj->setEspecie($this->getEspecie());
        $copyObj->setArquivo($this->getArquivo());
        $copyObj->setObs($this->getObs());
        $copyObj->setAtivo($this->getAtivo());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getContasPagarParcelass() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContasPagarParcelas($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getContasPagarTributoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContasPagarTributos($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getItemComprados() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addItemComprado($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdcontasPagar(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\ContasPagar Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildFornecedor object.
     *
     * @param  ChildFornecedor $v
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFornecedor(ChildFornecedor $v = null)
    {
        if ($v === null) {
            $this->setFornecedorId(NULL);
        } else {
            $this->setFornecedorId($v->getIdfornecedor());
        }

        $this->aFornecedor = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildFornecedor object, it will not be re-added.
        if ($v !== null) {
            $v->addContasPagar($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildFornecedor object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildFornecedor The associated ChildFornecedor object.
     * @throws PropelException
     */
    public function getFornecedor(ConnectionInterface $con = null)
    {
        if ($this->aFornecedor === null && ($this->fornecedor_id !== null)) {
            $this->aFornecedor = ChildFornecedorQuery::create()->findPk($this->fornecedor_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aFornecedor->addContasPagars($this);
             */
        }

        return $this->aFornecedor;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addContasPagar($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addContasPagars($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ContasPagarParcelas' == $relationName) {
            return $this->initContasPagarParcelass();
        }
        if ('ContasPagarTributos' == $relationName) {
            return $this->initContasPagarTributoss();
        }
        if ('ItemComprado' == $relationName) {
            return $this->initItemComprados();
        }
    }

    /**
     * Clears out the collContasPagarParcelass collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContasPagarParcelass()
     */
    public function clearContasPagarParcelass()
    {
        $this->collContasPagarParcelass = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContasPagarParcelass collection loaded partially.
     */
    public function resetPartialContasPagarParcelass($v = true)
    {
        $this->collContasPagarParcelassPartial = $v;
    }

    /**
     * Initializes the collContasPagarParcelass collection.
     *
     * By default this just sets the collContasPagarParcelass collection to an empty array (like clearcollContasPagarParcelass());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContasPagarParcelass($overrideExisting = true)
    {
        if (null !== $this->collContasPagarParcelass && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContasPagarParcelasTableMap::getTableMap()->getCollectionClassName();

        $this->collContasPagarParcelass = new $collectionClassName;
        $this->collContasPagarParcelass->setModel('\ImaTelecomBundle\Model\ContasPagarParcelas');
    }

    /**
     * Gets an array of ChildContasPagarParcelas objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildContasPagar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContasPagarParcelas[] List of ChildContasPagarParcelas objects
     * @throws PropelException
     */
    public function getContasPagarParcelass(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarParcelassPartial && !$this->isNew();
        if (null === $this->collContasPagarParcelass || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContasPagarParcelass) {
                // return empty collection
                $this->initContasPagarParcelass();
            } else {
                $collContasPagarParcelass = ChildContasPagarParcelasQuery::create(null, $criteria)
                    ->filterByContasPagar($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContasPagarParcelassPartial && count($collContasPagarParcelass)) {
                        $this->initContasPagarParcelass(false);

                        foreach ($collContasPagarParcelass as $obj) {
                            if (false == $this->collContasPagarParcelass->contains($obj)) {
                                $this->collContasPagarParcelass->append($obj);
                            }
                        }

                        $this->collContasPagarParcelassPartial = true;
                    }

                    return $collContasPagarParcelass;
                }

                if ($partial && $this->collContasPagarParcelass) {
                    foreach ($this->collContasPagarParcelass as $obj) {
                        if ($obj->isNew()) {
                            $collContasPagarParcelass[] = $obj;
                        }
                    }
                }

                $this->collContasPagarParcelass = $collContasPagarParcelass;
                $this->collContasPagarParcelassPartial = false;
            }
        }

        return $this->collContasPagarParcelass;
    }

    /**
     * Sets a collection of ChildContasPagarParcelas objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contasPagarParcelass A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildContasPagar The current object (for fluent API support)
     */
    public function setContasPagarParcelass(Collection $contasPagarParcelass, ConnectionInterface $con = null)
    {
        /** @var ChildContasPagarParcelas[] $contasPagarParcelassToDelete */
        $contasPagarParcelassToDelete = $this->getContasPagarParcelass(new Criteria(), $con)->diff($contasPagarParcelass);


        $this->contasPagarParcelassScheduledForDeletion = $contasPagarParcelassToDelete;

        foreach ($contasPagarParcelassToDelete as $contasPagarParcelasRemoved) {
            $contasPagarParcelasRemoved->setContasPagar(null);
        }

        $this->collContasPagarParcelass = null;
        foreach ($contasPagarParcelass as $contasPagarParcelas) {
            $this->addContasPagarParcelas($contasPagarParcelas);
        }

        $this->collContasPagarParcelass = $contasPagarParcelass;
        $this->collContasPagarParcelassPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ContasPagarParcelas objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ContasPagarParcelas objects.
     * @throws PropelException
     */
    public function countContasPagarParcelass(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarParcelassPartial && !$this->isNew();
        if (null === $this->collContasPagarParcelass || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContasPagarParcelass) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContasPagarParcelass());
            }

            $query = ChildContasPagarParcelasQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByContasPagar($this)
                ->count($con);
        }

        return count($this->collContasPagarParcelass);
    }

    /**
     * Method called to associate a ChildContasPagarParcelas object to this object
     * through the ChildContasPagarParcelas foreign key attribute.
     *
     * @param  ChildContasPagarParcelas $l ChildContasPagarParcelas
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function addContasPagarParcelas(ChildContasPagarParcelas $l)
    {
        if ($this->collContasPagarParcelass === null) {
            $this->initContasPagarParcelass();
            $this->collContasPagarParcelassPartial = true;
        }

        if (!$this->collContasPagarParcelass->contains($l)) {
            $this->doAddContasPagarParcelas($l);

            if ($this->contasPagarParcelassScheduledForDeletion and $this->contasPagarParcelassScheduledForDeletion->contains($l)) {
                $this->contasPagarParcelassScheduledForDeletion->remove($this->contasPagarParcelassScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContasPagarParcelas $contasPagarParcelas The ChildContasPagarParcelas object to add.
     */
    protected function doAddContasPagarParcelas(ChildContasPagarParcelas $contasPagarParcelas)
    {
        $this->collContasPagarParcelass[]= $contasPagarParcelas;
        $contasPagarParcelas->setContasPagar($this);
    }

    /**
     * @param  ChildContasPagarParcelas $contasPagarParcelas The ChildContasPagarParcelas object to remove.
     * @return $this|ChildContasPagar The current object (for fluent API support)
     */
    public function removeContasPagarParcelas(ChildContasPagarParcelas $contasPagarParcelas)
    {
        if ($this->getContasPagarParcelass()->contains($contasPagarParcelas)) {
            $pos = $this->collContasPagarParcelass->search($contasPagarParcelas);
            $this->collContasPagarParcelass->remove($pos);
            if (null === $this->contasPagarParcelassScheduledForDeletion) {
                $this->contasPagarParcelassScheduledForDeletion = clone $this->collContasPagarParcelass;
                $this->contasPagarParcelassScheduledForDeletion->clear();
            }
            $this->contasPagarParcelassScheduledForDeletion[]= clone $contasPagarParcelas;
            $contasPagarParcelas->setContasPagar(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ContasPagar is new, it will return
     * an empty collection; or if this ContasPagar has previously
     * been saved, it will retrieve related ContasPagarParcelass from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ContasPagar.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContasPagarParcelas[] List of ChildContasPagarParcelas objects
     */
    public function getContasPagarParcelassJoinCaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContasPagarParcelasQuery::create(null, $criteria);
        $query->joinWith('Caixa', $joinBehavior);

        return $this->getContasPagarParcelass($query, $con);
    }

    /**
     * Clears out the collContasPagarTributoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContasPagarTributoss()
     */
    public function clearContasPagarTributoss()
    {
        $this->collContasPagarTributoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContasPagarTributoss collection loaded partially.
     */
    public function resetPartialContasPagarTributoss($v = true)
    {
        $this->collContasPagarTributossPartial = $v;
    }

    /**
     * Initializes the collContasPagarTributoss collection.
     *
     * By default this just sets the collContasPagarTributoss collection to an empty array (like clearcollContasPagarTributoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContasPagarTributoss($overrideExisting = true)
    {
        if (null !== $this->collContasPagarTributoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContasPagarTributosTableMap::getTableMap()->getCollectionClassName();

        $this->collContasPagarTributoss = new $collectionClassName;
        $this->collContasPagarTributoss->setModel('\ImaTelecomBundle\Model\ContasPagarTributos');
    }

    /**
     * Gets an array of ChildContasPagarTributos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildContasPagar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContasPagarTributos[] List of ChildContasPagarTributos objects
     * @throws PropelException
     */
    public function getContasPagarTributoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarTributossPartial && !$this->isNew();
        if (null === $this->collContasPagarTributoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContasPagarTributoss) {
                // return empty collection
                $this->initContasPagarTributoss();
            } else {
                $collContasPagarTributoss = ChildContasPagarTributosQuery::create(null, $criteria)
                    ->filterByContasPagar($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContasPagarTributossPartial && count($collContasPagarTributoss)) {
                        $this->initContasPagarTributoss(false);

                        foreach ($collContasPagarTributoss as $obj) {
                            if (false == $this->collContasPagarTributoss->contains($obj)) {
                                $this->collContasPagarTributoss->append($obj);
                            }
                        }

                        $this->collContasPagarTributossPartial = true;
                    }

                    return $collContasPagarTributoss;
                }

                if ($partial && $this->collContasPagarTributoss) {
                    foreach ($this->collContasPagarTributoss as $obj) {
                        if ($obj->isNew()) {
                            $collContasPagarTributoss[] = $obj;
                        }
                    }
                }

                $this->collContasPagarTributoss = $collContasPagarTributoss;
                $this->collContasPagarTributossPartial = false;
            }
        }

        return $this->collContasPagarTributoss;
    }

    /**
     * Sets a collection of ChildContasPagarTributos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contasPagarTributoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildContasPagar The current object (for fluent API support)
     */
    public function setContasPagarTributoss(Collection $contasPagarTributoss, ConnectionInterface $con = null)
    {
        /** @var ChildContasPagarTributos[] $contasPagarTributossToDelete */
        $contasPagarTributossToDelete = $this->getContasPagarTributoss(new Criteria(), $con)->diff($contasPagarTributoss);


        $this->contasPagarTributossScheduledForDeletion = $contasPagarTributossToDelete;

        foreach ($contasPagarTributossToDelete as $contasPagarTributosRemoved) {
            $contasPagarTributosRemoved->setContasPagar(null);
        }

        $this->collContasPagarTributoss = null;
        foreach ($contasPagarTributoss as $contasPagarTributos) {
            $this->addContasPagarTributos($contasPagarTributos);
        }

        $this->collContasPagarTributoss = $contasPagarTributoss;
        $this->collContasPagarTributossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ContasPagarTributos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ContasPagarTributos objects.
     * @throws PropelException
     */
    public function countContasPagarTributoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarTributossPartial && !$this->isNew();
        if (null === $this->collContasPagarTributoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContasPagarTributoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContasPagarTributoss());
            }

            $query = ChildContasPagarTributosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByContasPagar($this)
                ->count($con);
        }

        return count($this->collContasPagarTributoss);
    }

    /**
     * Method called to associate a ChildContasPagarTributos object to this object
     * through the ChildContasPagarTributos foreign key attribute.
     *
     * @param  ChildContasPagarTributos $l ChildContasPagarTributos
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function addContasPagarTributos(ChildContasPagarTributos $l)
    {
        if ($this->collContasPagarTributoss === null) {
            $this->initContasPagarTributoss();
            $this->collContasPagarTributossPartial = true;
        }

        if (!$this->collContasPagarTributoss->contains($l)) {
            $this->doAddContasPagarTributos($l);

            if ($this->contasPagarTributossScheduledForDeletion and $this->contasPagarTributossScheduledForDeletion->contains($l)) {
                $this->contasPagarTributossScheduledForDeletion->remove($this->contasPagarTributossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContasPagarTributos $contasPagarTributos The ChildContasPagarTributos object to add.
     */
    protected function doAddContasPagarTributos(ChildContasPagarTributos $contasPagarTributos)
    {
        $this->collContasPagarTributoss[]= $contasPagarTributos;
        $contasPagarTributos->setContasPagar($this);
    }

    /**
     * @param  ChildContasPagarTributos $contasPagarTributos The ChildContasPagarTributos object to remove.
     * @return $this|ChildContasPagar The current object (for fluent API support)
     */
    public function removeContasPagarTributos(ChildContasPagarTributos $contasPagarTributos)
    {
        if ($this->getContasPagarTributoss()->contains($contasPagarTributos)) {
            $pos = $this->collContasPagarTributoss->search($contasPagarTributos);
            $this->collContasPagarTributoss->remove($pos);
            if (null === $this->contasPagarTributossScheduledForDeletion) {
                $this->contasPagarTributossScheduledForDeletion = clone $this->collContasPagarTributoss;
                $this->contasPagarTributossScheduledForDeletion->clear();
            }
            $this->contasPagarTributossScheduledForDeletion[]= clone $contasPagarTributos;
            $contasPagarTributos->setContasPagar(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ContasPagar is new, it will return
     * an empty collection; or if this ContasPagar has previously
     * been saved, it will retrieve related ContasPagarTributoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ContasPagar.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContasPagarTributos[] List of ChildContasPagarTributos objects
     */
    public function getContasPagarTributossJoinTributo(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContasPagarTributosQuery::create(null, $criteria);
        $query->joinWith('Tributo', $joinBehavior);

        return $this->getContasPagarTributoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ContasPagar is new, it will return
     * an empty collection; or if this ContasPagar has previously
     * been saved, it will retrieve related ContasPagarTributoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ContasPagar.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContasPagarTributos[] List of ChildContasPagarTributos objects
     */
    public function getContasPagarTributossJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContasPagarTributosQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getContasPagarTributoss($query, $con);
    }

    /**
     * Clears out the collItemComprados collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addItemComprados()
     */
    public function clearItemComprados()
    {
        $this->collItemComprados = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collItemComprados collection loaded partially.
     */
    public function resetPartialItemComprados($v = true)
    {
        $this->collItemCompradosPartial = $v;
    }

    /**
     * Initializes the collItemComprados collection.
     *
     * By default this just sets the collItemComprados collection to an empty array (like clearcollItemComprados());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initItemComprados($overrideExisting = true)
    {
        if (null !== $this->collItemComprados && !$overrideExisting) {
            return;
        }

        $collectionClassName = ItemCompradoTableMap::getTableMap()->getCollectionClassName();

        $this->collItemComprados = new $collectionClassName;
        $this->collItemComprados->setModel('\ImaTelecomBundle\Model\ItemComprado');
    }

    /**
     * Gets an array of ChildItemComprado objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildContasPagar is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildItemComprado[] List of ChildItemComprado objects
     * @throws PropelException
     */
    public function getItemComprados(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collItemCompradosPartial && !$this->isNew();
        if (null === $this->collItemComprados || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collItemComprados) {
                // return empty collection
                $this->initItemComprados();
            } else {
                $collItemComprados = ChildItemCompradoQuery::create(null, $criteria)
                    ->filterByContasPagar($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collItemCompradosPartial && count($collItemComprados)) {
                        $this->initItemComprados(false);

                        foreach ($collItemComprados as $obj) {
                            if (false == $this->collItemComprados->contains($obj)) {
                                $this->collItemComprados->append($obj);
                            }
                        }

                        $this->collItemCompradosPartial = true;
                    }

                    return $collItemComprados;
                }

                if ($partial && $this->collItemComprados) {
                    foreach ($this->collItemComprados as $obj) {
                        if ($obj->isNew()) {
                            $collItemComprados[] = $obj;
                        }
                    }
                }

                $this->collItemComprados = $collItemComprados;
                $this->collItemCompradosPartial = false;
            }
        }

        return $this->collItemComprados;
    }

    /**
     * Sets a collection of ChildItemComprado objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $itemComprados A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildContasPagar The current object (for fluent API support)
     */
    public function setItemComprados(Collection $itemComprados, ConnectionInterface $con = null)
    {
        /** @var ChildItemComprado[] $itemCompradosToDelete */
        $itemCompradosToDelete = $this->getItemComprados(new Criteria(), $con)->diff($itemComprados);


        $this->itemCompradosScheduledForDeletion = $itemCompradosToDelete;

        foreach ($itemCompradosToDelete as $itemCompradoRemoved) {
            $itemCompradoRemoved->setContasPagar(null);
        }

        $this->collItemComprados = null;
        foreach ($itemComprados as $itemComprado) {
            $this->addItemComprado($itemComprado);
        }

        $this->collItemComprados = $itemComprados;
        $this->collItemCompradosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ItemComprado objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ItemComprado objects.
     * @throws PropelException
     */
    public function countItemComprados(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collItemCompradosPartial && !$this->isNew();
        if (null === $this->collItemComprados || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collItemComprados) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getItemComprados());
            }

            $query = ChildItemCompradoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByContasPagar($this)
                ->count($con);
        }

        return count($this->collItemComprados);
    }

    /**
     * Method called to associate a ChildItemComprado object to this object
     * through the ChildItemComprado foreign key attribute.
     *
     * @param  ChildItemComprado $l ChildItemComprado
     * @return $this|\ImaTelecomBundle\Model\ContasPagar The current object (for fluent API support)
     */
    public function addItemComprado(ChildItemComprado $l)
    {
        if ($this->collItemComprados === null) {
            $this->initItemComprados();
            $this->collItemCompradosPartial = true;
        }

        if (!$this->collItemComprados->contains($l)) {
            $this->doAddItemComprado($l);

            if ($this->itemCompradosScheduledForDeletion and $this->itemCompradosScheduledForDeletion->contains($l)) {
                $this->itemCompradosScheduledForDeletion->remove($this->itemCompradosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildItemComprado $itemComprado The ChildItemComprado object to add.
     */
    protected function doAddItemComprado(ChildItemComprado $itemComprado)
    {
        $this->collItemComprados[]= $itemComprado;
        $itemComprado->setContasPagar($this);
    }

    /**
     * @param  ChildItemComprado $itemComprado The ChildItemComprado object to remove.
     * @return $this|ChildContasPagar The current object (for fluent API support)
     */
    public function removeItemComprado(ChildItemComprado $itemComprado)
    {
        if ($this->getItemComprados()->contains($itemComprado)) {
            $pos = $this->collItemComprados->search($itemComprado);
            $this->collItemComprados->remove($pos);
            if (null === $this->itemCompradosScheduledForDeletion) {
                $this->itemCompradosScheduledForDeletion = clone $this->collItemComprados;
                $this->itemCompradosScheduledForDeletion->clear();
            }
            $this->itemCompradosScheduledForDeletion[]= clone $itemComprado;
            $itemComprado->setContasPagar(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ContasPagar is new, it will return
     * an empty collection; or if this ContasPagar has previously
     * been saved, it will retrieve related ItemComprados from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ContasPagar.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildItemComprado[] List of ChildItemComprado objects
     */
    public function getItemCompradosJoinItemDeCompra(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildItemCompradoQuery::create(null, $criteria);
        $query->joinWith('ItemDeCompra', $joinBehavior);

        return $this->getItemComprados($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ContasPagar is new, it will return
     * an empty collection; or if this ContasPagar has previously
     * been saved, it will retrieve related ItemComprados from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ContasPagar.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildItemComprado[] List of ChildItemComprado objects
     */
    public function getItemCompradosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildItemCompradoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getItemComprados($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aFornecedor) {
            $this->aFornecedor->removeContasPagar($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeContasPagar($this);
        }
        $this->idcontas_pagar = null;
        $this->valor = null;
        $this->valor_frete = null;
        $this->fornecedor_id = null;
        $this->num_documento = null;
        $this->juros = null;
        $this->multa = null;
        $this->data_emissao = null;
        $this->tipo = null;
        $this->especie = null;
        $this->arquivo = null;
        $this->obs = null;
        $this->ativo = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collContasPagarParcelass) {
                foreach ($this->collContasPagarParcelass as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collContasPagarTributoss) {
                foreach ($this->collContasPagarTributoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collItemComprados) {
                foreach ($this->collItemComprados as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collContasPagarParcelass = null;
        $this->collContasPagarTributoss = null;
        $this->collItemComprados = null;
        $this->aFornecedor = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ContasPagarTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
