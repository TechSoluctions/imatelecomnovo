<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Contrato as ChildContrato;
use ImaTelecomBundle\Model\ContratoQuery as ChildContratoQuery;
use ImaTelecomBundle\Model\Map\ContratoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'contrato' table.
 *
 *
 *
 * @method     ChildContratoQuery orderByIdcontrato($order = Criteria::ASC) Order by the idcontrato column
 * @method     ChildContratoQuery orderByDescricao($order = Criteria::ASC) Order by the descricao column
 * @method     ChildContratoQuery orderByValor($order = Criteria::ASC) Order by the valor column
 * @method     ChildContratoQuery orderByEnderecoClienteId($order = Criteria::ASC) Order by the endereco_cliente_id column
 * @method     ChildContratoQuery orderByDataContratado($order = Criteria::ASC) Order by the data_contratado column
 * @method     ChildContratoQuery orderByDataCancelado($order = Criteria::ASC) Order by the data_cancelado column
 * @method     ChildContratoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildContratoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildContratoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildContratoQuery orderByCancelado($order = Criteria::ASC) Order by the cancelado column
 * @method     ChildContratoQuery orderByClienteId($order = Criteria::ASC) Order by the cliente_id column
 * @method     ChildContratoQuery orderByFormaPagamentoId($order = Criteria::ASC) Order by the forma_pagamento_id column
 * @method     ChildContratoQuery orderByProfilePagamentoId($order = Criteria::ASC) Order by the profile_pagamento_id column
 * @method     ChildContratoQuery orderByVendedorId($order = Criteria::ASC) Order by the vendedor_id column
 * @method     ChildContratoQuery orderByRenovadoAte($order = Criteria::ASC) Order by the renovado_ate column
 * @method     ChildContratoQuery orderByPrimeiroVencimento($order = Criteria::ASC) Order by the primeiro_vencimento column
 * @method     ChildContratoQuery orderByPercentualMulta($order = Criteria::ASC) Order by the percentual_multa column
 * @method     ChildContratoQuery orderByPercentualAcrescimo($order = Criteria::ASC) Order by the percentual_acrescimo column
 * @method     ChildContratoQuery orderByUsuarioCancelamento($order = Criteria::ASC) Order by the usuario_cancelamento column
 * @method     ChildContratoQuery orderByMotivoCancelamento($order = Criteria::ASC) Order by the motivo_cancelamento column
 * @method     ChildContratoQuery orderByAgenciaDebito($order = Criteria::ASC) Order by the agencia_debito column
 * @method     ChildContratoQuery orderByIdClienteBanco($order = Criteria::ASC) Order by the id_cliente_banco column
 * @method     ChildContratoQuery orderByEmissaoNf21($order = Criteria::ASC) Order by the emissao_nf_21 column
 * @method     ChildContratoQuery orderByEmissaoNfPre($order = Criteria::ASC) Order by the emissao_nf_pre column
 * @method     ChildContratoQuery orderByIgnoraRenovacaoAutomatica($order = Criteria::ASC) Order by the ignora_renovacao_automatica column
 * @method     ChildContratoQuery orderByComodato($order = Criteria::ASC) Order by the comodato column
 * @method     ChildContratoQuery orderByEmpresaId($order = Criteria::ASC) Order by the empresa_id column
 * @method     ChildContratoQuery orderByDescricaoComodato($order = Criteria::ASC) Order by the descricao_comodato column
 * @method     ChildContratoQuery orderByDataComodato($order = Criteria::ASC) Order by the data_comodato column
 * @method     ChildContratoQuery orderByObsComodato($order = Criteria::ASC) Order by the obs_comodato column
 * @method     ChildContratoQuery orderByCentrocusto($order = Criteria::ASC) Order by the centrocusto column
 * @method     ChildContratoQuery orderByUnidadeNegocio($order = Criteria::ASC) Order by the unidade_negocio column
 * @method     ChildContratoQuery orderByValorRenovacao($order = Criteria::ASC) Order by the valor_renovacao column
 * @method     ChildContratoQuery orderByModeloNf($order = Criteria::ASC) Order by the modelo_nf column
 * @method     ChildContratoQuery orderByEmissaoNfse($order = Criteria::ASC) Order by the emissao_nfse column
 * @method     ChildContratoQuery orderBySuspenso($order = Criteria::ASC) Order by the suspenso column
 * @method     ChildContratoQuery orderByDataSuspensao($order = Criteria::ASC) Order by the data_suspensao column
 * @method     ChildContratoQuery orderByUsuarioSuspensao($order = Criteria::ASC) Order by the usuario_suspensao column
 * @method     ChildContratoQuery orderByMotivoSuspensao($order = Criteria::ASC) Order by the motivo_suspensao column
 * @method     ChildContratoQuery orderByVersao($order = Criteria::ASC) Order by the versao column
 * @method     ChildContratoQuery orderByFaturamentoAutomatico($order = Criteria::ASC) Order by the faturamento_automatico column
 * @method     ChildContratoQuery orderByRegraFaturamentoId($order = Criteria::ASC) Order by the regra_faturamento_id column
 * @method     ChildContratoQuery orderByEnderecoNfId($order = Criteria::ASC) Order by the endereco_nf_id column
 * @method     ChildContratoQuery orderByEnderecoNfseId($order = Criteria::ASC) Order by the endereco_nfse_id column
 * @method     ChildContratoQuery orderByObsNf($order = Criteria::ASC) Order by the obs_nf column
 * @method     ChildContratoQuery orderByTipoFaturamentoId($order = Criteria::ASC) Order by the tipo_faturamento_id column
 * @method     ChildContratoQuery orderByTemScm($order = Criteria::ASC) Order by the tem_scm column
 * @method     ChildContratoQuery orderByScm($order = Criteria::ASC) Order by the scm column
 * @method     ChildContratoQuery orderByTemSva($order = Criteria::ASC) Order by the tem_sva column
 * @method     ChildContratoQuery orderBySva($order = Criteria::ASC) Order by the sva column
 * @method     ChildContratoQuery orderByForcarValor($order = Criteria::ASC) Order by the forcar_valor column
 * @method     ChildContratoQuery orderByDescricaoScm($order = Criteria::ASC) Order by the descricao_scm column
 * @method     ChildContratoQuery orderByDescricaoSva($order = Criteria::ASC) Order by the descricao_sva column
 * @method     ChildContratoQuery orderByTipoScm($order = Criteria::ASC) Order by the tipo_scm column
 * @method     ChildContratoQuery orderByTipoSva($order = Criteria::ASC) Order by the tipo_sva column
 * @method     ChildContratoQuery orderByGerarCobranca($order = Criteria::ASC) Order by the gerar_cobranca column
 * @method     ChildContratoQuery orderByCodigoObra($order = Criteria::ASC) Order by the codigo_obra column
 * @method     ChildContratoQuery orderByArt($order = Criteria::ASC) Order by the art column
 * @method     ChildContratoQuery orderByTemReembolso($order = Criteria::ASC) Order by the tem_reembolso column
 *
 * @method     ChildContratoQuery groupByIdcontrato() Group by the idcontrato column
 * @method     ChildContratoQuery groupByDescricao() Group by the descricao column
 * @method     ChildContratoQuery groupByValor() Group by the valor column
 * @method     ChildContratoQuery groupByEnderecoClienteId() Group by the endereco_cliente_id column
 * @method     ChildContratoQuery groupByDataContratado() Group by the data_contratado column
 * @method     ChildContratoQuery groupByDataCancelado() Group by the data_cancelado column
 * @method     ChildContratoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildContratoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildContratoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildContratoQuery groupByCancelado() Group by the cancelado column
 * @method     ChildContratoQuery groupByClienteId() Group by the cliente_id column
 * @method     ChildContratoQuery groupByFormaPagamentoId() Group by the forma_pagamento_id column
 * @method     ChildContratoQuery groupByProfilePagamentoId() Group by the profile_pagamento_id column
 * @method     ChildContratoQuery groupByVendedorId() Group by the vendedor_id column
 * @method     ChildContratoQuery groupByRenovadoAte() Group by the renovado_ate column
 * @method     ChildContratoQuery groupByPrimeiroVencimento() Group by the primeiro_vencimento column
 * @method     ChildContratoQuery groupByPercentualMulta() Group by the percentual_multa column
 * @method     ChildContratoQuery groupByPercentualAcrescimo() Group by the percentual_acrescimo column
 * @method     ChildContratoQuery groupByUsuarioCancelamento() Group by the usuario_cancelamento column
 * @method     ChildContratoQuery groupByMotivoCancelamento() Group by the motivo_cancelamento column
 * @method     ChildContratoQuery groupByAgenciaDebito() Group by the agencia_debito column
 * @method     ChildContratoQuery groupByIdClienteBanco() Group by the id_cliente_banco column
 * @method     ChildContratoQuery groupByEmissaoNf21() Group by the emissao_nf_21 column
 * @method     ChildContratoQuery groupByEmissaoNfPre() Group by the emissao_nf_pre column
 * @method     ChildContratoQuery groupByIgnoraRenovacaoAutomatica() Group by the ignora_renovacao_automatica column
 * @method     ChildContratoQuery groupByComodato() Group by the comodato column
 * @method     ChildContratoQuery groupByEmpresaId() Group by the empresa_id column
 * @method     ChildContratoQuery groupByDescricaoComodato() Group by the descricao_comodato column
 * @method     ChildContratoQuery groupByDataComodato() Group by the data_comodato column
 * @method     ChildContratoQuery groupByObsComodato() Group by the obs_comodato column
 * @method     ChildContratoQuery groupByCentrocusto() Group by the centrocusto column
 * @method     ChildContratoQuery groupByUnidadeNegocio() Group by the unidade_negocio column
 * @method     ChildContratoQuery groupByValorRenovacao() Group by the valor_renovacao column
 * @method     ChildContratoQuery groupByModeloNf() Group by the modelo_nf column
 * @method     ChildContratoQuery groupByEmissaoNfse() Group by the emissao_nfse column
 * @method     ChildContratoQuery groupBySuspenso() Group by the suspenso column
 * @method     ChildContratoQuery groupByDataSuspensao() Group by the data_suspensao column
 * @method     ChildContratoQuery groupByUsuarioSuspensao() Group by the usuario_suspensao column
 * @method     ChildContratoQuery groupByMotivoSuspensao() Group by the motivo_suspensao column
 * @method     ChildContratoQuery groupByVersao() Group by the versao column
 * @method     ChildContratoQuery groupByFaturamentoAutomatico() Group by the faturamento_automatico column
 * @method     ChildContratoQuery groupByRegraFaturamentoId() Group by the regra_faturamento_id column
 * @method     ChildContratoQuery groupByEnderecoNfId() Group by the endereco_nf_id column
 * @method     ChildContratoQuery groupByEnderecoNfseId() Group by the endereco_nfse_id column
 * @method     ChildContratoQuery groupByObsNf() Group by the obs_nf column
 * @method     ChildContratoQuery groupByTipoFaturamentoId() Group by the tipo_faturamento_id column
 * @method     ChildContratoQuery groupByTemScm() Group by the tem_scm column
 * @method     ChildContratoQuery groupByScm() Group by the scm column
 * @method     ChildContratoQuery groupByTemSva() Group by the tem_sva column
 * @method     ChildContratoQuery groupBySva() Group by the sva column
 * @method     ChildContratoQuery groupByForcarValor() Group by the forcar_valor column
 * @method     ChildContratoQuery groupByDescricaoScm() Group by the descricao_scm column
 * @method     ChildContratoQuery groupByDescricaoSva() Group by the descricao_sva column
 * @method     ChildContratoQuery groupByTipoScm() Group by the tipo_scm column
 * @method     ChildContratoQuery groupByTipoSva() Group by the tipo_sva column
 * @method     ChildContratoQuery groupByGerarCobranca() Group by the gerar_cobranca column
 * @method     ChildContratoQuery groupByCodigoObra() Group by the codigo_obra column
 * @method     ChildContratoQuery groupByArt() Group by the art column
 * @method     ChildContratoQuery groupByTemReembolso() Group by the tem_reembolso column
 *
 * @method     ChildContratoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildContratoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildContratoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildContratoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildContratoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildContratoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildContratoQuery leftJoinEnderecoCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoCliente relation
 * @method     ChildContratoQuery rightJoinEnderecoCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoCliente relation
 * @method     ChildContratoQuery innerJoinEnderecoCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoCliente relation
 *
 * @method     ChildContratoQuery joinWithEnderecoCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoCliente relation
 *
 * @method     ChildContratoQuery leftJoinWithEnderecoCliente() Adds a LEFT JOIN clause and with to the query using the EnderecoCliente relation
 * @method     ChildContratoQuery rightJoinWithEnderecoCliente() Adds a RIGHT JOIN clause and with to the query using the EnderecoCliente relation
 * @method     ChildContratoQuery innerJoinWithEnderecoCliente() Adds a INNER JOIN clause and with to the query using the EnderecoCliente relation
 *
 * @method     ChildContratoQuery leftJoinRegraFaturamento($relationAlias = null) Adds a LEFT JOIN clause to the query using the RegraFaturamento relation
 * @method     ChildContratoQuery rightJoinRegraFaturamento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RegraFaturamento relation
 * @method     ChildContratoQuery innerJoinRegraFaturamento($relationAlias = null) Adds a INNER JOIN clause to the query using the RegraFaturamento relation
 *
 * @method     ChildContratoQuery joinWithRegraFaturamento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RegraFaturamento relation
 *
 * @method     ChildContratoQuery leftJoinWithRegraFaturamento() Adds a LEFT JOIN clause and with to the query using the RegraFaturamento relation
 * @method     ChildContratoQuery rightJoinWithRegraFaturamento() Adds a RIGHT JOIN clause and with to the query using the RegraFaturamento relation
 * @method     ChildContratoQuery innerJoinWithRegraFaturamento() Adds a INNER JOIN clause and with to the query using the RegraFaturamento relation
 *
 * @method     ChildContratoQuery leftJoinServicoCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildContratoQuery rightJoinServicoCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildContratoQuery innerJoinServicoCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicoCliente relation
 *
 * @method     ChildContratoQuery joinWithServicoCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicoCliente relation
 *
 * @method     ChildContratoQuery leftJoinWithServicoCliente() Adds a LEFT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildContratoQuery rightJoinWithServicoCliente() Adds a RIGHT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildContratoQuery innerJoinWithServicoCliente() Adds a INNER JOIN clause and with to the query using the ServicoCliente relation
 *
 * @method     \ImaTelecomBundle\Model\EnderecoClienteQuery|\ImaTelecomBundle\Model\RegraFaturamentoQuery|\ImaTelecomBundle\Model\ServicoClienteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildContrato findOne(ConnectionInterface $con = null) Return the first ChildContrato matching the query
 * @method     ChildContrato findOneOrCreate(ConnectionInterface $con = null) Return the first ChildContrato matching the query, or a new ChildContrato object populated from the query conditions when no match is found
 *
 * @method     ChildContrato findOneByIdcontrato(int $idcontrato) Return the first ChildContrato filtered by the idcontrato column
 * @method     ChildContrato findOneByDescricao(string $descricao) Return the first ChildContrato filtered by the descricao column
 * @method     ChildContrato findOneByValor(string $valor) Return the first ChildContrato filtered by the valor column
 * @method     ChildContrato findOneByEnderecoClienteId(int $endereco_cliente_id) Return the first ChildContrato filtered by the endereco_cliente_id column
 * @method     ChildContrato findOneByDataContratado(string $data_contratado) Return the first ChildContrato filtered by the data_contratado column
 * @method     ChildContrato findOneByDataCancelado(string $data_cancelado) Return the first ChildContrato filtered by the data_cancelado column
 * @method     ChildContrato findOneByDataCadastro(string $data_cadastro) Return the first ChildContrato filtered by the data_cadastro column
 * @method     ChildContrato findOneByDataAlterado(string $data_alterado) Return the first ChildContrato filtered by the data_alterado column
 * @method     ChildContrato findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildContrato filtered by the usuario_alterado column
 * @method     ChildContrato findOneByCancelado(boolean $cancelado) Return the first ChildContrato filtered by the cancelado column
 * @method     ChildContrato findOneByClienteId(int $cliente_id) Return the first ChildContrato filtered by the cliente_id column
 * @method     ChildContrato findOneByFormaPagamentoId(int $forma_pagamento_id) Return the first ChildContrato filtered by the forma_pagamento_id column
 * @method     ChildContrato findOneByProfilePagamentoId(int $profile_pagamento_id) Return the first ChildContrato filtered by the profile_pagamento_id column
 * @method     ChildContrato findOneByVendedorId(int $vendedor_id) Return the first ChildContrato filtered by the vendedor_id column
 * @method     ChildContrato findOneByRenovadoAte(string $renovado_ate) Return the first ChildContrato filtered by the renovado_ate column
 * @method     ChildContrato findOneByPrimeiroVencimento(string $primeiro_vencimento) Return the first ChildContrato filtered by the primeiro_vencimento column
 * @method     ChildContrato findOneByPercentualMulta(int $percentual_multa) Return the first ChildContrato filtered by the percentual_multa column
 * @method     ChildContrato findOneByPercentualAcrescimo(int $percentual_acrescimo) Return the first ChildContrato filtered by the percentual_acrescimo column
 * @method     ChildContrato findOneByUsuarioCancelamento(int $usuario_cancelamento) Return the first ChildContrato filtered by the usuario_cancelamento column
 * @method     ChildContrato findOneByMotivoCancelamento(int $motivo_cancelamento) Return the first ChildContrato filtered by the motivo_cancelamento column
 * @method     ChildContrato findOneByAgenciaDebito(string $agencia_debito) Return the first ChildContrato filtered by the agencia_debito column
 * @method     ChildContrato findOneByIdClienteBanco(string $id_cliente_banco) Return the first ChildContrato filtered by the id_cliente_banco column
 * @method     ChildContrato findOneByEmissaoNf21(boolean $emissao_nf_21) Return the first ChildContrato filtered by the emissao_nf_21 column
 * @method     ChildContrato findOneByEmissaoNfPre(boolean $emissao_nf_pre) Return the first ChildContrato filtered by the emissao_nf_pre column
 * @method     ChildContrato findOneByIgnoraRenovacaoAutomatica(boolean $ignora_renovacao_automatica) Return the first ChildContrato filtered by the ignora_renovacao_automatica column
 * @method     ChildContrato findOneByComodato(int $comodato) Return the first ChildContrato filtered by the comodato column
 * @method     ChildContrato findOneByEmpresaId(int $empresa_id) Return the first ChildContrato filtered by the empresa_id column
 * @method     ChildContrato findOneByDescricaoComodato(string $descricao_comodato) Return the first ChildContrato filtered by the descricao_comodato column
 * @method     ChildContrato findOneByDataComodato(string $data_comodato) Return the first ChildContrato filtered by the data_comodato column
 * @method     ChildContrato findOneByObsComodato(string $obs_comodato) Return the first ChildContrato filtered by the obs_comodato column
 * @method     ChildContrato findOneByCentrocusto(int $centrocusto) Return the first ChildContrato filtered by the centrocusto column
 * @method     ChildContrato findOneByUnidadeNegocio(int $unidade_negocio) Return the first ChildContrato filtered by the unidade_negocio column
 * @method     ChildContrato findOneByValorRenovacao(string $valor_renovacao) Return the first ChildContrato filtered by the valor_renovacao column
 * @method     ChildContrato findOneByModeloNf(int $modelo_nf) Return the first ChildContrato filtered by the modelo_nf column
 * @method     ChildContrato findOneByEmissaoNfse(boolean $emissao_nfse) Return the first ChildContrato filtered by the emissao_nfse column
 * @method     ChildContrato findOneBySuspenso(boolean $suspenso) Return the first ChildContrato filtered by the suspenso column
 * @method     ChildContrato findOneByDataSuspensao(string $data_suspensao) Return the first ChildContrato filtered by the data_suspensao column
 * @method     ChildContrato findOneByUsuarioSuspensao(int $usuario_suspensao) Return the first ChildContrato filtered by the usuario_suspensao column
 * @method     ChildContrato findOneByMotivoSuspensao(int $motivo_suspensao) Return the first ChildContrato filtered by the motivo_suspensao column
 * @method     ChildContrato findOneByVersao(int $versao) Return the first ChildContrato filtered by the versao column
 * @method     ChildContrato findOneByFaturamentoAutomatico(boolean $faturamento_automatico) Return the first ChildContrato filtered by the faturamento_automatico column
 * @method     ChildContrato findOneByRegraFaturamentoId(int $regra_faturamento_id) Return the first ChildContrato filtered by the regra_faturamento_id column
 * @method     ChildContrato findOneByEnderecoNfId(int $endereco_nf_id) Return the first ChildContrato filtered by the endereco_nf_id column
 * @method     ChildContrato findOneByEnderecoNfseId(int $endereco_nfse_id) Return the first ChildContrato filtered by the endereco_nfse_id column
 * @method     ChildContrato findOneByObsNf(string $obs_nf) Return the first ChildContrato filtered by the obs_nf column
 * @method     ChildContrato findOneByTipoFaturamentoId(int $tipo_faturamento_id) Return the first ChildContrato filtered by the tipo_faturamento_id column
 * @method     ChildContrato findOneByTemScm(boolean $tem_scm) Return the first ChildContrato filtered by the tem_scm column
 * @method     ChildContrato findOneByScm(string $scm) Return the first ChildContrato filtered by the scm column
 * @method     ChildContrato findOneByTemSva(boolean $tem_sva) Return the first ChildContrato filtered by the tem_sva column
 * @method     ChildContrato findOneBySva(string $sva) Return the first ChildContrato filtered by the sva column
 * @method     ChildContrato findOneByForcarValor(boolean $forcar_valor) Return the first ChildContrato filtered by the forcar_valor column
 * @method     ChildContrato findOneByDescricaoScm(string $descricao_scm) Return the first ChildContrato filtered by the descricao_scm column
 * @method     ChildContrato findOneByDescricaoSva(string $descricao_sva) Return the first ChildContrato filtered by the descricao_sva column
 * @method     ChildContrato findOneByTipoScm(string $tipo_scm) Return the first ChildContrato filtered by the tipo_scm column
 * @method     ChildContrato findOneByTipoSva(string $tipo_sva) Return the first ChildContrato filtered by the tipo_sva column
 * @method     ChildContrato findOneByGerarCobranca(boolean $gerar_cobranca) Return the first ChildContrato filtered by the gerar_cobranca column
 * @method     ChildContrato findOneByCodigoObra(string $codigo_obra) Return the first ChildContrato filtered by the codigo_obra column
 * @method     ChildContrato findOneByArt(string $art) Return the first ChildContrato filtered by the art column
 * @method     ChildContrato findOneByTemReembolso(boolean $tem_reembolso) Return the first ChildContrato filtered by the tem_reembolso column *

 * @method     ChildContrato requirePk($key, ConnectionInterface $con = null) Return the ChildContrato by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOne(ConnectionInterface $con = null) Return the first ChildContrato matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContrato requireOneByIdcontrato(int $idcontrato) Return the first ChildContrato filtered by the idcontrato column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDescricao(string $descricao) Return the first ChildContrato filtered by the descricao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByValor(string $valor) Return the first ChildContrato filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByEnderecoClienteId(int $endereco_cliente_id) Return the first ChildContrato filtered by the endereco_cliente_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDataContratado(string $data_contratado) Return the first ChildContrato filtered by the data_contratado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDataCancelado(string $data_cancelado) Return the first ChildContrato filtered by the data_cancelado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDataCadastro(string $data_cadastro) Return the first ChildContrato filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDataAlterado(string $data_alterado) Return the first ChildContrato filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildContrato filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByCancelado(boolean $cancelado) Return the first ChildContrato filtered by the cancelado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByClienteId(int $cliente_id) Return the first ChildContrato filtered by the cliente_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByFormaPagamentoId(int $forma_pagamento_id) Return the first ChildContrato filtered by the forma_pagamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByProfilePagamentoId(int $profile_pagamento_id) Return the first ChildContrato filtered by the profile_pagamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByVendedorId(int $vendedor_id) Return the first ChildContrato filtered by the vendedor_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByRenovadoAte(string $renovado_ate) Return the first ChildContrato filtered by the renovado_ate column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByPrimeiroVencimento(string $primeiro_vencimento) Return the first ChildContrato filtered by the primeiro_vencimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByPercentualMulta(int $percentual_multa) Return the first ChildContrato filtered by the percentual_multa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByPercentualAcrescimo(int $percentual_acrescimo) Return the first ChildContrato filtered by the percentual_acrescimo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByUsuarioCancelamento(int $usuario_cancelamento) Return the first ChildContrato filtered by the usuario_cancelamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByMotivoCancelamento(int $motivo_cancelamento) Return the first ChildContrato filtered by the motivo_cancelamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByAgenciaDebito(string $agencia_debito) Return the first ChildContrato filtered by the agencia_debito column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByIdClienteBanco(string $id_cliente_banco) Return the first ChildContrato filtered by the id_cliente_banco column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByEmissaoNf21(boolean $emissao_nf_21) Return the first ChildContrato filtered by the emissao_nf_21 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByEmissaoNfPre(boolean $emissao_nf_pre) Return the first ChildContrato filtered by the emissao_nf_pre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByIgnoraRenovacaoAutomatica(boolean $ignora_renovacao_automatica) Return the first ChildContrato filtered by the ignora_renovacao_automatica column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByComodato(int $comodato) Return the first ChildContrato filtered by the comodato column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByEmpresaId(int $empresa_id) Return the first ChildContrato filtered by the empresa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDescricaoComodato(string $descricao_comodato) Return the first ChildContrato filtered by the descricao_comodato column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDataComodato(string $data_comodato) Return the first ChildContrato filtered by the data_comodato column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByObsComodato(string $obs_comodato) Return the first ChildContrato filtered by the obs_comodato column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByCentrocusto(int $centrocusto) Return the first ChildContrato filtered by the centrocusto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByUnidadeNegocio(int $unidade_negocio) Return the first ChildContrato filtered by the unidade_negocio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByValorRenovacao(string $valor_renovacao) Return the first ChildContrato filtered by the valor_renovacao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByModeloNf(int $modelo_nf) Return the first ChildContrato filtered by the modelo_nf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByEmissaoNfse(boolean $emissao_nfse) Return the first ChildContrato filtered by the emissao_nfse column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneBySuspenso(boolean $suspenso) Return the first ChildContrato filtered by the suspenso column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDataSuspensao(string $data_suspensao) Return the first ChildContrato filtered by the data_suspensao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByUsuarioSuspensao(int $usuario_suspensao) Return the first ChildContrato filtered by the usuario_suspensao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByMotivoSuspensao(int $motivo_suspensao) Return the first ChildContrato filtered by the motivo_suspensao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByVersao(int $versao) Return the first ChildContrato filtered by the versao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByFaturamentoAutomatico(boolean $faturamento_automatico) Return the first ChildContrato filtered by the faturamento_automatico column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByRegraFaturamentoId(int $regra_faturamento_id) Return the first ChildContrato filtered by the regra_faturamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByEnderecoNfId(int $endereco_nf_id) Return the first ChildContrato filtered by the endereco_nf_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByEnderecoNfseId(int $endereco_nfse_id) Return the first ChildContrato filtered by the endereco_nfse_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByObsNf(string $obs_nf) Return the first ChildContrato filtered by the obs_nf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByTipoFaturamentoId(int $tipo_faturamento_id) Return the first ChildContrato filtered by the tipo_faturamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByTemScm(boolean $tem_scm) Return the first ChildContrato filtered by the tem_scm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByScm(string $scm) Return the first ChildContrato filtered by the scm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByTemSva(boolean $tem_sva) Return the first ChildContrato filtered by the tem_sva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneBySva(string $sva) Return the first ChildContrato filtered by the sva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByForcarValor(boolean $forcar_valor) Return the first ChildContrato filtered by the forcar_valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDescricaoScm(string $descricao_scm) Return the first ChildContrato filtered by the descricao_scm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByDescricaoSva(string $descricao_sva) Return the first ChildContrato filtered by the descricao_sva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByTipoScm(string $tipo_scm) Return the first ChildContrato filtered by the tipo_scm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByTipoSva(string $tipo_sva) Return the first ChildContrato filtered by the tipo_sva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByGerarCobranca(boolean $gerar_cobranca) Return the first ChildContrato filtered by the gerar_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByCodigoObra(string $codigo_obra) Return the first ChildContrato filtered by the codigo_obra column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByArt(string $art) Return the first ChildContrato filtered by the art column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrato requireOneByTemReembolso(boolean $tem_reembolso) Return the first ChildContrato filtered by the tem_reembolso column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContrato[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildContrato objects based on current ModelCriteria
 * @method     ChildContrato[]|ObjectCollection findByIdcontrato(int $idcontrato) Return ChildContrato objects filtered by the idcontrato column
 * @method     ChildContrato[]|ObjectCollection findByDescricao(string $descricao) Return ChildContrato objects filtered by the descricao column
 * @method     ChildContrato[]|ObjectCollection findByValor(string $valor) Return ChildContrato objects filtered by the valor column
 * @method     ChildContrato[]|ObjectCollection findByEnderecoClienteId(int $endereco_cliente_id) Return ChildContrato objects filtered by the endereco_cliente_id column
 * @method     ChildContrato[]|ObjectCollection findByDataContratado(string $data_contratado) Return ChildContrato objects filtered by the data_contratado column
 * @method     ChildContrato[]|ObjectCollection findByDataCancelado(string $data_cancelado) Return ChildContrato objects filtered by the data_cancelado column
 * @method     ChildContrato[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildContrato objects filtered by the data_cadastro column
 * @method     ChildContrato[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildContrato objects filtered by the data_alterado column
 * @method     ChildContrato[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildContrato objects filtered by the usuario_alterado column
 * @method     ChildContrato[]|ObjectCollection findByCancelado(boolean $cancelado) Return ChildContrato objects filtered by the cancelado column
 * @method     ChildContrato[]|ObjectCollection findByClienteId(int $cliente_id) Return ChildContrato objects filtered by the cliente_id column
 * @method     ChildContrato[]|ObjectCollection findByFormaPagamentoId(int $forma_pagamento_id) Return ChildContrato objects filtered by the forma_pagamento_id column
 * @method     ChildContrato[]|ObjectCollection findByProfilePagamentoId(int $profile_pagamento_id) Return ChildContrato objects filtered by the profile_pagamento_id column
 * @method     ChildContrato[]|ObjectCollection findByVendedorId(int $vendedor_id) Return ChildContrato objects filtered by the vendedor_id column
 * @method     ChildContrato[]|ObjectCollection findByRenovadoAte(string $renovado_ate) Return ChildContrato objects filtered by the renovado_ate column
 * @method     ChildContrato[]|ObjectCollection findByPrimeiroVencimento(string $primeiro_vencimento) Return ChildContrato objects filtered by the primeiro_vencimento column
 * @method     ChildContrato[]|ObjectCollection findByPercentualMulta(int $percentual_multa) Return ChildContrato objects filtered by the percentual_multa column
 * @method     ChildContrato[]|ObjectCollection findByPercentualAcrescimo(int $percentual_acrescimo) Return ChildContrato objects filtered by the percentual_acrescimo column
 * @method     ChildContrato[]|ObjectCollection findByUsuarioCancelamento(int $usuario_cancelamento) Return ChildContrato objects filtered by the usuario_cancelamento column
 * @method     ChildContrato[]|ObjectCollection findByMotivoCancelamento(int $motivo_cancelamento) Return ChildContrato objects filtered by the motivo_cancelamento column
 * @method     ChildContrato[]|ObjectCollection findByAgenciaDebito(string $agencia_debito) Return ChildContrato objects filtered by the agencia_debito column
 * @method     ChildContrato[]|ObjectCollection findByIdClienteBanco(string $id_cliente_banco) Return ChildContrato objects filtered by the id_cliente_banco column
 * @method     ChildContrato[]|ObjectCollection findByEmissaoNf21(boolean $emissao_nf_21) Return ChildContrato objects filtered by the emissao_nf_21 column
 * @method     ChildContrato[]|ObjectCollection findByEmissaoNfPre(boolean $emissao_nf_pre) Return ChildContrato objects filtered by the emissao_nf_pre column
 * @method     ChildContrato[]|ObjectCollection findByIgnoraRenovacaoAutomatica(boolean $ignora_renovacao_automatica) Return ChildContrato objects filtered by the ignora_renovacao_automatica column
 * @method     ChildContrato[]|ObjectCollection findByComodato(int $comodato) Return ChildContrato objects filtered by the comodato column
 * @method     ChildContrato[]|ObjectCollection findByEmpresaId(int $empresa_id) Return ChildContrato objects filtered by the empresa_id column
 * @method     ChildContrato[]|ObjectCollection findByDescricaoComodato(string $descricao_comodato) Return ChildContrato objects filtered by the descricao_comodato column
 * @method     ChildContrato[]|ObjectCollection findByDataComodato(string $data_comodato) Return ChildContrato objects filtered by the data_comodato column
 * @method     ChildContrato[]|ObjectCollection findByObsComodato(string $obs_comodato) Return ChildContrato objects filtered by the obs_comodato column
 * @method     ChildContrato[]|ObjectCollection findByCentrocusto(int $centrocusto) Return ChildContrato objects filtered by the centrocusto column
 * @method     ChildContrato[]|ObjectCollection findByUnidadeNegocio(int $unidade_negocio) Return ChildContrato objects filtered by the unidade_negocio column
 * @method     ChildContrato[]|ObjectCollection findByValorRenovacao(string $valor_renovacao) Return ChildContrato objects filtered by the valor_renovacao column
 * @method     ChildContrato[]|ObjectCollection findByModeloNf(int $modelo_nf) Return ChildContrato objects filtered by the modelo_nf column
 * @method     ChildContrato[]|ObjectCollection findByEmissaoNfse(boolean $emissao_nfse) Return ChildContrato objects filtered by the emissao_nfse column
 * @method     ChildContrato[]|ObjectCollection findBySuspenso(boolean $suspenso) Return ChildContrato objects filtered by the suspenso column
 * @method     ChildContrato[]|ObjectCollection findByDataSuspensao(string $data_suspensao) Return ChildContrato objects filtered by the data_suspensao column
 * @method     ChildContrato[]|ObjectCollection findByUsuarioSuspensao(int $usuario_suspensao) Return ChildContrato objects filtered by the usuario_suspensao column
 * @method     ChildContrato[]|ObjectCollection findByMotivoSuspensao(int $motivo_suspensao) Return ChildContrato objects filtered by the motivo_suspensao column
 * @method     ChildContrato[]|ObjectCollection findByVersao(int $versao) Return ChildContrato objects filtered by the versao column
 * @method     ChildContrato[]|ObjectCollection findByFaturamentoAutomatico(boolean $faturamento_automatico) Return ChildContrato objects filtered by the faturamento_automatico column
 * @method     ChildContrato[]|ObjectCollection findByRegraFaturamentoId(int $regra_faturamento_id) Return ChildContrato objects filtered by the regra_faturamento_id column
 * @method     ChildContrato[]|ObjectCollection findByEnderecoNfId(int $endereco_nf_id) Return ChildContrato objects filtered by the endereco_nf_id column
 * @method     ChildContrato[]|ObjectCollection findByEnderecoNfseId(int $endereco_nfse_id) Return ChildContrato objects filtered by the endereco_nfse_id column
 * @method     ChildContrato[]|ObjectCollection findByObsNf(string $obs_nf) Return ChildContrato objects filtered by the obs_nf column
 * @method     ChildContrato[]|ObjectCollection findByTipoFaturamentoId(int $tipo_faturamento_id) Return ChildContrato objects filtered by the tipo_faturamento_id column
 * @method     ChildContrato[]|ObjectCollection findByTemScm(boolean $tem_scm) Return ChildContrato objects filtered by the tem_scm column
 * @method     ChildContrato[]|ObjectCollection findByScm(string $scm) Return ChildContrato objects filtered by the scm column
 * @method     ChildContrato[]|ObjectCollection findByTemSva(boolean $tem_sva) Return ChildContrato objects filtered by the tem_sva column
 * @method     ChildContrato[]|ObjectCollection findBySva(string $sva) Return ChildContrato objects filtered by the sva column
 * @method     ChildContrato[]|ObjectCollection findByForcarValor(boolean $forcar_valor) Return ChildContrato objects filtered by the forcar_valor column
 * @method     ChildContrato[]|ObjectCollection findByDescricaoScm(string $descricao_scm) Return ChildContrato objects filtered by the descricao_scm column
 * @method     ChildContrato[]|ObjectCollection findByDescricaoSva(string $descricao_sva) Return ChildContrato objects filtered by the descricao_sva column
 * @method     ChildContrato[]|ObjectCollection findByTipoScm(string $tipo_scm) Return ChildContrato objects filtered by the tipo_scm column
 * @method     ChildContrato[]|ObjectCollection findByTipoSva(string $tipo_sva) Return ChildContrato objects filtered by the tipo_sva column
 * @method     ChildContrato[]|ObjectCollection findByGerarCobranca(boolean $gerar_cobranca) Return ChildContrato objects filtered by the gerar_cobranca column
 * @method     ChildContrato[]|ObjectCollection findByCodigoObra(string $codigo_obra) Return ChildContrato objects filtered by the codigo_obra column
 * @method     ChildContrato[]|ObjectCollection findByArt(string $art) Return ChildContrato objects filtered by the art column
 * @method     ChildContrato[]|ObjectCollection findByTemReembolso(boolean $tem_reembolso) Return ChildContrato objects filtered by the tem_reembolso column
 * @method     ChildContrato[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ContratoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ContratoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Contrato', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildContratoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildContratoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildContratoQuery) {
            return $criteria;
        }
        $query = new ChildContratoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildContrato|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContratoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ContratoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContrato A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcontrato, descricao, valor, endereco_cliente_id, data_contratado, data_cancelado, data_cadastro, data_alterado, usuario_alterado, cancelado, cliente_id, forma_pagamento_id, profile_pagamento_id, vendedor_id, renovado_ate, primeiro_vencimento, percentual_multa, percentual_acrescimo, usuario_cancelamento, motivo_cancelamento, agencia_debito, id_cliente_banco, emissao_nf_21, emissao_nf_pre, ignora_renovacao_automatica, comodato, empresa_id, descricao_comodato, data_comodato, obs_comodato, centrocusto, unidade_negocio, valor_renovacao, modelo_nf, emissao_nfse, suspenso, data_suspensao, usuario_suspensao, motivo_suspensao, versao, faturamento_automatico, regra_faturamento_id, endereco_nf_id, endereco_nfse_id, obs_nf, tipo_faturamento_id, tem_scm, scm, tem_sva, sva, forcar_valor, descricao_scm, descricao_sva, tipo_scm, tipo_sva, gerar_cobranca, codigo_obra, art, tem_reembolso FROM contrato WHERE idcontrato = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildContrato $obj */
            $obj = new ChildContrato();
            $obj->hydrate($row);
            ContratoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildContrato|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ContratoTableMap::COL_IDCONTRATO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ContratoTableMap::COL_IDCONTRATO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcontrato column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcontrato(1234); // WHERE idcontrato = 1234
     * $query->filterByIdcontrato(array(12, 34)); // WHERE idcontrato IN (12, 34)
     * $query->filterByIdcontrato(array('min' => 12)); // WHERE idcontrato > 12
     * </code>
     *
     * @param     mixed $idcontrato The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByIdcontrato($idcontrato = null, $comparison = null)
    {
        if (is_array($idcontrato)) {
            $useMinMax = false;
            if (isset($idcontrato['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_IDCONTRATO, $idcontrato['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcontrato['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_IDCONTRATO, $idcontrato['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_IDCONTRATO, $idcontrato, $comparison);
    }

    /**
     * Filter the query on the descricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricao('fooValue');   // WHERE descricao = 'fooValue'
     * $query->filterByDescricao('%fooValue%', Criteria::LIKE); // WHERE descricao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDescricao($descricao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DESCRICAO, $descricao, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query on the endereco_cliente_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEnderecoClienteId(1234); // WHERE endereco_cliente_id = 1234
     * $query->filterByEnderecoClienteId(array(12, 34)); // WHERE endereco_cliente_id IN (12, 34)
     * $query->filterByEnderecoClienteId(array('min' => 12)); // WHERE endereco_cliente_id > 12
     * </code>
     *
     * @see       filterByEnderecoCliente()
     *
     * @param     mixed $enderecoClienteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByEnderecoClienteId($enderecoClienteId = null, $comparison = null)
    {
        if (is_array($enderecoClienteId)) {
            $useMinMax = false;
            if (isset($enderecoClienteId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_ENDERECO_CLIENTE_ID, $enderecoClienteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enderecoClienteId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_ENDERECO_CLIENTE_ID, $enderecoClienteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_ENDERECO_CLIENTE_ID, $enderecoClienteId, $comparison);
    }

    /**
     * Filter the query on the data_contratado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataContratado('2011-03-14'); // WHERE data_contratado = '2011-03-14'
     * $query->filterByDataContratado('now'); // WHERE data_contratado = '2011-03-14'
     * $query->filterByDataContratado(array('max' => 'yesterday')); // WHERE data_contratado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataContratado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDataContratado($dataContratado = null, $comparison = null)
    {
        if (is_array($dataContratado)) {
            $useMinMax = false;
            if (isset($dataContratado['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_CONTRATADO, $dataContratado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataContratado['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_CONTRATADO, $dataContratado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DATA_CONTRATADO, $dataContratado, $comparison);
    }

    /**
     * Filter the query on the data_cancelado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCancelado('2011-03-14'); // WHERE data_cancelado = '2011-03-14'
     * $query->filterByDataCancelado('now'); // WHERE data_cancelado = '2011-03-14'
     * $query->filterByDataCancelado(array('max' => 'yesterday')); // WHERE data_cancelado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCancelado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDataCancelado($dataCancelado = null, $comparison = null)
    {
        if (is_array($dataCancelado)) {
            $useMinMax = false;
            if (isset($dataCancelado['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_CANCELADO, $dataCancelado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCancelado['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_CANCELADO, $dataCancelado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DATA_CANCELADO, $dataCancelado, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the cancelado column
     *
     * Example usage:
     * <code>
     * $query->filterByCancelado(true); // WHERE cancelado = true
     * $query->filterByCancelado('yes'); // WHERE cancelado = true
     * </code>
     *
     * @param     boolean|string $cancelado The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByCancelado($cancelado = null, $comparison = null)
    {
        if (is_string($cancelado)) {
            $cancelado = in_array(strtolower($cancelado), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_CANCELADO, $cancelado, $comparison);
    }

    /**
     * Filter the query on the cliente_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClienteId(1234); // WHERE cliente_id = 1234
     * $query->filterByClienteId(array(12, 34)); // WHERE cliente_id IN (12, 34)
     * $query->filterByClienteId(array('min' => 12)); // WHERE cliente_id > 12
     * </code>
     *
     * @param     mixed $clienteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByClienteId($clienteId = null, $comparison = null)
    {
        if (is_array($clienteId)) {
            $useMinMax = false;
            if (isset($clienteId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_CLIENTE_ID, $clienteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clienteId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_CLIENTE_ID, $clienteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_CLIENTE_ID, $clienteId, $comparison);
    }

    /**
     * Filter the query on the forma_pagamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFormaPagamentoId(1234); // WHERE forma_pagamento_id = 1234
     * $query->filterByFormaPagamentoId(array(12, 34)); // WHERE forma_pagamento_id IN (12, 34)
     * $query->filterByFormaPagamentoId(array('min' => 12)); // WHERE forma_pagamento_id > 12
     * </code>
     *
     * @param     mixed $formaPagamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByFormaPagamentoId($formaPagamentoId = null, $comparison = null)
    {
        if (is_array($formaPagamentoId)) {
            $useMinMax = false;
            if (isset($formaPagamentoId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($formaPagamentoId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId, $comparison);
    }

    /**
     * Filter the query on the profile_pagamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProfilePagamentoId(1234); // WHERE profile_pagamento_id = 1234
     * $query->filterByProfilePagamentoId(array(12, 34)); // WHERE profile_pagamento_id IN (12, 34)
     * $query->filterByProfilePagamentoId(array('min' => 12)); // WHERE profile_pagamento_id > 12
     * </code>
     *
     * @param     mixed $profilePagamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByProfilePagamentoId($profilePagamentoId = null, $comparison = null)
    {
        if (is_array($profilePagamentoId)) {
            $useMinMax = false;
            if (isset($profilePagamentoId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_PROFILE_PAGAMENTO_ID, $profilePagamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($profilePagamentoId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_PROFILE_PAGAMENTO_ID, $profilePagamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_PROFILE_PAGAMENTO_ID, $profilePagamentoId, $comparison);
    }

    /**
     * Filter the query on the vendedor_id column
     *
     * Example usage:
     * <code>
     * $query->filterByVendedorId(1234); // WHERE vendedor_id = 1234
     * $query->filterByVendedorId(array(12, 34)); // WHERE vendedor_id IN (12, 34)
     * $query->filterByVendedorId(array('min' => 12)); // WHERE vendedor_id > 12
     * </code>
     *
     * @param     mixed $vendedorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByVendedorId($vendedorId = null, $comparison = null)
    {
        if (is_array($vendedorId)) {
            $useMinMax = false;
            if (isset($vendedorId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_VENDEDOR_ID, $vendedorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vendedorId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_VENDEDOR_ID, $vendedorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_VENDEDOR_ID, $vendedorId, $comparison);
    }

    /**
     * Filter the query on the renovado_ate column
     *
     * Example usage:
     * <code>
     * $query->filterByRenovadoAte('2011-03-14'); // WHERE renovado_ate = '2011-03-14'
     * $query->filterByRenovadoAte('now'); // WHERE renovado_ate = '2011-03-14'
     * $query->filterByRenovadoAte(array('max' => 'yesterday')); // WHERE renovado_ate > '2011-03-13'
     * </code>
     *
     * @param     mixed $renovadoAte The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByRenovadoAte($renovadoAte = null, $comparison = null)
    {
        if (is_array($renovadoAte)) {
            $useMinMax = false;
            if (isset($renovadoAte['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_RENOVADO_ATE, $renovadoAte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($renovadoAte['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_RENOVADO_ATE, $renovadoAte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_RENOVADO_ATE, $renovadoAte, $comparison);
    }

    /**
     * Filter the query on the primeiro_vencimento column
     *
     * Example usage:
     * <code>
     * $query->filterByPrimeiroVencimento('2011-03-14'); // WHERE primeiro_vencimento = '2011-03-14'
     * $query->filterByPrimeiroVencimento('now'); // WHERE primeiro_vencimento = '2011-03-14'
     * $query->filterByPrimeiroVencimento(array('max' => 'yesterday')); // WHERE primeiro_vencimento > '2011-03-13'
     * </code>
     *
     * @param     mixed $primeiroVencimento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByPrimeiroVencimento($primeiroVencimento = null, $comparison = null)
    {
        if (is_array($primeiroVencimento)) {
            $useMinMax = false;
            if (isset($primeiroVencimento['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_PRIMEIRO_VENCIMENTO, $primeiroVencimento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($primeiroVencimento['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_PRIMEIRO_VENCIMENTO, $primeiroVencimento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_PRIMEIRO_VENCIMENTO, $primeiroVencimento, $comparison);
    }

    /**
     * Filter the query on the percentual_multa column
     *
     * Example usage:
     * <code>
     * $query->filterByPercentualMulta(1234); // WHERE percentual_multa = 1234
     * $query->filterByPercentualMulta(array(12, 34)); // WHERE percentual_multa IN (12, 34)
     * $query->filterByPercentualMulta(array('min' => 12)); // WHERE percentual_multa > 12
     * </code>
     *
     * @param     mixed $percentualMulta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByPercentualMulta($percentualMulta = null, $comparison = null)
    {
        if (is_array($percentualMulta)) {
            $useMinMax = false;
            if (isset($percentualMulta['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_PERCENTUAL_MULTA, $percentualMulta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($percentualMulta['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_PERCENTUAL_MULTA, $percentualMulta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_PERCENTUAL_MULTA, $percentualMulta, $comparison);
    }

    /**
     * Filter the query on the percentual_acrescimo column
     *
     * Example usage:
     * <code>
     * $query->filterByPercentualAcrescimo(1234); // WHERE percentual_acrescimo = 1234
     * $query->filterByPercentualAcrescimo(array(12, 34)); // WHERE percentual_acrescimo IN (12, 34)
     * $query->filterByPercentualAcrescimo(array('min' => 12)); // WHERE percentual_acrescimo > 12
     * </code>
     *
     * @param     mixed $percentualAcrescimo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByPercentualAcrescimo($percentualAcrescimo = null, $comparison = null)
    {
        if (is_array($percentualAcrescimo)) {
            $useMinMax = false;
            if (isset($percentualAcrescimo['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_PERCENTUAL_ACRESCIMO, $percentualAcrescimo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($percentualAcrescimo['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_PERCENTUAL_ACRESCIMO, $percentualAcrescimo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_PERCENTUAL_ACRESCIMO, $percentualAcrescimo, $comparison);
    }

    /**
     * Filter the query on the usuario_cancelamento column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioCancelamento(1234); // WHERE usuario_cancelamento = 1234
     * $query->filterByUsuarioCancelamento(array(12, 34)); // WHERE usuario_cancelamento IN (12, 34)
     * $query->filterByUsuarioCancelamento(array('min' => 12)); // WHERE usuario_cancelamento > 12
     * </code>
     *
     * @param     mixed $usuarioCancelamento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByUsuarioCancelamento($usuarioCancelamento = null, $comparison = null)
    {
        if (is_array($usuarioCancelamento)) {
            $useMinMax = false;
            if (isset($usuarioCancelamento['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_USUARIO_CANCELAMENTO, $usuarioCancelamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioCancelamento['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_USUARIO_CANCELAMENTO, $usuarioCancelamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_USUARIO_CANCELAMENTO, $usuarioCancelamento, $comparison);
    }

    /**
     * Filter the query on the motivo_cancelamento column
     *
     * Example usage:
     * <code>
     * $query->filterByMotivoCancelamento(1234); // WHERE motivo_cancelamento = 1234
     * $query->filterByMotivoCancelamento(array(12, 34)); // WHERE motivo_cancelamento IN (12, 34)
     * $query->filterByMotivoCancelamento(array('min' => 12)); // WHERE motivo_cancelamento > 12
     * </code>
     *
     * @param     mixed $motivoCancelamento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByMotivoCancelamento($motivoCancelamento = null, $comparison = null)
    {
        if (is_array($motivoCancelamento)) {
            $useMinMax = false;
            if (isset($motivoCancelamento['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_MOTIVO_CANCELAMENTO, $motivoCancelamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($motivoCancelamento['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_MOTIVO_CANCELAMENTO, $motivoCancelamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_MOTIVO_CANCELAMENTO, $motivoCancelamento, $comparison);
    }

    /**
     * Filter the query on the agencia_debito column
     *
     * Example usage:
     * <code>
     * $query->filterByAgenciaDebito('fooValue');   // WHERE agencia_debito = 'fooValue'
     * $query->filterByAgenciaDebito('%fooValue%', Criteria::LIKE); // WHERE agencia_debito LIKE '%fooValue%'
     * </code>
     *
     * @param     string $agenciaDebito The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByAgenciaDebito($agenciaDebito = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($agenciaDebito)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_AGENCIA_DEBITO, $agenciaDebito, $comparison);
    }

    /**
     * Filter the query on the id_cliente_banco column
     *
     * Example usage:
     * <code>
     * $query->filterByIdClienteBanco('fooValue');   // WHERE id_cliente_banco = 'fooValue'
     * $query->filterByIdClienteBanco('%fooValue%', Criteria::LIKE); // WHERE id_cliente_banco LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idClienteBanco The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByIdClienteBanco($idClienteBanco = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idClienteBanco)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_ID_CLIENTE_BANCO, $idClienteBanco, $comparison);
    }

    /**
     * Filter the query on the emissao_nf_21 column
     *
     * Example usage:
     * <code>
     * $query->filterByEmissaoNf21(true); // WHERE emissao_nf_21 = true
     * $query->filterByEmissaoNf21('yes'); // WHERE emissao_nf_21 = true
     * </code>
     *
     * @param     boolean|string $emissaoNf21 The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByEmissaoNf21($emissaoNf21 = null, $comparison = null)
    {
        if (is_string($emissaoNf21)) {
            $emissaoNf21 = in_array(strtolower($emissaoNf21), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_EMISSAO_NF_21, $emissaoNf21, $comparison);
    }

    /**
     * Filter the query on the emissao_nf_pre column
     *
     * Example usage:
     * <code>
     * $query->filterByEmissaoNfPre(true); // WHERE emissao_nf_pre = true
     * $query->filterByEmissaoNfPre('yes'); // WHERE emissao_nf_pre = true
     * </code>
     *
     * @param     boolean|string $emissaoNfPre The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByEmissaoNfPre($emissaoNfPre = null, $comparison = null)
    {
        if (is_string($emissaoNfPre)) {
            $emissaoNfPre = in_array(strtolower($emissaoNfPre), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_EMISSAO_NF_PRE, $emissaoNfPre, $comparison);
    }

    /**
     * Filter the query on the ignora_renovacao_automatica column
     *
     * Example usage:
     * <code>
     * $query->filterByIgnoraRenovacaoAutomatica(true); // WHERE ignora_renovacao_automatica = true
     * $query->filterByIgnoraRenovacaoAutomatica('yes'); // WHERE ignora_renovacao_automatica = true
     * </code>
     *
     * @param     boolean|string $ignoraRenovacaoAutomatica The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByIgnoraRenovacaoAutomatica($ignoraRenovacaoAutomatica = null, $comparison = null)
    {
        if (is_string($ignoraRenovacaoAutomatica)) {
            $ignoraRenovacaoAutomatica = in_array(strtolower($ignoraRenovacaoAutomatica), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_IGNORA_RENOVACAO_AUTOMATICA, $ignoraRenovacaoAutomatica, $comparison);
    }

    /**
     * Filter the query on the comodato column
     *
     * Example usage:
     * <code>
     * $query->filterByComodato(1234); // WHERE comodato = 1234
     * $query->filterByComodato(array(12, 34)); // WHERE comodato IN (12, 34)
     * $query->filterByComodato(array('min' => 12)); // WHERE comodato > 12
     * </code>
     *
     * @param     mixed $comodato The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByComodato($comodato = null, $comparison = null)
    {
        if (is_array($comodato)) {
            $useMinMax = false;
            if (isset($comodato['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_COMODATO, $comodato['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($comodato['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_COMODATO, $comodato['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_COMODATO, $comodato, $comparison);
    }

    /**
     * Filter the query on the empresa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEmpresaId(1234); // WHERE empresa_id = 1234
     * $query->filterByEmpresaId(array(12, 34)); // WHERE empresa_id IN (12, 34)
     * $query->filterByEmpresaId(array('min' => 12)); // WHERE empresa_id > 12
     * </code>
     *
     * @param     mixed $empresaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByEmpresaId($empresaId = null, $comparison = null)
    {
        if (is_array($empresaId)) {
            $useMinMax = false;
            if (isset($empresaId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_EMPRESA_ID, $empresaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($empresaId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_EMPRESA_ID, $empresaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_EMPRESA_ID, $empresaId, $comparison);
    }

    /**
     * Filter the query on the descricao_comodato column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricaoComodato('fooValue');   // WHERE descricao_comodato = 'fooValue'
     * $query->filterByDescricaoComodato('%fooValue%', Criteria::LIKE); // WHERE descricao_comodato LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricaoComodato The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDescricaoComodato($descricaoComodato = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricaoComodato)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DESCRICAO_COMODATO, $descricaoComodato, $comparison);
    }

    /**
     * Filter the query on the data_comodato column
     *
     * Example usage:
     * <code>
     * $query->filterByDataComodato('2011-03-14'); // WHERE data_comodato = '2011-03-14'
     * $query->filterByDataComodato('now'); // WHERE data_comodato = '2011-03-14'
     * $query->filterByDataComodato(array('max' => 'yesterday')); // WHERE data_comodato > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataComodato The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDataComodato($dataComodato = null, $comparison = null)
    {
        if (is_array($dataComodato)) {
            $useMinMax = false;
            if (isset($dataComodato['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_COMODATO, $dataComodato['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataComodato['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_COMODATO, $dataComodato['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DATA_COMODATO, $dataComodato, $comparison);
    }

    /**
     * Filter the query on the obs_comodato column
     *
     * Example usage:
     * <code>
     * $query->filterByObsComodato('fooValue');   // WHERE obs_comodato = 'fooValue'
     * $query->filterByObsComodato('%fooValue%', Criteria::LIKE); // WHERE obs_comodato LIKE '%fooValue%'
     * </code>
     *
     * @param     string $obsComodato The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByObsComodato($obsComodato = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($obsComodato)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_OBS_COMODATO, $obsComodato, $comparison);
    }

    /**
     * Filter the query on the centrocusto column
     *
     * Example usage:
     * <code>
     * $query->filterByCentrocusto(1234); // WHERE centrocusto = 1234
     * $query->filterByCentrocusto(array(12, 34)); // WHERE centrocusto IN (12, 34)
     * $query->filterByCentrocusto(array('min' => 12)); // WHERE centrocusto > 12
     * </code>
     *
     * @param     mixed $centrocusto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByCentrocusto($centrocusto = null, $comparison = null)
    {
        if (is_array($centrocusto)) {
            $useMinMax = false;
            if (isset($centrocusto['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_CENTROCUSTO, $centrocusto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($centrocusto['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_CENTROCUSTO, $centrocusto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_CENTROCUSTO, $centrocusto, $comparison);
    }

    /**
     * Filter the query on the unidade_negocio column
     *
     * Example usage:
     * <code>
     * $query->filterByUnidadeNegocio(1234); // WHERE unidade_negocio = 1234
     * $query->filterByUnidadeNegocio(array(12, 34)); // WHERE unidade_negocio IN (12, 34)
     * $query->filterByUnidadeNegocio(array('min' => 12)); // WHERE unidade_negocio > 12
     * </code>
     *
     * @param     mixed $unidadeNegocio The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByUnidadeNegocio($unidadeNegocio = null, $comparison = null)
    {
        if (is_array($unidadeNegocio)) {
            $useMinMax = false;
            if (isset($unidadeNegocio['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_UNIDADE_NEGOCIO, $unidadeNegocio['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unidadeNegocio['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_UNIDADE_NEGOCIO, $unidadeNegocio['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_UNIDADE_NEGOCIO, $unidadeNegocio, $comparison);
    }

    /**
     * Filter the query on the valor_renovacao column
     *
     * Example usage:
     * <code>
     * $query->filterByValorRenovacao(1234); // WHERE valor_renovacao = 1234
     * $query->filterByValorRenovacao(array(12, 34)); // WHERE valor_renovacao IN (12, 34)
     * $query->filterByValorRenovacao(array('min' => 12)); // WHERE valor_renovacao > 12
     * </code>
     *
     * @param     mixed $valorRenovacao The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByValorRenovacao($valorRenovacao = null, $comparison = null)
    {
        if (is_array($valorRenovacao)) {
            $useMinMax = false;
            if (isset($valorRenovacao['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_VALOR_RENOVACAO, $valorRenovacao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorRenovacao['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_VALOR_RENOVACAO, $valorRenovacao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_VALOR_RENOVACAO, $valorRenovacao, $comparison);
    }

    /**
     * Filter the query on the modelo_nf column
     *
     * Example usage:
     * <code>
     * $query->filterByModeloNf(1234); // WHERE modelo_nf = 1234
     * $query->filterByModeloNf(array(12, 34)); // WHERE modelo_nf IN (12, 34)
     * $query->filterByModeloNf(array('min' => 12)); // WHERE modelo_nf > 12
     * </code>
     *
     * @param     mixed $modeloNf The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByModeloNf($modeloNf = null, $comparison = null)
    {
        if (is_array($modeloNf)) {
            $useMinMax = false;
            if (isset($modeloNf['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_MODELO_NF, $modeloNf['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modeloNf['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_MODELO_NF, $modeloNf['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_MODELO_NF, $modeloNf, $comparison);
    }

    /**
     * Filter the query on the emissao_nfse column
     *
     * Example usage:
     * <code>
     * $query->filterByEmissaoNfse(true); // WHERE emissao_nfse = true
     * $query->filterByEmissaoNfse('yes'); // WHERE emissao_nfse = true
     * </code>
     *
     * @param     boolean|string $emissaoNfse The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByEmissaoNfse($emissaoNfse = null, $comparison = null)
    {
        if (is_string($emissaoNfse)) {
            $emissaoNfse = in_array(strtolower($emissaoNfse), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_EMISSAO_NFSE, $emissaoNfse, $comparison);
    }

    /**
     * Filter the query on the suspenso column
     *
     * Example usage:
     * <code>
     * $query->filterBySuspenso(true); // WHERE suspenso = true
     * $query->filterBySuspenso('yes'); // WHERE suspenso = true
     * </code>
     *
     * @param     boolean|string $suspenso The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterBySuspenso($suspenso = null, $comparison = null)
    {
        if (is_string($suspenso)) {
            $suspenso = in_array(strtolower($suspenso), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_SUSPENSO, $suspenso, $comparison);
    }

    /**
     * Filter the query on the data_suspensao column
     *
     * Example usage:
     * <code>
     * $query->filterByDataSuspensao('2011-03-14'); // WHERE data_suspensao = '2011-03-14'
     * $query->filterByDataSuspensao('now'); // WHERE data_suspensao = '2011-03-14'
     * $query->filterByDataSuspensao(array('max' => 'yesterday')); // WHERE data_suspensao > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataSuspensao The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDataSuspensao($dataSuspensao = null, $comparison = null)
    {
        if (is_array($dataSuspensao)) {
            $useMinMax = false;
            if (isset($dataSuspensao['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_SUSPENSAO, $dataSuspensao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataSuspensao['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_DATA_SUSPENSAO, $dataSuspensao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DATA_SUSPENSAO, $dataSuspensao, $comparison);
    }

    /**
     * Filter the query on the usuario_suspensao column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioSuspensao(1234); // WHERE usuario_suspensao = 1234
     * $query->filterByUsuarioSuspensao(array(12, 34)); // WHERE usuario_suspensao IN (12, 34)
     * $query->filterByUsuarioSuspensao(array('min' => 12)); // WHERE usuario_suspensao > 12
     * </code>
     *
     * @param     mixed $usuarioSuspensao The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByUsuarioSuspensao($usuarioSuspensao = null, $comparison = null)
    {
        if (is_array($usuarioSuspensao)) {
            $useMinMax = false;
            if (isset($usuarioSuspensao['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_USUARIO_SUSPENSAO, $usuarioSuspensao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioSuspensao['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_USUARIO_SUSPENSAO, $usuarioSuspensao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_USUARIO_SUSPENSAO, $usuarioSuspensao, $comparison);
    }

    /**
     * Filter the query on the motivo_suspensao column
     *
     * Example usage:
     * <code>
     * $query->filterByMotivoSuspensao(1234); // WHERE motivo_suspensao = 1234
     * $query->filterByMotivoSuspensao(array(12, 34)); // WHERE motivo_suspensao IN (12, 34)
     * $query->filterByMotivoSuspensao(array('min' => 12)); // WHERE motivo_suspensao > 12
     * </code>
     *
     * @param     mixed $motivoSuspensao The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByMotivoSuspensao($motivoSuspensao = null, $comparison = null)
    {
        if (is_array($motivoSuspensao)) {
            $useMinMax = false;
            if (isset($motivoSuspensao['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_MOTIVO_SUSPENSAO, $motivoSuspensao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($motivoSuspensao['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_MOTIVO_SUSPENSAO, $motivoSuspensao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_MOTIVO_SUSPENSAO, $motivoSuspensao, $comparison);
    }

    /**
     * Filter the query on the versao column
     *
     * Example usage:
     * <code>
     * $query->filterByVersao(1234); // WHERE versao = 1234
     * $query->filterByVersao(array(12, 34)); // WHERE versao IN (12, 34)
     * $query->filterByVersao(array('min' => 12)); // WHERE versao > 12
     * </code>
     *
     * @param     mixed $versao The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByVersao($versao = null, $comparison = null)
    {
        if (is_array($versao)) {
            $useMinMax = false;
            if (isset($versao['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_VERSAO, $versao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($versao['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_VERSAO, $versao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_VERSAO, $versao, $comparison);
    }

    /**
     * Filter the query on the faturamento_automatico column
     *
     * Example usage:
     * <code>
     * $query->filterByFaturamentoAutomatico(true); // WHERE faturamento_automatico = true
     * $query->filterByFaturamentoAutomatico('yes'); // WHERE faturamento_automatico = true
     * </code>
     *
     * @param     boolean|string $faturamentoAutomatico The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByFaturamentoAutomatico($faturamentoAutomatico = null, $comparison = null)
    {
        if (is_string($faturamentoAutomatico)) {
            $faturamentoAutomatico = in_array(strtolower($faturamentoAutomatico), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_FATURAMENTO_AUTOMATICO, $faturamentoAutomatico, $comparison);
    }

    /**
     * Filter the query on the regra_faturamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRegraFaturamentoId(1234); // WHERE regra_faturamento_id = 1234
     * $query->filterByRegraFaturamentoId(array(12, 34)); // WHERE regra_faturamento_id IN (12, 34)
     * $query->filterByRegraFaturamentoId(array('min' => 12)); // WHERE regra_faturamento_id > 12
     * </code>
     *
     * @see       filterByRegraFaturamento()
     *
     * @param     mixed $regraFaturamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByRegraFaturamentoId($regraFaturamentoId = null, $comparison = null)
    {
        if (is_array($regraFaturamentoId)) {
            $useMinMax = false;
            if (isset($regraFaturamentoId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_REGRA_FATURAMENTO_ID, $regraFaturamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($regraFaturamentoId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_REGRA_FATURAMENTO_ID, $regraFaturamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_REGRA_FATURAMENTO_ID, $regraFaturamentoId, $comparison);
    }

    /**
     * Filter the query on the endereco_nf_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEnderecoNfId(1234); // WHERE endereco_nf_id = 1234
     * $query->filterByEnderecoNfId(array(12, 34)); // WHERE endereco_nf_id IN (12, 34)
     * $query->filterByEnderecoNfId(array('min' => 12)); // WHERE endereco_nf_id > 12
     * </code>
     *
     * @param     mixed $enderecoNfId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByEnderecoNfId($enderecoNfId = null, $comparison = null)
    {
        if (is_array($enderecoNfId)) {
            $useMinMax = false;
            if (isset($enderecoNfId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_ENDERECO_NF_ID, $enderecoNfId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enderecoNfId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_ENDERECO_NF_ID, $enderecoNfId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_ENDERECO_NF_ID, $enderecoNfId, $comparison);
    }

    /**
     * Filter the query on the endereco_nfse_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEnderecoNfseId(1234); // WHERE endereco_nfse_id = 1234
     * $query->filterByEnderecoNfseId(array(12, 34)); // WHERE endereco_nfse_id IN (12, 34)
     * $query->filterByEnderecoNfseId(array('min' => 12)); // WHERE endereco_nfse_id > 12
     * </code>
     *
     * @param     mixed $enderecoNfseId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByEnderecoNfseId($enderecoNfseId = null, $comparison = null)
    {
        if (is_array($enderecoNfseId)) {
            $useMinMax = false;
            if (isset($enderecoNfseId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_ENDERECO_NFSE_ID, $enderecoNfseId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($enderecoNfseId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_ENDERECO_NFSE_ID, $enderecoNfseId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_ENDERECO_NFSE_ID, $enderecoNfseId, $comparison);
    }

    /**
     * Filter the query on the obs_nf column
     *
     * Example usage:
     * <code>
     * $query->filterByObsNf('fooValue');   // WHERE obs_nf = 'fooValue'
     * $query->filterByObsNf('%fooValue%', Criteria::LIKE); // WHERE obs_nf LIKE '%fooValue%'
     * </code>
     *
     * @param     string $obsNf The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByObsNf($obsNf = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($obsNf)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_OBS_NF, $obsNf, $comparison);
    }

    /**
     * Filter the query on the tipo_faturamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoFaturamentoId(1234); // WHERE tipo_faturamento_id = 1234
     * $query->filterByTipoFaturamentoId(array(12, 34)); // WHERE tipo_faturamento_id IN (12, 34)
     * $query->filterByTipoFaturamentoId(array('min' => 12)); // WHERE tipo_faturamento_id > 12
     * </code>
     *
     * @param     mixed $tipoFaturamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByTipoFaturamentoId($tipoFaturamentoId = null, $comparison = null)
    {
        if (is_array($tipoFaturamentoId)) {
            $useMinMax = false;
            if (isset($tipoFaturamentoId['min'])) {
                $this->addUsingAlias(ContratoTableMap::COL_TIPO_FATURAMENTO_ID, $tipoFaturamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tipoFaturamentoId['max'])) {
                $this->addUsingAlias(ContratoTableMap::COL_TIPO_FATURAMENTO_ID, $tipoFaturamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_TIPO_FATURAMENTO_ID, $tipoFaturamentoId, $comparison);
    }

    /**
     * Filter the query on the tem_scm column
     *
     * Example usage:
     * <code>
     * $query->filterByTemScm(true); // WHERE tem_scm = true
     * $query->filterByTemScm('yes'); // WHERE tem_scm = true
     * </code>
     *
     * @param     boolean|string $temScm The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByTemScm($temScm = null, $comparison = null)
    {
        if (is_string($temScm)) {
            $temScm = in_array(strtolower($temScm), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_TEM_SCM, $temScm, $comparison);
    }

    /**
     * Filter the query on the scm column
     *
     * Example usage:
     * <code>
     * $query->filterByScm('fooValue');   // WHERE scm = 'fooValue'
     * $query->filterByScm('%fooValue%', Criteria::LIKE); // WHERE scm LIKE '%fooValue%'
     * </code>
     *
     * @param     string $scm The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByScm($scm = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($scm)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_SCM, $scm, $comparison);
    }

    /**
     * Filter the query on the tem_sva column
     *
     * Example usage:
     * <code>
     * $query->filterByTemSva(true); // WHERE tem_sva = true
     * $query->filterByTemSva('yes'); // WHERE tem_sva = true
     * </code>
     *
     * @param     boolean|string $temSva The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByTemSva($temSva = null, $comparison = null)
    {
        if (is_string($temSva)) {
            $temSva = in_array(strtolower($temSva), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_TEM_SVA, $temSva, $comparison);
    }

    /**
     * Filter the query on the sva column
     *
     * Example usage:
     * <code>
     * $query->filterBySva('fooValue');   // WHERE sva = 'fooValue'
     * $query->filterBySva('%fooValue%', Criteria::LIKE); // WHERE sva LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sva The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterBySva($sva = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sva)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_SVA, $sva, $comparison);
    }

    /**
     * Filter the query on the forcar_valor column
     *
     * Example usage:
     * <code>
     * $query->filterByForcarValor(true); // WHERE forcar_valor = true
     * $query->filterByForcarValor('yes'); // WHERE forcar_valor = true
     * </code>
     *
     * @param     boolean|string $forcarValor The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByForcarValor($forcarValor = null, $comparison = null)
    {
        if (is_string($forcarValor)) {
            $forcarValor = in_array(strtolower($forcarValor), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_FORCAR_VALOR, $forcarValor, $comparison);
    }

    /**
     * Filter the query on the descricao_scm column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricaoScm('fooValue');   // WHERE descricao_scm = 'fooValue'
     * $query->filterByDescricaoScm('%fooValue%', Criteria::LIKE); // WHERE descricao_scm LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricaoScm The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDescricaoScm($descricaoScm = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricaoScm)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DESCRICAO_SCM, $descricaoScm, $comparison);
    }

    /**
     * Filter the query on the descricao_sva column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricaoSva('fooValue');   // WHERE descricao_sva = 'fooValue'
     * $query->filterByDescricaoSva('%fooValue%', Criteria::LIKE); // WHERE descricao_sva LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricaoSva The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByDescricaoSva($descricaoSva = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricaoSva)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_DESCRICAO_SVA, $descricaoSva, $comparison);
    }

    /**
     * Filter the query on the tipo_scm column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoScm('fooValue');   // WHERE tipo_scm = 'fooValue'
     * $query->filterByTipoScm('%fooValue%', Criteria::LIKE); // WHERE tipo_scm LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoScm The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByTipoScm($tipoScm = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoScm)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_TIPO_SCM, $tipoScm, $comparison);
    }

    /**
     * Filter the query on the tipo_sva column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoSva('fooValue');   // WHERE tipo_sva = 'fooValue'
     * $query->filterByTipoSva('%fooValue%', Criteria::LIKE); // WHERE tipo_sva LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoSva The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByTipoSva($tipoSva = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoSva)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_TIPO_SVA, $tipoSva, $comparison);
    }

    /**
     * Filter the query on the gerar_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByGerarCobranca(true); // WHERE gerar_cobranca = true
     * $query->filterByGerarCobranca('yes'); // WHERE gerar_cobranca = true
     * </code>
     *
     * @param     boolean|string $gerarCobranca The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByGerarCobranca($gerarCobranca = null, $comparison = null)
    {
        if (is_string($gerarCobranca)) {
            $gerarCobranca = in_array(strtolower($gerarCobranca), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_GERAR_COBRANCA, $gerarCobranca, $comparison);
    }

    /**
     * Filter the query on the codigo_obra column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigoObra('fooValue');   // WHERE codigo_obra = 'fooValue'
     * $query->filterByCodigoObra('%fooValue%', Criteria::LIKE); // WHERE codigo_obra LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigoObra The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByCodigoObra($codigoObra = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigoObra)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_CODIGO_OBRA, $codigoObra, $comparison);
    }

    /**
     * Filter the query on the art column
     *
     * Example usage:
     * <code>
     * $query->filterByArt('fooValue');   // WHERE art = 'fooValue'
     * $query->filterByArt('%fooValue%', Criteria::LIKE); // WHERE art LIKE '%fooValue%'
     * </code>
     *
     * @param     string $art The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByArt($art = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($art)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratoTableMap::COL_ART, $art, $comparison);
    }

    /**
     * Filter the query on the tem_reembolso column
     *
     * Example usage:
     * <code>
     * $query->filterByTemReembolso(true); // WHERE tem_reembolso = true
     * $query->filterByTemReembolso('yes'); // WHERE tem_reembolso = true
     * </code>
     *
     * @param     boolean|string $temReembolso The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function filterByTemReembolso($temReembolso = null, $comparison = null)
    {
        if (is_string($temReembolso)) {
            $temReembolso = in_array(strtolower($temReembolso), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContratoTableMap::COL_TEM_REEMBOLSO, $temReembolso, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoCliente object
     *
     * @param \ImaTelecomBundle\Model\EnderecoCliente|ObjectCollection $enderecoCliente The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContratoQuery The current query, for fluid interface
     */
    public function filterByEnderecoCliente($enderecoCliente, $comparison = null)
    {
        if ($enderecoCliente instanceof \ImaTelecomBundle\Model\EnderecoCliente) {
            return $this
                ->addUsingAlias(ContratoTableMap::COL_ENDERECO_CLIENTE_ID, $enderecoCliente->getIdenderecoCliente(), $comparison);
        } elseif ($enderecoCliente instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContratoTableMap::COL_ENDERECO_CLIENTE_ID, $enderecoCliente->toKeyValue('PrimaryKey', 'IdenderecoCliente'), $comparison);
        } else {
            throw new PropelException('filterByEnderecoCliente() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function joinEnderecoCliente($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoCliente');
        }

        return $this;
    }

    /**
     * Use the EnderecoCliente relation EnderecoCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoClienteQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoClienteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinEnderecoCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoCliente', '\ImaTelecomBundle\Model\EnderecoClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\RegraFaturamento object
     *
     * @param \ImaTelecomBundle\Model\RegraFaturamento|ObjectCollection $regraFaturamento The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContratoQuery The current query, for fluid interface
     */
    public function filterByRegraFaturamento($regraFaturamento, $comparison = null)
    {
        if ($regraFaturamento instanceof \ImaTelecomBundle\Model\RegraFaturamento) {
            return $this
                ->addUsingAlias(ContratoTableMap::COL_REGRA_FATURAMENTO_ID, $regraFaturamento->getIdregraFaturamento(), $comparison);
        } elseif ($regraFaturamento instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContratoTableMap::COL_REGRA_FATURAMENTO_ID, $regraFaturamento->toKeyValue('PrimaryKey', 'IdregraFaturamento'), $comparison);
        } else {
            throw new PropelException('filterByRegraFaturamento() only accepts arguments of type \ImaTelecomBundle\Model\RegraFaturamento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RegraFaturamento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function joinRegraFaturamento($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RegraFaturamento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RegraFaturamento');
        }

        return $this;
    }

    /**
     * Use the RegraFaturamento relation RegraFaturamento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\RegraFaturamentoQuery A secondary query class using the current class as primary query
     */
    public function useRegraFaturamentoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRegraFaturamento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RegraFaturamento', '\ImaTelecomBundle\Model\RegraFaturamentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ServicoCliente object
     *
     * @param \ImaTelecomBundle\Model\ServicoCliente|ObjectCollection $servicoCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildContratoQuery The current query, for fluid interface
     */
    public function filterByServicoCliente($servicoCliente, $comparison = null)
    {
        if ($servicoCliente instanceof \ImaTelecomBundle\Model\ServicoCliente) {
            return $this
                ->addUsingAlias(ContratoTableMap::COL_IDCONTRATO, $servicoCliente->getContratoId(), $comparison);
        } elseif ($servicoCliente instanceof ObjectCollection) {
            return $this
                ->useServicoClienteQuery()
                ->filterByPrimaryKeys($servicoCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicoCliente() only accepts arguments of type \ImaTelecomBundle\Model\ServicoCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicoCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function joinServicoCliente($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicoCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicoCliente');
        }

        return $this;
    }

    /**
     * Use the ServicoCliente relation ServicoCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ServicoClienteQuery A secondary query class using the current class as primary query
     */
    public function useServicoClienteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinServicoCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicoCliente', '\ImaTelecomBundle\Model\ServicoClienteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildContrato $contrato Object to remove from the list of results
     *
     * @return $this|ChildContratoQuery The current query, for fluid interface
     */
    public function prune($contrato = null)
    {
        if ($contrato) {
            $this->addUsingAlias(ContratoTableMap::COL_IDCONTRATO, $contrato->getIdcontrato(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the contrato table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ContratoTableMap::clearInstancePool();
            ContratoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ContratoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ContratoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ContratoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ContratoQuery
