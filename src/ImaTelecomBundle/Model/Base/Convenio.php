<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BancoAgenciaConta as ChildBancoAgenciaConta;
use ImaTelecomBundle\Model\BancoAgenciaContaQuery as ChildBancoAgenciaContaQuery;
use ImaTelecomBundle\Model\Convenio as ChildConvenio;
use ImaTelecomBundle\Model\ConvenioQuery as ChildConvenioQuery;
use ImaTelecomBundle\Model\Empresa as ChildEmpresa;
use ImaTelecomBundle\Model\EmpresaQuery as ChildEmpresaQuery;
use ImaTelecomBundle\Model\Lancamentos as ChildLancamentos;
use ImaTelecomBundle\Model\LancamentosBoletos as ChildLancamentosBoletos;
use ImaTelecomBundle\Model\LancamentosBoletosQuery as ChildLancamentosBoletosQuery;
use ImaTelecomBundle\Model\LancamentosQuery as ChildLancamentosQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BancoAgenciaContaTableMap;
use ImaTelecomBundle\Model\Map\ConvenioTableMap;
use ImaTelecomBundle\Model\Map\LancamentosBoletosTableMap;
use ImaTelecomBundle\Model\Map\LancamentosTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'convenio' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Convenio implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\ConvenioTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idconvenio field.
     *
     * @var        int
     */
    protected $idconvenio;

    /**
     * The value for the codigo_convenio field.
     *
     * @var        string
     */
    protected $codigo_convenio;

    /**
     * The value for the digito_verificador field.
     *
     * @var        string
     */
    protected $digito_verificador;

    /**
     * The value for the nome field.
     *
     * @var        string
     */
    protected $nome;

    /**
     * The value for the ativo field.
     *
     * @var        boolean
     */
    protected $ativo;

    /**
     * The value for the carteira field.
     *
     * @var        string
     */
    protected $carteira;

    /**
     * The value for the tipo_carteira field.
     *
     * @var        string
     */
    protected $tipo_carteira;

    /**
     * The value for the cnpj field.
     *
     * @var        string
     */
    protected $cnpj;

    /**
     * The value for the tipo_cobranca field.
     *
     * @var        string
     */
    protected $tipo_cobranca;

    /**
     * The value for the tipo_nosso_numero field.
     *
     * @var        string
     */
    protected $tipo_nosso_numero;

    /**
     * The value for the limite_inferior field.
     *
     * @var        string
     */
    protected $limite_inferior;

    /**
     * The value for the limite_superior field.
     *
     * @var        string
     */
    protected $limite_superior;

    /**
     * The value for the ultimo_sequencial field.
     *
     * @var        string
     */
    protected $ultimo_sequencial;

    /**
     * The value for the valor_codigo_barra field.
     *
     * @var        string
     */
    protected $valor_codigo_barra;

    /**
     * The value for the ultimo_sequencial_arquivo_cobranca field.
     *
     * @var        string
     */
    protected $ultimo_sequencial_arquivo_cobranca;

    /**
     * The value for the ultimo_sequencial_debito_automatico field.
     *
     * @var        string
     */
    protected $ultimo_sequencial_debito_automatico;

    /**
     * The value for the empresa_id field.
     *
     * @var        int
     */
    protected $empresa_id;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * @var        ChildEmpresa
     */
    protected $aEmpresa;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildBancoAgenciaConta[] Collection to store aggregation of ChildBancoAgenciaConta objects.
     */
    protected $collBancoAgenciaContas;
    protected $collBancoAgenciaContasPartial;

    /**
     * @var        ObjectCollection|ChildLancamentos[] Collection to store aggregation of ChildLancamentos objects.
     */
    protected $collLancamentoss;
    protected $collLancamentossPartial;

    /**
     * @var        ObjectCollection|ChildLancamentosBoletos[] Collection to store aggregation of ChildLancamentosBoletos objects.
     */
    protected $collLancamentosBoletoss;
    protected $collLancamentosBoletossPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBancoAgenciaConta[]
     */
    protected $bancoAgenciaContasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLancamentos[]
     */
    protected $lancamentossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLancamentosBoletos[]
     */
    protected $lancamentosBoletossScheduledForDeletion = null;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Convenio object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Convenio</code> instance.  If
     * <code>obj</code> is an instance of <code>Convenio</code>, delegates to
     * <code>equals(Convenio)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Convenio The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idconvenio] column value.
     *
     * @return int
     */
    public function getIdconvenio()
    {
        return $this->idconvenio;
    }

    /**
     * Get the [codigo_convenio] column value.
     *
     * @return string
     */
    public function getCodigoConvenio()
    {
        return $this->codigo_convenio;
    }

    /**
     * Get the [digito_verificador] column value.
     *
     * @return string
     */
    public function getDigitoVerificador()
    {
        return $this->digito_verificador;
    }

    /**
     * Get the [nome] column value.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->getAtivo();
    }

    /**
     * Get the [carteira] column value.
     *
     * @return string
     */
    public function getCarteira()
    {
        return $this->carteira;
    }

    /**
     * Get the [tipo_carteira] column value.
     *
     * @return string
     */
    public function getTipoCarteira()
    {
        return $this->tipo_carteira;
    }

    /**
     * Get the [cnpj] column value.
     *
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Get the [tipo_cobranca] column value.
     *
     * @return string
     */
    public function getTipoCobranca()
    {
        return $this->tipo_cobranca;
    }

    /**
     * Get the [tipo_nosso_numero] column value.
     *
     * @return string
     */
    public function getTipoNossoNumero()
    {
        return $this->tipo_nosso_numero;
    }

    /**
     * Get the [limite_inferior] column value.
     *
     * @return string
     */
    public function getLimiteInferior()
    {
        return $this->limite_inferior;
    }

    /**
     * Get the [limite_superior] column value.
     *
     * @return string
     */
    public function getLimiteSuperior()
    {
        return $this->limite_superior;
    }

    /**
     * Get the [ultimo_sequencial] column value.
     *
     * @return string
     */
    public function getUltimoSequencial()
    {
        return $this->ultimo_sequencial;
    }

    /**
     * Get the [valor_codigo_barra] column value.
     *
     * @return string
     */
    public function getValorCodigoBarra()
    {
        return $this->valor_codigo_barra;
    }

    /**
     * Get the [ultimo_sequencial_arquivo_cobranca] column value.
     *
     * @return string
     */
    public function getUltimoSequencialArquivoCobranca()
    {
        return $this->ultimo_sequencial_arquivo_cobranca;
    }

    /**
     * Get the [ultimo_sequencial_debito_automatico] column value.
     *
     * @return string
     */
    public function getUltimoSequencialDebitoAutomatico()
    {
        return $this->ultimo_sequencial_debito_automatico;
    }

    /**
     * Get the [empresa_id] column value.
     *
     * @return int
     */
    public function getEmpresaId()
    {
        return $this->empresa_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Set the value of [idconvenio] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setIdconvenio($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idconvenio !== $v) {
            $this->idconvenio = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_IDCONVENIO] = true;
        }

        return $this;
    } // setIdconvenio()

    /**
     * Set the value of [codigo_convenio] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setCodigoConvenio($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo_convenio !== $v) {
            $this->codigo_convenio = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_CODIGO_CONVENIO] = true;
        }

        return $this;
    } // setCodigoConvenio()

    /**
     * Set the value of [digito_verificador] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setDigitoVerificador($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->digito_verificador !== $v) {
            $this->digito_verificador = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_DIGITO_VERIFICADOR] = true;
        }

        return $this;
    } // setDigitoVerificador()

    /**
     * Set the value of [nome] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setNome($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome !== $v) {
            $this->nome = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_NOME] = true;
        }

        return $this;
    } // setNome()

    /**
     * Sets the value of the [ativo] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setAtivo($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ativo !== $v) {
            $this->ativo = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_ATIVO] = true;
        }

        return $this;
    } // setAtivo()

    /**
     * Set the value of [carteira] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setCarteira($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->carteira !== $v) {
            $this->carteira = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_CARTEIRA] = true;
        }

        return $this;
    } // setCarteira()

    /**
     * Set the value of [tipo_carteira] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setTipoCarteira($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_carteira !== $v) {
            $this->tipo_carteira = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_TIPO_CARTEIRA] = true;
        }

        return $this;
    } // setTipoCarteira()

    /**
     * Set the value of [cnpj] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setCnpj($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cnpj !== $v) {
            $this->cnpj = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_CNPJ] = true;
        }

        return $this;
    } // setCnpj()

    /**
     * Set the value of [tipo_cobranca] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setTipoCobranca($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_cobranca !== $v) {
            $this->tipo_cobranca = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_TIPO_COBRANCA] = true;
        }

        return $this;
    } // setTipoCobranca()

    /**
     * Set the value of [tipo_nosso_numero] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setTipoNossoNumero($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_nosso_numero !== $v) {
            $this->tipo_nosso_numero = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_TIPO_NOSSO_NUMERO] = true;
        }

        return $this;
    } // setTipoNossoNumero()

    /**
     * Set the value of [limite_inferior] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setLimiteInferior($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->limite_inferior !== $v) {
            $this->limite_inferior = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_LIMITE_INFERIOR] = true;
        }

        return $this;
    } // setLimiteInferior()

    /**
     * Set the value of [limite_superior] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setLimiteSuperior($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->limite_superior !== $v) {
            $this->limite_superior = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_LIMITE_SUPERIOR] = true;
        }

        return $this;
    } // setLimiteSuperior()

    /**
     * Set the value of [ultimo_sequencial] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setUltimoSequencial($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ultimo_sequencial !== $v) {
            $this->ultimo_sequencial = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_ULTIMO_SEQUENCIAL] = true;
        }

        return $this;
    } // setUltimoSequencial()

    /**
     * Set the value of [valor_codigo_barra] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setValorCodigoBarra($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_codigo_barra !== $v) {
            $this->valor_codigo_barra = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_VALOR_CODIGO_BARRA] = true;
        }

        return $this;
    } // setValorCodigoBarra()

    /**
     * Set the value of [ultimo_sequencial_arquivo_cobranca] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setUltimoSequencialArquivoCobranca($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ultimo_sequencial_arquivo_cobranca !== $v) {
            $this->ultimo_sequencial_arquivo_cobranca = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_ARQUIVO_COBRANCA] = true;
        }

        return $this;
    } // setUltimoSequencialArquivoCobranca()

    /**
     * Set the value of [ultimo_sequencial_debito_automatico] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setUltimoSequencialDebitoAutomatico($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ultimo_sequencial_debito_automatico !== $v) {
            $this->ultimo_sequencial_debito_automatico = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_DEBITO_AUTOMATICO] = true;
        }

        return $this;
    } // setUltimoSequencialDebitoAutomatico()

    /**
     * Set the value of [empresa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setEmpresaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->empresa_id !== $v) {
            $this->empresa_id = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_EMPRESA_ID] = true;
        }

        if ($this->aEmpresa !== null && $this->aEmpresa->getIdempresa() !== $v) {
            $this->aEmpresa = null;
        }

        return $this;
    } // setEmpresaId()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ConvenioTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ConvenioTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[ConvenioTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ConvenioTableMap::translateFieldName('Idconvenio', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idconvenio = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ConvenioTableMap::translateFieldName('CodigoConvenio', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codigo_convenio = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ConvenioTableMap::translateFieldName('DigitoVerificador', TableMap::TYPE_PHPNAME, $indexType)];
            $this->digito_verificador = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ConvenioTableMap::translateFieldName('Nome', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nome = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ConvenioTableMap::translateFieldName('Ativo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ativo = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ConvenioTableMap::translateFieldName('Carteira', TableMap::TYPE_PHPNAME, $indexType)];
            $this->carteira = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ConvenioTableMap::translateFieldName('TipoCarteira', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_carteira = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ConvenioTableMap::translateFieldName('Cnpj', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cnpj = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ConvenioTableMap::translateFieldName('TipoCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_cobranca = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ConvenioTableMap::translateFieldName('TipoNossoNumero', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_nosso_numero = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ConvenioTableMap::translateFieldName('LimiteInferior', TableMap::TYPE_PHPNAME, $indexType)];
            $this->limite_inferior = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ConvenioTableMap::translateFieldName('LimiteSuperior', TableMap::TYPE_PHPNAME, $indexType)];
            $this->limite_superior = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ConvenioTableMap::translateFieldName('UltimoSequencial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ultimo_sequencial = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ConvenioTableMap::translateFieldName('ValorCodigoBarra', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_codigo_barra = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ConvenioTableMap::translateFieldName('UltimoSequencialArquivoCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ultimo_sequencial_arquivo_cobranca = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ConvenioTableMap::translateFieldName('UltimoSequencialDebitoAutomatico', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ultimo_sequencial_debito_automatico = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ConvenioTableMap::translateFieldName('EmpresaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->empresa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ConvenioTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ConvenioTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ConvenioTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 20; // 20 = ConvenioTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Convenio'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aEmpresa !== null && $this->empresa_id !== $this->aEmpresa->getIdempresa()) {
            $this->aEmpresa = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ConvenioTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildConvenioQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEmpresa = null;
            $this->aUsuario = null;
            $this->collBancoAgenciaContas = null;

            $this->collLancamentoss = null;

            $this->collLancamentosBoletoss = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Convenio::setDeleted()
     * @see Convenio::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConvenioTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildConvenioQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConvenioTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ConvenioTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEmpresa !== null) {
                if ($this->aEmpresa->isModified() || $this->aEmpresa->isNew()) {
                    $affectedRows += $this->aEmpresa->save($con);
                }
                $this->setEmpresa($this->aEmpresa);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->bancoAgenciaContasScheduledForDeletion !== null) {
                if (!$this->bancoAgenciaContasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BancoAgenciaContaQuery::create()
                        ->filterByPrimaryKeys($this->bancoAgenciaContasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bancoAgenciaContasScheduledForDeletion = null;
                }
            }

            if ($this->collBancoAgenciaContas !== null) {
                foreach ($this->collBancoAgenciaContas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lancamentossScheduledForDeletion !== null) {
                if (!$this->lancamentossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\LancamentosQuery::create()
                        ->filterByPrimaryKeys($this->lancamentossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lancamentossScheduledForDeletion = null;
                }
            }

            if ($this->collLancamentoss !== null) {
                foreach ($this->collLancamentoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lancamentosBoletossScheduledForDeletion !== null) {
                if (!$this->lancamentosBoletossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\LancamentosBoletosQuery::create()
                        ->filterByPrimaryKeys($this->lancamentosBoletossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lancamentosBoletossScheduledForDeletion = null;
                }
            }

            if ($this->collLancamentosBoletoss !== null) {
                foreach ($this->collLancamentosBoletoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ConvenioTableMap::COL_IDCONVENIO] = true;
        if (null !== $this->idconvenio) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ConvenioTableMap::COL_IDCONVENIO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ConvenioTableMap::COL_IDCONVENIO)) {
            $modifiedColumns[':p' . $index++]  = 'idconvenio';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_CODIGO_CONVENIO)) {
            $modifiedColumns[':p' . $index++]  = 'codigo_convenio';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_DIGITO_VERIFICADOR)) {
            $modifiedColumns[':p' . $index++]  = 'digito_verificador';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_NOME)) {
            $modifiedColumns[':p' . $index++]  = 'nome';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_ATIVO)) {
            $modifiedColumns[':p' . $index++]  = 'ativo';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_CARTEIRA)) {
            $modifiedColumns[':p' . $index++]  = 'carteira';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_TIPO_CARTEIRA)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_carteira';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_CNPJ)) {
            $modifiedColumns[':p' . $index++]  = 'cnpj';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_TIPO_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_cobranca';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_TIPO_NOSSO_NUMERO)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_nosso_numero';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_LIMITE_INFERIOR)) {
            $modifiedColumns[':p' . $index++]  = 'limite_inferior';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_LIMITE_SUPERIOR)) {
            $modifiedColumns[':p' . $index++]  = 'limite_superior';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL)) {
            $modifiedColumns[':p' . $index++]  = 'ultimo_sequencial';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_VALOR_CODIGO_BARRA)) {
            $modifiedColumns[':p' . $index++]  = 'valor_codigo_barra';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_ARQUIVO_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'ultimo_sequencial_arquivo_cobranca';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_DEBITO_AUTOMATICO)) {
            $modifiedColumns[':p' . $index++]  = 'ultimo_sequencial_debito_automatico';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_EMPRESA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'empresa_id';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }

        $sql = sprintf(
            'INSERT INTO convenio (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idconvenio':
                        $stmt->bindValue($identifier, $this->idconvenio, PDO::PARAM_INT);
                        break;
                    case 'codigo_convenio':
                        $stmt->bindValue($identifier, $this->codigo_convenio, PDO::PARAM_STR);
                        break;
                    case 'digito_verificador':
                        $stmt->bindValue($identifier, $this->digito_verificador, PDO::PARAM_STR);
                        break;
                    case 'nome':
                        $stmt->bindValue($identifier, $this->nome, PDO::PARAM_STR);
                        break;
                    case 'ativo':
                        $stmt->bindValue($identifier, (int) $this->ativo, PDO::PARAM_INT);
                        break;
                    case 'carteira':
                        $stmt->bindValue($identifier, $this->carteira, PDO::PARAM_STR);
                        break;
                    case 'tipo_carteira':
                        $stmt->bindValue($identifier, $this->tipo_carteira, PDO::PARAM_STR);
                        break;
                    case 'cnpj':
                        $stmt->bindValue($identifier, $this->cnpj, PDO::PARAM_STR);
                        break;
                    case 'tipo_cobranca':
                        $stmt->bindValue($identifier, $this->tipo_cobranca, PDO::PARAM_STR);
                        break;
                    case 'tipo_nosso_numero':
                        $stmt->bindValue($identifier, $this->tipo_nosso_numero, PDO::PARAM_STR);
                        break;
                    case 'limite_inferior':
                        $stmt->bindValue($identifier, $this->limite_inferior, PDO::PARAM_STR);
                        break;
                    case 'limite_superior':
                        $stmt->bindValue($identifier, $this->limite_superior, PDO::PARAM_STR);
                        break;
                    case 'ultimo_sequencial':
                        $stmt->bindValue($identifier, $this->ultimo_sequencial, PDO::PARAM_STR);
                        break;
                    case 'valor_codigo_barra':
                        $stmt->bindValue($identifier, $this->valor_codigo_barra, PDO::PARAM_STR);
                        break;
                    case 'ultimo_sequencial_arquivo_cobranca':
                        $stmt->bindValue($identifier, $this->ultimo_sequencial_arquivo_cobranca, PDO::PARAM_STR);
                        break;
                    case 'ultimo_sequencial_debito_automatico':
                        $stmt->bindValue($identifier, $this->ultimo_sequencial_debito_automatico, PDO::PARAM_STR);
                        break;
                    case 'empresa_id':
                        $stmt->bindValue($identifier, $this->empresa_id, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdconvenio($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ConvenioTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdconvenio();
                break;
            case 1:
                return $this->getCodigoConvenio();
                break;
            case 2:
                return $this->getDigitoVerificador();
                break;
            case 3:
                return $this->getNome();
                break;
            case 4:
                return $this->getAtivo();
                break;
            case 5:
                return $this->getCarteira();
                break;
            case 6:
                return $this->getTipoCarteira();
                break;
            case 7:
                return $this->getCnpj();
                break;
            case 8:
                return $this->getTipoCobranca();
                break;
            case 9:
                return $this->getTipoNossoNumero();
                break;
            case 10:
                return $this->getLimiteInferior();
                break;
            case 11:
                return $this->getLimiteSuperior();
                break;
            case 12:
                return $this->getUltimoSequencial();
                break;
            case 13:
                return $this->getValorCodigoBarra();
                break;
            case 14:
                return $this->getUltimoSequencialArquivoCobranca();
                break;
            case 15:
                return $this->getUltimoSequencialDebitoAutomatico();
                break;
            case 16:
                return $this->getEmpresaId();
                break;
            case 17:
                return $this->getDataCadastro();
                break;
            case 18:
                return $this->getDataAlterado();
                break;
            case 19:
                return $this->getUsuarioAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Convenio'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Convenio'][$this->hashCode()] = true;
        $keys = ConvenioTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdconvenio(),
            $keys[1] => $this->getCodigoConvenio(),
            $keys[2] => $this->getDigitoVerificador(),
            $keys[3] => $this->getNome(),
            $keys[4] => $this->getAtivo(),
            $keys[5] => $this->getCarteira(),
            $keys[6] => $this->getTipoCarteira(),
            $keys[7] => $this->getCnpj(),
            $keys[8] => $this->getTipoCobranca(),
            $keys[9] => $this->getTipoNossoNumero(),
            $keys[10] => $this->getLimiteInferior(),
            $keys[11] => $this->getLimiteSuperior(),
            $keys[12] => $this->getUltimoSequencial(),
            $keys[13] => $this->getValorCodigoBarra(),
            $keys[14] => $this->getUltimoSequencialArquivoCobranca(),
            $keys[15] => $this->getUltimoSequencialDebitoAutomatico(),
            $keys[16] => $this->getEmpresaId(),
            $keys[17] => $this->getDataCadastro(),
            $keys[18] => $this->getDataAlterado(),
            $keys[19] => $this->getUsuarioAlterado(),
        );
        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEmpresa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'empresa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'empresa';
                        break;
                    default:
                        $key = 'Empresa';
                }

                $result[$key] = $this->aEmpresa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBancoAgenciaContas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bancoAgenciaContas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'banco_agencia_contas';
                        break;
                    default:
                        $key = 'BancoAgenciaContas';
                }

                $result[$key] = $this->collBancoAgenciaContas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLancamentoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lancamentoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lancamentoss';
                        break;
                    default:
                        $key = 'Lancamentoss';
                }

                $result[$key] = $this->collLancamentoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLancamentosBoletoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lancamentosBoletoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lancamentos_boletoss';
                        break;
                    default:
                        $key = 'LancamentosBoletoss';
                }

                $result[$key] = $this->collLancamentosBoletoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Convenio
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ConvenioTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Convenio
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdconvenio($value);
                break;
            case 1:
                $this->setCodigoConvenio($value);
                break;
            case 2:
                $this->setDigitoVerificador($value);
                break;
            case 3:
                $this->setNome($value);
                break;
            case 4:
                $this->setAtivo($value);
                break;
            case 5:
                $this->setCarteira($value);
                break;
            case 6:
                $this->setTipoCarteira($value);
                break;
            case 7:
                $this->setCnpj($value);
                break;
            case 8:
                $this->setTipoCobranca($value);
                break;
            case 9:
                $this->setTipoNossoNumero($value);
                break;
            case 10:
                $this->setLimiteInferior($value);
                break;
            case 11:
                $this->setLimiteSuperior($value);
                break;
            case 12:
                $this->setUltimoSequencial($value);
                break;
            case 13:
                $this->setValorCodigoBarra($value);
                break;
            case 14:
                $this->setUltimoSequencialArquivoCobranca($value);
                break;
            case 15:
                $this->setUltimoSequencialDebitoAutomatico($value);
                break;
            case 16:
                $this->setEmpresaId($value);
                break;
            case 17:
                $this->setDataCadastro($value);
                break;
            case 18:
                $this->setDataAlterado($value);
                break;
            case 19:
                $this->setUsuarioAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ConvenioTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdconvenio($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setCodigoConvenio($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDigitoVerificador($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNome($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setAtivo($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCarteira($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setTipoCarteira($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCnpj($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTipoCobranca($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTipoNossoNumero($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setLimiteInferior($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setLimiteSuperior($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUltimoSequencial($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setValorCodigoBarra($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setUltimoSequencialArquivoCobranca($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setUltimoSequencialDebitoAutomatico($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setEmpresaId($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setDataCadastro($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setDataAlterado($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[19]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ConvenioTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ConvenioTableMap::COL_IDCONVENIO)) {
            $criteria->add(ConvenioTableMap::COL_IDCONVENIO, $this->idconvenio);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_CODIGO_CONVENIO)) {
            $criteria->add(ConvenioTableMap::COL_CODIGO_CONVENIO, $this->codigo_convenio);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_DIGITO_VERIFICADOR)) {
            $criteria->add(ConvenioTableMap::COL_DIGITO_VERIFICADOR, $this->digito_verificador);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_NOME)) {
            $criteria->add(ConvenioTableMap::COL_NOME, $this->nome);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_ATIVO)) {
            $criteria->add(ConvenioTableMap::COL_ATIVO, $this->ativo);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_CARTEIRA)) {
            $criteria->add(ConvenioTableMap::COL_CARTEIRA, $this->carteira);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_TIPO_CARTEIRA)) {
            $criteria->add(ConvenioTableMap::COL_TIPO_CARTEIRA, $this->tipo_carteira);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_CNPJ)) {
            $criteria->add(ConvenioTableMap::COL_CNPJ, $this->cnpj);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_TIPO_COBRANCA)) {
            $criteria->add(ConvenioTableMap::COL_TIPO_COBRANCA, $this->tipo_cobranca);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_TIPO_NOSSO_NUMERO)) {
            $criteria->add(ConvenioTableMap::COL_TIPO_NOSSO_NUMERO, $this->tipo_nosso_numero);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_LIMITE_INFERIOR)) {
            $criteria->add(ConvenioTableMap::COL_LIMITE_INFERIOR, $this->limite_inferior);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_LIMITE_SUPERIOR)) {
            $criteria->add(ConvenioTableMap::COL_LIMITE_SUPERIOR, $this->limite_superior);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL)) {
            $criteria->add(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL, $this->ultimo_sequencial);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_VALOR_CODIGO_BARRA)) {
            $criteria->add(ConvenioTableMap::COL_VALOR_CODIGO_BARRA, $this->valor_codigo_barra);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_ARQUIVO_COBRANCA)) {
            $criteria->add(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_ARQUIVO_COBRANCA, $this->ultimo_sequencial_arquivo_cobranca);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_DEBITO_AUTOMATICO)) {
            $criteria->add(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_DEBITO_AUTOMATICO, $this->ultimo_sequencial_debito_automatico);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_EMPRESA_ID)) {
            $criteria->add(ConvenioTableMap::COL_EMPRESA_ID, $this->empresa_id);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(ConvenioTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(ConvenioTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(ConvenioTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(ConvenioTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildConvenioQuery::create();
        $criteria->add(ConvenioTableMap::COL_IDCONVENIO, $this->idconvenio);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdconvenio();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdconvenio();
    }

    /**
     * Generic method to set the primary key (idconvenio column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdconvenio($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdconvenio();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Convenio (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setCodigoConvenio($this->getCodigoConvenio());
        $copyObj->setDigitoVerificador($this->getDigitoVerificador());
        $copyObj->setNome($this->getNome());
        $copyObj->setAtivo($this->getAtivo());
        $copyObj->setCarteira($this->getCarteira());
        $copyObj->setTipoCarteira($this->getTipoCarteira());
        $copyObj->setCnpj($this->getCnpj());
        $copyObj->setTipoCobranca($this->getTipoCobranca());
        $copyObj->setTipoNossoNumero($this->getTipoNossoNumero());
        $copyObj->setLimiteInferior($this->getLimiteInferior());
        $copyObj->setLimiteSuperior($this->getLimiteSuperior());
        $copyObj->setUltimoSequencial($this->getUltimoSequencial());
        $copyObj->setValorCodigoBarra($this->getValorCodigoBarra());
        $copyObj->setUltimoSequencialArquivoCobranca($this->getUltimoSequencialArquivoCobranca());
        $copyObj->setUltimoSequencialDebitoAutomatico($this->getUltimoSequencialDebitoAutomatico());
        $copyObj->setEmpresaId($this->getEmpresaId());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBancoAgenciaContas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBancoAgenciaConta($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLancamentoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLancamentos($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLancamentosBoletoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLancamentosBoletos($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdconvenio(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Convenio Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEmpresa object.
     *
     * @param  ChildEmpresa $v
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEmpresa(ChildEmpresa $v = null)
    {
        if ($v === null) {
            $this->setEmpresaId(NULL);
        } else {
            $this->setEmpresaId($v->getIdempresa());
        }

        $this->aEmpresa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEmpresa object, it will not be re-added.
        if ($v !== null) {
            $v->addConvenio($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEmpresa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEmpresa The associated ChildEmpresa object.
     * @throws PropelException
     */
    public function getEmpresa(ConnectionInterface $con = null)
    {
        if ($this->aEmpresa === null && ($this->empresa_id !== null)) {
            $this->aEmpresa = ChildEmpresaQuery::create()->findPk($this->empresa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEmpresa->addConvenios($this);
             */
        }

        return $this->aEmpresa;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addConvenio($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addConvenios($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('BancoAgenciaConta' == $relationName) {
            return $this->initBancoAgenciaContas();
        }
        if ('Lancamentos' == $relationName) {
            return $this->initLancamentoss();
        }
        if ('LancamentosBoletos' == $relationName) {
            return $this->initLancamentosBoletoss();
        }
    }

    /**
     * Clears out the collBancoAgenciaContas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBancoAgenciaContas()
     */
    public function clearBancoAgenciaContas()
    {
        $this->collBancoAgenciaContas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBancoAgenciaContas collection loaded partially.
     */
    public function resetPartialBancoAgenciaContas($v = true)
    {
        $this->collBancoAgenciaContasPartial = $v;
    }

    /**
     * Initializes the collBancoAgenciaContas collection.
     *
     * By default this just sets the collBancoAgenciaContas collection to an empty array (like clearcollBancoAgenciaContas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBancoAgenciaContas($overrideExisting = true)
    {
        if (null !== $this->collBancoAgenciaContas && !$overrideExisting) {
            return;
        }

        $collectionClassName = BancoAgenciaContaTableMap::getTableMap()->getCollectionClassName();

        $this->collBancoAgenciaContas = new $collectionClassName;
        $this->collBancoAgenciaContas->setModel('\ImaTelecomBundle\Model\BancoAgenciaConta');
    }

    /**
     * Gets an array of ChildBancoAgenciaConta objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildConvenio is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBancoAgenciaConta[] List of ChildBancoAgenciaConta objects
     * @throws PropelException
     */
    public function getBancoAgenciaContas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBancoAgenciaContasPartial && !$this->isNew();
        if (null === $this->collBancoAgenciaContas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBancoAgenciaContas) {
                // return empty collection
                $this->initBancoAgenciaContas();
            } else {
                $collBancoAgenciaContas = ChildBancoAgenciaContaQuery::create(null, $criteria)
                    ->filterByConvenio($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBancoAgenciaContasPartial && count($collBancoAgenciaContas)) {
                        $this->initBancoAgenciaContas(false);

                        foreach ($collBancoAgenciaContas as $obj) {
                            if (false == $this->collBancoAgenciaContas->contains($obj)) {
                                $this->collBancoAgenciaContas->append($obj);
                            }
                        }

                        $this->collBancoAgenciaContasPartial = true;
                    }

                    return $collBancoAgenciaContas;
                }

                if ($partial && $this->collBancoAgenciaContas) {
                    foreach ($this->collBancoAgenciaContas as $obj) {
                        if ($obj->isNew()) {
                            $collBancoAgenciaContas[] = $obj;
                        }
                    }
                }

                $this->collBancoAgenciaContas = $collBancoAgenciaContas;
                $this->collBancoAgenciaContasPartial = false;
            }
        }

        return $this->collBancoAgenciaContas;
    }

    /**
     * Sets a collection of ChildBancoAgenciaConta objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bancoAgenciaContas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildConvenio The current object (for fluent API support)
     */
    public function setBancoAgenciaContas(Collection $bancoAgenciaContas, ConnectionInterface $con = null)
    {
        /** @var ChildBancoAgenciaConta[] $bancoAgenciaContasToDelete */
        $bancoAgenciaContasToDelete = $this->getBancoAgenciaContas(new Criteria(), $con)->diff($bancoAgenciaContas);


        $this->bancoAgenciaContasScheduledForDeletion = $bancoAgenciaContasToDelete;

        foreach ($bancoAgenciaContasToDelete as $bancoAgenciaContaRemoved) {
            $bancoAgenciaContaRemoved->setConvenio(null);
        }

        $this->collBancoAgenciaContas = null;
        foreach ($bancoAgenciaContas as $bancoAgenciaConta) {
            $this->addBancoAgenciaConta($bancoAgenciaConta);
        }

        $this->collBancoAgenciaContas = $bancoAgenciaContas;
        $this->collBancoAgenciaContasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BancoAgenciaConta objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BancoAgenciaConta objects.
     * @throws PropelException
     */
    public function countBancoAgenciaContas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBancoAgenciaContasPartial && !$this->isNew();
        if (null === $this->collBancoAgenciaContas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBancoAgenciaContas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBancoAgenciaContas());
            }

            $query = ChildBancoAgenciaContaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByConvenio($this)
                ->count($con);
        }

        return count($this->collBancoAgenciaContas);
    }

    /**
     * Method called to associate a ChildBancoAgenciaConta object to this object
     * through the ChildBancoAgenciaConta foreign key attribute.
     *
     * @param  ChildBancoAgenciaConta $l ChildBancoAgenciaConta
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function addBancoAgenciaConta(ChildBancoAgenciaConta $l)
    {
        if ($this->collBancoAgenciaContas === null) {
            $this->initBancoAgenciaContas();
            $this->collBancoAgenciaContasPartial = true;
        }

        if (!$this->collBancoAgenciaContas->contains($l)) {
            $this->doAddBancoAgenciaConta($l);

            if ($this->bancoAgenciaContasScheduledForDeletion and $this->bancoAgenciaContasScheduledForDeletion->contains($l)) {
                $this->bancoAgenciaContasScheduledForDeletion->remove($this->bancoAgenciaContasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBancoAgenciaConta $bancoAgenciaConta The ChildBancoAgenciaConta object to add.
     */
    protected function doAddBancoAgenciaConta(ChildBancoAgenciaConta $bancoAgenciaConta)
    {
        $this->collBancoAgenciaContas[]= $bancoAgenciaConta;
        $bancoAgenciaConta->setConvenio($this);
    }

    /**
     * @param  ChildBancoAgenciaConta $bancoAgenciaConta The ChildBancoAgenciaConta object to remove.
     * @return $this|ChildConvenio The current object (for fluent API support)
     */
    public function removeBancoAgenciaConta(ChildBancoAgenciaConta $bancoAgenciaConta)
    {
        if ($this->getBancoAgenciaContas()->contains($bancoAgenciaConta)) {
            $pos = $this->collBancoAgenciaContas->search($bancoAgenciaConta);
            $this->collBancoAgenciaContas->remove($pos);
            if (null === $this->bancoAgenciaContasScheduledForDeletion) {
                $this->bancoAgenciaContasScheduledForDeletion = clone $this->collBancoAgenciaContas;
                $this->bancoAgenciaContasScheduledForDeletion->clear();
            }
            $this->bancoAgenciaContasScheduledForDeletion[]= clone $bancoAgenciaConta;
            $bancoAgenciaConta->setConvenio(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Convenio is new, it will return
     * an empty collection; or if this Convenio has previously
     * been saved, it will retrieve related BancoAgenciaContas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Convenio.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBancoAgenciaConta[] List of ChildBancoAgenciaConta objects
     */
    public function getBancoAgenciaContasJoinBancoAgencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBancoAgenciaContaQuery::create(null, $criteria);
        $query->joinWith('BancoAgencia', $joinBehavior);

        return $this->getBancoAgenciaContas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Convenio is new, it will return
     * an empty collection; or if this Convenio has previously
     * been saved, it will retrieve related BancoAgenciaContas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Convenio.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBancoAgenciaConta[] List of ChildBancoAgenciaConta objects
     */
    public function getBancoAgenciaContasJoinBanco(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBancoAgenciaContaQuery::create(null, $criteria);
        $query->joinWith('Banco', $joinBehavior);

        return $this->getBancoAgenciaContas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Convenio is new, it will return
     * an empty collection; or if this Convenio has previously
     * been saved, it will retrieve related BancoAgenciaContas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Convenio.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBancoAgenciaConta[] List of ChildBancoAgenciaConta objects
     */
    public function getBancoAgenciaContasJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBancoAgenciaContaQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBancoAgenciaContas($query, $con);
    }

    /**
     * Clears out the collLancamentoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLancamentoss()
     */
    public function clearLancamentoss()
    {
        $this->collLancamentoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLancamentoss collection loaded partially.
     */
    public function resetPartialLancamentoss($v = true)
    {
        $this->collLancamentossPartial = $v;
    }

    /**
     * Initializes the collLancamentoss collection.
     *
     * By default this just sets the collLancamentoss collection to an empty array (like clearcollLancamentoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLancamentoss($overrideExisting = true)
    {
        if (null !== $this->collLancamentoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = LancamentosTableMap::getTableMap()->getCollectionClassName();

        $this->collLancamentoss = new $collectionClassName;
        $this->collLancamentoss->setModel('\ImaTelecomBundle\Model\Lancamentos');
    }

    /**
     * Gets an array of ChildLancamentos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildConvenio is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLancamentos[] List of ChildLancamentos objects
     * @throws PropelException
     */
    public function getLancamentoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentossPartial && !$this->isNew();
        if (null === $this->collLancamentoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLancamentoss) {
                // return empty collection
                $this->initLancamentoss();
            } else {
                $collLancamentoss = ChildLancamentosQuery::create(null, $criteria)
                    ->filterByConvenio($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLancamentossPartial && count($collLancamentoss)) {
                        $this->initLancamentoss(false);

                        foreach ($collLancamentoss as $obj) {
                            if (false == $this->collLancamentoss->contains($obj)) {
                                $this->collLancamentoss->append($obj);
                            }
                        }

                        $this->collLancamentossPartial = true;
                    }

                    return $collLancamentoss;
                }

                if ($partial && $this->collLancamentoss) {
                    foreach ($this->collLancamentoss as $obj) {
                        if ($obj->isNew()) {
                            $collLancamentoss[] = $obj;
                        }
                    }
                }

                $this->collLancamentoss = $collLancamentoss;
                $this->collLancamentossPartial = false;
            }
        }

        return $this->collLancamentoss;
    }

    /**
     * Sets a collection of ChildLancamentos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lancamentoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildConvenio The current object (for fluent API support)
     */
    public function setLancamentoss(Collection $lancamentoss, ConnectionInterface $con = null)
    {
        /** @var ChildLancamentos[] $lancamentossToDelete */
        $lancamentossToDelete = $this->getLancamentoss(new Criteria(), $con)->diff($lancamentoss);


        $this->lancamentossScheduledForDeletion = $lancamentossToDelete;

        foreach ($lancamentossToDelete as $lancamentosRemoved) {
            $lancamentosRemoved->setConvenio(null);
        }

        $this->collLancamentoss = null;
        foreach ($lancamentoss as $lancamentos) {
            $this->addLancamentos($lancamentos);
        }

        $this->collLancamentoss = $lancamentoss;
        $this->collLancamentossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Lancamentos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Lancamentos objects.
     * @throws PropelException
     */
    public function countLancamentoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentossPartial && !$this->isNew();
        if (null === $this->collLancamentoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLancamentoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLancamentoss());
            }

            $query = ChildLancamentosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByConvenio($this)
                ->count($con);
        }

        return count($this->collLancamentoss);
    }

    /**
     * Method called to associate a ChildLancamentos object to this object
     * through the ChildLancamentos foreign key attribute.
     *
     * @param  ChildLancamentos $l ChildLancamentos
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function addLancamentos(ChildLancamentos $l)
    {
        if ($this->collLancamentoss === null) {
            $this->initLancamentoss();
            $this->collLancamentossPartial = true;
        }

        if (!$this->collLancamentoss->contains($l)) {
            $this->doAddLancamentos($l);

            if ($this->lancamentossScheduledForDeletion and $this->lancamentossScheduledForDeletion->contains($l)) {
                $this->lancamentossScheduledForDeletion->remove($this->lancamentossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLancamentos $lancamentos The ChildLancamentos object to add.
     */
    protected function doAddLancamentos(ChildLancamentos $lancamentos)
    {
        $this->collLancamentoss[]= $lancamentos;
        $lancamentos->setConvenio($this);
    }

    /**
     * @param  ChildLancamentos $lancamentos The ChildLancamentos object to remove.
     * @return $this|ChildConvenio The current object (for fluent API support)
     */
    public function removeLancamentos(ChildLancamentos $lancamentos)
    {
        if ($this->getLancamentoss()->contains($lancamentos)) {
            $pos = $this->collLancamentoss->search($lancamentos);
            $this->collLancamentoss->remove($pos);
            if (null === $this->lancamentossScheduledForDeletion) {
                $this->lancamentossScheduledForDeletion = clone $this->collLancamentoss;
                $this->lancamentossScheduledForDeletion->clear();
            }
            $this->lancamentossScheduledForDeletion[]= clone $lancamentos;
            $lancamentos->setConvenio(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Convenio is new, it will return
     * an empty collection; or if this Convenio has previously
     * been saved, it will retrieve related Lancamentoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Convenio.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentos[] List of ChildLancamentos objects
     */
    public function getLancamentossJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getLancamentoss($query, $con);
    }

    /**
     * Clears out the collLancamentosBoletoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLancamentosBoletoss()
     */
    public function clearLancamentosBoletoss()
    {
        $this->collLancamentosBoletoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLancamentosBoletoss collection loaded partially.
     */
    public function resetPartialLancamentosBoletoss($v = true)
    {
        $this->collLancamentosBoletossPartial = $v;
    }

    /**
     * Initializes the collLancamentosBoletoss collection.
     *
     * By default this just sets the collLancamentosBoletoss collection to an empty array (like clearcollLancamentosBoletoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLancamentosBoletoss($overrideExisting = true)
    {
        if (null !== $this->collLancamentosBoletoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = LancamentosBoletosTableMap::getTableMap()->getCollectionClassName();

        $this->collLancamentosBoletoss = new $collectionClassName;
        $this->collLancamentosBoletoss->setModel('\ImaTelecomBundle\Model\LancamentosBoletos');
    }

    /**
     * Gets an array of ChildLancamentosBoletos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildConvenio is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     * @throws PropelException
     */
    public function getLancamentosBoletoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentosBoletossPartial && !$this->isNew();
        if (null === $this->collLancamentosBoletoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLancamentosBoletoss) {
                // return empty collection
                $this->initLancamentosBoletoss();
            } else {
                $collLancamentosBoletoss = ChildLancamentosBoletosQuery::create(null, $criteria)
                    ->filterByConvenio($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLancamentosBoletossPartial && count($collLancamentosBoletoss)) {
                        $this->initLancamentosBoletoss(false);

                        foreach ($collLancamentosBoletoss as $obj) {
                            if (false == $this->collLancamentosBoletoss->contains($obj)) {
                                $this->collLancamentosBoletoss->append($obj);
                            }
                        }

                        $this->collLancamentosBoletossPartial = true;
                    }

                    return $collLancamentosBoletoss;
                }

                if ($partial && $this->collLancamentosBoletoss) {
                    foreach ($this->collLancamentosBoletoss as $obj) {
                        if ($obj->isNew()) {
                            $collLancamentosBoletoss[] = $obj;
                        }
                    }
                }

                $this->collLancamentosBoletoss = $collLancamentosBoletoss;
                $this->collLancamentosBoletossPartial = false;
            }
        }

        return $this->collLancamentosBoletoss;
    }

    /**
     * Sets a collection of ChildLancamentosBoletos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lancamentosBoletoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildConvenio The current object (for fluent API support)
     */
    public function setLancamentosBoletoss(Collection $lancamentosBoletoss, ConnectionInterface $con = null)
    {
        /** @var ChildLancamentosBoletos[] $lancamentosBoletossToDelete */
        $lancamentosBoletossToDelete = $this->getLancamentosBoletoss(new Criteria(), $con)->diff($lancamentosBoletoss);


        $this->lancamentosBoletossScheduledForDeletion = $lancamentosBoletossToDelete;

        foreach ($lancamentosBoletossToDelete as $lancamentosBoletosRemoved) {
            $lancamentosBoletosRemoved->setConvenio(null);
        }

        $this->collLancamentosBoletoss = null;
        foreach ($lancamentosBoletoss as $lancamentosBoletos) {
            $this->addLancamentosBoletos($lancamentosBoletos);
        }

        $this->collLancamentosBoletoss = $lancamentosBoletoss;
        $this->collLancamentosBoletossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LancamentosBoletos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related LancamentosBoletos objects.
     * @throws PropelException
     */
    public function countLancamentosBoletoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentosBoletossPartial && !$this->isNew();
        if (null === $this->collLancamentosBoletoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLancamentosBoletoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLancamentosBoletoss());
            }

            $query = ChildLancamentosBoletosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByConvenio($this)
                ->count($con);
        }

        return count($this->collLancamentosBoletoss);
    }

    /**
     * Method called to associate a ChildLancamentosBoletos object to this object
     * through the ChildLancamentosBoletos foreign key attribute.
     *
     * @param  ChildLancamentosBoletos $l ChildLancamentosBoletos
     * @return $this|\ImaTelecomBundle\Model\Convenio The current object (for fluent API support)
     */
    public function addLancamentosBoletos(ChildLancamentosBoletos $l)
    {
        if ($this->collLancamentosBoletoss === null) {
            $this->initLancamentosBoletoss();
            $this->collLancamentosBoletossPartial = true;
        }

        if (!$this->collLancamentosBoletoss->contains($l)) {
            $this->doAddLancamentosBoletos($l);

            if ($this->lancamentosBoletossScheduledForDeletion and $this->lancamentosBoletossScheduledForDeletion->contains($l)) {
                $this->lancamentosBoletossScheduledForDeletion->remove($this->lancamentosBoletossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLancamentosBoletos $lancamentosBoletos The ChildLancamentosBoletos object to add.
     */
    protected function doAddLancamentosBoletos(ChildLancamentosBoletos $lancamentosBoletos)
    {
        $this->collLancamentosBoletoss[]= $lancamentosBoletos;
        $lancamentosBoletos->setConvenio($this);
    }

    /**
     * @param  ChildLancamentosBoletos $lancamentosBoletos The ChildLancamentosBoletos object to remove.
     * @return $this|ChildConvenio The current object (for fluent API support)
     */
    public function removeLancamentosBoletos(ChildLancamentosBoletos $lancamentosBoletos)
    {
        if ($this->getLancamentosBoletoss()->contains($lancamentosBoletos)) {
            $pos = $this->collLancamentosBoletoss->search($lancamentosBoletos);
            $this->collLancamentosBoletoss->remove($pos);
            if (null === $this->lancamentosBoletossScheduledForDeletion) {
                $this->lancamentosBoletossScheduledForDeletion = clone $this->collLancamentosBoletoss;
                $this->lancamentosBoletossScheduledForDeletion->clear();
            }
            $this->lancamentosBoletossScheduledForDeletion[]= clone $lancamentosBoletos;
            $lancamentosBoletos->setConvenio(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Convenio is new, it will return
     * an empty collection; or if this Convenio has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Convenio.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Convenio is new, it will return
     * an empty collection; or if this Convenio has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Convenio.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Convenio is new, it will return
     * an empty collection; or if this Convenio has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Convenio.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinLancamentos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Lancamentos', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEmpresa) {
            $this->aEmpresa->removeConvenio($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeConvenio($this);
        }
        $this->idconvenio = null;
        $this->codigo_convenio = null;
        $this->digito_verificador = null;
        $this->nome = null;
        $this->ativo = null;
        $this->carteira = null;
        $this->tipo_carteira = null;
        $this->cnpj = null;
        $this->tipo_cobranca = null;
        $this->tipo_nosso_numero = null;
        $this->limite_inferior = null;
        $this->limite_superior = null;
        $this->ultimo_sequencial = null;
        $this->valor_codigo_barra = null;
        $this->ultimo_sequencial_arquivo_cobranca = null;
        $this->ultimo_sequencial_debito_automatico = null;
        $this->empresa_id = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBancoAgenciaContas) {
                foreach ($this->collBancoAgenciaContas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLancamentoss) {
                foreach ($this->collLancamentoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLancamentosBoletoss) {
                foreach ($this->collLancamentosBoletoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBancoAgenciaContas = null;
        $this->collLancamentoss = null;
        $this->collLancamentosBoletoss = null;
        $this->aEmpresa = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ConvenioTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
