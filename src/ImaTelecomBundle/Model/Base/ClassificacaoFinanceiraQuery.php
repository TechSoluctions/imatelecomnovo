<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ClassificacaoFinanceira as ChildClassificacaoFinanceira;
use ImaTelecomBundle\Model\ClassificacaoFinanceiraQuery as ChildClassificacaoFinanceiraQuery;
use ImaTelecomBundle\Model\Map\ClassificacaoFinanceiraTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'classificacao_financeira' table.
 *
 *
 *
 * @method     ChildClassificacaoFinanceiraQuery orderByIdclassificacaoFinanceira($order = Criteria::ASC) Order by the idclassificacao_financeira column
 * @method     ChildClassificacaoFinanceiraQuery orderByCodigo($order = Criteria::ASC) Order by the codigo column
 * @method     ChildClassificacaoFinanceiraQuery orderByDescricao($order = Criteria::ASC) Order by the descricao column
 * @method     ChildClassificacaoFinanceiraQuery orderByPai($order = Criteria::ASC) Order by the pai column
 *
 * @method     ChildClassificacaoFinanceiraQuery groupByIdclassificacaoFinanceira() Group by the idclassificacao_financeira column
 * @method     ChildClassificacaoFinanceiraQuery groupByCodigo() Group by the codigo column
 * @method     ChildClassificacaoFinanceiraQuery groupByDescricao() Group by the descricao column
 * @method     ChildClassificacaoFinanceiraQuery groupByPai() Group by the pai column
 *
 * @method     ChildClassificacaoFinanceiraQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildClassificacaoFinanceiraQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildClassificacaoFinanceiraQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildClassificacaoFinanceiraQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildClassificacaoFinanceiraQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildClassificacaoFinanceiraQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildClassificacaoFinanceiraQuery leftJoinItemDeCompra($relationAlias = null) Adds a LEFT JOIN clause to the query using the ItemDeCompra relation
 * @method     ChildClassificacaoFinanceiraQuery rightJoinItemDeCompra($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ItemDeCompra relation
 * @method     ChildClassificacaoFinanceiraQuery innerJoinItemDeCompra($relationAlias = null) Adds a INNER JOIN clause to the query using the ItemDeCompra relation
 *
 * @method     ChildClassificacaoFinanceiraQuery joinWithItemDeCompra($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ItemDeCompra relation
 *
 * @method     ChildClassificacaoFinanceiraQuery leftJoinWithItemDeCompra() Adds a LEFT JOIN clause and with to the query using the ItemDeCompra relation
 * @method     ChildClassificacaoFinanceiraQuery rightJoinWithItemDeCompra() Adds a RIGHT JOIN clause and with to the query using the ItemDeCompra relation
 * @method     ChildClassificacaoFinanceiraQuery innerJoinWithItemDeCompra() Adds a INNER JOIN clause and with to the query using the ItemDeCompra relation
 *
 * @method     \ImaTelecomBundle\Model\ItemDeCompraQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildClassificacaoFinanceira findOne(ConnectionInterface $con = null) Return the first ChildClassificacaoFinanceira matching the query
 * @method     ChildClassificacaoFinanceira findOneOrCreate(ConnectionInterface $con = null) Return the first ChildClassificacaoFinanceira matching the query, or a new ChildClassificacaoFinanceira object populated from the query conditions when no match is found
 *
 * @method     ChildClassificacaoFinanceira findOneByIdclassificacaoFinanceira(int $idclassificacao_financeira) Return the first ChildClassificacaoFinanceira filtered by the idclassificacao_financeira column
 * @method     ChildClassificacaoFinanceira findOneByCodigo(string $codigo) Return the first ChildClassificacaoFinanceira filtered by the codigo column
 * @method     ChildClassificacaoFinanceira findOneByDescricao(string $descricao) Return the first ChildClassificacaoFinanceira filtered by the descricao column
 * @method     ChildClassificacaoFinanceira findOneByPai(int $pai) Return the first ChildClassificacaoFinanceira filtered by the pai column *

 * @method     ChildClassificacaoFinanceira requirePk($key, ConnectionInterface $con = null) Return the ChildClassificacaoFinanceira by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassificacaoFinanceira requireOne(ConnectionInterface $con = null) Return the first ChildClassificacaoFinanceira matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClassificacaoFinanceira requireOneByIdclassificacaoFinanceira(int $idclassificacao_financeira) Return the first ChildClassificacaoFinanceira filtered by the idclassificacao_financeira column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassificacaoFinanceira requireOneByCodigo(string $codigo) Return the first ChildClassificacaoFinanceira filtered by the codigo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassificacaoFinanceira requireOneByDescricao(string $descricao) Return the first ChildClassificacaoFinanceira filtered by the descricao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClassificacaoFinanceira requireOneByPai(int $pai) Return the first ChildClassificacaoFinanceira filtered by the pai column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClassificacaoFinanceira[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildClassificacaoFinanceira objects based on current ModelCriteria
 * @method     ChildClassificacaoFinanceira[]|ObjectCollection findByIdclassificacaoFinanceira(int $idclassificacao_financeira) Return ChildClassificacaoFinanceira objects filtered by the idclassificacao_financeira column
 * @method     ChildClassificacaoFinanceira[]|ObjectCollection findByCodigo(string $codigo) Return ChildClassificacaoFinanceira objects filtered by the codigo column
 * @method     ChildClassificacaoFinanceira[]|ObjectCollection findByDescricao(string $descricao) Return ChildClassificacaoFinanceira objects filtered by the descricao column
 * @method     ChildClassificacaoFinanceira[]|ObjectCollection findByPai(int $pai) Return ChildClassificacaoFinanceira objects filtered by the pai column
 * @method     ChildClassificacaoFinanceira[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ClassificacaoFinanceiraQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ClassificacaoFinanceiraQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ClassificacaoFinanceira', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildClassificacaoFinanceiraQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildClassificacaoFinanceiraQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildClassificacaoFinanceiraQuery) {
            return $criteria;
        }
        $query = new ChildClassificacaoFinanceiraQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildClassificacaoFinanceira|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ClassificacaoFinanceiraTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ClassificacaoFinanceiraTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClassificacaoFinanceira A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idclassificacao_financeira, codigo, descricao, pai FROM classificacao_financeira WHERE idclassificacao_financeira = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildClassificacaoFinanceira $obj */
            $obj = new ChildClassificacaoFinanceira();
            $obj->hydrate($row);
            ClassificacaoFinanceiraTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildClassificacaoFinanceira|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildClassificacaoFinanceiraQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildClassificacaoFinanceiraQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idclassificacao_financeira column
     *
     * Example usage:
     * <code>
     * $query->filterByIdclassificacaoFinanceira(1234); // WHERE idclassificacao_financeira = 1234
     * $query->filterByIdclassificacaoFinanceira(array(12, 34)); // WHERE idclassificacao_financeira IN (12, 34)
     * $query->filterByIdclassificacaoFinanceira(array('min' => 12)); // WHERE idclassificacao_financeira > 12
     * </code>
     *
     * @param     mixed $idclassificacaoFinanceira The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassificacaoFinanceiraQuery The current query, for fluid interface
     */
    public function filterByIdclassificacaoFinanceira($idclassificacaoFinanceira = null, $comparison = null)
    {
        if (is_array($idclassificacaoFinanceira)) {
            $useMinMax = false;
            if (isset($idclassificacaoFinanceira['min'])) {
                $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA, $idclassificacaoFinanceira['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idclassificacaoFinanceira['max'])) {
                $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA, $idclassificacaoFinanceira['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA, $idclassificacaoFinanceira, $comparison);
    }

    /**
     * Filter the query on the codigo column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigo('fooValue');   // WHERE codigo = 'fooValue'
     * $query->filterByCodigo('%fooValue%', Criteria::LIKE); // WHERE codigo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassificacaoFinanceiraQuery The current query, for fluid interface
     */
    public function filterByCodigo($codigo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_CODIGO, $codigo, $comparison);
    }

    /**
     * Filter the query on the descricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricao('fooValue');   // WHERE descricao = 'fooValue'
     * $query->filterByDescricao('%fooValue%', Criteria::LIKE); // WHERE descricao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassificacaoFinanceiraQuery The current query, for fluid interface
     */
    public function filterByDescricao($descricao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_DESCRICAO, $descricao, $comparison);
    }

    /**
     * Filter the query on the pai column
     *
     * Example usage:
     * <code>
     * $query->filterByPai(1234); // WHERE pai = 1234
     * $query->filterByPai(array(12, 34)); // WHERE pai IN (12, 34)
     * $query->filterByPai(array('min' => 12)); // WHERE pai > 12
     * </code>
     *
     * @param     mixed $pai The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClassificacaoFinanceiraQuery The current query, for fluid interface
     */
    public function filterByPai($pai = null, $comparison = null)
    {
        if (is_array($pai)) {
            $useMinMax = false;
            if (isset($pai['min'])) {
                $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_PAI, $pai['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pai['max'])) {
                $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_PAI, $pai['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_PAI, $pai, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ItemDeCompra object
     *
     * @param \ImaTelecomBundle\Model\ItemDeCompra|ObjectCollection $itemDeCompra the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildClassificacaoFinanceiraQuery The current query, for fluid interface
     */
    public function filterByItemDeCompra($itemDeCompra, $comparison = null)
    {
        if ($itemDeCompra instanceof \ImaTelecomBundle\Model\ItemDeCompra) {
            return $this
                ->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA, $itemDeCompra->getClassificacaoFinanceiraId(), $comparison);
        } elseif ($itemDeCompra instanceof ObjectCollection) {
            return $this
                ->useItemDeCompraQuery()
                ->filterByPrimaryKeys($itemDeCompra->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByItemDeCompra() only accepts arguments of type \ImaTelecomBundle\Model\ItemDeCompra or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ItemDeCompra relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClassificacaoFinanceiraQuery The current query, for fluid interface
     */
    public function joinItemDeCompra($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ItemDeCompra');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ItemDeCompra');
        }

        return $this;
    }

    /**
     * Use the ItemDeCompra relation ItemDeCompra object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ItemDeCompraQuery A secondary query class using the current class as primary query
     */
    public function useItemDeCompraQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinItemDeCompra($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ItemDeCompra', '\ImaTelecomBundle\Model\ItemDeCompraQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildClassificacaoFinanceira $classificacaoFinanceira Object to remove from the list of results
     *
     * @return $this|ChildClassificacaoFinanceiraQuery The current query, for fluid interface
     */
    public function prune($classificacaoFinanceira = null)
    {
        if ($classificacaoFinanceira) {
            $this->addUsingAlias(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA, $classificacaoFinanceira->getIdclassificacaoFinanceira(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the classificacao_financeira table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassificacaoFinanceiraTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ClassificacaoFinanceiraTableMap::clearInstancePool();
            ClassificacaoFinanceiraTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassificacaoFinanceiraTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ClassificacaoFinanceiraTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ClassificacaoFinanceiraTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ClassificacaoFinanceiraTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ClassificacaoFinanceiraQuery
