<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Banco as ChildBanco;
use ImaTelecomBundle\Model\BancoQuery as ChildBancoQuery;
use ImaTelecomBundle\Model\LayoutBancario as ChildLayoutBancario;
use ImaTelecomBundle\Model\LayoutBancarioQuery as ChildLayoutBancarioQuery;
use ImaTelecomBundle\Model\Map\BancoTableMap;
use ImaTelecomBundle\Model\Map\LayoutBancarioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'layout_bancario' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class LayoutBancario implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\LayoutBancarioTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idlayout_bancario field.
     *
     * @var        int
     */
    protected $idlayout_bancario;

    /**
     * The value for the tamanho_nosso_numero field.
     *
     * @var        int
     */
    protected $tamanho_nosso_numero;

    /**
     * The value for the posicao_nosso_numero_inicial field.
     *
     * @var        int
     */
    protected $posicao_nosso_numero_inicial;

    /**
     * The value for the posicao_nosso_numero_final field.
     *
     * @var        int
     */
    protected $posicao_nosso_numero_final;

    /**
     * The value for the posicao_chave_lancamento_inicial field.
     *
     * @var        int
     */
    protected $posicao_chave_lancamento_inicial;

    /**
     * The value for the posicao_chave_lancamento_final field.
     *
     * @var        int
     */
    protected $posicao_chave_lancamento_final;

    /**
     * The value for the posicao_chave_lancamento2_inicial field.
     *
     * @var        int
     */
    protected $posicao_chave_lancamento2_inicial;

    /**
     * The value for the posicao_chave_lancamento2_final field.
     *
     * @var        int
     */
    protected $posicao_chave_lancamento2_final;

    /**
     * The value for the posicao_valor_original_inicial field.
     *
     * @var        int
     */
    protected $posicao_valor_original_inicial;

    /**
     * The value for the posicao_valor_original_final field.
     *
     * @var        int
     */
    protected $posicao_valor_original_final;

    /**
     * The value for the posicao_valor_baixa_inicial field.
     *
     * @var        int
     */
    protected $posicao_valor_baixa_inicial;

    /**
     * The value for the posicao_valor_baixa_final field.
     *
     * @var        int
     */
    protected $posicao_valor_baixa_final;

    /**
     * The value for the posicao_valor_juros_inicial field.
     *
     * @var        int
     */
    protected $posicao_valor_juros_inicial;

    /**
     * The value for the posicao_valor_juros_final field.
     *
     * @var        int
     */
    protected $posicao_valor_juros_final;

    /**
     * The value for the posicao_valor_multa_inicial field.
     *
     * @var        int
     */
    protected $posicao_valor_multa_inicial;

    /**
     * The value for the posicao_valor_multa_final field.
     *
     * @var        int
     */
    protected $posicao_valor_multa_final;

    /**
     * The value for the posicao_valor_desconto_inicial field.
     *
     * @var        int
     */
    protected $posicao_valor_desconto_inicial;

    /**
     * The value for the posicao_valor_desconto_final field.
     *
     * @var        int
     */
    protected $posicao_valor_desconto_final;

    /**
     * The value for the posicao_codigo_retorno_inicial field.
     *
     * @var        int
     */
    protected $posicao_codigo_retorno_inicial;

    /**
     * The value for the posicao_codigo_retorno_final field.
     *
     * @var        int
     */
    protected $posicao_codigo_retorno_final;

    /**
     * The value for the posicao_quantidade_erros_inicial field.
     *
     * @var        int
     */
    protected $posicao_quantidade_erros_inicial;

    /**
     * The value for the posicao_quantidade_erros_final field.
     *
     * @var        int
     */
    protected $posicao_quantidade_erros_final;

    /**
     * The value for the posicao_numero_documento_inicial field.
     *
     * @var        int
     */
    protected $posicao_numero_documento_inicial;

    /**
     * The value for the posicao_numero_documento_final field.
     *
     * @var        int
     */
    protected $posicao_numero_documento_final;

    /**
     * The value for the posicao_data_baixa_inicial field.
     *
     * @var        int
     */
    protected $posicao_data_baixa_inicial;

    /**
     * The value for the posicao_data_baixa_final field.
     *
     * @var        int
     */
    protected $posicao_data_baixa_final;

    /**
     * The value for the formato_data_baixa field.
     *
     * @var        string
     */
    protected $formato_data_baixa;

    /**
     * The value for the posicao_data_compensacao_inicial field.
     *
     * @var        int
     */
    protected $posicao_data_compensacao_inicial;

    /**
     * The value for the posicao_data_compensacao_final field.
     *
     * @var        int
     */
    protected $posicao_data_compensacao_final;

    /**
     * The value for the formato_data_compensacao field.
     *
     * @var        string
     */
    protected $formato_data_compensacao;

    /**
     * The value for the posicao_campo_complementar_inicial field.
     *
     * @var        int
     */
    protected $posicao_campo_complementar_inicial;

    /**
     * The value for the posicao_campo_complementar_final field.
     *
     * @var        int
     */
    protected $posicao_campo_complementar_final;

    /**
     * The value for the formato_complementar field.
     *
     * @var        string
     */
    protected $formato_complementar;

    /**
     * The value for the posicao_data_complementar_inicial field.
     *
     * @var        int
     */
    protected $posicao_data_complementar_inicial;

    /**
     * The value for the posicao_data_complementar_final field.
     *
     * @var        int
     */
    protected $posicao_data_complementar_final;

    /**
     * The value for the formato_data_complementar field.
     *
     * @var        string
     */
    protected $formato_data_complementar;

    /**
     * The value for the codigo_retorno_baixa field.
     *
     * @var        string
     */
    protected $codigo_retorno_baixa;

    /**
     * The value for the codigo_retorno_resgistrado field.
     *
     * @var        string
     */
    protected $codigo_retorno_resgistrado;

    /**
     * The value for the codigo_retorno_recusado field.
     *
     * @var        string
     */
    protected $codigo_retorno_recusado;

    /**
     * The value for the data_cadastrado field.
     *
     * @var        DateTime
     */
    protected $data_cadastrado;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * @var        ObjectCollection|ChildBanco[] Collection to store aggregation of ChildBanco objects.
     */
    protected $collBancos;
    protected $collBancosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBanco[]
     */
    protected $bancosScheduledForDeletion = null;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\LayoutBancario object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>LayoutBancario</code> instance.  If
     * <code>obj</code> is an instance of <code>LayoutBancario</code>, delegates to
     * <code>equals(LayoutBancario)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|LayoutBancario The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idlayout_bancario] column value.
     *
     * @return int
     */
    public function getIdlayoutBancario()
    {
        return $this->idlayout_bancario;
    }

    /**
     * Get the [tamanho_nosso_numero] column value.
     *
     * @return int
     */
    public function getTamanhoNossoNumero()
    {
        return $this->tamanho_nosso_numero;
    }

    /**
     * Get the [posicao_nosso_numero_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoNossoNumeroInicial()
    {
        return $this->posicao_nosso_numero_inicial;
    }

    /**
     * Get the [posicao_nosso_numero_final] column value.
     *
     * @return int
     */
    public function getPosicaoNossoNumeroFinal()
    {
        return $this->posicao_nosso_numero_final;
    }

    /**
     * Get the [posicao_chave_lancamento_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoChaveLancamentoInicial()
    {
        return $this->posicao_chave_lancamento_inicial;
    }

    /**
     * Get the [posicao_chave_lancamento_final] column value.
     *
     * @return int
     */
    public function getPosicaoChaveLancamentoFinal()
    {
        return $this->posicao_chave_lancamento_final;
    }

    /**
     * Get the [posicao_chave_lancamento2_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoChaveLancamento2Inicial()
    {
        return $this->posicao_chave_lancamento2_inicial;
    }

    /**
     * Get the [posicao_chave_lancamento2_final] column value.
     *
     * @return int
     */
    public function getPosicaoChaveLancamento2Final()
    {
        return $this->posicao_chave_lancamento2_final;
    }

    /**
     * Get the [posicao_valor_original_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoValorOriginalInicial()
    {
        return $this->posicao_valor_original_inicial;
    }

    /**
     * Get the [posicao_valor_original_final] column value.
     *
     * @return int
     */
    public function getPosicaoValorOriginalFinal()
    {
        return $this->posicao_valor_original_final;
    }

    /**
     * Get the [posicao_valor_baixa_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoValorBaixaInicial()
    {
        return $this->posicao_valor_baixa_inicial;
    }

    /**
     * Get the [posicao_valor_baixa_final] column value.
     *
     * @return int
     */
    public function getPosicaoValorBaixaFinal()
    {
        return $this->posicao_valor_baixa_final;
    }

    /**
     * Get the [posicao_valor_juros_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoValorJurosInicial()
    {
        return $this->posicao_valor_juros_inicial;
    }

    /**
     * Get the [posicao_valor_juros_final] column value.
     *
     * @return int
     */
    public function getPosicaoValorJurosFinal()
    {
        return $this->posicao_valor_juros_final;
    }

    /**
     * Get the [posicao_valor_multa_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoValorMultaInicial()
    {
        return $this->posicao_valor_multa_inicial;
    }

    /**
     * Get the [posicao_valor_multa_final] column value.
     *
     * @return int
     */
    public function getPosicaoValorMultaFinal()
    {
        return $this->posicao_valor_multa_final;
    }

    /**
     * Get the [posicao_valor_desconto_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoValorDescontoInicial()
    {
        return $this->posicao_valor_desconto_inicial;
    }

    /**
     * Get the [posicao_valor_desconto_final] column value.
     *
     * @return int
     */
    public function getPosicaoValorDescontoFinal()
    {
        return $this->posicao_valor_desconto_final;
    }

    /**
     * Get the [posicao_codigo_retorno_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoCodigoRetornoInicial()
    {
        return $this->posicao_codigo_retorno_inicial;
    }

    /**
     * Get the [posicao_codigo_retorno_final] column value.
     *
     * @return int
     */
    public function getPosicaoCodigoRetornoFinal()
    {
        return $this->posicao_codigo_retorno_final;
    }

    /**
     * Get the [posicao_quantidade_erros_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoQuantidadeErrosInicial()
    {
        return $this->posicao_quantidade_erros_inicial;
    }

    /**
     * Get the [posicao_quantidade_erros_final] column value.
     *
     * @return int
     */
    public function getPosicaoQuantidadeErrosFinal()
    {
        return $this->posicao_quantidade_erros_final;
    }

    /**
     * Get the [posicao_numero_documento_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoNumeroDocumentoInicial()
    {
        return $this->posicao_numero_documento_inicial;
    }

    /**
     * Get the [posicao_numero_documento_final] column value.
     *
     * @return int
     */
    public function getPosicaoNumeroDocumentoFinal()
    {
        return $this->posicao_numero_documento_final;
    }

    /**
     * Get the [posicao_data_baixa_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoDataBaixaInicial()
    {
        return $this->posicao_data_baixa_inicial;
    }

    /**
     * Get the [posicao_data_baixa_final] column value.
     *
     * @return int
     */
    public function getPosicaoDataBaixaFinal()
    {
        return $this->posicao_data_baixa_final;
    }

    /**
     * Get the [formato_data_baixa] column value.
     *
     * @return string
     */
    public function getFormatoDataBaixa()
    {
        return $this->formato_data_baixa;
    }

    /**
     * Get the [posicao_data_compensacao_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoDataCompensacaoInicial()
    {
        return $this->posicao_data_compensacao_inicial;
    }

    /**
     * Get the [posicao_data_compensacao_final] column value.
     *
     * @return int
     */
    public function getPosicaoDataCompensacaoFinal()
    {
        return $this->posicao_data_compensacao_final;
    }

    /**
     * Get the [formato_data_compensacao] column value.
     *
     * @return string
     */
    public function getFormatoDataCompensacao()
    {
        return $this->formato_data_compensacao;
    }

    /**
     * Get the [posicao_campo_complementar_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoCampoComplementarInicial()
    {
        return $this->posicao_campo_complementar_inicial;
    }

    /**
     * Get the [posicao_campo_complementar_final] column value.
     *
     * @return int
     */
    public function getPosicaoCampoComplementarFinal()
    {
        return $this->posicao_campo_complementar_final;
    }

    /**
     * Get the [formato_complementar] column value.
     *
     * @return string
     */
    public function getFormatoComplementar()
    {
        return $this->formato_complementar;
    }

    /**
     * Get the [posicao_data_complementar_inicial] column value.
     *
     * @return int
     */
    public function getPosicaoDataComplementarInicial()
    {
        return $this->posicao_data_complementar_inicial;
    }

    /**
     * Get the [posicao_data_complementar_final] column value.
     *
     * @return int
     */
    public function getPosicaoDataComplementarFinal()
    {
        return $this->posicao_data_complementar_final;
    }

    /**
     * Get the [formato_data_complementar] column value.
     *
     * @return string
     */
    public function getFormatoDataComplementar()
    {
        return $this->formato_data_complementar;
    }

    /**
     * Get the [codigo_retorno_baixa] column value.
     *
     * @return string
     */
    public function getCodigoRetornoBaixa()
    {
        return $this->codigo_retorno_baixa;
    }

    /**
     * Get the [codigo_retorno_resgistrado] column value.
     *
     * @return string
     */
    public function getCodigoRetornoResgistrado()
    {
        return $this->codigo_retorno_resgistrado;
    }

    /**
     * Get the [codigo_retorno_recusado] column value.
     *
     * @return string
     */
    public function getCodigoRetornoRecusado()
    {
        return $this->codigo_retorno_recusado;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastrado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastrado($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastrado;
        } else {
            return $this->data_cadastrado instanceof \DateTimeInterface ? $this->data_cadastrado->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Set the value of [idlayout_bancario] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setIdlayoutBancario($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idlayout_bancario !== $v) {
            $this->idlayout_bancario = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO] = true;
        }

        return $this;
    } // setIdlayoutBancario()

    /**
     * Set the value of [tamanho_nosso_numero] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setTamanhoNossoNumero($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->tamanho_nosso_numero !== $v) {
            $this->tamanho_nosso_numero = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO] = true;
        }

        return $this;
    } // setTamanhoNossoNumero()

    /**
     * Set the value of [posicao_nosso_numero_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoNossoNumeroInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_nosso_numero_inicial !== $v) {
            $this->posicao_nosso_numero_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL] = true;
        }

        return $this;
    } // setPosicaoNossoNumeroInicial()

    /**
     * Set the value of [posicao_nosso_numero_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoNossoNumeroFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_nosso_numero_final !== $v) {
            $this->posicao_nosso_numero_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL] = true;
        }

        return $this;
    } // setPosicaoNossoNumeroFinal()

    /**
     * Set the value of [posicao_chave_lancamento_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoChaveLancamentoInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_chave_lancamento_inicial !== $v) {
            $this->posicao_chave_lancamento_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL] = true;
        }

        return $this;
    } // setPosicaoChaveLancamentoInicial()

    /**
     * Set the value of [posicao_chave_lancamento_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoChaveLancamentoFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_chave_lancamento_final !== $v) {
            $this->posicao_chave_lancamento_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL] = true;
        }

        return $this;
    } // setPosicaoChaveLancamentoFinal()

    /**
     * Set the value of [posicao_chave_lancamento2_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoChaveLancamento2Inicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_chave_lancamento2_inicial !== $v) {
            $this->posicao_chave_lancamento2_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL] = true;
        }

        return $this;
    } // setPosicaoChaveLancamento2Inicial()

    /**
     * Set the value of [posicao_chave_lancamento2_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoChaveLancamento2Final($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_chave_lancamento2_final !== $v) {
            $this->posicao_chave_lancamento2_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL] = true;
        }

        return $this;
    } // setPosicaoChaveLancamento2Final()

    /**
     * Set the value of [posicao_valor_original_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorOriginalInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_original_inicial !== $v) {
            $this->posicao_valor_original_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL] = true;
        }

        return $this;
    } // setPosicaoValorOriginalInicial()

    /**
     * Set the value of [posicao_valor_original_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorOriginalFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_original_final !== $v) {
            $this->posicao_valor_original_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL] = true;
        }

        return $this;
    } // setPosicaoValorOriginalFinal()

    /**
     * Set the value of [posicao_valor_baixa_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorBaixaInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_baixa_inicial !== $v) {
            $this->posicao_valor_baixa_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL] = true;
        }

        return $this;
    } // setPosicaoValorBaixaInicial()

    /**
     * Set the value of [posicao_valor_baixa_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorBaixaFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_baixa_final !== $v) {
            $this->posicao_valor_baixa_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL] = true;
        }

        return $this;
    } // setPosicaoValorBaixaFinal()

    /**
     * Set the value of [posicao_valor_juros_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorJurosInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_juros_inicial !== $v) {
            $this->posicao_valor_juros_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL] = true;
        }

        return $this;
    } // setPosicaoValorJurosInicial()

    /**
     * Set the value of [posicao_valor_juros_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorJurosFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_juros_final !== $v) {
            $this->posicao_valor_juros_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL] = true;
        }

        return $this;
    } // setPosicaoValorJurosFinal()

    /**
     * Set the value of [posicao_valor_multa_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorMultaInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_multa_inicial !== $v) {
            $this->posicao_valor_multa_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL] = true;
        }

        return $this;
    } // setPosicaoValorMultaInicial()

    /**
     * Set the value of [posicao_valor_multa_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorMultaFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_multa_final !== $v) {
            $this->posicao_valor_multa_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL] = true;
        }

        return $this;
    } // setPosicaoValorMultaFinal()

    /**
     * Set the value of [posicao_valor_desconto_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorDescontoInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_desconto_inicial !== $v) {
            $this->posicao_valor_desconto_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL] = true;
        }

        return $this;
    } // setPosicaoValorDescontoInicial()

    /**
     * Set the value of [posicao_valor_desconto_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoValorDescontoFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_valor_desconto_final !== $v) {
            $this->posicao_valor_desconto_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL] = true;
        }

        return $this;
    } // setPosicaoValorDescontoFinal()

    /**
     * Set the value of [posicao_codigo_retorno_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoCodigoRetornoInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_codigo_retorno_inicial !== $v) {
            $this->posicao_codigo_retorno_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL] = true;
        }

        return $this;
    } // setPosicaoCodigoRetornoInicial()

    /**
     * Set the value of [posicao_codigo_retorno_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoCodigoRetornoFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_codigo_retorno_final !== $v) {
            $this->posicao_codigo_retorno_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL] = true;
        }

        return $this;
    } // setPosicaoCodigoRetornoFinal()

    /**
     * Set the value of [posicao_quantidade_erros_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoQuantidadeErrosInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_quantidade_erros_inicial !== $v) {
            $this->posicao_quantidade_erros_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL] = true;
        }

        return $this;
    } // setPosicaoQuantidadeErrosInicial()

    /**
     * Set the value of [posicao_quantidade_erros_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoQuantidadeErrosFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_quantidade_erros_final !== $v) {
            $this->posicao_quantidade_erros_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL] = true;
        }

        return $this;
    } // setPosicaoQuantidadeErrosFinal()

    /**
     * Set the value of [posicao_numero_documento_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoNumeroDocumentoInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_numero_documento_inicial !== $v) {
            $this->posicao_numero_documento_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL] = true;
        }

        return $this;
    } // setPosicaoNumeroDocumentoInicial()

    /**
     * Set the value of [posicao_numero_documento_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoNumeroDocumentoFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_numero_documento_final !== $v) {
            $this->posicao_numero_documento_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL] = true;
        }

        return $this;
    } // setPosicaoNumeroDocumentoFinal()

    /**
     * Set the value of [posicao_data_baixa_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoDataBaixaInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_data_baixa_inicial !== $v) {
            $this->posicao_data_baixa_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL] = true;
        }

        return $this;
    } // setPosicaoDataBaixaInicial()

    /**
     * Set the value of [posicao_data_baixa_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoDataBaixaFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_data_baixa_final !== $v) {
            $this->posicao_data_baixa_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL] = true;
        }

        return $this;
    } // setPosicaoDataBaixaFinal()

    /**
     * Set the value of [formato_data_baixa] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setFormatoDataBaixa($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->formato_data_baixa !== $v) {
            $this->formato_data_baixa = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_FORMATO_DATA_BAIXA] = true;
        }

        return $this;
    } // setFormatoDataBaixa()

    /**
     * Set the value of [posicao_data_compensacao_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoDataCompensacaoInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_data_compensacao_inicial !== $v) {
            $this->posicao_data_compensacao_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL] = true;
        }

        return $this;
    } // setPosicaoDataCompensacaoInicial()

    /**
     * Set the value of [posicao_data_compensacao_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoDataCompensacaoFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_data_compensacao_final !== $v) {
            $this->posicao_data_compensacao_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL] = true;
        }

        return $this;
    } // setPosicaoDataCompensacaoFinal()

    /**
     * Set the value of [formato_data_compensacao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setFormatoDataCompensacao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->formato_data_compensacao !== $v) {
            $this->formato_data_compensacao = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_FORMATO_DATA_COMPENSACAO] = true;
        }

        return $this;
    } // setFormatoDataCompensacao()

    /**
     * Set the value of [posicao_campo_complementar_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoCampoComplementarInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_campo_complementar_inicial !== $v) {
            $this->posicao_campo_complementar_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL] = true;
        }

        return $this;
    } // setPosicaoCampoComplementarInicial()

    /**
     * Set the value of [posicao_campo_complementar_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoCampoComplementarFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_campo_complementar_final !== $v) {
            $this->posicao_campo_complementar_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL] = true;
        }

        return $this;
    } // setPosicaoCampoComplementarFinal()

    /**
     * Set the value of [formato_complementar] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setFormatoComplementar($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->formato_complementar !== $v) {
            $this->formato_complementar = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_FORMATO_COMPLEMENTAR] = true;
        }

        return $this;
    } // setFormatoComplementar()

    /**
     * Set the value of [posicao_data_complementar_inicial] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoDataComplementarInicial($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_data_complementar_inicial !== $v) {
            $this->posicao_data_complementar_inicial = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL] = true;
        }

        return $this;
    } // setPosicaoDataComplementarInicial()

    /**
     * Set the value of [posicao_data_complementar_final] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setPosicaoDataComplementarFinal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_data_complementar_final !== $v) {
            $this->posicao_data_complementar_final = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL] = true;
        }

        return $this;
    } // setPosicaoDataComplementarFinal()

    /**
     * Set the value of [formato_data_complementar] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setFormatoDataComplementar($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->formato_data_complementar !== $v) {
            $this->formato_data_complementar = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_FORMATO_DATA_COMPLEMENTAR] = true;
        }

        return $this;
    } // setFormatoDataComplementar()

    /**
     * Set the value of [codigo_retorno_baixa] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setCodigoRetornoBaixa($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo_retorno_baixa !== $v) {
            $this->codigo_retorno_baixa = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_CODIGO_RETORNO_BAIXA] = true;
        }

        return $this;
    } // setCodigoRetornoBaixa()

    /**
     * Set the value of [codigo_retorno_resgistrado] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setCodigoRetornoResgistrado($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo_retorno_resgistrado !== $v) {
            $this->codigo_retorno_resgistrado = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_CODIGO_RETORNO_RESGISTRADO] = true;
        }

        return $this;
    } // setCodigoRetornoResgistrado()

    /**
     * Set the value of [codigo_retorno_recusado] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setCodigoRetornoRecusado($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo_retorno_recusado !== $v) {
            $this->codigo_retorno_recusado = $v;
            $this->modifiedColumns[LayoutBancarioTableMap::COL_CODIGO_RETORNO_RECUSADO] = true;
        }

        return $this;
    } // setCodigoRetornoRecusado()

    /**
     * Sets the value of [data_cadastrado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setDataCadastrado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastrado !== null || $dt !== null) {
            if ($this->data_cadastrado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastrado->format("Y-m-d H:i:s.u")) {
                $this->data_cadastrado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[LayoutBancarioTableMap::COL_DATA_CADASTRADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastrado()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[LayoutBancarioTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : LayoutBancarioTableMap::translateFieldName('IdlayoutBancario', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idlayout_bancario = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : LayoutBancarioTableMap::translateFieldName('TamanhoNossoNumero', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tamanho_nosso_numero = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoNossoNumeroInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_nosso_numero_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoNossoNumeroFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_nosso_numero_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoChaveLancamentoInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_chave_lancamento_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoChaveLancamentoFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_chave_lancamento_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoChaveLancamento2Inicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_chave_lancamento2_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoChaveLancamento2Final', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_chave_lancamento2_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorOriginalInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_original_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorOriginalFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_original_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorBaixaInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_baixa_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorBaixaFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_baixa_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorJurosInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_juros_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorJurosFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_juros_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorMultaInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_multa_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorMultaFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_multa_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorDescontoInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_desconto_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoValorDescontoFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_valor_desconto_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoCodigoRetornoInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_codigo_retorno_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoCodigoRetornoFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_codigo_retorno_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoQuantidadeErrosInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_quantidade_erros_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoQuantidadeErrosFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_quantidade_erros_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoNumeroDocumentoInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_numero_documento_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoNumeroDocumentoFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_numero_documento_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoDataBaixaInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_data_baixa_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoDataBaixaFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_data_baixa_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : LayoutBancarioTableMap::translateFieldName('FormatoDataBaixa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->formato_data_baixa = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoDataCompensacaoInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_data_compensacao_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoDataCompensacaoFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_data_compensacao_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : LayoutBancarioTableMap::translateFieldName('FormatoDataCompensacao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->formato_data_compensacao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoCampoComplementarInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_campo_complementar_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoCampoComplementarFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_campo_complementar_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : LayoutBancarioTableMap::translateFieldName('FormatoComplementar', TableMap::TYPE_PHPNAME, $indexType)];
            $this->formato_complementar = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoDataComplementarInicial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_data_complementar_inicial = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : LayoutBancarioTableMap::translateFieldName('PosicaoDataComplementarFinal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_data_complementar_final = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 35 + $startcol : LayoutBancarioTableMap::translateFieldName('FormatoDataComplementar', TableMap::TYPE_PHPNAME, $indexType)];
            $this->formato_data_complementar = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 36 + $startcol : LayoutBancarioTableMap::translateFieldName('CodigoRetornoBaixa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codigo_retorno_baixa = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 37 + $startcol : LayoutBancarioTableMap::translateFieldName('CodigoRetornoResgistrado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codigo_retorno_resgistrado = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 38 + $startcol : LayoutBancarioTableMap::translateFieldName('CodigoRetornoRecusado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codigo_retorno_recusado = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 39 + $startcol : LayoutBancarioTableMap::translateFieldName('DataCadastrado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastrado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 40 + $startcol : LayoutBancarioTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 41; // 41 = LayoutBancarioTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\LayoutBancario'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LayoutBancarioTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildLayoutBancarioQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collBancos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see LayoutBancario::setDeleted()
     * @see LayoutBancario::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LayoutBancarioTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildLayoutBancarioQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LayoutBancarioTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                LayoutBancarioTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->bancosScheduledForDeletion !== null) {
                if (!$this->bancosScheduledForDeletion->isEmpty()) {
                    foreach ($this->bancosScheduledForDeletion as $banco) {
                        // need to save related object because we set the relation to null
                        $banco->save($con);
                    }
                    $this->bancosScheduledForDeletion = null;
                }
            }

            if ($this->collBancos !== null) {
                foreach ($this->collBancos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO] = true;
        if (null !== $this->idlayout_bancario) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO)) {
            $modifiedColumns[':p' . $index++]  = 'idlayout_bancario';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO)) {
            $modifiedColumns[':p' . $index++]  = 'tamanho_nosso_numero';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_nosso_numero_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_nosso_numero_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_chave_lancamento_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_chave_lancamento_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_chave_lancamento2_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_chave_lancamento2_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_original_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_original_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_baixa_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_baixa_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_juros_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_juros_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_multa_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_multa_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_desconto_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_valor_desconto_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_codigo_retorno_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_codigo_retorno_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_quantidade_erros_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_quantidade_erros_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_numero_documento_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_numero_documento_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_data_baixa_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_data_baixa_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_FORMATO_DATA_BAIXA)) {
            $modifiedColumns[':p' . $index++]  = 'formato_data_baixa';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_data_compensacao_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_data_compensacao_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPENSACAO)) {
            $modifiedColumns[':p' . $index++]  = 'formato_data_compensacao';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_campo_complementar_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_campo_complementar_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_FORMATO_COMPLEMENTAR)) {
            $modifiedColumns[':p' . $index++]  = 'formato_complementar';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_data_complementar_inicial';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_data_complementar_final';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPLEMENTAR)) {
            $modifiedColumns[':p' . $index++]  = 'formato_data_complementar';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_CODIGO_RETORNO_BAIXA)) {
            $modifiedColumns[':p' . $index++]  = 'codigo_retorno_baixa';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RESGISTRADO)) {
            $modifiedColumns[':p' . $index++]  = 'codigo_retorno_resgistrado';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RECUSADO)) {
            $modifiedColumns[':p' . $index++]  = 'codigo_retorno_recusado';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_DATA_CADASTRADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastrado';
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }

        $sql = sprintf(
            'INSERT INTO layout_bancario (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idlayout_bancario':
                        $stmt->bindValue($identifier, $this->idlayout_bancario, PDO::PARAM_INT);
                        break;
                    case 'tamanho_nosso_numero':
                        $stmt->bindValue($identifier, $this->tamanho_nosso_numero, PDO::PARAM_INT);
                        break;
                    case 'posicao_nosso_numero_inicial':
                        $stmt->bindValue($identifier, $this->posicao_nosso_numero_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_nosso_numero_final':
                        $stmt->bindValue($identifier, $this->posicao_nosso_numero_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_chave_lancamento_inicial':
                        $stmt->bindValue($identifier, $this->posicao_chave_lancamento_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_chave_lancamento_final':
                        $stmt->bindValue($identifier, $this->posicao_chave_lancamento_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_chave_lancamento2_inicial':
                        $stmt->bindValue($identifier, $this->posicao_chave_lancamento2_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_chave_lancamento2_final':
                        $stmt->bindValue($identifier, $this->posicao_chave_lancamento2_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_original_inicial':
                        $stmt->bindValue($identifier, $this->posicao_valor_original_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_original_final':
                        $stmt->bindValue($identifier, $this->posicao_valor_original_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_baixa_inicial':
                        $stmt->bindValue($identifier, $this->posicao_valor_baixa_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_baixa_final':
                        $stmt->bindValue($identifier, $this->posicao_valor_baixa_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_juros_inicial':
                        $stmt->bindValue($identifier, $this->posicao_valor_juros_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_juros_final':
                        $stmt->bindValue($identifier, $this->posicao_valor_juros_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_multa_inicial':
                        $stmt->bindValue($identifier, $this->posicao_valor_multa_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_multa_final':
                        $stmt->bindValue($identifier, $this->posicao_valor_multa_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_desconto_inicial':
                        $stmt->bindValue($identifier, $this->posicao_valor_desconto_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_valor_desconto_final':
                        $stmt->bindValue($identifier, $this->posicao_valor_desconto_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_codigo_retorno_inicial':
                        $stmt->bindValue($identifier, $this->posicao_codigo_retorno_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_codigo_retorno_final':
                        $stmt->bindValue($identifier, $this->posicao_codigo_retorno_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_quantidade_erros_inicial':
                        $stmt->bindValue($identifier, $this->posicao_quantidade_erros_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_quantidade_erros_final':
                        $stmt->bindValue($identifier, $this->posicao_quantidade_erros_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_numero_documento_inicial':
                        $stmt->bindValue($identifier, $this->posicao_numero_documento_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_numero_documento_final':
                        $stmt->bindValue($identifier, $this->posicao_numero_documento_final, PDO::PARAM_INT);
                        break;
                    case 'posicao_data_baixa_inicial':
                        $stmt->bindValue($identifier, $this->posicao_data_baixa_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_data_baixa_final':
                        $stmt->bindValue($identifier, $this->posicao_data_baixa_final, PDO::PARAM_INT);
                        break;
                    case 'formato_data_baixa':
                        $stmt->bindValue($identifier, $this->formato_data_baixa, PDO::PARAM_STR);
                        break;
                    case 'posicao_data_compensacao_inicial':
                        $stmt->bindValue($identifier, $this->posicao_data_compensacao_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_data_compensacao_final':
                        $stmt->bindValue($identifier, $this->posicao_data_compensacao_final, PDO::PARAM_INT);
                        break;
                    case 'formato_data_compensacao':
                        $stmt->bindValue($identifier, $this->formato_data_compensacao, PDO::PARAM_STR);
                        break;
                    case 'posicao_campo_complementar_inicial':
                        $stmt->bindValue($identifier, $this->posicao_campo_complementar_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_campo_complementar_final':
                        $stmt->bindValue($identifier, $this->posicao_campo_complementar_final, PDO::PARAM_INT);
                        break;
                    case 'formato_complementar':
                        $stmt->bindValue($identifier, $this->formato_complementar, PDO::PARAM_STR);
                        break;
                    case 'posicao_data_complementar_inicial':
                        $stmt->bindValue($identifier, $this->posicao_data_complementar_inicial, PDO::PARAM_INT);
                        break;
                    case 'posicao_data_complementar_final':
                        $stmt->bindValue($identifier, $this->posicao_data_complementar_final, PDO::PARAM_INT);
                        break;
                    case 'formato_data_complementar':
                        $stmt->bindValue($identifier, $this->formato_data_complementar, PDO::PARAM_STR);
                        break;
                    case 'codigo_retorno_baixa':
                        $stmt->bindValue($identifier, $this->codigo_retorno_baixa, PDO::PARAM_STR);
                        break;
                    case 'codigo_retorno_resgistrado':
                        $stmt->bindValue($identifier, $this->codigo_retorno_resgistrado, PDO::PARAM_STR);
                        break;
                    case 'codigo_retorno_recusado':
                        $stmt->bindValue($identifier, $this->codigo_retorno_recusado, PDO::PARAM_STR);
                        break;
                    case 'data_cadastrado':
                        $stmt->bindValue($identifier, $this->data_cadastrado ? $this->data_cadastrado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdlayoutBancario($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = LayoutBancarioTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdlayoutBancario();
                break;
            case 1:
                return $this->getTamanhoNossoNumero();
                break;
            case 2:
                return $this->getPosicaoNossoNumeroInicial();
                break;
            case 3:
                return $this->getPosicaoNossoNumeroFinal();
                break;
            case 4:
                return $this->getPosicaoChaveLancamentoInicial();
                break;
            case 5:
                return $this->getPosicaoChaveLancamentoFinal();
                break;
            case 6:
                return $this->getPosicaoChaveLancamento2Inicial();
                break;
            case 7:
                return $this->getPosicaoChaveLancamento2Final();
                break;
            case 8:
                return $this->getPosicaoValorOriginalInicial();
                break;
            case 9:
                return $this->getPosicaoValorOriginalFinal();
                break;
            case 10:
                return $this->getPosicaoValorBaixaInicial();
                break;
            case 11:
                return $this->getPosicaoValorBaixaFinal();
                break;
            case 12:
                return $this->getPosicaoValorJurosInicial();
                break;
            case 13:
                return $this->getPosicaoValorJurosFinal();
                break;
            case 14:
                return $this->getPosicaoValorMultaInicial();
                break;
            case 15:
                return $this->getPosicaoValorMultaFinal();
                break;
            case 16:
                return $this->getPosicaoValorDescontoInicial();
                break;
            case 17:
                return $this->getPosicaoValorDescontoFinal();
                break;
            case 18:
                return $this->getPosicaoCodigoRetornoInicial();
                break;
            case 19:
                return $this->getPosicaoCodigoRetornoFinal();
                break;
            case 20:
                return $this->getPosicaoQuantidadeErrosInicial();
                break;
            case 21:
                return $this->getPosicaoQuantidadeErrosFinal();
                break;
            case 22:
                return $this->getPosicaoNumeroDocumentoInicial();
                break;
            case 23:
                return $this->getPosicaoNumeroDocumentoFinal();
                break;
            case 24:
                return $this->getPosicaoDataBaixaInicial();
                break;
            case 25:
                return $this->getPosicaoDataBaixaFinal();
                break;
            case 26:
                return $this->getFormatoDataBaixa();
                break;
            case 27:
                return $this->getPosicaoDataCompensacaoInicial();
                break;
            case 28:
                return $this->getPosicaoDataCompensacaoFinal();
                break;
            case 29:
                return $this->getFormatoDataCompensacao();
                break;
            case 30:
                return $this->getPosicaoCampoComplementarInicial();
                break;
            case 31:
                return $this->getPosicaoCampoComplementarFinal();
                break;
            case 32:
                return $this->getFormatoComplementar();
                break;
            case 33:
                return $this->getPosicaoDataComplementarInicial();
                break;
            case 34:
                return $this->getPosicaoDataComplementarFinal();
                break;
            case 35:
                return $this->getFormatoDataComplementar();
                break;
            case 36:
                return $this->getCodigoRetornoBaixa();
                break;
            case 37:
                return $this->getCodigoRetornoResgistrado();
                break;
            case 38:
                return $this->getCodigoRetornoRecusado();
                break;
            case 39:
                return $this->getDataCadastrado();
                break;
            case 40:
                return $this->getDataAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['LayoutBancario'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['LayoutBancario'][$this->hashCode()] = true;
        $keys = LayoutBancarioTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdlayoutBancario(),
            $keys[1] => $this->getTamanhoNossoNumero(),
            $keys[2] => $this->getPosicaoNossoNumeroInicial(),
            $keys[3] => $this->getPosicaoNossoNumeroFinal(),
            $keys[4] => $this->getPosicaoChaveLancamentoInicial(),
            $keys[5] => $this->getPosicaoChaveLancamentoFinal(),
            $keys[6] => $this->getPosicaoChaveLancamento2Inicial(),
            $keys[7] => $this->getPosicaoChaveLancamento2Final(),
            $keys[8] => $this->getPosicaoValorOriginalInicial(),
            $keys[9] => $this->getPosicaoValorOriginalFinal(),
            $keys[10] => $this->getPosicaoValorBaixaInicial(),
            $keys[11] => $this->getPosicaoValorBaixaFinal(),
            $keys[12] => $this->getPosicaoValorJurosInicial(),
            $keys[13] => $this->getPosicaoValorJurosFinal(),
            $keys[14] => $this->getPosicaoValorMultaInicial(),
            $keys[15] => $this->getPosicaoValorMultaFinal(),
            $keys[16] => $this->getPosicaoValorDescontoInicial(),
            $keys[17] => $this->getPosicaoValorDescontoFinal(),
            $keys[18] => $this->getPosicaoCodigoRetornoInicial(),
            $keys[19] => $this->getPosicaoCodigoRetornoFinal(),
            $keys[20] => $this->getPosicaoQuantidadeErrosInicial(),
            $keys[21] => $this->getPosicaoQuantidadeErrosFinal(),
            $keys[22] => $this->getPosicaoNumeroDocumentoInicial(),
            $keys[23] => $this->getPosicaoNumeroDocumentoFinal(),
            $keys[24] => $this->getPosicaoDataBaixaInicial(),
            $keys[25] => $this->getPosicaoDataBaixaFinal(),
            $keys[26] => $this->getFormatoDataBaixa(),
            $keys[27] => $this->getPosicaoDataCompensacaoInicial(),
            $keys[28] => $this->getPosicaoDataCompensacaoFinal(),
            $keys[29] => $this->getFormatoDataCompensacao(),
            $keys[30] => $this->getPosicaoCampoComplementarInicial(),
            $keys[31] => $this->getPosicaoCampoComplementarFinal(),
            $keys[32] => $this->getFormatoComplementar(),
            $keys[33] => $this->getPosicaoDataComplementarInicial(),
            $keys[34] => $this->getPosicaoDataComplementarFinal(),
            $keys[35] => $this->getFormatoDataComplementar(),
            $keys[36] => $this->getCodigoRetornoBaixa(),
            $keys[37] => $this->getCodigoRetornoResgistrado(),
            $keys[38] => $this->getCodigoRetornoRecusado(),
            $keys[39] => $this->getDataCadastrado(),
            $keys[40] => $this->getDataAlterado(),
        );
        if ($result[$keys[39]] instanceof \DateTime) {
            $result[$keys[39]] = $result[$keys[39]]->format('c');
        }

        if ($result[$keys[40]] instanceof \DateTime) {
            $result[$keys[40]] = $result[$keys[40]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collBancos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bancos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'bancos';
                        break;
                    default:
                        $key = 'Bancos';
                }

                $result[$key] = $this->collBancos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = LayoutBancarioTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdlayoutBancario($value);
                break;
            case 1:
                $this->setTamanhoNossoNumero($value);
                break;
            case 2:
                $this->setPosicaoNossoNumeroInicial($value);
                break;
            case 3:
                $this->setPosicaoNossoNumeroFinal($value);
                break;
            case 4:
                $this->setPosicaoChaveLancamentoInicial($value);
                break;
            case 5:
                $this->setPosicaoChaveLancamentoFinal($value);
                break;
            case 6:
                $this->setPosicaoChaveLancamento2Inicial($value);
                break;
            case 7:
                $this->setPosicaoChaveLancamento2Final($value);
                break;
            case 8:
                $this->setPosicaoValorOriginalInicial($value);
                break;
            case 9:
                $this->setPosicaoValorOriginalFinal($value);
                break;
            case 10:
                $this->setPosicaoValorBaixaInicial($value);
                break;
            case 11:
                $this->setPosicaoValorBaixaFinal($value);
                break;
            case 12:
                $this->setPosicaoValorJurosInicial($value);
                break;
            case 13:
                $this->setPosicaoValorJurosFinal($value);
                break;
            case 14:
                $this->setPosicaoValorMultaInicial($value);
                break;
            case 15:
                $this->setPosicaoValorMultaFinal($value);
                break;
            case 16:
                $this->setPosicaoValorDescontoInicial($value);
                break;
            case 17:
                $this->setPosicaoValorDescontoFinal($value);
                break;
            case 18:
                $this->setPosicaoCodigoRetornoInicial($value);
                break;
            case 19:
                $this->setPosicaoCodigoRetornoFinal($value);
                break;
            case 20:
                $this->setPosicaoQuantidadeErrosInicial($value);
                break;
            case 21:
                $this->setPosicaoQuantidadeErrosFinal($value);
                break;
            case 22:
                $this->setPosicaoNumeroDocumentoInicial($value);
                break;
            case 23:
                $this->setPosicaoNumeroDocumentoFinal($value);
                break;
            case 24:
                $this->setPosicaoDataBaixaInicial($value);
                break;
            case 25:
                $this->setPosicaoDataBaixaFinal($value);
                break;
            case 26:
                $this->setFormatoDataBaixa($value);
                break;
            case 27:
                $this->setPosicaoDataCompensacaoInicial($value);
                break;
            case 28:
                $this->setPosicaoDataCompensacaoFinal($value);
                break;
            case 29:
                $this->setFormatoDataCompensacao($value);
                break;
            case 30:
                $this->setPosicaoCampoComplementarInicial($value);
                break;
            case 31:
                $this->setPosicaoCampoComplementarFinal($value);
                break;
            case 32:
                $this->setFormatoComplementar($value);
                break;
            case 33:
                $this->setPosicaoDataComplementarInicial($value);
                break;
            case 34:
                $this->setPosicaoDataComplementarFinal($value);
                break;
            case 35:
                $this->setFormatoDataComplementar($value);
                break;
            case 36:
                $this->setCodigoRetornoBaixa($value);
                break;
            case 37:
                $this->setCodigoRetornoResgistrado($value);
                break;
            case 38:
                $this->setCodigoRetornoRecusado($value);
                break;
            case 39:
                $this->setDataCadastrado($value);
                break;
            case 40:
                $this->setDataAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = LayoutBancarioTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdlayoutBancario($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTamanhoNossoNumero($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPosicaoNossoNumeroInicial($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPosicaoNossoNumeroFinal($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPosicaoChaveLancamentoInicial($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPosicaoChaveLancamentoFinal($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPosicaoChaveLancamento2Inicial($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setPosicaoChaveLancamento2Final($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPosicaoValorOriginalInicial($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPosicaoValorOriginalFinal($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPosicaoValorBaixaInicial($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setPosicaoValorBaixaFinal($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setPosicaoValorJurosInicial($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setPosicaoValorJurosFinal($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setPosicaoValorMultaInicial($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setPosicaoValorMultaFinal($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setPosicaoValorDescontoInicial($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setPosicaoValorDescontoFinal($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setPosicaoCodigoRetornoInicial($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setPosicaoCodigoRetornoFinal($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setPosicaoQuantidadeErrosInicial($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setPosicaoQuantidadeErrosFinal($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setPosicaoNumeroDocumentoInicial($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setPosicaoNumeroDocumentoFinal($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setPosicaoDataBaixaInicial($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setPosicaoDataBaixaFinal($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setFormatoDataBaixa($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setPosicaoDataCompensacaoInicial($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setPosicaoDataCompensacaoFinal($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setFormatoDataCompensacao($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setPosicaoCampoComplementarInicial($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setPosicaoCampoComplementarFinal($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setFormatoComplementar($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setPosicaoDataComplementarInicial($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setPosicaoDataComplementarFinal($arr[$keys[34]]);
        }
        if (array_key_exists($keys[35], $arr)) {
            $this->setFormatoDataComplementar($arr[$keys[35]]);
        }
        if (array_key_exists($keys[36], $arr)) {
            $this->setCodigoRetornoBaixa($arr[$keys[36]]);
        }
        if (array_key_exists($keys[37], $arr)) {
            $this->setCodigoRetornoResgistrado($arr[$keys[37]]);
        }
        if (array_key_exists($keys[38], $arr)) {
            $this->setCodigoRetornoRecusado($arr[$keys[38]]);
        }
        if (array_key_exists($keys[39], $arr)) {
            $this->setDataCadastrado($arr[$keys[39]]);
        }
        if (array_key_exists($keys[40], $arr)) {
            $this->setDataAlterado($arr[$keys[40]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(LayoutBancarioTableMap::DATABASE_NAME);

        if ($this->isColumnModified(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO)) {
            $criteria->add(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, $this->idlayout_bancario);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO)) {
            $criteria->add(LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO, $this->tamanho_nosso_numero);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL, $this->posicao_nosso_numero_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL, $this->posicao_nosso_numero_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL, $this->posicao_chave_lancamento_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL, $this->posicao_chave_lancamento_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL, $this->posicao_chave_lancamento2_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL, $this->posicao_chave_lancamento2_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL, $this->posicao_valor_original_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL, $this->posicao_valor_original_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL, $this->posicao_valor_baixa_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL, $this->posicao_valor_baixa_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL, $this->posicao_valor_juros_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL, $this->posicao_valor_juros_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL, $this->posicao_valor_multa_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL, $this->posicao_valor_multa_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL, $this->posicao_valor_desconto_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL, $this->posicao_valor_desconto_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL, $this->posicao_codigo_retorno_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL, $this->posicao_codigo_retorno_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL, $this->posicao_quantidade_erros_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL, $this->posicao_quantidade_erros_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL, $this->posicao_numero_documento_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL, $this->posicao_numero_documento_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL, $this->posicao_data_baixa_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL, $this->posicao_data_baixa_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_FORMATO_DATA_BAIXA)) {
            $criteria->add(LayoutBancarioTableMap::COL_FORMATO_DATA_BAIXA, $this->formato_data_baixa);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL, $this->posicao_data_compensacao_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL, $this->posicao_data_compensacao_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPENSACAO)) {
            $criteria->add(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPENSACAO, $this->formato_data_compensacao);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL, $this->posicao_campo_complementar_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL, $this->posicao_campo_complementar_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_FORMATO_COMPLEMENTAR)) {
            $criteria->add(LayoutBancarioTableMap::COL_FORMATO_COMPLEMENTAR, $this->formato_complementar);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL, $this->posicao_data_complementar_inicial);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL)) {
            $criteria->add(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL, $this->posicao_data_complementar_final);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPLEMENTAR)) {
            $criteria->add(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPLEMENTAR, $this->formato_data_complementar);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_CODIGO_RETORNO_BAIXA)) {
            $criteria->add(LayoutBancarioTableMap::COL_CODIGO_RETORNO_BAIXA, $this->codigo_retorno_baixa);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RESGISTRADO)) {
            $criteria->add(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RESGISTRADO, $this->codigo_retorno_resgistrado);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RECUSADO)) {
            $criteria->add(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RECUSADO, $this->codigo_retorno_recusado);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_DATA_CADASTRADO)) {
            $criteria->add(LayoutBancarioTableMap::COL_DATA_CADASTRADO, $this->data_cadastrado);
        }
        if ($this->isColumnModified(LayoutBancarioTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(LayoutBancarioTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildLayoutBancarioQuery::create();
        $criteria->add(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, $this->idlayout_bancario);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdlayoutBancario();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdlayoutBancario();
    }

    /**
     * Generic method to set the primary key (idlayout_bancario column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdlayoutBancario($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdlayoutBancario();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\LayoutBancario (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTamanhoNossoNumero($this->getTamanhoNossoNumero());
        $copyObj->setPosicaoNossoNumeroInicial($this->getPosicaoNossoNumeroInicial());
        $copyObj->setPosicaoNossoNumeroFinal($this->getPosicaoNossoNumeroFinal());
        $copyObj->setPosicaoChaveLancamentoInicial($this->getPosicaoChaveLancamentoInicial());
        $copyObj->setPosicaoChaveLancamentoFinal($this->getPosicaoChaveLancamentoFinal());
        $copyObj->setPosicaoChaveLancamento2Inicial($this->getPosicaoChaveLancamento2Inicial());
        $copyObj->setPosicaoChaveLancamento2Final($this->getPosicaoChaveLancamento2Final());
        $copyObj->setPosicaoValorOriginalInicial($this->getPosicaoValorOriginalInicial());
        $copyObj->setPosicaoValorOriginalFinal($this->getPosicaoValorOriginalFinal());
        $copyObj->setPosicaoValorBaixaInicial($this->getPosicaoValorBaixaInicial());
        $copyObj->setPosicaoValorBaixaFinal($this->getPosicaoValorBaixaFinal());
        $copyObj->setPosicaoValorJurosInicial($this->getPosicaoValorJurosInicial());
        $copyObj->setPosicaoValorJurosFinal($this->getPosicaoValorJurosFinal());
        $copyObj->setPosicaoValorMultaInicial($this->getPosicaoValorMultaInicial());
        $copyObj->setPosicaoValorMultaFinal($this->getPosicaoValorMultaFinal());
        $copyObj->setPosicaoValorDescontoInicial($this->getPosicaoValorDescontoInicial());
        $copyObj->setPosicaoValorDescontoFinal($this->getPosicaoValorDescontoFinal());
        $copyObj->setPosicaoCodigoRetornoInicial($this->getPosicaoCodigoRetornoInicial());
        $copyObj->setPosicaoCodigoRetornoFinal($this->getPosicaoCodigoRetornoFinal());
        $copyObj->setPosicaoQuantidadeErrosInicial($this->getPosicaoQuantidadeErrosInicial());
        $copyObj->setPosicaoQuantidadeErrosFinal($this->getPosicaoQuantidadeErrosFinal());
        $copyObj->setPosicaoNumeroDocumentoInicial($this->getPosicaoNumeroDocumentoInicial());
        $copyObj->setPosicaoNumeroDocumentoFinal($this->getPosicaoNumeroDocumentoFinal());
        $copyObj->setPosicaoDataBaixaInicial($this->getPosicaoDataBaixaInicial());
        $copyObj->setPosicaoDataBaixaFinal($this->getPosicaoDataBaixaFinal());
        $copyObj->setFormatoDataBaixa($this->getFormatoDataBaixa());
        $copyObj->setPosicaoDataCompensacaoInicial($this->getPosicaoDataCompensacaoInicial());
        $copyObj->setPosicaoDataCompensacaoFinal($this->getPosicaoDataCompensacaoFinal());
        $copyObj->setFormatoDataCompensacao($this->getFormatoDataCompensacao());
        $copyObj->setPosicaoCampoComplementarInicial($this->getPosicaoCampoComplementarInicial());
        $copyObj->setPosicaoCampoComplementarFinal($this->getPosicaoCampoComplementarFinal());
        $copyObj->setFormatoComplementar($this->getFormatoComplementar());
        $copyObj->setPosicaoDataComplementarInicial($this->getPosicaoDataComplementarInicial());
        $copyObj->setPosicaoDataComplementarFinal($this->getPosicaoDataComplementarFinal());
        $copyObj->setFormatoDataComplementar($this->getFormatoDataComplementar());
        $copyObj->setCodigoRetornoBaixa($this->getCodigoRetornoBaixa());
        $copyObj->setCodigoRetornoResgistrado($this->getCodigoRetornoResgistrado());
        $copyObj->setCodigoRetornoRecusado($this->getCodigoRetornoRecusado());
        $copyObj->setDataCadastrado($this->getDataCadastrado());
        $copyObj->setDataAlterado($this->getDataAlterado());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBancos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBanco($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdlayoutBancario(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\LayoutBancario Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Banco' == $relationName) {
            return $this->initBancos();
        }
    }

    /**
     * Clears out the collBancos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBancos()
     */
    public function clearBancos()
    {
        $this->collBancos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBancos collection loaded partially.
     */
    public function resetPartialBancos($v = true)
    {
        $this->collBancosPartial = $v;
    }

    /**
     * Initializes the collBancos collection.
     *
     * By default this just sets the collBancos collection to an empty array (like clearcollBancos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBancos($overrideExisting = true)
    {
        if (null !== $this->collBancos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BancoTableMap::getTableMap()->getCollectionClassName();

        $this->collBancos = new $collectionClassName;
        $this->collBancos->setModel('\ImaTelecomBundle\Model\Banco');
    }

    /**
     * Gets an array of ChildBanco objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLayoutBancario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBanco[] List of ChildBanco objects
     * @throws PropelException
     */
    public function getBancos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBancosPartial && !$this->isNew();
        if (null === $this->collBancos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBancos) {
                // return empty collection
                $this->initBancos();
            } else {
                $collBancos = ChildBancoQuery::create(null, $criteria)
                    ->filterByLayoutBancario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBancosPartial && count($collBancos)) {
                        $this->initBancos(false);

                        foreach ($collBancos as $obj) {
                            if (false == $this->collBancos->contains($obj)) {
                                $this->collBancos->append($obj);
                            }
                        }

                        $this->collBancosPartial = true;
                    }

                    return $collBancos;
                }

                if ($partial && $this->collBancos) {
                    foreach ($this->collBancos as $obj) {
                        if ($obj->isNew()) {
                            $collBancos[] = $obj;
                        }
                    }
                }

                $this->collBancos = $collBancos;
                $this->collBancosPartial = false;
            }
        }

        return $this->collBancos;
    }

    /**
     * Sets a collection of ChildBanco objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bancos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLayoutBancario The current object (for fluent API support)
     */
    public function setBancos(Collection $bancos, ConnectionInterface $con = null)
    {
        /** @var ChildBanco[] $bancosToDelete */
        $bancosToDelete = $this->getBancos(new Criteria(), $con)->diff($bancos);


        $this->bancosScheduledForDeletion = $bancosToDelete;

        foreach ($bancosToDelete as $bancoRemoved) {
            $bancoRemoved->setLayoutBancario(null);
        }

        $this->collBancos = null;
        foreach ($bancos as $banco) {
            $this->addBanco($banco);
        }

        $this->collBancos = $bancos;
        $this->collBancosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Banco objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Banco objects.
     * @throws PropelException
     */
    public function countBancos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBancosPartial && !$this->isNew();
        if (null === $this->collBancos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBancos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBancos());
            }

            $query = ChildBancoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLayoutBancario($this)
                ->count($con);
        }

        return count($this->collBancos);
    }

    /**
     * Method called to associate a ChildBanco object to this object
     * through the ChildBanco foreign key attribute.
     *
     * @param  ChildBanco $l ChildBanco
     * @return $this|\ImaTelecomBundle\Model\LayoutBancario The current object (for fluent API support)
     */
    public function addBanco(ChildBanco $l)
    {
        if ($this->collBancos === null) {
            $this->initBancos();
            $this->collBancosPartial = true;
        }

        if (!$this->collBancos->contains($l)) {
            $this->doAddBanco($l);

            if ($this->bancosScheduledForDeletion and $this->bancosScheduledForDeletion->contains($l)) {
                $this->bancosScheduledForDeletion->remove($this->bancosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBanco $banco The ChildBanco object to add.
     */
    protected function doAddBanco(ChildBanco $banco)
    {
        $this->collBancos[]= $banco;
        $banco->setLayoutBancario($this);
    }

    /**
     * @param  ChildBanco $banco The ChildBanco object to remove.
     * @return $this|ChildLayoutBancario The current object (for fluent API support)
     */
    public function removeBanco(ChildBanco $banco)
    {
        if ($this->getBancos()->contains($banco)) {
            $pos = $this->collBancos->search($banco);
            $this->collBancos->remove($pos);
            if (null === $this->bancosScheduledForDeletion) {
                $this->bancosScheduledForDeletion = clone $this->collBancos;
                $this->bancosScheduledForDeletion->clear();
            }
            $this->bancosScheduledForDeletion[]= $banco;
            $banco->setLayoutBancario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this LayoutBancario is new, it will return
     * an empty collection; or if this LayoutBancario has previously
     * been saved, it will retrieve related Bancos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in LayoutBancario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBanco[] List of ChildBanco objects
     */
    public function getBancosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBancoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBancos($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->idlayout_bancario = null;
        $this->tamanho_nosso_numero = null;
        $this->posicao_nosso_numero_inicial = null;
        $this->posicao_nosso_numero_final = null;
        $this->posicao_chave_lancamento_inicial = null;
        $this->posicao_chave_lancamento_final = null;
        $this->posicao_chave_lancamento2_inicial = null;
        $this->posicao_chave_lancamento2_final = null;
        $this->posicao_valor_original_inicial = null;
        $this->posicao_valor_original_final = null;
        $this->posicao_valor_baixa_inicial = null;
        $this->posicao_valor_baixa_final = null;
        $this->posicao_valor_juros_inicial = null;
        $this->posicao_valor_juros_final = null;
        $this->posicao_valor_multa_inicial = null;
        $this->posicao_valor_multa_final = null;
        $this->posicao_valor_desconto_inicial = null;
        $this->posicao_valor_desconto_final = null;
        $this->posicao_codigo_retorno_inicial = null;
        $this->posicao_codigo_retorno_final = null;
        $this->posicao_quantidade_erros_inicial = null;
        $this->posicao_quantidade_erros_final = null;
        $this->posicao_numero_documento_inicial = null;
        $this->posicao_numero_documento_final = null;
        $this->posicao_data_baixa_inicial = null;
        $this->posicao_data_baixa_final = null;
        $this->formato_data_baixa = null;
        $this->posicao_data_compensacao_inicial = null;
        $this->posicao_data_compensacao_final = null;
        $this->formato_data_compensacao = null;
        $this->posicao_campo_complementar_inicial = null;
        $this->posicao_campo_complementar_final = null;
        $this->formato_complementar = null;
        $this->posicao_data_complementar_inicial = null;
        $this->posicao_data_complementar_final = null;
        $this->formato_data_complementar = null;
        $this->codigo_retorno_baixa = null;
        $this->codigo_retorno_resgistrado = null;
        $this->codigo_retorno_recusado = null;
        $this->data_cadastrado = null;
        $this->data_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBancos) {
                foreach ($this->collBancos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBancos = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(LayoutBancarioTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
