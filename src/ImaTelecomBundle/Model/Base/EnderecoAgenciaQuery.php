<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\EnderecoAgencia as ChildEnderecoAgencia;
use ImaTelecomBundle\Model\EnderecoAgenciaQuery as ChildEnderecoAgenciaQuery;
use ImaTelecomBundle\Model\Map\EnderecoAgenciaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'endereco_agencia' table.
 *
 *
 *
 * @method     ChildEnderecoAgenciaQuery orderByIdenderecoAgencia($order = Criteria::ASC) Order by the idendereco_agencia column
 * @method     ChildEnderecoAgenciaQuery orderByRua($order = Criteria::ASC) Order by the rua column
 * @method     ChildEnderecoAgenciaQuery orderByBairro($order = Criteria::ASC) Order by the bairro column
 * @method     ChildEnderecoAgenciaQuery orderByNum($order = Criteria::ASC) Order by the num column
 * @method     ChildEnderecoAgenciaQuery orderByComplemento($order = Criteria::ASC) Order by the complemento column
 * @method     ChildEnderecoAgenciaQuery orderByCidadeId($order = Criteria::ASC) Order by the cidade_id column
 * @method     ChildEnderecoAgenciaQuery orderByCep($order = Criteria::ASC) Order by the cep column
 * @method     ChildEnderecoAgenciaQuery orderByUf($order = Criteria::ASC) Order by the uf column
 * @method     ChildEnderecoAgenciaQuery orderByPontoReferencia($order = Criteria::ASC) Order by the ponto_referencia column
 * @method     ChildEnderecoAgenciaQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildEnderecoAgenciaQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildEnderecoAgenciaQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildEnderecoAgenciaQuery groupByIdenderecoAgencia() Group by the idendereco_agencia column
 * @method     ChildEnderecoAgenciaQuery groupByRua() Group by the rua column
 * @method     ChildEnderecoAgenciaQuery groupByBairro() Group by the bairro column
 * @method     ChildEnderecoAgenciaQuery groupByNum() Group by the num column
 * @method     ChildEnderecoAgenciaQuery groupByComplemento() Group by the complemento column
 * @method     ChildEnderecoAgenciaQuery groupByCidadeId() Group by the cidade_id column
 * @method     ChildEnderecoAgenciaQuery groupByCep() Group by the cep column
 * @method     ChildEnderecoAgenciaQuery groupByUf() Group by the uf column
 * @method     ChildEnderecoAgenciaQuery groupByPontoReferencia() Group by the ponto_referencia column
 * @method     ChildEnderecoAgenciaQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildEnderecoAgenciaQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildEnderecoAgenciaQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildEnderecoAgenciaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEnderecoAgenciaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEnderecoAgenciaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEnderecoAgenciaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEnderecoAgenciaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEnderecoAgenciaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEnderecoAgenciaQuery leftJoinCidade($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cidade relation
 * @method     ChildEnderecoAgenciaQuery rightJoinCidade($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cidade relation
 * @method     ChildEnderecoAgenciaQuery innerJoinCidade($relationAlias = null) Adds a INNER JOIN clause to the query using the Cidade relation
 *
 * @method     ChildEnderecoAgenciaQuery joinWithCidade($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cidade relation
 *
 * @method     ChildEnderecoAgenciaQuery leftJoinWithCidade() Adds a LEFT JOIN clause and with to the query using the Cidade relation
 * @method     ChildEnderecoAgenciaQuery rightJoinWithCidade() Adds a RIGHT JOIN clause and with to the query using the Cidade relation
 * @method     ChildEnderecoAgenciaQuery innerJoinWithCidade() Adds a INNER JOIN clause and with to the query using the Cidade relation
 *
 * @method     ChildEnderecoAgenciaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildEnderecoAgenciaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildEnderecoAgenciaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildEnderecoAgenciaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildEnderecoAgenciaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEnderecoAgenciaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEnderecoAgenciaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildEnderecoAgenciaQuery leftJoinBancoAgencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the BancoAgencia relation
 * @method     ChildEnderecoAgenciaQuery rightJoinBancoAgencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BancoAgencia relation
 * @method     ChildEnderecoAgenciaQuery innerJoinBancoAgencia($relationAlias = null) Adds a INNER JOIN clause to the query using the BancoAgencia relation
 *
 * @method     ChildEnderecoAgenciaQuery joinWithBancoAgencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BancoAgencia relation
 *
 * @method     ChildEnderecoAgenciaQuery leftJoinWithBancoAgencia() Adds a LEFT JOIN clause and with to the query using the BancoAgencia relation
 * @method     ChildEnderecoAgenciaQuery rightJoinWithBancoAgencia() Adds a RIGHT JOIN clause and with to the query using the BancoAgencia relation
 * @method     ChildEnderecoAgenciaQuery innerJoinWithBancoAgencia() Adds a INNER JOIN clause and with to the query using the BancoAgencia relation
 *
 * @method     \ImaTelecomBundle\Model\CidadeQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BancoAgenciaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEnderecoAgencia findOne(ConnectionInterface $con = null) Return the first ChildEnderecoAgencia matching the query
 * @method     ChildEnderecoAgencia findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEnderecoAgencia matching the query, or a new ChildEnderecoAgencia object populated from the query conditions when no match is found
 *
 * @method     ChildEnderecoAgencia findOneByIdenderecoAgencia(int $idendereco_agencia) Return the first ChildEnderecoAgencia filtered by the idendereco_agencia column
 * @method     ChildEnderecoAgencia findOneByRua(string $rua) Return the first ChildEnderecoAgencia filtered by the rua column
 * @method     ChildEnderecoAgencia findOneByBairro(string $bairro) Return the first ChildEnderecoAgencia filtered by the bairro column
 * @method     ChildEnderecoAgencia findOneByNum(int $num) Return the first ChildEnderecoAgencia filtered by the num column
 * @method     ChildEnderecoAgencia findOneByComplemento(string $complemento) Return the first ChildEnderecoAgencia filtered by the complemento column
 * @method     ChildEnderecoAgencia findOneByCidadeId(int $cidade_id) Return the first ChildEnderecoAgencia filtered by the cidade_id column
 * @method     ChildEnderecoAgencia findOneByCep(string $cep) Return the first ChildEnderecoAgencia filtered by the cep column
 * @method     ChildEnderecoAgencia findOneByUf(string $uf) Return the first ChildEnderecoAgencia filtered by the uf column
 * @method     ChildEnderecoAgencia findOneByPontoReferencia(string $ponto_referencia) Return the first ChildEnderecoAgencia filtered by the ponto_referencia column
 * @method     ChildEnderecoAgencia findOneByDataCadastro(string $data_cadastro) Return the first ChildEnderecoAgencia filtered by the data_cadastro column
 * @method     ChildEnderecoAgencia findOneByDataAlterado(string $data_alterado) Return the first ChildEnderecoAgencia filtered by the data_alterado column
 * @method     ChildEnderecoAgencia findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEnderecoAgencia filtered by the usuario_alterado column *

 * @method     ChildEnderecoAgencia requirePk($key, ConnectionInterface $con = null) Return the ChildEnderecoAgencia by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOne(ConnectionInterface $con = null) Return the first ChildEnderecoAgencia matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEnderecoAgencia requireOneByIdenderecoAgencia(int $idendereco_agencia) Return the first ChildEnderecoAgencia filtered by the idendereco_agencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByRua(string $rua) Return the first ChildEnderecoAgencia filtered by the rua column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByBairro(string $bairro) Return the first ChildEnderecoAgencia filtered by the bairro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByNum(int $num) Return the first ChildEnderecoAgencia filtered by the num column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByComplemento(string $complemento) Return the first ChildEnderecoAgencia filtered by the complemento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByCidadeId(int $cidade_id) Return the first ChildEnderecoAgencia filtered by the cidade_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByCep(string $cep) Return the first ChildEnderecoAgencia filtered by the cep column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByUf(string $uf) Return the first ChildEnderecoAgencia filtered by the uf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByPontoReferencia(string $ponto_referencia) Return the first ChildEnderecoAgencia filtered by the ponto_referencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByDataCadastro(string $data_cadastro) Return the first ChildEnderecoAgencia filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByDataAlterado(string $data_alterado) Return the first ChildEnderecoAgencia filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoAgencia requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEnderecoAgencia filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEnderecoAgencia[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEnderecoAgencia objects based on current ModelCriteria
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByIdenderecoAgencia(int $idendereco_agencia) Return ChildEnderecoAgencia objects filtered by the idendereco_agencia column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByRua(string $rua) Return ChildEnderecoAgencia objects filtered by the rua column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByBairro(string $bairro) Return ChildEnderecoAgencia objects filtered by the bairro column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByNum(int $num) Return ChildEnderecoAgencia objects filtered by the num column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByComplemento(string $complemento) Return ChildEnderecoAgencia objects filtered by the complemento column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByCidadeId(int $cidade_id) Return ChildEnderecoAgencia objects filtered by the cidade_id column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByCep(string $cep) Return ChildEnderecoAgencia objects filtered by the cep column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByUf(string $uf) Return ChildEnderecoAgencia objects filtered by the uf column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByPontoReferencia(string $ponto_referencia) Return ChildEnderecoAgencia objects filtered by the ponto_referencia column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildEnderecoAgencia objects filtered by the data_cadastro column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildEnderecoAgencia objects filtered by the data_alterado column
 * @method     ChildEnderecoAgencia[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildEnderecoAgencia objects filtered by the usuario_alterado column
 * @method     ChildEnderecoAgencia[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EnderecoAgenciaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\EnderecoAgenciaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\EnderecoAgencia', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEnderecoAgenciaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEnderecoAgenciaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEnderecoAgenciaQuery) {
            return $criteria;
        }
        $query = new ChildEnderecoAgenciaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEnderecoAgencia|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EnderecoAgenciaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EnderecoAgenciaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEnderecoAgencia A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idendereco_agencia, rua, bairro, num, complemento, cidade_id, cep, uf, ponto_referencia, data_cadastro, data_alterado, usuario_alterado FROM endereco_agencia WHERE idendereco_agencia = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEnderecoAgencia $obj */
            $obj = new ChildEnderecoAgencia();
            $obj->hydrate($row);
            EnderecoAgenciaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEnderecoAgencia|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idendereco_agencia column
     *
     * Example usage:
     * <code>
     * $query->filterByIdenderecoAgencia(1234); // WHERE idendereco_agencia = 1234
     * $query->filterByIdenderecoAgencia(array(12, 34)); // WHERE idendereco_agencia IN (12, 34)
     * $query->filterByIdenderecoAgencia(array('min' => 12)); // WHERE idendereco_agencia > 12
     * </code>
     *
     * @param     mixed $idenderecoAgencia The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByIdenderecoAgencia($idenderecoAgencia = null, $comparison = null)
    {
        if (is_array($idenderecoAgencia)) {
            $useMinMax = false;
            if (isset($idenderecoAgencia['min'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA, $idenderecoAgencia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idenderecoAgencia['max'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA, $idenderecoAgencia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA, $idenderecoAgencia, $comparison);
    }

    /**
     * Filter the query on the rua column
     *
     * Example usage:
     * <code>
     * $query->filterByRua('fooValue');   // WHERE rua = 'fooValue'
     * $query->filterByRua('%fooValue%', Criteria::LIKE); // WHERE rua LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rua The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByRua($rua = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rua)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_RUA, $rua, $comparison);
    }

    /**
     * Filter the query on the bairro column
     *
     * Example usage:
     * <code>
     * $query->filterByBairro('fooValue');   // WHERE bairro = 'fooValue'
     * $query->filterByBairro('%fooValue%', Criteria::LIKE); // WHERE bairro LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bairro The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByBairro($bairro = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bairro)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_BAIRRO, $bairro, $comparison);
    }

    /**
     * Filter the query on the num column
     *
     * Example usage:
     * <code>
     * $query->filterByNum(1234); // WHERE num = 1234
     * $query->filterByNum(array(12, 34)); // WHERE num IN (12, 34)
     * $query->filterByNum(array('min' => 12)); // WHERE num > 12
     * </code>
     *
     * @param     mixed $num The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByNum($num = null, $comparison = null)
    {
        if (is_array($num)) {
            $useMinMax = false;
            if (isset($num['min'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_NUM, $num['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($num['max'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_NUM, $num['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_NUM, $num, $comparison);
    }

    /**
     * Filter the query on the complemento column
     *
     * Example usage:
     * <code>
     * $query->filterByComplemento('fooValue');   // WHERE complemento = 'fooValue'
     * $query->filterByComplemento('%fooValue%', Criteria::LIKE); // WHERE complemento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $complemento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByComplemento($complemento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($complemento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_COMPLEMENTO, $complemento, $comparison);
    }

    /**
     * Filter the query on the cidade_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCidadeId(1234); // WHERE cidade_id = 1234
     * $query->filterByCidadeId(array(12, 34)); // WHERE cidade_id IN (12, 34)
     * $query->filterByCidadeId(array('min' => 12)); // WHERE cidade_id > 12
     * </code>
     *
     * @see       filterByCidade()
     *
     * @param     mixed $cidadeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByCidadeId($cidadeId = null, $comparison = null)
    {
        if (is_array($cidadeId)) {
            $useMinMax = false;
            if (isset($cidadeId['min'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_CIDADE_ID, $cidadeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cidadeId['max'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_CIDADE_ID, $cidadeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_CIDADE_ID, $cidadeId, $comparison);
    }

    /**
     * Filter the query on the cep column
     *
     * Example usage:
     * <code>
     * $query->filterByCep('fooValue');   // WHERE cep = 'fooValue'
     * $query->filterByCep('%fooValue%', Criteria::LIKE); // WHERE cep LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cep The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByCep($cep = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cep)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_CEP, $cep, $comparison);
    }

    /**
     * Filter the query on the uf column
     *
     * Example usage:
     * <code>
     * $query->filterByUf('fooValue');   // WHERE uf = 'fooValue'
     * $query->filterByUf('%fooValue%', Criteria::LIKE); // WHERE uf LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uf The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByUf($uf = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uf)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_UF, $uf, $comparison);
    }

    /**
     * Filter the query on the ponto_referencia column
     *
     * Example usage:
     * <code>
     * $query->filterByPontoReferencia('fooValue');   // WHERE ponto_referencia = 'fooValue'
     * $query->filterByPontoReferencia('%fooValue%', Criteria::LIKE); // WHERE ponto_referencia LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pontoReferencia The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByPontoReferencia($pontoReferencia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pontoReferencia)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_PONTO_REFERENCIA, $pontoReferencia, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(EnderecoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cidade object
     *
     * @param \ImaTelecomBundle\Model\Cidade|ObjectCollection $cidade The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByCidade($cidade, $comparison = null)
    {
        if ($cidade instanceof \ImaTelecomBundle\Model\Cidade) {
            return $this
                ->addUsingAlias(EnderecoAgenciaTableMap::COL_CIDADE_ID, $cidade->getIdcidade(), $comparison);
        } elseif ($cidade instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EnderecoAgenciaTableMap::COL_CIDADE_ID, $cidade->toKeyValue('PrimaryKey', 'Idcidade'), $comparison);
        } else {
            throw new PropelException('filterByCidade() only accepts arguments of type \ImaTelecomBundle\Model\Cidade or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cidade relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function joinCidade($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cidade');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cidade');
        }

        return $this;
    }

    /**
     * Use the Cidade relation Cidade object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CidadeQuery A secondary query class using the current class as primary query
     */
    public function useCidadeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCidade($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cidade', '\ImaTelecomBundle\Model\CidadeQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(EnderecoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EnderecoAgenciaTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BancoAgencia object
     *
     * @param \ImaTelecomBundle\Model\BancoAgencia|ObjectCollection $bancoAgencia the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function filterByBancoAgencia($bancoAgencia, $comparison = null)
    {
        if ($bancoAgencia instanceof \ImaTelecomBundle\Model\BancoAgencia) {
            return $this
                ->addUsingAlias(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA, $bancoAgencia->getEnderecoAgenciaId(), $comparison);
        } elseif ($bancoAgencia instanceof ObjectCollection) {
            return $this
                ->useBancoAgenciaQuery()
                ->filterByPrimaryKeys($bancoAgencia->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBancoAgencia() only accepts arguments of type \ImaTelecomBundle\Model\BancoAgencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BancoAgencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function joinBancoAgencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BancoAgencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BancoAgencia');
        }

        return $this;
    }

    /**
     * Use the BancoAgencia relation BancoAgencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoAgenciaQuery A secondary query class using the current class as primary query
     */
    public function useBancoAgenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBancoAgencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BancoAgencia', '\ImaTelecomBundle\Model\BancoAgenciaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEnderecoAgencia $enderecoAgencia Object to remove from the list of results
     *
     * @return $this|ChildEnderecoAgenciaQuery The current query, for fluid interface
     */
    public function prune($enderecoAgencia = null)
    {
        if ($enderecoAgencia) {
            $this->addUsingAlias(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA, $enderecoAgencia->getIdenderecoAgencia(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the endereco_agencia table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EnderecoAgenciaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EnderecoAgenciaTableMap::clearInstancePool();
            EnderecoAgenciaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EnderecoAgenciaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EnderecoAgenciaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EnderecoAgenciaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EnderecoAgenciaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EnderecoAgenciaQuery
