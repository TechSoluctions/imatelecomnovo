<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Planos as ChildPlanos;
use ImaTelecomBundle\Model\PlanosQuery as ChildPlanosQuery;
use ImaTelecomBundle\Model\Map\PlanosTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'planos' table.
 *
 *
 *
 * @method     ChildPlanosQuery orderByIdplano($order = Criteria::ASC) Order by the idplano column
 * @method     ChildPlanosQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildPlanosQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method     ChildPlanosQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildPlanosQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildPlanosQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildPlanosQuery orderByValor($order = Criteria::ASC) Order by the valor column
 *
 * @method     ChildPlanosQuery groupByIdplano() Group by the idplano column
 * @method     ChildPlanosQuery groupByNome() Group by the nome column
 * @method     ChildPlanosQuery groupByTipo() Group by the tipo column
 * @method     ChildPlanosQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildPlanosQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildPlanosQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildPlanosQuery groupByValor() Group by the valor column
 *
 * @method     ChildPlanosQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPlanosQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPlanosQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPlanosQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPlanosQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPlanosQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPlanosQuery leftJoinServicoPrestado($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicoPrestado relation
 * @method     ChildPlanosQuery rightJoinServicoPrestado($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicoPrestado relation
 * @method     ChildPlanosQuery innerJoinServicoPrestado($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicoPrestado relation
 *
 * @method     ChildPlanosQuery joinWithServicoPrestado($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicoPrestado relation
 *
 * @method     ChildPlanosQuery leftJoinWithServicoPrestado() Adds a LEFT JOIN clause and with to the query using the ServicoPrestado relation
 * @method     ChildPlanosQuery rightJoinWithServicoPrestado() Adds a RIGHT JOIN clause and with to the query using the ServicoPrestado relation
 * @method     ChildPlanosQuery innerJoinWithServicoPrestado() Adds a INNER JOIN clause and with to the query using the ServicoPrestado relation
 *
 * @method     \ImaTelecomBundle\Model\ServicoPrestadoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPlanos findOne(ConnectionInterface $con = null) Return the first ChildPlanos matching the query
 * @method     ChildPlanos findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPlanos matching the query, or a new ChildPlanos object populated from the query conditions when no match is found
 *
 * @method     ChildPlanos findOneByIdplano(int $idplano) Return the first ChildPlanos filtered by the idplano column
 * @method     ChildPlanos findOneByNome(string $nome) Return the first ChildPlanos filtered by the nome column
 * @method     ChildPlanos findOneByTipo(string $tipo) Return the first ChildPlanos filtered by the tipo column
 * @method     ChildPlanos findOneByDataCadastro(string $data_cadastro) Return the first ChildPlanos filtered by the data_cadastro column
 * @method     ChildPlanos findOneByDataAlterado(string $data_alterado) Return the first ChildPlanos filtered by the data_alterado column
 * @method     ChildPlanos findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildPlanos filtered by the usuario_alterado column
 * @method     ChildPlanos findOneByValor(string $valor) Return the first ChildPlanos filtered by the valor column *

 * @method     ChildPlanos requirePk($key, ConnectionInterface $con = null) Return the ChildPlanos by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlanos requireOne(ConnectionInterface $con = null) Return the first ChildPlanos matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlanos requireOneByIdplano(int $idplano) Return the first ChildPlanos filtered by the idplano column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlanos requireOneByNome(string $nome) Return the first ChildPlanos filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlanos requireOneByTipo(string $tipo) Return the first ChildPlanos filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlanos requireOneByDataCadastro(string $data_cadastro) Return the first ChildPlanos filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlanos requireOneByDataAlterado(string $data_alterado) Return the first ChildPlanos filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlanos requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildPlanos filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPlanos requireOneByValor(string $valor) Return the first ChildPlanos filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPlanos[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPlanos objects based on current ModelCriteria
 * @method     ChildPlanos[]|ObjectCollection findByIdplano(int $idplano) Return ChildPlanos objects filtered by the idplano column
 * @method     ChildPlanos[]|ObjectCollection findByNome(string $nome) Return ChildPlanos objects filtered by the nome column
 * @method     ChildPlanos[]|ObjectCollection findByTipo(string $tipo) Return ChildPlanos objects filtered by the tipo column
 * @method     ChildPlanos[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildPlanos objects filtered by the data_cadastro column
 * @method     ChildPlanos[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildPlanos objects filtered by the data_alterado column
 * @method     ChildPlanos[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildPlanos objects filtered by the usuario_alterado column
 * @method     ChildPlanos[]|ObjectCollection findByValor(string $valor) Return ChildPlanos objects filtered by the valor column
 * @method     ChildPlanos[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PlanosQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\PlanosQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Planos', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPlanosQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPlanosQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPlanosQuery) {
            return $criteria;
        }
        $query = new ChildPlanosQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPlanos|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PlanosTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PlanosTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPlanos A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idplano, nome, tipo, data_cadastro, data_alterado, usuario_alterado, valor FROM planos WHERE idplano = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPlanos $obj */
            $obj = new ChildPlanos();
            $obj->hydrate($row);
            PlanosTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPlanos|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PlanosTableMap::COL_IDPLANO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PlanosTableMap::COL_IDPLANO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idplano column
     *
     * Example usage:
     * <code>
     * $query->filterByIdplano(1234); // WHERE idplano = 1234
     * $query->filterByIdplano(array(12, 34)); // WHERE idplano IN (12, 34)
     * $query->filterByIdplano(array('min' => 12)); // WHERE idplano > 12
     * </code>
     *
     * @param     mixed $idplano The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByIdplano($idplano = null, $comparison = null)
    {
        if (is_array($idplano)) {
            $useMinMax = false;
            if (isset($idplano['min'])) {
                $this->addUsingAlias(PlanosTableMap::COL_IDPLANO, $idplano['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idplano['max'])) {
                $this->addUsingAlias(PlanosTableMap::COL_IDPLANO, $idplano['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlanosTableMap::COL_IDPLANO, $idplano, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlanosTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlanosTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(PlanosTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(PlanosTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlanosTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(PlanosTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(PlanosTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlanosTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(PlanosTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(PlanosTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlanosTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(PlanosTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(PlanosTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PlanosTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ServicoPrestado object
     *
     * @param \ImaTelecomBundle\Model\ServicoPrestado|ObjectCollection $servicoPrestado the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPlanosQuery The current query, for fluid interface
     */
    public function filterByServicoPrestado($servicoPrestado, $comparison = null)
    {
        if ($servicoPrestado instanceof \ImaTelecomBundle\Model\ServicoPrestado) {
            return $this
                ->addUsingAlias(PlanosTableMap::COL_IDPLANO, $servicoPrestado->getPlanoId(), $comparison);
        } elseif ($servicoPrestado instanceof ObjectCollection) {
            return $this
                ->useServicoPrestadoQuery()
                ->filterByPrimaryKeys($servicoPrestado->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicoPrestado() only accepts arguments of type \ImaTelecomBundle\Model\ServicoPrestado or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicoPrestado relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function joinServicoPrestado($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicoPrestado');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicoPrestado');
        }

        return $this;
    }

    /**
     * Use the ServicoPrestado relation ServicoPrestado object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ServicoPrestadoQuery A secondary query class using the current class as primary query
     */
    public function useServicoPrestadoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinServicoPrestado($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicoPrestado', '\ImaTelecomBundle\Model\ServicoPrestadoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPlanos $planos Object to remove from the list of results
     *
     * @return $this|ChildPlanosQuery The current query, for fluid interface
     */
    public function prune($planos = null)
    {
        if ($planos) {
            $this->addUsingAlias(PlanosTableMap::COL_IDPLANO, $planos->getIdplano(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the planos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlanosTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PlanosTableMap::clearInstancePool();
            PlanosTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlanosTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PlanosTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PlanosTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PlanosTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PlanosQuery
