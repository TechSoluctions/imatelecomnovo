<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoItem as ChildBoletoItem;
use ImaTelecomBundle\Model\BoletoItemQuery as ChildBoletoItemQuery;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\DadosFiscal as ChildDadosFiscal;
use ImaTelecomBundle\Model\DadosFiscalItemQuery as ChildDadosFiscalItemQuery;
use ImaTelecomBundle\Model\DadosFiscalQuery as ChildDadosFiscalQuery;
use ImaTelecomBundle\Model\Map\DadosFiscalItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'dados_fiscal_item' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class DadosFiscalItem implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\DadosFiscalItemTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the iddados_fiscal_item field.
     *
     * @var        int
     */
    protected $iddados_fiscal_item;

    /**
     * The value for the boleto_id field.
     *
     * @var        int
     */
    protected $boleto_id;

    /**
     * The value for the boleto_item_id field.
     *
     * @var        int
     */
    protected $boleto_item_id;

    /**
     * The value for the item field.
     *
     * @var        string
     */
    protected $item;

    /**
     * The value for the numero_item_nota_fiscal field.
     *
     * @var        int
     */
    protected $numero_item_nota_fiscal;

    /**
     * The value for the valor_item field.
     *
     * @var        string
     */
    protected $valor_item;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the dados_fiscal_id field.
     *
     * @var        int
     */
    protected $dados_fiscal_id;

    /**
     * The value for the posicao_item field.
     *
     * @var        int
     */
    protected $posicao_item;

    /**
     * The value for the valor_scm field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_scm;

    /**
     * The value for the valor_sva field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_sva;

    /**
     * The value for the tem_scm field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $tem_scm;

    /**
     * The value for the tem_sva field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $tem_sva;

    /**
     * @var        ChildBoleto
     */
    protected $aBoleto;

    /**
     * @var        ChildBoletoItem
     */
    protected $aBoletoItem;

    /**
     * @var        ChildDadosFiscal
     */
    protected $aDadosFiscal;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->valor_scm = '0.00';
        $this->valor_sva = '0.00';
        $this->tem_scm = false;
        $this->tem_sva = false;
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\DadosFiscalItem object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>DadosFiscalItem</code> instance.  If
     * <code>obj</code> is an instance of <code>DadosFiscalItem</code>, delegates to
     * <code>equals(DadosFiscalItem)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|DadosFiscalItem The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [iddados_fiscal_item] column value.
     *
     * @return int
     */
    public function getIddadosFiscalItem()
    {
        return $this->iddados_fiscal_item;
    }

    /**
     * Get the [boleto_id] column value.
     *
     * @return int
     */
    public function getBoletoId()
    {
        return $this->boleto_id;
    }

    /**
     * Get the [boleto_item_id] column value.
     *
     * @return int
     */
    public function getBoletoItemId()
    {
        return $this->boleto_item_id;
    }

    /**
     * Get the [item] column value.
     *
     * @return string
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Get the [numero_item_nota_fiscal] column value.
     *
     * @return int
     */
    public function getNumeroItemNotaFiscal()
    {
        return $this->numero_item_nota_fiscal;
    }

    /**
     * Get the [valor_item] column value.
     *
     * @return string
     */
    public function getValorItem()
    {
        return $this->valor_item;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [dados_fiscal_id] column value.
     *
     * @return int
     */
    public function getDadosFiscalId()
    {
        return $this->dados_fiscal_id;
    }

    /**
     * Get the [posicao_item] column value.
     *
     * @return int
     */
    public function getPosicaoItem()
    {
        return $this->posicao_item;
    }

    /**
     * Get the [valor_scm] column value.
     *
     * @return string
     */
    public function getValorScm()
    {
        return $this->valor_scm;
    }

    /**
     * Get the [valor_sva] column value.
     *
     * @return string
     */
    public function getValorSva()
    {
        return $this->valor_sva;
    }

    /**
     * Get the [tem_scm] column value.
     *
     * @return boolean
     */
    public function getTemScm()
    {
        return $this->tem_scm;
    }

    /**
     * Get the [tem_scm] column value.
     *
     * @return boolean
     */
    public function isTemScm()
    {
        return $this->getTemScm();
    }

    /**
     * Get the [tem_sva] column value.
     *
     * @return boolean
     */
    public function getTemSva()
    {
        return $this->tem_sva;
    }

    /**
     * Get the [tem_sva] column value.
     *
     * @return boolean
     */
    public function isTemSva()
    {
        return $this->getTemSva();
    }

    /**
     * Set the value of [iddados_fiscal_item] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setIddadosFiscalItem($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->iddados_fiscal_item !== $v) {
            $this->iddados_fiscal_item = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM] = true;
        }

        return $this;
    } // setIddadosFiscalItem()

    /**
     * Set the value of [boleto_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setBoletoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->boleto_id !== $v) {
            $this->boleto_id = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_BOLETO_ID] = true;
        }

        if ($this->aBoleto !== null && $this->aBoleto->getIdboleto() !== $v) {
            $this->aBoleto = null;
        }

        return $this;
    } // setBoletoId()

    /**
     * Set the value of [boleto_item_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setBoletoItemId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->boleto_item_id !== $v) {
            $this->boleto_item_id = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID] = true;
        }

        if ($this->aBoletoItem !== null && $this->aBoletoItem->getIdboletoItem() !== $v) {
            $this->aBoletoItem = null;
        }

        return $this;
    } // setBoletoItemId()

    /**
     * Set the value of [item] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setItem($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->item !== $v) {
            $this->item = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_ITEM] = true;
        }

        return $this;
    } // setItem()

    /**
     * Set the value of [numero_item_nota_fiscal] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setNumeroItemNotaFiscal($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->numero_item_nota_fiscal !== $v) {
            $this->numero_item_nota_fiscal = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL] = true;
        }

        return $this;
    } // setNumeroItemNotaFiscal()

    /**
     * Set the value of [valor_item] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setValorItem($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_item !== $v) {
            $this->valor_item = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_VALOR_ITEM] = true;
        }

        return $this;
    } // setValorItem()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[DadosFiscalItemTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[DadosFiscalItemTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [dados_fiscal_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setDadosFiscalId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->dados_fiscal_id !== $v) {
            $this->dados_fiscal_id = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID] = true;
        }

        if ($this->aDadosFiscal !== null && $this->aDadosFiscal->getId() !== $v) {
            $this->aDadosFiscal = null;
        }

        return $this;
    } // setDadosFiscalId()

    /**
     * Set the value of [posicao_item] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setPosicaoItem($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->posicao_item !== $v) {
            $this->posicao_item = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_POSICAO_ITEM] = true;
        }

        return $this;
    } // setPosicaoItem()

    /**
     * Set the value of [valor_scm] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setValorScm($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_scm !== $v) {
            $this->valor_scm = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_VALOR_SCM] = true;
        }

        return $this;
    } // setValorScm()

    /**
     * Set the value of [valor_sva] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setValorSva($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_sva !== $v) {
            $this->valor_sva = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_VALOR_SVA] = true;
        }

        return $this;
    } // setValorSva()

    /**
     * Sets the value of the [tem_scm] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setTemScm($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->tem_scm !== $v) {
            $this->tem_scm = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_TEM_SCM] = true;
        }

        return $this;
    } // setTemScm()

    /**
     * Sets the value of the [tem_sva] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     */
    public function setTemSva($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->tem_sva !== $v) {
            $this->tem_sva = $v;
            $this->modifiedColumns[DadosFiscalItemTableMap::COL_TEM_SVA] = true;
        }

        return $this;
    } // setTemSva()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->valor_scm !== '0.00') {
                return false;
            }

            if ($this->valor_sva !== '0.00') {
                return false;
            }

            if ($this->tem_scm !== false) {
                return false;
            }

            if ($this->tem_sva !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : DadosFiscalItemTableMap::translateFieldName('IddadosFiscalItem', TableMap::TYPE_PHPNAME, $indexType)];
            $this->iddados_fiscal_item = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : DadosFiscalItemTableMap::translateFieldName('BoletoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->boleto_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : DadosFiscalItemTableMap::translateFieldName('BoletoItemId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->boleto_item_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : DadosFiscalItemTableMap::translateFieldName('Item', TableMap::TYPE_PHPNAME, $indexType)];
            $this->item = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : DadosFiscalItemTableMap::translateFieldName('NumeroItemNotaFiscal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->numero_item_nota_fiscal = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : DadosFiscalItemTableMap::translateFieldName('ValorItem', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_item = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : DadosFiscalItemTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : DadosFiscalItemTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : DadosFiscalItemTableMap::translateFieldName('DadosFiscalId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dados_fiscal_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : DadosFiscalItemTableMap::translateFieldName('PosicaoItem', TableMap::TYPE_PHPNAME, $indexType)];
            $this->posicao_item = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : DadosFiscalItemTableMap::translateFieldName('ValorScm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_scm = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : DadosFiscalItemTableMap::translateFieldName('ValorSva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_sva = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : DadosFiscalItemTableMap::translateFieldName('TemScm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tem_scm = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : DadosFiscalItemTableMap::translateFieldName('TemSva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tem_sva = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = DadosFiscalItemTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\DadosFiscalItem'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aBoleto !== null && $this->boleto_id !== $this->aBoleto->getIdboleto()) {
            $this->aBoleto = null;
        }
        if ($this->aBoletoItem !== null && $this->boleto_item_id !== $this->aBoletoItem->getIdboletoItem()) {
            $this->aBoletoItem = null;
        }
        if ($this->aDadosFiscal !== null && $this->dados_fiscal_id !== $this->aDadosFiscal->getId()) {
            $this->aDadosFiscal = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DadosFiscalItemTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildDadosFiscalItemQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBoleto = null;
            $this->aBoletoItem = null;
            $this->aDadosFiscal = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see DadosFiscalItem::setDeleted()
     * @see DadosFiscalItem::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalItemTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildDadosFiscalItemQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalItemTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DadosFiscalItemTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBoleto !== null) {
                if ($this->aBoleto->isModified() || $this->aBoleto->isNew()) {
                    $affectedRows += $this->aBoleto->save($con);
                }
                $this->setBoleto($this->aBoleto);
            }

            if ($this->aBoletoItem !== null) {
                if ($this->aBoletoItem->isModified() || $this->aBoletoItem->isNew()) {
                    $affectedRows += $this->aBoletoItem->save($con);
                }
                $this->setBoletoItem($this->aBoletoItem);
            }

            if ($this->aDadosFiscal !== null) {
                if ($this->aDadosFiscal->isModified() || $this->aDadosFiscal->isNew()) {
                    $affectedRows += $this->aDadosFiscal->save($con);
                }
                $this->setDadosFiscal($this->aDadosFiscal);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM] = true;
        if (null !== $this->iddados_fiscal_item) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM)) {
            $modifiedColumns[':p' . $index++]  = 'iddados_fiscal_item';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_BOLETO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'boleto_id';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'boleto_item_id';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_ITEM)) {
            $modifiedColumns[':p' . $index++]  = 'item';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL)) {
            $modifiedColumns[':p' . $index++]  = 'numero_item_nota_fiscal';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_VALOR_ITEM)) {
            $modifiedColumns[':p' . $index++]  = 'valor_item';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'dados_fiscal_id';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_POSICAO_ITEM)) {
            $modifiedColumns[':p' . $index++]  = 'posicao_item';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_VALOR_SCM)) {
            $modifiedColumns[':p' . $index++]  = 'valor_scm';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_VALOR_SVA)) {
            $modifiedColumns[':p' . $index++]  = 'valor_sva';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_TEM_SCM)) {
            $modifiedColumns[':p' . $index++]  = 'tem_scm';
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_TEM_SVA)) {
            $modifiedColumns[':p' . $index++]  = 'tem_sva';
        }

        $sql = sprintf(
            'INSERT INTO dados_fiscal_item (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'iddados_fiscal_item':
                        $stmt->bindValue($identifier, $this->iddados_fiscal_item, PDO::PARAM_INT);
                        break;
                    case 'boleto_id':
                        $stmt->bindValue($identifier, $this->boleto_id, PDO::PARAM_INT);
                        break;
                    case 'boleto_item_id':
                        $stmt->bindValue($identifier, $this->boleto_item_id, PDO::PARAM_INT);
                        break;
                    case 'item':
                        $stmt->bindValue($identifier, $this->item, PDO::PARAM_STR);
                        break;
                    case 'numero_item_nota_fiscal':
                        $stmt->bindValue($identifier, $this->numero_item_nota_fiscal, PDO::PARAM_INT);
                        break;
                    case 'valor_item':
                        $stmt->bindValue($identifier, $this->valor_item, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'dados_fiscal_id':
                        $stmt->bindValue($identifier, $this->dados_fiscal_id, PDO::PARAM_INT);
                        break;
                    case 'posicao_item':
                        $stmt->bindValue($identifier, $this->posicao_item, PDO::PARAM_INT);
                        break;
                    case 'valor_scm':
                        $stmt->bindValue($identifier, $this->valor_scm, PDO::PARAM_STR);
                        break;
                    case 'valor_sva':
                        $stmt->bindValue($identifier, $this->valor_sva, PDO::PARAM_STR);
                        break;
                    case 'tem_scm':
                        $stmt->bindValue($identifier, (int) $this->tem_scm, PDO::PARAM_INT);
                        break;
                    case 'tem_sva':
                        $stmt->bindValue($identifier, (int) $this->tem_sva, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIddadosFiscalItem($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DadosFiscalItemTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIddadosFiscalItem();
                break;
            case 1:
                return $this->getBoletoId();
                break;
            case 2:
                return $this->getBoletoItemId();
                break;
            case 3:
                return $this->getItem();
                break;
            case 4:
                return $this->getNumeroItemNotaFiscal();
                break;
            case 5:
                return $this->getValorItem();
                break;
            case 6:
                return $this->getDataCadastro();
                break;
            case 7:
                return $this->getDataAlterado();
                break;
            case 8:
                return $this->getDadosFiscalId();
                break;
            case 9:
                return $this->getPosicaoItem();
                break;
            case 10:
                return $this->getValorScm();
                break;
            case 11:
                return $this->getValorSva();
                break;
            case 12:
                return $this->getTemScm();
                break;
            case 13:
                return $this->getTemSva();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['DadosFiscalItem'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['DadosFiscalItem'][$this->hashCode()] = true;
        $keys = DadosFiscalItemTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIddadosFiscalItem(),
            $keys[1] => $this->getBoletoId(),
            $keys[2] => $this->getBoletoItemId(),
            $keys[3] => $this->getItem(),
            $keys[4] => $this->getNumeroItemNotaFiscal(),
            $keys[5] => $this->getValorItem(),
            $keys[6] => $this->getDataCadastro(),
            $keys[7] => $this->getDataAlterado(),
            $keys[8] => $this->getDadosFiscalId(),
            $keys[9] => $this->getPosicaoItem(),
            $keys[10] => $this->getValorScm(),
            $keys[11] => $this->getValorSva(),
            $keys[12] => $this->getTemScm(),
            $keys[13] => $this->getTemSva(),
        );
        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBoleto) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boleto';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto';
                        break;
                    default:
                        $key = 'Boleto';
                }

                $result[$key] = $this->aBoleto->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBoletoItem) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoItem';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_item';
                        break;
                    default:
                        $key = 'BoletoItem';
                }

                $result[$key] = $this->aBoletoItem->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aDadosFiscal) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dadosFiscal';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'dados_fiscal';
                        break;
                    default:
                        $key = 'DadosFiscal';
                }

                $result[$key] = $this->aDadosFiscal->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DadosFiscalItemTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIddadosFiscalItem($value);
                break;
            case 1:
                $this->setBoletoId($value);
                break;
            case 2:
                $this->setBoletoItemId($value);
                break;
            case 3:
                $this->setItem($value);
                break;
            case 4:
                $this->setNumeroItemNotaFiscal($value);
                break;
            case 5:
                $this->setValorItem($value);
                break;
            case 6:
                $this->setDataCadastro($value);
                break;
            case 7:
                $this->setDataAlterado($value);
                break;
            case 8:
                $this->setDadosFiscalId($value);
                break;
            case 9:
                $this->setPosicaoItem($value);
                break;
            case 10:
                $this->setValorScm($value);
                break;
            case 11:
                $this->setValorSva($value);
                break;
            case 12:
                $this->setTemScm($value);
                break;
            case 13:
                $this->setTemSva($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = DadosFiscalItemTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIddadosFiscalItem($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBoletoId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setBoletoItemId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setItem($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setNumeroItemNotaFiscal($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setValorItem($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDataCadastro($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDataAlterado($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDadosFiscalId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPosicaoItem($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setValorScm($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setValorSva($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setTemScm($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setTemSva($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DadosFiscalItemTableMap::DATABASE_NAME);

        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM)) {
            $criteria->add(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, $this->iddados_fiscal_item);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_BOLETO_ID)) {
            $criteria->add(DadosFiscalItemTableMap::COL_BOLETO_ID, $this->boleto_id);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID)) {
            $criteria->add(DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID, $this->boleto_item_id);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_ITEM)) {
            $criteria->add(DadosFiscalItemTableMap::COL_ITEM, $this->item);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL)) {
            $criteria->add(DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL, $this->numero_item_nota_fiscal);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_VALOR_ITEM)) {
            $criteria->add(DadosFiscalItemTableMap::COL_VALOR_ITEM, $this->valor_item);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(DadosFiscalItemTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(DadosFiscalItemTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID)) {
            $criteria->add(DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID, $this->dados_fiscal_id);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_POSICAO_ITEM)) {
            $criteria->add(DadosFiscalItemTableMap::COL_POSICAO_ITEM, $this->posicao_item);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_VALOR_SCM)) {
            $criteria->add(DadosFiscalItemTableMap::COL_VALOR_SCM, $this->valor_scm);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_VALOR_SVA)) {
            $criteria->add(DadosFiscalItemTableMap::COL_VALOR_SVA, $this->valor_sva);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_TEM_SCM)) {
            $criteria->add(DadosFiscalItemTableMap::COL_TEM_SCM, $this->tem_scm);
        }
        if ($this->isColumnModified(DadosFiscalItemTableMap::COL_TEM_SVA)) {
            $criteria->add(DadosFiscalItemTableMap::COL_TEM_SVA, $this->tem_sva);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildDadosFiscalItemQuery::create();
        $criteria->add(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, $this->iddados_fiscal_item);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIddadosFiscalItem();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIddadosFiscalItem();
    }

    /**
     * Generic method to set the primary key (iddados_fiscal_item column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIddadosFiscalItem($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIddadosFiscalItem();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\DadosFiscalItem (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBoletoId($this->getBoletoId());
        $copyObj->setBoletoItemId($this->getBoletoItemId());
        $copyObj->setItem($this->getItem());
        $copyObj->setNumeroItemNotaFiscal($this->getNumeroItemNotaFiscal());
        $copyObj->setValorItem($this->getValorItem());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setDadosFiscalId($this->getDadosFiscalId());
        $copyObj->setPosicaoItem($this->getPosicaoItem());
        $copyObj->setValorScm($this->getValorScm());
        $copyObj->setValorSva($this->getValorSva());
        $copyObj->setTemScm($this->getTemScm());
        $copyObj->setTemSva($this->getTemSva());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIddadosFiscalItem(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\DadosFiscalItem Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBoleto object.
     *
     * @param  ChildBoleto $v
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBoleto(ChildBoleto $v = null)
    {
        if ($v === null) {
            $this->setBoletoId(NULL);
        } else {
            $this->setBoletoId($v->getIdboleto());
        }

        $this->aBoleto = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBoleto object, it will not be re-added.
        if ($v !== null) {
            $v->addDadosFiscalItem($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBoleto object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBoleto The associated ChildBoleto object.
     * @throws PropelException
     */
    public function getBoleto(ConnectionInterface $con = null)
    {
        if ($this->aBoleto === null && ($this->boleto_id !== null)) {
            $this->aBoleto = ChildBoletoQuery::create()->findPk($this->boleto_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBoleto->addDadosFiscalItems($this);
             */
        }

        return $this->aBoleto;
    }

    /**
     * Declares an association between this object and a ChildBoletoItem object.
     *
     * @param  ChildBoletoItem $v
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBoletoItem(ChildBoletoItem $v = null)
    {
        if ($v === null) {
            $this->setBoletoItemId(NULL);
        } else {
            $this->setBoletoItemId($v->getIdboletoItem());
        }

        $this->aBoletoItem = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBoletoItem object, it will not be re-added.
        if ($v !== null) {
            $v->addDadosFiscalItem($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBoletoItem object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBoletoItem The associated ChildBoletoItem object.
     * @throws PropelException
     */
    public function getBoletoItem(ConnectionInterface $con = null)
    {
        if ($this->aBoletoItem === null && ($this->boleto_item_id !== null)) {
            $this->aBoletoItem = ChildBoletoItemQuery::create()->findPk($this->boleto_item_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBoletoItem->addDadosFiscalItems($this);
             */
        }

        return $this->aBoletoItem;
    }

    /**
     * Declares an association between this object and a ChildDadosFiscal object.
     *
     * @param  ChildDadosFiscal $v
     * @return $this|\ImaTelecomBundle\Model\DadosFiscalItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDadosFiscal(ChildDadosFiscal $v = null)
    {
        if ($v === null) {
            $this->setDadosFiscalId(NULL);
        } else {
            $this->setDadosFiscalId($v->getId());
        }

        $this->aDadosFiscal = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildDadosFiscal object, it will not be re-added.
        if ($v !== null) {
            $v->addDadosFiscalItem($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildDadosFiscal object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildDadosFiscal The associated ChildDadosFiscal object.
     * @throws PropelException
     */
    public function getDadosFiscal(ConnectionInterface $con = null)
    {
        if ($this->aDadosFiscal === null && ($this->dados_fiscal_id !== null)) {
            $this->aDadosFiscal = ChildDadosFiscalQuery::create()->findPk($this->dados_fiscal_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDadosFiscal->addDadosFiscalItems($this);
             */
        }

        return $this->aDadosFiscal;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBoleto) {
            $this->aBoleto->removeDadosFiscalItem($this);
        }
        if (null !== $this->aBoletoItem) {
            $this->aBoletoItem->removeDadosFiscalItem($this);
        }
        if (null !== $this->aDadosFiscal) {
            $this->aDadosFiscal->removeDadosFiscalItem($this);
        }
        $this->iddados_fiscal_item = null;
        $this->boleto_id = null;
        $this->boleto_item_id = null;
        $this->item = null;
        $this->numero_item_nota_fiscal = null;
        $this->valor_item = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->dados_fiscal_id = null;
        $this->posicao_item = null;
        $this->valor_scm = null;
        $this->valor_sva = null;
        $this->tem_scm = null;
        $this->tem_sva = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aBoleto = null;
        $this->aBoletoItem = null;
        $this->aDadosFiscal = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DadosFiscalItemTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
