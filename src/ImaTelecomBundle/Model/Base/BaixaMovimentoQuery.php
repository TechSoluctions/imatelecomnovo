<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BaixaMovimento as ChildBaixaMovimento;
use ImaTelecomBundle\Model\BaixaMovimentoQuery as ChildBaixaMovimentoQuery;
use ImaTelecomBundle\Model\Map\BaixaMovimentoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'baixa_movimento' table.
 *
 *
 *
 * @method     ChildBaixaMovimentoQuery orderByIdbaixaMovimento($order = Criteria::ASC) Order by the idbaixa_movimento column
 * @method     ChildBaixaMovimentoQuery orderByBaixaId($order = Criteria::ASC) Order by the baixa_id column
 * @method     ChildBaixaMovimentoQuery orderByFormaPagamentoId($order = Criteria::ASC) Order by the forma_pagamento_id column
 * @method     ChildBaixaMovimentoQuery orderByUsuarioBaixa($order = Criteria::ASC) Order by the usuario_baixa column
 * @method     ChildBaixaMovimentoQuery orderByDataBaixa($order = Criteria::ASC) Order by the data_baixa column
 * @method     ChildBaixaMovimentoQuery orderByValor($order = Criteria::ASC) Order by the valor column
 * @method     ChildBaixaMovimentoQuery orderByNumeroDocumento($order = Criteria::ASC) Order by the numero_documento column
 * @method     ChildBaixaMovimentoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildBaixaMovimentoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 *
 * @method     ChildBaixaMovimentoQuery groupByIdbaixaMovimento() Group by the idbaixa_movimento column
 * @method     ChildBaixaMovimentoQuery groupByBaixaId() Group by the baixa_id column
 * @method     ChildBaixaMovimentoQuery groupByFormaPagamentoId() Group by the forma_pagamento_id column
 * @method     ChildBaixaMovimentoQuery groupByUsuarioBaixa() Group by the usuario_baixa column
 * @method     ChildBaixaMovimentoQuery groupByDataBaixa() Group by the data_baixa column
 * @method     ChildBaixaMovimentoQuery groupByValor() Group by the valor column
 * @method     ChildBaixaMovimentoQuery groupByNumeroDocumento() Group by the numero_documento column
 * @method     ChildBaixaMovimentoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildBaixaMovimentoQuery groupByDataAlterado() Group by the data_alterado column
 *
 * @method     ChildBaixaMovimentoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBaixaMovimentoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBaixaMovimentoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBaixaMovimentoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBaixaMovimentoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBaixaMovimentoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBaixaMovimentoQuery leftJoinBaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Baixa relation
 * @method     ChildBaixaMovimentoQuery rightJoinBaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Baixa relation
 * @method     ChildBaixaMovimentoQuery innerJoinBaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Baixa relation
 *
 * @method     ChildBaixaMovimentoQuery joinWithBaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Baixa relation
 *
 * @method     ChildBaixaMovimentoQuery leftJoinWithBaixa() Adds a LEFT JOIN clause and with to the query using the Baixa relation
 * @method     ChildBaixaMovimentoQuery rightJoinWithBaixa() Adds a RIGHT JOIN clause and with to the query using the Baixa relation
 * @method     ChildBaixaMovimentoQuery innerJoinWithBaixa() Adds a INNER JOIN clause and with to the query using the Baixa relation
 *
 * @method     ChildBaixaMovimentoQuery leftJoinFormaPagamento($relationAlias = null) Adds a LEFT JOIN clause to the query using the FormaPagamento relation
 * @method     ChildBaixaMovimentoQuery rightJoinFormaPagamento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FormaPagamento relation
 * @method     ChildBaixaMovimentoQuery innerJoinFormaPagamento($relationAlias = null) Adds a INNER JOIN clause to the query using the FormaPagamento relation
 *
 * @method     ChildBaixaMovimentoQuery joinWithFormaPagamento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FormaPagamento relation
 *
 * @method     ChildBaixaMovimentoQuery leftJoinWithFormaPagamento() Adds a LEFT JOIN clause and with to the query using the FormaPagamento relation
 * @method     ChildBaixaMovimentoQuery rightJoinWithFormaPagamento() Adds a RIGHT JOIN clause and with to the query using the FormaPagamento relation
 * @method     ChildBaixaMovimentoQuery innerJoinWithFormaPagamento() Adds a INNER JOIN clause and with to the query using the FormaPagamento relation
 *
 * @method     ChildBaixaMovimentoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBaixaMovimentoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBaixaMovimentoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBaixaMovimentoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBaixaMovimentoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBaixaMovimentoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBaixaMovimentoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \ImaTelecomBundle\Model\BaixaQuery|\ImaTelecomBundle\Model\FormaPagamentoQuery|\ImaTelecomBundle\Model\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBaixaMovimento findOne(ConnectionInterface $con = null) Return the first ChildBaixaMovimento matching the query
 * @method     ChildBaixaMovimento findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBaixaMovimento matching the query, or a new ChildBaixaMovimento object populated from the query conditions when no match is found
 *
 * @method     ChildBaixaMovimento findOneByIdbaixaMovimento(int $idbaixa_movimento) Return the first ChildBaixaMovimento filtered by the idbaixa_movimento column
 * @method     ChildBaixaMovimento findOneByBaixaId(int $baixa_id) Return the first ChildBaixaMovimento filtered by the baixa_id column
 * @method     ChildBaixaMovimento findOneByFormaPagamentoId(int $forma_pagamento_id) Return the first ChildBaixaMovimento filtered by the forma_pagamento_id column
 * @method     ChildBaixaMovimento findOneByUsuarioBaixa(int $usuario_baixa) Return the first ChildBaixaMovimento filtered by the usuario_baixa column
 * @method     ChildBaixaMovimento findOneByDataBaixa(string $data_baixa) Return the first ChildBaixaMovimento filtered by the data_baixa column
 * @method     ChildBaixaMovimento findOneByValor(string $valor) Return the first ChildBaixaMovimento filtered by the valor column
 * @method     ChildBaixaMovimento findOneByNumeroDocumento(string $numero_documento) Return the first ChildBaixaMovimento filtered by the numero_documento column
 * @method     ChildBaixaMovimento findOneByDataCadastro(string $data_cadastro) Return the first ChildBaixaMovimento filtered by the data_cadastro column
 * @method     ChildBaixaMovimento findOneByDataAlterado(string $data_alterado) Return the first ChildBaixaMovimento filtered by the data_alterado column *

 * @method     ChildBaixaMovimento requirePk($key, ConnectionInterface $con = null) Return the ChildBaixaMovimento by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaMovimento requireOne(ConnectionInterface $con = null) Return the first ChildBaixaMovimento matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBaixaMovimento requireOneByIdbaixaMovimento(int $idbaixa_movimento) Return the first ChildBaixaMovimento filtered by the idbaixa_movimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaMovimento requireOneByBaixaId(int $baixa_id) Return the first ChildBaixaMovimento filtered by the baixa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaMovimento requireOneByFormaPagamentoId(int $forma_pagamento_id) Return the first ChildBaixaMovimento filtered by the forma_pagamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaMovimento requireOneByUsuarioBaixa(int $usuario_baixa) Return the first ChildBaixaMovimento filtered by the usuario_baixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaMovimento requireOneByDataBaixa(string $data_baixa) Return the first ChildBaixaMovimento filtered by the data_baixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaMovimento requireOneByValor(string $valor) Return the first ChildBaixaMovimento filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaMovimento requireOneByNumeroDocumento(string $numero_documento) Return the first ChildBaixaMovimento filtered by the numero_documento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaMovimento requireOneByDataCadastro(string $data_cadastro) Return the first ChildBaixaMovimento filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaMovimento requireOneByDataAlterado(string $data_alterado) Return the first ChildBaixaMovimento filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBaixaMovimento[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBaixaMovimento objects based on current ModelCriteria
 * @method     ChildBaixaMovimento[]|ObjectCollection findByIdbaixaMovimento(int $idbaixa_movimento) Return ChildBaixaMovimento objects filtered by the idbaixa_movimento column
 * @method     ChildBaixaMovimento[]|ObjectCollection findByBaixaId(int $baixa_id) Return ChildBaixaMovimento objects filtered by the baixa_id column
 * @method     ChildBaixaMovimento[]|ObjectCollection findByFormaPagamentoId(int $forma_pagamento_id) Return ChildBaixaMovimento objects filtered by the forma_pagamento_id column
 * @method     ChildBaixaMovimento[]|ObjectCollection findByUsuarioBaixa(int $usuario_baixa) Return ChildBaixaMovimento objects filtered by the usuario_baixa column
 * @method     ChildBaixaMovimento[]|ObjectCollection findByDataBaixa(string $data_baixa) Return ChildBaixaMovimento objects filtered by the data_baixa column
 * @method     ChildBaixaMovimento[]|ObjectCollection findByValor(string $valor) Return ChildBaixaMovimento objects filtered by the valor column
 * @method     ChildBaixaMovimento[]|ObjectCollection findByNumeroDocumento(string $numero_documento) Return ChildBaixaMovimento objects filtered by the numero_documento column
 * @method     ChildBaixaMovimento[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildBaixaMovimento objects filtered by the data_cadastro column
 * @method     ChildBaixaMovimento[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildBaixaMovimento objects filtered by the data_alterado column
 * @method     ChildBaixaMovimento[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BaixaMovimentoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\BaixaMovimentoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\BaixaMovimento', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBaixaMovimentoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBaixaMovimentoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBaixaMovimentoQuery) {
            return $criteria;
        }
        $query = new ChildBaixaMovimentoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBaixaMovimento|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BaixaMovimentoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BaixaMovimentoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaMovimento A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idbaixa_movimento, baixa_id, forma_pagamento_id, usuario_baixa, data_baixa, valor, numero_documento, data_cadastro, data_alterado FROM baixa_movimento WHERE idbaixa_movimento = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBaixaMovimento $obj */
            $obj = new ChildBaixaMovimento();
            $obj->hydrate($row);
            BaixaMovimentoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBaixaMovimento|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idbaixa_movimento column
     *
     * Example usage:
     * <code>
     * $query->filterByIdbaixaMovimento(1234); // WHERE idbaixa_movimento = 1234
     * $query->filterByIdbaixaMovimento(array(12, 34)); // WHERE idbaixa_movimento IN (12, 34)
     * $query->filterByIdbaixaMovimento(array('min' => 12)); // WHERE idbaixa_movimento > 12
     * </code>
     *
     * @param     mixed $idbaixaMovimento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByIdbaixaMovimento($idbaixaMovimento = null, $comparison = null)
    {
        if (is_array($idbaixaMovimento)) {
            $useMinMax = false;
            if (isset($idbaixaMovimento['min'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO, $idbaixaMovimento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idbaixaMovimento['max'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO, $idbaixaMovimento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO, $idbaixaMovimento, $comparison);
    }

    /**
     * Filter the query on the baixa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBaixaId(1234); // WHERE baixa_id = 1234
     * $query->filterByBaixaId(array(12, 34)); // WHERE baixa_id IN (12, 34)
     * $query->filterByBaixaId(array('min' => 12)); // WHERE baixa_id > 12
     * </code>
     *
     * @see       filterByBaixa()
     *
     * @param     mixed $baixaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByBaixaId($baixaId = null, $comparison = null)
    {
        if (is_array($baixaId)) {
            $useMinMax = false;
            if (isset($baixaId['min'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_BAIXA_ID, $baixaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baixaId['max'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_BAIXA_ID, $baixaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_BAIXA_ID, $baixaId, $comparison);
    }

    /**
     * Filter the query on the forma_pagamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFormaPagamentoId(1234); // WHERE forma_pagamento_id = 1234
     * $query->filterByFormaPagamentoId(array(12, 34)); // WHERE forma_pagamento_id IN (12, 34)
     * $query->filterByFormaPagamentoId(array('min' => 12)); // WHERE forma_pagamento_id > 12
     * </code>
     *
     * @see       filterByFormaPagamento()
     *
     * @param     mixed $formaPagamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByFormaPagamentoId($formaPagamentoId = null, $comparison = null)
    {
        if (is_array($formaPagamentoId)) {
            $useMinMax = false;
            if (isset($formaPagamentoId['min'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($formaPagamentoId['max'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId, $comparison);
    }

    /**
     * Filter the query on the usuario_baixa column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioBaixa(1234); // WHERE usuario_baixa = 1234
     * $query->filterByUsuarioBaixa(array(12, 34)); // WHERE usuario_baixa IN (12, 34)
     * $query->filterByUsuarioBaixa(array('min' => 12)); // WHERE usuario_baixa > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioBaixa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByUsuarioBaixa($usuarioBaixa = null, $comparison = null)
    {
        if (is_array($usuarioBaixa)) {
            $useMinMax = false;
            if (isset($usuarioBaixa['min'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_USUARIO_BAIXA, $usuarioBaixa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioBaixa['max'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_USUARIO_BAIXA, $usuarioBaixa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_USUARIO_BAIXA, $usuarioBaixa, $comparison);
    }

    /**
     * Filter the query on the data_baixa column
     *
     * Example usage:
     * <code>
     * $query->filterByDataBaixa('2011-03-14'); // WHERE data_baixa = '2011-03-14'
     * $query->filterByDataBaixa('now'); // WHERE data_baixa = '2011-03-14'
     * $query->filterByDataBaixa(array('max' => 'yesterday')); // WHERE data_baixa > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataBaixa The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByDataBaixa($dataBaixa = null, $comparison = null)
    {
        if (is_array($dataBaixa)) {
            $useMinMax = false;
            if (isset($dataBaixa['min'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_DATA_BAIXA, $dataBaixa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataBaixa['max'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_DATA_BAIXA, $dataBaixa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_DATA_BAIXA, $dataBaixa, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query on the numero_documento column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroDocumento('fooValue');   // WHERE numero_documento = 'fooValue'
     * $query->filterByNumeroDocumento('%fooValue%', Criteria::LIKE); // WHERE numero_documento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numeroDocumento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByNumeroDocumento($numeroDocumento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numeroDocumento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_NUMERO_DOCUMENTO, $numeroDocumento, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(BaixaMovimentoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaMovimentoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Baixa object
     *
     * @param \ImaTelecomBundle\Model\Baixa|ObjectCollection $baixa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByBaixa($baixa, $comparison = null)
    {
        if ($baixa instanceof \ImaTelecomBundle\Model\Baixa) {
            return $this
                ->addUsingAlias(BaixaMovimentoTableMap::COL_BAIXA_ID, $baixa->getIdbaixa(), $comparison);
        } elseif ($baixa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaMovimentoTableMap::COL_BAIXA_ID, $baixa->toKeyValue('PrimaryKey', 'Idbaixa'), $comparison);
        } else {
            throw new PropelException('filterByBaixa() only accepts arguments of type \ImaTelecomBundle\Model\Baixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Baixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function joinBaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Baixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Baixa');
        }

        return $this;
    }

    /**
     * Use the Baixa relation Baixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaQuery A secondary query class using the current class as primary query
     */
    public function useBaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Baixa', '\ImaTelecomBundle\Model\BaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\FormaPagamento object
     *
     * @param \ImaTelecomBundle\Model\FormaPagamento|ObjectCollection $formaPagamento The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByFormaPagamento($formaPagamento, $comparison = null)
    {
        if ($formaPagamento instanceof \ImaTelecomBundle\Model\FormaPagamento) {
            return $this
                ->addUsingAlias(BaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamento->getIdformaPagamento(), $comparison);
        } elseif ($formaPagamento instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamento->toKeyValue('PrimaryKey', 'IdformaPagamento'), $comparison);
        } else {
            throw new PropelException('filterByFormaPagamento() only accepts arguments of type \ImaTelecomBundle\Model\FormaPagamento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FormaPagamento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function joinFormaPagamento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FormaPagamento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FormaPagamento');
        }

        return $this;
    }

    /**
     * Use the FormaPagamento relation FormaPagamento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FormaPagamentoQuery A secondary query class using the current class as primary query
     */
    public function useFormaPagamentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFormaPagamento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FormaPagamento', '\ImaTelecomBundle\Model\FormaPagamentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(BaixaMovimentoTableMap::COL_USUARIO_BAIXA, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaMovimentoTableMap::COL_USUARIO_BAIXA, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBaixaMovimento $baixaMovimento Object to remove from the list of results
     *
     * @return $this|ChildBaixaMovimentoQuery The current query, for fluid interface
     */
    public function prune($baixaMovimento = null)
    {
        if ($baixaMovimento) {
            $this->addUsingAlias(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO, $baixaMovimento->getIdbaixaMovimento(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the baixa_movimento table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaMovimentoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BaixaMovimentoTableMap::clearInstancePool();
            BaixaMovimentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaMovimentoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BaixaMovimentoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BaixaMovimentoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BaixaMovimentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BaixaMovimentoQuery
