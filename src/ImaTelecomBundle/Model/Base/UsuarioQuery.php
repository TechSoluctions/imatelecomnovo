<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\UsuarioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'usuario' table.
 *
 *
 *
 * @method     ChildUsuarioQuery orderByIdusuario($order = Criteria::ASC) Order by the idusuario column
 * @method     ChildUsuarioQuery orderByPessoaId($order = Criteria::ASC) Order by the pessoa_id column
 * @method     ChildUsuarioQuery orderByLogin($order = Criteria::ASC) Order by the login column
 * @method     ChildUsuarioQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildUsuarioQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildUsuarioQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildUsuarioQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 *
 * @method     ChildUsuarioQuery groupByIdusuario() Group by the idusuario column
 * @method     ChildUsuarioQuery groupByPessoaId() Group by the pessoa_id column
 * @method     ChildUsuarioQuery groupByLogin() Group by the login column
 * @method     ChildUsuarioQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildUsuarioQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildUsuarioQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildUsuarioQuery groupByAtivo() Group by the ativo column
 *
 * @method     ChildUsuarioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUsuarioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUsuarioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUsuarioQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUsuarioQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUsuarioQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUsuarioQuery leftJoinPessoa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pessoa relation
 * @method     ChildUsuarioQuery rightJoinPessoa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pessoa relation
 * @method     ChildUsuarioQuery innerJoinPessoa($relationAlias = null) Adds a INNER JOIN clause to the query using the Pessoa relation
 *
 * @method     ChildUsuarioQuery joinWithPessoa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pessoa relation
 *
 * @method     ChildUsuarioQuery leftJoinWithPessoa() Adds a LEFT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildUsuarioQuery rightJoinWithPessoa() Adds a RIGHT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildUsuarioQuery innerJoinWithPessoa() Adds a INNER JOIN clause and with to the query using the Pessoa relation
 *
 * @method     ChildUsuarioQuery leftJoinBaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Baixa relation
 * @method     ChildUsuarioQuery rightJoinBaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Baixa relation
 * @method     ChildUsuarioQuery innerJoinBaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Baixa relation
 *
 * @method     ChildUsuarioQuery joinWithBaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Baixa relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBaixa() Adds a LEFT JOIN clause and with to the query using the Baixa relation
 * @method     ChildUsuarioQuery rightJoinWithBaixa() Adds a RIGHT JOIN clause and with to the query using the Baixa relation
 * @method     ChildUsuarioQuery innerJoinWithBaixa() Adds a INNER JOIN clause and with to the query using the Baixa relation
 *
 * @method     ChildUsuarioQuery leftJoinBaixaEstorno($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildUsuarioQuery rightJoinBaixaEstorno($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildUsuarioQuery innerJoinBaixaEstorno($relationAlias = null) Adds a INNER JOIN clause to the query using the BaixaEstorno relation
 *
 * @method     ChildUsuarioQuery joinWithBaixaEstorno($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBaixaEstorno() Adds a LEFT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildUsuarioQuery rightJoinWithBaixaEstorno() Adds a RIGHT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildUsuarioQuery innerJoinWithBaixaEstorno() Adds a INNER JOIN clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildUsuarioQuery leftJoinBaixaMovimento($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaixaMovimento relation
 * @method     ChildUsuarioQuery rightJoinBaixaMovimento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaixaMovimento relation
 * @method     ChildUsuarioQuery innerJoinBaixaMovimento($relationAlias = null) Adds a INNER JOIN clause to the query using the BaixaMovimento relation
 *
 * @method     ChildUsuarioQuery joinWithBaixaMovimento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaixaMovimento relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBaixaMovimento() Adds a LEFT JOIN clause and with to the query using the BaixaMovimento relation
 * @method     ChildUsuarioQuery rightJoinWithBaixaMovimento() Adds a RIGHT JOIN clause and with to the query using the BaixaMovimento relation
 * @method     ChildUsuarioQuery innerJoinWithBaixaMovimento() Adds a INNER JOIN clause and with to the query using the BaixaMovimento relation
 *
 * @method     ChildUsuarioQuery leftJoinBanco($relationAlias = null) Adds a LEFT JOIN clause to the query using the Banco relation
 * @method     ChildUsuarioQuery rightJoinBanco($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Banco relation
 * @method     ChildUsuarioQuery innerJoinBanco($relationAlias = null) Adds a INNER JOIN clause to the query using the Banco relation
 *
 * @method     ChildUsuarioQuery joinWithBanco($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Banco relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBanco() Adds a LEFT JOIN clause and with to the query using the Banco relation
 * @method     ChildUsuarioQuery rightJoinWithBanco() Adds a RIGHT JOIN clause and with to the query using the Banco relation
 * @method     ChildUsuarioQuery innerJoinWithBanco() Adds a INNER JOIN clause and with to the query using the Banco relation
 *
 * @method     ChildUsuarioQuery leftJoinBancoAgencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the BancoAgencia relation
 * @method     ChildUsuarioQuery rightJoinBancoAgencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BancoAgencia relation
 * @method     ChildUsuarioQuery innerJoinBancoAgencia($relationAlias = null) Adds a INNER JOIN clause to the query using the BancoAgencia relation
 *
 * @method     ChildUsuarioQuery joinWithBancoAgencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BancoAgencia relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBancoAgencia() Adds a LEFT JOIN clause and with to the query using the BancoAgencia relation
 * @method     ChildUsuarioQuery rightJoinWithBancoAgencia() Adds a RIGHT JOIN clause and with to the query using the BancoAgencia relation
 * @method     ChildUsuarioQuery innerJoinWithBancoAgencia() Adds a INNER JOIN clause and with to the query using the BancoAgencia relation
 *
 * @method     ChildUsuarioQuery leftJoinBancoAgenciaConta($relationAlias = null) Adds a LEFT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildUsuarioQuery rightJoinBancoAgenciaConta($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildUsuarioQuery innerJoinBancoAgenciaConta($relationAlias = null) Adds a INNER JOIN clause to the query using the BancoAgenciaConta relation
 *
 * @method     ChildUsuarioQuery joinWithBancoAgenciaConta($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBancoAgenciaConta() Adds a LEFT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildUsuarioQuery rightJoinWithBancoAgenciaConta() Adds a RIGHT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildUsuarioQuery innerJoinWithBancoAgenciaConta() Adds a INNER JOIN clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildUsuarioQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildUsuarioQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildUsuarioQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildUsuarioQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildUsuarioQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildUsuarioQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildUsuarioQuery leftJoinBoletoBaixaHistorico($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildUsuarioQuery rightJoinBoletoBaixaHistorico($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildUsuarioQuery innerJoinBoletoBaixaHistorico($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildUsuarioQuery joinWithBoletoBaixaHistorico($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBoletoBaixaHistorico() Adds a LEFT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildUsuarioQuery rightJoinWithBoletoBaixaHistorico() Adds a RIGHT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildUsuarioQuery innerJoinWithBoletoBaixaHistorico() Adds a INNER JOIN clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildUsuarioQuery leftJoinBoletoItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoItem relation
 * @method     ChildUsuarioQuery rightJoinBoletoItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoItem relation
 * @method     ChildUsuarioQuery innerJoinBoletoItem($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoItem relation
 *
 * @method     ChildUsuarioQuery joinWithBoletoItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoItem relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBoletoItem() Adds a LEFT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildUsuarioQuery rightJoinWithBoletoItem() Adds a RIGHT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildUsuarioQuery innerJoinWithBoletoItem() Adds a INNER JOIN clause and with to the query using the BoletoItem relation
 *
 * @method     ChildUsuarioQuery leftJoinCaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Caixa relation
 * @method     ChildUsuarioQuery rightJoinCaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Caixa relation
 * @method     ChildUsuarioQuery innerJoinCaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Caixa relation
 *
 * @method     ChildUsuarioQuery joinWithCaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Caixa relation
 *
 * @method     ChildUsuarioQuery leftJoinWithCaixa() Adds a LEFT JOIN clause and with to the query using the Caixa relation
 * @method     ChildUsuarioQuery rightJoinWithCaixa() Adds a RIGHT JOIN clause and with to the query using the Caixa relation
 * @method     ChildUsuarioQuery innerJoinWithCaixa() Adds a INNER JOIN clause and with to the query using the Caixa relation
 *
 * @method     ChildUsuarioQuery leftJoinCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cliente relation
 * @method     ChildUsuarioQuery rightJoinCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cliente relation
 * @method     ChildUsuarioQuery innerJoinCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the Cliente relation
 *
 * @method     ChildUsuarioQuery joinWithCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cliente relation
 *
 * @method     ChildUsuarioQuery leftJoinWithCliente() Adds a LEFT JOIN clause and with to the query using the Cliente relation
 * @method     ChildUsuarioQuery rightJoinWithCliente() Adds a RIGHT JOIN clause and with to the query using the Cliente relation
 * @method     ChildUsuarioQuery innerJoinWithCliente() Adds a INNER JOIN clause and with to the query using the Cliente relation
 *
 * @method     ChildUsuarioQuery leftJoinClienteEstoqueItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the ClienteEstoqueItem relation
 * @method     ChildUsuarioQuery rightJoinClienteEstoqueItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ClienteEstoqueItem relation
 * @method     ChildUsuarioQuery innerJoinClienteEstoqueItem($relationAlias = null) Adds a INNER JOIN clause to the query using the ClienteEstoqueItem relation
 *
 * @method     ChildUsuarioQuery joinWithClienteEstoqueItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ClienteEstoqueItem relation
 *
 * @method     ChildUsuarioQuery leftJoinWithClienteEstoqueItem() Adds a LEFT JOIN clause and with to the query using the ClienteEstoqueItem relation
 * @method     ChildUsuarioQuery rightJoinWithClienteEstoqueItem() Adds a RIGHT JOIN clause and with to the query using the ClienteEstoqueItem relation
 * @method     ChildUsuarioQuery innerJoinWithClienteEstoqueItem() Adds a INNER JOIN clause and with to the query using the ClienteEstoqueItem relation
 *
 * @method     ChildUsuarioQuery leftJoinContaCaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContaCaixa relation
 * @method     ChildUsuarioQuery rightJoinContaCaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContaCaixa relation
 * @method     ChildUsuarioQuery innerJoinContaCaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the ContaCaixa relation
 *
 * @method     ChildUsuarioQuery joinWithContaCaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContaCaixa relation
 *
 * @method     ChildUsuarioQuery leftJoinWithContaCaixa() Adds a LEFT JOIN clause and with to the query using the ContaCaixa relation
 * @method     ChildUsuarioQuery rightJoinWithContaCaixa() Adds a RIGHT JOIN clause and with to the query using the ContaCaixa relation
 * @method     ChildUsuarioQuery innerJoinWithContaCaixa() Adds a INNER JOIN clause and with to the query using the ContaCaixa relation
 *
 * @method     ChildUsuarioQuery leftJoinContasPagar($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagar relation
 * @method     ChildUsuarioQuery rightJoinContasPagar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagar relation
 * @method     ChildUsuarioQuery innerJoinContasPagar($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagar relation
 *
 * @method     ChildUsuarioQuery joinWithContasPagar($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagar relation
 *
 * @method     ChildUsuarioQuery leftJoinWithContasPagar() Adds a LEFT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildUsuarioQuery rightJoinWithContasPagar() Adds a RIGHT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildUsuarioQuery innerJoinWithContasPagar() Adds a INNER JOIN clause and with to the query using the ContasPagar relation
 *
 * @method     ChildUsuarioQuery leftJoinContasPagarTributos($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagarTributos relation
 * @method     ChildUsuarioQuery rightJoinContasPagarTributos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagarTributos relation
 * @method     ChildUsuarioQuery innerJoinContasPagarTributos($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagarTributos relation
 *
 * @method     ChildUsuarioQuery joinWithContasPagarTributos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagarTributos relation
 *
 * @method     ChildUsuarioQuery leftJoinWithContasPagarTributos() Adds a LEFT JOIN clause and with to the query using the ContasPagarTributos relation
 * @method     ChildUsuarioQuery rightJoinWithContasPagarTributos() Adds a RIGHT JOIN clause and with to the query using the ContasPagarTributos relation
 * @method     ChildUsuarioQuery innerJoinWithContasPagarTributos() Adds a INNER JOIN clause and with to the query using the ContasPagarTributos relation
 *
 * @method     ChildUsuarioQuery leftJoinConvenio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Convenio relation
 * @method     ChildUsuarioQuery rightJoinConvenio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Convenio relation
 * @method     ChildUsuarioQuery innerJoinConvenio($relationAlias = null) Adds a INNER JOIN clause to the query using the Convenio relation
 *
 * @method     ChildUsuarioQuery joinWithConvenio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Convenio relation
 *
 * @method     ChildUsuarioQuery leftJoinWithConvenio() Adds a LEFT JOIN clause and with to the query using the Convenio relation
 * @method     ChildUsuarioQuery rightJoinWithConvenio() Adds a RIGHT JOIN clause and with to the query using the Convenio relation
 * @method     ChildUsuarioQuery innerJoinWithConvenio() Adds a INNER JOIN clause and with to the query using the Convenio relation
 *
 * @method     ChildUsuarioQuery leftJoinCronTask($relationAlias = null) Adds a LEFT JOIN clause to the query using the CronTask relation
 * @method     ChildUsuarioQuery rightJoinCronTask($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CronTask relation
 * @method     ChildUsuarioQuery innerJoinCronTask($relationAlias = null) Adds a INNER JOIN clause to the query using the CronTask relation
 *
 * @method     ChildUsuarioQuery joinWithCronTask($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CronTask relation
 *
 * @method     ChildUsuarioQuery leftJoinWithCronTask() Adds a LEFT JOIN clause and with to the query using the CronTask relation
 * @method     ChildUsuarioQuery rightJoinWithCronTask() Adds a RIGHT JOIN clause and with to the query using the CronTask relation
 * @method     ChildUsuarioQuery innerJoinWithCronTask() Adds a INNER JOIN clause and with to the query using the CronTask relation
 *
 * @method     ChildUsuarioQuery leftJoinCronTaskGrupo($relationAlias = null) Adds a LEFT JOIN clause to the query using the CronTaskGrupo relation
 * @method     ChildUsuarioQuery rightJoinCronTaskGrupo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CronTaskGrupo relation
 * @method     ChildUsuarioQuery innerJoinCronTaskGrupo($relationAlias = null) Adds a INNER JOIN clause to the query using the CronTaskGrupo relation
 *
 * @method     ChildUsuarioQuery joinWithCronTaskGrupo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CronTaskGrupo relation
 *
 * @method     ChildUsuarioQuery leftJoinWithCronTaskGrupo() Adds a LEFT JOIN clause and with to the query using the CronTaskGrupo relation
 * @method     ChildUsuarioQuery rightJoinWithCronTaskGrupo() Adds a RIGHT JOIN clause and with to the query using the CronTaskGrupo relation
 * @method     ChildUsuarioQuery innerJoinWithCronTaskGrupo() Adds a INNER JOIN clause and with to the query using the CronTaskGrupo relation
 *
 * @method     ChildUsuarioQuery leftJoinEnderecoAgencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoAgencia relation
 * @method     ChildUsuarioQuery rightJoinEnderecoAgencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoAgencia relation
 * @method     ChildUsuarioQuery innerJoinEnderecoAgencia($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoAgencia relation
 *
 * @method     ChildUsuarioQuery joinWithEnderecoAgencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoAgencia relation
 *
 * @method     ChildUsuarioQuery leftJoinWithEnderecoAgencia() Adds a LEFT JOIN clause and with to the query using the EnderecoAgencia relation
 * @method     ChildUsuarioQuery rightJoinWithEnderecoAgencia() Adds a RIGHT JOIN clause and with to the query using the EnderecoAgencia relation
 * @method     ChildUsuarioQuery innerJoinWithEnderecoAgencia() Adds a INNER JOIN clause and with to the query using the EnderecoAgencia relation
 *
 * @method     ChildUsuarioQuery leftJoinEnderecoCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoCliente relation
 * @method     ChildUsuarioQuery rightJoinEnderecoCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoCliente relation
 * @method     ChildUsuarioQuery innerJoinEnderecoCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoCliente relation
 *
 * @method     ChildUsuarioQuery joinWithEnderecoCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoCliente relation
 *
 * @method     ChildUsuarioQuery leftJoinWithEnderecoCliente() Adds a LEFT JOIN clause and with to the query using the EnderecoCliente relation
 * @method     ChildUsuarioQuery rightJoinWithEnderecoCliente() Adds a RIGHT JOIN clause and with to the query using the EnderecoCliente relation
 * @method     ChildUsuarioQuery innerJoinWithEnderecoCliente() Adds a INNER JOIN clause and with to the query using the EnderecoCliente relation
 *
 * @method     ChildUsuarioQuery leftJoinEnderecoServCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoServCliente relation
 * @method     ChildUsuarioQuery rightJoinEnderecoServCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoServCliente relation
 * @method     ChildUsuarioQuery innerJoinEnderecoServCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoServCliente relation
 *
 * @method     ChildUsuarioQuery joinWithEnderecoServCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoServCliente relation
 *
 * @method     ChildUsuarioQuery leftJoinWithEnderecoServCliente() Adds a LEFT JOIN clause and with to the query using the EnderecoServCliente relation
 * @method     ChildUsuarioQuery rightJoinWithEnderecoServCliente() Adds a RIGHT JOIN clause and with to the query using the EnderecoServCliente relation
 * @method     ChildUsuarioQuery innerJoinWithEnderecoServCliente() Adds a INNER JOIN clause and with to the query using the EnderecoServCliente relation
 *
 * @method     ChildUsuarioQuery leftJoinEstoque($relationAlias = null) Adds a LEFT JOIN clause to the query using the Estoque relation
 * @method     ChildUsuarioQuery rightJoinEstoque($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Estoque relation
 * @method     ChildUsuarioQuery innerJoinEstoque($relationAlias = null) Adds a INNER JOIN clause to the query using the Estoque relation
 *
 * @method     ChildUsuarioQuery joinWithEstoque($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Estoque relation
 *
 * @method     ChildUsuarioQuery leftJoinWithEstoque() Adds a LEFT JOIN clause and with to the query using the Estoque relation
 * @method     ChildUsuarioQuery rightJoinWithEstoque() Adds a RIGHT JOIN clause and with to the query using the Estoque relation
 * @method     ChildUsuarioQuery innerJoinWithEstoque() Adds a INNER JOIN clause and with to the query using the Estoque relation
 *
 * @method     ChildUsuarioQuery leftJoinEstoqueLancamento($relationAlias = null) Adds a LEFT JOIN clause to the query using the EstoqueLancamento relation
 * @method     ChildUsuarioQuery rightJoinEstoqueLancamento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EstoqueLancamento relation
 * @method     ChildUsuarioQuery innerJoinEstoqueLancamento($relationAlias = null) Adds a INNER JOIN clause to the query using the EstoqueLancamento relation
 *
 * @method     ChildUsuarioQuery joinWithEstoqueLancamento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EstoqueLancamento relation
 *
 * @method     ChildUsuarioQuery leftJoinWithEstoqueLancamento() Adds a LEFT JOIN clause and with to the query using the EstoqueLancamento relation
 * @method     ChildUsuarioQuery rightJoinWithEstoqueLancamento() Adds a RIGHT JOIN clause and with to the query using the EstoqueLancamento relation
 * @method     ChildUsuarioQuery innerJoinWithEstoqueLancamento() Adds a INNER JOIN clause and with to the query using the EstoqueLancamento relation
 *
 * @method     ChildUsuarioQuery leftJoinEstoqueLancamentoItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the EstoqueLancamentoItem relation
 * @method     ChildUsuarioQuery rightJoinEstoqueLancamentoItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EstoqueLancamentoItem relation
 * @method     ChildUsuarioQuery innerJoinEstoqueLancamentoItem($relationAlias = null) Adds a INNER JOIN clause to the query using the EstoqueLancamentoItem relation
 *
 * @method     ChildUsuarioQuery joinWithEstoqueLancamentoItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EstoqueLancamentoItem relation
 *
 * @method     ChildUsuarioQuery leftJoinWithEstoqueLancamentoItem() Adds a LEFT JOIN clause and with to the query using the EstoqueLancamentoItem relation
 * @method     ChildUsuarioQuery rightJoinWithEstoqueLancamentoItem() Adds a RIGHT JOIN clause and with to the query using the EstoqueLancamentoItem relation
 * @method     ChildUsuarioQuery innerJoinWithEstoqueLancamentoItem() Adds a INNER JOIN clause and with to the query using the EstoqueLancamentoItem relation
 *
 * @method     ChildUsuarioQuery leftJoinFormaPagamento($relationAlias = null) Adds a LEFT JOIN clause to the query using the FormaPagamento relation
 * @method     ChildUsuarioQuery rightJoinFormaPagamento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FormaPagamento relation
 * @method     ChildUsuarioQuery innerJoinFormaPagamento($relationAlias = null) Adds a INNER JOIN clause to the query using the FormaPagamento relation
 *
 * @method     ChildUsuarioQuery joinWithFormaPagamento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FormaPagamento relation
 *
 * @method     ChildUsuarioQuery leftJoinWithFormaPagamento() Adds a LEFT JOIN clause and with to the query using the FormaPagamento relation
 * @method     ChildUsuarioQuery rightJoinWithFormaPagamento() Adds a RIGHT JOIN clause and with to the query using the FormaPagamento relation
 * @method     ChildUsuarioQuery innerJoinWithFormaPagamento() Adds a INNER JOIN clause and with to the query using the FormaPagamento relation
 *
 * @method     ChildUsuarioQuery leftJoinFornecedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fornecedor relation
 * @method     ChildUsuarioQuery rightJoinFornecedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fornecedor relation
 * @method     ChildUsuarioQuery innerJoinFornecedor($relationAlias = null) Adds a INNER JOIN clause to the query using the Fornecedor relation
 *
 * @method     ChildUsuarioQuery joinWithFornecedor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fornecedor relation
 *
 * @method     ChildUsuarioQuery leftJoinWithFornecedor() Adds a LEFT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildUsuarioQuery rightJoinWithFornecedor() Adds a RIGHT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildUsuarioQuery innerJoinWithFornecedor() Adds a INNER JOIN clause and with to the query using the Fornecedor relation
 *
 * @method     ChildUsuarioQuery leftJoinItemComprado($relationAlias = null) Adds a LEFT JOIN clause to the query using the ItemComprado relation
 * @method     ChildUsuarioQuery rightJoinItemComprado($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ItemComprado relation
 * @method     ChildUsuarioQuery innerJoinItemComprado($relationAlias = null) Adds a INNER JOIN clause to the query using the ItemComprado relation
 *
 * @method     ChildUsuarioQuery joinWithItemComprado($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ItemComprado relation
 *
 * @method     ChildUsuarioQuery leftJoinWithItemComprado() Adds a LEFT JOIN clause and with to the query using the ItemComprado relation
 * @method     ChildUsuarioQuery rightJoinWithItemComprado() Adds a RIGHT JOIN clause and with to the query using the ItemComprado relation
 * @method     ChildUsuarioQuery innerJoinWithItemComprado() Adds a INNER JOIN clause and with to the query using the ItemComprado relation
 *
 * @method     ChildUsuarioQuery leftJoinItemDeCompra($relationAlias = null) Adds a LEFT JOIN clause to the query using the ItemDeCompra relation
 * @method     ChildUsuarioQuery rightJoinItemDeCompra($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ItemDeCompra relation
 * @method     ChildUsuarioQuery innerJoinItemDeCompra($relationAlias = null) Adds a INNER JOIN clause to the query using the ItemDeCompra relation
 *
 * @method     ChildUsuarioQuery joinWithItemDeCompra($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ItemDeCompra relation
 *
 * @method     ChildUsuarioQuery leftJoinWithItemDeCompra() Adds a LEFT JOIN clause and with to the query using the ItemDeCompra relation
 * @method     ChildUsuarioQuery rightJoinWithItemDeCompra() Adds a RIGHT JOIN clause and with to the query using the ItemDeCompra relation
 * @method     ChildUsuarioQuery innerJoinWithItemDeCompra() Adds a INNER JOIN clause and with to the query using the ItemDeCompra relation
 *
 * @method     ChildUsuarioQuery leftJoinServicoCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildUsuarioQuery rightJoinServicoCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildUsuarioQuery innerJoinServicoCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicoCliente relation
 *
 * @method     ChildUsuarioQuery joinWithServicoCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicoCliente relation
 *
 * @method     ChildUsuarioQuery leftJoinWithServicoCliente() Adds a LEFT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildUsuarioQuery rightJoinWithServicoCliente() Adds a RIGHT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildUsuarioQuery innerJoinWithServicoCliente() Adds a INNER JOIN clause and with to the query using the ServicoCliente relation
 *
 * @method     ChildUsuarioQuery leftJoinServicoPrestado($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicoPrestado relation
 * @method     ChildUsuarioQuery rightJoinServicoPrestado($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicoPrestado relation
 * @method     ChildUsuarioQuery innerJoinServicoPrestado($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicoPrestado relation
 *
 * @method     ChildUsuarioQuery joinWithServicoPrestado($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicoPrestado relation
 *
 * @method     ChildUsuarioQuery leftJoinWithServicoPrestado() Adds a LEFT JOIN clause and with to the query using the ServicoPrestado relation
 * @method     ChildUsuarioQuery rightJoinWithServicoPrestado() Adds a RIGHT JOIN clause and with to the query using the ServicoPrestado relation
 * @method     ChildUsuarioQuery innerJoinWithServicoPrestado() Adds a INNER JOIN clause and with to the query using the ServicoPrestado relation
 *
 * @method     ChildUsuarioQuery leftJoinSici($relationAlias = null) Adds a LEFT JOIN clause to the query using the Sici relation
 * @method     ChildUsuarioQuery rightJoinSici($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Sici relation
 * @method     ChildUsuarioQuery innerJoinSici($relationAlias = null) Adds a INNER JOIN clause to the query using the Sici relation
 *
 * @method     ChildUsuarioQuery joinWithSici($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Sici relation
 *
 * @method     ChildUsuarioQuery leftJoinWithSici() Adds a LEFT JOIN clause and with to the query using the Sici relation
 * @method     ChildUsuarioQuery rightJoinWithSici() Adds a RIGHT JOIN clause and with to the query using the Sici relation
 * @method     ChildUsuarioQuery innerJoinWithSici() Adds a INNER JOIN clause and with to the query using the Sici relation
 *
 * @method     ChildUsuarioQuery leftJoinSysPerfil($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysPerfil relation
 * @method     ChildUsuarioQuery rightJoinSysPerfil($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysPerfil relation
 * @method     ChildUsuarioQuery innerJoinSysPerfil($relationAlias = null) Adds a INNER JOIN clause to the query using the SysPerfil relation
 *
 * @method     ChildUsuarioQuery joinWithSysPerfil($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SysPerfil relation
 *
 * @method     ChildUsuarioQuery leftJoinWithSysPerfil() Adds a LEFT JOIN clause and with to the query using the SysPerfil relation
 * @method     ChildUsuarioQuery rightJoinWithSysPerfil() Adds a RIGHT JOIN clause and with to the query using the SysPerfil relation
 * @method     ChildUsuarioQuery innerJoinWithSysPerfil() Adds a INNER JOIN clause and with to the query using the SysPerfil relation
 *
 * @method     ChildUsuarioQuery leftJoinSysProcessos($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysProcessos relation
 * @method     ChildUsuarioQuery rightJoinSysProcessos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysProcessos relation
 * @method     ChildUsuarioQuery innerJoinSysProcessos($relationAlias = null) Adds a INNER JOIN clause to the query using the SysProcessos relation
 *
 * @method     ChildUsuarioQuery joinWithSysProcessos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SysProcessos relation
 *
 * @method     ChildUsuarioQuery leftJoinWithSysProcessos() Adds a LEFT JOIN clause and with to the query using the SysProcessos relation
 * @method     ChildUsuarioQuery rightJoinWithSysProcessos() Adds a RIGHT JOIN clause and with to the query using the SysProcessos relation
 * @method     ChildUsuarioQuery innerJoinWithSysProcessos() Adds a INNER JOIN clause and with to the query using the SysProcessos relation
 *
 * @method     ChildUsuarioQuery leftJoinSysUsuarioPerfil($relationAlias = null) Adds a LEFT JOIN clause to the query using the SysUsuarioPerfil relation
 * @method     ChildUsuarioQuery rightJoinSysUsuarioPerfil($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SysUsuarioPerfil relation
 * @method     ChildUsuarioQuery innerJoinSysUsuarioPerfil($relationAlias = null) Adds a INNER JOIN clause to the query using the SysUsuarioPerfil relation
 *
 * @method     ChildUsuarioQuery joinWithSysUsuarioPerfil($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SysUsuarioPerfil relation
 *
 * @method     ChildUsuarioQuery leftJoinWithSysUsuarioPerfil() Adds a LEFT JOIN clause and with to the query using the SysUsuarioPerfil relation
 * @method     ChildUsuarioQuery rightJoinWithSysUsuarioPerfil() Adds a RIGHT JOIN clause and with to the query using the SysUsuarioPerfil relation
 * @method     ChildUsuarioQuery innerJoinWithSysUsuarioPerfil() Adds a INNER JOIN clause and with to the query using the SysUsuarioPerfil relation
 *
 * @method     ChildUsuarioQuery leftJoinTelefone($relationAlias = null) Adds a LEFT JOIN clause to the query using the Telefone relation
 * @method     ChildUsuarioQuery rightJoinTelefone($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Telefone relation
 * @method     ChildUsuarioQuery innerJoinTelefone($relationAlias = null) Adds a INNER JOIN clause to the query using the Telefone relation
 *
 * @method     ChildUsuarioQuery joinWithTelefone($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Telefone relation
 *
 * @method     ChildUsuarioQuery leftJoinWithTelefone() Adds a LEFT JOIN clause and with to the query using the Telefone relation
 * @method     ChildUsuarioQuery rightJoinWithTelefone() Adds a RIGHT JOIN clause and with to the query using the Telefone relation
 * @method     ChildUsuarioQuery innerJoinWithTelefone() Adds a INNER JOIN clause and with to the query using the Telefone relation
 *
 * @method     ChildUsuarioQuery leftJoinTributo($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tributo relation
 * @method     ChildUsuarioQuery rightJoinTributo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tributo relation
 * @method     ChildUsuarioQuery innerJoinTributo($relationAlias = null) Adds a INNER JOIN clause to the query using the Tributo relation
 *
 * @method     ChildUsuarioQuery joinWithTributo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Tributo relation
 *
 * @method     ChildUsuarioQuery leftJoinWithTributo() Adds a LEFT JOIN clause and with to the query using the Tributo relation
 * @method     ChildUsuarioQuery rightJoinWithTributo() Adds a RIGHT JOIN clause and with to the query using the Tributo relation
 * @method     ChildUsuarioQuery innerJoinWithTributo() Adds a INNER JOIN clause and with to the query using the Tributo relation
 *
 * @method     \ImaTelecomBundle\Model\PessoaQuery|\ImaTelecomBundle\Model\BaixaQuery|\ImaTelecomBundle\Model\BaixaEstornoQuery|\ImaTelecomBundle\Model\BaixaMovimentoQuery|\ImaTelecomBundle\Model\BancoQuery|\ImaTelecomBundle\Model\BancoAgenciaQuery|\ImaTelecomBundle\Model\BancoAgenciaContaQuery|\ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery|\ImaTelecomBundle\Model\BoletoItemQuery|\ImaTelecomBundle\Model\CaixaQuery|\ImaTelecomBundle\Model\ClienteQuery|\ImaTelecomBundle\Model\ClienteEstoqueItemQuery|\ImaTelecomBundle\Model\ContaCaixaQuery|\ImaTelecomBundle\Model\ContasPagarQuery|\ImaTelecomBundle\Model\ContasPagarTributosQuery|\ImaTelecomBundle\Model\ConvenioQuery|\ImaTelecomBundle\Model\CronTaskQuery|\ImaTelecomBundle\Model\CronTaskGrupoQuery|\ImaTelecomBundle\Model\EnderecoAgenciaQuery|\ImaTelecomBundle\Model\EnderecoClienteQuery|\ImaTelecomBundle\Model\EnderecoServClienteQuery|\ImaTelecomBundle\Model\EstoqueQuery|\ImaTelecomBundle\Model\EstoqueLancamentoQuery|\ImaTelecomBundle\Model\EstoqueLancamentoItemQuery|\ImaTelecomBundle\Model\FormaPagamentoQuery|\ImaTelecomBundle\Model\FornecedorQuery|\ImaTelecomBundle\Model\ItemCompradoQuery|\ImaTelecomBundle\Model\ItemDeCompraQuery|\ImaTelecomBundle\Model\ServicoClienteQuery|\ImaTelecomBundle\Model\ServicoPrestadoQuery|\ImaTelecomBundle\Model\SiciQuery|\ImaTelecomBundle\Model\SysPerfilQuery|\ImaTelecomBundle\Model\SysProcessosQuery|\ImaTelecomBundle\Model\SysUsuarioPerfilQuery|\ImaTelecomBundle\Model\TelefoneQuery|\ImaTelecomBundle\Model\TributoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUsuario findOne(ConnectionInterface $con = null) Return the first ChildUsuario matching the query
 * @method     ChildUsuario findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUsuario matching the query, or a new ChildUsuario object populated from the query conditions when no match is found
 *
 * @method     ChildUsuario findOneByIdusuario(int $idusuario) Return the first ChildUsuario filtered by the idusuario column
 * @method     ChildUsuario findOneByPessoaId(int $pessoa_id) Return the first ChildUsuario filtered by the pessoa_id column
 * @method     ChildUsuario findOneByLogin(string $login) Return the first ChildUsuario filtered by the login column
 * @method     ChildUsuario findOneByDataCadastro(string $data_cadastro) Return the first ChildUsuario filtered by the data_cadastro column
 * @method     ChildUsuario findOneByDataAlterado(string $data_alterado) Return the first ChildUsuario filtered by the data_alterado column
 * @method     ChildUsuario findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildUsuario filtered by the usuario_alterado column
 * @method     ChildUsuario findOneByAtivo(boolean $ativo) Return the first ChildUsuario filtered by the ativo column *

 * @method     ChildUsuario requirePk($key, ConnectionInterface $con = null) Return the ChildUsuario by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOne(ConnectionInterface $con = null) Return the first ChildUsuario matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsuario requireOneByIdusuario(int $idusuario) Return the first ChildUsuario filtered by the idusuario column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByPessoaId(int $pessoa_id) Return the first ChildUsuario filtered by the pessoa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByLogin(string $login) Return the first ChildUsuario filtered by the login column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByDataCadastro(string $data_cadastro) Return the first ChildUsuario filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByDataAlterado(string $data_alterado) Return the first ChildUsuario filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildUsuario filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByAtivo(boolean $ativo) Return the first ChildUsuario filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsuario[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUsuario objects based on current ModelCriteria
 * @method     ChildUsuario[]|ObjectCollection findByIdusuario(int $idusuario) Return ChildUsuario objects filtered by the idusuario column
 * @method     ChildUsuario[]|ObjectCollection findByPessoaId(int $pessoa_id) Return ChildUsuario objects filtered by the pessoa_id column
 * @method     ChildUsuario[]|ObjectCollection findByLogin(string $login) Return ChildUsuario objects filtered by the login column
 * @method     ChildUsuario[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildUsuario objects filtered by the data_cadastro column
 * @method     ChildUsuario[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildUsuario objects filtered by the data_alterado column
 * @method     ChildUsuario[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildUsuario objects filtered by the usuario_alterado column
 * @method     ChildUsuario[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildUsuario objects filtered by the ativo column
 * @method     ChildUsuario[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UsuarioQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\UsuarioQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Usuario', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUsuarioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUsuarioQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUsuarioQuery) {
            return $criteria;
        }
        $query = new ChildUsuarioQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUsuario|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsuarioTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UsuarioTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsuario A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idusuario, pessoa_id, login, data_cadastro, data_alterado, usuario_alterado, ativo FROM usuario WHERE idusuario = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUsuario $obj */
            $obj = new ChildUsuario();
            $obj->hydrate($row);
            UsuarioTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUsuario|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idusuario column
     *
     * Example usage:
     * <code>
     * $query->filterByIdusuario(1234); // WHERE idusuario = 1234
     * $query->filterByIdusuario(array(12, 34)); // WHERE idusuario IN (12, 34)
     * $query->filterByIdusuario(array('min' => 12)); // WHERE idusuario > 12
     * </code>
     *
     * @param     mixed $idusuario The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByIdusuario($idusuario = null, $comparison = null)
    {
        if (is_array($idusuario)) {
            $useMinMax = false;
            if (isset($idusuario['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $idusuario['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idusuario['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $idusuario['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $idusuario, $comparison);
    }

    /**
     * Filter the query on the pessoa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPessoaId(1234); // WHERE pessoa_id = 1234
     * $query->filterByPessoaId(array(12, 34)); // WHERE pessoa_id IN (12, 34)
     * $query->filterByPessoaId(array('min' => 12)); // WHERE pessoa_id > 12
     * </code>
     *
     * @see       filterByPessoa()
     *
     * @param     mixed $pessoaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByPessoaId($pessoaId = null, $comparison = null)
    {
        if (is_array($pessoaId)) {
            $useMinMax = false;
            if (isset($pessoaId['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_PESSOA_ID, $pessoaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pessoaId['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_PESSOA_ID, $pessoaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_PESSOA_ID, $pessoaId, $comparison);
    }

    /**
     * Filter the query on the login column
     *
     * Example usage:
     * <code>
     * $query->filterByLogin('fooValue');   // WHERE login = 'fooValue'
     * $query->filterByLogin('%fooValue%', Criteria::LIKE); // WHERE login LIKE '%fooValue%'
     * </code>
     *
     * @param     string $login The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByLogin($login = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($login)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_LOGIN, $login, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Pessoa object
     *
     * @param \ImaTelecomBundle\Model\Pessoa|ObjectCollection $pessoa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByPessoa($pessoa, $comparison = null)
    {
        if ($pessoa instanceof \ImaTelecomBundle\Model\Pessoa) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_PESSOA_ID, $pessoa->getId(), $comparison);
        } elseif ($pessoa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UsuarioTableMap::COL_PESSOA_ID, $pessoa->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPessoa() only accepts arguments of type \ImaTelecomBundle\Model\Pessoa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pessoa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinPessoa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pessoa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pessoa');
        }

        return $this;
    }

    /**
     * Use the Pessoa relation Pessoa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\PessoaQuery A secondary query class using the current class as primary query
     */
    public function usePessoaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPessoa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pessoa', '\ImaTelecomBundle\Model\PessoaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Baixa object
     *
     * @param \ImaTelecomBundle\Model\Baixa|ObjectCollection $baixa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBaixa($baixa, $comparison = null)
    {
        if ($baixa instanceof \ImaTelecomBundle\Model\Baixa) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $baixa->getUsuarioBaixa(), $comparison);
        } elseif ($baixa instanceof ObjectCollection) {
            return $this
                ->useBaixaQuery()
                ->filterByPrimaryKeys($baixa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixa() only accepts arguments of type \ImaTelecomBundle\Model\Baixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Baixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Baixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Baixa');
        }

        return $this;
    }

    /**
     * Use the Baixa relation Baixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaQuery A secondary query class using the current class as primary query
     */
    public function useBaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Baixa', '\ImaTelecomBundle\Model\BaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BaixaEstorno object
     *
     * @param \ImaTelecomBundle\Model\BaixaEstorno|ObjectCollection $baixaEstorno the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBaixaEstorno($baixaEstorno, $comparison = null)
    {
        if ($baixaEstorno instanceof \ImaTelecomBundle\Model\BaixaEstorno) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $baixaEstorno->getUsuarioEstorno(), $comparison);
        } elseif ($baixaEstorno instanceof ObjectCollection) {
            return $this
                ->useBaixaEstornoQuery()
                ->filterByPrimaryKeys($baixaEstorno->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixaEstorno() only accepts arguments of type \ImaTelecomBundle\Model\BaixaEstorno or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaixaEstorno relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBaixaEstorno($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaixaEstorno');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaixaEstorno');
        }

        return $this;
    }

    /**
     * Use the BaixaEstorno relation BaixaEstorno object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaEstornoQuery A secondary query class using the current class as primary query
     */
    public function useBaixaEstornoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixaEstorno($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaixaEstorno', '\ImaTelecomBundle\Model\BaixaEstornoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BaixaMovimento object
     *
     * @param \ImaTelecomBundle\Model\BaixaMovimento|ObjectCollection $baixaMovimento the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBaixaMovimento($baixaMovimento, $comparison = null)
    {
        if ($baixaMovimento instanceof \ImaTelecomBundle\Model\BaixaMovimento) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $baixaMovimento->getUsuarioBaixa(), $comparison);
        } elseif ($baixaMovimento instanceof ObjectCollection) {
            return $this
                ->useBaixaMovimentoQuery()
                ->filterByPrimaryKeys($baixaMovimento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixaMovimento() only accepts arguments of type \ImaTelecomBundle\Model\BaixaMovimento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaixaMovimento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBaixaMovimento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaixaMovimento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaixaMovimento');
        }

        return $this;
    }

    /**
     * Use the BaixaMovimento relation BaixaMovimento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaMovimentoQuery A secondary query class using the current class as primary query
     */
    public function useBaixaMovimentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixaMovimento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaixaMovimento', '\ImaTelecomBundle\Model\BaixaMovimentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Banco object
     *
     * @param \ImaTelecomBundle\Model\Banco|ObjectCollection $banco the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBanco($banco, $comparison = null)
    {
        if ($banco instanceof \ImaTelecomBundle\Model\Banco) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $banco->getUsuarioAlterado(), $comparison);
        } elseif ($banco instanceof ObjectCollection) {
            return $this
                ->useBancoQuery()
                ->filterByPrimaryKeys($banco->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBanco() only accepts arguments of type \ImaTelecomBundle\Model\Banco or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Banco relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBanco($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Banco');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Banco');
        }

        return $this;
    }

    /**
     * Use the Banco relation Banco object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoQuery A secondary query class using the current class as primary query
     */
    public function useBancoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBanco($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Banco', '\ImaTelecomBundle\Model\BancoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BancoAgencia object
     *
     * @param \ImaTelecomBundle\Model\BancoAgencia|ObjectCollection $bancoAgencia the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBancoAgencia($bancoAgencia, $comparison = null)
    {
        if ($bancoAgencia instanceof \ImaTelecomBundle\Model\BancoAgencia) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $bancoAgencia->getUsuarioAlterado(), $comparison);
        } elseif ($bancoAgencia instanceof ObjectCollection) {
            return $this
                ->useBancoAgenciaQuery()
                ->filterByPrimaryKeys($bancoAgencia->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBancoAgencia() only accepts arguments of type \ImaTelecomBundle\Model\BancoAgencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BancoAgencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBancoAgencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BancoAgencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BancoAgencia');
        }

        return $this;
    }

    /**
     * Use the BancoAgencia relation BancoAgencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoAgenciaQuery A secondary query class using the current class as primary query
     */
    public function useBancoAgenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBancoAgencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BancoAgencia', '\ImaTelecomBundle\Model\BancoAgenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BancoAgenciaConta object
     *
     * @param \ImaTelecomBundle\Model\BancoAgenciaConta|ObjectCollection $bancoAgenciaConta the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBancoAgenciaConta($bancoAgenciaConta, $comparison = null)
    {
        if ($bancoAgenciaConta instanceof \ImaTelecomBundle\Model\BancoAgenciaConta) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $bancoAgenciaConta->getUsuarioAlterado(), $comparison);
        } elseif ($bancoAgenciaConta instanceof ObjectCollection) {
            return $this
                ->useBancoAgenciaContaQuery()
                ->filterByPrimaryKeys($bancoAgenciaConta->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBancoAgenciaConta() only accepts arguments of type \ImaTelecomBundle\Model\BancoAgenciaConta or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BancoAgenciaConta relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBancoAgenciaConta($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BancoAgenciaConta');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BancoAgenciaConta');
        }

        return $this;
    }

    /**
     * Use the BancoAgenciaConta relation BancoAgenciaConta object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoAgenciaContaQuery A secondary query class using the current class as primary query
     */
    public function useBancoAgenciaContaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBancoAgenciaConta($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BancoAgenciaConta', '\ImaTelecomBundle\Model\BancoAgenciaContaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $boleto->getUsuarioAlterado(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            return $this
                ->useBoletoQuery()
                ->filterByPrimaryKeys($boleto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoBaixaHistorico object
     *
     * @param \ImaTelecomBundle\Model\BoletoBaixaHistorico|ObjectCollection $boletoBaixaHistorico the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBoletoBaixaHistorico($boletoBaixaHistorico, $comparison = null)
    {
        if ($boletoBaixaHistorico instanceof \ImaTelecomBundle\Model\BoletoBaixaHistorico) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $boletoBaixaHistorico->getUsuarioAlterado(), $comparison);
        } elseif ($boletoBaixaHistorico instanceof ObjectCollection) {
            return $this
                ->useBoletoBaixaHistoricoQuery()
                ->filterByPrimaryKeys($boletoBaixaHistorico->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoBaixaHistorico() only accepts arguments of type \ImaTelecomBundle\Model\BoletoBaixaHistorico or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoBaixaHistorico relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBoletoBaixaHistorico($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoBaixaHistorico');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoBaixaHistorico');
        }

        return $this;
    }

    /**
     * Use the BoletoBaixaHistorico relation BoletoBaixaHistorico object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoBaixaHistoricoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoletoBaixaHistorico($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoBaixaHistorico', '\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoItem object
     *
     * @param \ImaTelecomBundle\Model\BoletoItem|ObjectCollection $boletoItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBoletoItem($boletoItem, $comparison = null)
    {
        if ($boletoItem instanceof \ImaTelecomBundle\Model\BoletoItem) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $boletoItem->getUsuarioAlterado(), $comparison);
        } elseif ($boletoItem instanceof ObjectCollection) {
            return $this
                ->useBoletoItemQuery()
                ->filterByPrimaryKeys($boletoItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoItem() only accepts arguments of type \ImaTelecomBundle\Model\BoletoItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBoletoItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoItem');
        }

        return $this;
    }

    /**
     * Use the BoletoItem relation BoletoItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoItemQuery A secondary query class using the current class as primary query
     */
    public function useBoletoItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoletoItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoItem', '\ImaTelecomBundle\Model\BoletoItemQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Caixa object
     *
     * @param \ImaTelecomBundle\Model\Caixa|ObjectCollection $caixa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByCaixa($caixa, $comparison = null)
    {
        if ($caixa instanceof \ImaTelecomBundle\Model\Caixa) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $caixa->getUsuarioBaixa(), $comparison);
        } elseif ($caixa instanceof ObjectCollection) {
            return $this
                ->useCaixaQuery()
                ->filterByPrimaryKeys($caixa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCaixa() only accepts arguments of type \ImaTelecomBundle\Model\Caixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Caixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinCaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Caixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Caixa');
        }

        return $this;
    }

    /**
     * Use the Caixa relation Caixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CaixaQuery A secondary query class using the current class as primary query
     */
    public function useCaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Caixa', '\ImaTelecomBundle\Model\CaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cliente object
     *
     * @param \ImaTelecomBundle\Model\Cliente|ObjectCollection $cliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByCliente($cliente, $comparison = null)
    {
        if ($cliente instanceof \ImaTelecomBundle\Model\Cliente) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $cliente->getUsuarioAlterado(), $comparison);
        } elseif ($cliente instanceof ObjectCollection) {
            return $this
                ->useClienteQuery()
                ->filterByPrimaryKeys($cliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCliente() only accepts arguments of type \ImaTelecomBundle\Model\Cliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cliente');
        }

        return $this;
    }

    /**
     * Use the Cliente relation Cliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteQuery A secondary query class using the current class as primary query
     */
    public function useClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cliente', '\ImaTelecomBundle\Model\ClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ClienteEstoqueItem object
     *
     * @param \ImaTelecomBundle\Model\ClienteEstoqueItem|ObjectCollection $clienteEstoqueItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByClienteEstoqueItem($clienteEstoqueItem, $comparison = null)
    {
        if ($clienteEstoqueItem instanceof \ImaTelecomBundle\Model\ClienteEstoqueItem) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $clienteEstoqueItem->getUsuarioAlterado(), $comparison);
        } elseif ($clienteEstoqueItem instanceof ObjectCollection) {
            return $this
                ->useClienteEstoqueItemQuery()
                ->filterByPrimaryKeys($clienteEstoqueItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByClienteEstoqueItem() only accepts arguments of type \ImaTelecomBundle\Model\ClienteEstoqueItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ClienteEstoqueItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinClienteEstoqueItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ClienteEstoqueItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ClienteEstoqueItem');
        }

        return $this;
    }

    /**
     * Use the ClienteEstoqueItem relation ClienteEstoqueItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteEstoqueItemQuery A secondary query class using the current class as primary query
     */
    public function useClienteEstoqueItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinClienteEstoqueItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ClienteEstoqueItem', '\ImaTelecomBundle\Model\ClienteEstoqueItemQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContaCaixa object
     *
     * @param \ImaTelecomBundle\Model\ContaCaixa|ObjectCollection $contaCaixa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByContaCaixa($contaCaixa, $comparison = null)
    {
        if ($contaCaixa instanceof \ImaTelecomBundle\Model\ContaCaixa) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $contaCaixa->getUsuarioAlterado(), $comparison);
        } elseif ($contaCaixa instanceof ObjectCollection) {
            return $this
                ->useContaCaixaQuery()
                ->filterByPrimaryKeys($contaCaixa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContaCaixa() only accepts arguments of type \ImaTelecomBundle\Model\ContaCaixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContaCaixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinContaCaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContaCaixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContaCaixa');
        }

        return $this;
    }

    /**
     * Use the ContaCaixa relation ContaCaixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContaCaixaQuery A secondary query class using the current class as primary query
     */
    public function useContaCaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContaCaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContaCaixa', '\ImaTelecomBundle\Model\ContaCaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagar object
     *
     * @param \ImaTelecomBundle\Model\ContasPagar|ObjectCollection $contasPagar the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByContasPagar($contasPagar, $comparison = null)
    {
        if ($contasPagar instanceof \ImaTelecomBundle\Model\ContasPagar) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $contasPagar->getUsuarioAlterado(), $comparison);
        } elseif ($contasPagar instanceof ObjectCollection) {
            return $this
                ->useContasPagarQuery()
                ->filterByPrimaryKeys($contasPagar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContasPagar() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinContasPagar($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagar');
        }

        return $this;
    }

    /**
     * Use the ContasPagar relation ContasPagar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContasPagar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagar', '\ImaTelecomBundle\Model\ContasPagarQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagarTributos object
     *
     * @param \ImaTelecomBundle\Model\ContasPagarTributos|ObjectCollection $contasPagarTributos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByContasPagarTributos($contasPagarTributos, $comparison = null)
    {
        if ($contasPagarTributos instanceof \ImaTelecomBundle\Model\ContasPagarTributos) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $contasPagarTributos->getUsuarioAlterado(), $comparison);
        } elseif ($contasPagarTributos instanceof ObjectCollection) {
            return $this
                ->useContasPagarTributosQuery()
                ->filterByPrimaryKeys($contasPagarTributos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContasPagarTributos() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagarTributos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagarTributos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinContasPagarTributos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagarTributos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagarTributos');
        }

        return $this;
    }

    /**
     * Use the ContasPagarTributos relation ContasPagarTributos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarTributosQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarTributosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContasPagarTributos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagarTributos', '\ImaTelecomBundle\Model\ContasPagarTributosQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Convenio object
     *
     * @param \ImaTelecomBundle\Model\Convenio|ObjectCollection $convenio the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByConvenio($convenio, $comparison = null)
    {
        if ($convenio instanceof \ImaTelecomBundle\Model\Convenio) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $convenio->getUsuarioAlterado(), $comparison);
        } elseif ($convenio instanceof ObjectCollection) {
            return $this
                ->useConvenioQuery()
                ->filterByPrimaryKeys($convenio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByConvenio() only accepts arguments of type \ImaTelecomBundle\Model\Convenio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Convenio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinConvenio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Convenio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Convenio');
        }

        return $this;
    }

    /**
     * Use the Convenio relation Convenio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ConvenioQuery A secondary query class using the current class as primary query
     */
    public function useConvenioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinConvenio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Convenio', '\ImaTelecomBundle\Model\ConvenioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\CronTask object
     *
     * @param \ImaTelecomBundle\Model\CronTask|ObjectCollection $cronTask the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByCronTask($cronTask, $comparison = null)
    {
        if ($cronTask instanceof \ImaTelecomBundle\Model\CronTask) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $cronTask->getUsuarioAlterado(), $comparison);
        } elseif ($cronTask instanceof ObjectCollection) {
            return $this
                ->useCronTaskQuery()
                ->filterByPrimaryKeys($cronTask->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCronTask() only accepts arguments of type \ImaTelecomBundle\Model\CronTask or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CronTask relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinCronTask($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CronTask');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CronTask');
        }

        return $this;
    }

    /**
     * Use the CronTask relation CronTask object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CronTaskQuery A secondary query class using the current class as primary query
     */
    public function useCronTaskQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCronTask($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CronTask', '\ImaTelecomBundle\Model\CronTaskQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\CronTaskGrupo object
     *
     * @param \ImaTelecomBundle\Model\CronTaskGrupo|ObjectCollection $cronTaskGrupo the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByCronTaskGrupo($cronTaskGrupo, $comparison = null)
    {
        if ($cronTaskGrupo instanceof \ImaTelecomBundle\Model\CronTaskGrupo) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $cronTaskGrupo->getUsuarioAlterado(), $comparison);
        } elseif ($cronTaskGrupo instanceof ObjectCollection) {
            return $this
                ->useCronTaskGrupoQuery()
                ->filterByPrimaryKeys($cronTaskGrupo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCronTaskGrupo() only accepts arguments of type \ImaTelecomBundle\Model\CronTaskGrupo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CronTaskGrupo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinCronTaskGrupo($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CronTaskGrupo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CronTaskGrupo');
        }

        return $this;
    }

    /**
     * Use the CronTaskGrupo relation CronTaskGrupo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CronTaskGrupoQuery A secondary query class using the current class as primary query
     */
    public function useCronTaskGrupoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCronTaskGrupo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CronTaskGrupo', '\ImaTelecomBundle\Model\CronTaskGrupoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoAgencia object
     *
     * @param \ImaTelecomBundle\Model\EnderecoAgencia|ObjectCollection $enderecoAgencia the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByEnderecoAgencia($enderecoAgencia, $comparison = null)
    {
        if ($enderecoAgencia instanceof \ImaTelecomBundle\Model\EnderecoAgencia) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $enderecoAgencia->getUsuarioAlterado(), $comparison);
        } elseif ($enderecoAgencia instanceof ObjectCollection) {
            return $this
                ->useEnderecoAgenciaQuery()
                ->filterByPrimaryKeys($enderecoAgencia->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEnderecoAgencia() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoAgencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoAgencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinEnderecoAgencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoAgencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoAgencia');
        }

        return $this;
    }

    /**
     * Use the EnderecoAgencia relation EnderecoAgencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoAgenciaQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoAgenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEnderecoAgencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoAgencia', '\ImaTelecomBundle\Model\EnderecoAgenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoCliente object
     *
     * @param \ImaTelecomBundle\Model\EnderecoCliente|ObjectCollection $enderecoCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByEnderecoCliente($enderecoCliente, $comparison = null)
    {
        if ($enderecoCliente instanceof \ImaTelecomBundle\Model\EnderecoCliente) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $enderecoCliente->getUsuarioAlterado(), $comparison);
        } elseif ($enderecoCliente instanceof ObjectCollection) {
            return $this
                ->useEnderecoClienteQuery()
                ->filterByPrimaryKeys($enderecoCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEnderecoCliente() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinEnderecoCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoCliente');
        }

        return $this;
    }

    /**
     * Use the EnderecoCliente relation EnderecoCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoClienteQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEnderecoCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoCliente', '\ImaTelecomBundle\Model\EnderecoClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoServCliente object
     *
     * @param \ImaTelecomBundle\Model\EnderecoServCliente|ObjectCollection $enderecoServCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByEnderecoServCliente($enderecoServCliente, $comparison = null)
    {
        if ($enderecoServCliente instanceof \ImaTelecomBundle\Model\EnderecoServCliente) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $enderecoServCliente->getUsuarioAlterado(), $comparison);
        } elseif ($enderecoServCliente instanceof ObjectCollection) {
            return $this
                ->useEnderecoServClienteQuery()
                ->filterByPrimaryKeys($enderecoServCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEnderecoServCliente() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoServCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoServCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinEnderecoServCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoServCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoServCliente');
        }

        return $this;
    }

    /**
     * Use the EnderecoServCliente relation EnderecoServCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoServClienteQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoServClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEnderecoServCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoServCliente', '\ImaTelecomBundle\Model\EnderecoServClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Estoque object
     *
     * @param \ImaTelecomBundle\Model\Estoque|ObjectCollection $estoque the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByEstoque($estoque, $comparison = null)
    {
        if ($estoque instanceof \ImaTelecomBundle\Model\Estoque) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $estoque->getUsuarioAlterado(), $comparison);
        } elseif ($estoque instanceof ObjectCollection) {
            return $this
                ->useEstoqueQuery()
                ->filterByPrimaryKeys($estoque->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEstoque() only accepts arguments of type \ImaTelecomBundle\Model\Estoque or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Estoque relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinEstoque($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Estoque');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Estoque');
        }

        return $this;
    }

    /**
     * Use the Estoque relation Estoque object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstoque($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Estoque', '\ImaTelecomBundle\Model\EstoqueQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EstoqueLancamento object
     *
     * @param \ImaTelecomBundle\Model\EstoqueLancamento|ObjectCollection $estoqueLancamento the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByEstoqueLancamento($estoqueLancamento, $comparison = null)
    {
        if ($estoqueLancamento instanceof \ImaTelecomBundle\Model\EstoqueLancamento) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $estoqueLancamento->getUsuarioAlterado(), $comparison);
        } elseif ($estoqueLancamento instanceof ObjectCollection) {
            return $this
                ->useEstoqueLancamentoQuery()
                ->filterByPrimaryKeys($estoqueLancamento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEstoqueLancamento() only accepts arguments of type \ImaTelecomBundle\Model\EstoqueLancamento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EstoqueLancamento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinEstoqueLancamento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EstoqueLancamento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EstoqueLancamento');
        }

        return $this;
    }

    /**
     * Use the EstoqueLancamento relation EstoqueLancamento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueLancamentoQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueLancamentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstoqueLancamento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EstoqueLancamento', '\ImaTelecomBundle\Model\EstoqueLancamentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EstoqueLancamentoItem object
     *
     * @param \ImaTelecomBundle\Model\EstoqueLancamentoItem|ObjectCollection $estoqueLancamentoItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByEstoqueLancamentoItem($estoqueLancamentoItem, $comparison = null)
    {
        if ($estoqueLancamentoItem instanceof \ImaTelecomBundle\Model\EstoqueLancamentoItem) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $estoqueLancamentoItem->getUsuarioAlterado(), $comparison);
        } elseif ($estoqueLancamentoItem instanceof ObjectCollection) {
            return $this
                ->useEstoqueLancamentoItemQuery()
                ->filterByPrimaryKeys($estoqueLancamentoItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEstoqueLancamentoItem() only accepts arguments of type \ImaTelecomBundle\Model\EstoqueLancamentoItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EstoqueLancamentoItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinEstoqueLancamentoItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EstoqueLancamentoItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EstoqueLancamentoItem');
        }

        return $this;
    }

    /**
     * Use the EstoqueLancamentoItem relation EstoqueLancamentoItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueLancamentoItemQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueLancamentoItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstoqueLancamentoItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EstoqueLancamentoItem', '\ImaTelecomBundle\Model\EstoqueLancamentoItemQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\FormaPagamento object
     *
     * @param \ImaTelecomBundle\Model\FormaPagamento|ObjectCollection $formaPagamento the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByFormaPagamento($formaPagamento, $comparison = null)
    {
        if ($formaPagamento instanceof \ImaTelecomBundle\Model\FormaPagamento) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $formaPagamento->getUsuarioAlterado(), $comparison);
        } elseif ($formaPagamento instanceof ObjectCollection) {
            return $this
                ->useFormaPagamentoQuery()
                ->filterByPrimaryKeys($formaPagamento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFormaPagamento() only accepts arguments of type \ImaTelecomBundle\Model\FormaPagamento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FormaPagamento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinFormaPagamento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FormaPagamento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FormaPagamento');
        }

        return $this;
    }

    /**
     * Use the FormaPagamento relation FormaPagamento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FormaPagamentoQuery A secondary query class using the current class as primary query
     */
    public function useFormaPagamentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFormaPagamento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FormaPagamento', '\ImaTelecomBundle\Model\FormaPagamentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Fornecedor object
     *
     * @param \ImaTelecomBundle\Model\Fornecedor|ObjectCollection $fornecedor the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByFornecedor($fornecedor, $comparison = null)
    {
        if ($fornecedor instanceof \ImaTelecomBundle\Model\Fornecedor) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $fornecedor->getUsuarioAlterado(), $comparison);
        } elseif ($fornecedor instanceof ObjectCollection) {
            return $this
                ->useFornecedorQuery()
                ->filterByPrimaryKeys($fornecedor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFornecedor() only accepts arguments of type \ImaTelecomBundle\Model\Fornecedor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fornecedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinFornecedor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fornecedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fornecedor');
        }

        return $this;
    }

    /**
     * Use the Fornecedor relation Fornecedor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FornecedorQuery A secondary query class using the current class as primary query
     */
    public function useFornecedorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFornecedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fornecedor', '\ImaTelecomBundle\Model\FornecedorQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ItemComprado object
     *
     * @param \ImaTelecomBundle\Model\ItemComprado|ObjectCollection $itemComprado the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByItemComprado($itemComprado, $comparison = null)
    {
        if ($itemComprado instanceof \ImaTelecomBundle\Model\ItemComprado) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $itemComprado->getUsuarioAlterado(), $comparison);
        } elseif ($itemComprado instanceof ObjectCollection) {
            return $this
                ->useItemCompradoQuery()
                ->filterByPrimaryKeys($itemComprado->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByItemComprado() only accepts arguments of type \ImaTelecomBundle\Model\ItemComprado or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ItemComprado relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinItemComprado($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ItemComprado');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ItemComprado');
        }

        return $this;
    }

    /**
     * Use the ItemComprado relation ItemComprado object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ItemCompradoQuery A secondary query class using the current class as primary query
     */
    public function useItemCompradoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinItemComprado($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ItemComprado', '\ImaTelecomBundle\Model\ItemCompradoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ItemDeCompra object
     *
     * @param \ImaTelecomBundle\Model\ItemDeCompra|ObjectCollection $itemDeCompra the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByItemDeCompra($itemDeCompra, $comparison = null)
    {
        if ($itemDeCompra instanceof \ImaTelecomBundle\Model\ItemDeCompra) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $itemDeCompra->getUsuarioAlterado(), $comparison);
        } elseif ($itemDeCompra instanceof ObjectCollection) {
            return $this
                ->useItemDeCompraQuery()
                ->filterByPrimaryKeys($itemDeCompra->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByItemDeCompra() only accepts arguments of type \ImaTelecomBundle\Model\ItemDeCompra or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ItemDeCompra relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinItemDeCompra($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ItemDeCompra');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ItemDeCompra');
        }

        return $this;
    }

    /**
     * Use the ItemDeCompra relation ItemDeCompra object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ItemDeCompraQuery A secondary query class using the current class as primary query
     */
    public function useItemDeCompraQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinItemDeCompra($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ItemDeCompra', '\ImaTelecomBundle\Model\ItemDeCompraQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ServicoCliente object
     *
     * @param \ImaTelecomBundle\Model\ServicoCliente|ObjectCollection $servicoCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByServicoCliente($servicoCliente, $comparison = null)
    {
        if ($servicoCliente instanceof \ImaTelecomBundle\Model\ServicoCliente) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $servicoCliente->getUsuarioAlterado(), $comparison);
        } elseif ($servicoCliente instanceof ObjectCollection) {
            return $this
                ->useServicoClienteQuery()
                ->filterByPrimaryKeys($servicoCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicoCliente() only accepts arguments of type \ImaTelecomBundle\Model\ServicoCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicoCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinServicoCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicoCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicoCliente');
        }

        return $this;
    }

    /**
     * Use the ServicoCliente relation ServicoCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ServicoClienteQuery A secondary query class using the current class as primary query
     */
    public function useServicoClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicoCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicoCliente', '\ImaTelecomBundle\Model\ServicoClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ServicoPrestado object
     *
     * @param \ImaTelecomBundle\Model\ServicoPrestado|ObjectCollection $servicoPrestado the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByServicoPrestado($servicoPrestado, $comparison = null)
    {
        if ($servicoPrestado instanceof \ImaTelecomBundle\Model\ServicoPrestado) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $servicoPrestado->getUsuarioAlterado(), $comparison);
        } elseif ($servicoPrestado instanceof ObjectCollection) {
            return $this
                ->useServicoPrestadoQuery()
                ->filterByPrimaryKeys($servicoPrestado->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicoPrestado() only accepts arguments of type \ImaTelecomBundle\Model\ServicoPrestado or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicoPrestado relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinServicoPrestado($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicoPrestado');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicoPrestado');
        }

        return $this;
    }

    /**
     * Use the ServicoPrestado relation ServicoPrestado object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ServicoPrestadoQuery A secondary query class using the current class as primary query
     */
    public function useServicoPrestadoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicoPrestado($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicoPrestado', '\ImaTelecomBundle\Model\ServicoPrestadoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Sici object
     *
     * @param \ImaTelecomBundle\Model\Sici|ObjectCollection $sici the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterBySici($sici, $comparison = null)
    {
        if ($sici instanceof \ImaTelecomBundle\Model\Sici) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $sici->getUsuarioAlterado(), $comparison);
        } elseif ($sici instanceof ObjectCollection) {
            return $this
                ->useSiciQuery()
                ->filterByPrimaryKeys($sici->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySici() only accepts arguments of type \ImaTelecomBundle\Model\Sici or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Sici relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinSici($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Sici');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Sici');
        }

        return $this;
    }

    /**
     * Use the Sici relation Sici object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\SiciQuery A secondary query class using the current class as primary query
     */
    public function useSiciQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSici($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Sici', '\ImaTelecomBundle\Model\SiciQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\SysPerfil object
     *
     * @param \ImaTelecomBundle\Model\SysPerfil|ObjectCollection $sysPerfil the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterBySysPerfil($sysPerfil, $comparison = null)
    {
        if ($sysPerfil instanceof \ImaTelecomBundle\Model\SysPerfil) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $sysPerfil->getUsuarioAlterado(), $comparison);
        } elseif ($sysPerfil instanceof ObjectCollection) {
            return $this
                ->useSysPerfilQuery()
                ->filterByPrimaryKeys($sysPerfil->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysPerfil() only accepts arguments of type \ImaTelecomBundle\Model\SysPerfil or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysPerfil relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinSysPerfil($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysPerfil');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysPerfil');
        }

        return $this;
    }

    /**
     * Use the SysPerfil relation SysPerfil object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\SysPerfilQuery A secondary query class using the current class as primary query
     */
    public function useSysPerfilQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSysPerfil($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysPerfil', '\ImaTelecomBundle\Model\SysPerfilQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\SysProcessos object
     *
     * @param \ImaTelecomBundle\Model\SysProcessos|ObjectCollection $sysProcessos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterBySysProcessos($sysProcessos, $comparison = null)
    {
        if ($sysProcessos instanceof \ImaTelecomBundle\Model\SysProcessos) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $sysProcessos->getUsuarioAlterado(), $comparison);
        } elseif ($sysProcessos instanceof ObjectCollection) {
            return $this
                ->useSysProcessosQuery()
                ->filterByPrimaryKeys($sysProcessos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysProcessos() only accepts arguments of type \ImaTelecomBundle\Model\SysProcessos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysProcessos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinSysProcessos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysProcessos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysProcessos');
        }

        return $this;
    }

    /**
     * Use the SysProcessos relation SysProcessos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\SysProcessosQuery A secondary query class using the current class as primary query
     */
    public function useSysProcessosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSysProcessos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysProcessos', '\ImaTelecomBundle\Model\SysProcessosQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\SysUsuarioPerfil object
     *
     * @param \ImaTelecomBundle\Model\SysUsuarioPerfil|ObjectCollection $sysUsuarioPerfil the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterBySysUsuarioPerfil($sysUsuarioPerfil, $comparison = null)
    {
        if ($sysUsuarioPerfil instanceof \ImaTelecomBundle\Model\SysUsuarioPerfil) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $sysUsuarioPerfil->getUsuarioId(), $comparison);
        } elseif ($sysUsuarioPerfil instanceof ObjectCollection) {
            return $this
                ->useSysUsuarioPerfilQuery()
                ->filterByPrimaryKeys($sysUsuarioPerfil->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySysUsuarioPerfil() only accepts arguments of type \ImaTelecomBundle\Model\SysUsuarioPerfil or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SysUsuarioPerfil relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinSysUsuarioPerfil($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SysUsuarioPerfil');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SysUsuarioPerfil');
        }

        return $this;
    }

    /**
     * Use the SysUsuarioPerfil relation SysUsuarioPerfil object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\SysUsuarioPerfilQuery A secondary query class using the current class as primary query
     */
    public function useSysUsuarioPerfilQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSysUsuarioPerfil($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SysUsuarioPerfil', '\ImaTelecomBundle\Model\SysUsuarioPerfilQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Telefone object
     *
     * @param \ImaTelecomBundle\Model\Telefone|ObjectCollection $telefone the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByTelefone($telefone, $comparison = null)
    {
        if ($telefone instanceof \ImaTelecomBundle\Model\Telefone) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $telefone->getUsuarioAlterado(), $comparison);
        } elseif ($telefone instanceof ObjectCollection) {
            return $this
                ->useTelefoneQuery()
                ->filterByPrimaryKeys($telefone->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTelefone() only accepts arguments of type \ImaTelecomBundle\Model\Telefone or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Telefone relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinTelefone($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Telefone');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Telefone');
        }

        return $this;
    }

    /**
     * Use the Telefone relation Telefone object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\TelefoneQuery A secondary query class using the current class as primary query
     */
    public function useTelefoneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTelefone($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Telefone', '\ImaTelecomBundle\Model\TelefoneQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Tributo object
     *
     * @param \ImaTelecomBundle\Model\Tributo|ObjectCollection $tributo the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByTributo($tributo, $comparison = null)
    {
        if ($tributo instanceof \ImaTelecomBundle\Model\Tributo) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $tributo->getUsuarioAlterado(), $comparison);
        } elseif ($tributo instanceof ObjectCollection) {
            return $this
                ->useTributoQuery()
                ->filterByPrimaryKeys($tributo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTributo() only accepts arguments of type \ImaTelecomBundle\Model\Tributo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Tributo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinTributo($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Tributo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Tributo');
        }

        return $this;
    }

    /**
     * Use the Tributo relation Tributo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\TributoQuery A secondary query class using the current class as primary query
     */
    public function useTributoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTributo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Tributo', '\ImaTelecomBundle\Model\TributoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUsuario $usuario Object to remove from the list of results
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function prune($usuario = null)
    {
        if ($usuario) {
            $this->addUsingAlias(UsuarioTableMap::COL_IDUSUARIO, $usuario->getIdusuario(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the usuario table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UsuarioTableMap::clearInstancePool();
            UsuarioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UsuarioTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UsuarioTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UsuarioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UsuarioQuery
