<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'boleto' table.
 *
 *
 *
 * @method     ChildBoletoQuery orderByIdboleto($order = Criteria::ASC) Order by the idboleto column
 * @method     ChildBoletoQuery orderByValor($order = Criteria::ASC) Order by the valor column
 * @method     ChildBoletoQuery orderByVencimento($order = Criteria::ASC) Order by the vencimento column
 * @method     ChildBoletoQuery orderByClienteId($order = Criteria::ASC) Order by the cliente_id column
 * @method     ChildBoletoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildBoletoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildBoletoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildBoletoQuery orderByImportId($order = Criteria::ASC) Order by the import_id column
 * @method     ChildBoletoQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildBoletoQuery orderByNossoNumero($order = Criteria::ASC) Order by the nosso_numero column
 * @method     ChildBoletoQuery orderByNumeroDocumento($order = Criteria::ASC) Order by the numero_documento column
 * @method     ChildBoletoQuery orderByRegistrado($order = Criteria::ASC) Order by the registrado column
 * @method     ChildBoletoQuery orderByValorOriginal($order = Criteria::ASC) Order by the valor_original column
 * @method     ChildBoletoQuery orderByValorMulta($order = Criteria::ASC) Order by the valor_multa column
 * @method     ChildBoletoQuery orderByValorJuros($order = Criteria::ASC) Order by the valor_juros column
 * @method     ChildBoletoQuery orderByValorDesconto($order = Criteria::ASC) Order by the valor_desconto column
 * @method     ChildBoletoQuery orderByValorPago($order = Criteria::ASC) Order by the valor_pago column
 * @method     ChildBoletoQuery orderByLinhaDigitavel($order = Criteria::ASC) Order by the linha_digitavel column
 * @method     ChildBoletoQuery orderByDataPagamento($order = Criteria::ASC) Order by the data_pagamento column
 * @method     ChildBoletoQuery orderByEstaPago($order = Criteria::ASC) Order by the esta_pago column
 * @method     ChildBoletoQuery orderByValorAbatimento($order = Criteria::ASC) Order by the valor_abatimento column
 * @method     ChildBoletoQuery orderByValorIof($order = Criteria::ASC) Order by the valor_iof column
 * @method     ChildBoletoQuery orderByValorLiquido($order = Criteria::ASC) Order by the valor_liquido column
 * @method     ChildBoletoQuery orderByValorOutrasDespesas($order = Criteria::ASC) Order by the valor_outras_despesas column
 * @method     ChildBoletoQuery orderByValorOutrosCreditos($order = Criteria::ASC) Order by the valor_outros_creditos column
 * @method     ChildBoletoQuery orderByNotaScmGerada($order = Criteria::ASC) Order by the nota_scm_gerada column
 * @method     ChildBoletoQuery orderByNotaScmCancelada($order = Criteria::ASC) Order by the nota_scm_cancelada column
 * @method     ChildBoletoQuery orderByBaseCalculoScm($order = Criteria::ASC) Order by the base_calculo_scm column
 * @method     ChildBoletoQuery orderByBaseCalculoSva($order = Criteria::ASC) Order by the base_calculo_sva column
 * @method     ChildBoletoQuery orderByNotaSvaGerada($order = Criteria::ASC) Order by the nota_sva_gerada column
 * @method     ChildBoletoQuery orderByNotaSvaCancelada($order = Criteria::ASC) Order by the nota_sva_cancelada column
 * @method     ChildBoletoQuery orderByTipoConta($order = Criteria::ASC) Order by the tipo_conta column
 * @method     ChildBoletoQuery orderByBaixaId($order = Criteria::ASC) Order by the baixa_id column
 * @method     ChildBoletoQuery orderByBaixaEstornoId($order = Criteria::ASC) Order by the baixa_estorno_id column
 * @method     ChildBoletoQuery orderByFornecedorId($order = Criteria::ASC) Order by the fornecedor_id column
 *
 * @method     ChildBoletoQuery groupByIdboleto() Group by the idboleto column
 * @method     ChildBoletoQuery groupByValor() Group by the valor column
 * @method     ChildBoletoQuery groupByVencimento() Group by the vencimento column
 * @method     ChildBoletoQuery groupByClienteId() Group by the cliente_id column
 * @method     ChildBoletoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildBoletoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildBoletoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildBoletoQuery groupByImportId() Group by the import_id column
 * @method     ChildBoletoQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildBoletoQuery groupByNossoNumero() Group by the nosso_numero column
 * @method     ChildBoletoQuery groupByNumeroDocumento() Group by the numero_documento column
 * @method     ChildBoletoQuery groupByRegistrado() Group by the registrado column
 * @method     ChildBoletoQuery groupByValorOriginal() Group by the valor_original column
 * @method     ChildBoletoQuery groupByValorMulta() Group by the valor_multa column
 * @method     ChildBoletoQuery groupByValorJuros() Group by the valor_juros column
 * @method     ChildBoletoQuery groupByValorDesconto() Group by the valor_desconto column
 * @method     ChildBoletoQuery groupByValorPago() Group by the valor_pago column
 * @method     ChildBoletoQuery groupByLinhaDigitavel() Group by the linha_digitavel column
 * @method     ChildBoletoQuery groupByDataPagamento() Group by the data_pagamento column
 * @method     ChildBoletoQuery groupByEstaPago() Group by the esta_pago column
 * @method     ChildBoletoQuery groupByValorAbatimento() Group by the valor_abatimento column
 * @method     ChildBoletoQuery groupByValorIof() Group by the valor_iof column
 * @method     ChildBoletoQuery groupByValorLiquido() Group by the valor_liquido column
 * @method     ChildBoletoQuery groupByValorOutrasDespesas() Group by the valor_outras_despesas column
 * @method     ChildBoletoQuery groupByValorOutrosCreditos() Group by the valor_outros_creditos column
 * @method     ChildBoletoQuery groupByNotaScmGerada() Group by the nota_scm_gerada column
 * @method     ChildBoletoQuery groupByNotaScmCancelada() Group by the nota_scm_cancelada column
 * @method     ChildBoletoQuery groupByBaseCalculoScm() Group by the base_calculo_scm column
 * @method     ChildBoletoQuery groupByBaseCalculoSva() Group by the base_calculo_sva column
 * @method     ChildBoletoQuery groupByNotaSvaGerada() Group by the nota_sva_gerada column
 * @method     ChildBoletoQuery groupByNotaSvaCancelada() Group by the nota_sva_cancelada column
 * @method     ChildBoletoQuery groupByTipoConta() Group by the tipo_conta column
 * @method     ChildBoletoQuery groupByBaixaId() Group by the baixa_id column
 * @method     ChildBoletoQuery groupByBaixaEstornoId() Group by the baixa_estorno_id column
 * @method     ChildBoletoQuery groupByFornecedorId() Group by the fornecedor_id column
 *
 * @method     ChildBoletoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBoletoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBoletoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBoletoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBoletoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBoletoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBoletoQuery leftJoinBaixaEstorno($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildBoletoQuery rightJoinBaixaEstorno($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildBoletoQuery innerJoinBaixaEstorno($relationAlias = null) Adds a INNER JOIN clause to the query using the BaixaEstorno relation
 *
 * @method     ChildBoletoQuery joinWithBaixaEstorno($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildBoletoQuery leftJoinWithBaixaEstorno() Adds a LEFT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildBoletoQuery rightJoinWithBaixaEstorno() Adds a RIGHT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildBoletoQuery innerJoinWithBaixaEstorno() Adds a INNER JOIN clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildBoletoQuery leftJoinBaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Baixa relation
 * @method     ChildBoletoQuery rightJoinBaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Baixa relation
 * @method     ChildBoletoQuery innerJoinBaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Baixa relation
 *
 * @method     ChildBoletoQuery joinWithBaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Baixa relation
 *
 * @method     ChildBoletoQuery leftJoinWithBaixa() Adds a LEFT JOIN clause and with to the query using the Baixa relation
 * @method     ChildBoletoQuery rightJoinWithBaixa() Adds a RIGHT JOIN clause and with to the query using the Baixa relation
 * @method     ChildBoletoQuery innerJoinWithBaixa() Adds a INNER JOIN clause and with to the query using the Baixa relation
 *
 * @method     ChildBoletoQuery leftJoinCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cliente relation
 * @method     ChildBoletoQuery rightJoinCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cliente relation
 * @method     ChildBoletoQuery innerJoinCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the Cliente relation
 *
 * @method     ChildBoletoQuery joinWithCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cliente relation
 *
 * @method     ChildBoletoQuery leftJoinWithCliente() Adds a LEFT JOIN clause and with to the query using the Cliente relation
 * @method     ChildBoletoQuery rightJoinWithCliente() Adds a RIGHT JOIN clause and with to the query using the Cliente relation
 * @method     ChildBoletoQuery innerJoinWithCliente() Adds a INNER JOIN clause and with to the query using the Cliente relation
 *
 * @method     ChildBoletoQuery leftJoinCompetencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Competencia relation
 * @method     ChildBoletoQuery rightJoinCompetencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Competencia relation
 * @method     ChildBoletoQuery innerJoinCompetencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Competencia relation
 *
 * @method     ChildBoletoQuery joinWithCompetencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Competencia relation
 *
 * @method     ChildBoletoQuery leftJoinWithCompetencia() Adds a LEFT JOIN clause and with to the query using the Competencia relation
 * @method     ChildBoletoQuery rightJoinWithCompetencia() Adds a RIGHT JOIN clause and with to the query using the Competencia relation
 * @method     ChildBoletoQuery innerJoinWithCompetencia() Adds a INNER JOIN clause and with to the query using the Competencia relation
 *
 * @method     ChildBoletoQuery leftJoinFornecedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fornecedor relation
 * @method     ChildBoletoQuery rightJoinFornecedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fornecedor relation
 * @method     ChildBoletoQuery innerJoinFornecedor($relationAlias = null) Adds a INNER JOIN clause to the query using the Fornecedor relation
 *
 * @method     ChildBoletoQuery joinWithFornecedor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fornecedor relation
 *
 * @method     ChildBoletoQuery leftJoinWithFornecedor() Adds a LEFT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildBoletoQuery rightJoinWithFornecedor() Adds a RIGHT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildBoletoQuery innerJoinWithFornecedor() Adds a INNER JOIN clause and with to the query using the Fornecedor relation
 *
 * @method     ChildBoletoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBoletoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBoletoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBoletoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBoletoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBoletoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBoletoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildBoletoQuery leftJoinBoletoBaixaHistorico($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildBoletoQuery rightJoinBoletoBaixaHistorico($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildBoletoQuery innerJoinBoletoBaixaHistorico($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildBoletoQuery joinWithBoletoBaixaHistorico($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildBoletoQuery leftJoinWithBoletoBaixaHistorico() Adds a LEFT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildBoletoQuery rightJoinWithBoletoBaixaHistorico() Adds a RIGHT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildBoletoQuery innerJoinWithBoletoBaixaHistorico() Adds a INNER JOIN clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildBoletoQuery leftJoinBoletoItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoItem relation
 * @method     ChildBoletoQuery rightJoinBoletoItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoItem relation
 * @method     ChildBoletoQuery innerJoinBoletoItem($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoItem relation
 *
 * @method     ChildBoletoQuery joinWithBoletoItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoItem relation
 *
 * @method     ChildBoletoQuery leftJoinWithBoletoItem() Adds a LEFT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildBoletoQuery rightJoinWithBoletoItem() Adds a RIGHT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildBoletoQuery innerJoinWithBoletoItem() Adds a INNER JOIN clause and with to the query using the BoletoItem relation
 *
 * @method     ChildBoletoQuery leftJoinDadosFiscal($relationAlias = null) Adds a LEFT JOIN clause to the query using the DadosFiscal relation
 * @method     ChildBoletoQuery rightJoinDadosFiscal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DadosFiscal relation
 * @method     ChildBoletoQuery innerJoinDadosFiscal($relationAlias = null) Adds a INNER JOIN clause to the query using the DadosFiscal relation
 *
 * @method     ChildBoletoQuery joinWithDadosFiscal($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DadosFiscal relation
 *
 * @method     ChildBoletoQuery leftJoinWithDadosFiscal() Adds a LEFT JOIN clause and with to the query using the DadosFiscal relation
 * @method     ChildBoletoQuery rightJoinWithDadosFiscal() Adds a RIGHT JOIN clause and with to the query using the DadosFiscal relation
 * @method     ChildBoletoQuery innerJoinWithDadosFiscal() Adds a INNER JOIN clause and with to the query using the DadosFiscal relation
 *
 * @method     ChildBoletoQuery leftJoinDadosFiscalItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the DadosFiscalItem relation
 * @method     ChildBoletoQuery rightJoinDadosFiscalItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DadosFiscalItem relation
 * @method     ChildBoletoQuery innerJoinDadosFiscalItem($relationAlias = null) Adds a INNER JOIN clause to the query using the DadosFiscalItem relation
 *
 * @method     ChildBoletoQuery joinWithDadosFiscalItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DadosFiscalItem relation
 *
 * @method     ChildBoletoQuery leftJoinWithDadosFiscalItem() Adds a LEFT JOIN clause and with to the query using the DadosFiscalItem relation
 * @method     ChildBoletoQuery rightJoinWithDadosFiscalItem() Adds a RIGHT JOIN clause and with to the query using the DadosFiscalItem relation
 * @method     ChildBoletoQuery innerJoinWithDadosFiscalItem() Adds a INNER JOIN clause and with to the query using the DadosFiscalItem relation
 *
 * @method     ChildBoletoQuery leftJoinLancamentosBoletos($relationAlias = null) Adds a LEFT JOIN clause to the query using the LancamentosBoletos relation
 * @method     ChildBoletoQuery rightJoinLancamentosBoletos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LancamentosBoletos relation
 * @method     ChildBoletoQuery innerJoinLancamentosBoletos($relationAlias = null) Adds a INNER JOIN clause to the query using the LancamentosBoletos relation
 *
 * @method     ChildBoletoQuery joinWithLancamentosBoletos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LancamentosBoletos relation
 *
 * @method     ChildBoletoQuery leftJoinWithLancamentosBoletos() Adds a LEFT JOIN clause and with to the query using the LancamentosBoletos relation
 * @method     ChildBoletoQuery rightJoinWithLancamentosBoletos() Adds a RIGHT JOIN clause and with to the query using the LancamentosBoletos relation
 * @method     ChildBoletoQuery innerJoinWithLancamentosBoletos() Adds a INNER JOIN clause and with to the query using the LancamentosBoletos relation
 *
 * @method     \ImaTelecomBundle\Model\BaixaEstornoQuery|\ImaTelecomBundle\Model\BaixaQuery|\ImaTelecomBundle\Model\ClienteQuery|\ImaTelecomBundle\Model\CompetenciaQuery|\ImaTelecomBundle\Model\FornecedorQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery|\ImaTelecomBundle\Model\BoletoItemQuery|\ImaTelecomBundle\Model\DadosFiscalQuery|\ImaTelecomBundle\Model\DadosFiscalItemQuery|\ImaTelecomBundle\Model\LancamentosBoletosQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBoleto findOne(ConnectionInterface $con = null) Return the first ChildBoleto matching the query
 * @method     ChildBoleto findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBoleto matching the query, or a new ChildBoleto object populated from the query conditions when no match is found
 *
 * @method     ChildBoleto findOneByIdboleto(int $idboleto) Return the first ChildBoleto filtered by the idboleto column
 * @method     ChildBoleto findOneByValor(double $valor) Return the first ChildBoleto filtered by the valor column
 * @method     ChildBoleto findOneByVencimento(string $vencimento) Return the first ChildBoleto filtered by the vencimento column
 * @method     ChildBoleto findOneByClienteId(int $cliente_id) Return the first ChildBoleto filtered by the cliente_id column
 * @method     ChildBoleto findOneByDataCadastro(string $data_cadastro) Return the first ChildBoleto filtered by the data_cadastro column
 * @method     ChildBoleto findOneByDataAlterado(string $data_alterado) Return the first ChildBoleto filtered by the data_alterado column
 * @method     ChildBoleto findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBoleto filtered by the usuario_alterado column
 * @method     ChildBoleto findOneByImportId(int $import_id) Return the first ChildBoleto filtered by the import_id column
 * @method     ChildBoleto findOneByCompetenciaId(int $competencia_id) Return the first ChildBoleto filtered by the competencia_id column
 * @method     ChildBoleto findOneByNossoNumero(string $nosso_numero) Return the first ChildBoleto filtered by the nosso_numero column
 * @method     ChildBoleto findOneByNumeroDocumento(string $numero_documento) Return the first ChildBoleto filtered by the numero_documento column
 * @method     ChildBoleto findOneByRegistrado(boolean $registrado) Return the first ChildBoleto filtered by the registrado column
 * @method     ChildBoleto findOneByValorOriginal(string $valor_original) Return the first ChildBoleto filtered by the valor_original column
 * @method     ChildBoleto findOneByValorMulta(string $valor_multa) Return the first ChildBoleto filtered by the valor_multa column
 * @method     ChildBoleto findOneByValorJuros(string $valor_juros) Return the first ChildBoleto filtered by the valor_juros column
 * @method     ChildBoleto findOneByValorDesconto(string $valor_desconto) Return the first ChildBoleto filtered by the valor_desconto column
 * @method     ChildBoleto findOneByValorPago(string $valor_pago) Return the first ChildBoleto filtered by the valor_pago column
 * @method     ChildBoleto findOneByLinhaDigitavel(string $linha_digitavel) Return the first ChildBoleto filtered by the linha_digitavel column
 * @method     ChildBoleto findOneByDataPagamento(string $data_pagamento) Return the first ChildBoleto filtered by the data_pagamento column
 * @method     ChildBoleto findOneByEstaPago(boolean $esta_pago) Return the first ChildBoleto filtered by the esta_pago column
 * @method     ChildBoleto findOneByValorAbatimento(string $valor_abatimento) Return the first ChildBoleto filtered by the valor_abatimento column
 * @method     ChildBoleto findOneByValorIof(string $valor_iof) Return the first ChildBoleto filtered by the valor_iof column
 * @method     ChildBoleto findOneByValorLiquido(string $valor_liquido) Return the first ChildBoleto filtered by the valor_liquido column
 * @method     ChildBoleto findOneByValorOutrasDespesas(string $valor_outras_despesas) Return the first ChildBoleto filtered by the valor_outras_despesas column
 * @method     ChildBoleto findOneByValorOutrosCreditos(string $valor_outros_creditos) Return the first ChildBoleto filtered by the valor_outros_creditos column
 * @method     ChildBoleto findOneByNotaScmGerada(boolean $nota_scm_gerada) Return the first ChildBoleto filtered by the nota_scm_gerada column
 * @method     ChildBoleto findOneByNotaScmCancelada(boolean $nota_scm_cancelada) Return the first ChildBoleto filtered by the nota_scm_cancelada column
 * @method     ChildBoleto findOneByBaseCalculoScm(string $base_calculo_scm) Return the first ChildBoleto filtered by the base_calculo_scm column
 * @method     ChildBoleto findOneByBaseCalculoSva(string $base_calculo_sva) Return the first ChildBoleto filtered by the base_calculo_sva column
 * @method     ChildBoleto findOneByNotaSvaGerada(boolean $nota_sva_gerada) Return the first ChildBoleto filtered by the nota_sva_gerada column
 * @method     ChildBoleto findOneByNotaSvaCancelada(boolean $nota_sva_cancelada) Return the first ChildBoleto filtered by the nota_sva_cancelada column
 * @method     ChildBoleto findOneByTipoConta(string $tipo_conta) Return the first ChildBoleto filtered by the tipo_conta column
 * @method     ChildBoleto findOneByBaixaId(int $baixa_id) Return the first ChildBoleto filtered by the baixa_id column
 * @method     ChildBoleto findOneByBaixaEstornoId(int $baixa_estorno_id) Return the first ChildBoleto filtered by the baixa_estorno_id column
 * @method     ChildBoleto findOneByFornecedorId(int $fornecedor_id) Return the first ChildBoleto filtered by the fornecedor_id column *

 * @method     ChildBoleto requirePk($key, ConnectionInterface $con = null) Return the ChildBoleto by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOne(ConnectionInterface $con = null) Return the first ChildBoleto matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBoleto requireOneByIdboleto(int $idboleto) Return the first ChildBoleto filtered by the idboleto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValor(double $valor) Return the first ChildBoleto filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByVencimento(string $vencimento) Return the first ChildBoleto filtered by the vencimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByClienteId(int $cliente_id) Return the first ChildBoleto filtered by the cliente_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByDataCadastro(string $data_cadastro) Return the first ChildBoleto filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByDataAlterado(string $data_alterado) Return the first ChildBoleto filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBoleto filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByImportId(int $import_id) Return the first ChildBoleto filtered by the import_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByCompetenciaId(int $competencia_id) Return the first ChildBoleto filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByNossoNumero(string $nosso_numero) Return the first ChildBoleto filtered by the nosso_numero column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByNumeroDocumento(string $numero_documento) Return the first ChildBoleto filtered by the numero_documento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByRegistrado(boolean $registrado) Return the first ChildBoleto filtered by the registrado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorOriginal(string $valor_original) Return the first ChildBoleto filtered by the valor_original column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorMulta(string $valor_multa) Return the first ChildBoleto filtered by the valor_multa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorJuros(string $valor_juros) Return the first ChildBoleto filtered by the valor_juros column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorDesconto(string $valor_desconto) Return the first ChildBoleto filtered by the valor_desconto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorPago(string $valor_pago) Return the first ChildBoleto filtered by the valor_pago column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByLinhaDigitavel(string $linha_digitavel) Return the first ChildBoleto filtered by the linha_digitavel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByDataPagamento(string $data_pagamento) Return the first ChildBoleto filtered by the data_pagamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByEstaPago(boolean $esta_pago) Return the first ChildBoleto filtered by the esta_pago column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorAbatimento(string $valor_abatimento) Return the first ChildBoleto filtered by the valor_abatimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorIof(string $valor_iof) Return the first ChildBoleto filtered by the valor_iof column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorLiquido(string $valor_liquido) Return the first ChildBoleto filtered by the valor_liquido column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorOutrasDespesas(string $valor_outras_despesas) Return the first ChildBoleto filtered by the valor_outras_despesas column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByValorOutrosCreditos(string $valor_outros_creditos) Return the first ChildBoleto filtered by the valor_outros_creditos column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByNotaScmGerada(boolean $nota_scm_gerada) Return the first ChildBoleto filtered by the nota_scm_gerada column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByNotaScmCancelada(boolean $nota_scm_cancelada) Return the first ChildBoleto filtered by the nota_scm_cancelada column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByBaseCalculoScm(string $base_calculo_scm) Return the first ChildBoleto filtered by the base_calculo_scm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByBaseCalculoSva(string $base_calculo_sva) Return the first ChildBoleto filtered by the base_calculo_sva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByNotaSvaGerada(boolean $nota_sva_gerada) Return the first ChildBoleto filtered by the nota_sva_gerada column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByNotaSvaCancelada(boolean $nota_sva_cancelada) Return the first ChildBoleto filtered by the nota_sva_cancelada column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByTipoConta(string $tipo_conta) Return the first ChildBoleto filtered by the tipo_conta column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByBaixaId(int $baixa_id) Return the first ChildBoleto filtered by the baixa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByBaixaEstornoId(int $baixa_estorno_id) Return the first ChildBoleto filtered by the baixa_estorno_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoleto requireOneByFornecedorId(int $fornecedor_id) Return the first ChildBoleto filtered by the fornecedor_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBoleto[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBoleto objects based on current ModelCriteria
 * @method     ChildBoleto[]|ObjectCollection findByIdboleto(int $idboleto) Return ChildBoleto objects filtered by the idboleto column
 * @method     ChildBoleto[]|ObjectCollection findByValor(double $valor) Return ChildBoleto objects filtered by the valor column
 * @method     ChildBoleto[]|ObjectCollection findByVencimento(string $vencimento) Return ChildBoleto objects filtered by the vencimento column
 * @method     ChildBoleto[]|ObjectCollection findByClienteId(int $cliente_id) Return ChildBoleto objects filtered by the cliente_id column
 * @method     ChildBoleto[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildBoleto objects filtered by the data_cadastro column
 * @method     ChildBoleto[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildBoleto objects filtered by the data_alterado column
 * @method     ChildBoleto[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildBoleto objects filtered by the usuario_alterado column
 * @method     ChildBoleto[]|ObjectCollection findByImportId(int $import_id) Return ChildBoleto objects filtered by the import_id column
 * @method     ChildBoleto[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildBoleto objects filtered by the competencia_id column
 * @method     ChildBoleto[]|ObjectCollection findByNossoNumero(string $nosso_numero) Return ChildBoleto objects filtered by the nosso_numero column
 * @method     ChildBoleto[]|ObjectCollection findByNumeroDocumento(string $numero_documento) Return ChildBoleto objects filtered by the numero_documento column
 * @method     ChildBoleto[]|ObjectCollection findByRegistrado(boolean $registrado) Return ChildBoleto objects filtered by the registrado column
 * @method     ChildBoleto[]|ObjectCollection findByValorOriginal(string $valor_original) Return ChildBoleto objects filtered by the valor_original column
 * @method     ChildBoleto[]|ObjectCollection findByValorMulta(string $valor_multa) Return ChildBoleto objects filtered by the valor_multa column
 * @method     ChildBoleto[]|ObjectCollection findByValorJuros(string $valor_juros) Return ChildBoleto objects filtered by the valor_juros column
 * @method     ChildBoleto[]|ObjectCollection findByValorDesconto(string $valor_desconto) Return ChildBoleto objects filtered by the valor_desconto column
 * @method     ChildBoleto[]|ObjectCollection findByValorPago(string $valor_pago) Return ChildBoleto objects filtered by the valor_pago column
 * @method     ChildBoleto[]|ObjectCollection findByLinhaDigitavel(string $linha_digitavel) Return ChildBoleto objects filtered by the linha_digitavel column
 * @method     ChildBoleto[]|ObjectCollection findByDataPagamento(string $data_pagamento) Return ChildBoleto objects filtered by the data_pagamento column
 * @method     ChildBoleto[]|ObjectCollection findByEstaPago(boolean $esta_pago) Return ChildBoleto objects filtered by the esta_pago column
 * @method     ChildBoleto[]|ObjectCollection findByValorAbatimento(string $valor_abatimento) Return ChildBoleto objects filtered by the valor_abatimento column
 * @method     ChildBoleto[]|ObjectCollection findByValorIof(string $valor_iof) Return ChildBoleto objects filtered by the valor_iof column
 * @method     ChildBoleto[]|ObjectCollection findByValorLiquido(string $valor_liquido) Return ChildBoleto objects filtered by the valor_liquido column
 * @method     ChildBoleto[]|ObjectCollection findByValorOutrasDespesas(string $valor_outras_despesas) Return ChildBoleto objects filtered by the valor_outras_despesas column
 * @method     ChildBoleto[]|ObjectCollection findByValorOutrosCreditos(string $valor_outros_creditos) Return ChildBoleto objects filtered by the valor_outros_creditos column
 * @method     ChildBoleto[]|ObjectCollection findByNotaScmGerada(boolean $nota_scm_gerada) Return ChildBoleto objects filtered by the nota_scm_gerada column
 * @method     ChildBoleto[]|ObjectCollection findByNotaScmCancelada(boolean $nota_scm_cancelada) Return ChildBoleto objects filtered by the nota_scm_cancelada column
 * @method     ChildBoleto[]|ObjectCollection findByBaseCalculoScm(string $base_calculo_scm) Return ChildBoleto objects filtered by the base_calculo_scm column
 * @method     ChildBoleto[]|ObjectCollection findByBaseCalculoSva(string $base_calculo_sva) Return ChildBoleto objects filtered by the base_calculo_sva column
 * @method     ChildBoleto[]|ObjectCollection findByNotaSvaGerada(boolean $nota_sva_gerada) Return ChildBoleto objects filtered by the nota_sva_gerada column
 * @method     ChildBoleto[]|ObjectCollection findByNotaSvaCancelada(boolean $nota_sva_cancelada) Return ChildBoleto objects filtered by the nota_sva_cancelada column
 * @method     ChildBoleto[]|ObjectCollection findByTipoConta(string $tipo_conta) Return ChildBoleto objects filtered by the tipo_conta column
 * @method     ChildBoleto[]|ObjectCollection findByBaixaId(int $baixa_id) Return ChildBoleto objects filtered by the baixa_id column
 * @method     ChildBoleto[]|ObjectCollection findByBaixaEstornoId(int $baixa_estorno_id) Return ChildBoleto objects filtered by the baixa_estorno_id column
 * @method     ChildBoleto[]|ObjectCollection findByFornecedorId(int $fornecedor_id) Return ChildBoleto objects filtered by the fornecedor_id column
 * @method     ChildBoleto[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BoletoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\BoletoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Boleto', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBoletoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBoletoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBoletoQuery) {
            return $criteria;
        }
        $query = new ChildBoletoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBoleto|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BoletoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BoletoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoleto A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idboleto, valor, vencimento, cliente_id, data_cadastro, data_alterado, usuario_alterado, import_id, competencia_id, nosso_numero, numero_documento, registrado, valor_original, valor_multa, valor_juros, valor_desconto, valor_pago, linha_digitavel, data_pagamento, esta_pago, valor_abatimento, valor_iof, valor_liquido, valor_outras_despesas, valor_outros_creditos, nota_scm_gerada, nota_scm_cancelada, base_calculo_scm, base_calculo_sva, nota_sva_gerada, nota_sva_cancelada, tipo_conta, baixa_id, baixa_estorno_id, fornecedor_id FROM boleto WHERE idboleto = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBoleto $obj */
            $obj = new ChildBoleto();
            $obj->hydrate($row);
            BoletoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBoleto|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idboleto column
     *
     * Example usage:
     * <code>
     * $query->filterByIdboleto(1234); // WHERE idboleto = 1234
     * $query->filterByIdboleto(array(12, 34)); // WHERE idboleto IN (12, 34)
     * $query->filterByIdboleto(array('min' => 12)); // WHERE idboleto > 12
     * </code>
     *
     * @param     mixed $idboleto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByIdboleto($idboleto = null, $comparison = null)
    {
        if (is_array($idboleto)) {
            $useMinMax = false;
            if (isset($idboleto['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $idboleto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idboleto['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $idboleto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $idboleto, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query on the vencimento column
     *
     * Example usage:
     * <code>
     * $query->filterByVencimento('2011-03-14'); // WHERE vencimento = '2011-03-14'
     * $query->filterByVencimento('now'); // WHERE vencimento = '2011-03-14'
     * $query->filterByVencimento(array('max' => 'yesterday')); // WHERE vencimento > '2011-03-13'
     * </code>
     *
     * @param     mixed $vencimento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByVencimento($vencimento = null, $comparison = null)
    {
        if (is_array($vencimento)) {
            $useMinMax = false;
            if (isset($vencimento['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VENCIMENTO, $vencimento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vencimento['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VENCIMENTO, $vencimento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VENCIMENTO, $vencimento, $comparison);
    }

    /**
     * Filter the query on the cliente_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClienteId(1234); // WHERE cliente_id = 1234
     * $query->filterByClienteId(array(12, 34)); // WHERE cliente_id IN (12, 34)
     * $query->filterByClienteId(array('min' => 12)); // WHERE cliente_id > 12
     * </code>
     *
     * @see       filterByCliente()
     *
     * @param     mixed $clienteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByClienteId($clienteId = null, $comparison = null)
    {
        if (is_array($clienteId)) {
            $useMinMax = false;
            if (isset($clienteId['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_CLIENTE_ID, $clienteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clienteId['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_CLIENTE_ID, $clienteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_CLIENTE_ID, $clienteId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the import_id column
     *
     * Example usage:
     * <code>
     * $query->filterByImportId(1234); // WHERE import_id = 1234
     * $query->filterByImportId(array(12, 34)); // WHERE import_id IN (12, 34)
     * $query->filterByImportId(array('min' => 12)); // WHERE import_id > 12
     * </code>
     *
     * @param     mixed $importId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByImportId($importId = null, $comparison = null)
    {
        if (is_array($importId)) {
            $useMinMax = false;
            if (isset($importId['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_IMPORT_ID, $importId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importId['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_IMPORT_ID, $importId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_IMPORT_ID, $importId, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @see       filterByCompetencia()
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the nosso_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByNossoNumero('fooValue');   // WHERE nosso_numero = 'fooValue'
     * $query->filterByNossoNumero('%fooValue%', Criteria::LIKE); // WHERE nosso_numero LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nossoNumero The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByNossoNumero($nossoNumero = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nossoNumero)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_NOSSO_NUMERO, $nossoNumero, $comparison);
    }

    /**
     * Filter the query on the numero_documento column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroDocumento('fooValue');   // WHERE numero_documento = 'fooValue'
     * $query->filterByNumeroDocumento('%fooValue%', Criteria::LIKE); // WHERE numero_documento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numeroDocumento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByNumeroDocumento($numeroDocumento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numeroDocumento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_NUMERO_DOCUMENTO, $numeroDocumento, $comparison);
    }

    /**
     * Filter the query on the registrado column
     *
     * Example usage:
     * <code>
     * $query->filterByRegistrado(true); // WHERE registrado = true
     * $query->filterByRegistrado('yes'); // WHERE registrado = true
     * </code>
     *
     * @param     boolean|string $registrado The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByRegistrado($registrado = null, $comparison = null)
    {
        if (is_string($registrado)) {
            $registrado = in_array(strtolower($registrado), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BoletoTableMap::COL_REGISTRADO, $registrado, $comparison);
    }

    /**
     * Filter the query on the valor_original column
     *
     * Example usage:
     * <code>
     * $query->filterByValorOriginal(1234); // WHERE valor_original = 1234
     * $query->filterByValorOriginal(array(12, 34)); // WHERE valor_original IN (12, 34)
     * $query->filterByValorOriginal(array('min' => 12)); // WHERE valor_original > 12
     * </code>
     *
     * @param     mixed $valorOriginal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorOriginal($valorOriginal = null, $comparison = null)
    {
        if (is_array($valorOriginal)) {
            $useMinMax = false;
            if (isset($valorOriginal['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_ORIGINAL, $valorOriginal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorOriginal['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_ORIGINAL, $valorOriginal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_ORIGINAL, $valorOriginal, $comparison);
    }

    /**
     * Filter the query on the valor_multa column
     *
     * Example usage:
     * <code>
     * $query->filterByValorMulta(1234); // WHERE valor_multa = 1234
     * $query->filterByValorMulta(array(12, 34)); // WHERE valor_multa IN (12, 34)
     * $query->filterByValorMulta(array('min' => 12)); // WHERE valor_multa > 12
     * </code>
     *
     * @param     mixed $valorMulta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorMulta($valorMulta = null, $comparison = null)
    {
        if (is_array($valorMulta)) {
            $useMinMax = false;
            if (isset($valorMulta['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_MULTA, $valorMulta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorMulta['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_MULTA, $valorMulta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_MULTA, $valorMulta, $comparison);
    }

    /**
     * Filter the query on the valor_juros column
     *
     * Example usage:
     * <code>
     * $query->filterByValorJuros(1234); // WHERE valor_juros = 1234
     * $query->filterByValorJuros(array(12, 34)); // WHERE valor_juros IN (12, 34)
     * $query->filterByValorJuros(array('min' => 12)); // WHERE valor_juros > 12
     * </code>
     *
     * @param     mixed $valorJuros The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorJuros($valorJuros = null, $comparison = null)
    {
        if (is_array($valorJuros)) {
            $useMinMax = false;
            if (isset($valorJuros['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_JUROS, $valorJuros['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorJuros['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_JUROS, $valorJuros['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_JUROS, $valorJuros, $comparison);
    }

    /**
     * Filter the query on the valor_desconto column
     *
     * Example usage:
     * <code>
     * $query->filterByValorDesconto(1234); // WHERE valor_desconto = 1234
     * $query->filterByValorDesconto(array(12, 34)); // WHERE valor_desconto IN (12, 34)
     * $query->filterByValorDesconto(array('min' => 12)); // WHERE valor_desconto > 12
     * </code>
     *
     * @param     mixed $valorDesconto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorDesconto($valorDesconto = null, $comparison = null)
    {
        if (is_array($valorDesconto)) {
            $useMinMax = false;
            if (isset($valorDesconto['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_DESCONTO, $valorDesconto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorDesconto['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_DESCONTO, $valorDesconto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_DESCONTO, $valorDesconto, $comparison);
    }

    /**
     * Filter the query on the valor_pago column
     *
     * Example usage:
     * <code>
     * $query->filterByValorPago(1234); // WHERE valor_pago = 1234
     * $query->filterByValorPago(array(12, 34)); // WHERE valor_pago IN (12, 34)
     * $query->filterByValorPago(array('min' => 12)); // WHERE valor_pago > 12
     * </code>
     *
     * @param     mixed $valorPago The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorPago($valorPago = null, $comparison = null)
    {
        if (is_array($valorPago)) {
            $useMinMax = false;
            if (isset($valorPago['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_PAGO, $valorPago['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorPago['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_PAGO, $valorPago['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_PAGO, $valorPago, $comparison);
    }

    /**
     * Filter the query on the linha_digitavel column
     *
     * Example usage:
     * <code>
     * $query->filterByLinhaDigitavel('fooValue');   // WHERE linha_digitavel = 'fooValue'
     * $query->filterByLinhaDigitavel('%fooValue%', Criteria::LIKE); // WHERE linha_digitavel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $linhaDigitavel The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByLinhaDigitavel($linhaDigitavel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($linhaDigitavel)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_LINHA_DIGITAVEL, $linhaDigitavel, $comparison);
    }

    /**
     * Filter the query on the data_pagamento column
     *
     * Example usage:
     * <code>
     * $query->filterByDataPagamento('2011-03-14'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByDataPagamento('now'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByDataPagamento(array('max' => 'yesterday')); // WHERE data_pagamento > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataPagamento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByDataPagamento($dataPagamento = null, $comparison = null)
    {
        if (is_array($dataPagamento)) {
            $useMinMax = false;
            if (isset($dataPagamento['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_DATA_PAGAMENTO, $dataPagamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataPagamento['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_DATA_PAGAMENTO, $dataPagamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_DATA_PAGAMENTO, $dataPagamento, $comparison);
    }

    /**
     * Filter the query on the esta_pago column
     *
     * Example usage:
     * <code>
     * $query->filterByEstaPago(true); // WHERE esta_pago = true
     * $query->filterByEstaPago('yes'); // WHERE esta_pago = true
     * </code>
     *
     * @param     boolean|string $estaPago The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByEstaPago($estaPago = null, $comparison = null)
    {
        if (is_string($estaPago)) {
            $estaPago = in_array(strtolower($estaPago), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BoletoTableMap::COL_ESTA_PAGO, $estaPago, $comparison);
    }

    /**
     * Filter the query on the valor_abatimento column
     *
     * Example usage:
     * <code>
     * $query->filterByValorAbatimento(1234); // WHERE valor_abatimento = 1234
     * $query->filterByValorAbatimento(array(12, 34)); // WHERE valor_abatimento IN (12, 34)
     * $query->filterByValorAbatimento(array('min' => 12)); // WHERE valor_abatimento > 12
     * </code>
     *
     * @param     mixed $valorAbatimento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorAbatimento($valorAbatimento = null, $comparison = null)
    {
        if (is_array($valorAbatimento)) {
            $useMinMax = false;
            if (isset($valorAbatimento['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_ABATIMENTO, $valorAbatimento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorAbatimento['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_ABATIMENTO, $valorAbatimento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_ABATIMENTO, $valorAbatimento, $comparison);
    }

    /**
     * Filter the query on the valor_iof column
     *
     * Example usage:
     * <code>
     * $query->filterByValorIof(1234); // WHERE valor_iof = 1234
     * $query->filterByValorIof(array(12, 34)); // WHERE valor_iof IN (12, 34)
     * $query->filterByValorIof(array('min' => 12)); // WHERE valor_iof > 12
     * </code>
     *
     * @param     mixed $valorIof The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorIof($valorIof = null, $comparison = null)
    {
        if (is_array($valorIof)) {
            $useMinMax = false;
            if (isset($valorIof['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_IOF, $valorIof['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorIof['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_IOF, $valorIof['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_IOF, $valorIof, $comparison);
    }

    /**
     * Filter the query on the valor_liquido column
     *
     * Example usage:
     * <code>
     * $query->filterByValorLiquido(1234); // WHERE valor_liquido = 1234
     * $query->filterByValorLiquido(array(12, 34)); // WHERE valor_liquido IN (12, 34)
     * $query->filterByValorLiquido(array('min' => 12)); // WHERE valor_liquido > 12
     * </code>
     *
     * @param     mixed $valorLiquido The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorLiquido($valorLiquido = null, $comparison = null)
    {
        if (is_array($valorLiquido)) {
            $useMinMax = false;
            if (isset($valorLiquido['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_LIQUIDO, $valorLiquido['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorLiquido['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_LIQUIDO, $valorLiquido['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_LIQUIDO, $valorLiquido, $comparison);
    }

    /**
     * Filter the query on the valor_outras_despesas column
     *
     * Example usage:
     * <code>
     * $query->filterByValorOutrasDespesas(1234); // WHERE valor_outras_despesas = 1234
     * $query->filterByValorOutrasDespesas(array(12, 34)); // WHERE valor_outras_despesas IN (12, 34)
     * $query->filterByValorOutrasDespesas(array('min' => 12)); // WHERE valor_outras_despesas > 12
     * </code>
     *
     * @param     mixed $valorOutrasDespesas The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorOutrasDespesas($valorOutrasDespesas = null, $comparison = null)
    {
        if (is_array($valorOutrasDespesas)) {
            $useMinMax = false;
            if (isset($valorOutrasDespesas['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS, $valorOutrasDespesas['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorOutrasDespesas['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS, $valorOutrasDespesas['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS, $valorOutrasDespesas, $comparison);
    }

    /**
     * Filter the query on the valor_outros_creditos column
     *
     * Example usage:
     * <code>
     * $query->filterByValorOutrosCreditos(1234); // WHERE valor_outros_creditos = 1234
     * $query->filterByValorOutrosCreditos(array(12, 34)); // WHERE valor_outros_creditos IN (12, 34)
     * $query->filterByValorOutrosCreditos(array('min' => 12)); // WHERE valor_outros_creditos > 12
     * </code>
     *
     * @param     mixed $valorOutrosCreditos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByValorOutrosCreditos($valorOutrosCreditos = null, $comparison = null)
    {
        if (is_array($valorOutrosCreditos)) {
            $useMinMax = false;
            if (isset($valorOutrosCreditos['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_OUTROS_CREDITOS, $valorOutrosCreditos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorOutrosCreditos['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_VALOR_OUTROS_CREDITOS, $valorOutrosCreditos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_VALOR_OUTROS_CREDITOS, $valorOutrosCreditos, $comparison);
    }

    /**
     * Filter the query on the nota_scm_gerada column
     *
     * Example usage:
     * <code>
     * $query->filterByNotaScmGerada(true); // WHERE nota_scm_gerada = true
     * $query->filterByNotaScmGerada('yes'); // WHERE nota_scm_gerada = true
     * </code>
     *
     * @param     boolean|string $notaScmGerada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByNotaScmGerada($notaScmGerada = null, $comparison = null)
    {
        if (is_string($notaScmGerada)) {
            $notaScmGerada = in_array(strtolower($notaScmGerada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BoletoTableMap::COL_NOTA_SCM_GERADA, $notaScmGerada, $comparison);
    }

    /**
     * Filter the query on the nota_scm_cancelada column
     *
     * Example usage:
     * <code>
     * $query->filterByNotaScmCancelada(true); // WHERE nota_scm_cancelada = true
     * $query->filterByNotaScmCancelada('yes'); // WHERE nota_scm_cancelada = true
     * </code>
     *
     * @param     boolean|string $notaScmCancelada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByNotaScmCancelada($notaScmCancelada = null, $comparison = null)
    {
        if (is_string($notaScmCancelada)) {
            $notaScmCancelada = in_array(strtolower($notaScmCancelada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BoletoTableMap::COL_NOTA_SCM_CANCELADA, $notaScmCancelada, $comparison);
    }

    /**
     * Filter the query on the base_calculo_scm column
     *
     * Example usage:
     * <code>
     * $query->filterByBaseCalculoScm(1234); // WHERE base_calculo_scm = 1234
     * $query->filterByBaseCalculoScm(array(12, 34)); // WHERE base_calculo_scm IN (12, 34)
     * $query->filterByBaseCalculoScm(array('min' => 12)); // WHERE base_calculo_scm > 12
     * </code>
     *
     * @param     mixed $baseCalculoScm The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByBaseCalculoScm($baseCalculoScm = null, $comparison = null)
    {
        if (is_array($baseCalculoScm)) {
            $useMinMax = false;
            if (isset($baseCalculoScm['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_BASE_CALCULO_SCM, $baseCalculoScm['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baseCalculoScm['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_BASE_CALCULO_SCM, $baseCalculoScm['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_BASE_CALCULO_SCM, $baseCalculoScm, $comparison);
    }

    /**
     * Filter the query on the base_calculo_sva column
     *
     * Example usage:
     * <code>
     * $query->filterByBaseCalculoSva(1234); // WHERE base_calculo_sva = 1234
     * $query->filterByBaseCalculoSva(array(12, 34)); // WHERE base_calculo_sva IN (12, 34)
     * $query->filterByBaseCalculoSva(array('min' => 12)); // WHERE base_calculo_sva > 12
     * </code>
     *
     * @param     mixed $baseCalculoSva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByBaseCalculoSva($baseCalculoSva = null, $comparison = null)
    {
        if (is_array($baseCalculoSva)) {
            $useMinMax = false;
            if (isset($baseCalculoSva['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_BASE_CALCULO_SVA, $baseCalculoSva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baseCalculoSva['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_BASE_CALCULO_SVA, $baseCalculoSva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_BASE_CALCULO_SVA, $baseCalculoSva, $comparison);
    }

    /**
     * Filter the query on the nota_sva_gerada column
     *
     * Example usage:
     * <code>
     * $query->filterByNotaSvaGerada(true); // WHERE nota_sva_gerada = true
     * $query->filterByNotaSvaGerada('yes'); // WHERE nota_sva_gerada = true
     * </code>
     *
     * @param     boolean|string $notaSvaGerada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByNotaSvaGerada($notaSvaGerada = null, $comparison = null)
    {
        if (is_string($notaSvaGerada)) {
            $notaSvaGerada = in_array(strtolower($notaSvaGerada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BoletoTableMap::COL_NOTA_SVA_GERADA, $notaSvaGerada, $comparison);
    }

    /**
     * Filter the query on the nota_sva_cancelada column
     *
     * Example usage:
     * <code>
     * $query->filterByNotaSvaCancelada(true); // WHERE nota_sva_cancelada = true
     * $query->filterByNotaSvaCancelada('yes'); // WHERE nota_sva_cancelada = true
     * </code>
     *
     * @param     boolean|string $notaSvaCancelada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByNotaSvaCancelada($notaSvaCancelada = null, $comparison = null)
    {
        if (is_string($notaSvaCancelada)) {
            $notaSvaCancelada = in_array(strtolower($notaSvaCancelada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(BoletoTableMap::COL_NOTA_SVA_CANCELADA, $notaSvaCancelada, $comparison);
    }

    /**
     * Filter the query on the tipo_conta column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoConta('fooValue');   // WHERE tipo_conta = 'fooValue'
     * $query->filterByTipoConta('%fooValue%', Criteria::LIKE); // WHERE tipo_conta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoConta The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByTipoConta($tipoConta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoConta)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_TIPO_CONTA, $tipoConta, $comparison);
    }

    /**
     * Filter the query on the baixa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBaixaId(1234); // WHERE baixa_id = 1234
     * $query->filterByBaixaId(array(12, 34)); // WHERE baixa_id IN (12, 34)
     * $query->filterByBaixaId(array('min' => 12)); // WHERE baixa_id > 12
     * </code>
     *
     * @see       filterByBaixa()
     *
     * @param     mixed $baixaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByBaixaId($baixaId = null, $comparison = null)
    {
        if (is_array($baixaId)) {
            $useMinMax = false;
            if (isset($baixaId['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_BAIXA_ID, $baixaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baixaId['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_BAIXA_ID, $baixaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_BAIXA_ID, $baixaId, $comparison);
    }

    /**
     * Filter the query on the baixa_estorno_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBaixaEstornoId(1234); // WHERE baixa_estorno_id = 1234
     * $query->filterByBaixaEstornoId(array(12, 34)); // WHERE baixa_estorno_id IN (12, 34)
     * $query->filterByBaixaEstornoId(array('min' => 12)); // WHERE baixa_estorno_id > 12
     * </code>
     *
     * @see       filterByBaixaEstorno()
     *
     * @param     mixed $baixaEstornoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByBaixaEstornoId($baixaEstornoId = null, $comparison = null)
    {
        if (is_array($baixaEstornoId)) {
            $useMinMax = false;
            if (isset($baixaEstornoId['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstornoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baixaEstornoId['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstornoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstornoId, $comparison);
    }

    /**
     * Filter the query on the fornecedor_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFornecedorId(1234); // WHERE fornecedor_id = 1234
     * $query->filterByFornecedorId(array(12, 34)); // WHERE fornecedor_id IN (12, 34)
     * $query->filterByFornecedorId(array('min' => 12)); // WHERE fornecedor_id > 12
     * </code>
     *
     * @see       filterByFornecedor()
     *
     * @param     mixed $fornecedorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByFornecedorId($fornecedorId = null, $comparison = null)
    {
        if (is_array($fornecedorId)) {
            $useMinMax = false;
            if (isset($fornecedorId['min'])) {
                $this->addUsingAlias(BoletoTableMap::COL_FORNECEDOR_ID, $fornecedorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fornecedorId['max'])) {
                $this->addUsingAlias(BoletoTableMap::COL_FORNECEDOR_ID, $fornecedorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoTableMap::COL_FORNECEDOR_ID, $fornecedorId, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BaixaEstorno object
     *
     * @param \ImaTelecomBundle\Model\BaixaEstorno|ObjectCollection $baixaEstorno The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByBaixaEstorno($baixaEstorno, $comparison = null)
    {
        if ($baixaEstorno instanceof \ImaTelecomBundle\Model\BaixaEstorno) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstorno->getIdbaixaEstorno(), $comparison);
        } elseif ($baixaEstorno instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoTableMap::COL_BAIXA_ESTORNO_ID, $baixaEstorno->toKeyValue('PrimaryKey', 'IdbaixaEstorno'), $comparison);
        } else {
            throw new PropelException('filterByBaixaEstorno() only accepts arguments of type \ImaTelecomBundle\Model\BaixaEstorno or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaixaEstorno relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinBaixaEstorno($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaixaEstorno');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaixaEstorno');
        }

        return $this;
    }

    /**
     * Use the BaixaEstorno relation BaixaEstorno object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaEstornoQuery A secondary query class using the current class as primary query
     */
    public function useBaixaEstornoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBaixaEstorno($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaixaEstorno', '\ImaTelecomBundle\Model\BaixaEstornoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Baixa object
     *
     * @param \ImaTelecomBundle\Model\Baixa|ObjectCollection $baixa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByBaixa($baixa, $comparison = null)
    {
        if ($baixa instanceof \ImaTelecomBundle\Model\Baixa) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_BAIXA_ID, $baixa->getIdbaixa(), $comparison);
        } elseif ($baixa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoTableMap::COL_BAIXA_ID, $baixa->toKeyValue('PrimaryKey', 'Idbaixa'), $comparison);
        } else {
            throw new PropelException('filterByBaixa() only accepts arguments of type \ImaTelecomBundle\Model\Baixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Baixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinBaixa($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Baixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Baixa');
        }

        return $this;
    }

    /**
     * Use the Baixa relation Baixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaQuery A secondary query class using the current class as primary query
     */
    public function useBaixaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Baixa', '\ImaTelecomBundle\Model\BaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cliente object
     *
     * @param \ImaTelecomBundle\Model\Cliente|ObjectCollection $cliente The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByCliente($cliente, $comparison = null)
    {
        if ($cliente instanceof \ImaTelecomBundle\Model\Cliente) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_CLIENTE_ID, $cliente->getIdcliente(), $comparison);
        } elseif ($cliente instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoTableMap::COL_CLIENTE_ID, $cliente->toKeyValue('PrimaryKey', 'Idcliente'), $comparison);
        } else {
            throw new PropelException('filterByCliente() only accepts arguments of type \ImaTelecomBundle\Model\Cliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cliente');
        }

        return $this;
    }

    /**
     * Use the Cliente relation Cliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteQuery A secondary query class using the current class as primary query
     */
    public function useClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cliente', '\ImaTelecomBundle\Model\ClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Competencia object
     *
     * @param \ImaTelecomBundle\Model\Competencia|ObjectCollection $competencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByCompetencia($competencia, $comparison = null)
    {
        if ($competencia instanceof \ImaTelecomBundle\Model\Competencia) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_COMPETENCIA_ID, $competencia->getId(), $comparison);
        } elseif ($competencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoTableMap::COL_COMPETENCIA_ID, $competencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompetencia() only accepts arguments of type \ImaTelecomBundle\Model\Competencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Competencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinCompetencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Competencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Competencia');
        }

        return $this;
    }

    /**
     * Use the Competencia relation Competencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CompetenciaQuery A secondary query class using the current class as primary query
     */
    public function useCompetenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompetencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Competencia', '\ImaTelecomBundle\Model\CompetenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Fornecedor object
     *
     * @param \ImaTelecomBundle\Model\Fornecedor|ObjectCollection $fornecedor The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByFornecedor($fornecedor, $comparison = null)
    {
        if ($fornecedor instanceof \ImaTelecomBundle\Model\Fornecedor) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_FORNECEDOR_ID, $fornecedor->getIdfornecedor(), $comparison);
        } elseif ($fornecedor instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoTableMap::COL_FORNECEDOR_ID, $fornecedor->toKeyValue('PrimaryKey', 'Idfornecedor'), $comparison);
        } else {
            throw new PropelException('filterByFornecedor() only accepts arguments of type \ImaTelecomBundle\Model\Fornecedor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fornecedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinFornecedor($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fornecedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fornecedor');
        }

        return $this;
    }

    /**
     * Use the Fornecedor relation Fornecedor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FornecedorQuery A secondary query class using the current class as primary query
     */
    public function useFornecedorQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFornecedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fornecedor', '\ImaTelecomBundle\Model\FornecedorQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoBaixaHistorico object
     *
     * @param \ImaTelecomBundle\Model\BoletoBaixaHistorico|ObjectCollection $boletoBaixaHistorico the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByBoletoBaixaHistorico($boletoBaixaHistorico, $comparison = null)
    {
        if ($boletoBaixaHistorico instanceof \ImaTelecomBundle\Model\BoletoBaixaHistorico) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $boletoBaixaHistorico->getBoletoId(), $comparison);
        } elseif ($boletoBaixaHistorico instanceof ObjectCollection) {
            return $this
                ->useBoletoBaixaHistoricoQuery()
                ->filterByPrimaryKeys($boletoBaixaHistorico->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoBaixaHistorico() only accepts arguments of type \ImaTelecomBundle\Model\BoletoBaixaHistorico or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoBaixaHistorico relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinBoletoBaixaHistorico($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoBaixaHistorico');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoBaixaHistorico');
        }

        return $this;
    }

    /**
     * Use the BoletoBaixaHistorico relation BoletoBaixaHistorico object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoBaixaHistoricoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoletoBaixaHistorico($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoBaixaHistorico', '\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoItem object
     *
     * @param \ImaTelecomBundle\Model\BoletoItem|ObjectCollection $boletoItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByBoletoItem($boletoItem, $comparison = null)
    {
        if ($boletoItem instanceof \ImaTelecomBundle\Model\BoletoItem) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $boletoItem->getBoletoId(), $comparison);
        } elseif ($boletoItem instanceof ObjectCollection) {
            return $this
                ->useBoletoItemQuery()
                ->filterByPrimaryKeys($boletoItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoItem() only accepts arguments of type \ImaTelecomBundle\Model\BoletoItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinBoletoItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoItem');
        }

        return $this;
    }

    /**
     * Use the BoletoItem relation BoletoItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoItemQuery A secondary query class using the current class as primary query
     */
    public function useBoletoItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoletoItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoItem', '\ImaTelecomBundle\Model\BoletoItemQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\DadosFiscal object
     *
     * @param \ImaTelecomBundle\Model\DadosFiscal|ObjectCollection $dadosFiscal the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByDadosFiscal($dadosFiscal, $comparison = null)
    {
        if ($dadosFiscal instanceof \ImaTelecomBundle\Model\DadosFiscal) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $dadosFiscal->getBoletoId(), $comparison);
        } elseif ($dadosFiscal instanceof ObjectCollection) {
            return $this
                ->useDadosFiscalQuery()
                ->filterByPrimaryKeys($dadosFiscal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDadosFiscal() only accepts arguments of type \ImaTelecomBundle\Model\DadosFiscal or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DadosFiscal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinDadosFiscal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DadosFiscal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DadosFiscal');
        }

        return $this;
    }

    /**
     * Use the DadosFiscal relation DadosFiscal object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\DadosFiscalQuery A secondary query class using the current class as primary query
     */
    public function useDadosFiscalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDadosFiscal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DadosFiscal', '\ImaTelecomBundle\Model\DadosFiscalQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\DadosFiscalItem object
     *
     * @param \ImaTelecomBundle\Model\DadosFiscalItem|ObjectCollection $dadosFiscalItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByDadosFiscalItem($dadosFiscalItem, $comparison = null)
    {
        if ($dadosFiscalItem instanceof \ImaTelecomBundle\Model\DadosFiscalItem) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $dadosFiscalItem->getBoletoId(), $comparison);
        } elseif ($dadosFiscalItem instanceof ObjectCollection) {
            return $this
                ->useDadosFiscalItemQuery()
                ->filterByPrimaryKeys($dadosFiscalItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDadosFiscalItem() only accepts arguments of type \ImaTelecomBundle\Model\DadosFiscalItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DadosFiscalItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinDadosFiscalItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DadosFiscalItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DadosFiscalItem');
        }

        return $this;
    }

    /**
     * Use the DadosFiscalItem relation DadosFiscalItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\DadosFiscalItemQuery A secondary query class using the current class as primary query
     */
    public function useDadosFiscalItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDadosFiscalItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DadosFiscalItem', '\ImaTelecomBundle\Model\DadosFiscalItemQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\LancamentosBoletos object
     *
     * @param \ImaTelecomBundle\Model\LancamentosBoletos|ObjectCollection $lancamentosBoletos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBoletoQuery The current query, for fluid interface
     */
    public function filterByLancamentosBoletos($lancamentosBoletos, $comparison = null)
    {
        if ($lancamentosBoletos instanceof \ImaTelecomBundle\Model\LancamentosBoletos) {
            return $this
                ->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $lancamentosBoletos->getBoletoId(), $comparison);
        } elseif ($lancamentosBoletos instanceof ObjectCollection) {
            return $this
                ->useLancamentosBoletosQuery()
                ->filterByPrimaryKeys($lancamentosBoletos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLancamentosBoletos() only accepts arguments of type \ImaTelecomBundle\Model\LancamentosBoletos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LancamentosBoletos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function joinLancamentosBoletos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LancamentosBoletos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LancamentosBoletos');
        }

        return $this;
    }

    /**
     * Use the LancamentosBoletos relation LancamentosBoletos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\LancamentosBoletosQuery A secondary query class using the current class as primary query
     */
    public function useLancamentosBoletosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLancamentosBoletos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LancamentosBoletos', '\ImaTelecomBundle\Model\LancamentosBoletosQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBoleto $boleto Object to remove from the list of results
     *
     * @return $this|ChildBoletoQuery The current query, for fluid interface
     */
    public function prune($boleto = null)
    {
        if ($boleto) {
            $this->addUsingAlias(BoletoTableMap::COL_IDBOLETO, $boleto->getIdboleto(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the boleto table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BoletoTableMap::clearInstancePool();
            BoletoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BoletoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BoletoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BoletoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BoletoQuery
