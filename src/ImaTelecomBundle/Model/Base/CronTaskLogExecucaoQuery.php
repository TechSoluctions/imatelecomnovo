<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\CronTaskLogExecucao as ChildCronTaskLogExecucao;
use ImaTelecomBundle\Model\CronTaskLogExecucaoQuery as ChildCronTaskLogExecucaoQuery;
use ImaTelecomBundle\Model\Map\CronTaskLogExecucaoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'cron_task_log_execucao' table.
 *
 *
 *
 * @method     ChildCronTaskLogExecucaoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCronTaskLogExecucaoQuery orderByArquivo($order = Criteria::ASC) Order by the arquivo column
 * @method     ChildCronTaskLogExecucaoQuery orderByDiretorioFilho($order = Criteria::ASC) Order by the diretorio_filho column
 * @method     ChildCronTaskLogExecucaoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildCronTaskLogExecucaoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildCronTaskLogExecucaoQuery orderByCronTaskId($order = Criteria::ASC) Order by the cron_task_id column
 * @method     ChildCronTaskLogExecucaoQuery orderByLogEmAndamento($order = Criteria::ASC) Order by the log_em_andamento column
 * @method     ChildCronTaskLogExecucaoQuery orderByComandoExecutadoShell($order = Criteria::ASC) Order by the comando_executado_shell column
 *
 * @method     ChildCronTaskLogExecucaoQuery groupById() Group by the id column
 * @method     ChildCronTaskLogExecucaoQuery groupByArquivo() Group by the arquivo column
 * @method     ChildCronTaskLogExecucaoQuery groupByDiretorioFilho() Group by the diretorio_filho column
 * @method     ChildCronTaskLogExecucaoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildCronTaskLogExecucaoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildCronTaskLogExecucaoQuery groupByCronTaskId() Group by the cron_task_id column
 * @method     ChildCronTaskLogExecucaoQuery groupByLogEmAndamento() Group by the log_em_andamento column
 * @method     ChildCronTaskLogExecucaoQuery groupByComandoExecutadoShell() Group by the comando_executado_shell column
 *
 * @method     ChildCronTaskLogExecucaoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCronTaskLogExecucaoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCronTaskLogExecucaoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCronTaskLogExecucaoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCronTaskLogExecucaoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCronTaskLogExecucaoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCronTaskLogExecucaoQuery leftJoinCronTask($relationAlias = null) Adds a LEFT JOIN clause to the query using the CronTask relation
 * @method     ChildCronTaskLogExecucaoQuery rightJoinCronTask($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CronTask relation
 * @method     ChildCronTaskLogExecucaoQuery innerJoinCronTask($relationAlias = null) Adds a INNER JOIN clause to the query using the CronTask relation
 *
 * @method     ChildCronTaskLogExecucaoQuery joinWithCronTask($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CronTask relation
 *
 * @method     ChildCronTaskLogExecucaoQuery leftJoinWithCronTask() Adds a LEFT JOIN clause and with to the query using the CronTask relation
 * @method     ChildCronTaskLogExecucaoQuery rightJoinWithCronTask() Adds a RIGHT JOIN clause and with to the query using the CronTask relation
 * @method     ChildCronTaskLogExecucaoQuery innerJoinWithCronTask() Adds a INNER JOIN clause and with to the query using the CronTask relation
 *
 * @method     \ImaTelecomBundle\Model\CronTaskQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCronTaskLogExecucao findOne(ConnectionInterface $con = null) Return the first ChildCronTaskLogExecucao matching the query
 * @method     ChildCronTaskLogExecucao findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCronTaskLogExecucao matching the query, or a new ChildCronTaskLogExecucao object populated from the query conditions when no match is found
 *
 * @method     ChildCronTaskLogExecucao findOneById(int $id) Return the first ChildCronTaskLogExecucao filtered by the id column
 * @method     ChildCronTaskLogExecucao findOneByArquivo(string $arquivo) Return the first ChildCronTaskLogExecucao filtered by the arquivo column
 * @method     ChildCronTaskLogExecucao findOneByDiretorioFilho(string $diretorio_filho) Return the first ChildCronTaskLogExecucao filtered by the diretorio_filho column
 * @method     ChildCronTaskLogExecucao findOneByDataCadastro(string $data_cadastro) Return the first ChildCronTaskLogExecucao filtered by the data_cadastro column
 * @method     ChildCronTaskLogExecucao findOneByDataAlterado(string $data_alterado) Return the first ChildCronTaskLogExecucao filtered by the data_alterado column
 * @method     ChildCronTaskLogExecucao findOneByCronTaskId(int $cron_task_id) Return the first ChildCronTaskLogExecucao filtered by the cron_task_id column
 * @method     ChildCronTaskLogExecucao findOneByLogEmAndamento(boolean $log_em_andamento) Return the first ChildCronTaskLogExecucao filtered by the log_em_andamento column
 * @method     ChildCronTaskLogExecucao findOneByComandoExecutadoShell(string $comando_executado_shell) Return the first ChildCronTaskLogExecucao filtered by the comando_executado_shell column *

 * @method     ChildCronTaskLogExecucao requirePk($key, ConnectionInterface $con = null) Return the ChildCronTaskLogExecucao by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTaskLogExecucao requireOne(ConnectionInterface $con = null) Return the first ChildCronTaskLogExecucao matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCronTaskLogExecucao requireOneById(int $id) Return the first ChildCronTaskLogExecucao filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTaskLogExecucao requireOneByArquivo(string $arquivo) Return the first ChildCronTaskLogExecucao filtered by the arquivo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTaskLogExecucao requireOneByDiretorioFilho(string $diretorio_filho) Return the first ChildCronTaskLogExecucao filtered by the diretorio_filho column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTaskLogExecucao requireOneByDataCadastro(string $data_cadastro) Return the first ChildCronTaskLogExecucao filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTaskLogExecucao requireOneByDataAlterado(string $data_alterado) Return the first ChildCronTaskLogExecucao filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTaskLogExecucao requireOneByCronTaskId(int $cron_task_id) Return the first ChildCronTaskLogExecucao filtered by the cron_task_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTaskLogExecucao requireOneByLogEmAndamento(boolean $log_em_andamento) Return the first ChildCronTaskLogExecucao filtered by the log_em_andamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTaskLogExecucao requireOneByComandoExecutadoShell(string $comando_executado_shell) Return the first ChildCronTaskLogExecucao filtered by the comando_executado_shell column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCronTaskLogExecucao[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCronTaskLogExecucao objects based on current ModelCriteria
 * @method     ChildCronTaskLogExecucao[]|ObjectCollection findById(int $id) Return ChildCronTaskLogExecucao objects filtered by the id column
 * @method     ChildCronTaskLogExecucao[]|ObjectCollection findByArquivo(string $arquivo) Return ChildCronTaskLogExecucao objects filtered by the arquivo column
 * @method     ChildCronTaskLogExecucao[]|ObjectCollection findByDiretorioFilho(string $diretorio_filho) Return ChildCronTaskLogExecucao objects filtered by the diretorio_filho column
 * @method     ChildCronTaskLogExecucao[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildCronTaskLogExecucao objects filtered by the data_cadastro column
 * @method     ChildCronTaskLogExecucao[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildCronTaskLogExecucao objects filtered by the data_alterado column
 * @method     ChildCronTaskLogExecucao[]|ObjectCollection findByCronTaskId(int $cron_task_id) Return ChildCronTaskLogExecucao objects filtered by the cron_task_id column
 * @method     ChildCronTaskLogExecucao[]|ObjectCollection findByLogEmAndamento(boolean $log_em_andamento) Return ChildCronTaskLogExecucao objects filtered by the log_em_andamento column
 * @method     ChildCronTaskLogExecucao[]|ObjectCollection findByComandoExecutadoShell(string $comando_executado_shell) Return ChildCronTaskLogExecucao objects filtered by the comando_executado_shell column
 * @method     ChildCronTaskLogExecucao[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CronTaskLogExecucaoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\CronTaskLogExecucaoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\CronTaskLogExecucao', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCronTaskLogExecucaoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCronTaskLogExecucaoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCronTaskLogExecucaoQuery) {
            return $criteria;
        }
        $query = new ChildCronTaskLogExecucaoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCronTaskLogExecucao|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CronTaskLogExecucaoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CronTaskLogExecucaoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCronTaskLogExecucao A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, arquivo, diretorio_filho, data_cadastro, data_alterado, cron_task_id, log_em_andamento, comando_executado_shell FROM cron_task_log_execucao WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCronTaskLogExecucao $obj */
            $obj = new ChildCronTaskLogExecucao();
            $obj->hydrate($row);
            CronTaskLogExecucaoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCronTaskLogExecucao|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the arquivo column
     *
     * Example usage:
     * <code>
     * $query->filterByArquivo('fooValue');   // WHERE arquivo = 'fooValue'
     * $query->filterByArquivo('%fooValue%', Criteria::LIKE); // WHERE arquivo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $arquivo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByArquivo($arquivo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($arquivo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_ARQUIVO, $arquivo, $comparison);
    }

    /**
     * Filter the query on the diretorio_filho column
     *
     * Example usage:
     * <code>
     * $query->filterByDiretorioFilho('fooValue');   // WHERE diretorio_filho = 'fooValue'
     * $query->filterByDiretorioFilho('%fooValue%', Criteria::LIKE); // WHERE diretorio_filho LIKE '%fooValue%'
     * </code>
     *
     * @param     string $diretorioFilho The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByDiretorioFilho($diretorioFilho = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($diretorioFilho)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_DIRETORIO_FILHO, $diretorioFilho, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the cron_task_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCronTaskId(1234); // WHERE cron_task_id = 1234
     * $query->filterByCronTaskId(array(12, 34)); // WHERE cron_task_id IN (12, 34)
     * $query->filterByCronTaskId(array('min' => 12)); // WHERE cron_task_id > 12
     * </code>
     *
     * @see       filterByCronTask()
     *
     * @param     mixed $cronTaskId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByCronTaskId($cronTaskId = null, $comparison = null)
    {
        if (is_array($cronTaskId)) {
            $useMinMax = false;
            if (isset($cronTaskId['min'])) {
                $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_CRON_TASK_ID, $cronTaskId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cronTaskId['max'])) {
                $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_CRON_TASK_ID, $cronTaskId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_CRON_TASK_ID, $cronTaskId, $comparison);
    }

    /**
     * Filter the query on the log_em_andamento column
     *
     * Example usage:
     * <code>
     * $query->filterByLogEmAndamento(true); // WHERE log_em_andamento = true
     * $query->filterByLogEmAndamento('yes'); // WHERE log_em_andamento = true
     * </code>
     *
     * @param     boolean|string $logEmAndamento The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByLogEmAndamento($logEmAndamento = null, $comparison = null)
    {
        if (is_string($logEmAndamento)) {
            $logEmAndamento = in_array(strtolower($logEmAndamento), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_LOG_EM_ANDAMENTO, $logEmAndamento, $comparison);
    }

    /**
     * Filter the query on the comando_executado_shell column
     *
     * Example usage:
     * <code>
     * $query->filterByComandoExecutadoShell('fooValue');   // WHERE comando_executado_shell = 'fooValue'
     * $query->filterByComandoExecutadoShell('%fooValue%', Criteria::LIKE); // WHERE comando_executado_shell LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comandoExecutadoShell The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByComandoExecutadoShell($comandoExecutadoShell = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comandoExecutadoShell)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_COMANDO_EXECUTADO_SHELL, $comandoExecutadoShell, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\CronTask object
     *
     * @param \ImaTelecomBundle\Model\CronTask|ObjectCollection $cronTask The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function filterByCronTask($cronTask, $comparison = null)
    {
        if ($cronTask instanceof \ImaTelecomBundle\Model\CronTask) {
            return $this
                ->addUsingAlias(CronTaskLogExecucaoTableMap::COL_CRON_TASK_ID, $cronTask->getId(), $comparison);
        } elseif ($cronTask instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CronTaskLogExecucaoTableMap::COL_CRON_TASK_ID, $cronTask->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCronTask() only accepts arguments of type \ImaTelecomBundle\Model\CronTask or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CronTask relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function joinCronTask($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CronTask');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CronTask');
        }

        return $this;
    }

    /**
     * Use the CronTask relation CronTask object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CronTaskQuery A secondary query class using the current class as primary query
     */
    public function useCronTaskQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCronTask($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CronTask', '\ImaTelecomBundle\Model\CronTaskQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCronTaskLogExecucao $cronTaskLogExecucao Object to remove from the list of results
     *
     * @return $this|ChildCronTaskLogExecucaoQuery The current query, for fluid interface
     */
    public function prune($cronTaskLogExecucao = null)
    {
        if ($cronTaskLogExecucao) {
            $this->addUsingAlias(CronTaskLogExecucaoTableMap::COL_ID, $cronTaskLogExecucao->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the cron_task_log_execucao table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskLogExecucaoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CronTaskLogExecucaoTableMap::clearInstancePool();
            CronTaskLogExecucaoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskLogExecucaoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CronTaskLogExecucaoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CronTaskLogExecucaoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CronTaskLogExecucaoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CronTaskLogExecucaoQuery
