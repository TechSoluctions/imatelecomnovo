<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ClienteEstoqueItem as ChildClienteEstoqueItem;
use ImaTelecomBundle\Model\ClienteEstoqueItemQuery as ChildClienteEstoqueItemQuery;
use ImaTelecomBundle\Model\Map\ClienteEstoqueItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'cliente_estoque_item' table.
 *
 *
 *
 * @method     ChildClienteEstoqueItemQuery orderByIdclienteEstoqueItem($order = Criteria::ASC) Order by the idcliente_estoque_item column
 * @method     ChildClienteEstoqueItemQuery orderByClienteId($order = Criteria::ASC) Order by the cliente_id column
 * @method     ChildClienteEstoqueItemQuery orderByEstoqueId($order = Criteria::ASC) Order by the estoque_id column
 * @method     ChildClienteEstoqueItemQuery orderByQtde($order = Criteria::ASC) Order by the qtde column
 * @method     ChildClienteEstoqueItemQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildClienteEstoqueItemQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildClienteEstoqueItemQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildClienteEstoqueItemQuery groupByIdclienteEstoqueItem() Group by the idcliente_estoque_item column
 * @method     ChildClienteEstoqueItemQuery groupByClienteId() Group by the cliente_id column
 * @method     ChildClienteEstoqueItemQuery groupByEstoqueId() Group by the estoque_id column
 * @method     ChildClienteEstoqueItemQuery groupByQtde() Group by the qtde column
 * @method     ChildClienteEstoqueItemQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildClienteEstoqueItemQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildClienteEstoqueItemQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildClienteEstoqueItemQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildClienteEstoqueItemQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildClienteEstoqueItemQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildClienteEstoqueItemQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildClienteEstoqueItemQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildClienteEstoqueItemQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildClienteEstoqueItemQuery leftJoinCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cliente relation
 * @method     ChildClienteEstoqueItemQuery rightJoinCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cliente relation
 * @method     ChildClienteEstoqueItemQuery innerJoinCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the Cliente relation
 *
 * @method     ChildClienteEstoqueItemQuery joinWithCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cliente relation
 *
 * @method     ChildClienteEstoqueItemQuery leftJoinWithCliente() Adds a LEFT JOIN clause and with to the query using the Cliente relation
 * @method     ChildClienteEstoqueItemQuery rightJoinWithCliente() Adds a RIGHT JOIN clause and with to the query using the Cliente relation
 * @method     ChildClienteEstoqueItemQuery innerJoinWithCliente() Adds a INNER JOIN clause and with to the query using the Cliente relation
 *
 * @method     ChildClienteEstoqueItemQuery leftJoinEstoque($relationAlias = null) Adds a LEFT JOIN clause to the query using the Estoque relation
 * @method     ChildClienteEstoqueItemQuery rightJoinEstoque($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Estoque relation
 * @method     ChildClienteEstoqueItemQuery innerJoinEstoque($relationAlias = null) Adds a INNER JOIN clause to the query using the Estoque relation
 *
 * @method     ChildClienteEstoqueItemQuery joinWithEstoque($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Estoque relation
 *
 * @method     ChildClienteEstoqueItemQuery leftJoinWithEstoque() Adds a LEFT JOIN clause and with to the query using the Estoque relation
 * @method     ChildClienteEstoqueItemQuery rightJoinWithEstoque() Adds a RIGHT JOIN clause and with to the query using the Estoque relation
 * @method     ChildClienteEstoqueItemQuery innerJoinWithEstoque() Adds a INNER JOIN clause and with to the query using the Estoque relation
 *
 * @method     ChildClienteEstoqueItemQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildClienteEstoqueItemQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildClienteEstoqueItemQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildClienteEstoqueItemQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildClienteEstoqueItemQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildClienteEstoqueItemQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildClienteEstoqueItemQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \ImaTelecomBundle\Model\ClienteQuery|\ImaTelecomBundle\Model\EstoqueQuery|\ImaTelecomBundle\Model\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildClienteEstoqueItem findOne(ConnectionInterface $con = null) Return the first ChildClienteEstoqueItem matching the query
 * @method     ChildClienteEstoqueItem findOneOrCreate(ConnectionInterface $con = null) Return the first ChildClienteEstoqueItem matching the query, or a new ChildClienteEstoqueItem object populated from the query conditions when no match is found
 *
 * @method     ChildClienteEstoqueItem findOneByIdclienteEstoqueItem(int $idcliente_estoque_item) Return the first ChildClienteEstoqueItem filtered by the idcliente_estoque_item column
 * @method     ChildClienteEstoqueItem findOneByClienteId(int $cliente_id) Return the first ChildClienteEstoqueItem filtered by the cliente_id column
 * @method     ChildClienteEstoqueItem findOneByEstoqueId(int $estoque_id) Return the first ChildClienteEstoqueItem filtered by the estoque_id column
 * @method     ChildClienteEstoqueItem findOneByQtde(double $qtde) Return the first ChildClienteEstoqueItem filtered by the qtde column
 * @method     ChildClienteEstoqueItem findOneByDataCadastro(string $data_cadastro) Return the first ChildClienteEstoqueItem filtered by the data_cadastro column
 * @method     ChildClienteEstoqueItem findOneByDataAlterado(string $data_alterado) Return the first ChildClienteEstoqueItem filtered by the data_alterado column
 * @method     ChildClienteEstoqueItem findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildClienteEstoqueItem filtered by the usuario_alterado column *

 * @method     ChildClienteEstoqueItem requirePk($key, ConnectionInterface $con = null) Return the ChildClienteEstoqueItem by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClienteEstoqueItem requireOne(ConnectionInterface $con = null) Return the first ChildClienteEstoqueItem matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClienteEstoqueItem requireOneByIdclienteEstoqueItem(int $idcliente_estoque_item) Return the first ChildClienteEstoqueItem filtered by the idcliente_estoque_item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClienteEstoqueItem requireOneByClienteId(int $cliente_id) Return the first ChildClienteEstoqueItem filtered by the cliente_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClienteEstoqueItem requireOneByEstoqueId(int $estoque_id) Return the first ChildClienteEstoqueItem filtered by the estoque_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClienteEstoqueItem requireOneByQtde(double $qtde) Return the first ChildClienteEstoqueItem filtered by the qtde column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClienteEstoqueItem requireOneByDataCadastro(string $data_cadastro) Return the first ChildClienteEstoqueItem filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClienteEstoqueItem requireOneByDataAlterado(string $data_alterado) Return the first ChildClienteEstoqueItem filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildClienteEstoqueItem requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildClienteEstoqueItem filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildClienteEstoqueItem[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildClienteEstoqueItem objects based on current ModelCriteria
 * @method     ChildClienteEstoqueItem[]|ObjectCollection findByIdclienteEstoqueItem(int $idcliente_estoque_item) Return ChildClienteEstoqueItem objects filtered by the idcliente_estoque_item column
 * @method     ChildClienteEstoqueItem[]|ObjectCollection findByClienteId(int $cliente_id) Return ChildClienteEstoqueItem objects filtered by the cliente_id column
 * @method     ChildClienteEstoqueItem[]|ObjectCollection findByEstoqueId(int $estoque_id) Return ChildClienteEstoqueItem objects filtered by the estoque_id column
 * @method     ChildClienteEstoqueItem[]|ObjectCollection findByQtde(double $qtde) Return ChildClienteEstoqueItem objects filtered by the qtde column
 * @method     ChildClienteEstoqueItem[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildClienteEstoqueItem objects filtered by the data_cadastro column
 * @method     ChildClienteEstoqueItem[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildClienteEstoqueItem objects filtered by the data_alterado column
 * @method     ChildClienteEstoqueItem[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildClienteEstoqueItem objects filtered by the usuario_alterado column
 * @method     ChildClienteEstoqueItem[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ClienteEstoqueItemQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ClienteEstoqueItemQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ClienteEstoqueItem', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildClienteEstoqueItemQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildClienteEstoqueItemQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildClienteEstoqueItemQuery) {
            return $criteria;
        }
        $query = new ChildClienteEstoqueItemQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildClienteEstoqueItem|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ClienteEstoqueItemTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ClienteEstoqueItemTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClienteEstoqueItem A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcliente_estoque_item, cliente_id, estoque_id, qtde, data_cadastro, data_alterado, usuario_alterado FROM cliente_estoque_item WHERE idcliente_estoque_item = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildClienteEstoqueItem $obj */
            $obj = new ChildClienteEstoqueItem();
            $obj->hydrate($row);
            ClienteEstoqueItemTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildClienteEstoqueItem|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_IDCLIENTE_ESTOQUE_ITEM, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_IDCLIENTE_ESTOQUE_ITEM, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcliente_estoque_item column
     *
     * Example usage:
     * <code>
     * $query->filterByIdclienteEstoqueItem(1234); // WHERE idcliente_estoque_item = 1234
     * $query->filterByIdclienteEstoqueItem(array(12, 34)); // WHERE idcliente_estoque_item IN (12, 34)
     * $query->filterByIdclienteEstoqueItem(array('min' => 12)); // WHERE idcliente_estoque_item > 12
     * </code>
     *
     * @param     mixed $idclienteEstoqueItem The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByIdclienteEstoqueItem($idclienteEstoqueItem = null, $comparison = null)
    {
        if (is_array($idclienteEstoqueItem)) {
            $useMinMax = false;
            if (isset($idclienteEstoqueItem['min'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_IDCLIENTE_ESTOQUE_ITEM, $idclienteEstoqueItem['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idclienteEstoqueItem['max'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_IDCLIENTE_ESTOQUE_ITEM, $idclienteEstoqueItem['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_IDCLIENTE_ESTOQUE_ITEM, $idclienteEstoqueItem, $comparison);
    }

    /**
     * Filter the query on the cliente_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClienteId(1234); // WHERE cliente_id = 1234
     * $query->filterByClienteId(array(12, 34)); // WHERE cliente_id IN (12, 34)
     * $query->filterByClienteId(array('min' => 12)); // WHERE cliente_id > 12
     * </code>
     *
     * @see       filterByCliente()
     *
     * @param     mixed $clienteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByClienteId($clienteId = null, $comparison = null)
    {
        if (is_array($clienteId)) {
            $useMinMax = false;
            if (isset($clienteId['min'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_CLIENTE_ID, $clienteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clienteId['max'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_CLIENTE_ID, $clienteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_CLIENTE_ID, $clienteId, $comparison);
    }

    /**
     * Filter the query on the estoque_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEstoqueId(1234); // WHERE estoque_id = 1234
     * $query->filterByEstoqueId(array(12, 34)); // WHERE estoque_id IN (12, 34)
     * $query->filterByEstoqueId(array('min' => 12)); // WHERE estoque_id > 12
     * </code>
     *
     * @see       filterByEstoque()
     *
     * @param     mixed $estoqueId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByEstoqueId($estoqueId = null, $comparison = null)
    {
        if (is_array($estoqueId)) {
            $useMinMax = false;
            if (isset($estoqueId['min'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_ESTOQUE_ID, $estoqueId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($estoqueId['max'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_ESTOQUE_ID, $estoqueId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_ESTOQUE_ID, $estoqueId, $comparison);
    }

    /**
     * Filter the query on the qtde column
     *
     * Example usage:
     * <code>
     * $query->filterByQtde(1234); // WHERE qtde = 1234
     * $query->filterByQtde(array(12, 34)); // WHERE qtde IN (12, 34)
     * $query->filterByQtde(array('min' => 12)); // WHERE qtde > 12
     * </code>
     *
     * @param     mixed $qtde The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByQtde($qtde = null, $comparison = null)
    {
        if (is_array($qtde)) {
            $useMinMax = false;
            if (isset($qtde['min'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_QTDE, $qtde['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($qtde['max'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_QTDE, $qtde['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_QTDE, $qtde, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cliente object
     *
     * @param \ImaTelecomBundle\Model\Cliente|ObjectCollection $cliente The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByCliente($cliente, $comparison = null)
    {
        if ($cliente instanceof \ImaTelecomBundle\Model\Cliente) {
            return $this
                ->addUsingAlias(ClienteEstoqueItemTableMap::COL_CLIENTE_ID, $cliente->getIdcliente(), $comparison);
        } elseif ($cliente instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ClienteEstoqueItemTableMap::COL_CLIENTE_ID, $cliente->toKeyValue('PrimaryKey', 'Idcliente'), $comparison);
        } else {
            throw new PropelException('filterByCliente() only accepts arguments of type \ImaTelecomBundle\Model\Cliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function joinCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cliente');
        }

        return $this;
    }

    /**
     * Use the Cliente relation Cliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteQuery A secondary query class using the current class as primary query
     */
    public function useClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cliente', '\ImaTelecomBundle\Model\ClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Estoque object
     *
     * @param \ImaTelecomBundle\Model\Estoque|ObjectCollection $estoque The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByEstoque($estoque, $comparison = null)
    {
        if ($estoque instanceof \ImaTelecomBundle\Model\Estoque) {
            return $this
                ->addUsingAlias(ClienteEstoqueItemTableMap::COL_ESTOQUE_ID, $estoque->getIdestoque(), $comparison);
        } elseif ($estoque instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ClienteEstoqueItemTableMap::COL_ESTOQUE_ID, $estoque->toKeyValue('PrimaryKey', 'Idestoque'), $comparison);
        } else {
            throw new PropelException('filterByEstoque() only accepts arguments of type \ImaTelecomBundle\Model\Estoque or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Estoque relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function joinEstoque($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Estoque');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Estoque');
        }

        return $this;
    }

    /**
     * Use the Estoque relation Estoque object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstoque($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Estoque', '\ImaTelecomBundle\Model\EstoqueQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ClienteEstoqueItemTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ClienteEstoqueItemTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildClienteEstoqueItem $clienteEstoqueItem Object to remove from the list of results
     *
     * @return $this|ChildClienteEstoqueItemQuery The current query, for fluid interface
     */
    public function prune($clienteEstoqueItem = null)
    {
        if ($clienteEstoqueItem) {
            $this->addUsingAlias(ClienteEstoqueItemTableMap::COL_IDCLIENTE_ESTOQUE_ITEM, $clienteEstoqueItem->getIdclienteEstoqueItem(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the cliente_estoque_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClienteEstoqueItemTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ClienteEstoqueItemTableMap::clearInstancePool();
            ClienteEstoqueItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClienteEstoqueItemTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ClienteEstoqueItemTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ClienteEstoqueItemTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ClienteEstoqueItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ClienteEstoqueItemQuery
