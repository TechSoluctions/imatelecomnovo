<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BancoAgenciaConta as ChildBancoAgenciaConta;
use ImaTelecomBundle\Model\BancoAgenciaContaQuery as ChildBancoAgenciaContaQuery;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Cliente as ChildCliente;
use ImaTelecomBundle\Model\ClienteQuery as ChildClienteQuery;
use ImaTelecomBundle\Model\ContasPagar as ChildContasPagar;
use ImaTelecomBundle\Model\ContasPagarQuery as ChildContasPagarQuery;
use ImaTelecomBundle\Model\Fornecedor as ChildFornecedor;
use ImaTelecomBundle\Model\FornecedorQuery as ChildFornecedorQuery;
use ImaTelecomBundle\Model\Pessoa as ChildPessoa;
use ImaTelecomBundle\Model\PessoaQuery as ChildPessoaQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use ImaTelecomBundle\Model\Map\ContasPagarTableMap;
use ImaTelecomBundle\Model\Map\FornecedorTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'fornecedor' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Fornecedor implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\FornecedorTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idfornecedor field.
     *
     * @var        int
     */
    protected $idfornecedor;

    /**
     * The value for the cliente_id field.
     *
     * @var        int
     */
    protected $cliente_id;

    /**
     * The value for the pessoa_id field.
     *
     * @var        int
     */
    protected $pessoa_id;

    /**
     * The value for the banco_agencia_conta_id field.
     *
     * @var        int
     */
    protected $banco_agencia_conta_id;

    /**
     * The value for the observacao field.
     *
     * @var        string
     */
    protected $observacao;

    /**
     * The value for the ativo field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $ativo;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * @var        ChildBancoAgenciaConta
     */
    protected $aBancoAgenciaConta;

    /**
     * @var        ChildCliente
     */
    protected $aCliente;

    /**
     * @var        ChildPessoa
     */
    protected $aPessoa;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildBoleto[] Collection to store aggregation of ChildBoleto objects.
     */
    protected $collBoletos;
    protected $collBoletosPartial;

    /**
     * @var        ObjectCollection|ChildContasPagar[] Collection to store aggregation of ChildContasPagar objects.
     */
    protected $collContasPagars;
    protected $collContasPagarsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoleto[]
     */
    protected $boletosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContasPagar[]
     */
    protected $contasPagarsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->ativo = true;
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Fornecedor object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Fornecedor</code> instance.  If
     * <code>obj</code> is an instance of <code>Fornecedor</code>, delegates to
     * <code>equals(Fornecedor)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Fornecedor The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idfornecedor] column value.
     *
     * @return int
     */
    public function getIdfornecedor()
    {
        return $this->idfornecedor;
    }

    /**
     * Get the [cliente_id] column value.
     *
     * @return int
     */
    public function getClienteId()
    {
        return $this->cliente_id;
    }

    /**
     * Get the [pessoa_id] column value.
     *
     * @return int
     */
    public function getPessoaId()
    {
        return $this->pessoa_id;
    }

    /**
     * Get the [banco_agencia_conta_id] column value.
     *
     * @return int
     */
    public function getBancoAgenciaContaId()
    {
        return $this->banco_agencia_conta_id;
    }

    /**
     * Get the [observacao] column value.
     *
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->getAtivo();
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Set the value of [idfornecedor] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function setIdfornecedor($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idfornecedor !== $v) {
            $this->idfornecedor = $v;
            $this->modifiedColumns[FornecedorTableMap::COL_IDFORNECEDOR] = true;
        }

        return $this;
    } // setIdfornecedor()

    /**
     * Set the value of [cliente_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function setClienteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cliente_id !== $v) {
            $this->cliente_id = $v;
            $this->modifiedColumns[FornecedorTableMap::COL_CLIENTE_ID] = true;
        }

        if ($this->aCliente !== null && $this->aCliente->getIdcliente() !== $v) {
            $this->aCliente = null;
        }

        return $this;
    } // setClienteId()

    /**
     * Set the value of [pessoa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function setPessoaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pessoa_id !== $v) {
            $this->pessoa_id = $v;
            $this->modifiedColumns[FornecedorTableMap::COL_PESSOA_ID] = true;
        }

        if ($this->aPessoa !== null && $this->aPessoa->getId() !== $v) {
            $this->aPessoa = null;
        }

        return $this;
    } // setPessoaId()

    /**
     * Set the value of [banco_agencia_conta_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function setBancoAgenciaContaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->banco_agencia_conta_id !== $v) {
            $this->banco_agencia_conta_id = $v;
            $this->modifiedColumns[FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID] = true;
        }

        if ($this->aBancoAgenciaConta !== null && $this->aBancoAgenciaConta->getIdbancoAgenciaConta() !== $v) {
            $this->aBancoAgenciaConta = null;
        }

        return $this;
    } // setBancoAgenciaContaId()

    /**
     * Set the value of [observacao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function setObservacao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observacao !== $v) {
            $this->observacao = $v;
            $this->modifiedColumns[FornecedorTableMap::COL_OBSERVACAO] = true;
        }

        return $this;
    } // setObservacao()

    /**
     * Sets the value of the [ativo] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function setAtivo($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ativo !== $v) {
            $this->ativo = $v;
            $this->modifiedColumns[FornecedorTableMap::COL_ATIVO] = true;
        }

        return $this;
    } // setAtivo()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FornecedorTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FornecedorTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[FornecedorTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->ativo !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : FornecedorTableMap::translateFieldName('Idfornecedor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idfornecedor = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : FornecedorTableMap::translateFieldName('ClienteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cliente_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : FornecedorTableMap::translateFieldName('PessoaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pessoa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : FornecedorTableMap::translateFieldName('BancoAgenciaContaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->banco_agencia_conta_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : FornecedorTableMap::translateFieldName('Observacao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observacao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : FornecedorTableMap::translateFieldName('Ativo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ativo = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : FornecedorTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : FornecedorTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : FornecedorTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = FornecedorTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Fornecedor'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCliente !== null && $this->cliente_id !== $this->aCliente->getIdcliente()) {
            $this->aCliente = null;
        }
        if ($this->aPessoa !== null && $this->pessoa_id !== $this->aPessoa->getId()) {
            $this->aPessoa = null;
        }
        if ($this->aBancoAgenciaConta !== null && $this->banco_agencia_conta_id !== $this->aBancoAgenciaConta->getIdbancoAgenciaConta()) {
            $this->aBancoAgenciaConta = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FornecedorTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildFornecedorQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBancoAgenciaConta = null;
            $this->aCliente = null;
            $this->aPessoa = null;
            $this->aUsuario = null;
            $this->collBoletos = null;

            $this->collContasPagars = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Fornecedor::setDeleted()
     * @see Fornecedor::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FornecedorTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildFornecedorQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FornecedorTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FornecedorTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBancoAgenciaConta !== null) {
                if ($this->aBancoAgenciaConta->isModified() || $this->aBancoAgenciaConta->isNew()) {
                    $affectedRows += $this->aBancoAgenciaConta->save($con);
                }
                $this->setBancoAgenciaConta($this->aBancoAgenciaConta);
            }

            if ($this->aCliente !== null) {
                if ($this->aCliente->isModified() || $this->aCliente->isNew()) {
                    $affectedRows += $this->aCliente->save($con);
                }
                $this->setCliente($this->aCliente);
            }

            if ($this->aPessoa !== null) {
                if ($this->aPessoa->isModified() || $this->aPessoa->isNew()) {
                    $affectedRows += $this->aPessoa->save($con);
                }
                $this->setPessoa($this->aPessoa);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->boletosScheduledForDeletion !== null) {
                if (!$this->boletosScheduledForDeletion->isEmpty()) {
                    foreach ($this->boletosScheduledForDeletion as $boleto) {
                        // need to save related object because we set the relation to null
                        $boleto->save($con);
                    }
                    $this->boletosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletos !== null) {
                foreach ($this->collBoletos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->contasPagarsScheduledForDeletion !== null) {
                if (!$this->contasPagarsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ContasPagarQuery::create()
                        ->filterByPrimaryKeys($this->contasPagarsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contasPagarsScheduledForDeletion = null;
                }
            }

            if ($this->collContasPagars !== null) {
                foreach ($this->collContasPagars as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[FornecedorTableMap::COL_IDFORNECEDOR] = true;
        if (null !== $this->idfornecedor) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . FornecedorTableMap::COL_IDFORNECEDOR . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FornecedorTableMap::COL_IDFORNECEDOR)) {
            $modifiedColumns[':p' . $index++]  = 'idfornecedor';
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_CLIENTE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cliente_id';
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_PESSOA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pessoa_id';
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'banco_agencia_conta_id';
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_OBSERVACAO)) {
            $modifiedColumns[':p' . $index++]  = 'observacao';
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_ATIVO)) {
            $modifiedColumns[':p' . $index++]  = 'ativo';
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }

        $sql = sprintf(
            'INSERT INTO fornecedor (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idfornecedor':
                        $stmt->bindValue($identifier, $this->idfornecedor, PDO::PARAM_INT);
                        break;
                    case 'cliente_id':
                        $stmt->bindValue($identifier, $this->cliente_id, PDO::PARAM_INT);
                        break;
                    case 'pessoa_id':
                        $stmt->bindValue($identifier, $this->pessoa_id, PDO::PARAM_INT);
                        break;
                    case 'banco_agencia_conta_id':
                        $stmt->bindValue($identifier, $this->banco_agencia_conta_id, PDO::PARAM_INT);
                        break;
                    case 'observacao':
                        $stmt->bindValue($identifier, $this->observacao, PDO::PARAM_STR);
                        break;
                    case 'ativo':
                        $stmt->bindValue($identifier, (int) $this->ativo, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdfornecedor($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FornecedorTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdfornecedor();
                break;
            case 1:
                return $this->getClienteId();
                break;
            case 2:
                return $this->getPessoaId();
                break;
            case 3:
                return $this->getBancoAgenciaContaId();
                break;
            case 4:
                return $this->getObservacao();
                break;
            case 5:
                return $this->getAtivo();
                break;
            case 6:
                return $this->getDataCadastro();
                break;
            case 7:
                return $this->getDataAlterado();
                break;
            case 8:
                return $this->getUsuarioAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Fornecedor'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Fornecedor'][$this->hashCode()] = true;
        $keys = FornecedorTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdfornecedor(),
            $keys[1] => $this->getClienteId(),
            $keys[2] => $this->getPessoaId(),
            $keys[3] => $this->getBancoAgenciaContaId(),
            $keys[4] => $this->getObservacao(),
            $keys[5] => $this->getAtivo(),
            $keys[6] => $this->getDataCadastro(),
            $keys[7] => $this->getDataAlterado(),
            $keys[8] => $this->getUsuarioAlterado(),
        );
        if ($result[$keys[6]] instanceof \DateTime) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBancoAgenciaConta) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bancoAgenciaConta';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'banco_agencia_conta';
                        break;
                    default:
                        $key = 'BancoAgenciaConta';
                }

                $result[$key] = $this->aBancoAgenciaConta->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCliente) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'cliente';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cliente';
                        break;
                    default:
                        $key = 'Cliente';
                }

                $result[$key] = $this->aCliente->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPessoa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pessoa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'pessoa';
                        break;
                    default:
                        $key = 'Pessoa';
                }

                $result[$key] = $this->aPessoa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBoletos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boletos';
                        break;
                    default:
                        $key = 'Boletos';
                }

                $result[$key] = $this->collBoletos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collContasPagars) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contasPagars';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contas_pagars';
                        break;
                    default:
                        $key = 'ContasPagars';
                }

                $result[$key] = $this->collContasPagars->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Fornecedor
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FornecedorTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Fornecedor
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdfornecedor($value);
                break;
            case 1:
                $this->setClienteId($value);
                break;
            case 2:
                $this->setPessoaId($value);
                break;
            case 3:
                $this->setBancoAgenciaContaId($value);
                break;
            case 4:
                $this->setObservacao($value);
                break;
            case 5:
                $this->setAtivo($value);
                break;
            case 6:
                $this->setDataCadastro($value);
                break;
            case 7:
                $this->setDataAlterado($value);
                break;
            case 8:
                $this->setUsuarioAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = FornecedorTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdfornecedor($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setClienteId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPessoaId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setBancoAgenciaContaId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setObservacao($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setAtivo($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDataCadastro($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDataAlterado($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(FornecedorTableMap::DATABASE_NAME);

        if ($this->isColumnModified(FornecedorTableMap::COL_IDFORNECEDOR)) {
            $criteria->add(FornecedorTableMap::COL_IDFORNECEDOR, $this->idfornecedor);
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_CLIENTE_ID)) {
            $criteria->add(FornecedorTableMap::COL_CLIENTE_ID, $this->cliente_id);
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_PESSOA_ID)) {
            $criteria->add(FornecedorTableMap::COL_PESSOA_ID, $this->pessoa_id);
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID)) {
            $criteria->add(FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID, $this->banco_agencia_conta_id);
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_OBSERVACAO)) {
            $criteria->add(FornecedorTableMap::COL_OBSERVACAO, $this->observacao);
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_ATIVO)) {
            $criteria->add(FornecedorTableMap::COL_ATIVO, $this->ativo);
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(FornecedorTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(FornecedorTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(FornecedorTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(FornecedorTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildFornecedorQuery::create();
        $criteria->add(FornecedorTableMap::COL_IDFORNECEDOR, $this->idfornecedor);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdfornecedor();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdfornecedor();
    }

    /**
     * Generic method to set the primary key (idfornecedor column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdfornecedor($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdfornecedor();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Fornecedor (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setClienteId($this->getClienteId());
        $copyObj->setPessoaId($this->getPessoaId());
        $copyObj->setBancoAgenciaContaId($this->getBancoAgenciaContaId());
        $copyObj->setObservacao($this->getObservacao());
        $copyObj->setAtivo($this->getAtivo());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBoletos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoleto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getContasPagars() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContasPagar($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdfornecedor(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Fornecedor Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBancoAgenciaConta object.
     *
     * @param  ChildBancoAgenciaConta $v
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBancoAgenciaConta(ChildBancoAgenciaConta $v = null)
    {
        if ($v === null) {
            $this->setBancoAgenciaContaId(NULL);
        } else {
            $this->setBancoAgenciaContaId($v->getIdbancoAgenciaConta());
        }

        $this->aBancoAgenciaConta = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBancoAgenciaConta object, it will not be re-added.
        if ($v !== null) {
            $v->addFornecedor($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBancoAgenciaConta object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBancoAgenciaConta The associated ChildBancoAgenciaConta object.
     * @throws PropelException
     */
    public function getBancoAgenciaConta(ConnectionInterface $con = null)
    {
        if ($this->aBancoAgenciaConta === null && ($this->banco_agencia_conta_id !== null)) {
            $this->aBancoAgenciaConta = ChildBancoAgenciaContaQuery::create()->findPk($this->banco_agencia_conta_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBancoAgenciaConta->addFornecedors($this);
             */
        }

        return $this->aBancoAgenciaConta;
    }

    /**
     * Declares an association between this object and a ChildCliente object.
     *
     * @param  ChildCliente $v
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCliente(ChildCliente $v = null)
    {
        if ($v === null) {
            $this->setClienteId(NULL);
        } else {
            $this->setClienteId($v->getIdcliente());
        }

        $this->aCliente = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCliente object, it will not be re-added.
        if ($v !== null) {
            $v->addFornecedor($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCliente object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCliente The associated ChildCliente object.
     * @throws PropelException
     */
    public function getCliente(ConnectionInterface $con = null)
    {
        if ($this->aCliente === null && ($this->cliente_id !== null)) {
            $this->aCliente = ChildClienteQuery::create()->findPk($this->cliente_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCliente->addFornecedors($this);
             */
        }

        return $this->aCliente;
    }

    /**
     * Declares an association between this object and a ChildPessoa object.
     *
     * @param  ChildPessoa $v
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPessoa(ChildPessoa $v = null)
    {
        if ($v === null) {
            $this->setPessoaId(NULL);
        } else {
            $this->setPessoaId($v->getId());
        }

        $this->aPessoa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPessoa object, it will not be re-added.
        if ($v !== null) {
            $v->addFornecedor($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPessoa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPessoa The associated ChildPessoa object.
     * @throws PropelException
     */
    public function getPessoa(ConnectionInterface $con = null)
    {
        if ($this->aPessoa === null && ($this->pessoa_id !== null)) {
            $this->aPessoa = ChildPessoaQuery::create()->findPk($this->pessoa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPessoa->addFornecedors($this);
             */
        }

        return $this->aPessoa;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addFornecedor($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addFornecedors($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Boleto' == $relationName) {
            return $this->initBoletos();
        }
        if ('ContasPagar' == $relationName) {
            return $this->initContasPagars();
        }
    }

    /**
     * Clears out the collBoletos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletos()
     */
    public function clearBoletos()
    {
        $this->collBoletos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletos collection loaded partially.
     */
    public function resetPartialBoletos($v = true)
    {
        $this->collBoletosPartial = $v;
    }

    /**
     * Initializes the collBoletos collection.
     *
     * By default this just sets the collBoletos collection to an empty array (like clearcollBoletos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletos($overrideExisting = true)
    {
        if (null !== $this->collBoletos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletos = new $collectionClassName;
        $this->collBoletos->setModel('\ImaTelecomBundle\Model\Boleto');
    }

    /**
     * Gets an array of ChildBoleto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFornecedor is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     * @throws PropelException
     */
    public function getBoletos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                // return empty collection
                $this->initBoletos();
            } else {
                $collBoletos = ChildBoletoQuery::create(null, $criteria)
                    ->filterByFornecedor($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletosPartial && count($collBoletos)) {
                        $this->initBoletos(false);

                        foreach ($collBoletos as $obj) {
                            if (false == $this->collBoletos->contains($obj)) {
                                $this->collBoletos->append($obj);
                            }
                        }

                        $this->collBoletosPartial = true;
                    }

                    return $collBoletos;
                }

                if ($partial && $this->collBoletos) {
                    foreach ($this->collBoletos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletos[] = $obj;
                        }
                    }
                }

                $this->collBoletos = $collBoletos;
                $this->collBoletosPartial = false;
            }
        }

        return $this->collBoletos;
    }

    /**
     * Sets a collection of ChildBoleto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFornecedor The current object (for fluent API support)
     */
    public function setBoletos(Collection $boletos, ConnectionInterface $con = null)
    {
        /** @var ChildBoleto[] $boletosToDelete */
        $boletosToDelete = $this->getBoletos(new Criteria(), $con)->diff($boletos);


        $this->boletosScheduledForDeletion = $boletosToDelete;

        foreach ($boletosToDelete as $boletoRemoved) {
            $boletoRemoved->setFornecedor(null);
        }

        $this->collBoletos = null;
        foreach ($boletos as $boleto) {
            $this->addBoleto($boleto);
        }

        $this->collBoletos = $boletos;
        $this->collBoletosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Boleto objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Boleto objects.
     * @throws PropelException
     */
    public function countBoletos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletos());
            }

            $query = ChildBoletoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFornecedor($this)
                ->count($con);
        }

        return count($this->collBoletos);
    }

    /**
     * Method called to associate a ChildBoleto object to this object
     * through the ChildBoleto foreign key attribute.
     *
     * @param  ChildBoleto $l ChildBoleto
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function addBoleto(ChildBoleto $l)
    {
        if ($this->collBoletos === null) {
            $this->initBoletos();
            $this->collBoletosPartial = true;
        }

        if (!$this->collBoletos->contains($l)) {
            $this->doAddBoleto($l);

            if ($this->boletosScheduledForDeletion and $this->boletosScheduledForDeletion->contains($l)) {
                $this->boletosScheduledForDeletion->remove($this->boletosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoleto $boleto The ChildBoleto object to add.
     */
    protected function doAddBoleto(ChildBoleto $boleto)
    {
        $this->collBoletos[]= $boleto;
        $boleto->setFornecedor($this);
    }

    /**
     * @param  ChildBoleto $boleto The ChildBoleto object to remove.
     * @return $this|ChildFornecedor The current object (for fluent API support)
     */
    public function removeBoleto(ChildBoleto $boleto)
    {
        if ($this->getBoletos()->contains($boleto)) {
            $pos = $this->collBoletos->search($boleto);
            $this->collBoletos->remove($pos);
            if (null === $this->boletosScheduledForDeletion) {
                $this->boletosScheduledForDeletion = clone $this->collBoletos;
                $this->boletosScheduledForDeletion->clear();
            }
            $this->boletosScheduledForDeletion[]= $boleto;
            $boleto->setFornecedor(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Fornecedor is new, it will return
     * an empty collection; or if this Fornecedor has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Fornecedor.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinBaixaEstorno(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('BaixaEstorno', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Fornecedor is new, it will return
     * an empty collection; or if this Fornecedor has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Fornecedor.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Fornecedor is new, it will return
     * an empty collection; or if this Fornecedor has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Fornecedor.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Fornecedor is new, it will return
     * an empty collection; or if this Fornecedor has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Fornecedor.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Fornecedor is new, it will return
     * an empty collection; or if this Fornecedor has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Fornecedor.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletos($query, $con);
    }

    /**
     * Clears out the collContasPagars collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContasPagars()
     */
    public function clearContasPagars()
    {
        $this->collContasPagars = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContasPagars collection loaded partially.
     */
    public function resetPartialContasPagars($v = true)
    {
        $this->collContasPagarsPartial = $v;
    }

    /**
     * Initializes the collContasPagars collection.
     *
     * By default this just sets the collContasPagars collection to an empty array (like clearcollContasPagars());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContasPagars($overrideExisting = true)
    {
        if (null !== $this->collContasPagars && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContasPagarTableMap::getTableMap()->getCollectionClassName();

        $this->collContasPagars = new $collectionClassName;
        $this->collContasPagars->setModel('\ImaTelecomBundle\Model\ContasPagar');
    }

    /**
     * Gets an array of ChildContasPagar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFornecedor is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContasPagar[] List of ChildContasPagar objects
     * @throws PropelException
     */
    public function getContasPagars(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarsPartial && !$this->isNew();
        if (null === $this->collContasPagars || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContasPagars) {
                // return empty collection
                $this->initContasPagars();
            } else {
                $collContasPagars = ChildContasPagarQuery::create(null, $criteria)
                    ->filterByFornecedor($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContasPagarsPartial && count($collContasPagars)) {
                        $this->initContasPagars(false);

                        foreach ($collContasPagars as $obj) {
                            if (false == $this->collContasPagars->contains($obj)) {
                                $this->collContasPagars->append($obj);
                            }
                        }

                        $this->collContasPagarsPartial = true;
                    }

                    return $collContasPagars;
                }

                if ($partial && $this->collContasPagars) {
                    foreach ($this->collContasPagars as $obj) {
                        if ($obj->isNew()) {
                            $collContasPagars[] = $obj;
                        }
                    }
                }

                $this->collContasPagars = $collContasPagars;
                $this->collContasPagarsPartial = false;
            }
        }

        return $this->collContasPagars;
    }

    /**
     * Sets a collection of ChildContasPagar objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contasPagars A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFornecedor The current object (for fluent API support)
     */
    public function setContasPagars(Collection $contasPagars, ConnectionInterface $con = null)
    {
        /** @var ChildContasPagar[] $contasPagarsToDelete */
        $contasPagarsToDelete = $this->getContasPagars(new Criteria(), $con)->diff($contasPagars);


        $this->contasPagarsScheduledForDeletion = $contasPagarsToDelete;

        foreach ($contasPagarsToDelete as $contasPagarRemoved) {
            $contasPagarRemoved->setFornecedor(null);
        }

        $this->collContasPagars = null;
        foreach ($contasPagars as $contasPagar) {
            $this->addContasPagar($contasPagar);
        }

        $this->collContasPagars = $contasPagars;
        $this->collContasPagarsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ContasPagar objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ContasPagar objects.
     * @throws PropelException
     */
    public function countContasPagars(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarsPartial && !$this->isNew();
        if (null === $this->collContasPagars || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContasPagars) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContasPagars());
            }

            $query = ChildContasPagarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFornecedor($this)
                ->count($con);
        }

        return count($this->collContasPagars);
    }

    /**
     * Method called to associate a ChildContasPagar object to this object
     * through the ChildContasPagar foreign key attribute.
     *
     * @param  ChildContasPagar $l ChildContasPagar
     * @return $this|\ImaTelecomBundle\Model\Fornecedor The current object (for fluent API support)
     */
    public function addContasPagar(ChildContasPagar $l)
    {
        if ($this->collContasPagars === null) {
            $this->initContasPagars();
            $this->collContasPagarsPartial = true;
        }

        if (!$this->collContasPagars->contains($l)) {
            $this->doAddContasPagar($l);

            if ($this->contasPagarsScheduledForDeletion and $this->contasPagarsScheduledForDeletion->contains($l)) {
                $this->contasPagarsScheduledForDeletion->remove($this->contasPagarsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContasPagar $contasPagar The ChildContasPagar object to add.
     */
    protected function doAddContasPagar(ChildContasPagar $contasPagar)
    {
        $this->collContasPagars[]= $contasPagar;
        $contasPagar->setFornecedor($this);
    }

    /**
     * @param  ChildContasPagar $contasPagar The ChildContasPagar object to remove.
     * @return $this|ChildFornecedor The current object (for fluent API support)
     */
    public function removeContasPagar(ChildContasPagar $contasPagar)
    {
        if ($this->getContasPagars()->contains($contasPagar)) {
            $pos = $this->collContasPagars->search($contasPagar);
            $this->collContasPagars->remove($pos);
            if (null === $this->contasPagarsScheduledForDeletion) {
                $this->contasPagarsScheduledForDeletion = clone $this->collContasPagars;
                $this->contasPagarsScheduledForDeletion->clear();
            }
            $this->contasPagarsScheduledForDeletion[]= clone $contasPagar;
            $contasPagar->setFornecedor(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Fornecedor is new, it will return
     * an empty collection; or if this Fornecedor has previously
     * been saved, it will retrieve related ContasPagars from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Fornecedor.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContasPagar[] List of ChildContasPagar objects
     */
    public function getContasPagarsJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContasPagarQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getContasPagars($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBancoAgenciaConta) {
            $this->aBancoAgenciaConta->removeFornecedor($this);
        }
        if (null !== $this->aCliente) {
            $this->aCliente->removeFornecedor($this);
        }
        if (null !== $this->aPessoa) {
            $this->aPessoa->removeFornecedor($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeFornecedor($this);
        }
        $this->idfornecedor = null;
        $this->cliente_id = null;
        $this->pessoa_id = null;
        $this->banco_agencia_conta_id = null;
        $this->observacao = null;
        $this->ativo = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBoletos) {
                foreach ($this->collBoletos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collContasPagars) {
                foreach ($this->collContasPagars as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBoletos = null;
        $this->collContasPagars = null;
        $this->aBancoAgenciaConta = null;
        $this->aCliente = null;
        $this->aPessoa = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(FornecedorTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
