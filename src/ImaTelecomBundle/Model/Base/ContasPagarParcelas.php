<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Caixa as ChildCaixa;
use ImaTelecomBundle\Model\CaixaQuery as ChildCaixaQuery;
use ImaTelecomBundle\Model\ContasPagar as ChildContasPagar;
use ImaTelecomBundle\Model\ContasPagarParcelasQuery as ChildContasPagarParcelasQuery;
use ImaTelecomBundle\Model\ContasPagarQuery as ChildContasPagarQuery;
use ImaTelecomBundle\Model\Map\ContasPagarParcelasTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'contas_pagar_parcelas' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class ContasPagarParcelas implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\ContasPagarParcelasTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idcontas_pagar_parcelas field.
     *
     * @var        int
     */
    protected $idcontas_pagar_parcelas;

    /**
     * The value for the valor field.
     *
     * @var        string
     */
    protected $valor;

    /**
     * The value for the vencimento field.
     *
     * @var        DateTime
     */
    protected $vencimento;

    /**
     * The value for the conta_pagar_id field.
     *
     * @var        int
     */
    protected $conta_pagar_id;

    /**
     * The value for the num_parcela field.
     *
     * @var        int
     */
    protected $num_parcela;

    /**
     * The value for the caixa_id field.
     *
     * @var        int
     */
    protected $caixa_id;

    /**
     * The value for the esta_pago field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $esta_pago;

    /**
     * @var        ChildCaixa
     */
    protected $aCaixa;

    /**
     * @var        ChildContasPagar
     */
    protected $aContasPagar;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->esta_pago = false;
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\ContasPagarParcelas object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ContasPagarParcelas</code> instance.  If
     * <code>obj</code> is an instance of <code>ContasPagarParcelas</code>, delegates to
     * <code>equals(ContasPagarParcelas)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ContasPagarParcelas The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idcontas_pagar_parcelas] column value.
     *
     * @return int
     */
    public function getIdcontasPagarParcelas()
    {
        return $this->idcontas_pagar_parcelas;
    }

    /**
     * Get the [valor] column value.
     *
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Get the [optionally formatted] temporal [vencimento] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getVencimento($format = NULL)
    {
        if ($format === null) {
            return $this->vencimento;
        } else {
            return $this->vencimento instanceof \DateTimeInterface ? $this->vencimento->format($format) : null;
        }
    }

    /**
     * Get the [conta_pagar_id] column value.
     *
     * @return int
     */
    public function getContaPagarId()
    {
        return $this->conta_pagar_id;
    }

    /**
     * Get the [num_parcela] column value.
     *
     * @return int
     */
    public function getNumParcela()
    {
        return $this->num_parcela;
    }

    /**
     * Get the [caixa_id] column value.
     *
     * @return int
     */
    public function getCaixaId()
    {
        return $this->caixa_id;
    }

    /**
     * Get the [esta_pago] column value.
     *
     * @return boolean
     */
    public function getEstaPago()
    {
        return $this->esta_pago;
    }

    /**
     * Get the [esta_pago] column value.
     *
     * @return boolean
     */
    public function isEstaPago()
    {
        return $this->getEstaPago();
    }

    /**
     * Set the value of [idcontas_pagar_parcelas] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object (for fluent API support)
     */
    public function setIdcontasPagarParcelas($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idcontas_pagar_parcelas !== $v) {
            $this->idcontas_pagar_parcelas = $v;
            $this->modifiedColumns[ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS] = true;
        }

        return $this;
    } // setIdcontasPagarParcelas()

    /**
     * Set the value of [valor] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object (for fluent API support)
     */
    public function setValor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor !== $v) {
            $this->valor = $v;
            $this->modifiedColumns[ContasPagarParcelasTableMap::COL_VALOR] = true;
        }

        return $this;
    } // setValor()

    /**
     * Sets the value of [vencimento] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object (for fluent API support)
     */
    public function setVencimento($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vencimento !== null || $dt !== null) {
            if ($this->vencimento === null || $dt === null || $dt->format("Y-m-d") !== $this->vencimento->format("Y-m-d")) {
                $this->vencimento = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContasPagarParcelasTableMap::COL_VENCIMENTO] = true;
            }
        } // if either are not null

        return $this;
    } // setVencimento()

    /**
     * Set the value of [conta_pagar_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object (for fluent API support)
     */
    public function setContaPagarId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->conta_pagar_id !== $v) {
            $this->conta_pagar_id = $v;
            $this->modifiedColumns[ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID] = true;
        }

        if ($this->aContasPagar !== null && $this->aContasPagar->getIdcontasPagar() !== $v) {
            $this->aContasPagar = null;
        }

        return $this;
    } // setContaPagarId()

    /**
     * Set the value of [num_parcela] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object (for fluent API support)
     */
    public function setNumParcela($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->num_parcela !== $v) {
            $this->num_parcela = $v;
            $this->modifiedColumns[ContasPagarParcelasTableMap::COL_NUM_PARCELA] = true;
        }

        return $this;
    } // setNumParcela()

    /**
     * Set the value of [caixa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object (for fluent API support)
     */
    public function setCaixaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->caixa_id !== $v) {
            $this->caixa_id = $v;
            $this->modifiedColumns[ContasPagarParcelasTableMap::COL_CAIXA_ID] = true;
        }

        if ($this->aCaixa !== null && $this->aCaixa->getIdcaixa() !== $v) {
            $this->aCaixa = null;
        }

        return $this;
    } // setCaixaId()

    /**
     * Sets the value of the [esta_pago] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object (for fluent API support)
     */
    public function setEstaPago($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->esta_pago !== $v) {
            $this->esta_pago = $v;
            $this->modifiedColumns[ContasPagarParcelasTableMap::COL_ESTA_PAGO] = true;
        }

        return $this;
    } // setEstaPago()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->esta_pago !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ContasPagarParcelasTableMap::translateFieldName('IdcontasPagarParcelas', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idcontas_pagar_parcelas = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ContasPagarParcelasTableMap::translateFieldName('Valor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ContasPagarParcelasTableMap::translateFieldName('Vencimento', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->vencimento = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ContasPagarParcelasTableMap::translateFieldName('ContaPagarId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->conta_pagar_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ContasPagarParcelasTableMap::translateFieldName('NumParcela', TableMap::TYPE_PHPNAME, $indexType)];
            $this->num_parcela = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ContasPagarParcelasTableMap::translateFieldName('CaixaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->caixa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ContasPagarParcelasTableMap::translateFieldName('EstaPago', TableMap::TYPE_PHPNAME, $indexType)];
            $this->esta_pago = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = ContasPagarParcelasTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\ContasPagarParcelas'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aContasPagar !== null && $this->conta_pagar_id !== $this->aContasPagar->getIdcontasPagar()) {
            $this->aContasPagar = null;
        }
        if ($this->aCaixa !== null && $this->caixa_id !== $this->aCaixa->getIdcaixa()) {
            $this->aCaixa = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContasPagarParcelasTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildContasPagarParcelasQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCaixa = null;
            $this->aContasPagar = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ContasPagarParcelas::setDeleted()
     * @see ContasPagarParcelas::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarParcelasTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildContasPagarParcelasQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarParcelasTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ContasPagarParcelasTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCaixa !== null) {
                if ($this->aCaixa->isModified() || $this->aCaixa->isNew()) {
                    $affectedRows += $this->aCaixa->save($con);
                }
                $this->setCaixa($this->aCaixa);
            }

            if ($this->aContasPagar !== null) {
                if ($this->aContasPagar->isModified() || $this->aContasPagar->isNew()) {
                    $affectedRows += $this->aContasPagar->save($con);
                }
                $this->setContasPagar($this->aContasPagar);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS] = true;
        if (null !== $this->idcontas_pagar_parcelas) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS)) {
            $modifiedColumns[':p' . $index++]  = 'idcontas_pagar_parcelas';
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_VALOR)) {
            $modifiedColumns[':p' . $index++]  = 'valor';
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_VENCIMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'vencimento';
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'conta_pagar_id';
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_NUM_PARCELA)) {
            $modifiedColumns[':p' . $index++]  = 'num_parcela';
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_CAIXA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'caixa_id';
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_ESTA_PAGO)) {
            $modifiedColumns[':p' . $index++]  = 'esta_pago';
        }

        $sql = sprintf(
            'INSERT INTO contas_pagar_parcelas (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idcontas_pagar_parcelas':
                        $stmt->bindValue($identifier, $this->idcontas_pagar_parcelas, PDO::PARAM_INT);
                        break;
                    case 'valor':
                        $stmt->bindValue($identifier, $this->valor, PDO::PARAM_STR);
                        break;
                    case 'vencimento':
                        $stmt->bindValue($identifier, $this->vencimento ? $this->vencimento->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'conta_pagar_id':
                        $stmt->bindValue($identifier, $this->conta_pagar_id, PDO::PARAM_INT);
                        break;
                    case 'num_parcela':
                        $stmt->bindValue($identifier, $this->num_parcela, PDO::PARAM_INT);
                        break;
                    case 'caixa_id':
                        $stmt->bindValue($identifier, $this->caixa_id, PDO::PARAM_INT);
                        break;
                    case 'esta_pago':
                        $stmt->bindValue($identifier, (int) $this->esta_pago, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdcontasPagarParcelas($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContasPagarParcelasTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdcontasPagarParcelas();
                break;
            case 1:
                return $this->getValor();
                break;
            case 2:
                return $this->getVencimento();
                break;
            case 3:
                return $this->getContaPagarId();
                break;
            case 4:
                return $this->getNumParcela();
                break;
            case 5:
                return $this->getCaixaId();
                break;
            case 6:
                return $this->getEstaPago();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ContasPagarParcelas'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ContasPagarParcelas'][$this->hashCode()] = true;
        $keys = ContasPagarParcelasTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdcontasPagarParcelas(),
            $keys[1] => $this->getValor(),
            $keys[2] => $this->getVencimento(),
            $keys[3] => $this->getContaPagarId(),
            $keys[4] => $this->getNumParcela(),
            $keys[5] => $this->getCaixaId(),
            $keys[6] => $this->getEstaPago(),
        );
        if ($result[$keys[2]] instanceof \DateTime) {
            $result[$keys[2]] = $result[$keys[2]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCaixa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'caixa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'caixa';
                        break;
                    default:
                        $key = 'Caixa';
                }

                $result[$key] = $this->aCaixa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aContasPagar) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contasPagar';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contas_pagar';
                        break;
                    default:
                        $key = 'ContasPagar';
                }

                $result[$key] = $this->aContasPagar->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContasPagarParcelasTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdcontasPagarParcelas($value);
                break;
            case 1:
                $this->setValor($value);
                break;
            case 2:
                $this->setVencimento($value);
                break;
            case 3:
                $this->setContaPagarId($value);
                break;
            case 4:
                $this->setNumParcela($value);
                break;
            case 5:
                $this->setCaixaId($value);
                break;
            case 6:
                $this->setEstaPago($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ContasPagarParcelasTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdcontasPagarParcelas($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setValor($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setVencimento($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setContaPagarId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setNumParcela($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCaixaId($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setEstaPago($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ContasPagarParcelasTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS)) {
            $criteria->add(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, $this->idcontas_pagar_parcelas);
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_VALOR)) {
            $criteria->add(ContasPagarParcelasTableMap::COL_VALOR, $this->valor);
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_VENCIMENTO)) {
            $criteria->add(ContasPagarParcelasTableMap::COL_VENCIMENTO, $this->vencimento);
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID)) {
            $criteria->add(ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID, $this->conta_pagar_id);
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_NUM_PARCELA)) {
            $criteria->add(ContasPagarParcelasTableMap::COL_NUM_PARCELA, $this->num_parcela);
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_CAIXA_ID)) {
            $criteria->add(ContasPagarParcelasTableMap::COL_CAIXA_ID, $this->caixa_id);
        }
        if ($this->isColumnModified(ContasPagarParcelasTableMap::COL_ESTA_PAGO)) {
            $criteria->add(ContasPagarParcelasTableMap::COL_ESTA_PAGO, $this->esta_pago);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildContasPagarParcelasQuery::create();
        $criteria->add(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, $this->idcontas_pagar_parcelas);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdcontasPagarParcelas();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdcontasPagarParcelas();
    }

    /**
     * Generic method to set the primary key (idcontas_pagar_parcelas column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdcontasPagarParcelas($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdcontasPagarParcelas();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\ContasPagarParcelas (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setValor($this->getValor());
        $copyObj->setVencimento($this->getVencimento());
        $copyObj->setContaPagarId($this->getContaPagarId());
        $copyObj->setNumParcela($this->getNumParcela());
        $copyObj->setCaixaId($this->getCaixaId());
        $copyObj->setEstaPago($this->getEstaPago());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdcontasPagarParcelas(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\ContasPagarParcelas Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCaixa object.
     *
     * @param  ChildCaixa $v
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCaixa(ChildCaixa $v = null)
    {
        if ($v === null) {
            $this->setCaixaId(NULL);
        } else {
            $this->setCaixaId($v->getIdcaixa());
        }

        $this->aCaixa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCaixa object, it will not be re-added.
        if ($v !== null) {
            $v->addContasPagarParcelas($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCaixa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCaixa The associated ChildCaixa object.
     * @throws PropelException
     */
    public function getCaixa(ConnectionInterface $con = null)
    {
        if ($this->aCaixa === null && ($this->caixa_id !== null)) {
            $this->aCaixa = ChildCaixaQuery::create()->findPk($this->caixa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCaixa->addContasPagarParcelass($this);
             */
        }

        return $this->aCaixa;
    }

    /**
     * Declares an association between this object and a ChildContasPagar object.
     *
     * @param  ChildContasPagar $v
     * @return $this|\ImaTelecomBundle\Model\ContasPagarParcelas The current object (for fluent API support)
     * @throws PropelException
     */
    public function setContasPagar(ChildContasPagar $v = null)
    {
        if ($v === null) {
            $this->setContaPagarId(NULL);
        } else {
            $this->setContaPagarId($v->getIdcontasPagar());
        }

        $this->aContasPagar = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildContasPagar object, it will not be re-added.
        if ($v !== null) {
            $v->addContasPagarParcelas($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildContasPagar object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildContasPagar The associated ChildContasPagar object.
     * @throws PropelException
     */
    public function getContasPagar(ConnectionInterface $con = null)
    {
        if ($this->aContasPagar === null && ($this->conta_pagar_id !== null)) {
            $this->aContasPagar = ChildContasPagarQuery::create()->findPk($this->conta_pagar_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aContasPagar->addContasPagarParcelass($this);
             */
        }

        return $this->aContasPagar;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCaixa) {
            $this->aCaixa->removeContasPagarParcelas($this);
        }
        if (null !== $this->aContasPagar) {
            $this->aContasPagar->removeContasPagarParcelas($this);
        }
        $this->idcontas_pagar_parcelas = null;
        $this->valor = null;
        $this->vencimento = null;
        $this->conta_pagar_id = null;
        $this->num_parcela = null;
        $this->caixa_id = null;
        $this->esta_pago = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aCaixa = null;
        $this->aContasPagar = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ContasPagarParcelasTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
