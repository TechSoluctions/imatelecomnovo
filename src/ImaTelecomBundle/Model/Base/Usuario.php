<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Baixa as ChildBaixa;
use ImaTelecomBundle\Model\BaixaEstorno as ChildBaixaEstorno;
use ImaTelecomBundle\Model\BaixaEstornoQuery as ChildBaixaEstornoQuery;
use ImaTelecomBundle\Model\BaixaMovimento as ChildBaixaMovimento;
use ImaTelecomBundle\Model\BaixaMovimentoQuery as ChildBaixaMovimentoQuery;
use ImaTelecomBundle\Model\BaixaQuery as ChildBaixaQuery;
use ImaTelecomBundle\Model\Banco as ChildBanco;
use ImaTelecomBundle\Model\BancoAgencia as ChildBancoAgencia;
use ImaTelecomBundle\Model\BancoAgenciaConta as ChildBancoAgenciaConta;
use ImaTelecomBundle\Model\BancoAgenciaContaQuery as ChildBancoAgenciaContaQuery;
use ImaTelecomBundle\Model\BancoAgenciaQuery as ChildBancoAgenciaQuery;
use ImaTelecomBundle\Model\BancoQuery as ChildBancoQuery;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoBaixaHistorico as ChildBoletoBaixaHistorico;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery as ChildBoletoBaixaHistoricoQuery;
use ImaTelecomBundle\Model\BoletoItem as ChildBoletoItem;
use ImaTelecomBundle\Model\BoletoItemQuery as ChildBoletoItemQuery;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Caixa as ChildCaixa;
use ImaTelecomBundle\Model\CaixaQuery as ChildCaixaQuery;
use ImaTelecomBundle\Model\Cliente as ChildCliente;
use ImaTelecomBundle\Model\ClienteEstoqueItem as ChildClienteEstoqueItem;
use ImaTelecomBundle\Model\ClienteEstoqueItemQuery as ChildClienteEstoqueItemQuery;
use ImaTelecomBundle\Model\ClienteQuery as ChildClienteQuery;
use ImaTelecomBundle\Model\ContaCaixa as ChildContaCaixa;
use ImaTelecomBundle\Model\ContaCaixaQuery as ChildContaCaixaQuery;
use ImaTelecomBundle\Model\ContasPagar as ChildContasPagar;
use ImaTelecomBundle\Model\ContasPagarQuery as ChildContasPagarQuery;
use ImaTelecomBundle\Model\ContasPagarTributos as ChildContasPagarTributos;
use ImaTelecomBundle\Model\ContasPagarTributosQuery as ChildContasPagarTributosQuery;
use ImaTelecomBundle\Model\Convenio as ChildConvenio;
use ImaTelecomBundle\Model\ConvenioQuery as ChildConvenioQuery;
use ImaTelecomBundle\Model\CronTask as ChildCronTask;
use ImaTelecomBundle\Model\CronTaskGrupo as ChildCronTaskGrupo;
use ImaTelecomBundle\Model\CronTaskGrupoQuery as ChildCronTaskGrupoQuery;
use ImaTelecomBundle\Model\CronTaskQuery as ChildCronTaskQuery;
use ImaTelecomBundle\Model\EnderecoAgencia as ChildEnderecoAgencia;
use ImaTelecomBundle\Model\EnderecoAgenciaQuery as ChildEnderecoAgenciaQuery;
use ImaTelecomBundle\Model\EnderecoCliente as ChildEnderecoCliente;
use ImaTelecomBundle\Model\EnderecoClienteQuery as ChildEnderecoClienteQuery;
use ImaTelecomBundle\Model\EnderecoServCliente as ChildEnderecoServCliente;
use ImaTelecomBundle\Model\EnderecoServClienteQuery as ChildEnderecoServClienteQuery;
use ImaTelecomBundle\Model\Estoque as ChildEstoque;
use ImaTelecomBundle\Model\EstoqueLancamento as ChildEstoqueLancamento;
use ImaTelecomBundle\Model\EstoqueLancamentoItem as ChildEstoqueLancamentoItem;
use ImaTelecomBundle\Model\EstoqueLancamentoItemQuery as ChildEstoqueLancamentoItemQuery;
use ImaTelecomBundle\Model\EstoqueLancamentoQuery as ChildEstoqueLancamentoQuery;
use ImaTelecomBundle\Model\EstoqueQuery as ChildEstoqueQuery;
use ImaTelecomBundle\Model\FormaPagamento as ChildFormaPagamento;
use ImaTelecomBundle\Model\FormaPagamentoQuery as ChildFormaPagamentoQuery;
use ImaTelecomBundle\Model\Fornecedor as ChildFornecedor;
use ImaTelecomBundle\Model\FornecedorQuery as ChildFornecedorQuery;
use ImaTelecomBundle\Model\ItemComprado as ChildItemComprado;
use ImaTelecomBundle\Model\ItemCompradoQuery as ChildItemCompradoQuery;
use ImaTelecomBundle\Model\ItemDeCompra as ChildItemDeCompra;
use ImaTelecomBundle\Model\ItemDeCompraQuery as ChildItemDeCompraQuery;
use ImaTelecomBundle\Model\Pessoa as ChildPessoa;
use ImaTelecomBundle\Model\PessoaQuery as ChildPessoaQuery;
use ImaTelecomBundle\Model\ServicoCliente as ChildServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery as ChildServicoClienteQuery;
use ImaTelecomBundle\Model\ServicoPrestado as ChildServicoPrestado;
use ImaTelecomBundle\Model\ServicoPrestadoQuery as ChildServicoPrestadoQuery;
use ImaTelecomBundle\Model\Sici as ChildSici;
use ImaTelecomBundle\Model\SiciQuery as ChildSiciQuery;
use ImaTelecomBundle\Model\SysPerfil as ChildSysPerfil;
use ImaTelecomBundle\Model\SysPerfilQuery as ChildSysPerfilQuery;
use ImaTelecomBundle\Model\SysProcessos as ChildSysProcessos;
use ImaTelecomBundle\Model\SysProcessosQuery as ChildSysProcessosQuery;
use ImaTelecomBundle\Model\SysUsuarioPerfil as ChildSysUsuarioPerfil;
use ImaTelecomBundle\Model\SysUsuarioPerfilQuery as ChildSysUsuarioPerfilQuery;
use ImaTelecomBundle\Model\Telefone as ChildTelefone;
use ImaTelecomBundle\Model\TelefoneQuery as ChildTelefoneQuery;
use ImaTelecomBundle\Model\Tributo as ChildTributo;
use ImaTelecomBundle\Model\TributoQuery as ChildTributoQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BaixaEstornoTableMap;
use ImaTelecomBundle\Model\Map\BaixaMovimentoTableMap;
use ImaTelecomBundle\Model\Map\BaixaTableMap;
use ImaTelecomBundle\Model\Map\BancoAgenciaContaTableMap;
use ImaTelecomBundle\Model\Map\BancoAgenciaTableMap;
use ImaTelecomBundle\Model\Map\BancoTableMap;
use ImaTelecomBundle\Model\Map\BoletoBaixaHistoricoTableMap;
use ImaTelecomBundle\Model\Map\BoletoItemTableMap;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use ImaTelecomBundle\Model\Map\CaixaTableMap;
use ImaTelecomBundle\Model\Map\ClienteEstoqueItemTableMap;
use ImaTelecomBundle\Model\Map\ClienteTableMap;
use ImaTelecomBundle\Model\Map\ContaCaixaTableMap;
use ImaTelecomBundle\Model\Map\ContasPagarTableMap;
use ImaTelecomBundle\Model\Map\ContasPagarTributosTableMap;
use ImaTelecomBundle\Model\Map\ConvenioTableMap;
use ImaTelecomBundle\Model\Map\CronTaskGrupoTableMap;
use ImaTelecomBundle\Model\Map\CronTaskTableMap;
use ImaTelecomBundle\Model\Map\EnderecoAgenciaTableMap;
use ImaTelecomBundle\Model\Map\EnderecoClienteTableMap;
use ImaTelecomBundle\Model\Map\EnderecoServClienteTableMap;
use ImaTelecomBundle\Model\Map\EstoqueLancamentoItemTableMap;
use ImaTelecomBundle\Model\Map\EstoqueLancamentoTableMap;
use ImaTelecomBundle\Model\Map\EstoqueTableMap;
use ImaTelecomBundle\Model\Map\FormaPagamentoTableMap;
use ImaTelecomBundle\Model\Map\FornecedorTableMap;
use ImaTelecomBundle\Model\Map\ItemCompradoTableMap;
use ImaTelecomBundle\Model\Map\ItemDeCompraTableMap;
use ImaTelecomBundle\Model\Map\ServicoClienteTableMap;
use ImaTelecomBundle\Model\Map\ServicoPrestadoTableMap;
use ImaTelecomBundle\Model\Map\SiciTableMap;
use ImaTelecomBundle\Model\Map\SysPerfilTableMap;
use ImaTelecomBundle\Model\Map\SysProcessosTableMap;
use ImaTelecomBundle\Model\Map\SysUsuarioPerfilTableMap;
use ImaTelecomBundle\Model\Map\TelefoneTableMap;
use ImaTelecomBundle\Model\Map\TributoTableMap;
use ImaTelecomBundle\Model\Map\UsuarioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'usuario' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Usuario implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\UsuarioTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idusuario field.
     *
     * @var        int
     */
    protected $idusuario;

    /**
     * The value for the pessoa_id field.
     *
     * @var        int
     */
    protected $pessoa_id;

    /**
     * The value for the login field.
     *
     * @var        string
     */
    protected $login;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the ativo field.
     *
     * @var        boolean
     */
    protected $ativo;

    /**
     * @var        ChildPessoa
     */
    protected $aPessoa;

    /**
     * @var        ObjectCollection|ChildBaixa[] Collection to store aggregation of ChildBaixa objects.
     */
    protected $collBaixas;
    protected $collBaixasPartial;

    /**
     * @var        ObjectCollection|ChildBaixaEstorno[] Collection to store aggregation of ChildBaixaEstorno objects.
     */
    protected $collBaixaEstornos;
    protected $collBaixaEstornosPartial;

    /**
     * @var        ObjectCollection|ChildBaixaMovimento[] Collection to store aggregation of ChildBaixaMovimento objects.
     */
    protected $collBaixaMovimentos;
    protected $collBaixaMovimentosPartial;

    /**
     * @var        ObjectCollection|ChildBanco[] Collection to store aggregation of ChildBanco objects.
     */
    protected $collBancos;
    protected $collBancosPartial;

    /**
     * @var        ObjectCollection|ChildBancoAgencia[] Collection to store aggregation of ChildBancoAgencia objects.
     */
    protected $collBancoAgencias;
    protected $collBancoAgenciasPartial;

    /**
     * @var        ObjectCollection|ChildBancoAgenciaConta[] Collection to store aggregation of ChildBancoAgenciaConta objects.
     */
    protected $collBancoAgenciaContas;
    protected $collBancoAgenciaContasPartial;

    /**
     * @var        ObjectCollection|ChildBoleto[] Collection to store aggregation of ChildBoleto objects.
     */
    protected $collBoletos;
    protected $collBoletosPartial;

    /**
     * @var        ObjectCollection|ChildBoletoBaixaHistorico[] Collection to store aggregation of ChildBoletoBaixaHistorico objects.
     */
    protected $collBoletoBaixaHistoricos;
    protected $collBoletoBaixaHistoricosPartial;

    /**
     * @var        ObjectCollection|ChildBoletoItem[] Collection to store aggregation of ChildBoletoItem objects.
     */
    protected $collBoletoItems;
    protected $collBoletoItemsPartial;

    /**
     * @var        ObjectCollection|ChildCaixa[] Collection to store aggregation of ChildCaixa objects.
     */
    protected $collCaixas;
    protected $collCaixasPartial;

    /**
     * @var        ObjectCollection|ChildCliente[] Collection to store aggregation of ChildCliente objects.
     */
    protected $collClientes;
    protected $collClientesPartial;

    /**
     * @var        ObjectCollection|ChildClienteEstoqueItem[] Collection to store aggregation of ChildClienteEstoqueItem objects.
     */
    protected $collClienteEstoqueItems;
    protected $collClienteEstoqueItemsPartial;

    /**
     * @var        ObjectCollection|ChildContaCaixa[] Collection to store aggregation of ChildContaCaixa objects.
     */
    protected $collContaCaixas;
    protected $collContaCaixasPartial;

    /**
     * @var        ObjectCollection|ChildContasPagar[] Collection to store aggregation of ChildContasPagar objects.
     */
    protected $collContasPagars;
    protected $collContasPagarsPartial;

    /**
     * @var        ObjectCollection|ChildContasPagarTributos[] Collection to store aggregation of ChildContasPagarTributos objects.
     */
    protected $collContasPagarTributoss;
    protected $collContasPagarTributossPartial;

    /**
     * @var        ObjectCollection|ChildConvenio[] Collection to store aggregation of ChildConvenio objects.
     */
    protected $collConvenios;
    protected $collConveniosPartial;

    /**
     * @var        ObjectCollection|ChildCronTask[] Collection to store aggregation of ChildCronTask objects.
     */
    protected $collCronTasks;
    protected $collCronTasksPartial;

    /**
     * @var        ObjectCollection|ChildCronTaskGrupo[] Collection to store aggregation of ChildCronTaskGrupo objects.
     */
    protected $collCronTaskGrupos;
    protected $collCronTaskGruposPartial;

    /**
     * @var        ObjectCollection|ChildEnderecoAgencia[] Collection to store aggregation of ChildEnderecoAgencia objects.
     */
    protected $collEnderecoAgencias;
    protected $collEnderecoAgenciasPartial;

    /**
     * @var        ObjectCollection|ChildEnderecoCliente[] Collection to store aggregation of ChildEnderecoCliente objects.
     */
    protected $collEnderecoClientes;
    protected $collEnderecoClientesPartial;

    /**
     * @var        ObjectCollection|ChildEnderecoServCliente[] Collection to store aggregation of ChildEnderecoServCliente objects.
     */
    protected $collEnderecoServClientes;
    protected $collEnderecoServClientesPartial;

    /**
     * @var        ObjectCollection|ChildEstoque[] Collection to store aggregation of ChildEstoque objects.
     */
    protected $collEstoques;
    protected $collEstoquesPartial;

    /**
     * @var        ObjectCollection|ChildEstoqueLancamento[] Collection to store aggregation of ChildEstoqueLancamento objects.
     */
    protected $collEstoqueLancamentos;
    protected $collEstoqueLancamentosPartial;

    /**
     * @var        ObjectCollection|ChildEstoqueLancamentoItem[] Collection to store aggregation of ChildEstoqueLancamentoItem objects.
     */
    protected $collEstoqueLancamentoItems;
    protected $collEstoqueLancamentoItemsPartial;

    /**
     * @var        ObjectCollection|ChildFormaPagamento[] Collection to store aggregation of ChildFormaPagamento objects.
     */
    protected $collFormaPagamentos;
    protected $collFormaPagamentosPartial;

    /**
     * @var        ObjectCollection|ChildFornecedor[] Collection to store aggregation of ChildFornecedor objects.
     */
    protected $collFornecedors;
    protected $collFornecedorsPartial;

    /**
     * @var        ObjectCollection|ChildItemComprado[] Collection to store aggregation of ChildItemComprado objects.
     */
    protected $collItemComprados;
    protected $collItemCompradosPartial;

    /**
     * @var        ObjectCollection|ChildItemDeCompra[] Collection to store aggregation of ChildItemDeCompra objects.
     */
    protected $collItemDeCompras;
    protected $collItemDeComprasPartial;

    /**
     * @var        ObjectCollection|ChildServicoCliente[] Collection to store aggregation of ChildServicoCliente objects.
     */
    protected $collServicoClientes;
    protected $collServicoClientesPartial;

    /**
     * @var        ObjectCollection|ChildServicoPrestado[] Collection to store aggregation of ChildServicoPrestado objects.
     */
    protected $collServicoPrestados;
    protected $collServicoPrestadosPartial;

    /**
     * @var        ObjectCollection|ChildSici[] Collection to store aggregation of ChildSici objects.
     */
    protected $collSicis;
    protected $collSicisPartial;

    /**
     * @var        ObjectCollection|ChildSysPerfil[] Collection to store aggregation of ChildSysPerfil objects.
     */
    protected $collSysPerfils;
    protected $collSysPerfilsPartial;

    /**
     * @var        ObjectCollection|ChildSysProcessos[] Collection to store aggregation of ChildSysProcessos objects.
     */
    protected $collSysProcessoss;
    protected $collSysProcessossPartial;

    /**
     * @var        ObjectCollection|ChildSysUsuarioPerfil[] Collection to store aggregation of ChildSysUsuarioPerfil objects.
     */
    protected $collSysUsuarioPerfils;
    protected $collSysUsuarioPerfilsPartial;

    /**
     * @var        ObjectCollection|ChildTelefone[] Collection to store aggregation of ChildTelefone objects.
     */
    protected $collTelefones;
    protected $collTelefonesPartial;

    /**
     * @var        ObjectCollection|ChildTributo[] Collection to store aggregation of ChildTributo objects.
     */
    protected $collTributos;
    protected $collTributosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBaixa[]
     */
    protected $baixasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBaixaEstorno[]
     */
    protected $baixaEstornosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBaixaMovimento[]
     */
    protected $baixaMovimentosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBanco[]
     */
    protected $bancosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBancoAgencia[]
     */
    protected $bancoAgenciasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBancoAgenciaConta[]
     */
    protected $bancoAgenciaContasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoleto[]
     */
    protected $boletosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoletoBaixaHistorico[]
     */
    protected $boletoBaixaHistoricosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoletoItem[]
     */
    protected $boletoItemsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCaixa[]
     */
    protected $caixasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCliente[]
     */
    protected $clientesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildClienteEstoqueItem[]
     */
    protected $clienteEstoqueItemsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContaCaixa[]
     */
    protected $contaCaixasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContasPagar[]
     */
    protected $contasPagarsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContasPagarTributos[]
     */
    protected $contasPagarTributossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildConvenio[]
     */
    protected $conveniosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCronTask[]
     */
    protected $cronTasksScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCronTaskGrupo[]
     */
    protected $cronTaskGruposScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEnderecoAgencia[]
     */
    protected $enderecoAgenciasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEnderecoCliente[]
     */
    protected $enderecoClientesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEnderecoServCliente[]
     */
    protected $enderecoServClientesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEstoque[]
     */
    protected $estoquesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEstoqueLancamento[]
     */
    protected $estoqueLancamentosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEstoqueLancamentoItem[]
     */
    protected $estoqueLancamentoItemsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFormaPagamento[]
     */
    protected $formaPagamentosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFornecedor[]
     */
    protected $fornecedorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildItemComprado[]
     */
    protected $itemCompradosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildItemDeCompra[]
     */
    protected $itemDeComprasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicoCliente[]
     */
    protected $servicoClientesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicoPrestado[]
     */
    protected $servicoPrestadosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSici[]
     */
    protected $sicisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSysPerfil[]
     */
    protected $sysPerfilsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSysProcessos[]
     */
    protected $sysProcessossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSysUsuarioPerfil[]
     */
    protected $sysUsuarioPerfilsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTelefone[]
     */
    protected $telefonesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTributo[]
     */
    protected $tributosScheduledForDeletion = null;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Usuario object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Usuario</code> instance.  If
     * <code>obj</code> is an instance of <code>Usuario</code>, delegates to
     * <code>equals(Usuario)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Usuario The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idusuario] column value.
     *
     * @return int
     */
    public function getIdusuario()
    {
        return $this->idusuario;
    }

    /**
     * Get the [pessoa_id] column value.
     *
     * @return int
     */
    public function getPessoaId()
    {
        return $this->pessoa_id;
    }

    /**
     * Get the [login] column value.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->getAtivo();
    }

    /**
     * Set the value of [idusuario] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function setIdusuario($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idusuario !== $v) {
            $this->idusuario = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_IDUSUARIO] = true;
        }

        return $this;
    } // setIdusuario()

    /**
     * Set the value of [pessoa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function setPessoaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pessoa_id !== $v) {
            $this->pessoa_id = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_PESSOA_ID] = true;
        }

        if ($this->aPessoa !== null && $this->aPessoa->getId() !== $v) {
            $this->aPessoa = null;
        }

        return $this;
    } // setPessoaId()

    /**
     * Set the value of [login] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function setLogin($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->login !== $v) {
            $this->login = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_LOGIN] = true;
        }

        return $this;
    } // setLogin()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UsuarioTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UsuarioTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_USUARIO_ALTERADO] = true;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Sets the value of the [ativo] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function setAtivo($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ativo !== $v) {
            $this->ativo = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_ATIVO] = true;
        }

        return $this;
    } // setAtivo()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UsuarioTableMap::translateFieldName('Idusuario', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idusuario = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UsuarioTableMap::translateFieldName('PessoaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pessoa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UsuarioTableMap::translateFieldName('Login', TableMap::TYPE_PHPNAME, $indexType)];
            $this->login = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UsuarioTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UsuarioTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UsuarioTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UsuarioTableMap::translateFieldName('Ativo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ativo = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = UsuarioTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Usuario'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aPessoa !== null && $this->pessoa_id !== $this->aPessoa->getId()) {
            $this->aPessoa = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsuarioTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUsuarioQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPessoa = null;
            $this->collBaixas = null;

            $this->collBaixaEstornos = null;

            $this->collBaixaMovimentos = null;

            $this->collBancos = null;

            $this->collBancoAgencias = null;

            $this->collBancoAgenciaContas = null;

            $this->collBoletos = null;

            $this->collBoletoBaixaHistoricos = null;

            $this->collBoletoItems = null;

            $this->collCaixas = null;

            $this->collClientes = null;

            $this->collClienteEstoqueItems = null;

            $this->collContaCaixas = null;

            $this->collContasPagars = null;

            $this->collContasPagarTributoss = null;

            $this->collConvenios = null;

            $this->collCronTasks = null;

            $this->collCronTaskGrupos = null;

            $this->collEnderecoAgencias = null;

            $this->collEnderecoClientes = null;

            $this->collEnderecoServClientes = null;

            $this->collEstoques = null;

            $this->collEstoqueLancamentos = null;

            $this->collEstoqueLancamentoItems = null;

            $this->collFormaPagamentos = null;

            $this->collFornecedors = null;

            $this->collItemComprados = null;

            $this->collItemDeCompras = null;

            $this->collServicoClientes = null;

            $this->collServicoPrestados = null;

            $this->collSicis = null;

            $this->collSysPerfils = null;

            $this->collSysProcessoss = null;

            $this->collSysUsuarioPerfils = null;

            $this->collTelefones = null;

            $this->collTributos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Usuario::setDeleted()
     * @see Usuario::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUsuarioQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UsuarioTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPessoa !== null) {
                if ($this->aPessoa->isModified() || $this->aPessoa->isNew()) {
                    $affectedRows += $this->aPessoa->save($con);
                }
                $this->setPessoa($this->aPessoa);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->baixasScheduledForDeletion !== null) {
                if (!$this->baixasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BaixaQuery::create()
                        ->filterByPrimaryKeys($this->baixasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->baixasScheduledForDeletion = null;
                }
            }

            if ($this->collBaixas !== null) {
                foreach ($this->collBaixas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->baixaEstornosScheduledForDeletion !== null) {
                if (!$this->baixaEstornosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BaixaEstornoQuery::create()
                        ->filterByPrimaryKeys($this->baixaEstornosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->baixaEstornosScheduledForDeletion = null;
                }
            }

            if ($this->collBaixaEstornos !== null) {
                foreach ($this->collBaixaEstornos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->baixaMovimentosScheduledForDeletion !== null) {
                if (!$this->baixaMovimentosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BaixaMovimentoQuery::create()
                        ->filterByPrimaryKeys($this->baixaMovimentosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->baixaMovimentosScheduledForDeletion = null;
                }
            }

            if ($this->collBaixaMovimentos !== null) {
                foreach ($this->collBaixaMovimentos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bancosScheduledForDeletion !== null) {
                if (!$this->bancosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BancoQuery::create()
                        ->filterByPrimaryKeys($this->bancosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bancosScheduledForDeletion = null;
                }
            }

            if ($this->collBancos !== null) {
                foreach ($this->collBancos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bancoAgenciasScheduledForDeletion !== null) {
                if (!$this->bancoAgenciasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BancoAgenciaQuery::create()
                        ->filterByPrimaryKeys($this->bancoAgenciasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bancoAgenciasScheduledForDeletion = null;
                }
            }

            if ($this->collBancoAgencias !== null) {
                foreach ($this->collBancoAgencias as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->bancoAgenciaContasScheduledForDeletion !== null) {
                if (!$this->bancoAgenciaContasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BancoAgenciaContaQuery::create()
                        ->filterByPrimaryKeys($this->bancoAgenciaContasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->bancoAgenciaContasScheduledForDeletion = null;
                }
            }

            if ($this->collBancoAgenciaContas !== null) {
                foreach ($this->collBancoAgenciaContas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->boletosScheduledForDeletion !== null) {
                if (!$this->boletosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BoletoQuery::create()
                        ->filterByPrimaryKeys($this->boletosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->boletosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletos !== null) {
                foreach ($this->collBoletos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->boletoBaixaHistoricosScheduledForDeletion !== null) {
                if (!$this->boletoBaixaHistoricosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery::create()
                        ->filterByPrimaryKeys($this->boletoBaixaHistoricosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->boletoBaixaHistoricosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletoBaixaHistoricos !== null) {
                foreach ($this->collBoletoBaixaHistoricos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->boletoItemsScheduledForDeletion !== null) {
                if (!$this->boletoItemsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BoletoItemQuery::create()
                        ->filterByPrimaryKeys($this->boletoItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->boletoItemsScheduledForDeletion = null;
                }
            }

            if ($this->collBoletoItems !== null) {
                foreach ($this->collBoletoItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->caixasScheduledForDeletion !== null) {
                if (!$this->caixasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\CaixaQuery::create()
                        ->filterByPrimaryKeys($this->caixasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->caixasScheduledForDeletion = null;
                }
            }

            if ($this->collCaixas !== null) {
                foreach ($this->collCaixas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->clientesScheduledForDeletion !== null) {
                if (!$this->clientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ClienteQuery::create()
                        ->filterByPrimaryKeys($this->clientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->clientesScheduledForDeletion = null;
                }
            }

            if ($this->collClientes !== null) {
                foreach ($this->collClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->clienteEstoqueItemsScheduledForDeletion !== null) {
                if (!$this->clienteEstoqueItemsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ClienteEstoqueItemQuery::create()
                        ->filterByPrimaryKeys($this->clienteEstoqueItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->clienteEstoqueItemsScheduledForDeletion = null;
                }
            }

            if ($this->collClienteEstoqueItems !== null) {
                foreach ($this->collClienteEstoqueItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->contaCaixasScheduledForDeletion !== null) {
                if (!$this->contaCaixasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ContaCaixaQuery::create()
                        ->filterByPrimaryKeys($this->contaCaixasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contaCaixasScheduledForDeletion = null;
                }
            }

            if ($this->collContaCaixas !== null) {
                foreach ($this->collContaCaixas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->contasPagarsScheduledForDeletion !== null) {
                if (!$this->contasPagarsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ContasPagarQuery::create()
                        ->filterByPrimaryKeys($this->contasPagarsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contasPagarsScheduledForDeletion = null;
                }
            }

            if ($this->collContasPagars !== null) {
                foreach ($this->collContasPagars as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->contasPagarTributossScheduledForDeletion !== null) {
                if (!$this->contasPagarTributossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ContasPagarTributosQuery::create()
                        ->filterByPrimaryKeys($this->contasPagarTributossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contasPagarTributossScheduledForDeletion = null;
                }
            }

            if ($this->collContasPagarTributoss !== null) {
                foreach ($this->collContasPagarTributoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->conveniosScheduledForDeletion !== null) {
                if (!$this->conveniosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ConvenioQuery::create()
                        ->filterByPrimaryKeys($this->conveniosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->conveniosScheduledForDeletion = null;
                }
            }

            if ($this->collConvenios !== null) {
                foreach ($this->collConvenios as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cronTasksScheduledForDeletion !== null) {
                if (!$this->cronTasksScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\CronTaskQuery::create()
                        ->filterByPrimaryKeys($this->cronTasksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->cronTasksScheduledForDeletion = null;
                }
            }

            if ($this->collCronTasks !== null) {
                foreach ($this->collCronTasks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cronTaskGruposScheduledForDeletion !== null) {
                if (!$this->cronTaskGruposScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\CronTaskGrupoQuery::create()
                        ->filterByPrimaryKeys($this->cronTaskGruposScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->cronTaskGruposScheduledForDeletion = null;
                }
            }

            if ($this->collCronTaskGrupos !== null) {
                foreach ($this->collCronTaskGrupos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->enderecoAgenciasScheduledForDeletion !== null) {
                if (!$this->enderecoAgenciasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EnderecoAgenciaQuery::create()
                        ->filterByPrimaryKeys($this->enderecoAgenciasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->enderecoAgenciasScheduledForDeletion = null;
                }
            }

            if ($this->collEnderecoAgencias !== null) {
                foreach ($this->collEnderecoAgencias as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->enderecoClientesScheduledForDeletion !== null) {
                if (!$this->enderecoClientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EnderecoClienteQuery::create()
                        ->filterByPrimaryKeys($this->enderecoClientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->enderecoClientesScheduledForDeletion = null;
                }
            }

            if ($this->collEnderecoClientes !== null) {
                foreach ($this->collEnderecoClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->enderecoServClientesScheduledForDeletion !== null) {
                if (!$this->enderecoServClientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EnderecoServClienteQuery::create()
                        ->filterByPrimaryKeys($this->enderecoServClientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->enderecoServClientesScheduledForDeletion = null;
                }
            }

            if ($this->collEnderecoServClientes !== null) {
                foreach ($this->collEnderecoServClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->estoquesScheduledForDeletion !== null) {
                if (!$this->estoquesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EstoqueQuery::create()
                        ->filterByPrimaryKeys($this->estoquesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->estoquesScheduledForDeletion = null;
                }
            }

            if ($this->collEstoques !== null) {
                foreach ($this->collEstoques as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->estoqueLancamentosScheduledForDeletion !== null) {
                if (!$this->estoqueLancamentosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EstoqueLancamentoQuery::create()
                        ->filterByPrimaryKeys($this->estoqueLancamentosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->estoqueLancamentosScheduledForDeletion = null;
                }
            }

            if ($this->collEstoqueLancamentos !== null) {
                foreach ($this->collEstoqueLancamentos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->estoqueLancamentoItemsScheduledForDeletion !== null) {
                if (!$this->estoqueLancamentoItemsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EstoqueLancamentoItemQuery::create()
                        ->filterByPrimaryKeys($this->estoqueLancamentoItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->estoqueLancamentoItemsScheduledForDeletion = null;
                }
            }

            if ($this->collEstoqueLancamentoItems !== null) {
                foreach ($this->collEstoqueLancamentoItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->formaPagamentosScheduledForDeletion !== null) {
                if (!$this->formaPagamentosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\FormaPagamentoQuery::create()
                        ->filterByPrimaryKeys($this->formaPagamentosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->formaPagamentosScheduledForDeletion = null;
                }
            }

            if ($this->collFormaPagamentos !== null) {
                foreach ($this->collFormaPagamentos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->fornecedorsScheduledForDeletion !== null) {
                if (!$this->fornecedorsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\FornecedorQuery::create()
                        ->filterByPrimaryKeys($this->fornecedorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->fornecedorsScheduledForDeletion = null;
                }
            }

            if ($this->collFornecedors !== null) {
                foreach ($this->collFornecedors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->itemCompradosScheduledForDeletion !== null) {
                if (!$this->itemCompradosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ItemCompradoQuery::create()
                        ->filterByPrimaryKeys($this->itemCompradosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->itemCompradosScheduledForDeletion = null;
                }
            }

            if ($this->collItemComprados !== null) {
                foreach ($this->collItemComprados as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->itemDeComprasScheduledForDeletion !== null) {
                if (!$this->itemDeComprasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ItemDeCompraQuery::create()
                        ->filterByPrimaryKeys($this->itemDeComprasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->itemDeComprasScheduledForDeletion = null;
                }
            }

            if ($this->collItemDeCompras !== null) {
                foreach ($this->collItemDeCompras as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->servicoClientesScheduledForDeletion !== null) {
                if (!$this->servicoClientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ServicoClienteQuery::create()
                        ->filterByPrimaryKeys($this->servicoClientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicoClientesScheduledForDeletion = null;
                }
            }

            if ($this->collServicoClientes !== null) {
                foreach ($this->collServicoClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->servicoPrestadosScheduledForDeletion !== null) {
                if (!$this->servicoPrestadosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ServicoPrestadoQuery::create()
                        ->filterByPrimaryKeys($this->servicoPrestadosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicoPrestadosScheduledForDeletion = null;
                }
            }

            if ($this->collServicoPrestados !== null) {
                foreach ($this->collServicoPrestados as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sicisScheduledForDeletion !== null) {
                if (!$this->sicisScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\SiciQuery::create()
                        ->filterByPrimaryKeys($this->sicisScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sicisScheduledForDeletion = null;
                }
            }

            if ($this->collSicis !== null) {
                foreach ($this->collSicis as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysPerfilsScheduledForDeletion !== null) {
                if (!$this->sysPerfilsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\SysPerfilQuery::create()
                        ->filterByPrimaryKeys($this->sysPerfilsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sysPerfilsScheduledForDeletion = null;
                }
            }

            if ($this->collSysPerfils !== null) {
                foreach ($this->collSysPerfils as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysProcessossScheduledForDeletion !== null) {
                if (!$this->sysProcessossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\SysProcessosQuery::create()
                        ->filterByPrimaryKeys($this->sysProcessossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sysProcessossScheduledForDeletion = null;
                }
            }

            if ($this->collSysProcessoss !== null) {
                foreach ($this->collSysProcessoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sysUsuarioPerfilsScheduledForDeletion !== null) {
                if (!$this->sysUsuarioPerfilsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\SysUsuarioPerfilQuery::create()
                        ->filterByPrimaryKeys($this->sysUsuarioPerfilsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sysUsuarioPerfilsScheduledForDeletion = null;
                }
            }

            if ($this->collSysUsuarioPerfils !== null) {
                foreach ($this->collSysUsuarioPerfils as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->telefonesScheduledForDeletion !== null) {
                if (!$this->telefonesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\TelefoneQuery::create()
                        ->filterByPrimaryKeys($this->telefonesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->telefonesScheduledForDeletion = null;
                }
            }

            if ($this->collTelefones !== null) {
                foreach ($this->collTelefones as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->tributosScheduledForDeletion !== null) {
                if (!$this->tributosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\TributoQuery::create()
                        ->filterByPrimaryKeys($this->tributosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->tributosScheduledForDeletion = null;
                }
            }

            if ($this->collTributos !== null) {
                foreach ($this->collTributos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UsuarioTableMap::COL_IDUSUARIO] = true;
        if (null !== $this->idusuario) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UsuarioTableMap::COL_IDUSUARIO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UsuarioTableMap::COL_IDUSUARIO)) {
            $modifiedColumns[':p' . $index++]  = 'idusuario';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_PESSOA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pessoa_id';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_LOGIN)) {
            $modifiedColumns[':p' . $index++]  = 'login';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ATIVO)) {
            $modifiedColumns[':p' . $index++]  = 'ativo';
        }

        $sql = sprintf(
            'INSERT INTO usuario (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idusuario':
                        $stmt->bindValue($identifier, $this->idusuario, PDO::PARAM_INT);
                        break;
                    case 'pessoa_id':
                        $stmt->bindValue($identifier, $this->pessoa_id, PDO::PARAM_INT);
                        break;
                    case 'login':
                        $stmt->bindValue($identifier, $this->login, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'ativo':
                        $stmt->bindValue($identifier, (int) $this->ativo, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdusuario($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsuarioTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdusuario();
                break;
            case 1:
                return $this->getPessoaId();
                break;
            case 2:
                return $this->getLogin();
                break;
            case 3:
                return $this->getDataCadastro();
                break;
            case 4:
                return $this->getDataAlterado();
                break;
            case 5:
                return $this->getUsuarioAlterado();
                break;
            case 6:
                return $this->getAtivo();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Usuario'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Usuario'][$this->hashCode()] = true;
        $keys = UsuarioTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdusuario(),
            $keys[1] => $this->getPessoaId(),
            $keys[2] => $this->getLogin(),
            $keys[3] => $this->getDataCadastro(),
            $keys[4] => $this->getDataAlterado(),
            $keys[5] => $this->getUsuarioAlterado(),
            $keys[6] => $this->getAtivo(),
        );
        if ($result[$keys[3]] instanceof \DateTime) {
            $result[$keys[3]] = $result[$keys[3]]->format('c');
        }

        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPessoa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pessoa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'pessoa';
                        break;
                    default:
                        $key = 'Pessoa';
                }

                $result[$key] = $this->aPessoa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBaixas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixas';
                        break;
                    default:
                        $key = 'Baixas';
                }

                $result[$key] = $this->collBaixas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBaixaEstornos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixaEstornos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixa_estornos';
                        break;
                    default:
                        $key = 'BaixaEstornos';
                }

                $result[$key] = $this->collBaixaEstornos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBaixaMovimentos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixaMovimentos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixa_movimentos';
                        break;
                    default:
                        $key = 'BaixaMovimentos';
                }

                $result[$key] = $this->collBaixaMovimentos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBancos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bancos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'bancos';
                        break;
                    default:
                        $key = 'Bancos';
                }

                $result[$key] = $this->collBancos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBancoAgencias) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bancoAgencias';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'banco_agencias';
                        break;
                    default:
                        $key = 'BancoAgencias';
                }

                $result[$key] = $this->collBancoAgencias->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBancoAgenciaContas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bancoAgenciaContas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'banco_agencia_contas';
                        break;
                    default:
                        $key = 'BancoAgenciaContas';
                }

                $result[$key] = $this->collBancoAgenciaContas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBoletos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boletos';
                        break;
                    default:
                        $key = 'Boletos';
                }

                $result[$key] = $this->collBoletos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBoletoBaixaHistoricos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoBaixaHistoricos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_baixa_historicos';
                        break;
                    default:
                        $key = 'BoletoBaixaHistoricos';
                }

                $result[$key] = $this->collBoletoBaixaHistoricos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBoletoItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_items';
                        break;
                    default:
                        $key = 'BoletoItems';
                }

                $result[$key] = $this->collBoletoItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCaixas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'caixas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'caixas';
                        break;
                    default:
                        $key = 'Caixas';
                }

                $result[$key] = $this->collCaixas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'clientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'clientes';
                        break;
                    default:
                        $key = 'Clientes';
                }

                $result[$key] = $this->collClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collClienteEstoqueItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'clienteEstoqueItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cliente_estoque_items';
                        break;
                    default:
                        $key = 'ClienteEstoqueItems';
                }

                $result[$key] = $this->collClienteEstoqueItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collContaCaixas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contaCaixas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'conta_caixas';
                        break;
                    default:
                        $key = 'ContaCaixas';
                }

                $result[$key] = $this->collContaCaixas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collContasPagars) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contasPagars';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contas_pagars';
                        break;
                    default:
                        $key = 'ContasPagars';
                }

                $result[$key] = $this->collContasPagars->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collContasPagarTributoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contasPagarTributoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contas_pagar_tributoss';
                        break;
                    default:
                        $key = 'ContasPagarTributoss';
                }

                $result[$key] = $this->collContasPagarTributoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collConvenios) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'convenios';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'convenios';
                        break;
                    default:
                        $key = 'Convenios';
                }

                $result[$key] = $this->collConvenios->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCronTasks) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'cronTasks';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cron_tasks';
                        break;
                    default:
                        $key = 'CronTasks';
                }

                $result[$key] = $this->collCronTasks->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCronTaskGrupos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'cronTaskGrupos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cron_task_grupos';
                        break;
                    default:
                        $key = 'CronTaskGrupos';
                }

                $result[$key] = $this->collCronTaskGrupos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEnderecoAgencias) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'enderecoAgencias';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'endereco_agencias';
                        break;
                    default:
                        $key = 'EnderecoAgencias';
                }

                $result[$key] = $this->collEnderecoAgencias->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEnderecoClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'enderecoClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'endereco_clientes';
                        break;
                    default:
                        $key = 'EnderecoClientes';
                }

                $result[$key] = $this->collEnderecoClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEnderecoServClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'enderecoServClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'endereco_serv_clientes';
                        break;
                    default:
                        $key = 'EnderecoServClientes';
                }

                $result[$key] = $this->collEnderecoServClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEstoques) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'estoques';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'estoques';
                        break;
                    default:
                        $key = 'Estoques';
                }

                $result[$key] = $this->collEstoques->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEstoqueLancamentos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'estoqueLancamentos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'estoque_lancamentos';
                        break;
                    default:
                        $key = 'EstoqueLancamentos';
                }

                $result[$key] = $this->collEstoqueLancamentos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEstoqueLancamentoItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'estoqueLancamentoItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'estoque_lancamento_items';
                        break;
                    default:
                        $key = 'EstoqueLancamentoItems';
                }

                $result[$key] = $this->collEstoqueLancamentoItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFormaPagamentos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'formaPagamentos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'forma_pagamentos';
                        break;
                    default:
                        $key = 'FormaPagamentos';
                }

                $result[$key] = $this->collFormaPagamentos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFornecedors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'fornecedors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'fornecedors';
                        break;
                    default:
                        $key = 'Fornecedors';
                }

                $result[$key] = $this->collFornecedors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collItemComprados) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'itemComprados';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'item_comprados';
                        break;
                    default:
                        $key = 'ItemComprados';
                }

                $result[$key] = $this->collItemComprados->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collItemDeCompras) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'itemDeCompras';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'item_de_compras';
                        break;
                    default:
                        $key = 'ItemDeCompras';
                }

                $result[$key] = $this->collItemDeCompras->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collServicoClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicoClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'servico_clientes';
                        break;
                    default:
                        $key = 'ServicoClientes';
                }

                $result[$key] = $this->collServicoClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collServicoPrestados) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicoPrestados';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'servico_prestados';
                        break;
                    default:
                        $key = 'ServicoPrestados';
                }

                $result[$key] = $this->collServicoPrestados->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSicis) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sicis';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sicis';
                        break;
                    default:
                        $key = 'Sicis';
                }

                $result[$key] = $this->collSicis->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysPerfils) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sysPerfils';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sys_perfils';
                        break;
                    default:
                        $key = 'SysPerfils';
                }

                $result[$key] = $this->collSysPerfils->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysProcessoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sysProcessoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sys_processoss';
                        break;
                    default:
                        $key = 'SysProcessoss';
                }

                $result[$key] = $this->collSysProcessoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSysUsuarioPerfils) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sysUsuarioPerfils';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sys_usuario_perfils';
                        break;
                    default:
                        $key = 'SysUsuarioPerfils';
                }

                $result[$key] = $this->collSysUsuarioPerfils->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTelefones) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'telefones';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'telefones';
                        break;
                    default:
                        $key = 'Telefones';
                }

                $result[$key] = $this->collTelefones->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTributos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'tributos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'tributos';
                        break;
                    default:
                        $key = 'Tributos';
                }

                $result[$key] = $this->collTributos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Usuario
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsuarioTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Usuario
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdusuario($value);
                break;
            case 1:
                $this->setPessoaId($value);
                break;
            case 2:
                $this->setLogin($value);
                break;
            case 3:
                $this->setDataCadastro($value);
                break;
            case 4:
                $this->setDataAlterado($value);
                break;
            case 5:
                $this->setUsuarioAlterado($value);
                break;
            case 6:
                $this->setAtivo($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UsuarioTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdusuario($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setPessoaId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setLogin($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDataCadastro($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDataAlterado($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setAtivo($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UsuarioTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UsuarioTableMap::COL_IDUSUARIO)) {
            $criteria->add(UsuarioTableMap::COL_IDUSUARIO, $this->idusuario);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_PESSOA_ID)) {
            $criteria->add(UsuarioTableMap::COL_PESSOA_ID, $this->pessoa_id);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_LOGIN)) {
            $criteria->add(UsuarioTableMap::COL_LOGIN, $this->login);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(UsuarioTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(UsuarioTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(UsuarioTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ATIVO)) {
            $criteria->add(UsuarioTableMap::COL_ATIVO, $this->ativo);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUsuarioQuery::create();
        $criteria->add(UsuarioTableMap::COL_IDUSUARIO, $this->idusuario);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdusuario();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdusuario();
    }

    /**
     * Generic method to set the primary key (idusuario column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdusuario($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdusuario();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Usuario (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPessoaId($this->getPessoaId());
        $copyObj->setLogin($this->getLogin());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setAtivo($this->getAtivo());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBaixas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBaixa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBaixaEstornos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBaixaEstorno($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBaixaMovimentos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBaixaMovimento($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBancos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBanco($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBancoAgencias() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBancoAgencia($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBancoAgenciaContas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBancoAgenciaConta($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBoletos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoleto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBoletoBaixaHistoricos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoletoBaixaHistorico($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBoletoItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoletoItem($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCaixas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCaixa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCliente($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getClienteEstoqueItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addClienteEstoqueItem($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getContaCaixas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContaCaixa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getContasPagars() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContasPagar($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getContasPagarTributoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContasPagarTributos($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getConvenios() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addConvenio($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCronTasks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCronTask($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCronTaskGrupos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCronTaskGrupo($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEnderecoAgencias() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEnderecoAgencia($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEnderecoClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEnderecoCliente($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEnderecoServClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEnderecoServCliente($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEstoques() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEstoque($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEstoqueLancamentos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEstoqueLancamento($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEstoqueLancamentoItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEstoqueLancamentoItem($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFormaPagamentos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFormaPagamento($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFornecedors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFornecedor($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getItemComprados() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addItemComprado($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getItemDeCompras() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addItemDeCompra($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getServicoClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicoCliente($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getServicoPrestados() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicoPrestado($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSicis() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSici($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysPerfils() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysPerfil($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysProcessoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysProcessos($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSysUsuarioPerfils() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSysUsuarioPerfil($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTelefones() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTelefone($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTributos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTributo($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdusuario(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Usuario Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildPessoa object.
     *
     * @param  ChildPessoa $v
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPessoa(ChildPessoa $v = null)
    {
        if ($v === null) {
            $this->setPessoaId(NULL);
        } else {
            $this->setPessoaId($v->getId());
        }

        $this->aPessoa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPessoa object, it will not be re-added.
        if ($v !== null) {
            $v->addUsuario($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPessoa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPessoa The associated ChildPessoa object.
     * @throws PropelException
     */
    public function getPessoa(ConnectionInterface $con = null)
    {
        if ($this->aPessoa === null && ($this->pessoa_id !== null)) {
            $this->aPessoa = ChildPessoaQuery::create()->findPk($this->pessoa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPessoa->addUsuarios($this);
             */
        }

        return $this->aPessoa;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Baixa' == $relationName) {
            return $this->initBaixas();
        }
        if ('BaixaEstorno' == $relationName) {
            return $this->initBaixaEstornos();
        }
        if ('BaixaMovimento' == $relationName) {
            return $this->initBaixaMovimentos();
        }
        if ('Banco' == $relationName) {
            return $this->initBancos();
        }
        if ('BancoAgencia' == $relationName) {
            return $this->initBancoAgencias();
        }
        if ('BancoAgenciaConta' == $relationName) {
            return $this->initBancoAgenciaContas();
        }
        if ('Boleto' == $relationName) {
            return $this->initBoletos();
        }
        if ('BoletoBaixaHistorico' == $relationName) {
            return $this->initBoletoBaixaHistoricos();
        }
        if ('BoletoItem' == $relationName) {
            return $this->initBoletoItems();
        }
        if ('Caixa' == $relationName) {
            return $this->initCaixas();
        }
        if ('Cliente' == $relationName) {
            return $this->initClientes();
        }
        if ('ClienteEstoqueItem' == $relationName) {
            return $this->initClienteEstoqueItems();
        }
        if ('ContaCaixa' == $relationName) {
            return $this->initContaCaixas();
        }
        if ('ContasPagar' == $relationName) {
            return $this->initContasPagars();
        }
        if ('ContasPagarTributos' == $relationName) {
            return $this->initContasPagarTributoss();
        }
        if ('Convenio' == $relationName) {
            return $this->initConvenios();
        }
        if ('CronTask' == $relationName) {
            return $this->initCronTasks();
        }
        if ('CronTaskGrupo' == $relationName) {
            return $this->initCronTaskGrupos();
        }
        if ('EnderecoAgencia' == $relationName) {
            return $this->initEnderecoAgencias();
        }
        if ('EnderecoCliente' == $relationName) {
            return $this->initEnderecoClientes();
        }
        if ('EnderecoServCliente' == $relationName) {
            return $this->initEnderecoServClientes();
        }
        if ('Estoque' == $relationName) {
            return $this->initEstoques();
        }
        if ('EstoqueLancamento' == $relationName) {
            return $this->initEstoqueLancamentos();
        }
        if ('EstoqueLancamentoItem' == $relationName) {
            return $this->initEstoqueLancamentoItems();
        }
        if ('FormaPagamento' == $relationName) {
            return $this->initFormaPagamentos();
        }
        if ('Fornecedor' == $relationName) {
            return $this->initFornecedors();
        }
        if ('ItemComprado' == $relationName) {
            return $this->initItemComprados();
        }
        if ('ItemDeCompra' == $relationName) {
            return $this->initItemDeCompras();
        }
        if ('ServicoCliente' == $relationName) {
            return $this->initServicoClientes();
        }
        if ('ServicoPrestado' == $relationName) {
            return $this->initServicoPrestados();
        }
        if ('Sici' == $relationName) {
            return $this->initSicis();
        }
        if ('SysPerfil' == $relationName) {
            return $this->initSysPerfils();
        }
        if ('SysProcessos' == $relationName) {
            return $this->initSysProcessoss();
        }
        if ('SysUsuarioPerfil' == $relationName) {
            return $this->initSysUsuarioPerfils();
        }
        if ('Telefone' == $relationName) {
            return $this->initTelefones();
        }
        if ('Tributo' == $relationName) {
            return $this->initTributos();
        }
    }

    /**
     * Clears out the collBaixas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBaixas()
     */
    public function clearBaixas()
    {
        $this->collBaixas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBaixas collection loaded partially.
     */
    public function resetPartialBaixas($v = true)
    {
        $this->collBaixasPartial = $v;
    }

    /**
     * Initializes the collBaixas collection.
     *
     * By default this just sets the collBaixas collection to an empty array (like clearcollBaixas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBaixas($overrideExisting = true)
    {
        if (null !== $this->collBaixas && !$overrideExisting) {
            return;
        }

        $collectionClassName = BaixaTableMap::getTableMap()->getCollectionClassName();

        $this->collBaixas = new $collectionClassName;
        $this->collBaixas->setModel('\ImaTelecomBundle\Model\Baixa');
    }

    /**
     * Gets an array of ChildBaixa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBaixa[] List of ChildBaixa objects
     * @throws PropelException
     */
    public function getBaixas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixasPartial && !$this->isNew();
        if (null === $this->collBaixas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBaixas) {
                // return empty collection
                $this->initBaixas();
            } else {
                $collBaixas = ChildBaixaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBaixasPartial && count($collBaixas)) {
                        $this->initBaixas(false);

                        foreach ($collBaixas as $obj) {
                            if (false == $this->collBaixas->contains($obj)) {
                                $this->collBaixas->append($obj);
                            }
                        }

                        $this->collBaixasPartial = true;
                    }

                    return $collBaixas;
                }

                if ($partial && $this->collBaixas) {
                    foreach ($this->collBaixas as $obj) {
                        if ($obj->isNew()) {
                            $collBaixas[] = $obj;
                        }
                    }
                }

                $this->collBaixas = $collBaixas;
                $this->collBaixasPartial = false;
            }
        }

        return $this->collBaixas;
    }

    /**
     * Sets a collection of ChildBaixa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $baixas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBaixas(Collection $baixas, ConnectionInterface $con = null)
    {
        /** @var ChildBaixa[] $baixasToDelete */
        $baixasToDelete = $this->getBaixas(new Criteria(), $con)->diff($baixas);


        $this->baixasScheduledForDeletion = $baixasToDelete;

        foreach ($baixasToDelete as $baixaRemoved) {
            $baixaRemoved->setUsuario(null);
        }

        $this->collBaixas = null;
        foreach ($baixas as $baixa) {
            $this->addBaixa($baixa);
        }

        $this->collBaixas = $baixas;
        $this->collBaixasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Baixa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Baixa objects.
     * @throws PropelException
     */
    public function countBaixas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixasPartial && !$this->isNew();
        if (null === $this->collBaixas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBaixas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBaixas());
            }

            $query = ChildBaixaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBaixas);
    }

    /**
     * Method called to associate a ChildBaixa object to this object
     * through the ChildBaixa foreign key attribute.
     *
     * @param  ChildBaixa $l ChildBaixa
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addBaixa(ChildBaixa $l)
    {
        if ($this->collBaixas === null) {
            $this->initBaixas();
            $this->collBaixasPartial = true;
        }

        if (!$this->collBaixas->contains($l)) {
            $this->doAddBaixa($l);

            if ($this->baixasScheduledForDeletion and $this->baixasScheduledForDeletion->contains($l)) {
                $this->baixasScheduledForDeletion->remove($this->baixasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBaixa $baixa The ChildBaixa object to add.
     */
    protected function doAddBaixa(ChildBaixa $baixa)
    {
        $this->collBaixas[]= $baixa;
        $baixa->setUsuario($this);
    }

    /**
     * @param  ChildBaixa $baixa The ChildBaixa object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBaixa(ChildBaixa $baixa)
    {
        if ($this->getBaixas()->contains($baixa)) {
            $pos = $this->collBaixas->search($baixa);
            $this->collBaixas->remove($pos);
            if (null === $this->baixasScheduledForDeletion) {
                $this->baixasScheduledForDeletion = clone $this->collBaixas;
                $this->baixasScheduledForDeletion->clear();
            }
            $this->baixasScheduledForDeletion[]= clone $baixa;
            $baixa->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Baixas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixa[] List of ChildBaixa objects
     */
    public function getBaixasJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBaixas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Baixas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixa[] List of ChildBaixa objects
     */
    public function getBaixasJoinContaCaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaQuery::create(null, $criteria);
        $query->joinWith('ContaCaixa', $joinBehavior);

        return $this->getBaixas($query, $con);
    }

    /**
     * Clears out the collBaixaEstornos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBaixaEstornos()
     */
    public function clearBaixaEstornos()
    {
        $this->collBaixaEstornos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBaixaEstornos collection loaded partially.
     */
    public function resetPartialBaixaEstornos($v = true)
    {
        $this->collBaixaEstornosPartial = $v;
    }

    /**
     * Initializes the collBaixaEstornos collection.
     *
     * By default this just sets the collBaixaEstornos collection to an empty array (like clearcollBaixaEstornos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBaixaEstornos($overrideExisting = true)
    {
        if (null !== $this->collBaixaEstornos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BaixaEstornoTableMap::getTableMap()->getCollectionClassName();

        $this->collBaixaEstornos = new $collectionClassName;
        $this->collBaixaEstornos->setModel('\ImaTelecomBundle\Model\BaixaEstorno');
    }

    /**
     * Gets an array of ChildBaixaEstorno objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBaixaEstorno[] List of ChildBaixaEstorno objects
     * @throws PropelException
     */
    public function getBaixaEstornos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixaEstornosPartial && !$this->isNew();
        if (null === $this->collBaixaEstornos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBaixaEstornos) {
                // return empty collection
                $this->initBaixaEstornos();
            } else {
                $collBaixaEstornos = ChildBaixaEstornoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBaixaEstornosPartial && count($collBaixaEstornos)) {
                        $this->initBaixaEstornos(false);

                        foreach ($collBaixaEstornos as $obj) {
                            if (false == $this->collBaixaEstornos->contains($obj)) {
                                $this->collBaixaEstornos->append($obj);
                            }
                        }

                        $this->collBaixaEstornosPartial = true;
                    }

                    return $collBaixaEstornos;
                }

                if ($partial && $this->collBaixaEstornos) {
                    foreach ($this->collBaixaEstornos as $obj) {
                        if ($obj->isNew()) {
                            $collBaixaEstornos[] = $obj;
                        }
                    }
                }

                $this->collBaixaEstornos = $collBaixaEstornos;
                $this->collBaixaEstornosPartial = false;
            }
        }

        return $this->collBaixaEstornos;
    }

    /**
     * Sets a collection of ChildBaixaEstorno objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $baixaEstornos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBaixaEstornos(Collection $baixaEstornos, ConnectionInterface $con = null)
    {
        /** @var ChildBaixaEstorno[] $baixaEstornosToDelete */
        $baixaEstornosToDelete = $this->getBaixaEstornos(new Criteria(), $con)->diff($baixaEstornos);


        $this->baixaEstornosScheduledForDeletion = $baixaEstornosToDelete;

        foreach ($baixaEstornosToDelete as $baixaEstornoRemoved) {
            $baixaEstornoRemoved->setUsuario(null);
        }

        $this->collBaixaEstornos = null;
        foreach ($baixaEstornos as $baixaEstorno) {
            $this->addBaixaEstorno($baixaEstorno);
        }

        $this->collBaixaEstornos = $baixaEstornos;
        $this->collBaixaEstornosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaixaEstorno objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaixaEstorno objects.
     * @throws PropelException
     */
    public function countBaixaEstornos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixaEstornosPartial && !$this->isNew();
        if (null === $this->collBaixaEstornos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBaixaEstornos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBaixaEstornos());
            }

            $query = ChildBaixaEstornoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBaixaEstornos);
    }

    /**
     * Method called to associate a ChildBaixaEstorno object to this object
     * through the ChildBaixaEstorno foreign key attribute.
     *
     * @param  ChildBaixaEstorno $l ChildBaixaEstorno
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addBaixaEstorno(ChildBaixaEstorno $l)
    {
        if ($this->collBaixaEstornos === null) {
            $this->initBaixaEstornos();
            $this->collBaixaEstornosPartial = true;
        }

        if (!$this->collBaixaEstornos->contains($l)) {
            $this->doAddBaixaEstorno($l);

            if ($this->baixaEstornosScheduledForDeletion and $this->baixaEstornosScheduledForDeletion->contains($l)) {
                $this->baixaEstornosScheduledForDeletion->remove($this->baixaEstornosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBaixaEstorno $baixaEstorno The ChildBaixaEstorno object to add.
     */
    protected function doAddBaixaEstorno(ChildBaixaEstorno $baixaEstorno)
    {
        $this->collBaixaEstornos[]= $baixaEstorno;
        $baixaEstorno->setUsuario($this);
    }

    /**
     * @param  ChildBaixaEstorno $baixaEstorno The ChildBaixaEstorno object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBaixaEstorno(ChildBaixaEstorno $baixaEstorno)
    {
        if ($this->getBaixaEstornos()->contains($baixaEstorno)) {
            $pos = $this->collBaixaEstornos->search($baixaEstorno);
            $this->collBaixaEstornos->remove($pos);
            if (null === $this->baixaEstornosScheduledForDeletion) {
                $this->baixaEstornosScheduledForDeletion = clone $this->collBaixaEstornos;
                $this->baixaEstornosScheduledForDeletion->clear();
            }
            $this->baixaEstornosScheduledForDeletion[]= clone $baixaEstorno;
            $baixaEstorno->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BaixaEstornos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixaEstorno[] List of ChildBaixaEstorno objects
     */
    public function getBaixaEstornosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaEstornoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBaixaEstornos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BaixaEstornos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixaEstorno[] List of ChildBaixaEstorno objects
     */
    public function getBaixaEstornosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaEstornoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBaixaEstornos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BaixaEstornos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixaEstorno[] List of ChildBaixaEstorno objects
     */
    public function getBaixaEstornosJoinContaCaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaEstornoQuery::create(null, $criteria);
        $query->joinWith('ContaCaixa', $joinBehavior);

        return $this->getBaixaEstornos($query, $con);
    }

    /**
     * Clears out the collBaixaMovimentos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBaixaMovimentos()
     */
    public function clearBaixaMovimentos()
    {
        $this->collBaixaMovimentos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBaixaMovimentos collection loaded partially.
     */
    public function resetPartialBaixaMovimentos($v = true)
    {
        $this->collBaixaMovimentosPartial = $v;
    }

    /**
     * Initializes the collBaixaMovimentos collection.
     *
     * By default this just sets the collBaixaMovimentos collection to an empty array (like clearcollBaixaMovimentos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBaixaMovimentos($overrideExisting = true)
    {
        if (null !== $this->collBaixaMovimentos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BaixaMovimentoTableMap::getTableMap()->getCollectionClassName();

        $this->collBaixaMovimentos = new $collectionClassName;
        $this->collBaixaMovimentos->setModel('\ImaTelecomBundle\Model\BaixaMovimento');
    }

    /**
     * Gets an array of ChildBaixaMovimento objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBaixaMovimento[] List of ChildBaixaMovimento objects
     * @throws PropelException
     */
    public function getBaixaMovimentos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixaMovimentosPartial && !$this->isNew();
        if (null === $this->collBaixaMovimentos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBaixaMovimentos) {
                // return empty collection
                $this->initBaixaMovimentos();
            } else {
                $collBaixaMovimentos = ChildBaixaMovimentoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBaixaMovimentosPartial && count($collBaixaMovimentos)) {
                        $this->initBaixaMovimentos(false);

                        foreach ($collBaixaMovimentos as $obj) {
                            if (false == $this->collBaixaMovimentos->contains($obj)) {
                                $this->collBaixaMovimentos->append($obj);
                            }
                        }

                        $this->collBaixaMovimentosPartial = true;
                    }

                    return $collBaixaMovimentos;
                }

                if ($partial && $this->collBaixaMovimentos) {
                    foreach ($this->collBaixaMovimentos as $obj) {
                        if ($obj->isNew()) {
                            $collBaixaMovimentos[] = $obj;
                        }
                    }
                }

                $this->collBaixaMovimentos = $collBaixaMovimentos;
                $this->collBaixaMovimentosPartial = false;
            }
        }

        return $this->collBaixaMovimentos;
    }

    /**
     * Sets a collection of ChildBaixaMovimento objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $baixaMovimentos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBaixaMovimentos(Collection $baixaMovimentos, ConnectionInterface $con = null)
    {
        /** @var ChildBaixaMovimento[] $baixaMovimentosToDelete */
        $baixaMovimentosToDelete = $this->getBaixaMovimentos(new Criteria(), $con)->diff($baixaMovimentos);


        $this->baixaMovimentosScheduledForDeletion = $baixaMovimentosToDelete;

        foreach ($baixaMovimentosToDelete as $baixaMovimentoRemoved) {
            $baixaMovimentoRemoved->setUsuario(null);
        }

        $this->collBaixaMovimentos = null;
        foreach ($baixaMovimentos as $baixaMovimento) {
            $this->addBaixaMovimento($baixaMovimento);
        }

        $this->collBaixaMovimentos = $baixaMovimentos;
        $this->collBaixaMovimentosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaixaMovimento objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaixaMovimento objects.
     * @throws PropelException
     */
    public function countBaixaMovimentos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixaMovimentosPartial && !$this->isNew();
        if (null === $this->collBaixaMovimentos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBaixaMovimentos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBaixaMovimentos());
            }

            $query = ChildBaixaMovimentoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBaixaMovimentos);
    }

    /**
     * Method called to associate a ChildBaixaMovimento object to this object
     * through the ChildBaixaMovimento foreign key attribute.
     *
     * @param  ChildBaixaMovimento $l ChildBaixaMovimento
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addBaixaMovimento(ChildBaixaMovimento $l)
    {
        if ($this->collBaixaMovimentos === null) {
            $this->initBaixaMovimentos();
            $this->collBaixaMovimentosPartial = true;
        }

        if (!$this->collBaixaMovimentos->contains($l)) {
            $this->doAddBaixaMovimento($l);

            if ($this->baixaMovimentosScheduledForDeletion and $this->baixaMovimentosScheduledForDeletion->contains($l)) {
                $this->baixaMovimentosScheduledForDeletion->remove($this->baixaMovimentosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBaixaMovimento $baixaMovimento The ChildBaixaMovimento object to add.
     */
    protected function doAddBaixaMovimento(ChildBaixaMovimento $baixaMovimento)
    {
        $this->collBaixaMovimentos[]= $baixaMovimento;
        $baixaMovimento->setUsuario($this);
    }

    /**
     * @param  ChildBaixaMovimento $baixaMovimento The ChildBaixaMovimento object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBaixaMovimento(ChildBaixaMovimento $baixaMovimento)
    {
        if ($this->getBaixaMovimentos()->contains($baixaMovimento)) {
            $pos = $this->collBaixaMovimentos->search($baixaMovimento);
            $this->collBaixaMovimentos->remove($pos);
            if (null === $this->baixaMovimentosScheduledForDeletion) {
                $this->baixaMovimentosScheduledForDeletion = clone $this->collBaixaMovimentos;
                $this->baixaMovimentosScheduledForDeletion->clear();
            }
            $this->baixaMovimentosScheduledForDeletion[]= clone $baixaMovimento;
            $baixaMovimento->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BaixaMovimentos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixaMovimento[] List of ChildBaixaMovimento objects
     */
    public function getBaixaMovimentosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaMovimentoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBaixaMovimentos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BaixaMovimentos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixaMovimento[] List of ChildBaixaMovimento objects
     */
    public function getBaixaMovimentosJoinFormaPagamento(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaMovimentoQuery::create(null, $criteria);
        $query->joinWith('FormaPagamento', $joinBehavior);

        return $this->getBaixaMovimentos($query, $con);
    }

    /**
     * Clears out the collBancos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBancos()
     */
    public function clearBancos()
    {
        $this->collBancos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBancos collection loaded partially.
     */
    public function resetPartialBancos($v = true)
    {
        $this->collBancosPartial = $v;
    }

    /**
     * Initializes the collBancos collection.
     *
     * By default this just sets the collBancos collection to an empty array (like clearcollBancos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBancos($overrideExisting = true)
    {
        if (null !== $this->collBancos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BancoTableMap::getTableMap()->getCollectionClassName();

        $this->collBancos = new $collectionClassName;
        $this->collBancos->setModel('\ImaTelecomBundle\Model\Banco');
    }

    /**
     * Gets an array of ChildBanco objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBanco[] List of ChildBanco objects
     * @throws PropelException
     */
    public function getBancos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBancosPartial && !$this->isNew();
        if (null === $this->collBancos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBancos) {
                // return empty collection
                $this->initBancos();
            } else {
                $collBancos = ChildBancoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBancosPartial && count($collBancos)) {
                        $this->initBancos(false);

                        foreach ($collBancos as $obj) {
                            if (false == $this->collBancos->contains($obj)) {
                                $this->collBancos->append($obj);
                            }
                        }

                        $this->collBancosPartial = true;
                    }

                    return $collBancos;
                }

                if ($partial && $this->collBancos) {
                    foreach ($this->collBancos as $obj) {
                        if ($obj->isNew()) {
                            $collBancos[] = $obj;
                        }
                    }
                }

                $this->collBancos = $collBancos;
                $this->collBancosPartial = false;
            }
        }

        return $this->collBancos;
    }

    /**
     * Sets a collection of ChildBanco objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bancos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBancos(Collection $bancos, ConnectionInterface $con = null)
    {
        /** @var ChildBanco[] $bancosToDelete */
        $bancosToDelete = $this->getBancos(new Criteria(), $con)->diff($bancos);


        $this->bancosScheduledForDeletion = $bancosToDelete;

        foreach ($bancosToDelete as $bancoRemoved) {
            $bancoRemoved->setUsuario(null);
        }

        $this->collBancos = null;
        foreach ($bancos as $banco) {
            $this->addBanco($banco);
        }

        $this->collBancos = $bancos;
        $this->collBancosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Banco objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Banco objects.
     * @throws PropelException
     */
    public function countBancos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBancosPartial && !$this->isNew();
        if (null === $this->collBancos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBancos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBancos());
            }

            $query = ChildBancoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBancos);
    }

    /**
     * Method called to associate a ChildBanco object to this object
     * through the ChildBanco foreign key attribute.
     *
     * @param  ChildBanco $l ChildBanco
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addBanco(ChildBanco $l)
    {
        if ($this->collBancos === null) {
            $this->initBancos();
            $this->collBancosPartial = true;
        }

        if (!$this->collBancos->contains($l)) {
            $this->doAddBanco($l);

            if ($this->bancosScheduledForDeletion and $this->bancosScheduledForDeletion->contains($l)) {
                $this->bancosScheduledForDeletion->remove($this->bancosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBanco $banco The ChildBanco object to add.
     */
    protected function doAddBanco(ChildBanco $banco)
    {
        $this->collBancos[]= $banco;
        $banco->setUsuario($this);
    }

    /**
     * @param  ChildBanco $banco The ChildBanco object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBanco(ChildBanco $banco)
    {
        if ($this->getBancos()->contains($banco)) {
            $pos = $this->collBancos->search($banco);
            $this->collBancos->remove($pos);
            if (null === $this->bancosScheduledForDeletion) {
                $this->bancosScheduledForDeletion = clone $this->collBancos;
                $this->bancosScheduledForDeletion->clear();
            }
            $this->bancosScheduledForDeletion[]= clone $banco;
            $banco->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Bancos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBanco[] List of ChildBanco objects
     */
    public function getBancosJoinLayoutBancario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBancoQuery::create(null, $criteria);
        $query->joinWith('LayoutBancario', $joinBehavior);

        return $this->getBancos($query, $con);
    }

    /**
     * Clears out the collBancoAgencias collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBancoAgencias()
     */
    public function clearBancoAgencias()
    {
        $this->collBancoAgencias = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBancoAgencias collection loaded partially.
     */
    public function resetPartialBancoAgencias($v = true)
    {
        $this->collBancoAgenciasPartial = $v;
    }

    /**
     * Initializes the collBancoAgencias collection.
     *
     * By default this just sets the collBancoAgencias collection to an empty array (like clearcollBancoAgencias());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBancoAgencias($overrideExisting = true)
    {
        if (null !== $this->collBancoAgencias && !$overrideExisting) {
            return;
        }

        $collectionClassName = BancoAgenciaTableMap::getTableMap()->getCollectionClassName();

        $this->collBancoAgencias = new $collectionClassName;
        $this->collBancoAgencias->setModel('\ImaTelecomBundle\Model\BancoAgencia');
    }

    /**
     * Gets an array of ChildBancoAgencia objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBancoAgencia[] List of ChildBancoAgencia objects
     * @throws PropelException
     */
    public function getBancoAgencias(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBancoAgenciasPartial && !$this->isNew();
        if (null === $this->collBancoAgencias || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBancoAgencias) {
                // return empty collection
                $this->initBancoAgencias();
            } else {
                $collBancoAgencias = ChildBancoAgenciaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBancoAgenciasPartial && count($collBancoAgencias)) {
                        $this->initBancoAgencias(false);

                        foreach ($collBancoAgencias as $obj) {
                            if (false == $this->collBancoAgencias->contains($obj)) {
                                $this->collBancoAgencias->append($obj);
                            }
                        }

                        $this->collBancoAgenciasPartial = true;
                    }

                    return $collBancoAgencias;
                }

                if ($partial && $this->collBancoAgencias) {
                    foreach ($this->collBancoAgencias as $obj) {
                        if ($obj->isNew()) {
                            $collBancoAgencias[] = $obj;
                        }
                    }
                }

                $this->collBancoAgencias = $collBancoAgencias;
                $this->collBancoAgenciasPartial = false;
            }
        }

        return $this->collBancoAgencias;
    }

    /**
     * Sets a collection of ChildBancoAgencia objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bancoAgencias A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBancoAgencias(Collection $bancoAgencias, ConnectionInterface $con = null)
    {
        /** @var ChildBancoAgencia[] $bancoAgenciasToDelete */
        $bancoAgenciasToDelete = $this->getBancoAgencias(new Criteria(), $con)->diff($bancoAgencias);


        $this->bancoAgenciasScheduledForDeletion = $bancoAgenciasToDelete;

        foreach ($bancoAgenciasToDelete as $bancoAgenciaRemoved) {
            $bancoAgenciaRemoved->setUsuario(null);
        }

        $this->collBancoAgencias = null;
        foreach ($bancoAgencias as $bancoAgencia) {
            $this->addBancoAgencia($bancoAgencia);
        }

        $this->collBancoAgencias = $bancoAgencias;
        $this->collBancoAgenciasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BancoAgencia objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BancoAgencia objects.
     * @throws PropelException
     */
    public function countBancoAgencias(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBancoAgenciasPartial && !$this->isNew();
        if (null === $this->collBancoAgencias || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBancoAgencias) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBancoAgencias());
            }

            $query = ChildBancoAgenciaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBancoAgencias);
    }

    /**
     * Method called to associate a ChildBancoAgencia object to this object
     * through the ChildBancoAgencia foreign key attribute.
     *
     * @param  ChildBancoAgencia $l ChildBancoAgencia
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addBancoAgencia(ChildBancoAgencia $l)
    {
        if ($this->collBancoAgencias === null) {
            $this->initBancoAgencias();
            $this->collBancoAgenciasPartial = true;
        }

        if (!$this->collBancoAgencias->contains($l)) {
            $this->doAddBancoAgencia($l);

            if ($this->bancoAgenciasScheduledForDeletion and $this->bancoAgenciasScheduledForDeletion->contains($l)) {
                $this->bancoAgenciasScheduledForDeletion->remove($this->bancoAgenciasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBancoAgencia $bancoAgencia The ChildBancoAgencia object to add.
     */
    protected function doAddBancoAgencia(ChildBancoAgencia $bancoAgencia)
    {
        $this->collBancoAgencias[]= $bancoAgencia;
        $bancoAgencia->setUsuario($this);
    }

    /**
     * @param  ChildBancoAgencia $bancoAgencia The ChildBancoAgencia object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBancoAgencia(ChildBancoAgencia $bancoAgencia)
    {
        if ($this->getBancoAgencias()->contains($bancoAgencia)) {
            $pos = $this->collBancoAgencias->search($bancoAgencia);
            $this->collBancoAgencias->remove($pos);
            if (null === $this->bancoAgenciasScheduledForDeletion) {
                $this->bancoAgenciasScheduledForDeletion = clone $this->collBancoAgencias;
                $this->bancoAgenciasScheduledForDeletion->clear();
            }
            $this->bancoAgenciasScheduledForDeletion[]= clone $bancoAgencia;
            $bancoAgencia->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BancoAgencias from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBancoAgencia[] List of ChildBancoAgencia objects
     */
    public function getBancoAgenciasJoinEnderecoAgencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBancoAgenciaQuery::create(null, $criteria);
        $query->joinWith('EnderecoAgencia', $joinBehavior);

        return $this->getBancoAgencias($query, $con);
    }

    /**
     * Clears out the collBancoAgenciaContas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBancoAgenciaContas()
     */
    public function clearBancoAgenciaContas()
    {
        $this->collBancoAgenciaContas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBancoAgenciaContas collection loaded partially.
     */
    public function resetPartialBancoAgenciaContas($v = true)
    {
        $this->collBancoAgenciaContasPartial = $v;
    }

    /**
     * Initializes the collBancoAgenciaContas collection.
     *
     * By default this just sets the collBancoAgenciaContas collection to an empty array (like clearcollBancoAgenciaContas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBancoAgenciaContas($overrideExisting = true)
    {
        if (null !== $this->collBancoAgenciaContas && !$overrideExisting) {
            return;
        }

        $collectionClassName = BancoAgenciaContaTableMap::getTableMap()->getCollectionClassName();

        $this->collBancoAgenciaContas = new $collectionClassName;
        $this->collBancoAgenciaContas->setModel('\ImaTelecomBundle\Model\BancoAgenciaConta');
    }

    /**
     * Gets an array of ChildBancoAgenciaConta objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBancoAgenciaConta[] List of ChildBancoAgenciaConta objects
     * @throws PropelException
     */
    public function getBancoAgenciaContas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBancoAgenciaContasPartial && !$this->isNew();
        if (null === $this->collBancoAgenciaContas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBancoAgenciaContas) {
                // return empty collection
                $this->initBancoAgenciaContas();
            } else {
                $collBancoAgenciaContas = ChildBancoAgenciaContaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBancoAgenciaContasPartial && count($collBancoAgenciaContas)) {
                        $this->initBancoAgenciaContas(false);

                        foreach ($collBancoAgenciaContas as $obj) {
                            if (false == $this->collBancoAgenciaContas->contains($obj)) {
                                $this->collBancoAgenciaContas->append($obj);
                            }
                        }

                        $this->collBancoAgenciaContasPartial = true;
                    }

                    return $collBancoAgenciaContas;
                }

                if ($partial && $this->collBancoAgenciaContas) {
                    foreach ($this->collBancoAgenciaContas as $obj) {
                        if ($obj->isNew()) {
                            $collBancoAgenciaContas[] = $obj;
                        }
                    }
                }

                $this->collBancoAgenciaContas = $collBancoAgenciaContas;
                $this->collBancoAgenciaContasPartial = false;
            }
        }

        return $this->collBancoAgenciaContas;
    }

    /**
     * Sets a collection of ChildBancoAgenciaConta objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $bancoAgenciaContas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBancoAgenciaContas(Collection $bancoAgenciaContas, ConnectionInterface $con = null)
    {
        /** @var ChildBancoAgenciaConta[] $bancoAgenciaContasToDelete */
        $bancoAgenciaContasToDelete = $this->getBancoAgenciaContas(new Criteria(), $con)->diff($bancoAgenciaContas);


        $this->bancoAgenciaContasScheduledForDeletion = $bancoAgenciaContasToDelete;

        foreach ($bancoAgenciaContasToDelete as $bancoAgenciaContaRemoved) {
            $bancoAgenciaContaRemoved->setUsuario(null);
        }

        $this->collBancoAgenciaContas = null;
        foreach ($bancoAgenciaContas as $bancoAgenciaConta) {
            $this->addBancoAgenciaConta($bancoAgenciaConta);
        }

        $this->collBancoAgenciaContas = $bancoAgenciaContas;
        $this->collBancoAgenciaContasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BancoAgenciaConta objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BancoAgenciaConta objects.
     * @throws PropelException
     */
    public function countBancoAgenciaContas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBancoAgenciaContasPartial && !$this->isNew();
        if (null === $this->collBancoAgenciaContas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBancoAgenciaContas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBancoAgenciaContas());
            }

            $query = ChildBancoAgenciaContaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBancoAgenciaContas);
    }

    /**
     * Method called to associate a ChildBancoAgenciaConta object to this object
     * through the ChildBancoAgenciaConta foreign key attribute.
     *
     * @param  ChildBancoAgenciaConta $l ChildBancoAgenciaConta
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addBancoAgenciaConta(ChildBancoAgenciaConta $l)
    {
        if ($this->collBancoAgenciaContas === null) {
            $this->initBancoAgenciaContas();
            $this->collBancoAgenciaContasPartial = true;
        }

        if (!$this->collBancoAgenciaContas->contains($l)) {
            $this->doAddBancoAgenciaConta($l);

            if ($this->bancoAgenciaContasScheduledForDeletion and $this->bancoAgenciaContasScheduledForDeletion->contains($l)) {
                $this->bancoAgenciaContasScheduledForDeletion->remove($this->bancoAgenciaContasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBancoAgenciaConta $bancoAgenciaConta The ChildBancoAgenciaConta object to add.
     */
    protected function doAddBancoAgenciaConta(ChildBancoAgenciaConta $bancoAgenciaConta)
    {
        $this->collBancoAgenciaContas[]= $bancoAgenciaConta;
        $bancoAgenciaConta->setUsuario($this);
    }

    /**
     * @param  ChildBancoAgenciaConta $bancoAgenciaConta The ChildBancoAgenciaConta object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBancoAgenciaConta(ChildBancoAgenciaConta $bancoAgenciaConta)
    {
        if ($this->getBancoAgenciaContas()->contains($bancoAgenciaConta)) {
            $pos = $this->collBancoAgenciaContas->search($bancoAgenciaConta);
            $this->collBancoAgenciaContas->remove($pos);
            if (null === $this->bancoAgenciaContasScheduledForDeletion) {
                $this->bancoAgenciaContasScheduledForDeletion = clone $this->collBancoAgenciaContas;
                $this->bancoAgenciaContasScheduledForDeletion->clear();
            }
            $this->bancoAgenciaContasScheduledForDeletion[]= clone $bancoAgenciaConta;
            $bancoAgenciaConta->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BancoAgenciaContas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBancoAgenciaConta[] List of ChildBancoAgenciaConta objects
     */
    public function getBancoAgenciaContasJoinBancoAgencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBancoAgenciaContaQuery::create(null, $criteria);
        $query->joinWith('BancoAgencia', $joinBehavior);

        return $this->getBancoAgenciaContas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BancoAgenciaContas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBancoAgenciaConta[] List of ChildBancoAgenciaConta objects
     */
    public function getBancoAgenciaContasJoinBanco(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBancoAgenciaContaQuery::create(null, $criteria);
        $query->joinWith('Banco', $joinBehavior);

        return $this->getBancoAgenciaContas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BancoAgenciaContas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBancoAgenciaConta[] List of ChildBancoAgenciaConta objects
     */
    public function getBancoAgenciaContasJoinConvenio(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBancoAgenciaContaQuery::create(null, $criteria);
        $query->joinWith('Convenio', $joinBehavior);

        return $this->getBancoAgenciaContas($query, $con);
    }

    /**
     * Clears out the collBoletos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletos()
     */
    public function clearBoletos()
    {
        $this->collBoletos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletos collection loaded partially.
     */
    public function resetPartialBoletos($v = true)
    {
        $this->collBoletosPartial = $v;
    }

    /**
     * Initializes the collBoletos collection.
     *
     * By default this just sets the collBoletos collection to an empty array (like clearcollBoletos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletos($overrideExisting = true)
    {
        if (null !== $this->collBoletos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletos = new $collectionClassName;
        $this->collBoletos->setModel('\ImaTelecomBundle\Model\Boleto');
    }

    /**
     * Gets an array of ChildBoleto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     * @throws PropelException
     */
    public function getBoletos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                // return empty collection
                $this->initBoletos();
            } else {
                $collBoletos = ChildBoletoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletosPartial && count($collBoletos)) {
                        $this->initBoletos(false);

                        foreach ($collBoletos as $obj) {
                            if (false == $this->collBoletos->contains($obj)) {
                                $this->collBoletos->append($obj);
                            }
                        }

                        $this->collBoletosPartial = true;
                    }

                    return $collBoletos;
                }

                if ($partial && $this->collBoletos) {
                    foreach ($this->collBoletos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletos[] = $obj;
                        }
                    }
                }

                $this->collBoletos = $collBoletos;
                $this->collBoletosPartial = false;
            }
        }

        return $this->collBoletos;
    }

    /**
     * Sets a collection of ChildBoleto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBoletos(Collection $boletos, ConnectionInterface $con = null)
    {
        /** @var ChildBoleto[] $boletosToDelete */
        $boletosToDelete = $this->getBoletos(new Criteria(), $con)->diff($boletos);


        $this->boletosScheduledForDeletion = $boletosToDelete;

        foreach ($boletosToDelete as $boletoRemoved) {
            $boletoRemoved->setUsuario(null);
        }

        $this->collBoletos = null;
        foreach ($boletos as $boleto) {
            $this->addBoleto($boleto);
        }

        $this->collBoletos = $boletos;
        $this->collBoletosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Boleto objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Boleto objects.
     * @throws PropelException
     */
    public function countBoletos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletos());
            }

            $query = ChildBoletoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBoletos);
    }

    /**
     * Method called to associate a ChildBoleto object to this object
     * through the ChildBoleto foreign key attribute.
     *
     * @param  ChildBoleto $l ChildBoleto
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addBoleto(ChildBoleto $l)
    {
        if ($this->collBoletos === null) {
            $this->initBoletos();
            $this->collBoletosPartial = true;
        }

        if (!$this->collBoletos->contains($l)) {
            $this->doAddBoleto($l);

            if ($this->boletosScheduledForDeletion and $this->boletosScheduledForDeletion->contains($l)) {
                $this->boletosScheduledForDeletion->remove($this->boletosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoleto $boleto The ChildBoleto object to add.
     */
    protected function doAddBoleto(ChildBoleto $boleto)
    {
        $this->collBoletos[]= $boleto;
        $boleto->setUsuario($this);
    }

    /**
     * @param  ChildBoleto $boleto The ChildBoleto object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBoleto(ChildBoleto $boleto)
    {
        if ($this->getBoletos()->contains($boleto)) {
            $pos = $this->collBoletos->search($boleto);
            $this->collBoletos->remove($pos);
            if (null === $this->boletosScheduledForDeletion) {
                $this->boletosScheduledForDeletion = clone $this->collBoletos;
                $this->boletosScheduledForDeletion->clear();
            }
            $this->boletosScheduledForDeletion[]= clone $boleto;
            $boleto->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinBaixaEstorno(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('BaixaEstorno', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinFornecedor(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Fornecedor', $joinBehavior);

        return $this->getBoletos($query, $con);
    }

    /**
     * Clears out the collBoletoBaixaHistoricos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletoBaixaHistoricos()
     */
    public function clearBoletoBaixaHistoricos()
    {
        $this->collBoletoBaixaHistoricos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletoBaixaHistoricos collection loaded partially.
     */
    public function resetPartialBoletoBaixaHistoricos($v = true)
    {
        $this->collBoletoBaixaHistoricosPartial = $v;
    }

    /**
     * Initializes the collBoletoBaixaHistoricos collection.
     *
     * By default this just sets the collBoletoBaixaHistoricos collection to an empty array (like clearcollBoletoBaixaHistoricos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletoBaixaHistoricos($overrideExisting = true)
    {
        if (null !== $this->collBoletoBaixaHistoricos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoBaixaHistoricoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletoBaixaHistoricos = new $collectionClassName;
        $this->collBoletoBaixaHistoricos->setModel('\ImaTelecomBundle\Model\BoletoBaixaHistorico');
    }

    /**
     * Gets an array of ChildBoletoBaixaHistorico objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     * @throws PropelException
     */
    public function getBoletoBaixaHistoricos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                // return empty collection
                $this->initBoletoBaixaHistoricos();
            } else {
                $collBoletoBaixaHistoricos = ChildBoletoBaixaHistoricoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletoBaixaHistoricosPartial && count($collBoletoBaixaHistoricos)) {
                        $this->initBoletoBaixaHistoricos(false);

                        foreach ($collBoletoBaixaHistoricos as $obj) {
                            if (false == $this->collBoletoBaixaHistoricos->contains($obj)) {
                                $this->collBoletoBaixaHistoricos->append($obj);
                            }
                        }

                        $this->collBoletoBaixaHistoricosPartial = true;
                    }

                    return $collBoletoBaixaHistoricos;
                }

                if ($partial && $this->collBoletoBaixaHistoricos) {
                    foreach ($this->collBoletoBaixaHistoricos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletoBaixaHistoricos[] = $obj;
                        }
                    }
                }

                $this->collBoletoBaixaHistoricos = $collBoletoBaixaHistoricos;
                $this->collBoletoBaixaHistoricosPartial = false;
            }
        }

        return $this->collBoletoBaixaHistoricos;
    }

    /**
     * Sets a collection of ChildBoletoBaixaHistorico objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletoBaixaHistoricos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBoletoBaixaHistoricos(Collection $boletoBaixaHistoricos, ConnectionInterface $con = null)
    {
        /** @var ChildBoletoBaixaHistorico[] $boletoBaixaHistoricosToDelete */
        $boletoBaixaHistoricosToDelete = $this->getBoletoBaixaHistoricos(new Criteria(), $con)->diff($boletoBaixaHistoricos);


        $this->boletoBaixaHistoricosScheduledForDeletion = $boletoBaixaHistoricosToDelete;

        foreach ($boletoBaixaHistoricosToDelete as $boletoBaixaHistoricoRemoved) {
            $boletoBaixaHistoricoRemoved->setUsuario(null);
        }

        $this->collBoletoBaixaHistoricos = null;
        foreach ($boletoBaixaHistoricos as $boletoBaixaHistorico) {
            $this->addBoletoBaixaHistorico($boletoBaixaHistorico);
        }

        $this->collBoletoBaixaHistoricos = $boletoBaixaHistoricos;
        $this->collBoletoBaixaHistoricosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BoletoBaixaHistorico objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BoletoBaixaHistorico objects.
     * @throws PropelException
     */
    public function countBoletoBaixaHistoricos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletoBaixaHistoricos());
            }

            $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBoletoBaixaHistoricos);
    }

    /**
     * Method called to associate a ChildBoletoBaixaHistorico object to this object
     * through the ChildBoletoBaixaHistorico foreign key attribute.
     *
     * @param  ChildBoletoBaixaHistorico $l ChildBoletoBaixaHistorico
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addBoletoBaixaHistorico(ChildBoletoBaixaHistorico $l)
    {
        if ($this->collBoletoBaixaHistoricos === null) {
            $this->initBoletoBaixaHistoricos();
            $this->collBoletoBaixaHistoricosPartial = true;
        }

        if (!$this->collBoletoBaixaHistoricos->contains($l)) {
            $this->doAddBoletoBaixaHistorico($l);

            if ($this->boletoBaixaHistoricosScheduledForDeletion and $this->boletoBaixaHistoricosScheduledForDeletion->contains($l)) {
                $this->boletoBaixaHistoricosScheduledForDeletion->remove($this->boletoBaixaHistoricosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to add.
     */
    protected function doAddBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        $this->collBoletoBaixaHistoricos[]= $boletoBaixaHistorico;
        $boletoBaixaHistorico->setUsuario($this);
    }

    /**
     * @param  ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        if ($this->getBoletoBaixaHistoricos()->contains($boletoBaixaHistorico)) {
            $pos = $this->collBoletoBaixaHistoricos->search($boletoBaixaHistorico);
            $this->collBoletoBaixaHistoricos->remove($pos);
            if (null === $this->boletoBaixaHistoricosScheduledForDeletion) {
                $this->boletoBaixaHistoricosScheduledForDeletion = clone $this->collBoletoBaixaHistoricos;
                $this->boletoBaixaHistoricosScheduledForDeletion->clear();
            }
            $this->boletoBaixaHistoricosScheduledForDeletion[]= clone $boletoBaixaHistorico;
            $boletoBaixaHistorico->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixaEstorno(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('BaixaEstorno', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinLancamentos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Lancamentos', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }

    /**
     * Clears out the collBoletoItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletoItems()
     */
    public function clearBoletoItems()
    {
        $this->collBoletoItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletoItems collection loaded partially.
     */
    public function resetPartialBoletoItems($v = true)
    {
        $this->collBoletoItemsPartial = $v;
    }

    /**
     * Initializes the collBoletoItems collection.
     *
     * By default this just sets the collBoletoItems collection to an empty array (like clearcollBoletoItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletoItems($overrideExisting = true)
    {
        if (null !== $this->collBoletoItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoItemTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletoItems = new $collectionClassName;
        $this->collBoletoItems->setModel('\ImaTelecomBundle\Model\BoletoItem');
    }

    /**
     * Gets an array of ChildBoletoItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoletoItem[] List of ChildBoletoItem objects
     * @throws PropelException
     */
    public function getBoletoItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoItemsPartial && !$this->isNew();
        if (null === $this->collBoletoItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletoItems) {
                // return empty collection
                $this->initBoletoItems();
            } else {
                $collBoletoItems = ChildBoletoItemQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletoItemsPartial && count($collBoletoItems)) {
                        $this->initBoletoItems(false);

                        foreach ($collBoletoItems as $obj) {
                            if (false == $this->collBoletoItems->contains($obj)) {
                                $this->collBoletoItems->append($obj);
                            }
                        }

                        $this->collBoletoItemsPartial = true;
                    }

                    return $collBoletoItems;
                }

                if ($partial && $this->collBoletoItems) {
                    foreach ($this->collBoletoItems as $obj) {
                        if ($obj->isNew()) {
                            $collBoletoItems[] = $obj;
                        }
                    }
                }

                $this->collBoletoItems = $collBoletoItems;
                $this->collBoletoItemsPartial = false;
            }
        }

        return $this->collBoletoItems;
    }

    /**
     * Sets a collection of ChildBoletoItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletoItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBoletoItems(Collection $boletoItems, ConnectionInterface $con = null)
    {
        /** @var ChildBoletoItem[] $boletoItemsToDelete */
        $boletoItemsToDelete = $this->getBoletoItems(new Criteria(), $con)->diff($boletoItems);


        $this->boletoItemsScheduledForDeletion = $boletoItemsToDelete;

        foreach ($boletoItemsToDelete as $boletoItemRemoved) {
            $boletoItemRemoved->setUsuario(null);
        }

        $this->collBoletoItems = null;
        foreach ($boletoItems as $boletoItem) {
            $this->addBoletoItem($boletoItem);
        }

        $this->collBoletoItems = $boletoItems;
        $this->collBoletoItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BoletoItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BoletoItem objects.
     * @throws PropelException
     */
    public function countBoletoItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoItemsPartial && !$this->isNew();
        if (null === $this->collBoletoItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletoItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletoItems());
            }

            $query = ChildBoletoItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBoletoItems);
    }

    /**
     * Method called to associate a ChildBoletoItem object to this object
     * through the ChildBoletoItem foreign key attribute.
     *
     * @param  ChildBoletoItem $l ChildBoletoItem
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addBoletoItem(ChildBoletoItem $l)
    {
        if ($this->collBoletoItems === null) {
            $this->initBoletoItems();
            $this->collBoletoItemsPartial = true;
        }

        if (!$this->collBoletoItems->contains($l)) {
            $this->doAddBoletoItem($l);

            if ($this->boletoItemsScheduledForDeletion and $this->boletoItemsScheduledForDeletion->contains($l)) {
                $this->boletoItemsScheduledForDeletion->remove($this->boletoItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoletoItem $boletoItem The ChildBoletoItem object to add.
     */
    protected function doAddBoletoItem(ChildBoletoItem $boletoItem)
    {
        $this->collBoletoItems[]= $boletoItem;
        $boletoItem->setUsuario($this);
    }

    /**
     * @param  ChildBoletoItem $boletoItem The ChildBoletoItem object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBoletoItem(ChildBoletoItem $boletoItem)
    {
        if ($this->getBoletoItems()->contains($boletoItem)) {
            $pos = $this->collBoletoItems->search($boletoItem);
            $this->collBoletoItems->remove($pos);
            if (null === $this->boletoItemsScheduledForDeletion) {
                $this->boletoItemsScheduledForDeletion = clone $this->collBoletoItems;
                $this->boletoItemsScheduledForDeletion->clear();
            }
            $this->boletoItemsScheduledForDeletion[]= clone $boletoItem;
            $boletoItem->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BoletoItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoItem[] List of ChildBoletoItem objects
     */
    public function getBoletoItemsJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoItemQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getBoletoItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related BoletoItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoItem[] List of ChildBoletoItem objects
     */
    public function getBoletoItemsJoinServicoCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoItemQuery::create(null, $criteria);
        $query->joinWith('ServicoCliente', $joinBehavior);

        return $this->getBoletoItems($query, $con);
    }

    /**
     * Clears out the collCaixas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCaixas()
     */
    public function clearCaixas()
    {
        $this->collCaixas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCaixas collection loaded partially.
     */
    public function resetPartialCaixas($v = true)
    {
        $this->collCaixasPartial = $v;
    }

    /**
     * Initializes the collCaixas collection.
     *
     * By default this just sets the collCaixas collection to an empty array (like clearcollCaixas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCaixas($overrideExisting = true)
    {
        if (null !== $this->collCaixas && !$overrideExisting) {
            return;
        }

        $collectionClassName = CaixaTableMap::getTableMap()->getCollectionClassName();

        $this->collCaixas = new $collectionClassName;
        $this->collCaixas->setModel('\ImaTelecomBundle\Model\Caixa');
    }

    /**
     * Gets an array of ChildCaixa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCaixa[] List of ChildCaixa objects
     * @throws PropelException
     */
    public function getCaixas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCaixasPartial && !$this->isNew();
        if (null === $this->collCaixas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCaixas) {
                // return empty collection
                $this->initCaixas();
            } else {
                $collCaixas = ChildCaixaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCaixasPartial && count($collCaixas)) {
                        $this->initCaixas(false);

                        foreach ($collCaixas as $obj) {
                            if (false == $this->collCaixas->contains($obj)) {
                                $this->collCaixas->append($obj);
                            }
                        }

                        $this->collCaixasPartial = true;
                    }

                    return $collCaixas;
                }

                if ($partial && $this->collCaixas) {
                    foreach ($this->collCaixas as $obj) {
                        if ($obj->isNew()) {
                            $collCaixas[] = $obj;
                        }
                    }
                }

                $this->collCaixas = $collCaixas;
                $this->collCaixasPartial = false;
            }
        }

        return $this->collCaixas;
    }

    /**
     * Sets a collection of ChildCaixa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $caixas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setCaixas(Collection $caixas, ConnectionInterface $con = null)
    {
        /** @var ChildCaixa[] $caixasToDelete */
        $caixasToDelete = $this->getCaixas(new Criteria(), $con)->diff($caixas);


        $this->caixasScheduledForDeletion = $caixasToDelete;

        foreach ($caixasToDelete as $caixaRemoved) {
            $caixaRemoved->setUsuario(null);
        }

        $this->collCaixas = null;
        foreach ($caixas as $caixa) {
            $this->addCaixa($caixa);
        }

        $this->collCaixas = $caixas;
        $this->collCaixasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Caixa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Caixa objects.
     * @throws PropelException
     */
    public function countCaixas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCaixasPartial && !$this->isNew();
        if (null === $this->collCaixas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCaixas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCaixas());
            }

            $query = ChildCaixaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collCaixas);
    }

    /**
     * Method called to associate a ChildCaixa object to this object
     * through the ChildCaixa foreign key attribute.
     *
     * @param  ChildCaixa $l ChildCaixa
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addCaixa(ChildCaixa $l)
    {
        if ($this->collCaixas === null) {
            $this->initCaixas();
            $this->collCaixasPartial = true;
        }

        if (!$this->collCaixas->contains($l)) {
            $this->doAddCaixa($l);

            if ($this->caixasScheduledForDeletion and $this->caixasScheduledForDeletion->contains($l)) {
                $this->caixasScheduledForDeletion->remove($this->caixasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCaixa $caixa The ChildCaixa object to add.
     */
    protected function doAddCaixa(ChildCaixa $caixa)
    {
        $this->collCaixas[]= $caixa;
        $caixa->setUsuario($this);
    }

    /**
     * @param  ChildCaixa $caixa The ChildCaixa object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeCaixa(ChildCaixa $caixa)
    {
        if ($this->getCaixas()->contains($caixa)) {
            $pos = $this->collCaixas->search($caixa);
            $this->collCaixas->remove($pos);
            if (null === $this->caixasScheduledForDeletion) {
                $this->caixasScheduledForDeletion = clone $this->collCaixas;
                $this->caixasScheduledForDeletion->clear();
            }
            $this->caixasScheduledForDeletion[]= clone $caixa;
            $caixa->setUsuario(null);
        }

        return $this;
    }

    /**
     * Clears out the collClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addClientes()
     */
    public function clearClientes()
    {
        $this->collClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collClientes collection loaded partially.
     */
    public function resetPartialClientes($v = true)
    {
        $this->collClientesPartial = $v;
    }

    /**
     * Initializes the collClientes collection.
     *
     * By default this just sets the collClientes collection to an empty array (like clearcollClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initClientes($overrideExisting = true)
    {
        if (null !== $this->collClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = ClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collClientes = new $collectionClassName;
        $this->collClientes->setModel('\ImaTelecomBundle\Model\Cliente');
    }

    /**
     * Gets an array of ChildCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCliente[] List of ChildCliente objects
     * @throws PropelException
     */
    public function getClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collClientesPartial && !$this->isNew();
        if (null === $this->collClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collClientes) {
                // return empty collection
                $this->initClientes();
            } else {
                $collClientes = ChildClienteQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collClientesPartial && count($collClientes)) {
                        $this->initClientes(false);

                        foreach ($collClientes as $obj) {
                            if (false == $this->collClientes->contains($obj)) {
                                $this->collClientes->append($obj);
                            }
                        }

                        $this->collClientesPartial = true;
                    }

                    return $collClientes;
                }

                if ($partial && $this->collClientes) {
                    foreach ($this->collClientes as $obj) {
                        if ($obj->isNew()) {
                            $collClientes[] = $obj;
                        }
                    }
                }

                $this->collClientes = $collClientes;
                $this->collClientesPartial = false;
            }
        }

        return $this->collClientes;
    }

    /**
     * Sets a collection of ChildCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $clientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setClientes(Collection $clientes, ConnectionInterface $con = null)
    {
        /** @var ChildCliente[] $clientesToDelete */
        $clientesToDelete = $this->getClientes(new Criteria(), $con)->diff($clientes);


        $this->clientesScheduledForDeletion = $clientesToDelete;

        foreach ($clientesToDelete as $clienteRemoved) {
            $clienteRemoved->setUsuario(null);
        }

        $this->collClientes = null;
        foreach ($clientes as $cliente) {
            $this->addCliente($cliente);
        }

        $this->collClientes = $clientes;
        $this->collClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Cliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Cliente objects.
     * @throws PropelException
     */
    public function countClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collClientesPartial && !$this->isNew();
        if (null === $this->collClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getClientes());
            }

            $query = ChildClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collClientes);
    }

    /**
     * Method called to associate a ChildCliente object to this object
     * through the ChildCliente foreign key attribute.
     *
     * @param  ChildCliente $l ChildCliente
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addCliente(ChildCliente $l)
    {
        if ($this->collClientes === null) {
            $this->initClientes();
            $this->collClientesPartial = true;
        }

        if (!$this->collClientes->contains($l)) {
            $this->doAddCliente($l);

            if ($this->clientesScheduledForDeletion and $this->clientesScheduledForDeletion->contains($l)) {
                $this->clientesScheduledForDeletion->remove($this->clientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCliente $cliente The ChildCliente object to add.
     */
    protected function doAddCliente(ChildCliente $cliente)
    {
        $this->collClientes[]= $cliente;
        $cliente->setUsuario($this);
    }

    /**
     * @param  ChildCliente $cliente The ChildCliente object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeCliente(ChildCliente $cliente)
    {
        if ($this->getClientes()->contains($cliente)) {
            $pos = $this->collClientes->search($cliente);
            $this->collClientes->remove($pos);
            if (null === $this->clientesScheduledForDeletion) {
                $this->clientesScheduledForDeletion = clone $this->collClientes;
                $this->clientesScheduledForDeletion->clear();
            }
            $this->clientesScheduledForDeletion[]= clone $cliente;
            $cliente->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Clientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCliente[] List of ChildCliente objects
     */
    public function getClientesJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildClienteQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getClientes($query, $con);
    }

    /**
     * Clears out the collClienteEstoqueItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addClienteEstoqueItems()
     */
    public function clearClienteEstoqueItems()
    {
        $this->collClienteEstoqueItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collClienteEstoqueItems collection loaded partially.
     */
    public function resetPartialClienteEstoqueItems($v = true)
    {
        $this->collClienteEstoqueItemsPartial = $v;
    }

    /**
     * Initializes the collClienteEstoqueItems collection.
     *
     * By default this just sets the collClienteEstoqueItems collection to an empty array (like clearcollClienteEstoqueItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initClienteEstoqueItems($overrideExisting = true)
    {
        if (null !== $this->collClienteEstoqueItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = ClienteEstoqueItemTableMap::getTableMap()->getCollectionClassName();

        $this->collClienteEstoqueItems = new $collectionClassName;
        $this->collClienteEstoqueItems->setModel('\ImaTelecomBundle\Model\ClienteEstoqueItem');
    }

    /**
     * Gets an array of ChildClienteEstoqueItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildClienteEstoqueItem[] List of ChildClienteEstoqueItem objects
     * @throws PropelException
     */
    public function getClienteEstoqueItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collClienteEstoqueItemsPartial && !$this->isNew();
        if (null === $this->collClienteEstoqueItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collClienteEstoqueItems) {
                // return empty collection
                $this->initClienteEstoqueItems();
            } else {
                $collClienteEstoqueItems = ChildClienteEstoqueItemQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collClienteEstoqueItemsPartial && count($collClienteEstoqueItems)) {
                        $this->initClienteEstoqueItems(false);

                        foreach ($collClienteEstoqueItems as $obj) {
                            if (false == $this->collClienteEstoqueItems->contains($obj)) {
                                $this->collClienteEstoqueItems->append($obj);
                            }
                        }

                        $this->collClienteEstoqueItemsPartial = true;
                    }

                    return $collClienteEstoqueItems;
                }

                if ($partial && $this->collClienteEstoqueItems) {
                    foreach ($this->collClienteEstoqueItems as $obj) {
                        if ($obj->isNew()) {
                            $collClienteEstoqueItems[] = $obj;
                        }
                    }
                }

                $this->collClienteEstoqueItems = $collClienteEstoqueItems;
                $this->collClienteEstoqueItemsPartial = false;
            }
        }

        return $this->collClienteEstoqueItems;
    }

    /**
     * Sets a collection of ChildClienteEstoqueItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $clienteEstoqueItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setClienteEstoqueItems(Collection $clienteEstoqueItems, ConnectionInterface $con = null)
    {
        /** @var ChildClienteEstoqueItem[] $clienteEstoqueItemsToDelete */
        $clienteEstoqueItemsToDelete = $this->getClienteEstoqueItems(new Criteria(), $con)->diff($clienteEstoqueItems);


        $this->clienteEstoqueItemsScheduledForDeletion = $clienteEstoqueItemsToDelete;

        foreach ($clienteEstoqueItemsToDelete as $clienteEstoqueItemRemoved) {
            $clienteEstoqueItemRemoved->setUsuario(null);
        }

        $this->collClienteEstoqueItems = null;
        foreach ($clienteEstoqueItems as $clienteEstoqueItem) {
            $this->addClienteEstoqueItem($clienteEstoqueItem);
        }

        $this->collClienteEstoqueItems = $clienteEstoqueItems;
        $this->collClienteEstoqueItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ClienteEstoqueItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ClienteEstoqueItem objects.
     * @throws PropelException
     */
    public function countClienteEstoqueItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collClienteEstoqueItemsPartial && !$this->isNew();
        if (null === $this->collClienteEstoqueItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collClienteEstoqueItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getClienteEstoqueItems());
            }

            $query = ChildClienteEstoqueItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collClienteEstoqueItems);
    }

    /**
     * Method called to associate a ChildClienteEstoqueItem object to this object
     * through the ChildClienteEstoqueItem foreign key attribute.
     *
     * @param  ChildClienteEstoqueItem $l ChildClienteEstoqueItem
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addClienteEstoqueItem(ChildClienteEstoqueItem $l)
    {
        if ($this->collClienteEstoqueItems === null) {
            $this->initClienteEstoqueItems();
            $this->collClienteEstoqueItemsPartial = true;
        }

        if (!$this->collClienteEstoqueItems->contains($l)) {
            $this->doAddClienteEstoqueItem($l);

            if ($this->clienteEstoqueItemsScheduledForDeletion and $this->clienteEstoqueItemsScheduledForDeletion->contains($l)) {
                $this->clienteEstoqueItemsScheduledForDeletion->remove($this->clienteEstoqueItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildClienteEstoqueItem $clienteEstoqueItem The ChildClienteEstoqueItem object to add.
     */
    protected function doAddClienteEstoqueItem(ChildClienteEstoqueItem $clienteEstoqueItem)
    {
        $this->collClienteEstoqueItems[]= $clienteEstoqueItem;
        $clienteEstoqueItem->setUsuario($this);
    }

    /**
     * @param  ChildClienteEstoqueItem $clienteEstoqueItem The ChildClienteEstoqueItem object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeClienteEstoqueItem(ChildClienteEstoqueItem $clienteEstoqueItem)
    {
        if ($this->getClienteEstoqueItems()->contains($clienteEstoqueItem)) {
            $pos = $this->collClienteEstoqueItems->search($clienteEstoqueItem);
            $this->collClienteEstoqueItems->remove($pos);
            if (null === $this->clienteEstoqueItemsScheduledForDeletion) {
                $this->clienteEstoqueItemsScheduledForDeletion = clone $this->collClienteEstoqueItems;
                $this->clienteEstoqueItemsScheduledForDeletion->clear();
            }
            $this->clienteEstoqueItemsScheduledForDeletion[]= clone $clienteEstoqueItem;
            $clienteEstoqueItem->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ClienteEstoqueItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildClienteEstoqueItem[] List of ChildClienteEstoqueItem objects
     */
    public function getClienteEstoqueItemsJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildClienteEstoqueItemQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getClienteEstoqueItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ClienteEstoqueItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildClienteEstoqueItem[] List of ChildClienteEstoqueItem objects
     */
    public function getClienteEstoqueItemsJoinEstoque(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildClienteEstoqueItemQuery::create(null, $criteria);
        $query->joinWith('Estoque', $joinBehavior);

        return $this->getClienteEstoqueItems($query, $con);
    }

    /**
     * Clears out the collContaCaixas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContaCaixas()
     */
    public function clearContaCaixas()
    {
        $this->collContaCaixas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContaCaixas collection loaded partially.
     */
    public function resetPartialContaCaixas($v = true)
    {
        $this->collContaCaixasPartial = $v;
    }

    /**
     * Initializes the collContaCaixas collection.
     *
     * By default this just sets the collContaCaixas collection to an empty array (like clearcollContaCaixas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContaCaixas($overrideExisting = true)
    {
        if (null !== $this->collContaCaixas && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContaCaixaTableMap::getTableMap()->getCollectionClassName();

        $this->collContaCaixas = new $collectionClassName;
        $this->collContaCaixas->setModel('\ImaTelecomBundle\Model\ContaCaixa');
    }

    /**
     * Gets an array of ChildContaCaixa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContaCaixa[] List of ChildContaCaixa objects
     * @throws PropelException
     */
    public function getContaCaixas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContaCaixasPartial && !$this->isNew();
        if (null === $this->collContaCaixas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContaCaixas) {
                // return empty collection
                $this->initContaCaixas();
            } else {
                $collContaCaixas = ChildContaCaixaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContaCaixasPartial && count($collContaCaixas)) {
                        $this->initContaCaixas(false);

                        foreach ($collContaCaixas as $obj) {
                            if (false == $this->collContaCaixas->contains($obj)) {
                                $this->collContaCaixas->append($obj);
                            }
                        }

                        $this->collContaCaixasPartial = true;
                    }

                    return $collContaCaixas;
                }

                if ($partial && $this->collContaCaixas) {
                    foreach ($this->collContaCaixas as $obj) {
                        if ($obj->isNew()) {
                            $collContaCaixas[] = $obj;
                        }
                    }
                }

                $this->collContaCaixas = $collContaCaixas;
                $this->collContaCaixasPartial = false;
            }
        }

        return $this->collContaCaixas;
    }

    /**
     * Sets a collection of ChildContaCaixa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contaCaixas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setContaCaixas(Collection $contaCaixas, ConnectionInterface $con = null)
    {
        /** @var ChildContaCaixa[] $contaCaixasToDelete */
        $contaCaixasToDelete = $this->getContaCaixas(new Criteria(), $con)->diff($contaCaixas);


        $this->contaCaixasScheduledForDeletion = $contaCaixasToDelete;

        foreach ($contaCaixasToDelete as $contaCaixaRemoved) {
            $contaCaixaRemoved->setUsuario(null);
        }

        $this->collContaCaixas = null;
        foreach ($contaCaixas as $contaCaixa) {
            $this->addContaCaixa($contaCaixa);
        }

        $this->collContaCaixas = $contaCaixas;
        $this->collContaCaixasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ContaCaixa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ContaCaixa objects.
     * @throws PropelException
     */
    public function countContaCaixas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContaCaixasPartial && !$this->isNew();
        if (null === $this->collContaCaixas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContaCaixas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContaCaixas());
            }

            $query = ChildContaCaixaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collContaCaixas);
    }

    /**
     * Method called to associate a ChildContaCaixa object to this object
     * through the ChildContaCaixa foreign key attribute.
     *
     * @param  ChildContaCaixa $l ChildContaCaixa
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addContaCaixa(ChildContaCaixa $l)
    {
        if ($this->collContaCaixas === null) {
            $this->initContaCaixas();
            $this->collContaCaixasPartial = true;
        }

        if (!$this->collContaCaixas->contains($l)) {
            $this->doAddContaCaixa($l);

            if ($this->contaCaixasScheduledForDeletion and $this->contaCaixasScheduledForDeletion->contains($l)) {
                $this->contaCaixasScheduledForDeletion->remove($this->contaCaixasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContaCaixa $contaCaixa The ChildContaCaixa object to add.
     */
    protected function doAddContaCaixa(ChildContaCaixa $contaCaixa)
    {
        $this->collContaCaixas[]= $contaCaixa;
        $contaCaixa->setUsuario($this);
    }

    /**
     * @param  ChildContaCaixa $contaCaixa The ChildContaCaixa object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeContaCaixa(ChildContaCaixa $contaCaixa)
    {
        if ($this->getContaCaixas()->contains($contaCaixa)) {
            $pos = $this->collContaCaixas->search($contaCaixa);
            $this->collContaCaixas->remove($pos);
            if (null === $this->contaCaixasScheduledForDeletion) {
                $this->contaCaixasScheduledForDeletion = clone $this->collContaCaixas;
                $this->contaCaixasScheduledForDeletion->clear();
            }
            $this->contaCaixasScheduledForDeletion[]= clone $contaCaixa;
            $contaCaixa->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ContaCaixas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContaCaixa[] List of ChildContaCaixa objects
     */
    public function getContaCaixasJoinBancoAgenciaConta(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContaCaixaQuery::create(null, $criteria);
        $query->joinWith('BancoAgenciaConta', $joinBehavior);

        return $this->getContaCaixas($query, $con);
    }

    /**
     * Clears out the collContasPagars collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContasPagars()
     */
    public function clearContasPagars()
    {
        $this->collContasPagars = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContasPagars collection loaded partially.
     */
    public function resetPartialContasPagars($v = true)
    {
        $this->collContasPagarsPartial = $v;
    }

    /**
     * Initializes the collContasPagars collection.
     *
     * By default this just sets the collContasPagars collection to an empty array (like clearcollContasPagars());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContasPagars($overrideExisting = true)
    {
        if (null !== $this->collContasPagars && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContasPagarTableMap::getTableMap()->getCollectionClassName();

        $this->collContasPagars = new $collectionClassName;
        $this->collContasPagars->setModel('\ImaTelecomBundle\Model\ContasPagar');
    }

    /**
     * Gets an array of ChildContasPagar objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContasPagar[] List of ChildContasPagar objects
     * @throws PropelException
     */
    public function getContasPagars(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarsPartial && !$this->isNew();
        if (null === $this->collContasPagars || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContasPagars) {
                // return empty collection
                $this->initContasPagars();
            } else {
                $collContasPagars = ChildContasPagarQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContasPagarsPartial && count($collContasPagars)) {
                        $this->initContasPagars(false);

                        foreach ($collContasPagars as $obj) {
                            if (false == $this->collContasPagars->contains($obj)) {
                                $this->collContasPagars->append($obj);
                            }
                        }

                        $this->collContasPagarsPartial = true;
                    }

                    return $collContasPagars;
                }

                if ($partial && $this->collContasPagars) {
                    foreach ($this->collContasPagars as $obj) {
                        if ($obj->isNew()) {
                            $collContasPagars[] = $obj;
                        }
                    }
                }

                $this->collContasPagars = $collContasPagars;
                $this->collContasPagarsPartial = false;
            }
        }

        return $this->collContasPagars;
    }

    /**
     * Sets a collection of ChildContasPagar objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contasPagars A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setContasPagars(Collection $contasPagars, ConnectionInterface $con = null)
    {
        /** @var ChildContasPagar[] $contasPagarsToDelete */
        $contasPagarsToDelete = $this->getContasPagars(new Criteria(), $con)->diff($contasPagars);


        $this->contasPagarsScheduledForDeletion = $contasPagarsToDelete;

        foreach ($contasPagarsToDelete as $contasPagarRemoved) {
            $contasPagarRemoved->setUsuario(null);
        }

        $this->collContasPagars = null;
        foreach ($contasPagars as $contasPagar) {
            $this->addContasPagar($contasPagar);
        }

        $this->collContasPagars = $contasPagars;
        $this->collContasPagarsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ContasPagar objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ContasPagar objects.
     * @throws PropelException
     */
    public function countContasPagars(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarsPartial && !$this->isNew();
        if (null === $this->collContasPagars || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContasPagars) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContasPagars());
            }

            $query = ChildContasPagarQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collContasPagars);
    }

    /**
     * Method called to associate a ChildContasPagar object to this object
     * through the ChildContasPagar foreign key attribute.
     *
     * @param  ChildContasPagar $l ChildContasPagar
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addContasPagar(ChildContasPagar $l)
    {
        if ($this->collContasPagars === null) {
            $this->initContasPagars();
            $this->collContasPagarsPartial = true;
        }

        if (!$this->collContasPagars->contains($l)) {
            $this->doAddContasPagar($l);

            if ($this->contasPagarsScheduledForDeletion and $this->contasPagarsScheduledForDeletion->contains($l)) {
                $this->contasPagarsScheduledForDeletion->remove($this->contasPagarsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContasPagar $contasPagar The ChildContasPagar object to add.
     */
    protected function doAddContasPagar(ChildContasPagar $contasPagar)
    {
        $this->collContasPagars[]= $contasPagar;
        $contasPagar->setUsuario($this);
    }

    /**
     * @param  ChildContasPagar $contasPagar The ChildContasPagar object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeContasPagar(ChildContasPagar $contasPagar)
    {
        if ($this->getContasPagars()->contains($contasPagar)) {
            $pos = $this->collContasPagars->search($contasPagar);
            $this->collContasPagars->remove($pos);
            if (null === $this->contasPagarsScheduledForDeletion) {
                $this->contasPagarsScheduledForDeletion = clone $this->collContasPagars;
                $this->contasPagarsScheduledForDeletion->clear();
            }
            $this->contasPagarsScheduledForDeletion[]= clone $contasPagar;
            $contasPagar->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ContasPagars from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContasPagar[] List of ChildContasPagar objects
     */
    public function getContasPagarsJoinFornecedor(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContasPagarQuery::create(null, $criteria);
        $query->joinWith('Fornecedor', $joinBehavior);

        return $this->getContasPagars($query, $con);
    }

    /**
     * Clears out the collContasPagarTributoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContasPagarTributoss()
     */
    public function clearContasPagarTributoss()
    {
        $this->collContasPagarTributoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContasPagarTributoss collection loaded partially.
     */
    public function resetPartialContasPagarTributoss($v = true)
    {
        $this->collContasPagarTributossPartial = $v;
    }

    /**
     * Initializes the collContasPagarTributoss collection.
     *
     * By default this just sets the collContasPagarTributoss collection to an empty array (like clearcollContasPagarTributoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContasPagarTributoss($overrideExisting = true)
    {
        if (null !== $this->collContasPagarTributoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContasPagarTributosTableMap::getTableMap()->getCollectionClassName();

        $this->collContasPagarTributoss = new $collectionClassName;
        $this->collContasPagarTributoss->setModel('\ImaTelecomBundle\Model\ContasPagarTributos');
    }

    /**
     * Gets an array of ChildContasPagarTributos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContasPagarTributos[] List of ChildContasPagarTributos objects
     * @throws PropelException
     */
    public function getContasPagarTributoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarTributossPartial && !$this->isNew();
        if (null === $this->collContasPagarTributoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContasPagarTributoss) {
                // return empty collection
                $this->initContasPagarTributoss();
            } else {
                $collContasPagarTributoss = ChildContasPagarTributosQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContasPagarTributossPartial && count($collContasPagarTributoss)) {
                        $this->initContasPagarTributoss(false);

                        foreach ($collContasPagarTributoss as $obj) {
                            if (false == $this->collContasPagarTributoss->contains($obj)) {
                                $this->collContasPagarTributoss->append($obj);
                            }
                        }

                        $this->collContasPagarTributossPartial = true;
                    }

                    return $collContasPagarTributoss;
                }

                if ($partial && $this->collContasPagarTributoss) {
                    foreach ($this->collContasPagarTributoss as $obj) {
                        if ($obj->isNew()) {
                            $collContasPagarTributoss[] = $obj;
                        }
                    }
                }

                $this->collContasPagarTributoss = $collContasPagarTributoss;
                $this->collContasPagarTributossPartial = false;
            }
        }

        return $this->collContasPagarTributoss;
    }

    /**
     * Sets a collection of ChildContasPagarTributos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contasPagarTributoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setContasPagarTributoss(Collection $contasPagarTributoss, ConnectionInterface $con = null)
    {
        /** @var ChildContasPagarTributos[] $contasPagarTributossToDelete */
        $contasPagarTributossToDelete = $this->getContasPagarTributoss(new Criteria(), $con)->diff($contasPagarTributoss);


        $this->contasPagarTributossScheduledForDeletion = $contasPagarTributossToDelete;

        foreach ($contasPagarTributossToDelete as $contasPagarTributosRemoved) {
            $contasPagarTributosRemoved->setUsuario(null);
        }

        $this->collContasPagarTributoss = null;
        foreach ($contasPagarTributoss as $contasPagarTributos) {
            $this->addContasPagarTributos($contasPagarTributos);
        }

        $this->collContasPagarTributoss = $contasPagarTributoss;
        $this->collContasPagarTributossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ContasPagarTributos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ContasPagarTributos objects.
     * @throws PropelException
     */
    public function countContasPagarTributoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarTributossPartial && !$this->isNew();
        if (null === $this->collContasPagarTributoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContasPagarTributoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContasPagarTributoss());
            }

            $query = ChildContasPagarTributosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collContasPagarTributoss);
    }

    /**
     * Method called to associate a ChildContasPagarTributos object to this object
     * through the ChildContasPagarTributos foreign key attribute.
     *
     * @param  ChildContasPagarTributos $l ChildContasPagarTributos
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addContasPagarTributos(ChildContasPagarTributos $l)
    {
        if ($this->collContasPagarTributoss === null) {
            $this->initContasPagarTributoss();
            $this->collContasPagarTributossPartial = true;
        }

        if (!$this->collContasPagarTributoss->contains($l)) {
            $this->doAddContasPagarTributos($l);

            if ($this->contasPagarTributossScheduledForDeletion and $this->contasPagarTributossScheduledForDeletion->contains($l)) {
                $this->contasPagarTributossScheduledForDeletion->remove($this->contasPagarTributossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContasPagarTributos $contasPagarTributos The ChildContasPagarTributos object to add.
     */
    protected function doAddContasPagarTributos(ChildContasPagarTributos $contasPagarTributos)
    {
        $this->collContasPagarTributoss[]= $contasPagarTributos;
        $contasPagarTributos->setUsuario($this);
    }

    /**
     * @param  ChildContasPagarTributos $contasPagarTributos The ChildContasPagarTributos object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeContasPagarTributos(ChildContasPagarTributos $contasPagarTributos)
    {
        if ($this->getContasPagarTributoss()->contains($contasPagarTributos)) {
            $pos = $this->collContasPagarTributoss->search($contasPagarTributos);
            $this->collContasPagarTributoss->remove($pos);
            if (null === $this->contasPagarTributossScheduledForDeletion) {
                $this->contasPagarTributossScheduledForDeletion = clone $this->collContasPagarTributoss;
                $this->contasPagarTributossScheduledForDeletion->clear();
            }
            $this->contasPagarTributossScheduledForDeletion[]= clone $contasPagarTributos;
            $contasPagarTributos->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ContasPagarTributoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContasPagarTributos[] List of ChildContasPagarTributos objects
     */
    public function getContasPagarTributossJoinContasPagar(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContasPagarTributosQuery::create(null, $criteria);
        $query->joinWith('ContasPagar', $joinBehavior);

        return $this->getContasPagarTributoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ContasPagarTributoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContasPagarTributos[] List of ChildContasPagarTributos objects
     */
    public function getContasPagarTributossJoinTributo(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContasPagarTributosQuery::create(null, $criteria);
        $query->joinWith('Tributo', $joinBehavior);

        return $this->getContasPagarTributoss($query, $con);
    }

    /**
     * Clears out the collConvenios collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addConvenios()
     */
    public function clearConvenios()
    {
        $this->collConvenios = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collConvenios collection loaded partially.
     */
    public function resetPartialConvenios($v = true)
    {
        $this->collConveniosPartial = $v;
    }

    /**
     * Initializes the collConvenios collection.
     *
     * By default this just sets the collConvenios collection to an empty array (like clearcollConvenios());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initConvenios($overrideExisting = true)
    {
        if (null !== $this->collConvenios && !$overrideExisting) {
            return;
        }

        $collectionClassName = ConvenioTableMap::getTableMap()->getCollectionClassName();

        $this->collConvenios = new $collectionClassName;
        $this->collConvenios->setModel('\ImaTelecomBundle\Model\Convenio');
    }

    /**
     * Gets an array of ChildConvenio objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildConvenio[] List of ChildConvenio objects
     * @throws PropelException
     */
    public function getConvenios(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collConveniosPartial && !$this->isNew();
        if (null === $this->collConvenios || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collConvenios) {
                // return empty collection
                $this->initConvenios();
            } else {
                $collConvenios = ChildConvenioQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collConveniosPartial && count($collConvenios)) {
                        $this->initConvenios(false);

                        foreach ($collConvenios as $obj) {
                            if (false == $this->collConvenios->contains($obj)) {
                                $this->collConvenios->append($obj);
                            }
                        }

                        $this->collConveniosPartial = true;
                    }

                    return $collConvenios;
                }

                if ($partial && $this->collConvenios) {
                    foreach ($this->collConvenios as $obj) {
                        if ($obj->isNew()) {
                            $collConvenios[] = $obj;
                        }
                    }
                }

                $this->collConvenios = $collConvenios;
                $this->collConveniosPartial = false;
            }
        }

        return $this->collConvenios;
    }

    /**
     * Sets a collection of ChildConvenio objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $convenios A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setConvenios(Collection $convenios, ConnectionInterface $con = null)
    {
        /** @var ChildConvenio[] $conveniosToDelete */
        $conveniosToDelete = $this->getConvenios(new Criteria(), $con)->diff($convenios);


        $this->conveniosScheduledForDeletion = $conveniosToDelete;

        foreach ($conveniosToDelete as $convenioRemoved) {
            $convenioRemoved->setUsuario(null);
        }

        $this->collConvenios = null;
        foreach ($convenios as $convenio) {
            $this->addConvenio($convenio);
        }

        $this->collConvenios = $convenios;
        $this->collConveniosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Convenio objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Convenio objects.
     * @throws PropelException
     */
    public function countConvenios(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collConveniosPartial && !$this->isNew();
        if (null === $this->collConvenios || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collConvenios) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getConvenios());
            }

            $query = ChildConvenioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collConvenios);
    }

    /**
     * Method called to associate a ChildConvenio object to this object
     * through the ChildConvenio foreign key attribute.
     *
     * @param  ChildConvenio $l ChildConvenio
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addConvenio(ChildConvenio $l)
    {
        if ($this->collConvenios === null) {
            $this->initConvenios();
            $this->collConveniosPartial = true;
        }

        if (!$this->collConvenios->contains($l)) {
            $this->doAddConvenio($l);

            if ($this->conveniosScheduledForDeletion and $this->conveniosScheduledForDeletion->contains($l)) {
                $this->conveniosScheduledForDeletion->remove($this->conveniosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildConvenio $convenio The ChildConvenio object to add.
     */
    protected function doAddConvenio(ChildConvenio $convenio)
    {
        $this->collConvenios[]= $convenio;
        $convenio->setUsuario($this);
    }

    /**
     * @param  ChildConvenio $convenio The ChildConvenio object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeConvenio(ChildConvenio $convenio)
    {
        if ($this->getConvenios()->contains($convenio)) {
            $pos = $this->collConvenios->search($convenio);
            $this->collConvenios->remove($pos);
            if (null === $this->conveniosScheduledForDeletion) {
                $this->conveniosScheduledForDeletion = clone $this->collConvenios;
                $this->conveniosScheduledForDeletion->clear();
            }
            $this->conveniosScheduledForDeletion[]= clone $convenio;
            $convenio->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Convenios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildConvenio[] List of ChildConvenio objects
     */
    public function getConveniosJoinEmpresa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildConvenioQuery::create(null, $criteria);
        $query->joinWith('Empresa', $joinBehavior);

        return $this->getConvenios($query, $con);
    }

    /**
     * Clears out the collCronTasks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCronTasks()
     */
    public function clearCronTasks()
    {
        $this->collCronTasks = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCronTasks collection loaded partially.
     */
    public function resetPartialCronTasks($v = true)
    {
        $this->collCronTasksPartial = $v;
    }

    /**
     * Initializes the collCronTasks collection.
     *
     * By default this just sets the collCronTasks collection to an empty array (like clearcollCronTasks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCronTasks($overrideExisting = true)
    {
        if (null !== $this->collCronTasks && !$overrideExisting) {
            return;
        }

        $collectionClassName = CronTaskTableMap::getTableMap()->getCollectionClassName();

        $this->collCronTasks = new $collectionClassName;
        $this->collCronTasks->setModel('\ImaTelecomBundle\Model\CronTask');
    }

    /**
     * Gets an array of ChildCronTask objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCronTask[] List of ChildCronTask objects
     * @throws PropelException
     */
    public function getCronTasks(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCronTasksPartial && !$this->isNew();
        if (null === $this->collCronTasks || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCronTasks) {
                // return empty collection
                $this->initCronTasks();
            } else {
                $collCronTasks = ChildCronTaskQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCronTasksPartial && count($collCronTasks)) {
                        $this->initCronTasks(false);

                        foreach ($collCronTasks as $obj) {
                            if (false == $this->collCronTasks->contains($obj)) {
                                $this->collCronTasks->append($obj);
                            }
                        }

                        $this->collCronTasksPartial = true;
                    }

                    return $collCronTasks;
                }

                if ($partial && $this->collCronTasks) {
                    foreach ($this->collCronTasks as $obj) {
                        if ($obj->isNew()) {
                            $collCronTasks[] = $obj;
                        }
                    }
                }

                $this->collCronTasks = $collCronTasks;
                $this->collCronTasksPartial = false;
            }
        }

        return $this->collCronTasks;
    }

    /**
     * Sets a collection of ChildCronTask objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $cronTasks A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setCronTasks(Collection $cronTasks, ConnectionInterface $con = null)
    {
        /** @var ChildCronTask[] $cronTasksToDelete */
        $cronTasksToDelete = $this->getCronTasks(new Criteria(), $con)->diff($cronTasks);


        $this->cronTasksScheduledForDeletion = $cronTasksToDelete;

        foreach ($cronTasksToDelete as $cronTaskRemoved) {
            $cronTaskRemoved->setUsuario(null);
        }

        $this->collCronTasks = null;
        foreach ($cronTasks as $cronTask) {
            $this->addCronTask($cronTask);
        }

        $this->collCronTasks = $cronTasks;
        $this->collCronTasksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CronTask objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CronTask objects.
     * @throws PropelException
     */
    public function countCronTasks(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCronTasksPartial && !$this->isNew();
        if (null === $this->collCronTasks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCronTasks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCronTasks());
            }

            $query = ChildCronTaskQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collCronTasks);
    }

    /**
     * Method called to associate a ChildCronTask object to this object
     * through the ChildCronTask foreign key attribute.
     *
     * @param  ChildCronTask $l ChildCronTask
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addCronTask(ChildCronTask $l)
    {
        if ($this->collCronTasks === null) {
            $this->initCronTasks();
            $this->collCronTasksPartial = true;
        }

        if (!$this->collCronTasks->contains($l)) {
            $this->doAddCronTask($l);

            if ($this->cronTasksScheduledForDeletion and $this->cronTasksScheduledForDeletion->contains($l)) {
                $this->cronTasksScheduledForDeletion->remove($this->cronTasksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCronTask $cronTask The ChildCronTask object to add.
     */
    protected function doAddCronTask(ChildCronTask $cronTask)
    {
        $this->collCronTasks[]= $cronTask;
        $cronTask->setUsuario($this);
    }

    /**
     * @param  ChildCronTask $cronTask The ChildCronTask object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeCronTask(ChildCronTask $cronTask)
    {
        if ($this->getCronTasks()->contains($cronTask)) {
            $pos = $this->collCronTasks->search($cronTask);
            $this->collCronTasks->remove($pos);
            if (null === $this->cronTasksScheduledForDeletion) {
                $this->cronTasksScheduledForDeletion = clone $this->collCronTasks;
                $this->cronTasksScheduledForDeletion->clear();
            }
            $this->cronTasksScheduledForDeletion[]= clone $cronTask;
            $cronTask->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related CronTasks from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCronTask[] List of ChildCronTask objects
     */
    public function getCronTasksJoinCronTaskGrupo(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCronTaskQuery::create(null, $criteria);
        $query->joinWith('CronTaskGrupo', $joinBehavior);

        return $this->getCronTasks($query, $con);
    }

    /**
     * Clears out the collCronTaskGrupos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCronTaskGrupos()
     */
    public function clearCronTaskGrupos()
    {
        $this->collCronTaskGrupos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCronTaskGrupos collection loaded partially.
     */
    public function resetPartialCronTaskGrupos($v = true)
    {
        $this->collCronTaskGruposPartial = $v;
    }

    /**
     * Initializes the collCronTaskGrupos collection.
     *
     * By default this just sets the collCronTaskGrupos collection to an empty array (like clearcollCronTaskGrupos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCronTaskGrupos($overrideExisting = true)
    {
        if (null !== $this->collCronTaskGrupos && !$overrideExisting) {
            return;
        }

        $collectionClassName = CronTaskGrupoTableMap::getTableMap()->getCollectionClassName();

        $this->collCronTaskGrupos = new $collectionClassName;
        $this->collCronTaskGrupos->setModel('\ImaTelecomBundle\Model\CronTaskGrupo');
    }

    /**
     * Gets an array of ChildCronTaskGrupo objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCronTaskGrupo[] List of ChildCronTaskGrupo objects
     * @throws PropelException
     */
    public function getCronTaskGrupos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCronTaskGruposPartial && !$this->isNew();
        if (null === $this->collCronTaskGrupos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCronTaskGrupos) {
                // return empty collection
                $this->initCronTaskGrupos();
            } else {
                $collCronTaskGrupos = ChildCronTaskGrupoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCronTaskGruposPartial && count($collCronTaskGrupos)) {
                        $this->initCronTaskGrupos(false);

                        foreach ($collCronTaskGrupos as $obj) {
                            if (false == $this->collCronTaskGrupos->contains($obj)) {
                                $this->collCronTaskGrupos->append($obj);
                            }
                        }

                        $this->collCronTaskGruposPartial = true;
                    }

                    return $collCronTaskGrupos;
                }

                if ($partial && $this->collCronTaskGrupos) {
                    foreach ($this->collCronTaskGrupos as $obj) {
                        if ($obj->isNew()) {
                            $collCronTaskGrupos[] = $obj;
                        }
                    }
                }

                $this->collCronTaskGrupos = $collCronTaskGrupos;
                $this->collCronTaskGruposPartial = false;
            }
        }

        return $this->collCronTaskGrupos;
    }

    /**
     * Sets a collection of ChildCronTaskGrupo objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $cronTaskGrupos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setCronTaskGrupos(Collection $cronTaskGrupos, ConnectionInterface $con = null)
    {
        /** @var ChildCronTaskGrupo[] $cronTaskGruposToDelete */
        $cronTaskGruposToDelete = $this->getCronTaskGrupos(new Criteria(), $con)->diff($cronTaskGrupos);


        $this->cronTaskGruposScheduledForDeletion = $cronTaskGruposToDelete;

        foreach ($cronTaskGruposToDelete as $cronTaskGrupoRemoved) {
            $cronTaskGrupoRemoved->setUsuario(null);
        }

        $this->collCronTaskGrupos = null;
        foreach ($cronTaskGrupos as $cronTaskGrupo) {
            $this->addCronTaskGrupo($cronTaskGrupo);
        }

        $this->collCronTaskGrupos = $cronTaskGrupos;
        $this->collCronTaskGruposPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CronTaskGrupo objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CronTaskGrupo objects.
     * @throws PropelException
     */
    public function countCronTaskGrupos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCronTaskGruposPartial && !$this->isNew();
        if (null === $this->collCronTaskGrupos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCronTaskGrupos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCronTaskGrupos());
            }

            $query = ChildCronTaskGrupoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collCronTaskGrupos);
    }

    /**
     * Method called to associate a ChildCronTaskGrupo object to this object
     * through the ChildCronTaskGrupo foreign key attribute.
     *
     * @param  ChildCronTaskGrupo $l ChildCronTaskGrupo
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addCronTaskGrupo(ChildCronTaskGrupo $l)
    {
        if ($this->collCronTaskGrupos === null) {
            $this->initCronTaskGrupos();
            $this->collCronTaskGruposPartial = true;
        }

        if (!$this->collCronTaskGrupos->contains($l)) {
            $this->doAddCronTaskGrupo($l);

            if ($this->cronTaskGruposScheduledForDeletion and $this->cronTaskGruposScheduledForDeletion->contains($l)) {
                $this->cronTaskGruposScheduledForDeletion->remove($this->cronTaskGruposScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCronTaskGrupo $cronTaskGrupo The ChildCronTaskGrupo object to add.
     */
    protected function doAddCronTaskGrupo(ChildCronTaskGrupo $cronTaskGrupo)
    {
        $this->collCronTaskGrupos[]= $cronTaskGrupo;
        $cronTaskGrupo->setUsuario($this);
    }

    /**
     * @param  ChildCronTaskGrupo $cronTaskGrupo The ChildCronTaskGrupo object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeCronTaskGrupo(ChildCronTaskGrupo $cronTaskGrupo)
    {
        if ($this->getCronTaskGrupos()->contains($cronTaskGrupo)) {
            $pos = $this->collCronTaskGrupos->search($cronTaskGrupo);
            $this->collCronTaskGrupos->remove($pos);
            if (null === $this->cronTaskGruposScheduledForDeletion) {
                $this->cronTaskGruposScheduledForDeletion = clone $this->collCronTaskGrupos;
                $this->cronTaskGruposScheduledForDeletion->clear();
            }
            $this->cronTaskGruposScheduledForDeletion[]= clone $cronTaskGrupo;
            $cronTaskGrupo->setUsuario(null);
        }

        return $this;
    }

    /**
     * Clears out the collEnderecoAgencias collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEnderecoAgencias()
     */
    public function clearEnderecoAgencias()
    {
        $this->collEnderecoAgencias = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEnderecoAgencias collection loaded partially.
     */
    public function resetPartialEnderecoAgencias($v = true)
    {
        $this->collEnderecoAgenciasPartial = $v;
    }

    /**
     * Initializes the collEnderecoAgencias collection.
     *
     * By default this just sets the collEnderecoAgencias collection to an empty array (like clearcollEnderecoAgencias());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEnderecoAgencias($overrideExisting = true)
    {
        if (null !== $this->collEnderecoAgencias && !$overrideExisting) {
            return;
        }

        $collectionClassName = EnderecoAgenciaTableMap::getTableMap()->getCollectionClassName();

        $this->collEnderecoAgencias = new $collectionClassName;
        $this->collEnderecoAgencias->setModel('\ImaTelecomBundle\Model\EnderecoAgencia');
    }

    /**
     * Gets an array of ChildEnderecoAgencia objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEnderecoAgencia[] List of ChildEnderecoAgencia objects
     * @throws PropelException
     */
    public function getEnderecoAgencias(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoAgenciasPartial && !$this->isNew();
        if (null === $this->collEnderecoAgencias || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEnderecoAgencias) {
                // return empty collection
                $this->initEnderecoAgencias();
            } else {
                $collEnderecoAgencias = ChildEnderecoAgenciaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEnderecoAgenciasPartial && count($collEnderecoAgencias)) {
                        $this->initEnderecoAgencias(false);

                        foreach ($collEnderecoAgencias as $obj) {
                            if (false == $this->collEnderecoAgencias->contains($obj)) {
                                $this->collEnderecoAgencias->append($obj);
                            }
                        }

                        $this->collEnderecoAgenciasPartial = true;
                    }

                    return $collEnderecoAgencias;
                }

                if ($partial && $this->collEnderecoAgencias) {
                    foreach ($this->collEnderecoAgencias as $obj) {
                        if ($obj->isNew()) {
                            $collEnderecoAgencias[] = $obj;
                        }
                    }
                }

                $this->collEnderecoAgencias = $collEnderecoAgencias;
                $this->collEnderecoAgenciasPartial = false;
            }
        }

        return $this->collEnderecoAgencias;
    }

    /**
     * Sets a collection of ChildEnderecoAgencia objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $enderecoAgencias A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setEnderecoAgencias(Collection $enderecoAgencias, ConnectionInterface $con = null)
    {
        /** @var ChildEnderecoAgencia[] $enderecoAgenciasToDelete */
        $enderecoAgenciasToDelete = $this->getEnderecoAgencias(new Criteria(), $con)->diff($enderecoAgencias);


        $this->enderecoAgenciasScheduledForDeletion = $enderecoAgenciasToDelete;

        foreach ($enderecoAgenciasToDelete as $enderecoAgenciaRemoved) {
            $enderecoAgenciaRemoved->setUsuario(null);
        }

        $this->collEnderecoAgencias = null;
        foreach ($enderecoAgencias as $enderecoAgencia) {
            $this->addEnderecoAgencia($enderecoAgencia);
        }

        $this->collEnderecoAgencias = $enderecoAgencias;
        $this->collEnderecoAgenciasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EnderecoAgencia objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EnderecoAgencia objects.
     * @throws PropelException
     */
    public function countEnderecoAgencias(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoAgenciasPartial && !$this->isNew();
        if (null === $this->collEnderecoAgencias || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEnderecoAgencias) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEnderecoAgencias());
            }

            $query = ChildEnderecoAgenciaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collEnderecoAgencias);
    }

    /**
     * Method called to associate a ChildEnderecoAgencia object to this object
     * through the ChildEnderecoAgencia foreign key attribute.
     *
     * @param  ChildEnderecoAgencia $l ChildEnderecoAgencia
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addEnderecoAgencia(ChildEnderecoAgencia $l)
    {
        if ($this->collEnderecoAgencias === null) {
            $this->initEnderecoAgencias();
            $this->collEnderecoAgenciasPartial = true;
        }

        if (!$this->collEnderecoAgencias->contains($l)) {
            $this->doAddEnderecoAgencia($l);

            if ($this->enderecoAgenciasScheduledForDeletion and $this->enderecoAgenciasScheduledForDeletion->contains($l)) {
                $this->enderecoAgenciasScheduledForDeletion->remove($this->enderecoAgenciasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEnderecoAgencia $enderecoAgencia The ChildEnderecoAgencia object to add.
     */
    protected function doAddEnderecoAgencia(ChildEnderecoAgencia $enderecoAgencia)
    {
        $this->collEnderecoAgencias[]= $enderecoAgencia;
        $enderecoAgencia->setUsuario($this);
    }

    /**
     * @param  ChildEnderecoAgencia $enderecoAgencia The ChildEnderecoAgencia object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeEnderecoAgencia(ChildEnderecoAgencia $enderecoAgencia)
    {
        if ($this->getEnderecoAgencias()->contains($enderecoAgencia)) {
            $pos = $this->collEnderecoAgencias->search($enderecoAgencia);
            $this->collEnderecoAgencias->remove($pos);
            if (null === $this->enderecoAgenciasScheduledForDeletion) {
                $this->enderecoAgenciasScheduledForDeletion = clone $this->collEnderecoAgencias;
                $this->enderecoAgenciasScheduledForDeletion->clear();
            }
            $this->enderecoAgenciasScheduledForDeletion[]= clone $enderecoAgencia;
            $enderecoAgencia->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related EnderecoAgencias from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoAgencia[] List of ChildEnderecoAgencia objects
     */
    public function getEnderecoAgenciasJoinCidade(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoAgenciaQuery::create(null, $criteria);
        $query->joinWith('Cidade', $joinBehavior);

        return $this->getEnderecoAgencias($query, $con);
    }

    /**
     * Clears out the collEnderecoClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEnderecoClientes()
     */
    public function clearEnderecoClientes()
    {
        $this->collEnderecoClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEnderecoClientes collection loaded partially.
     */
    public function resetPartialEnderecoClientes($v = true)
    {
        $this->collEnderecoClientesPartial = $v;
    }

    /**
     * Initializes the collEnderecoClientes collection.
     *
     * By default this just sets the collEnderecoClientes collection to an empty array (like clearcollEnderecoClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEnderecoClientes($overrideExisting = true)
    {
        if (null !== $this->collEnderecoClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = EnderecoClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collEnderecoClientes = new $collectionClassName;
        $this->collEnderecoClientes->setModel('\ImaTelecomBundle\Model\EnderecoCliente');
    }

    /**
     * Gets an array of ChildEnderecoCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEnderecoCliente[] List of ChildEnderecoCliente objects
     * @throws PropelException
     */
    public function getEnderecoClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEnderecoClientes) {
                // return empty collection
                $this->initEnderecoClientes();
            } else {
                $collEnderecoClientes = ChildEnderecoClienteQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEnderecoClientesPartial && count($collEnderecoClientes)) {
                        $this->initEnderecoClientes(false);

                        foreach ($collEnderecoClientes as $obj) {
                            if (false == $this->collEnderecoClientes->contains($obj)) {
                                $this->collEnderecoClientes->append($obj);
                            }
                        }

                        $this->collEnderecoClientesPartial = true;
                    }

                    return $collEnderecoClientes;
                }

                if ($partial && $this->collEnderecoClientes) {
                    foreach ($this->collEnderecoClientes as $obj) {
                        if ($obj->isNew()) {
                            $collEnderecoClientes[] = $obj;
                        }
                    }
                }

                $this->collEnderecoClientes = $collEnderecoClientes;
                $this->collEnderecoClientesPartial = false;
            }
        }

        return $this->collEnderecoClientes;
    }

    /**
     * Sets a collection of ChildEnderecoCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $enderecoClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setEnderecoClientes(Collection $enderecoClientes, ConnectionInterface $con = null)
    {
        /** @var ChildEnderecoCliente[] $enderecoClientesToDelete */
        $enderecoClientesToDelete = $this->getEnderecoClientes(new Criteria(), $con)->diff($enderecoClientes);


        $this->enderecoClientesScheduledForDeletion = $enderecoClientesToDelete;

        foreach ($enderecoClientesToDelete as $enderecoClienteRemoved) {
            $enderecoClienteRemoved->setUsuario(null);
        }

        $this->collEnderecoClientes = null;
        foreach ($enderecoClientes as $enderecoCliente) {
            $this->addEnderecoCliente($enderecoCliente);
        }

        $this->collEnderecoClientes = $enderecoClientes;
        $this->collEnderecoClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EnderecoCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EnderecoCliente objects.
     * @throws PropelException
     */
    public function countEnderecoClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEnderecoClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEnderecoClientes());
            }

            $query = ChildEnderecoClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collEnderecoClientes);
    }

    /**
     * Method called to associate a ChildEnderecoCliente object to this object
     * through the ChildEnderecoCliente foreign key attribute.
     *
     * @param  ChildEnderecoCliente $l ChildEnderecoCliente
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addEnderecoCliente(ChildEnderecoCliente $l)
    {
        if ($this->collEnderecoClientes === null) {
            $this->initEnderecoClientes();
            $this->collEnderecoClientesPartial = true;
        }

        if (!$this->collEnderecoClientes->contains($l)) {
            $this->doAddEnderecoCliente($l);

            if ($this->enderecoClientesScheduledForDeletion and $this->enderecoClientesScheduledForDeletion->contains($l)) {
                $this->enderecoClientesScheduledForDeletion->remove($this->enderecoClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEnderecoCliente $enderecoCliente The ChildEnderecoCliente object to add.
     */
    protected function doAddEnderecoCliente(ChildEnderecoCliente $enderecoCliente)
    {
        $this->collEnderecoClientes[]= $enderecoCliente;
        $enderecoCliente->setUsuario($this);
    }

    /**
     * @param  ChildEnderecoCliente $enderecoCliente The ChildEnderecoCliente object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeEnderecoCliente(ChildEnderecoCliente $enderecoCliente)
    {
        if ($this->getEnderecoClientes()->contains($enderecoCliente)) {
            $pos = $this->collEnderecoClientes->search($enderecoCliente);
            $this->collEnderecoClientes->remove($pos);
            if (null === $this->enderecoClientesScheduledForDeletion) {
                $this->enderecoClientesScheduledForDeletion = clone $this->collEnderecoClientes;
                $this->enderecoClientesScheduledForDeletion->clear();
            }
            $this->enderecoClientesScheduledForDeletion[]= clone $enderecoCliente;
            $enderecoCliente->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related EnderecoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoCliente[] List of ChildEnderecoCliente objects
     */
    public function getEnderecoClientesJoinCidade(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoClienteQuery::create(null, $criteria);
        $query->joinWith('Cidade', $joinBehavior);

        return $this->getEnderecoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related EnderecoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoCliente[] List of ChildEnderecoCliente objects
     */
    public function getEnderecoClientesJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoClienteQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getEnderecoClientes($query, $con);
    }

    /**
     * Clears out the collEnderecoServClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEnderecoServClientes()
     */
    public function clearEnderecoServClientes()
    {
        $this->collEnderecoServClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEnderecoServClientes collection loaded partially.
     */
    public function resetPartialEnderecoServClientes($v = true)
    {
        $this->collEnderecoServClientesPartial = $v;
    }

    /**
     * Initializes the collEnderecoServClientes collection.
     *
     * By default this just sets the collEnderecoServClientes collection to an empty array (like clearcollEnderecoServClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEnderecoServClientes($overrideExisting = true)
    {
        if (null !== $this->collEnderecoServClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = EnderecoServClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collEnderecoServClientes = new $collectionClassName;
        $this->collEnderecoServClientes->setModel('\ImaTelecomBundle\Model\EnderecoServCliente');
    }

    /**
     * Gets an array of ChildEnderecoServCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEnderecoServCliente[] List of ChildEnderecoServCliente objects
     * @throws PropelException
     */
    public function getEnderecoServClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoServClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoServClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEnderecoServClientes) {
                // return empty collection
                $this->initEnderecoServClientes();
            } else {
                $collEnderecoServClientes = ChildEnderecoServClienteQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEnderecoServClientesPartial && count($collEnderecoServClientes)) {
                        $this->initEnderecoServClientes(false);

                        foreach ($collEnderecoServClientes as $obj) {
                            if (false == $this->collEnderecoServClientes->contains($obj)) {
                                $this->collEnderecoServClientes->append($obj);
                            }
                        }

                        $this->collEnderecoServClientesPartial = true;
                    }

                    return $collEnderecoServClientes;
                }

                if ($partial && $this->collEnderecoServClientes) {
                    foreach ($this->collEnderecoServClientes as $obj) {
                        if ($obj->isNew()) {
                            $collEnderecoServClientes[] = $obj;
                        }
                    }
                }

                $this->collEnderecoServClientes = $collEnderecoServClientes;
                $this->collEnderecoServClientesPartial = false;
            }
        }

        return $this->collEnderecoServClientes;
    }

    /**
     * Sets a collection of ChildEnderecoServCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $enderecoServClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setEnderecoServClientes(Collection $enderecoServClientes, ConnectionInterface $con = null)
    {
        /** @var ChildEnderecoServCliente[] $enderecoServClientesToDelete */
        $enderecoServClientesToDelete = $this->getEnderecoServClientes(new Criteria(), $con)->diff($enderecoServClientes);


        $this->enderecoServClientesScheduledForDeletion = $enderecoServClientesToDelete;

        foreach ($enderecoServClientesToDelete as $enderecoServClienteRemoved) {
            $enderecoServClienteRemoved->setUsuario(null);
        }

        $this->collEnderecoServClientes = null;
        foreach ($enderecoServClientes as $enderecoServCliente) {
            $this->addEnderecoServCliente($enderecoServCliente);
        }

        $this->collEnderecoServClientes = $enderecoServClientes;
        $this->collEnderecoServClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EnderecoServCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EnderecoServCliente objects.
     * @throws PropelException
     */
    public function countEnderecoServClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoServClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoServClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEnderecoServClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEnderecoServClientes());
            }

            $query = ChildEnderecoServClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collEnderecoServClientes);
    }

    /**
     * Method called to associate a ChildEnderecoServCliente object to this object
     * through the ChildEnderecoServCliente foreign key attribute.
     *
     * @param  ChildEnderecoServCliente $l ChildEnderecoServCliente
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addEnderecoServCliente(ChildEnderecoServCliente $l)
    {
        if ($this->collEnderecoServClientes === null) {
            $this->initEnderecoServClientes();
            $this->collEnderecoServClientesPartial = true;
        }

        if (!$this->collEnderecoServClientes->contains($l)) {
            $this->doAddEnderecoServCliente($l);

            if ($this->enderecoServClientesScheduledForDeletion and $this->enderecoServClientesScheduledForDeletion->contains($l)) {
                $this->enderecoServClientesScheduledForDeletion->remove($this->enderecoServClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEnderecoServCliente $enderecoServCliente The ChildEnderecoServCliente object to add.
     */
    protected function doAddEnderecoServCliente(ChildEnderecoServCliente $enderecoServCliente)
    {
        $this->collEnderecoServClientes[]= $enderecoServCliente;
        $enderecoServCliente->setUsuario($this);
    }

    /**
     * @param  ChildEnderecoServCliente $enderecoServCliente The ChildEnderecoServCliente object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeEnderecoServCliente(ChildEnderecoServCliente $enderecoServCliente)
    {
        if ($this->getEnderecoServClientes()->contains($enderecoServCliente)) {
            $pos = $this->collEnderecoServClientes->search($enderecoServCliente);
            $this->collEnderecoServClientes->remove($pos);
            if (null === $this->enderecoServClientesScheduledForDeletion) {
                $this->enderecoServClientesScheduledForDeletion = clone $this->collEnderecoServClientes;
                $this->enderecoServClientesScheduledForDeletion->clear();
            }
            $this->enderecoServClientesScheduledForDeletion[]= clone $enderecoServCliente;
            $enderecoServCliente->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related EnderecoServClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoServCliente[] List of ChildEnderecoServCliente objects
     */
    public function getEnderecoServClientesJoinCidade(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoServClienteQuery::create(null, $criteria);
        $query->joinWith('Cidade', $joinBehavior);

        return $this->getEnderecoServClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related EnderecoServClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoServCliente[] List of ChildEnderecoServCliente objects
     */
    public function getEnderecoServClientesJoinServicoCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoServClienteQuery::create(null, $criteria);
        $query->joinWith('ServicoCliente', $joinBehavior);

        return $this->getEnderecoServClientes($query, $con);
    }

    /**
     * Clears out the collEstoques collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEstoques()
     */
    public function clearEstoques()
    {
        $this->collEstoques = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEstoques collection loaded partially.
     */
    public function resetPartialEstoques($v = true)
    {
        $this->collEstoquesPartial = $v;
    }

    /**
     * Initializes the collEstoques collection.
     *
     * By default this just sets the collEstoques collection to an empty array (like clearcollEstoques());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEstoques($overrideExisting = true)
    {
        if (null !== $this->collEstoques && !$overrideExisting) {
            return;
        }

        $collectionClassName = EstoqueTableMap::getTableMap()->getCollectionClassName();

        $this->collEstoques = new $collectionClassName;
        $this->collEstoques->setModel('\ImaTelecomBundle\Model\Estoque');
    }

    /**
     * Gets an array of ChildEstoque objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEstoque[] List of ChildEstoque objects
     * @throws PropelException
     */
    public function getEstoques(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEstoquesPartial && !$this->isNew();
        if (null === $this->collEstoques || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEstoques) {
                // return empty collection
                $this->initEstoques();
            } else {
                $collEstoques = ChildEstoqueQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEstoquesPartial && count($collEstoques)) {
                        $this->initEstoques(false);

                        foreach ($collEstoques as $obj) {
                            if (false == $this->collEstoques->contains($obj)) {
                                $this->collEstoques->append($obj);
                            }
                        }

                        $this->collEstoquesPartial = true;
                    }

                    return $collEstoques;
                }

                if ($partial && $this->collEstoques) {
                    foreach ($this->collEstoques as $obj) {
                        if ($obj->isNew()) {
                            $collEstoques[] = $obj;
                        }
                    }
                }

                $this->collEstoques = $collEstoques;
                $this->collEstoquesPartial = false;
            }
        }

        return $this->collEstoques;
    }

    /**
     * Sets a collection of ChildEstoque objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $estoques A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setEstoques(Collection $estoques, ConnectionInterface $con = null)
    {
        /** @var ChildEstoque[] $estoquesToDelete */
        $estoquesToDelete = $this->getEstoques(new Criteria(), $con)->diff($estoques);


        $this->estoquesScheduledForDeletion = $estoquesToDelete;

        foreach ($estoquesToDelete as $estoqueRemoved) {
            $estoqueRemoved->setUsuario(null);
        }

        $this->collEstoques = null;
        foreach ($estoques as $estoque) {
            $this->addEstoque($estoque);
        }

        $this->collEstoques = $estoques;
        $this->collEstoquesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Estoque objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Estoque objects.
     * @throws PropelException
     */
    public function countEstoques(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEstoquesPartial && !$this->isNew();
        if (null === $this->collEstoques || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEstoques) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEstoques());
            }

            $query = ChildEstoqueQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collEstoques);
    }

    /**
     * Method called to associate a ChildEstoque object to this object
     * through the ChildEstoque foreign key attribute.
     *
     * @param  ChildEstoque $l ChildEstoque
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addEstoque(ChildEstoque $l)
    {
        if ($this->collEstoques === null) {
            $this->initEstoques();
            $this->collEstoquesPartial = true;
        }

        if (!$this->collEstoques->contains($l)) {
            $this->doAddEstoque($l);

            if ($this->estoquesScheduledForDeletion and $this->estoquesScheduledForDeletion->contains($l)) {
                $this->estoquesScheduledForDeletion->remove($this->estoquesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEstoque $estoque The ChildEstoque object to add.
     */
    protected function doAddEstoque(ChildEstoque $estoque)
    {
        $this->collEstoques[]= $estoque;
        $estoque->setUsuario($this);
    }

    /**
     * @param  ChildEstoque $estoque The ChildEstoque object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeEstoque(ChildEstoque $estoque)
    {
        if ($this->getEstoques()->contains($estoque)) {
            $pos = $this->collEstoques->search($estoque);
            $this->collEstoques->remove($pos);
            if (null === $this->estoquesScheduledForDeletion) {
                $this->estoquesScheduledForDeletion = clone $this->collEstoques;
                $this->estoquesScheduledForDeletion->clear();
            }
            $this->estoquesScheduledForDeletion[]= clone $estoque;
            $estoque->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Estoques from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEstoque[] List of ChildEstoque objects
     */
    public function getEstoquesJoinItemDeCompra(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEstoqueQuery::create(null, $criteria);
        $query->joinWith('ItemDeCompra', $joinBehavior);

        return $this->getEstoques($query, $con);
    }

    /**
     * Clears out the collEstoqueLancamentos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEstoqueLancamentos()
     */
    public function clearEstoqueLancamentos()
    {
        $this->collEstoqueLancamentos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEstoqueLancamentos collection loaded partially.
     */
    public function resetPartialEstoqueLancamentos($v = true)
    {
        $this->collEstoqueLancamentosPartial = $v;
    }

    /**
     * Initializes the collEstoqueLancamentos collection.
     *
     * By default this just sets the collEstoqueLancamentos collection to an empty array (like clearcollEstoqueLancamentos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEstoqueLancamentos($overrideExisting = true)
    {
        if (null !== $this->collEstoqueLancamentos && !$overrideExisting) {
            return;
        }

        $collectionClassName = EstoqueLancamentoTableMap::getTableMap()->getCollectionClassName();

        $this->collEstoqueLancamentos = new $collectionClassName;
        $this->collEstoqueLancamentos->setModel('\ImaTelecomBundle\Model\EstoqueLancamento');
    }

    /**
     * Gets an array of ChildEstoqueLancamento objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEstoqueLancamento[] List of ChildEstoqueLancamento objects
     * @throws PropelException
     */
    public function getEstoqueLancamentos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEstoqueLancamentosPartial && !$this->isNew();
        if (null === $this->collEstoqueLancamentos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEstoqueLancamentos) {
                // return empty collection
                $this->initEstoqueLancamentos();
            } else {
                $collEstoqueLancamentos = ChildEstoqueLancamentoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEstoqueLancamentosPartial && count($collEstoqueLancamentos)) {
                        $this->initEstoqueLancamentos(false);

                        foreach ($collEstoqueLancamentos as $obj) {
                            if (false == $this->collEstoqueLancamentos->contains($obj)) {
                                $this->collEstoqueLancamentos->append($obj);
                            }
                        }

                        $this->collEstoqueLancamentosPartial = true;
                    }

                    return $collEstoqueLancamentos;
                }

                if ($partial && $this->collEstoqueLancamentos) {
                    foreach ($this->collEstoqueLancamentos as $obj) {
                        if ($obj->isNew()) {
                            $collEstoqueLancamentos[] = $obj;
                        }
                    }
                }

                $this->collEstoqueLancamentos = $collEstoqueLancamentos;
                $this->collEstoqueLancamentosPartial = false;
            }
        }

        return $this->collEstoqueLancamentos;
    }

    /**
     * Sets a collection of ChildEstoqueLancamento objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $estoqueLancamentos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setEstoqueLancamentos(Collection $estoqueLancamentos, ConnectionInterface $con = null)
    {
        /** @var ChildEstoqueLancamento[] $estoqueLancamentosToDelete */
        $estoqueLancamentosToDelete = $this->getEstoqueLancamentos(new Criteria(), $con)->diff($estoqueLancamentos);


        $this->estoqueLancamentosScheduledForDeletion = $estoqueLancamentosToDelete;

        foreach ($estoqueLancamentosToDelete as $estoqueLancamentoRemoved) {
            $estoqueLancamentoRemoved->setUsuario(null);
        }

        $this->collEstoqueLancamentos = null;
        foreach ($estoqueLancamentos as $estoqueLancamento) {
            $this->addEstoqueLancamento($estoqueLancamento);
        }

        $this->collEstoqueLancamentos = $estoqueLancamentos;
        $this->collEstoqueLancamentosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EstoqueLancamento objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EstoqueLancamento objects.
     * @throws PropelException
     */
    public function countEstoqueLancamentos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEstoqueLancamentosPartial && !$this->isNew();
        if (null === $this->collEstoqueLancamentos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEstoqueLancamentos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEstoqueLancamentos());
            }

            $query = ChildEstoqueLancamentoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collEstoqueLancamentos);
    }

    /**
     * Method called to associate a ChildEstoqueLancamento object to this object
     * through the ChildEstoqueLancamento foreign key attribute.
     *
     * @param  ChildEstoqueLancamento $l ChildEstoqueLancamento
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addEstoqueLancamento(ChildEstoqueLancamento $l)
    {
        if ($this->collEstoqueLancamentos === null) {
            $this->initEstoqueLancamentos();
            $this->collEstoqueLancamentosPartial = true;
        }

        if (!$this->collEstoqueLancamentos->contains($l)) {
            $this->doAddEstoqueLancamento($l);

            if ($this->estoqueLancamentosScheduledForDeletion and $this->estoqueLancamentosScheduledForDeletion->contains($l)) {
                $this->estoqueLancamentosScheduledForDeletion->remove($this->estoqueLancamentosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEstoqueLancamento $estoqueLancamento The ChildEstoqueLancamento object to add.
     */
    protected function doAddEstoqueLancamento(ChildEstoqueLancamento $estoqueLancamento)
    {
        $this->collEstoqueLancamentos[]= $estoqueLancamento;
        $estoqueLancamento->setUsuario($this);
    }

    /**
     * @param  ChildEstoqueLancamento $estoqueLancamento The ChildEstoqueLancamento object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeEstoqueLancamento(ChildEstoqueLancamento $estoqueLancamento)
    {
        if ($this->getEstoqueLancamentos()->contains($estoqueLancamento)) {
            $pos = $this->collEstoqueLancamentos->search($estoqueLancamento);
            $this->collEstoqueLancamentos->remove($pos);
            if (null === $this->estoqueLancamentosScheduledForDeletion) {
                $this->estoqueLancamentosScheduledForDeletion = clone $this->collEstoqueLancamentos;
                $this->estoqueLancamentosScheduledForDeletion->clear();
            }
            $this->estoqueLancamentosScheduledForDeletion[]= clone $estoqueLancamento;
            $estoqueLancamento->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related EstoqueLancamentos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEstoqueLancamento[] List of ChildEstoqueLancamento objects
     */
    public function getEstoqueLancamentosJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEstoqueLancamentoQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getEstoqueLancamentos($query, $con);
    }

    /**
     * Clears out the collEstoqueLancamentoItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEstoqueLancamentoItems()
     */
    public function clearEstoqueLancamentoItems()
    {
        $this->collEstoqueLancamentoItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEstoqueLancamentoItems collection loaded partially.
     */
    public function resetPartialEstoqueLancamentoItems($v = true)
    {
        $this->collEstoqueLancamentoItemsPartial = $v;
    }

    /**
     * Initializes the collEstoqueLancamentoItems collection.
     *
     * By default this just sets the collEstoqueLancamentoItems collection to an empty array (like clearcollEstoqueLancamentoItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEstoqueLancamentoItems($overrideExisting = true)
    {
        if (null !== $this->collEstoqueLancamentoItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = EstoqueLancamentoItemTableMap::getTableMap()->getCollectionClassName();

        $this->collEstoqueLancamentoItems = new $collectionClassName;
        $this->collEstoqueLancamentoItems->setModel('\ImaTelecomBundle\Model\EstoqueLancamentoItem');
    }

    /**
     * Gets an array of ChildEstoqueLancamentoItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEstoqueLancamentoItem[] List of ChildEstoqueLancamentoItem objects
     * @throws PropelException
     */
    public function getEstoqueLancamentoItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEstoqueLancamentoItemsPartial && !$this->isNew();
        if (null === $this->collEstoqueLancamentoItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEstoqueLancamentoItems) {
                // return empty collection
                $this->initEstoqueLancamentoItems();
            } else {
                $collEstoqueLancamentoItems = ChildEstoqueLancamentoItemQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEstoqueLancamentoItemsPartial && count($collEstoqueLancamentoItems)) {
                        $this->initEstoqueLancamentoItems(false);

                        foreach ($collEstoqueLancamentoItems as $obj) {
                            if (false == $this->collEstoqueLancamentoItems->contains($obj)) {
                                $this->collEstoqueLancamentoItems->append($obj);
                            }
                        }

                        $this->collEstoqueLancamentoItemsPartial = true;
                    }

                    return $collEstoqueLancamentoItems;
                }

                if ($partial && $this->collEstoqueLancamentoItems) {
                    foreach ($this->collEstoqueLancamentoItems as $obj) {
                        if ($obj->isNew()) {
                            $collEstoqueLancamentoItems[] = $obj;
                        }
                    }
                }

                $this->collEstoqueLancamentoItems = $collEstoqueLancamentoItems;
                $this->collEstoqueLancamentoItemsPartial = false;
            }
        }

        return $this->collEstoqueLancamentoItems;
    }

    /**
     * Sets a collection of ChildEstoqueLancamentoItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $estoqueLancamentoItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setEstoqueLancamentoItems(Collection $estoqueLancamentoItems, ConnectionInterface $con = null)
    {
        /** @var ChildEstoqueLancamentoItem[] $estoqueLancamentoItemsToDelete */
        $estoqueLancamentoItemsToDelete = $this->getEstoqueLancamentoItems(new Criteria(), $con)->diff($estoqueLancamentoItems);


        $this->estoqueLancamentoItemsScheduledForDeletion = $estoqueLancamentoItemsToDelete;

        foreach ($estoqueLancamentoItemsToDelete as $estoqueLancamentoItemRemoved) {
            $estoqueLancamentoItemRemoved->setUsuario(null);
        }

        $this->collEstoqueLancamentoItems = null;
        foreach ($estoqueLancamentoItems as $estoqueLancamentoItem) {
            $this->addEstoqueLancamentoItem($estoqueLancamentoItem);
        }

        $this->collEstoqueLancamentoItems = $estoqueLancamentoItems;
        $this->collEstoqueLancamentoItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EstoqueLancamentoItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EstoqueLancamentoItem objects.
     * @throws PropelException
     */
    public function countEstoqueLancamentoItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEstoqueLancamentoItemsPartial && !$this->isNew();
        if (null === $this->collEstoqueLancamentoItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEstoqueLancamentoItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEstoqueLancamentoItems());
            }

            $query = ChildEstoqueLancamentoItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collEstoqueLancamentoItems);
    }

    /**
     * Method called to associate a ChildEstoqueLancamentoItem object to this object
     * through the ChildEstoqueLancamentoItem foreign key attribute.
     *
     * @param  ChildEstoqueLancamentoItem $l ChildEstoqueLancamentoItem
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addEstoqueLancamentoItem(ChildEstoqueLancamentoItem $l)
    {
        if ($this->collEstoqueLancamentoItems === null) {
            $this->initEstoqueLancamentoItems();
            $this->collEstoqueLancamentoItemsPartial = true;
        }

        if (!$this->collEstoqueLancamentoItems->contains($l)) {
            $this->doAddEstoqueLancamentoItem($l);

            if ($this->estoqueLancamentoItemsScheduledForDeletion and $this->estoqueLancamentoItemsScheduledForDeletion->contains($l)) {
                $this->estoqueLancamentoItemsScheduledForDeletion->remove($this->estoqueLancamentoItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEstoqueLancamentoItem $estoqueLancamentoItem The ChildEstoqueLancamentoItem object to add.
     */
    protected function doAddEstoqueLancamentoItem(ChildEstoqueLancamentoItem $estoqueLancamentoItem)
    {
        $this->collEstoqueLancamentoItems[]= $estoqueLancamentoItem;
        $estoqueLancamentoItem->setUsuario($this);
    }

    /**
     * @param  ChildEstoqueLancamentoItem $estoqueLancamentoItem The ChildEstoqueLancamentoItem object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeEstoqueLancamentoItem(ChildEstoqueLancamentoItem $estoqueLancamentoItem)
    {
        if ($this->getEstoqueLancamentoItems()->contains($estoqueLancamentoItem)) {
            $pos = $this->collEstoqueLancamentoItems->search($estoqueLancamentoItem);
            $this->collEstoqueLancamentoItems->remove($pos);
            if (null === $this->estoqueLancamentoItemsScheduledForDeletion) {
                $this->estoqueLancamentoItemsScheduledForDeletion = clone $this->collEstoqueLancamentoItems;
                $this->estoqueLancamentoItemsScheduledForDeletion->clear();
            }
            $this->estoqueLancamentoItemsScheduledForDeletion[]= clone $estoqueLancamentoItem;
            $estoqueLancamentoItem->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related EstoqueLancamentoItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEstoqueLancamentoItem[] List of ChildEstoqueLancamentoItem objects
     */
    public function getEstoqueLancamentoItemsJoinEstoque(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEstoqueLancamentoItemQuery::create(null, $criteria);
        $query->joinWith('Estoque', $joinBehavior);

        return $this->getEstoqueLancamentoItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related EstoqueLancamentoItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEstoqueLancamentoItem[] List of ChildEstoqueLancamentoItem objects
     */
    public function getEstoqueLancamentoItemsJoinEstoqueLancamento(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEstoqueLancamentoItemQuery::create(null, $criteria);
        $query->joinWith('EstoqueLancamento', $joinBehavior);

        return $this->getEstoqueLancamentoItems($query, $con);
    }

    /**
     * Clears out the collFormaPagamentos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFormaPagamentos()
     */
    public function clearFormaPagamentos()
    {
        $this->collFormaPagamentos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFormaPagamentos collection loaded partially.
     */
    public function resetPartialFormaPagamentos($v = true)
    {
        $this->collFormaPagamentosPartial = $v;
    }

    /**
     * Initializes the collFormaPagamentos collection.
     *
     * By default this just sets the collFormaPagamentos collection to an empty array (like clearcollFormaPagamentos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFormaPagamentos($overrideExisting = true)
    {
        if (null !== $this->collFormaPagamentos && !$overrideExisting) {
            return;
        }

        $collectionClassName = FormaPagamentoTableMap::getTableMap()->getCollectionClassName();

        $this->collFormaPagamentos = new $collectionClassName;
        $this->collFormaPagamentos->setModel('\ImaTelecomBundle\Model\FormaPagamento');
    }

    /**
     * Gets an array of ChildFormaPagamento objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFormaPagamento[] List of ChildFormaPagamento objects
     * @throws PropelException
     */
    public function getFormaPagamentos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFormaPagamentosPartial && !$this->isNew();
        if (null === $this->collFormaPagamentos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFormaPagamentos) {
                // return empty collection
                $this->initFormaPagamentos();
            } else {
                $collFormaPagamentos = ChildFormaPagamentoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFormaPagamentosPartial && count($collFormaPagamentos)) {
                        $this->initFormaPagamentos(false);

                        foreach ($collFormaPagamentos as $obj) {
                            if (false == $this->collFormaPagamentos->contains($obj)) {
                                $this->collFormaPagamentos->append($obj);
                            }
                        }

                        $this->collFormaPagamentosPartial = true;
                    }

                    return $collFormaPagamentos;
                }

                if ($partial && $this->collFormaPagamentos) {
                    foreach ($this->collFormaPagamentos as $obj) {
                        if ($obj->isNew()) {
                            $collFormaPagamentos[] = $obj;
                        }
                    }
                }

                $this->collFormaPagamentos = $collFormaPagamentos;
                $this->collFormaPagamentosPartial = false;
            }
        }

        return $this->collFormaPagamentos;
    }

    /**
     * Sets a collection of ChildFormaPagamento objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $formaPagamentos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setFormaPagamentos(Collection $formaPagamentos, ConnectionInterface $con = null)
    {
        /** @var ChildFormaPagamento[] $formaPagamentosToDelete */
        $formaPagamentosToDelete = $this->getFormaPagamentos(new Criteria(), $con)->diff($formaPagamentos);


        $this->formaPagamentosScheduledForDeletion = $formaPagamentosToDelete;

        foreach ($formaPagamentosToDelete as $formaPagamentoRemoved) {
            $formaPagamentoRemoved->setUsuario(null);
        }

        $this->collFormaPagamentos = null;
        foreach ($formaPagamentos as $formaPagamento) {
            $this->addFormaPagamento($formaPagamento);
        }

        $this->collFormaPagamentos = $formaPagamentos;
        $this->collFormaPagamentosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FormaPagamento objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FormaPagamento objects.
     * @throws PropelException
     */
    public function countFormaPagamentos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFormaPagamentosPartial && !$this->isNew();
        if (null === $this->collFormaPagamentos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFormaPagamentos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFormaPagamentos());
            }

            $query = ChildFormaPagamentoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collFormaPagamentos);
    }

    /**
     * Method called to associate a ChildFormaPagamento object to this object
     * through the ChildFormaPagamento foreign key attribute.
     *
     * @param  ChildFormaPagamento $l ChildFormaPagamento
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addFormaPagamento(ChildFormaPagamento $l)
    {
        if ($this->collFormaPagamentos === null) {
            $this->initFormaPagamentos();
            $this->collFormaPagamentosPartial = true;
        }

        if (!$this->collFormaPagamentos->contains($l)) {
            $this->doAddFormaPagamento($l);

            if ($this->formaPagamentosScheduledForDeletion and $this->formaPagamentosScheduledForDeletion->contains($l)) {
                $this->formaPagamentosScheduledForDeletion->remove($this->formaPagamentosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFormaPagamento $formaPagamento The ChildFormaPagamento object to add.
     */
    protected function doAddFormaPagamento(ChildFormaPagamento $formaPagamento)
    {
        $this->collFormaPagamentos[]= $formaPagamento;
        $formaPagamento->setUsuario($this);
    }

    /**
     * @param  ChildFormaPagamento $formaPagamento The ChildFormaPagamento object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeFormaPagamento(ChildFormaPagamento $formaPagamento)
    {
        if ($this->getFormaPagamentos()->contains($formaPagamento)) {
            $pos = $this->collFormaPagamentos->search($formaPagamento);
            $this->collFormaPagamentos->remove($pos);
            if (null === $this->formaPagamentosScheduledForDeletion) {
                $this->formaPagamentosScheduledForDeletion = clone $this->collFormaPagamentos;
                $this->formaPagamentosScheduledForDeletion->clear();
            }
            $this->formaPagamentosScheduledForDeletion[]= clone $formaPagamento;
            $formaPagamento->setUsuario(null);
        }

        return $this;
    }

    /**
     * Clears out the collFornecedors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFornecedors()
     */
    public function clearFornecedors()
    {
        $this->collFornecedors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFornecedors collection loaded partially.
     */
    public function resetPartialFornecedors($v = true)
    {
        $this->collFornecedorsPartial = $v;
    }

    /**
     * Initializes the collFornecedors collection.
     *
     * By default this just sets the collFornecedors collection to an empty array (like clearcollFornecedors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFornecedors($overrideExisting = true)
    {
        if (null !== $this->collFornecedors && !$overrideExisting) {
            return;
        }

        $collectionClassName = FornecedorTableMap::getTableMap()->getCollectionClassName();

        $this->collFornecedors = new $collectionClassName;
        $this->collFornecedors->setModel('\ImaTelecomBundle\Model\Fornecedor');
    }

    /**
     * Gets an array of ChildFornecedor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     * @throws PropelException
     */
    public function getFornecedors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFornecedorsPartial && !$this->isNew();
        if (null === $this->collFornecedors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFornecedors) {
                // return empty collection
                $this->initFornecedors();
            } else {
                $collFornecedors = ChildFornecedorQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFornecedorsPartial && count($collFornecedors)) {
                        $this->initFornecedors(false);

                        foreach ($collFornecedors as $obj) {
                            if (false == $this->collFornecedors->contains($obj)) {
                                $this->collFornecedors->append($obj);
                            }
                        }

                        $this->collFornecedorsPartial = true;
                    }

                    return $collFornecedors;
                }

                if ($partial && $this->collFornecedors) {
                    foreach ($this->collFornecedors as $obj) {
                        if ($obj->isNew()) {
                            $collFornecedors[] = $obj;
                        }
                    }
                }

                $this->collFornecedors = $collFornecedors;
                $this->collFornecedorsPartial = false;
            }
        }

        return $this->collFornecedors;
    }

    /**
     * Sets a collection of ChildFornecedor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $fornecedors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setFornecedors(Collection $fornecedors, ConnectionInterface $con = null)
    {
        /** @var ChildFornecedor[] $fornecedorsToDelete */
        $fornecedorsToDelete = $this->getFornecedors(new Criteria(), $con)->diff($fornecedors);


        $this->fornecedorsScheduledForDeletion = $fornecedorsToDelete;

        foreach ($fornecedorsToDelete as $fornecedorRemoved) {
            $fornecedorRemoved->setUsuario(null);
        }

        $this->collFornecedors = null;
        foreach ($fornecedors as $fornecedor) {
            $this->addFornecedor($fornecedor);
        }

        $this->collFornecedors = $fornecedors;
        $this->collFornecedorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Fornecedor objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Fornecedor objects.
     * @throws PropelException
     */
    public function countFornecedors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFornecedorsPartial && !$this->isNew();
        if (null === $this->collFornecedors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFornecedors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFornecedors());
            }

            $query = ChildFornecedorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collFornecedors);
    }

    /**
     * Method called to associate a ChildFornecedor object to this object
     * through the ChildFornecedor foreign key attribute.
     *
     * @param  ChildFornecedor $l ChildFornecedor
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addFornecedor(ChildFornecedor $l)
    {
        if ($this->collFornecedors === null) {
            $this->initFornecedors();
            $this->collFornecedorsPartial = true;
        }

        if (!$this->collFornecedors->contains($l)) {
            $this->doAddFornecedor($l);

            if ($this->fornecedorsScheduledForDeletion and $this->fornecedorsScheduledForDeletion->contains($l)) {
                $this->fornecedorsScheduledForDeletion->remove($this->fornecedorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFornecedor $fornecedor The ChildFornecedor object to add.
     */
    protected function doAddFornecedor(ChildFornecedor $fornecedor)
    {
        $this->collFornecedors[]= $fornecedor;
        $fornecedor->setUsuario($this);
    }

    /**
     * @param  ChildFornecedor $fornecedor The ChildFornecedor object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeFornecedor(ChildFornecedor $fornecedor)
    {
        if ($this->getFornecedors()->contains($fornecedor)) {
            $pos = $this->collFornecedors->search($fornecedor);
            $this->collFornecedors->remove($pos);
            if (null === $this->fornecedorsScheduledForDeletion) {
                $this->fornecedorsScheduledForDeletion = clone $this->collFornecedors;
                $this->fornecedorsScheduledForDeletion->clear();
            }
            $this->fornecedorsScheduledForDeletion[]= clone $fornecedor;
            $fornecedor->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinBancoAgenciaConta(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('BancoAgenciaConta', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }

    /**
     * Clears out the collItemComprados collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addItemComprados()
     */
    public function clearItemComprados()
    {
        $this->collItemComprados = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collItemComprados collection loaded partially.
     */
    public function resetPartialItemComprados($v = true)
    {
        $this->collItemCompradosPartial = $v;
    }

    /**
     * Initializes the collItemComprados collection.
     *
     * By default this just sets the collItemComprados collection to an empty array (like clearcollItemComprados());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initItemComprados($overrideExisting = true)
    {
        if (null !== $this->collItemComprados && !$overrideExisting) {
            return;
        }

        $collectionClassName = ItemCompradoTableMap::getTableMap()->getCollectionClassName();

        $this->collItemComprados = new $collectionClassName;
        $this->collItemComprados->setModel('\ImaTelecomBundle\Model\ItemComprado');
    }

    /**
     * Gets an array of ChildItemComprado objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildItemComprado[] List of ChildItemComprado objects
     * @throws PropelException
     */
    public function getItemComprados(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collItemCompradosPartial && !$this->isNew();
        if (null === $this->collItemComprados || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collItemComprados) {
                // return empty collection
                $this->initItemComprados();
            } else {
                $collItemComprados = ChildItemCompradoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collItemCompradosPartial && count($collItemComprados)) {
                        $this->initItemComprados(false);

                        foreach ($collItemComprados as $obj) {
                            if (false == $this->collItemComprados->contains($obj)) {
                                $this->collItemComprados->append($obj);
                            }
                        }

                        $this->collItemCompradosPartial = true;
                    }

                    return $collItemComprados;
                }

                if ($partial && $this->collItemComprados) {
                    foreach ($this->collItemComprados as $obj) {
                        if ($obj->isNew()) {
                            $collItemComprados[] = $obj;
                        }
                    }
                }

                $this->collItemComprados = $collItemComprados;
                $this->collItemCompradosPartial = false;
            }
        }

        return $this->collItemComprados;
    }

    /**
     * Sets a collection of ChildItemComprado objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $itemComprados A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setItemComprados(Collection $itemComprados, ConnectionInterface $con = null)
    {
        /** @var ChildItemComprado[] $itemCompradosToDelete */
        $itemCompradosToDelete = $this->getItemComprados(new Criteria(), $con)->diff($itemComprados);


        $this->itemCompradosScheduledForDeletion = $itemCompradosToDelete;

        foreach ($itemCompradosToDelete as $itemCompradoRemoved) {
            $itemCompradoRemoved->setUsuario(null);
        }

        $this->collItemComprados = null;
        foreach ($itemComprados as $itemComprado) {
            $this->addItemComprado($itemComprado);
        }

        $this->collItemComprados = $itemComprados;
        $this->collItemCompradosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ItemComprado objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ItemComprado objects.
     * @throws PropelException
     */
    public function countItemComprados(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collItemCompradosPartial && !$this->isNew();
        if (null === $this->collItemComprados || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collItemComprados) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getItemComprados());
            }

            $query = ChildItemCompradoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collItemComprados);
    }

    /**
     * Method called to associate a ChildItemComprado object to this object
     * through the ChildItemComprado foreign key attribute.
     *
     * @param  ChildItemComprado $l ChildItemComprado
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addItemComprado(ChildItemComprado $l)
    {
        if ($this->collItemComprados === null) {
            $this->initItemComprados();
            $this->collItemCompradosPartial = true;
        }

        if (!$this->collItemComprados->contains($l)) {
            $this->doAddItemComprado($l);

            if ($this->itemCompradosScheduledForDeletion and $this->itemCompradosScheduledForDeletion->contains($l)) {
                $this->itemCompradosScheduledForDeletion->remove($this->itemCompradosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildItemComprado $itemComprado The ChildItemComprado object to add.
     */
    protected function doAddItemComprado(ChildItemComprado $itemComprado)
    {
        $this->collItemComprados[]= $itemComprado;
        $itemComprado->setUsuario($this);
    }

    /**
     * @param  ChildItemComprado $itemComprado The ChildItemComprado object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeItemComprado(ChildItemComprado $itemComprado)
    {
        if ($this->getItemComprados()->contains($itemComprado)) {
            $pos = $this->collItemComprados->search($itemComprado);
            $this->collItemComprados->remove($pos);
            if (null === $this->itemCompradosScheduledForDeletion) {
                $this->itemCompradosScheduledForDeletion = clone $this->collItemComprados;
                $this->itemCompradosScheduledForDeletion->clear();
            }
            $this->itemCompradosScheduledForDeletion[]= clone $itemComprado;
            $itemComprado->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ItemComprados from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildItemComprado[] List of ChildItemComprado objects
     */
    public function getItemCompradosJoinContasPagar(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildItemCompradoQuery::create(null, $criteria);
        $query->joinWith('ContasPagar', $joinBehavior);

        return $this->getItemComprados($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ItemComprados from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildItemComprado[] List of ChildItemComprado objects
     */
    public function getItemCompradosJoinItemDeCompra(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildItemCompradoQuery::create(null, $criteria);
        $query->joinWith('ItemDeCompra', $joinBehavior);

        return $this->getItemComprados($query, $con);
    }

    /**
     * Clears out the collItemDeCompras collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addItemDeCompras()
     */
    public function clearItemDeCompras()
    {
        $this->collItemDeCompras = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collItemDeCompras collection loaded partially.
     */
    public function resetPartialItemDeCompras($v = true)
    {
        $this->collItemDeComprasPartial = $v;
    }

    /**
     * Initializes the collItemDeCompras collection.
     *
     * By default this just sets the collItemDeCompras collection to an empty array (like clearcollItemDeCompras());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initItemDeCompras($overrideExisting = true)
    {
        if (null !== $this->collItemDeCompras && !$overrideExisting) {
            return;
        }

        $collectionClassName = ItemDeCompraTableMap::getTableMap()->getCollectionClassName();

        $this->collItemDeCompras = new $collectionClassName;
        $this->collItemDeCompras->setModel('\ImaTelecomBundle\Model\ItemDeCompra');
    }

    /**
     * Gets an array of ChildItemDeCompra objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildItemDeCompra[] List of ChildItemDeCompra objects
     * @throws PropelException
     */
    public function getItemDeCompras(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collItemDeComprasPartial && !$this->isNew();
        if (null === $this->collItemDeCompras || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collItemDeCompras) {
                // return empty collection
                $this->initItemDeCompras();
            } else {
                $collItemDeCompras = ChildItemDeCompraQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collItemDeComprasPartial && count($collItemDeCompras)) {
                        $this->initItemDeCompras(false);

                        foreach ($collItemDeCompras as $obj) {
                            if (false == $this->collItemDeCompras->contains($obj)) {
                                $this->collItemDeCompras->append($obj);
                            }
                        }

                        $this->collItemDeComprasPartial = true;
                    }

                    return $collItemDeCompras;
                }

                if ($partial && $this->collItemDeCompras) {
                    foreach ($this->collItemDeCompras as $obj) {
                        if ($obj->isNew()) {
                            $collItemDeCompras[] = $obj;
                        }
                    }
                }

                $this->collItemDeCompras = $collItemDeCompras;
                $this->collItemDeComprasPartial = false;
            }
        }

        return $this->collItemDeCompras;
    }

    /**
     * Sets a collection of ChildItemDeCompra objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $itemDeCompras A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setItemDeCompras(Collection $itemDeCompras, ConnectionInterface $con = null)
    {
        /** @var ChildItemDeCompra[] $itemDeComprasToDelete */
        $itemDeComprasToDelete = $this->getItemDeCompras(new Criteria(), $con)->diff($itemDeCompras);


        $this->itemDeComprasScheduledForDeletion = $itemDeComprasToDelete;

        foreach ($itemDeComprasToDelete as $itemDeCompraRemoved) {
            $itemDeCompraRemoved->setUsuario(null);
        }

        $this->collItemDeCompras = null;
        foreach ($itemDeCompras as $itemDeCompra) {
            $this->addItemDeCompra($itemDeCompra);
        }

        $this->collItemDeCompras = $itemDeCompras;
        $this->collItemDeComprasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ItemDeCompra objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ItemDeCompra objects.
     * @throws PropelException
     */
    public function countItemDeCompras(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collItemDeComprasPartial && !$this->isNew();
        if (null === $this->collItemDeCompras || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collItemDeCompras) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getItemDeCompras());
            }

            $query = ChildItemDeCompraQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collItemDeCompras);
    }

    /**
     * Method called to associate a ChildItemDeCompra object to this object
     * through the ChildItemDeCompra foreign key attribute.
     *
     * @param  ChildItemDeCompra $l ChildItemDeCompra
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addItemDeCompra(ChildItemDeCompra $l)
    {
        if ($this->collItemDeCompras === null) {
            $this->initItemDeCompras();
            $this->collItemDeComprasPartial = true;
        }

        if (!$this->collItemDeCompras->contains($l)) {
            $this->doAddItemDeCompra($l);

            if ($this->itemDeComprasScheduledForDeletion and $this->itemDeComprasScheduledForDeletion->contains($l)) {
                $this->itemDeComprasScheduledForDeletion->remove($this->itemDeComprasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildItemDeCompra $itemDeCompra The ChildItemDeCompra object to add.
     */
    protected function doAddItemDeCompra(ChildItemDeCompra $itemDeCompra)
    {
        $this->collItemDeCompras[]= $itemDeCompra;
        $itemDeCompra->setUsuario($this);
    }

    /**
     * @param  ChildItemDeCompra $itemDeCompra The ChildItemDeCompra object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeItemDeCompra(ChildItemDeCompra $itemDeCompra)
    {
        if ($this->getItemDeCompras()->contains($itemDeCompra)) {
            $pos = $this->collItemDeCompras->search($itemDeCompra);
            $this->collItemDeCompras->remove($pos);
            if (null === $this->itemDeComprasScheduledForDeletion) {
                $this->itemDeComprasScheduledForDeletion = clone $this->collItemDeCompras;
                $this->itemDeComprasScheduledForDeletion->clear();
            }
            $this->itemDeComprasScheduledForDeletion[]= clone $itemDeCompra;
            $itemDeCompra->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ItemDeCompras from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildItemDeCompra[] List of ChildItemDeCompra objects
     */
    public function getItemDeComprasJoinClassificacaoFinanceira(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildItemDeCompraQuery::create(null, $criteria);
        $query->joinWith('ClassificacaoFinanceira', $joinBehavior);

        return $this->getItemDeCompras($query, $con);
    }

    /**
     * Clears out the collServicoClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicoClientes()
     */
    public function clearServicoClientes()
    {
        $this->collServicoClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicoClientes collection loaded partially.
     */
    public function resetPartialServicoClientes($v = true)
    {
        $this->collServicoClientesPartial = $v;
    }

    /**
     * Initializes the collServicoClientes collection.
     *
     * By default this just sets the collServicoClientes collection to an empty array (like clearcollServicoClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicoClientes($overrideExisting = true)
    {
        if (null !== $this->collServicoClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicoClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collServicoClientes = new $collectionClassName;
        $this->collServicoClientes->setModel('\ImaTelecomBundle\Model\ServicoCliente');
    }

    /**
     * Gets an array of ChildServicoCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     * @throws PropelException
     */
    public function getServicoClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoClientesPartial && !$this->isNew();
        if (null === $this->collServicoClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicoClientes) {
                // return empty collection
                $this->initServicoClientes();
            } else {
                $collServicoClientes = ChildServicoClienteQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicoClientesPartial && count($collServicoClientes)) {
                        $this->initServicoClientes(false);

                        foreach ($collServicoClientes as $obj) {
                            if (false == $this->collServicoClientes->contains($obj)) {
                                $this->collServicoClientes->append($obj);
                            }
                        }

                        $this->collServicoClientesPartial = true;
                    }

                    return $collServicoClientes;
                }

                if ($partial && $this->collServicoClientes) {
                    foreach ($this->collServicoClientes as $obj) {
                        if ($obj->isNew()) {
                            $collServicoClientes[] = $obj;
                        }
                    }
                }

                $this->collServicoClientes = $collServicoClientes;
                $this->collServicoClientesPartial = false;
            }
        }

        return $this->collServicoClientes;
    }

    /**
     * Sets a collection of ChildServicoCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicoClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setServicoClientes(Collection $servicoClientes, ConnectionInterface $con = null)
    {
        /** @var ChildServicoCliente[] $servicoClientesToDelete */
        $servicoClientesToDelete = $this->getServicoClientes(new Criteria(), $con)->diff($servicoClientes);


        $this->servicoClientesScheduledForDeletion = $servicoClientesToDelete;

        foreach ($servicoClientesToDelete as $servicoClienteRemoved) {
            $servicoClienteRemoved->setUsuario(null);
        }

        $this->collServicoClientes = null;
        foreach ($servicoClientes as $servicoCliente) {
            $this->addServicoCliente($servicoCliente);
        }

        $this->collServicoClientes = $servicoClientes;
        $this->collServicoClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ServicoCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ServicoCliente objects.
     * @throws PropelException
     */
    public function countServicoClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoClientesPartial && !$this->isNew();
        if (null === $this->collServicoClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicoClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicoClientes());
            }

            $query = ChildServicoClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collServicoClientes);
    }

    /**
     * Method called to associate a ChildServicoCliente object to this object
     * through the ChildServicoCliente foreign key attribute.
     *
     * @param  ChildServicoCliente $l ChildServicoCliente
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addServicoCliente(ChildServicoCliente $l)
    {
        if ($this->collServicoClientes === null) {
            $this->initServicoClientes();
            $this->collServicoClientesPartial = true;
        }

        if (!$this->collServicoClientes->contains($l)) {
            $this->doAddServicoCliente($l);

            if ($this->servicoClientesScheduledForDeletion and $this->servicoClientesScheduledForDeletion->contains($l)) {
                $this->servicoClientesScheduledForDeletion->remove($this->servicoClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicoCliente $servicoCliente The ChildServicoCliente object to add.
     */
    protected function doAddServicoCliente(ChildServicoCliente $servicoCliente)
    {
        $this->collServicoClientes[]= $servicoCliente;
        $servicoCliente->setUsuario($this);
    }

    /**
     * @param  ChildServicoCliente $servicoCliente The ChildServicoCliente object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeServicoCliente(ChildServicoCliente $servicoCliente)
    {
        if ($this->getServicoClientes()->contains($servicoCliente)) {
            $pos = $this->collServicoClientes->search($servicoCliente);
            $this->collServicoClientes->remove($pos);
            if (null === $this->servicoClientesScheduledForDeletion) {
                $this->servicoClientesScheduledForDeletion = clone $this->collServicoClientes;
                $this->servicoClientesScheduledForDeletion->clear();
            }
            $this->servicoClientesScheduledForDeletion[]= clone $servicoCliente;
            $servicoCliente->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinContrato(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('Contrato', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinServicoPrestado(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('ServicoPrestado', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }

    /**
     * Clears out the collServicoPrestados collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicoPrestados()
     */
    public function clearServicoPrestados()
    {
        $this->collServicoPrestados = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicoPrestados collection loaded partially.
     */
    public function resetPartialServicoPrestados($v = true)
    {
        $this->collServicoPrestadosPartial = $v;
    }

    /**
     * Initializes the collServicoPrestados collection.
     *
     * By default this just sets the collServicoPrestados collection to an empty array (like clearcollServicoPrestados());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicoPrestados($overrideExisting = true)
    {
        if (null !== $this->collServicoPrestados && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicoPrestadoTableMap::getTableMap()->getCollectionClassName();

        $this->collServicoPrestados = new $collectionClassName;
        $this->collServicoPrestados->setModel('\ImaTelecomBundle\Model\ServicoPrestado');
    }

    /**
     * Gets an array of ChildServicoPrestado objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicoPrestado[] List of ChildServicoPrestado objects
     * @throws PropelException
     */
    public function getServicoPrestados(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoPrestadosPartial && !$this->isNew();
        if (null === $this->collServicoPrestados || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicoPrestados) {
                // return empty collection
                $this->initServicoPrestados();
            } else {
                $collServicoPrestados = ChildServicoPrestadoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicoPrestadosPartial && count($collServicoPrestados)) {
                        $this->initServicoPrestados(false);

                        foreach ($collServicoPrestados as $obj) {
                            if (false == $this->collServicoPrestados->contains($obj)) {
                                $this->collServicoPrestados->append($obj);
                            }
                        }

                        $this->collServicoPrestadosPartial = true;
                    }

                    return $collServicoPrestados;
                }

                if ($partial && $this->collServicoPrestados) {
                    foreach ($this->collServicoPrestados as $obj) {
                        if ($obj->isNew()) {
                            $collServicoPrestados[] = $obj;
                        }
                    }
                }

                $this->collServicoPrestados = $collServicoPrestados;
                $this->collServicoPrestadosPartial = false;
            }
        }

        return $this->collServicoPrestados;
    }

    /**
     * Sets a collection of ChildServicoPrestado objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicoPrestados A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setServicoPrestados(Collection $servicoPrestados, ConnectionInterface $con = null)
    {
        /** @var ChildServicoPrestado[] $servicoPrestadosToDelete */
        $servicoPrestadosToDelete = $this->getServicoPrestados(new Criteria(), $con)->diff($servicoPrestados);


        $this->servicoPrestadosScheduledForDeletion = $servicoPrestadosToDelete;

        foreach ($servicoPrestadosToDelete as $servicoPrestadoRemoved) {
            $servicoPrestadoRemoved->setUsuario(null);
        }

        $this->collServicoPrestados = null;
        foreach ($servicoPrestados as $servicoPrestado) {
            $this->addServicoPrestado($servicoPrestado);
        }

        $this->collServicoPrestados = $servicoPrestados;
        $this->collServicoPrestadosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ServicoPrestado objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ServicoPrestado objects.
     * @throws PropelException
     */
    public function countServicoPrestados(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoPrestadosPartial && !$this->isNew();
        if (null === $this->collServicoPrestados || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicoPrestados) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicoPrestados());
            }

            $query = ChildServicoPrestadoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collServicoPrestados);
    }

    /**
     * Method called to associate a ChildServicoPrestado object to this object
     * through the ChildServicoPrestado foreign key attribute.
     *
     * @param  ChildServicoPrestado $l ChildServicoPrestado
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addServicoPrestado(ChildServicoPrestado $l)
    {
        if ($this->collServicoPrestados === null) {
            $this->initServicoPrestados();
            $this->collServicoPrestadosPartial = true;
        }

        if (!$this->collServicoPrestados->contains($l)) {
            $this->doAddServicoPrestado($l);

            if ($this->servicoPrestadosScheduledForDeletion and $this->servicoPrestadosScheduledForDeletion->contains($l)) {
                $this->servicoPrestadosScheduledForDeletion->remove($this->servicoPrestadosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicoPrestado $servicoPrestado The ChildServicoPrestado object to add.
     */
    protected function doAddServicoPrestado(ChildServicoPrestado $servicoPrestado)
    {
        $this->collServicoPrestados[]= $servicoPrestado;
        $servicoPrestado->setUsuario($this);
    }

    /**
     * @param  ChildServicoPrestado $servicoPrestado The ChildServicoPrestado object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeServicoPrestado(ChildServicoPrestado $servicoPrestado)
    {
        if ($this->getServicoPrestados()->contains($servicoPrestado)) {
            $pos = $this->collServicoPrestados->search($servicoPrestado);
            $this->collServicoPrestados->remove($pos);
            if (null === $this->servicoPrestadosScheduledForDeletion) {
                $this->servicoPrestadosScheduledForDeletion = clone $this->collServicoPrestados;
                $this->servicoPrestadosScheduledForDeletion->clear();
            }
            $this->servicoPrestadosScheduledForDeletion[]= clone $servicoPrestado;
            $servicoPrestado->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ServicoPrestados from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoPrestado[] List of ChildServicoPrestado objects
     */
    public function getServicoPrestadosJoinPlanos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoPrestadoQuery::create(null, $criteria);
        $query->joinWith('Planos', $joinBehavior);

        return $this->getServicoPrestados($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ServicoPrestados from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoPrestado[] List of ChildServicoPrestado objects
     */
    public function getServicoPrestadosJoinTipoServicoPrestado(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoPrestadoQuery::create(null, $criteria);
        $query->joinWith('TipoServicoPrestado', $joinBehavior);

        return $this->getServicoPrestados($query, $con);
    }

    /**
     * Clears out the collSicis collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSicis()
     */
    public function clearSicis()
    {
        $this->collSicis = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSicis collection loaded partially.
     */
    public function resetPartialSicis($v = true)
    {
        $this->collSicisPartial = $v;
    }

    /**
     * Initializes the collSicis collection.
     *
     * By default this just sets the collSicis collection to an empty array (like clearcollSicis());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSicis($overrideExisting = true)
    {
        if (null !== $this->collSicis && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiciTableMap::getTableMap()->getCollectionClassName();

        $this->collSicis = new $collectionClassName;
        $this->collSicis->setModel('\ImaTelecomBundle\Model\Sici');
    }

    /**
     * Gets an array of ChildSici objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSici[] List of ChildSici objects
     * @throws PropelException
     */
    public function getSicis(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSicisPartial && !$this->isNew();
        if (null === $this->collSicis || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSicis) {
                // return empty collection
                $this->initSicis();
            } else {
                $collSicis = ChildSiciQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSicisPartial && count($collSicis)) {
                        $this->initSicis(false);

                        foreach ($collSicis as $obj) {
                            if (false == $this->collSicis->contains($obj)) {
                                $this->collSicis->append($obj);
                            }
                        }

                        $this->collSicisPartial = true;
                    }

                    return $collSicis;
                }

                if ($partial && $this->collSicis) {
                    foreach ($this->collSicis as $obj) {
                        if ($obj->isNew()) {
                            $collSicis[] = $obj;
                        }
                    }
                }

                $this->collSicis = $collSicis;
                $this->collSicisPartial = false;
            }
        }

        return $this->collSicis;
    }

    /**
     * Sets a collection of ChildSici objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sicis A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setSicis(Collection $sicis, ConnectionInterface $con = null)
    {
        /** @var ChildSici[] $sicisToDelete */
        $sicisToDelete = $this->getSicis(new Criteria(), $con)->diff($sicis);


        $this->sicisScheduledForDeletion = $sicisToDelete;

        foreach ($sicisToDelete as $siciRemoved) {
            $siciRemoved->setUsuario(null);
        }

        $this->collSicis = null;
        foreach ($sicis as $sici) {
            $this->addSici($sici);
        }

        $this->collSicis = $sicis;
        $this->collSicisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sici objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Sici objects.
     * @throws PropelException
     */
    public function countSicis(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSicisPartial && !$this->isNew();
        if (null === $this->collSicis || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSicis) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSicis());
            }

            $query = ChildSiciQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collSicis);
    }

    /**
     * Method called to associate a ChildSici object to this object
     * through the ChildSici foreign key attribute.
     *
     * @param  ChildSici $l ChildSici
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addSici(ChildSici $l)
    {
        if ($this->collSicis === null) {
            $this->initSicis();
            $this->collSicisPartial = true;
        }

        if (!$this->collSicis->contains($l)) {
            $this->doAddSici($l);

            if ($this->sicisScheduledForDeletion and $this->sicisScheduledForDeletion->contains($l)) {
                $this->sicisScheduledForDeletion->remove($this->sicisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSici $sici The ChildSici object to add.
     */
    protected function doAddSici(ChildSici $sici)
    {
        $this->collSicis[]= $sici;
        $sici->setUsuario($this);
    }

    /**
     * @param  ChildSici $sici The ChildSici object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeSici(ChildSici $sici)
    {
        if ($this->getSicis()->contains($sici)) {
            $pos = $this->collSicis->search($sici);
            $this->collSicis->remove($pos);
            if (null === $this->sicisScheduledForDeletion) {
                $this->sicisScheduledForDeletion = clone $this->collSicis;
                $this->sicisScheduledForDeletion->clear();
            }
            $this->sicisScheduledForDeletion[]= clone $sici;
            $sici->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Sicis from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSici[] List of ChildSici objects
     */
    public function getSicisJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiciQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getSicis($query, $con);
    }

    /**
     * Clears out the collSysPerfils collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSysPerfils()
     */
    public function clearSysPerfils()
    {
        $this->collSysPerfils = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSysPerfils collection loaded partially.
     */
    public function resetPartialSysPerfils($v = true)
    {
        $this->collSysPerfilsPartial = $v;
    }

    /**
     * Initializes the collSysPerfils collection.
     *
     * By default this just sets the collSysPerfils collection to an empty array (like clearcollSysPerfils());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysPerfils($overrideExisting = true)
    {
        if (null !== $this->collSysPerfils && !$overrideExisting) {
            return;
        }

        $collectionClassName = SysPerfilTableMap::getTableMap()->getCollectionClassName();

        $this->collSysPerfils = new $collectionClassName;
        $this->collSysPerfils->setModel('\ImaTelecomBundle\Model\SysPerfil');
    }

    /**
     * Gets an array of ChildSysPerfil objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSysPerfil[] List of ChildSysPerfil objects
     * @throws PropelException
     */
    public function getSysPerfils(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSysPerfilsPartial && !$this->isNew();
        if (null === $this->collSysPerfils || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysPerfils) {
                // return empty collection
                $this->initSysPerfils();
            } else {
                $collSysPerfils = ChildSysPerfilQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSysPerfilsPartial && count($collSysPerfils)) {
                        $this->initSysPerfils(false);

                        foreach ($collSysPerfils as $obj) {
                            if (false == $this->collSysPerfils->contains($obj)) {
                                $this->collSysPerfils->append($obj);
                            }
                        }

                        $this->collSysPerfilsPartial = true;
                    }

                    return $collSysPerfils;
                }

                if ($partial && $this->collSysPerfils) {
                    foreach ($this->collSysPerfils as $obj) {
                        if ($obj->isNew()) {
                            $collSysPerfils[] = $obj;
                        }
                    }
                }

                $this->collSysPerfils = $collSysPerfils;
                $this->collSysPerfilsPartial = false;
            }
        }

        return $this->collSysPerfils;
    }

    /**
     * Sets a collection of ChildSysPerfil objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sysPerfils A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setSysPerfils(Collection $sysPerfils, ConnectionInterface $con = null)
    {
        /** @var ChildSysPerfil[] $sysPerfilsToDelete */
        $sysPerfilsToDelete = $this->getSysPerfils(new Criteria(), $con)->diff($sysPerfils);


        $this->sysPerfilsScheduledForDeletion = $sysPerfilsToDelete;

        foreach ($sysPerfilsToDelete as $sysPerfilRemoved) {
            $sysPerfilRemoved->setUsuario(null);
        }

        $this->collSysPerfils = null;
        foreach ($sysPerfils as $sysPerfil) {
            $this->addSysPerfil($sysPerfil);
        }

        $this->collSysPerfils = $sysPerfils;
        $this->collSysPerfilsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysPerfil objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SysPerfil objects.
     * @throws PropelException
     */
    public function countSysPerfils(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSysPerfilsPartial && !$this->isNew();
        if (null === $this->collSysPerfils || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysPerfils) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysPerfils());
            }

            $query = ChildSysPerfilQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collSysPerfils);
    }

    /**
     * Method called to associate a ChildSysPerfil object to this object
     * through the ChildSysPerfil foreign key attribute.
     *
     * @param  ChildSysPerfil $l ChildSysPerfil
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addSysPerfil(ChildSysPerfil $l)
    {
        if ($this->collSysPerfils === null) {
            $this->initSysPerfils();
            $this->collSysPerfilsPartial = true;
        }

        if (!$this->collSysPerfils->contains($l)) {
            $this->doAddSysPerfil($l);

            if ($this->sysPerfilsScheduledForDeletion and $this->sysPerfilsScheduledForDeletion->contains($l)) {
                $this->sysPerfilsScheduledForDeletion->remove($this->sysPerfilsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSysPerfil $sysPerfil The ChildSysPerfil object to add.
     */
    protected function doAddSysPerfil(ChildSysPerfil $sysPerfil)
    {
        $this->collSysPerfils[]= $sysPerfil;
        $sysPerfil->setUsuario($this);
    }

    /**
     * @param  ChildSysPerfil $sysPerfil The ChildSysPerfil object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeSysPerfil(ChildSysPerfil $sysPerfil)
    {
        if ($this->getSysPerfils()->contains($sysPerfil)) {
            $pos = $this->collSysPerfils->search($sysPerfil);
            $this->collSysPerfils->remove($pos);
            if (null === $this->sysPerfilsScheduledForDeletion) {
                $this->sysPerfilsScheduledForDeletion = clone $this->collSysPerfils;
                $this->sysPerfilsScheduledForDeletion->clear();
            }
            $this->sysPerfilsScheduledForDeletion[]= clone $sysPerfil;
            $sysPerfil->setUsuario(null);
        }

        return $this;
    }

    /**
     * Clears out the collSysProcessoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSysProcessoss()
     */
    public function clearSysProcessoss()
    {
        $this->collSysProcessoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSysProcessoss collection loaded partially.
     */
    public function resetPartialSysProcessoss($v = true)
    {
        $this->collSysProcessossPartial = $v;
    }

    /**
     * Initializes the collSysProcessoss collection.
     *
     * By default this just sets the collSysProcessoss collection to an empty array (like clearcollSysProcessoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysProcessoss($overrideExisting = true)
    {
        if (null !== $this->collSysProcessoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = SysProcessosTableMap::getTableMap()->getCollectionClassName();

        $this->collSysProcessoss = new $collectionClassName;
        $this->collSysProcessoss->setModel('\ImaTelecomBundle\Model\SysProcessos');
    }

    /**
     * Gets an array of ChildSysProcessos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSysProcessos[] List of ChildSysProcessos objects
     * @throws PropelException
     */
    public function getSysProcessoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSysProcessossPartial && !$this->isNew();
        if (null === $this->collSysProcessoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysProcessoss) {
                // return empty collection
                $this->initSysProcessoss();
            } else {
                $collSysProcessoss = ChildSysProcessosQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSysProcessossPartial && count($collSysProcessoss)) {
                        $this->initSysProcessoss(false);

                        foreach ($collSysProcessoss as $obj) {
                            if (false == $this->collSysProcessoss->contains($obj)) {
                                $this->collSysProcessoss->append($obj);
                            }
                        }

                        $this->collSysProcessossPartial = true;
                    }

                    return $collSysProcessoss;
                }

                if ($partial && $this->collSysProcessoss) {
                    foreach ($this->collSysProcessoss as $obj) {
                        if ($obj->isNew()) {
                            $collSysProcessoss[] = $obj;
                        }
                    }
                }

                $this->collSysProcessoss = $collSysProcessoss;
                $this->collSysProcessossPartial = false;
            }
        }

        return $this->collSysProcessoss;
    }

    /**
     * Sets a collection of ChildSysProcessos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sysProcessoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setSysProcessoss(Collection $sysProcessoss, ConnectionInterface $con = null)
    {
        /** @var ChildSysProcessos[] $sysProcessossToDelete */
        $sysProcessossToDelete = $this->getSysProcessoss(new Criteria(), $con)->diff($sysProcessoss);


        $this->sysProcessossScheduledForDeletion = $sysProcessossToDelete;

        foreach ($sysProcessossToDelete as $sysProcessosRemoved) {
            $sysProcessosRemoved->setUsuario(null);
        }

        $this->collSysProcessoss = null;
        foreach ($sysProcessoss as $sysProcessos) {
            $this->addSysProcessos($sysProcessos);
        }

        $this->collSysProcessoss = $sysProcessoss;
        $this->collSysProcessossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysProcessos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SysProcessos objects.
     * @throws PropelException
     */
    public function countSysProcessoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSysProcessossPartial && !$this->isNew();
        if (null === $this->collSysProcessoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysProcessoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysProcessoss());
            }

            $query = ChildSysProcessosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collSysProcessoss);
    }

    /**
     * Method called to associate a ChildSysProcessos object to this object
     * through the ChildSysProcessos foreign key attribute.
     *
     * @param  ChildSysProcessos $l ChildSysProcessos
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addSysProcessos(ChildSysProcessos $l)
    {
        if ($this->collSysProcessoss === null) {
            $this->initSysProcessoss();
            $this->collSysProcessossPartial = true;
        }

        if (!$this->collSysProcessoss->contains($l)) {
            $this->doAddSysProcessos($l);

            if ($this->sysProcessossScheduledForDeletion and $this->sysProcessossScheduledForDeletion->contains($l)) {
                $this->sysProcessossScheduledForDeletion->remove($this->sysProcessossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSysProcessos $sysProcessos The ChildSysProcessos object to add.
     */
    protected function doAddSysProcessos(ChildSysProcessos $sysProcessos)
    {
        $this->collSysProcessoss[]= $sysProcessos;
        $sysProcessos->setUsuario($this);
    }

    /**
     * @param  ChildSysProcessos $sysProcessos The ChildSysProcessos object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeSysProcessos(ChildSysProcessos $sysProcessos)
    {
        if ($this->getSysProcessoss()->contains($sysProcessos)) {
            $pos = $this->collSysProcessoss->search($sysProcessos);
            $this->collSysProcessoss->remove($pos);
            if (null === $this->sysProcessossScheduledForDeletion) {
                $this->sysProcessossScheduledForDeletion = clone $this->collSysProcessoss;
                $this->sysProcessossScheduledForDeletion->clear();
            }
            $this->sysProcessossScheduledForDeletion[]= clone $sysProcessos;
            $sysProcessos->setUsuario(null);
        }

        return $this;
    }

    /**
     * Clears out the collSysUsuarioPerfils collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSysUsuarioPerfils()
     */
    public function clearSysUsuarioPerfils()
    {
        $this->collSysUsuarioPerfils = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSysUsuarioPerfils collection loaded partially.
     */
    public function resetPartialSysUsuarioPerfils($v = true)
    {
        $this->collSysUsuarioPerfilsPartial = $v;
    }

    /**
     * Initializes the collSysUsuarioPerfils collection.
     *
     * By default this just sets the collSysUsuarioPerfils collection to an empty array (like clearcollSysUsuarioPerfils());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSysUsuarioPerfils($overrideExisting = true)
    {
        if (null !== $this->collSysUsuarioPerfils && !$overrideExisting) {
            return;
        }

        $collectionClassName = SysUsuarioPerfilTableMap::getTableMap()->getCollectionClassName();

        $this->collSysUsuarioPerfils = new $collectionClassName;
        $this->collSysUsuarioPerfils->setModel('\ImaTelecomBundle\Model\SysUsuarioPerfil');
    }

    /**
     * Gets an array of ChildSysUsuarioPerfil objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSysUsuarioPerfil[] List of ChildSysUsuarioPerfil objects
     * @throws PropelException
     */
    public function getSysUsuarioPerfils(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSysUsuarioPerfilsPartial && !$this->isNew();
        if (null === $this->collSysUsuarioPerfils || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSysUsuarioPerfils) {
                // return empty collection
                $this->initSysUsuarioPerfils();
            } else {
                $collSysUsuarioPerfils = ChildSysUsuarioPerfilQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSysUsuarioPerfilsPartial && count($collSysUsuarioPerfils)) {
                        $this->initSysUsuarioPerfils(false);

                        foreach ($collSysUsuarioPerfils as $obj) {
                            if (false == $this->collSysUsuarioPerfils->contains($obj)) {
                                $this->collSysUsuarioPerfils->append($obj);
                            }
                        }

                        $this->collSysUsuarioPerfilsPartial = true;
                    }

                    return $collSysUsuarioPerfils;
                }

                if ($partial && $this->collSysUsuarioPerfils) {
                    foreach ($this->collSysUsuarioPerfils as $obj) {
                        if ($obj->isNew()) {
                            $collSysUsuarioPerfils[] = $obj;
                        }
                    }
                }

                $this->collSysUsuarioPerfils = $collSysUsuarioPerfils;
                $this->collSysUsuarioPerfilsPartial = false;
            }
        }

        return $this->collSysUsuarioPerfils;
    }

    /**
     * Sets a collection of ChildSysUsuarioPerfil objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sysUsuarioPerfils A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setSysUsuarioPerfils(Collection $sysUsuarioPerfils, ConnectionInterface $con = null)
    {
        /** @var ChildSysUsuarioPerfil[] $sysUsuarioPerfilsToDelete */
        $sysUsuarioPerfilsToDelete = $this->getSysUsuarioPerfils(new Criteria(), $con)->diff($sysUsuarioPerfils);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->sysUsuarioPerfilsScheduledForDeletion = clone $sysUsuarioPerfilsToDelete;

        foreach ($sysUsuarioPerfilsToDelete as $sysUsuarioPerfilRemoved) {
            $sysUsuarioPerfilRemoved->setUsuario(null);
        }

        $this->collSysUsuarioPerfils = null;
        foreach ($sysUsuarioPerfils as $sysUsuarioPerfil) {
            $this->addSysUsuarioPerfil($sysUsuarioPerfil);
        }

        $this->collSysUsuarioPerfils = $sysUsuarioPerfils;
        $this->collSysUsuarioPerfilsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SysUsuarioPerfil objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SysUsuarioPerfil objects.
     * @throws PropelException
     */
    public function countSysUsuarioPerfils(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSysUsuarioPerfilsPartial && !$this->isNew();
        if (null === $this->collSysUsuarioPerfils || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSysUsuarioPerfils) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSysUsuarioPerfils());
            }

            $query = ChildSysUsuarioPerfilQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collSysUsuarioPerfils);
    }

    /**
     * Method called to associate a ChildSysUsuarioPerfil object to this object
     * through the ChildSysUsuarioPerfil foreign key attribute.
     *
     * @param  ChildSysUsuarioPerfil $l ChildSysUsuarioPerfil
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addSysUsuarioPerfil(ChildSysUsuarioPerfil $l)
    {
        if ($this->collSysUsuarioPerfils === null) {
            $this->initSysUsuarioPerfils();
            $this->collSysUsuarioPerfilsPartial = true;
        }

        if (!$this->collSysUsuarioPerfils->contains($l)) {
            $this->doAddSysUsuarioPerfil($l);

            if ($this->sysUsuarioPerfilsScheduledForDeletion and $this->sysUsuarioPerfilsScheduledForDeletion->contains($l)) {
                $this->sysUsuarioPerfilsScheduledForDeletion->remove($this->sysUsuarioPerfilsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSysUsuarioPerfil $sysUsuarioPerfil The ChildSysUsuarioPerfil object to add.
     */
    protected function doAddSysUsuarioPerfil(ChildSysUsuarioPerfil $sysUsuarioPerfil)
    {
        $this->collSysUsuarioPerfils[]= $sysUsuarioPerfil;
        $sysUsuarioPerfil->setUsuario($this);
    }

    /**
     * @param  ChildSysUsuarioPerfil $sysUsuarioPerfil The ChildSysUsuarioPerfil object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeSysUsuarioPerfil(ChildSysUsuarioPerfil $sysUsuarioPerfil)
    {
        if ($this->getSysUsuarioPerfils()->contains($sysUsuarioPerfil)) {
            $pos = $this->collSysUsuarioPerfils->search($sysUsuarioPerfil);
            $this->collSysUsuarioPerfils->remove($pos);
            if (null === $this->sysUsuarioPerfilsScheduledForDeletion) {
                $this->sysUsuarioPerfilsScheduledForDeletion = clone $this->collSysUsuarioPerfils;
                $this->sysUsuarioPerfilsScheduledForDeletion->clear();
            }
            $this->sysUsuarioPerfilsScheduledForDeletion[]= clone $sysUsuarioPerfil;
            $sysUsuarioPerfil->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related SysUsuarioPerfils from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSysUsuarioPerfil[] List of ChildSysUsuarioPerfil objects
     */
    public function getSysUsuarioPerfilsJoinSysPerfil(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSysUsuarioPerfilQuery::create(null, $criteria);
        $query->joinWith('SysPerfil', $joinBehavior);

        return $this->getSysUsuarioPerfils($query, $con);
    }

    /**
     * Clears out the collTelefones collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTelefones()
     */
    public function clearTelefones()
    {
        $this->collTelefones = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTelefones collection loaded partially.
     */
    public function resetPartialTelefones($v = true)
    {
        $this->collTelefonesPartial = $v;
    }

    /**
     * Initializes the collTelefones collection.
     *
     * By default this just sets the collTelefones collection to an empty array (like clearcollTelefones());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTelefones($overrideExisting = true)
    {
        if (null !== $this->collTelefones && !$overrideExisting) {
            return;
        }

        $collectionClassName = TelefoneTableMap::getTableMap()->getCollectionClassName();

        $this->collTelefones = new $collectionClassName;
        $this->collTelefones->setModel('\ImaTelecomBundle\Model\Telefone');
    }

    /**
     * Gets an array of ChildTelefone objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTelefone[] List of ChildTelefone objects
     * @throws PropelException
     */
    public function getTelefones(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTelefonesPartial && !$this->isNew();
        if (null === $this->collTelefones || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTelefones) {
                // return empty collection
                $this->initTelefones();
            } else {
                $collTelefones = ChildTelefoneQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTelefonesPartial && count($collTelefones)) {
                        $this->initTelefones(false);

                        foreach ($collTelefones as $obj) {
                            if (false == $this->collTelefones->contains($obj)) {
                                $this->collTelefones->append($obj);
                            }
                        }

                        $this->collTelefonesPartial = true;
                    }

                    return $collTelefones;
                }

                if ($partial && $this->collTelefones) {
                    foreach ($this->collTelefones as $obj) {
                        if ($obj->isNew()) {
                            $collTelefones[] = $obj;
                        }
                    }
                }

                $this->collTelefones = $collTelefones;
                $this->collTelefonesPartial = false;
            }
        }

        return $this->collTelefones;
    }

    /**
     * Sets a collection of ChildTelefone objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $telefones A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setTelefones(Collection $telefones, ConnectionInterface $con = null)
    {
        /** @var ChildTelefone[] $telefonesToDelete */
        $telefonesToDelete = $this->getTelefones(new Criteria(), $con)->diff($telefones);


        $this->telefonesScheduledForDeletion = $telefonesToDelete;

        foreach ($telefonesToDelete as $telefoneRemoved) {
            $telefoneRemoved->setUsuario(null);
        }

        $this->collTelefones = null;
        foreach ($telefones as $telefone) {
            $this->addTelefone($telefone);
        }

        $this->collTelefones = $telefones;
        $this->collTelefonesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Telefone objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Telefone objects.
     * @throws PropelException
     */
    public function countTelefones(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTelefonesPartial && !$this->isNew();
        if (null === $this->collTelefones || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTelefones) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTelefones());
            }

            $query = ChildTelefoneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collTelefones);
    }

    /**
     * Method called to associate a ChildTelefone object to this object
     * through the ChildTelefone foreign key attribute.
     *
     * @param  ChildTelefone $l ChildTelefone
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addTelefone(ChildTelefone $l)
    {
        if ($this->collTelefones === null) {
            $this->initTelefones();
            $this->collTelefonesPartial = true;
        }

        if (!$this->collTelefones->contains($l)) {
            $this->doAddTelefone($l);

            if ($this->telefonesScheduledForDeletion and $this->telefonesScheduledForDeletion->contains($l)) {
                $this->telefonesScheduledForDeletion->remove($this->telefonesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTelefone $telefone The ChildTelefone object to add.
     */
    protected function doAddTelefone(ChildTelefone $telefone)
    {
        $this->collTelefones[]= $telefone;
        $telefone->setUsuario($this);
    }

    /**
     * @param  ChildTelefone $telefone The ChildTelefone object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeTelefone(ChildTelefone $telefone)
    {
        if ($this->getTelefones()->contains($telefone)) {
            $pos = $this->collTelefones->search($telefone);
            $this->collTelefones->remove($pos);
            if (null === $this->telefonesScheduledForDeletion) {
                $this->telefonesScheduledForDeletion = clone $this->collTelefones;
                $this->telefonesScheduledForDeletion->clear();
            }
            $this->telefonesScheduledForDeletion[]= clone $telefone;
            $telefone->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Telefones from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTelefone[] List of ChildTelefone objects
     */
    public function getTelefonesJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTelefoneQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getTelefones($query, $con);
    }

    /**
     * Clears out the collTributos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTributos()
     */
    public function clearTributos()
    {
        $this->collTributos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTributos collection loaded partially.
     */
    public function resetPartialTributos($v = true)
    {
        $this->collTributosPartial = $v;
    }

    /**
     * Initializes the collTributos collection.
     *
     * By default this just sets the collTributos collection to an empty array (like clearcollTributos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTributos($overrideExisting = true)
    {
        if (null !== $this->collTributos && !$overrideExisting) {
            return;
        }

        $collectionClassName = TributoTableMap::getTableMap()->getCollectionClassName();

        $this->collTributos = new $collectionClassName;
        $this->collTributos->setModel('\ImaTelecomBundle\Model\Tributo');
    }

    /**
     * Gets an array of ChildTributo objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTributo[] List of ChildTributo objects
     * @throws PropelException
     */
    public function getTributos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTributosPartial && !$this->isNew();
        if (null === $this->collTributos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTributos) {
                // return empty collection
                $this->initTributos();
            } else {
                $collTributos = ChildTributoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTributosPartial && count($collTributos)) {
                        $this->initTributos(false);

                        foreach ($collTributos as $obj) {
                            if (false == $this->collTributos->contains($obj)) {
                                $this->collTributos->append($obj);
                            }
                        }

                        $this->collTributosPartial = true;
                    }

                    return $collTributos;
                }

                if ($partial && $this->collTributos) {
                    foreach ($this->collTributos as $obj) {
                        if ($obj->isNew()) {
                            $collTributos[] = $obj;
                        }
                    }
                }

                $this->collTributos = $collTributos;
                $this->collTributosPartial = false;
            }
        }

        return $this->collTributos;
    }

    /**
     * Sets a collection of ChildTributo objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $tributos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setTributos(Collection $tributos, ConnectionInterface $con = null)
    {
        /** @var ChildTributo[] $tributosToDelete */
        $tributosToDelete = $this->getTributos(new Criteria(), $con)->diff($tributos);


        $this->tributosScheduledForDeletion = $tributosToDelete;

        foreach ($tributosToDelete as $tributoRemoved) {
            $tributoRemoved->setUsuario(null);
        }

        $this->collTributos = null;
        foreach ($tributos as $tributo) {
            $this->addTributo($tributo);
        }

        $this->collTributos = $tributos;
        $this->collTributosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Tributo objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Tributo objects.
     * @throws PropelException
     */
    public function countTributos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTributosPartial && !$this->isNew();
        if (null === $this->collTributos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTributos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTributos());
            }

            $query = ChildTributoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collTributos);
    }

    /**
     * Method called to associate a ChildTributo object to this object
     * through the ChildTributo foreign key attribute.
     *
     * @param  ChildTributo $l ChildTributo
     * @return $this|\ImaTelecomBundle\Model\Usuario The current object (for fluent API support)
     */
    public function addTributo(ChildTributo $l)
    {
        if ($this->collTributos === null) {
            $this->initTributos();
            $this->collTributosPartial = true;
        }

        if (!$this->collTributos->contains($l)) {
            $this->doAddTributo($l);

            if ($this->tributosScheduledForDeletion and $this->tributosScheduledForDeletion->contains($l)) {
                $this->tributosScheduledForDeletion->remove($this->tributosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTributo $tributo The ChildTributo object to add.
     */
    protected function doAddTributo(ChildTributo $tributo)
    {
        $this->collTributos[]= $tributo;
        $tributo->setUsuario($this);
    }

    /**
     * @param  ChildTributo $tributo The ChildTributo object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeTributo(ChildTributo $tributo)
    {
        if ($this->getTributos()->contains($tributo)) {
            $pos = $this->collTributos->search($tributo);
            $this->collTributos->remove($pos);
            if (null === $this->tributosScheduledForDeletion) {
                $this->tributosScheduledForDeletion = clone $this->collTributos;
                $this->tributosScheduledForDeletion->clear();
            }
            $this->tributosScheduledForDeletion[]= clone $tributo;
            $tributo->setUsuario(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aPessoa) {
            $this->aPessoa->removeUsuario($this);
        }
        $this->idusuario = null;
        $this->pessoa_id = null;
        $this->login = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->ativo = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBaixas) {
                foreach ($this->collBaixas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBaixaEstornos) {
                foreach ($this->collBaixaEstornos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBaixaMovimentos) {
                foreach ($this->collBaixaMovimentos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBancos) {
                foreach ($this->collBancos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBancoAgencias) {
                foreach ($this->collBancoAgencias as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBancoAgenciaContas) {
                foreach ($this->collBancoAgenciaContas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBoletos) {
                foreach ($this->collBoletos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBoletoBaixaHistoricos) {
                foreach ($this->collBoletoBaixaHistoricos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBoletoItems) {
                foreach ($this->collBoletoItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCaixas) {
                foreach ($this->collCaixas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collClientes) {
                foreach ($this->collClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collClienteEstoqueItems) {
                foreach ($this->collClienteEstoqueItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collContaCaixas) {
                foreach ($this->collContaCaixas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collContasPagars) {
                foreach ($this->collContasPagars as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collContasPagarTributoss) {
                foreach ($this->collContasPagarTributoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collConvenios) {
                foreach ($this->collConvenios as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCronTasks) {
                foreach ($this->collCronTasks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCronTaskGrupos) {
                foreach ($this->collCronTaskGrupos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEnderecoAgencias) {
                foreach ($this->collEnderecoAgencias as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEnderecoClientes) {
                foreach ($this->collEnderecoClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEnderecoServClientes) {
                foreach ($this->collEnderecoServClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEstoques) {
                foreach ($this->collEstoques as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEstoqueLancamentos) {
                foreach ($this->collEstoqueLancamentos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEstoqueLancamentoItems) {
                foreach ($this->collEstoqueLancamentoItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFormaPagamentos) {
                foreach ($this->collFormaPagamentos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFornecedors) {
                foreach ($this->collFornecedors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collItemComprados) {
                foreach ($this->collItemComprados as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collItemDeCompras) {
                foreach ($this->collItemDeCompras as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicoClientes) {
                foreach ($this->collServicoClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collServicoPrestados) {
                foreach ($this->collServicoPrestados as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSicis) {
                foreach ($this->collSicis as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysPerfils) {
                foreach ($this->collSysPerfils as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysProcessoss) {
                foreach ($this->collSysProcessoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSysUsuarioPerfils) {
                foreach ($this->collSysUsuarioPerfils as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTelefones) {
                foreach ($this->collTelefones as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTributos) {
                foreach ($this->collTributos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBaixas = null;
        $this->collBaixaEstornos = null;
        $this->collBaixaMovimentos = null;
        $this->collBancos = null;
        $this->collBancoAgencias = null;
        $this->collBancoAgenciaContas = null;
        $this->collBoletos = null;
        $this->collBoletoBaixaHistoricos = null;
        $this->collBoletoItems = null;
        $this->collCaixas = null;
        $this->collClientes = null;
        $this->collClienteEstoqueItems = null;
        $this->collContaCaixas = null;
        $this->collContasPagars = null;
        $this->collContasPagarTributoss = null;
        $this->collConvenios = null;
        $this->collCronTasks = null;
        $this->collCronTaskGrupos = null;
        $this->collEnderecoAgencias = null;
        $this->collEnderecoClientes = null;
        $this->collEnderecoServClientes = null;
        $this->collEstoques = null;
        $this->collEstoqueLancamentos = null;
        $this->collEstoqueLancamentoItems = null;
        $this->collFormaPagamentos = null;
        $this->collFornecedors = null;
        $this->collItemComprados = null;
        $this->collItemDeCompras = null;
        $this->collServicoClientes = null;
        $this->collServicoPrestados = null;
        $this->collSicis = null;
        $this->collSysPerfils = null;
        $this->collSysProcessoss = null;
        $this->collSysUsuarioPerfils = null;
        $this->collTelefones = null;
        $this->collTributos = null;
        $this->aPessoa = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UsuarioTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
