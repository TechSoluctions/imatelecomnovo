<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Baixa as ChildBaixa;
use ImaTelecomBundle\Model\BaixaEstorno as ChildBaixaEstorno;
use ImaTelecomBundle\Model\BaixaEstornoQuery as ChildBaixaEstornoQuery;
use ImaTelecomBundle\Model\BaixaQuery as ChildBaixaQuery;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoBaixaHistorico as ChildBoletoBaixaHistorico;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery as ChildBoletoBaixaHistoricoQuery;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Competencia as ChildCompetencia;
use ImaTelecomBundle\Model\CompetenciaQuery as ChildCompetenciaQuery;
use ImaTelecomBundle\Model\DadosFiscal as ChildDadosFiscal;
use ImaTelecomBundle\Model\DadosFiscalQuery as ChildDadosFiscalQuery;
use ImaTelecomBundle\Model\Faturamento as ChildFaturamento;
use ImaTelecomBundle\Model\FaturamentoQuery as ChildFaturamentoQuery;
use ImaTelecomBundle\Model\Lancamentos as ChildLancamentos;
use ImaTelecomBundle\Model\LancamentosBoletos as ChildLancamentosBoletos;
use ImaTelecomBundle\Model\LancamentosBoletosQuery as ChildLancamentosBoletosQuery;
use ImaTelecomBundle\Model\LancamentosQuery as ChildLancamentosQuery;
use ImaTelecomBundle\Model\Sici as ChildSici;
use ImaTelecomBundle\Model\SiciQuery as ChildSiciQuery;
use ImaTelecomBundle\Model\Sintegra as ChildSintegra;
use ImaTelecomBundle\Model\SintegraQuery as ChildSintegraQuery;
use ImaTelecomBundle\Model\Map\BaixaEstornoTableMap;
use ImaTelecomBundle\Model\Map\BaixaTableMap;
use ImaTelecomBundle\Model\Map\BoletoBaixaHistoricoTableMap;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use ImaTelecomBundle\Model\Map\CompetenciaTableMap;
use ImaTelecomBundle\Model\Map\DadosFiscalTableMap;
use ImaTelecomBundle\Model\Map\FaturamentoTableMap;
use ImaTelecomBundle\Model\Map\LancamentosBoletosTableMap;
use ImaTelecomBundle\Model\Map\LancamentosTableMap;
use ImaTelecomBundle\Model\Map\SiciTableMap;
use ImaTelecomBundle\Model\Map\SintegraTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'competencia' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Competencia implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\CompetenciaTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the ano field.
     *
     * @var        string
     */
    protected $ano;

    /**
     * The value for the mes field.
     *
     * @var        string
     */
    protected $mes;

    /**
     * The value for the numero_nf field.
     *
     * @var        int
     */
    protected $numero_nf;

    /**
     * The value for the periodo_inicial field.
     *
     * @var        DateTime
     */
    protected $periodo_inicial;

    /**
     * The value for the periodo_final field.
     *
     * @var        DateTime
     */
    protected $periodo_final;

    /**
     * The value for the ativo field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $ativo;

    /**
     * The value for the descricao field.
     *
     * @var        string
     */
    protected $descricao;

    /**
     * The value for the periodo_futuro field.
     *
     * @var        int
     */
    protected $periodo_futuro;

    /**
     * The value for the proxima_competencia field.
     *
     * @var        int
     */
    protected $proxima_competencia;

    /**
     * The value for the encerramento field.
     *
     * @var        int
     */
    protected $encerramento;

    /**
     * The value for the trabalhar_periodo_futuro field.
     *
     * @var        int
     */
    protected $trabalhar_periodo_futuro;

    /**
     * The value for the ultima_data_emissao field.
     *
     * @var        string
     */
    protected $ultima_data_emissao;

    /**
     * @var        ObjectCollection|ChildBaixa[] Collection to store aggregation of ChildBaixa objects.
     */
    protected $collBaixas;
    protected $collBaixasPartial;

    /**
     * @var        ObjectCollection|ChildBaixaEstorno[] Collection to store aggregation of ChildBaixaEstorno objects.
     */
    protected $collBaixaEstornos;
    protected $collBaixaEstornosPartial;

    /**
     * @var        ObjectCollection|ChildBoleto[] Collection to store aggregation of ChildBoleto objects.
     */
    protected $collBoletos;
    protected $collBoletosPartial;

    /**
     * @var        ObjectCollection|ChildBoletoBaixaHistorico[] Collection to store aggregation of ChildBoletoBaixaHistorico objects.
     */
    protected $collBoletoBaixaHistoricos;
    protected $collBoletoBaixaHistoricosPartial;

    /**
     * @var        ObjectCollection|ChildDadosFiscal[] Collection to store aggregation of ChildDadosFiscal objects.
     */
    protected $collDadosFiscals;
    protected $collDadosFiscalsPartial;

    /**
     * @var        ObjectCollection|ChildFaturamento[] Collection to store aggregation of ChildFaturamento objects.
     */
    protected $collFaturamentos;
    protected $collFaturamentosPartial;

    /**
     * @var        ObjectCollection|ChildLancamentos[] Collection to store aggregation of ChildLancamentos objects.
     */
    protected $collLancamentoss;
    protected $collLancamentossPartial;

    /**
     * @var        ObjectCollection|ChildLancamentosBoletos[] Collection to store aggregation of ChildLancamentosBoletos objects.
     */
    protected $collLancamentosBoletoss;
    protected $collLancamentosBoletossPartial;

    /**
     * @var        ObjectCollection|ChildSici[] Collection to store aggregation of ChildSici objects.
     */
    protected $collSicis;
    protected $collSicisPartial;

    /**
     * @var        ObjectCollection|ChildSintegra[] Collection to store aggregation of ChildSintegra objects.
     */
    protected $collSintegras;
    protected $collSintegrasPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBaixa[]
     */
    protected $baixasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBaixaEstorno[]
     */
    protected $baixaEstornosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoleto[]
     */
    protected $boletosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoletoBaixaHistorico[]
     */
    protected $boletoBaixaHistoricosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDadosFiscal[]
     */
    protected $dadosFiscalsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFaturamento[]
     */
    protected $faturamentosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLancamentos[]
     */
    protected $lancamentossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLancamentosBoletos[]
     */
    protected $lancamentosBoletossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSici[]
     */
    protected $sicisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSintegra[]
     */
    protected $sintegrasScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->ativo = 0;
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Competencia object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Competencia</code> instance.  If
     * <code>obj</code> is an instance of <code>Competencia</code>, delegates to
     * <code>equals(Competencia)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Competencia The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [ano] column value.
     *
     * @return string
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * Get the [mes] column value.
     *
     * @return string
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Get the [numero_nf] column value.
     *
     * @return int
     */
    public function getNumeroNf()
    {
        return $this->numero_nf;
    }

    /**
     * Get the [optionally formatted] temporal [periodo_inicial] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPeriodoInicial($format = NULL)
    {
        if ($format === null) {
            return $this->periodo_inicial;
        } else {
            return $this->periodo_inicial instanceof \DateTimeInterface ? $this->periodo_inicial->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [periodo_final] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPeriodoFinal($format = NULL)
    {
        if ($format === null) {
            return $this->periodo_final;
        } else {
            return $this->periodo_final instanceof \DateTimeInterface ? $this->periodo_final->format($format) : null;
        }
    }

    /**
     * Get the [ativo] column value.
     *
     * @return int
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get the [descricao] column value.
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Get the [periodo_futuro] column value.
     *
     * @return int
     */
    public function getPeriodoFuturo()
    {
        return $this->periodo_futuro;
    }

    /**
     * Get the [proxima_competencia] column value.
     *
     * @return int
     */
    public function getProximaCompetencia()
    {
        return $this->proxima_competencia;
    }

    /**
     * Get the [encerramento] column value.
     *
     * @return int
     */
    public function getEncerramento()
    {
        return $this->encerramento;
    }

    /**
     * Get the [trabalhar_periodo_futuro] column value.
     *
     * @return int
     */
    public function getTrabalharPeriodoFuturo()
    {
        return $this->trabalhar_periodo_futuro;
    }

    /**
     * Get the [ultima_data_emissao] column value.
     *
     * @return string
     */
    public function getUltimaDataEmissao()
    {
        return $this->ultima_data_emissao;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [ano] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setAno($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ano !== $v) {
            $this->ano = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_ANO] = true;
        }

        return $this;
    } // setAno()

    /**
     * Set the value of [mes] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setMes($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mes !== $v) {
            $this->mes = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_MES] = true;
        }

        return $this;
    } // setMes()

    /**
     * Set the value of [numero_nf] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setNumeroNf($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->numero_nf !== $v) {
            $this->numero_nf = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_NUMERO_NF] = true;
        }

        return $this;
    } // setNumeroNf()

    /**
     * Sets the value of [periodo_inicial] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setPeriodoInicial($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->periodo_inicial !== null || $dt !== null) {
            if ($this->periodo_inicial === null || $dt === null || $dt->format("Y-m-d") !== $this->periodo_inicial->format("Y-m-d")) {
                $this->periodo_inicial = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CompetenciaTableMap::COL_PERIODO_INICIAL] = true;
            }
        } // if either are not null

        return $this;
    } // setPeriodoInicial()

    /**
     * Sets the value of [periodo_final] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setPeriodoFinal($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->periodo_final !== null || $dt !== null) {
            if ($this->periodo_final === null || $dt === null || $dt->format("Y-m-d") !== $this->periodo_final->format("Y-m-d")) {
                $this->periodo_final = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CompetenciaTableMap::COL_PERIODO_FINAL] = true;
            }
        } // if either are not null

        return $this;
    } // setPeriodoFinal()

    /**
     * Set the value of [ativo] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setAtivo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ativo !== $v) {
            $this->ativo = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_ATIVO] = true;
        }

        return $this;
    } // setAtivo()

    /**
     * Set the value of [descricao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setDescricao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao !== $v) {
            $this->descricao = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_DESCRICAO] = true;
        }

        return $this;
    } // setDescricao()

    /**
     * Set the value of [periodo_futuro] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setPeriodoFuturo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->periodo_futuro !== $v) {
            $this->periodo_futuro = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_PERIODO_FUTURO] = true;
        }

        return $this;
    } // setPeriodoFuturo()

    /**
     * Set the value of [proxima_competencia] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setProximaCompetencia($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->proxima_competencia !== $v) {
            $this->proxima_competencia = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_PROXIMA_COMPETENCIA] = true;
        }

        return $this;
    } // setProximaCompetencia()

    /**
     * Set the value of [encerramento] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setEncerramento($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->encerramento !== $v) {
            $this->encerramento = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_ENCERRAMENTO] = true;
        }

        return $this;
    } // setEncerramento()

    /**
     * Set the value of [trabalhar_periodo_futuro] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setTrabalharPeriodoFuturo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->trabalhar_periodo_futuro !== $v) {
            $this->trabalhar_periodo_futuro = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO] = true;
        }

        return $this;
    } // setTrabalharPeriodoFuturo()

    /**
     * Set the value of [ultima_data_emissao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function setUltimaDataEmissao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ultima_data_emissao !== $v) {
            $this->ultima_data_emissao = $v;
            $this->modifiedColumns[CompetenciaTableMap::COL_ULTIMA_DATA_EMISSAO] = true;
        }

        return $this;
    } // setUltimaDataEmissao()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->ativo !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CompetenciaTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CompetenciaTableMap::translateFieldName('Ano', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ano = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CompetenciaTableMap::translateFieldName('Mes', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mes = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CompetenciaTableMap::translateFieldName('NumeroNf', TableMap::TYPE_PHPNAME, $indexType)];
            $this->numero_nf = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CompetenciaTableMap::translateFieldName('PeriodoInicial', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->periodo_inicial = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CompetenciaTableMap::translateFieldName('PeriodoFinal', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->periodo_final = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CompetenciaTableMap::translateFieldName('Ativo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ativo = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CompetenciaTableMap::translateFieldName('Descricao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CompetenciaTableMap::translateFieldName('PeriodoFuturo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->periodo_futuro = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CompetenciaTableMap::translateFieldName('ProximaCompetencia', TableMap::TYPE_PHPNAME, $indexType)];
            $this->proxima_competencia = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CompetenciaTableMap::translateFieldName('Encerramento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->encerramento = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CompetenciaTableMap::translateFieldName('TrabalharPeriodoFuturo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->trabalhar_periodo_futuro = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : CompetenciaTableMap::translateFieldName('UltimaDataEmissao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ultima_data_emissao = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 13; // 13 = CompetenciaTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Competencia'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompetenciaTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCompetenciaQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collBaixas = null;

            $this->collBaixaEstornos = null;

            $this->collBoletos = null;

            $this->collBoletoBaixaHistoricos = null;

            $this->collDadosFiscals = null;

            $this->collFaturamentos = null;

            $this->collLancamentoss = null;

            $this->collLancamentosBoletoss = null;

            $this->collSicis = null;

            $this->collSintegras = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Competencia::setDeleted()
     * @see Competencia::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompetenciaTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCompetenciaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompetenciaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CompetenciaTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->baixasScheduledForDeletion !== null) {
                if (!$this->baixasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BaixaQuery::create()
                        ->filterByPrimaryKeys($this->baixasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->baixasScheduledForDeletion = null;
                }
            }

            if ($this->collBaixas !== null) {
                foreach ($this->collBaixas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->baixaEstornosScheduledForDeletion !== null) {
                if (!$this->baixaEstornosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BaixaEstornoQuery::create()
                        ->filterByPrimaryKeys($this->baixaEstornosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->baixaEstornosScheduledForDeletion = null;
                }
            }

            if ($this->collBaixaEstornos !== null) {
                foreach ($this->collBaixaEstornos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->boletosScheduledForDeletion !== null) {
                if (!$this->boletosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BoletoQuery::create()
                        ->filterByPrimaryKeys($this->boletosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->boletosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletos !== null) {
                foreach ($this->collBoletos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->boletoBaixaHistoricosScheduledForDeletion !== null) {
                if (!$this->boletoBaixaHistoricosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery::create()
                        ->filterByPrimaryKeys($this->boletoBaixaHistoricosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->boletoBaixaHistoricosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletoBaixaHistoricos !== null) {
                foreach ($this->collBoletoBaixaHistoricos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dadosFiscalsScheduledForDeletion !== null) {
                if (!$this->dadosFiscalsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\DadosFiscalQuery::create()
                        ->filterByPrimaryKeys($this->dadosFiscalsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dadosFiscalsScheduledForDeletion = null;
                }
            }

            if ($this->collDadosFiscals !== null) {
                foreach ($this->collDadosFiscals as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->faturamentosScheduledForDeletion !== null) {
                if (!$this->faturamentosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\FaturamentoQuery::create()
                        ->filterByPrimaryKeys($this->faturamentosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->faturamentosScheduledForDeletion = null;
                }
            }

            if ($this->collFaturamentos !== null) {
                foreach ($this->collFaturamentos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lancamentossScheduledForDeletion !== null) {
                if (!$this->lancamentossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\LancamentosQuery::create()
                        ->filterByPrimaryKeys($this->lancamentossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lancamentossScheduledForDeletion = null;
                }
            }

            if ($this->collLancamentoss !== null) {
                foreach ($this->collLancamentoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lancamentosBoletossScheduledForDeletion !== null) {
                if (!$this->lancamentosBoletossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\LancamentosBoletosQuery::create()
                        ->filterByPrimaryKeys($this->lancamentosBoletossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lancamentosBoletossScheduledForDeletion = null;
                }
            }

            if ($this->collLancamentosBoletoss !== null) {
                foreach ($this->collLancamentosBoletoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sicisScheduledForDeletion !== null) {
                if (!$this->sicisScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\SiciQuery::create()
                        ->filterByPrimaryKeys($this->sicisScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sicisScheduledForDeletion = null;
                }
            }

            if ($this->collSicis !== null) {
                foreach ($this->collSicis as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sintegrasScheduledForDeletion !== null) {
                if (!$this->sintegrasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\SintegraQuery::create()
                        ->filterByPrimaryKeys($this->sintegrasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sintegrasScheduledForDeletion = null;
                }
            }

            if ($this->collSintegras !== null) {
                foreach ($this->collSintegras as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CompetenciaTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CompetenciaTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CompetenciaTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_ANO)) {
            $modifiedColumns[':p' . $index++]  = 'ano';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_MES)) {
            $modifiedColumns[':p' . $index++]  = 'mes';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_NUMERO_NF)) {
            $modifiedColumns[':p' . $index++]  = 'numero_nf';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_PERIODO_INICIAL)) {
            $modifiedColumns[':p' . $index++]  = 'periodo_inicial';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_PERIODO_FINAL)) {
            $modifiedColumns[':p' . $index++]  = 'periodo_final';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_ATIVO)) {
            $modifiedColumns[':p' . $index++]  = 'ativo';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_DESCRICAO)) {
            $modifiedColumns[':p' . $index++]  = 'descricao';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_PERIODO_FUTURO)) {
            $modifiedColumns[':p' . $index++]  = 'periodo_futuro';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_PROXIMA_COMPETENCIA)) {
            $modifiedColumns[':p' . $index++]  = 'proxima_competencia';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_ENCERRAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'encerramento';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO)) {
            $modifiedColumns[':p' . $index++]  = 'trabalhar_periodo_futuro';
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_ULTIMA_DATA_EMISSAO)) {
            $modifiedColumns[':p' . $index++]  = 'ultima_data_emissao';
        }

        $sql = sprintf(
            'INSERT INTO competencia (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'ano':
                        $stmt->bindValue($identifier, $this->ano, PDO::PARAM_STR);
                        break;
                    case 'mes':
                        $stmt->bindValue($identifier, $this->mes, PDO::PARAM_STR);
                        break;
                    case 'numero_nf':
                        $stmt->bindValue($identifier, $this->numero_nf, PDO::PARAM_INT);
                        break;
                    case 'periodo_inicial':
                        $stmt->bindValue($identifier, $this->periodo_inicial ? $this->periodo_inicial->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'periodo_final':
                        $stmt->bindValue($identifier, $this->periodo_final ? $this->periodo_final->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'ativo':
                        $stmt->bindValue($identifier, $this->ativo, PDO::PARAM_INT);
                        break;
                    case 'descricao':
                        $stmt->bindValue($identifier, $this->descricao, PDO::PARAM_STR);
                        break;
                    case 'periodo_futuro':
                        $stmt->bindValue($identifier, $this->periodo_futuro, PDO::PARAM_INT);
                        break;
                    case 'proxima_competencia':
                        $stmt->bindValue($identifier, $this->proxima_competencia, PDO::PARAM_INT);
                        break;
                    case 'encerramento':
                        $stmt->bindValue($identifier, $this->encerramento, PDO::PARAM_INT);
                        break;
                    case 'trabalhar_periodo_futuro':
                        $stmt->bindValue($identifier, $this->trabalhar_periodo_futuro, PDO::PARAM_INT);
                        break;
                    case 'ultima_data_emissao':
                        $stmt->bindValue($identifier, $this->ultima_data_emissao, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CompetenciaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getAno();
                break;
            case 2:
                return $this->getMes();
                break;
            case 3:
                return $this->getNumeroNf();
                break;
            case 4:
                return $this->getPeriodoInicial();
                break;
            case 5:
                return $this->getPeriodoFinal();
                break;
            case 6:
                return $this->getAtivo();
                break;
            case 7:
                return $this->getDescricao();
                break;
            case 8:
                return $this->getPeriodoFuturo();
                break;
            case 9:
                return $this->getProximaCompetencia();
                break;
            case 10:
                return $this->getEncerramento();
                break;
            case 11:
                return $this->getTrabalharPeriodoFuturo();
                break;
            case 12:
                return $this->getUltimaDataEmissao();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Competencia'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Competencia'][$this->hashCode()] = true;
        $keys = CompetenciaTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getAno(),
            $keys[2] => $this->getMes(),
            $keys[3] => $this->getNumeroNf(),
            $keys[4] => $this->getPeriodoInicial(),
            $keys[5] => $this->getPeriodoFinal(),
            $keys[6] => $this->getAtivo(),
            $keys[7] => $this->getDescricao(),
            $keys[8] => $this->getPeriodoFuturo(),
            $keys[9] => $this->getProximaCompetencia(),
            $keys[10] => $this->getEncerramento(),
            $keys[11] => $this->getTrabalharPeriodoFuturo(),
            $keys[12] => $this->getUltimaDataEmissao(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collBaixas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixas';
                        break;
                    default:
                        $key = 'Baixas';
                }

                $result[$key] = $this->collBaixas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBaixaEstornos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixaEstornos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixa_estornos';
                        break;
                    default:
                        $key = 'BaixaEstornos';
                }

                $result[$key] = $this->collBaixaEstornos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBoletos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boletos';
                        break;
                    default:
                        $key = 'Boletos';
                }

                $result[$key] = $this->collBoletos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBoletoBaixaHistoricos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoBaixaHistoricos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_baixa_historicos';
                        break;
                    default:
                        $key = 'BoletoBaixaHistoricos';
                }

                $result[$key] = $this->collBoletoBaixaHistoricos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDadosFiscals) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dadosFiscals';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'dados_fiscals';
                        break;
                    default:
                        $key = 'DadosFiscals';
                }

                $result[$key] = $this->collDadosFiscals->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFaturamentos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'faturamentos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'faturamentos';
                        break;
                    default:
                        $key = 'Faturamentos';
                }

                $result[$key] = $this->collFaturamentos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLancamentoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lancamentoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lancamentoss';
                        break;
                    default:
                        $key = 'Lancamentoss';
                }

                $result[$key] = $this->collLancamentoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLancamentosBoletoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lancamentosBoletoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lancamentos_boletoss';
                        break;
                    default:
                        $key = 'LancamentosBoletoss';
                }

                $result[$key] = $this->collLancamentosBoletoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSicis) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sicis';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sicis';
                        break;
                    default:
                        $key = 'Sicis';
                }

                $result[$key] = $this->collSicis->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSintegras) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sintegras';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sintegras';
                        break;
                    default:
                        $key = 'Sintegras';
                }

                $result[$key] = $this->collSintegras->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Competencia
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CompetenciaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Competencia
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setAno($value);
                break;
            case 2:
                $this->setMes($value);
                break;
            case 3:
                $this->setNumeroNf($value);
                break;
            case 4:
                $this->setPeriodoInicial($value);
                break;
            case 5:
                $this->setPeriodoFinal($value);
                break;
            case 6:
                $this->setAtivo($value);
                break;
            case 7:
                $this->setDescricao($value);
                break;
            case 8:
                $this->setPeriodoFuturo($value);
                break;
            case 9:
                $this->setProximaCompetencia($value);
                break;
            case 10:
                $this->setEncerramento($value);
                break;
            case 11:
                $this->setTrabalharPeriodoFuturo($value);
                break;
            case 12:
                $this->setUltimaDataEmissao($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CompetenciaTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setAno($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setMes($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNumeroNf($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPeriodoInicial($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPeriodoFinal($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setAtivo($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDescricao($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPeriodoFuturo($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setProximaCompetencia($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setEncerramento($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setTrabalharPeriodoFuturo($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUltimaDataEmissao($arr[$keys[12]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CompetenciaTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CompetenciaTableMap::COL_ID)) {
            $criteria->add(CompetenciaTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_ANO)) {
            $criteria->add(CompetenciaTableMap::COL_ANO, $this->ano);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_MES)) {
            $criteria->add(CompetenciaTableMap::COL_MES, $this->mes);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_NUMERO_NF)) {
            $criteria->add(CompetenciaTableMap::COL_NUMERO_NF, $this->numero_nf);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_PERIODO_INICIAL)) {
            $criteria->add(CompetenciaTableMap::COL_PERIODO_INICIAL, $this->periodo_inicial);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_PERIODO_FINAL)) {
            $criteria->add(CompetenciaTableMap::COL_PERIODO_FINAL, $this->periodo_final);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_ATIVO)) {
            $criteria->add(CompetenciaTableMap::COL_ATIVO, $this->ativo);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_DESCRICAO)) {
            $criteria->add(CompetenciaTableMap::COL_DESCRICAO, $this->descricao);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_PERIODO_FUTURO)) {
            $criteria->add(CompetenciaTableMap::COL_PERIODO_FUTURO, $this->periodo_futuro);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_PROXIMA_COMPETENCIA)) {
            $criteria->add(CompetenciaTableMap::COL_PROXIMA_COMPETENCIA, $this->proxima_competencia);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_ENCERRAMENTO)) {
            $criteria->add(CompetenciaTableMap::COL_ENCERRAMENTO, $this->encerramento);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO)) {
            $criteria->add(CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO, $this->trabalhar_periodo_futuro);
        }
        if ($this->isColumnModified(CompetenciaTableMap::COL_ULTIMA_DATA_EMISSAO)) {
            $criteria->add(CompetenciaTableMap::COL_ULTIMA_DATA_EMISSAO, $this->ultima_data_emissao);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCompetenciaQuery::create();
        $criteria->add(CompetenciaTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Competencia (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setAno($this->getAno());
        $copyObj->setMes($this->getMes());
        $copyObj->setNumeroNf($this->getNumeroNf());
        $copyObj->setPeriodoInicial($this->getPeriodoInicial());
        $copyObj->setPeriodoFinal($this->getPeriodoFinal());
        $copyObj->setAtivo($this->getAtivo());
        $copyObj->setDescricao($this->getDescricao());
        $copyObj->setPeriodoFuturo($this->getPeriodoFuturo());
        $copyObj->setProximaCompetencia($this->getProximaCompetencia());
        $copyObj->setEncerramento($this->getEncerramento());
        $copyObj->setTrabalharPeriodoFuturo($this->getTrabalharPeriodoFuturo());
        $copyObj->setUltimaDataEmissao($this->getUltimaDataEmissao());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBaixas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBaixa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBaixaEstornos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBaixaEstorno($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBoletos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoleto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBoletoBaixaHistoricos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoletoBaixaHistorico($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDadosFiscals() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDadosFiscal($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFaturamentos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFaturamento($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLancamentoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLancamentos($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLancamentosBoletoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLancamentosBoletos($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSicis() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSici($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSintegras() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSintegra($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Competencia Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Baixa' == $relationName) {
            return $this->initBaixas();
        }
        if ('BaixaEstorno' == $relationName) {
            return $this->initBaixaEstornos();
        }
        if ('Boleto' == $relationName) {
            return $this->initBoletos();
        }
        if ('BoletoBaixaHistorico' == $relationName) {
            return $this->initBoletoBaixaHistoricos();
        }
        if ('DadosFiscal' == $relationName) {
            return $this->initDadosFiscals();
        }
        if ('Faturamento' == $relationName) {
            return $this->initFaturamentos();
        }
        if ('Lancamentos' == $relationName) {
            return $this->initLancamentoss();
        }
        if ('LancamentosBoletos' == $relationName) {
            return $this->initLancamentosBoletoss();
        }
        if ('Sici' == $relationName) {
            return $this->initSicis();
        }
        if ('Sintegra' == $relationName) {
            return $this->initSintegras();
        }
    }

    /**
     * Clears out the collBaixas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBaixas()
     */
    public function clearBaixas()
    {
        $this->collBaixas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBaixas collection loaded partially.
     */
    public function resetPartialBaixas($v = true)
    {
        $this->collBaixasPartial = $v;
    }

    /**
     * Initializes the collBaixas collection.
     *
     * By default this just sets the collBaixas collection to an empty array (like clearcollBaixas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBaixas($overrideExisting = true)
    {
        if (null !== $this->collBaixas && !$overrideExisting) {
            return;
        }

        $collectionClassName = BaixaTableMap::getTableMap()->getCollectionClassName();

        $this->collBaixas = new $collectionClassName;
        $this->collBaixas->setModel('\ImaTelecomBundle\Model\Baixa');
    }

    /**
     * Gets an array of ChildBaixa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBaixa[] List of ChildBaixa objects
     * @throws PropelException
     */
    public function getBaixas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixasPartial && !$this->isNew();
        if (null === $this->collBaixas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBaixas) {
                // return empty collection
                $this->initBaixas();
            } else {
                $collBaixas = ChildBaixaQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBaixasPartial && count($collBaixas)) {
                        $this->initBaixas(false);

                        foreach ($collBaixas as $obj) {
                            if (false == $this->collBaixas->contains($obj)) {
                                $this->collBaixas->append($obj);
                            }
                        }

                        $this->collBaixasPartial = true;
                    }

                    return $collBaixas;
                }

                if ($partial && $this->collBaixas) {
                    foreach ($this->collBaixas as $obj) {
                        if ($obj->isNew()) {
                            $collBaixas[] = $obj;
                        }
                    }
                }

                $this->collBaixas = $collBaixas;
                $this->collBaixasPartial = false;
            }
        }

        return $this->collBaixas;
    }

    /**
     * Sets a collection of ChildBaixa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $baixas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setBaixas(Collection $baixas, ConnectionInterface $con = null)
    {
        /** @var ChildBaixa[] $baixasToDelete */
        $baixasToDelete = $this->getBaixas(new Criteria(), $con)->diff($baixas);


        $this->baixasScheduledForDeletion = $baixasToDelete;

        foreach ($baixasToDelete as $baixaRemoved) {
            $baixaRemoved->setCompetencia(null);
        }

        $this->collBaixas = null;
        foreach ($baixas as $baixa) {
            $this->addBaixa($baixa);
        }

        $this->collBaixas = $baixas;
        $this->collBaixasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Baixa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Baixa objects.
     * @throws PropelException
     */
    public function countBaixas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixasPartial && !$this->isNew();
        if (null === $this->collBaixas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBaixas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBaixas());
            }

            $query = ChildBaixaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collBaixas);
    }

    /**
     * Method called to associate a ChildBaixa object to this object
     * through the ChildBaixa foreign key attribute.
     *
     * @param  ChildBaixa $l ChildBaixa
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addBaixa(ChildBaixa $l)
    {
        if ($this->collBaixas === null) {
            $this->initBaixas();
            $this->collBaixasPartial = true;
        }

        if (!$this->collBaixas->contains($l)) {
            $this->doAddBaixa($l);

            if ($this->baixasScheduledForDeletion and $this->baixasScheduledForDeletion->contains($l)) {
                $this->baixasScheduledForDeletion->remove($this->baixasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBaixa $baixa The ChildBaixa object to add.
     */
    protected function doAddBaixa(ChildBaixa $baixa)
    {
        $this->collBaixas[]= $baixa;
        $baixa->setCompetencia($this);
    }

    /**
     * @param  ChildBaixa $baixa The ChildBaixa object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeBaixa(ChildBaixa $baixa)
    {
        if ($this->getBaixas()->contains($baixa)) {
            $pos = $this->collBaixas->search($baixa);
            $this->collBaixas->remove($pos);
            if (null === $this->baixasScheduledForDeletion) {
                $this->baixasScheduledForDeletion = clone $this->collBaixas;
                $this->baixasScheduledForDeletion->clear();
            }
            $this->baixasScheduledForDeletion[]= clone $baixa;
            $baixa->setCompetencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related Baixas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixa[] List of ChildBaixa objects
     */
    public function getBaixasJoinContaCaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaQuery::create(null, $criteria);
        $query->joinWith('ContaCaixa', $joinBehavior);

        return $this->getBaixas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related Baixas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixa[] List of ChildBaixa objects
     */
    public function getBaixasJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBaixas($query, $con);
    }

    /**
     * Clears out the collBaixaEstornos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBaixaEstornos()
     */
    public function clearBaixaEstornos()
    {
        $this->collBaixaEstornos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBaixaEstornos collection loaded partially.
     */
    public function resetPartialBaixaEstornos($v = true)
    {
        $this->collBaixaEstornosPartial = $v;
    }

    /**
     * Initializes the collBaixaEstornos collection.
     *
     * By default this just sets the collBaixaEstornos collection to an empty array (like clearcollBaixaEstornos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBaixaEstornos($overrideExisting = true)
    {
        if (null !== $this->collBaixaEstornos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BaixaEstornoTableMap::getTableMap()->getCollectionClassName();

        $this->collBaixaEstornos = new $collectionClassName;
        $this->collBaixaEstornos->setModel('\ImaTelecomBundle\Model\BaixaEstorno');
    }

    /**
     * Gets an array of ChildBaixaEstorno objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBaixaEstorno[] List of ChildBaixaEstorno objects
     * @throws PropelException
     */
    public function getBaixaEstornos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixaEstornosPartial && !$this->isNew();
        if (null === $this->collBaixaEstornos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBaixaEstornos) {
                // return empty collection
                $this->initBaixaEstornos();
            } else {
                $collBaixaEstornos = ChildBaixaEstornoQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBaixaEstornosPartial && count($collBaixaEstornos)) {
                        $this->initBaixaEstornos(false);

                        foreach ($collBaixaEstornos as $obj) {
                            if (false == $this->collBaixaEstornos->contains($obj)) {
                                $this->collBaixaEstornos->append($obj);
                            }
                        }

                        $this->collBaixaEstornosPartial = true;
                    }

                    return $collBaixaEstornos;
                }

                if ($partial && $this->collBaixaEstornos) {
                    foreach ($this->collBaixaEstornos as $obj) {
                        if ($obj->isNew()) {
                            $collBaixaEstornos[] = $obj;
                        }
                    }
                }

                $this->collBaixaEstornos = $collBaixaEstornos;
                $this->collBaixaEstornosPartial = false;
            }
        }

        return $this->collBaixaEstornos;
    }

    /**
     * Sets a collection of ChildBaixaEstorno objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $baixaEstornos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setBaixaEstornos(Collection $baixaEstornos, ConnectionInterface $con = null)
    {
        /** @var ChildBaixaEstorno[] $baixaEstornosToDelete */
        $baixaEstornosToDelete = $this->getBaixaEstornos(new Criteria(), $con)->diff($baixaEstornos);


        $this->baixaEstornosScheduledForDeletion = $baixaEstornosToDelete;

        foreach ($baixaEstornosToDelete as $baixaEstornoRemoved) {
            $baixaEstornoRemoved->setCompetencia(null);
        }

        $this->collBaixaEstornos = null;
        foreach ($baixaEstornos as $baixaEstorno) {
            $this->addBaixaEstorno($baixaEstorno);
        }

        $this->collBaixaEstornos = $baixaEstornos;
        $this->collBaixaEstornosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaixaEstorno objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaixaEstorno objects.
     * @throws PropelException
     */
    public function countBaixaEstornos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBaixaEstornosPartial && !$this->isNew();
        if (null === $this->collBaixaEstornos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBaixaEstornos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBaixaEstornos());
            }

            $query = ChildBaixaEstornoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collBaixaEstornos);
    }

    /**
     * Method called to associate a ChildBaixaEstorno object to this object
     * through the ChildBaixaEstorno foreign key attribute.
     *
     * @param  ChildBaixaEstorno $l ChildBaixaEstorno
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addBaixaEstorno(ChildBaixaEstorno $l)
    {
        if ($this->collBaixaEstornos === null) {
            $this->initBaixaEstornos();
            $this->collBaixaEstornosPartial = true;
        }

        if (!$this->collBaixaEstornos->contains($l)) {
            $this->doAddBaixaEstorno($l);

            if ($this->baixaEstornosScheduledForDeletion and $this->baixaEstornosScheduledForDeletion->contains($l)) {
                $this->baixaEstornosScheduledForDeletion->remove($this->baixaEstornosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBaixaEstorno $baixaEstorno The ChildBaixaEstorno object to add.
     */
    protected function doAddBaixaEstorno(ChildBaixaEstorno $baixaEstorno)
    {
        $this->collBaixaEstornos[]= $baixaEstorno;
        $baixaEstorno->setCompetencia($this);
    }

    /**
     * @param  ChildBaixaEstorno $baixaEstorno The ChildBaixaEstorno object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeBaixaEstorno(ChildBaixaEstorno $baixaEstorno)
    {
        if ($this->getBaixaEstornos()->contains($baixaEstorno)) {
            $pos = $this->collBaixaEstornos->search($baixaEstorno);
            $this->collBaixaEstornos->remove($pos);
            if (null === $this->baixaEstornosScheduledForDeletion) {
                $this->baixaEstornosScheduledForDeletion = clone $this->collBaixaEstornos;
                $this->baixaEstornosScheduledForDeletion->clear();
            }
            $this->baixaEstornosScheduledForDeletion[]= clone $baixaEstorno;
            $baixaEstorno->setCompetencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related BaixaEstornos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixaEstorno[] List of ChildBaixaEstorno objects
     */
    public function getBaixaEstornosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaEstornoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBaixaEstornos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related BaixaEstornos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixaEstorno[] List of ChildBaixaEstorno objects
     */
    public function getBaixaEstornosJoinContaCaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaEstornoQuery::create(null, $criteria);
        $query->joinWith('ContaCaixa', $joinBehavior);

        return $this->getBaixaEstornos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related BaixaEstornos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBaixaEstorno[] List of ChildBaixaEstorno objects
     */
    public function getBaixaEstornosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBaixaEstornoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBaixaEstornos($query, $con);
    }

    /**
     * Clears out the collBoletos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletos()
     */
    public function clearBoletos()
    {
        $this->collBoletos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletos collection loaded partially.
     */
    public function resetPartialBoletos($v = true)
    {
        $this->collBoletosPartial = $v;
    }

    /**
     * Initializes the collBoletos collection.
     *
     * By default this just sets the collBoletos collection to an empty array (like clearcollBoletos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletos($overrideExisting = true)
    {
        if (null !== $this->collBoletos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletos = new $collectionClassName;
        $this->collBoletos->setModel('\ImaTelecomBundle\Model\Boleto');
    }

    /**
     * Gets an array of ChildBoleto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     * @throws PropelException
     */
    public function getBoletos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                // return empty collection
                $this->initBoletos();
            } else {
                $collBoletos = ChildBoletoQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletosPartial && count($collBoletos)) {
                        $this->initBoletos(false);

                        foreach ($collBoletos as $obj) {
                            if (false == $this->collBoletos->contains($obj)) {
                                $this->collBoletos->append($obj);
                            }
                        }

                        $this->collBoletosPartial = true;
                    }

                    return $collBoletos;
                }

                if ($partial && $this->collBoletos) {
                    foreach ($this->collBoletos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletos[] = $obj;
                        }
                    }
                }

                $this->collBoletos = $collBoletos;
                $this->collBoletosPartial = false;
            }
        }

        return $this->collBoletos;
    }

    /**
     * Sets a collection of ChildBoleto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setBoletos(Collection $boletos, ConnectionInterface $con = null)
    {
        /** @var ChildBoleto[] $boletosToDelete */
        $boletosToDelete = $this->getBoletos(new Criteria(), $con)->diff($boletos);


        $this->boletosScheduledForDeletion = $boletosToDelete;

        foreach ($boletosToDelete as $boletoRemoved) {
            $boletoRemoved->setCompetencia(null);
        }

        $this->collBoletos = null;
        foreach ($boletos as $boleto) {
            $this->addBoleto($boleto);
        }

        $this->collBoletos = $boletos;
        $this->collBoletosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Boleto objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Boleto objects.
     * @throws PropelException
     */
    public function countBoletos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletosPartial && !$this->isNew();
        if (null === $this->collBoletos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletos());
            }

            $query = ChildBoletoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collBoletos);
    }

    /**
     * Method called to associate a ChildBoleto object to this object
     * through the ChildBoleto foreign key attribute.
     *
     * @param  ChildBoleto $l ChildBoleto
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addBoleto(ChildBoleto $l)
    {
        if ($this->collBoletos === null) {
            $this->initBoletos();
            $this->collBoletosPartial = true;
        }

        if (!$this->collBoletos->contains($l)) {
            $this->doAddBoleto($l);

            if ($this->boletosScheduledForDeletion and $this->boletosScheduledForDeletion->contains($l)) {
                $this->boletosScheduledForDeletion->remove($this->boletosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoleto $boleto The ChildBoleto object to add.
     */
    protected function doAddBoleto(ChildBoleto $boleto)
    {
        $this->collBoletos[]= $boleto;
        $boleto->setCompetencia($this);
    }

    /**
     * @param  ChildBoleto $boleto The ChildBoleto object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeBoleto(ChildBoleto $boleto)
    {
        if ($this->getBoletos()->contains($boleto)) {
            $pos = $this->collBoletos->search($boleto);
            $this->collBoletos->remove($pos);
            if (null === $this->boletosScheduledForDeletion) {
                $this->boletosScheduledForDeletion = clone $this->collBoletos;
                $this->boletosScheduledForDeletion->clear();
            }
            $this->boletosScheduledForDeletion[]= clone $boleto;
            $boleto->setCompetencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinBaixaEstorno(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('BaixaEstorno', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinFornecedor(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Fornecedor', $joinBehavior);

        return $this->getBoletos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related Boletos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoleto[] List of ChildBoleto objects
     */
    public function getBoletosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletos($query, $con);
    }

    /**
     * Clears out the collBoletoBaixaHistoricos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletoBaixaHistoricos()
     */
    public function clearBoletoBaixaHistoricos()
    {
        $this->collBoletoBaixaHistoricos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletoBaixaHistoricos collection loaded partially.
     */
    public function resetPartialBoletoBaixaHistoricos($v = true)
    {
        $this->collBoletoBaixaHistoricosPartial = $v;
    }

    /**
     * Initializes the collBoletoBaixaHistoricos collection.
     *
     * By default this just sets the collBoletoBaixaHistoricos collection to an empty array (like clearcollBoletoBaixaHistoricos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletoBaixaHistoricos($overrideExisting = true)
    {
        if (null !== $this->collBoletoBaixaHistoricos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoBaixaHistoricoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletoBaixaHistoricos = new $collectionClassName;
        $this->collBoletoBaixaHistoricos->setModel('\ImaTelecomBundle\Model\BoletoBaixaHistorico');
    }

    /**
     * Gets an array of ChildBoletoBaixaHistorico objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     * @throws PropelException
     */
    public function getBoletoBaixaHistoricos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                // return empty collection
                $this->initBoletoBaixaHistoricos();
            } else {
                $collBoletoBaixaHistoricos = ChildBoletoBaixaHistoricoQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletoBaixaHistoricosPartial && count($collBoletoBaixaHistoricos)) {
                        $this->initBoletoBaixaHistoricos(false);

                        foreach ($collBoletoBaixaHistoricos as $obj) {
                            if (false == $this->collBoletoBaixaHistoricos->contains($obj)) {
                                $this->collBoletoBaixaHistoricos->append($obj);
                            }
                        }

                        $this->collBoletoBaixaHistoricosPartial = true;
                    }

                    return $collBoletoBaixaHistoricos;
                }

                if ($partial && $this->collBoletoBaixaHistoricos) {
                    foreach ($this->collBoletoBaixaHistoricos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletoBaixaHistoricos[] = $obj;
                        }
                    }
                }

                $this->collBoletoBaixaHistoricos = $collBoletoBaixaHistoricos;
                $this->collBoletoBaixaHistoricosPartial = false;
            }
        }

        return $this->collBoletoBaixaHistoricos;
    }

    /**
     * Sets a collection of ChildBoletoBaixaHistorico objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletoBaixaHistoricos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setBoletoBaixaHistoricos(Collection $boletoBaixaHistoricos, ConnectionInterface $con = null)
    {
        /** @var ChildBoletoBaixaHistorico[] $boletoBaixaHistoricosToDelete */
        $boletoBaixaHistoricosToDelete = $this->getBoletoBaixaHistoricos(new Criteria(), $con)->diff($boletoBaixaHistoricos);


        $this->boletoBaixaHistoricosScheduledForDeletion = $boletoBaixaHistoricosToDelete;

        foreach ($boletoBaixaHistoricosToDelete as $boletoBaixaHistoricoRemoved) {
            $boletoBaixaHistoricoRemoved->setCompetencia(null);
        }

        $this->collBoletoBaixaHistoricos = null;
        foreach ($boletoBaixaHistoricos as $boletoBaixaHistorico) {
            $this->addBoletoBaixaHistorico($boletoBaixaHistorico);
        }

        $this->collBoletoBaixaHistoricos = $boletoBaixaHistoricos;
        $this->collBoletoBaixaHistoricosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BoletoBaixaHistorico objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BoletoBaixaHistorico objects.
     * @throws PropelException
     */
    public function countBoletoBaixaHistoricos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletoBaixaHistoricos());
            }

            $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collBoletoBaixaHistoricos);
    }

    /**
     * Method called to associate a ChildBoletoBaixaHistorico object to this object
     * through the ChildBoletoBaixaHistorico foreign key attribute.
     *
     * @param  ChildBoletoBaixaHistorico $l ChildBoletoBaixaHistorico
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addBoletoBaixaHistorico(ChildBoletoBaixaHistorico $l)
    {
        if ($this->collBoletoBaixaHistoricos === null) {
            $this->initBoletoBaixaHistoricos();
            $this->collBoletoBaixaHistoricosPartial = true;
        }

        if (!$this->collBoletoBaixaHistoricos->contains($l)) {
            $this->doAddBoletoBaixaHistorico($l);

            if ($this->boletoBaixaHistoricosScheduledForDeletion and $this->boletoBaixaHistoricosScheduledForDeletion->contains($l)) {
                $this->boletoBaixaHistoricosScheduledForDeletion->remove($this->boletoBaixaHistoricosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to add.
     */
    protected function doAddBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        $this->collBoletoBaixaHistoricos[]= $boletoBaixaHistorico;
        $boletoBaixaHistorico->setCompetencia($this);
    }

    /**
     * @param  ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        if ($this->getBoletoBaixaHistoricos()->contains($boletoBaixaHistorico)) {
            $pos = $this->collBoletoBaixaHistoricos->search($boletoBaixaHistorico);
            $this->collBoletoBaixaHistoricos->remove($pos);
            if (null === $this->boletoBaixaHistoricosScheduledForDeletion) {
                $this->boletoBaixaHistoricosScheduledForDeletion = clone $this->collBoletoBaixaHistoricos;
                $this->boletoBaixaHistoricosScheduledForDeletion->clear();
            }
            $this->boletoBaixaHistoricosScheduledForDeletion[]= clone $boletoBaixaHistorico;
            $boletoBaixaHistorico->setCompetencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixaEstorno(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('BaixaEstorno', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinLancamentos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Lancamentos', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }

    /**
     * Clears out the collDadosFiscals collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDadosFiscals()
     */
    public function clearDadosFiscals()
    {
        $this->collDadosFiscals = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDadosFiscals collection loaded partially.
     */
    public function resetPartialDadosFiscals($v = true)
    {
        $this->collDadosFiscalsPartial = $v;
    }

    /**
     * Initializes the collDadosFiscals collection.
     *
     * By default this just sets the collDadosFiscals collection to an empty array (like clearcollDadosFiscals());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDadosFiscals($overrideExisting = true)
    {
        if (null !== $this->collDadosFiscals && !$overrideExisting) {
            return;
        }

        $collectionClassName = DadosFiscalTableMap::getTableMap()->getCollectionClassName();

        $this->collDadosFiscals = new $collectionClassName;
        $this->collDadosFiscals->setModel('\ImaTelecomBundle\Model\DadosFiscal');
    }

    /**
     * Gets an array of ChildDadosFiscal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDadosFiscal[] List of ChildDadosFiscal objects
     * @throws PropelException
     */
    public function getDadosFiscals(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalsPartial && !$this->isNew();
        if (null === $this->collDadosFiscals || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscals) {
                // return empty collection
                $this->initDadosFiscals();
            } else {
                $collDadosFiscals = ChildDadosFiscalQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDadosFiscalsPartial && count($collDadosFiscals)) {
                        $this->initDadosFiscals(false);

                        foreach ($collDadosFiscals as $obj) {
                            if (false == $this->collDadosFiscals->contains($obj)) {
                                $this->collDadosFiscals->append($obj);
                            }
                        }

                        $this->collDadosFiscalsPartial = true;
                    }

                    return $collDadosFiscals;
                }

                if ($partial && $this->collDadosFiscals) {
                    foreach ($this->collDadosFiscals as $obj) {
                        if ($obj->isNew()) {
                            $collDadosFiscals[] = $obj;
                        }
                    }
                }

                $this->collDadosFiscals = $collDadosFiscals;
                $this->collDadosFiscalsPartial = false;
            }
        }

        return $this->collDadosFiscals;
    }

    /**
     * Sets a collection of ChildDadosFiscal objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $dadosFiscals A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setDadosFiscals(Collection $dadosFiscals, ConnectionInterface $con = null)
    {
        /** @var ChildDadosFiscal[] $dadosFiscalsToDelete */
        $dadosFiscalsToDelete = $this->getDadosFiscals(new Criteria(), $con)->diff($dadosFiscals);


        $this->dadosFiscalsScheduledForDeletion = $dadosFiscalsToDelete;

        foreach ($dadosFiscalsToDelete as $dadosFiscalRemoved) {
            $dadosFiscalRemoved->setCompetencia(null);
        }

        $this->collDadosFiscals = null;
        foreach ($dadosFiscals as $dadosFiscal) {
            $this->addDadosFiscal($dadosFiscal);
        }

        $this->collDadosFiscals = $dadosFiscals;
        $this->collDadosFiscalsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DadosFiscal objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related DadosFiscal objects.
     * @throws PropelException
     */
    public function countDadosFiscals(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalsPartial && !$this->isNew();
        if (null === $this->collDadosFiscals || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscals) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDadosFiscals());
            }

            $query = ChildDadosFiscalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collDadosFiscals);
    }

    /**
     * Method called to associate a ChildDadosFiscal object to this object
     * through the ChildDadosFiscal foreign key attribute.
     *
     * @param  ChildDadosFiscal $l ChildDadosFiscal
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addDadosFiscal(ChildDadosFiscal $l)
    {
        if ($this->collDadosFiscals === null) {
            $this->initDadosFiscals();
            $this->collDadosFiscalsPartial = true;
        }

        if (!$this->collDadosFiscals->contains($l)) {
            $this->doAddDadosFiscal($l);

            if ($this->dadosFiscalsScheduledForDeletion and $this->dadosFiscalsScheduledForDeletion->contains($l)) {
                $this->dadosFiscalsScheduledForDeletion->remove($this->dadosFiscalsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDadosFiscal $dadosFiscal The ChildDadosFiscal object to add.
     */
    protected function doAddDadosFiscal(ChildDadosFiscal $dadosFiscal)
    {
        $this->collDadosFiscals[]= $dadosFiscal;
        $dadosFiscal->setCompetencia($this);
    }

    /**
     * @param  ChildDadosFiscal $dadosFiscal The ChildDadosFiscal object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeDadosFiscal(ChildDadosFiscal $dadosFiscal)
    {
        if ($this->getDadosFiscals()->contains($dadosFiscal)) {
            $pos = $this->collDadosFiscals->search($dadosFiscal);
            $this->collDadosFiscals->remove($pos);
            if (null === $this->dadosFiscalsScheduledForDeletion) {
                $this->dadosFiscalsScheduledForDeletion = clone $this->collDadosFiscals;
                $this->dadosFiscalsScheduledForDeletion->clear();
            }
            $this->dadosFiscalsScheduledForDeletion[]= clone $dadosFiscal;
            $dadosFiscal->setCompetencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related DadosFiscals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscal[] List of ChildDadosFiscal objects
     */
    public function getDadosFiscalsJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getDadosFiscals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related DadosFiscals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscal[] List of ChildDadosFiscal objects
     */
    public function getDadosFiscalsJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getDadosFiscals($query, $con);
    }

    /**
     * Clears out the collFaturamentos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFaturamentos()
     */
    public function clearFaturamentos()
    {
        $this->collFaturamentos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFaturamentos collection loaded partially.
     */
    public function resetPartialFaturamentos($v = true)
    {
        $this->collFaturamentosPartial = $v;
    }

    /**
     * Initializes the collFaturamentos collection.
     *
     * By default this just sets the collFaturamentos collection to an empty array (like clearcollFaturamentos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFaturamentos($overrideExisting = true)
    {
        if (null !== $this->collFaturamentos && !$overrideExisting) {
            return;
        }

        $collectionClassName = FaturamentoTableMap::getTableMap()->getCollectionClassName();

        $this->collFaturamentos = new $collectionClassName;
        $this->collFaturamentos->setModel('\ImaTelecomBundle\Model\Faturamento');
    }

    /**
     * Gets an array of ChildFaturamento objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFaturamento[] List of ChildFaturamento objects
     * @throws PropelException
     */
    public function getFaturamentos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFaturamentosPartial && !$this->isNew();
        if (null === $this->collFaturamentos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFaturamentos) {
                // return empty collection
                $this->initFaturamentos();
            } else {
                $collFaturamentos = ChildFaturamentoQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFaturamentosPartial && count($collFaturamentos)) {
                        $this->initFaturamentos(false);

                        foreach ($collFaturamentos as $obj) {
                            if (false == $this->collFaturamentos->contains($obj)) {
                                $this->collFaturamentos->append($obj);
                            }
                        }

                        $this->collFaturamentosPartial = true;
                    }

                    return $collFaturamentos;
                }

                if ($partial && $this->collFaturamentos) {
                    foreach ($this->collFaturamentos as $obj) {
                        if ($obj->isNew()) {
                            $collFaturamentos[] = $obj;
                        }
                    }
                }

                $this->collFaturamentos = $collFaturamentos;
                $this->collFaturamentosPartial = false;
            }
        }

        return $this->collFaturamentos;
    }

    /**
     * Sets a collection of ChildFaturamento objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $faturamentos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setFaturamentos(Collection $faturamentos, ConnectionInterface $con = null)
    {
        /** @var ChildFaturamento[] $faturamentosToDelete */
        $faturamentosToDelete = $this->getFaturamentos(new Criteria(), $con)->diff($faturamentos);


        $this->faturamentosScheduledForDeletion = $faturamentosToDelete;

        foreach ($faturamentosToDelete as $faturamentoRemoved) {
            $faturamentoRemoved->setCompetencia(null);
        }

        $this->collFaturamentos = null;
        foreach ($faturamentos as $faturamento) {
            $this->addFaturamento($faturamento);
        }

        $this->collFaturamentos = $faturamentos;
        $this->collFaturamentosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Faturamento objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Faturamento objects.
     * @throws PropelException
     */
    public function countFaturamentos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFaturamentosPartial && !$this->isNew();
        if (null === $this->collFaturamentos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFaturamentos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFaturamentos());
            }

            $query = ChildFaturamentoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collFaturamentos);
    }

    /**
     * Method called to associate a ChildFaturamento object to this object
     * through the ChildFaturamento foreign key attribute.
     *
     * @param  ChildFaturamento $l ChildFaturamento
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addFaturamento(ChildFaturamento $l)
    {
        if ($this->collFaturamentos === null) {
            $this->initFaturamentos();
            $this->collFaturamentosPartial = true;
        }

        if (!$this->collFaturamentos->contains($l)) {
            $this->doAddFaturamento($l);

            if ($this->faturamentosScheduledForDeletion and $this->faturamentosScheduledForDeletion->contains($l)) {
                $this->faturamentosScheduledForDeletion->remove($this->faturamentosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFaturamento $faturamento The ChildFaturamento object to add.
     */
    protected function doAddFaturamento(ChildFaturamento $faturamento)
    {
        $this->collFaturamentos[]= $faturamento;
        $faturamento->setCompetencia($this);
    }

    /**
     * @param  ChildFaturamento $faturamento The ChildFaturamento object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeFaturamento(ChildFaturamento $faturamento)
    {
        if ($this->getFaturamentos()->contains($faturamento)) {
            $pos = $this->collFaturamentos->search($faturamento);
            $this->collFaturamentos->remove($pos);
            if (null === $this->faturamentosScheduledForDeletion) {
                $this->faturamentosScheduledForDeletion = clone $this->collFaturamentos;
                $this->faturamentosScheduledForDeletion->clear();
            }
            $this->faturamentosScheduledForDeletion[]= clone $faturamento;
            $faturamento->setCompetencia(null);
        }

        return $this;
    }

    /**
     * Clears out the collLancamentoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLancamentoss()
     */
    public function clearLancamentoss()
    {
        $this->collLancamentoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLancamentoss collection loaded partially.
     */
    public function resetPartialLancamentoss($v = true)
    {
        $this->collLancamentossPartial = $v;
    }

    /**
     * Initializes the collLancamentoss collection.
     *
     * By default this just sets the collLancamentoss collection to an empty array (like clearcollLancamentoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLancamentoss($overrideExisting = true)
    {
        if (null !== $this->collLancamentoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = LancamentosTableMap::getTableMap()->getCollectionClassName();

        $this->collLancamentoss = new $collectionClassName;
        $this->collLancamentoss->setModel('\ImaTelecomBundle\Model\Lancamentos');
    }

    /**
     * Gets an array of ChildLancamentos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLancamentos[] List of ChildLancamentos objects
     * @throws PropelException
     */
    public function getLancamentoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentossPartial && !$this->isNew();
        if (null === $this->collLancamentoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLancamentoss) {
                // return empty collection
                $this->initLancamentoss();
            } else {
                $collLancamentoss = ChildLancamentosQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLancamentossPartial && count($collLancamentoss)) {
                        $this->initLancamentoss(false);

                        foreach ($collLancamentoss as $obj) {
                            if (false == $this->collLancamentoss->contains($obj)) {
                                $this->collLancamentoss->append($obj);
                            }
                        }

                        $this->collLancamentossPartial = true;
                    }

                    return $collLancamentoss;
                }

                if ($partial && $this->collLancamentoss) {
                    foreach ($this->collLancamentoss as $obj) {
                        if ($obj->isNew()) {
                            $collLancamentoss[] = $obj;
                        }
                    }
                }

                $this->collLancamentoss = $collLancamentoss;
                $this->collLancamentossPartial = false;
            }
        }

        return $this->collLancamentoss;
    }

    /**
     * Sets a collection of ChildLancamentos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lancamentoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setLancamentoss(Collection $lancamentoss, ConnectionInterface $con = null)
    {
        /** @var ChildLancamentos[] $lancamentossToDelete */
        $lancamentossToDelete = $this->getLancamentoss(new Criteria(), $con)->diff($lancamentoss);


        $this->lancamentossScheduledForDeletion = $lancamentossToDelete;

        foreach ($lancamentossToDelete as $lancamentosRemoved) {
            $lancamentosRemoved->setCompetencia(null);
        }

        $this->collLancamentoss = null;
        foreach ($lancamentoss as $lancamentos) {
            $this->addLancamentos($lancamentos);
        }

        $this->collLancamentoss = $lancamentoss;
        $this->collLancamentossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Lancamentos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Lancamentos objects.
     * @throws PropelException
     */
    public function countLancamentoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentossPartial && !$this->isNew();
        if (null === $this->collLancamentoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLancamentoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLancamentoss());
            }

            $query = ChildLancamentosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collLancamentoss);
    }

    /**
     * Method called to associate a ChildLancamentos object to this object
     * through the ChildLancamentos foreign key attribute.
     *
     * @param  ChildLancamentos $l ChildLancamentos
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addLancamentos(ChildLancamentos $l)
    {
        if ($this->collLancamentoss === null) {
            $this->initLancamentoss();
            $this->collLancamentossPartial = true;
        }

        if (!$this->collLancamentoss->contains($l)) {
            $this->doAddLancamentos($l);

            if ($this->lancamentossScheduledForDeletion and $this->lancamentossScheduledForDeletion->contains($l)) {
                $this->lancamentossScheduledForDeletion->remove($this->lancamentossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLancamentos $lancamentos The ChildLancamentos object to add.
     */
    protected function doAddLancamentos(ChildLancamentos $lancamentos)
    {
        $this->collLancamentoss[]= $lancamentos;
        $lancamentos->setCompetencia($this);
    }

    /**
     * @param  ChildLancamentos $lancamentos The ChildLancamentos object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeLancamentos(ChildLancamentos $lancamentos)
    {
        if ($this->getLancamentoss()->contains($lancamentos)) {
            $pos = $this->collLancamentoss->search($lancamentos);
            $this->collLancamentoss->remove($pos);
            if (null === $this->lancamentossScheduledForDeletion) {
                $this->lancamentossScheduledForDeletion = clone $this->collLancamentoss;
                $this->lancamentossScheduledForDeletion->clear();
            }
            $this->lancamentossScheduledForDeletion[]= clone $lancamentos;
            $lancamentos->setCompetencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related Lancamentoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentos[] List of ChildLancamentos objects
     */
    public function getLancamentossJoinConvenio(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosQuery::create(null, $criteria);
        $query->joinWith('Convenio', $joinBehavior);

        return $this->getLancamentoss($query, $con);
    }

    /**
     * Clears out the collLancamentosBoletoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLancamentosBoletoss()
     */
    public function clearLancamentosBoletoss()
    {
        $this->collLancamentosBoletoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLancamentosBoletoss collection loaded partially.
     */
    public function resetPartialLancamentosBoletoss($v = true)
    {
        $this->collLancamentosBoletossPartial = $v;
    }

    /**
     * Initializes the collLancamentosBoletoss collection.
     *
     * By default this just sets the collLancamentosBoletoss collection to an empty array (like clearcollLancamentosBoletoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLancamentosBoletoss($overrideExisting = true)
    {
        if (null !== $this->collLancamentosBoletoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = LancamentosBoletosTableMap::getTableMap()->getCollectionClassName();

        $this->collLancamentosBoletoss = new $collectionClassName;
        $this->collLancamentosBoletoss->setModel('\ImaTelecomBundle\Model\LancamentosBoletos');
    }

    /**
     * Gets an array of ChildLancamentosBoletos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     * @throws PropelException
     */
    public function getLancamentosBoletoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentosBoletossPartial && !$this->isNew();
        if (null === $this->collLancamentosBoletoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLancamentosBoletoss) {
                // return empty collection
                $this->initLancamentosBoletoss();
            } else {
                $collLancamentosBoletoss = ChildLancamentosBoletosQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLancamentosBoletossPartial && count($collLancamentosBoletoss)) {
                        $this->initLancamentosBoletoss(false);

                        foreach ($collLancamentosBoletoss as $obj) {
                            if (false == $this->collLancamentosBoletoss->contains($obj)) {
                                $this->collLancamentosBoletoss->append($obj);
                            }
                        }

                        $this->collLancamentosBoletossPartial = true;
                    }

                    return $collLancamentosBoletoss;
                }

                if ($partial && $this->collLancamentosBoletoss) {
                    foreach ($this->collLancamentosBoletoss as $obj) {
                        if ($obj->isNew()) {
                            $collLancamentosBoletoss[] = $obj;
                        }
                    }
                }

                $this->collLancamentosBoletoss = $collLancamentosBoletoss;
                $this->collLancamentosBoletossPartial = false;
            }
        }

        return $this->collLancamentosBoletoss;
    }

    /**
     * Sets a collection of ChildLancamentosBoletos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lancamentosBoletoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setLancamentosBoletoss(Collection $lancamentosBoletoss, ConnectionInterface $con = null)
    {
        /** @var ChildLancamentosBoletos[] $lancamentosBoletossToDelete */
        $lancamentosBoletossToDelete = $this->getLancamentosBoletoss(new Criteria(), $con)->diff($lancamentosBoletoss);


        $this->lancamentosBoletossScheduledForDeletion = $lancamentosBoletossToDelete;

        foreach ($lancamentosBoletossToDelete as $lancamentosBoletosRemoved) {
            $lancamentosBoletosRemoved->setCompetencia(null);
        }

        $this->collLancamentosBoletoss = null;
        foreach ($lancamentosBoletoss as $lancamentosBoletos) {
            $this->addLancamentosBoletos($lancamentosBoletos);
        }

        $this->collLancamentosBoletoss = $lancamentosBoletoss;
        $this->collLancamentosBoletossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LancamentosBoletos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related LancamentosBoletos objects.
     * @throws PropelException
     */
    public function countLancamentosBoletoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentosBoletossPartial && !$this->isNew();
        if (null === $this->collLancamentosBoletoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLancamentosBoletoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLancamentosBoletoss());
            }

            $query = ChildLancamentosBoletosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collLancamentosBoletoss);
    }

    /**
     * Method called to associate a ChildLancamentosBoletos object to this object
     * through the ChildLancamentosBoletos foreign key attribute.
     *
     * @param  ChildLancamentosBoletos $l ChildLancamentosBoletos
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addLancamentosBoletos(ChildLancamentosBoletos $l)
    {
        if ($this->collLancamentosBoletoss === null) {
            $this->initLancamentosBoletoss();
            $this->collLancamentosBoletossPartial = true;
        }

        if (!$this->collLancamentosBoletoss->contains($l)) {
            $this->doAddLancamentosBoletos($l);

            if ($this->lancamentosBoletossScheduledForDeletion and $this->lancamentosBoletossScheduledForDeletion->contains($l)) {
                $this->lancamentosBoletossScheduledForDeletion->remove($this->lancamentosBoletossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLancamentosBoletos $lancamentosBoletos The ChildLancamentosBoletos object to add.
     */
    protected function doAddLancamentosBoletos(ChildLancamentosBoletos $lancamentosBoletos)
    {
        $this->collLancamentosBoletoss[]= $lancamentosBoletos;
        $lancamentosBoletos->setCompetencia($this);
    }

    /**
     * @param  ChildLancamentosBoletos $lancamentosBoletos The ChildLancamentosBoletos object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeLancamentosBoletos(ChildLancamentosBoletos $lancamentosBoletos)
    {
        if ($this->getLancamentosBoletoss()->contains($lancamentosBoletos)) {
            $pos = $this->collLancamentosBoletoss->search($lancamentosBoletos);
            $this->collLancamentosBoletoss->remove($pos);
            if (null === $this->lancamentosBoletossScheduledForDeletion) {
                $this->lancamentosBoletossScheduledForDeletion = clone $this->collLancamentosBoletoss;
                $this->lancamentosBoletossScheduledForDeletion->clear();
            }
            $this->lancamentosBoletossScheduledForDeletion[]= clone $lancamentosBoletos;
            $lancamentosBoletos->setCompetencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinConvenio(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Convenio', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinLancamentos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Lancamentos', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }

    /**
     * Clears out the collSicis collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSicis()
     */
    public function clearSicis()
    {
        $this->collSicis = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSicis collection loaded partially.
     */
    public function resetPartialSicis($v = true)
    {
        $this->collSicisPartial = $v;
    }

    /**
     * Initializes the collSicis collection.
     *
     * By default this just sets the collSicis collection to an empty array (like clearcollSicis());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSicis($overrideExisting = true)
    {
        if (null !== $this->collSicis && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiciTableMap::getTableMap()->getCollectionClassName();

        $this->collSicis = new $collectionClassName;
        $this->collSicis->setModel('\ImaTelecomBundle\Model\Sici');
    }

    /**
     * Gets an array of ChildSici objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSici[] List of ChildSici objects
     * @throws PropelException
     */
    public function getSicis(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSicisPartial && !$this->isNew();
        if (null === $this->collSicis || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSicis) {
                // return empty collection
                $this->initSicis();
            } else {
                $collSicis = ChildSiciQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSicisPartial && count($collSicis)) {
                        $this->initSicis(false);

                        foreach ($collSicis as $obj) {
                            if (false == $this->collSicis->contains($obj)) {
                                $this->collSicis->append($obj);
                            }
                        }

                        $this->collSicisPartial = true;
                    }

                    return $collSicis;
                }

                if ($partial && $this->collSicis) {
                    foreach ($this->collSicis as $obj) {
                        if ($obj->isNew()) {
                            $collSicis[] = $obj;
                        }
                    }
                }

                $this->collSicis = $collSicis;
                $this->collSicisPartial = false;
            }
        }

        return $this->collSicis;
    }

    /**
     * Sets a collection of ChildSici objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sicis A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setSicis(Collection $sicis, ConnectionInterface $con = null)
    {
        /** @var ChildSici[] $sicisToDelete */
        $sicisToDelete = $this->getSicis(new Criteria(), $con)->diff($sicis);


        $this->sicisScheduledForDeletion = $sicisToDelete;

        foreach ($sicisToDelete as $siciRemoved) {
            $siciRemoved->setCompetencia(null);
        }

        $this->collSicis = null;
        foreach ($sicis as $sici) {
            $this->addSici($sici);
        }

        $this->collSicis = $sicis;
        $this->collSicisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sici objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Sici objects.
     * @throws PropelException
     */
    public function countSicis(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSicisPartial && !$this->isNew();
        if (null === $this->collSicis || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSicis) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSicis());
            }

            $query = ChildSiciQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collSicis);
    }

    /**
     * Method called to associate a ChildSici object to this object
     * through the ChildSici foreign key attribute.
     *
     * @param  ChildSici $l ChildSici
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addSici(ChildSici $l)
    {
        if ($this->collSicis === null) {
            $this->initSicis();
            $this->collSicisPartial = true;
        }

        if (!$this->collSicis->contains($l)) {
            $this->doAddSici($l);

            if ($this->sicisScheduledForDeletion and $this->sicisScheduledForDeletion->contains($l)) {
                $this->sicisScheduledForDeletion->remove($this->sicisScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSici $sici The ChildSici object to add.
     */
    protected function doAddSici(ChildSici $sici)
    {
        $this->collSicis[]= $sici;
        $sici->setCompetencia($this);
    }

    /**
     * @param  ChildSici $sici The ChildSici object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeSici(ChildSici $sici)
    {
        if ($this->getSicis()->contains($sici)) {
            $pos = $this->collSicis->search($sici);
            $this->collSicis->remove($pos);
            if (null === $this->sicisScheduledForDeletion) {
                $this->sicisScheduledForDeletion = clone $this->collSicis;
                $this->sicisScheduledForDeletion->clear();
            }
            $this->sicisScheduledForDeletion[]= clone $sici;
            $sici->setCompetencia(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Competencia is new, it will return
     * an empty collection; or if this Competencia has previously
     * been saved, it will retrieve related Sicis from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Competencia.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSici[] List of ChildSici objects
     */
    public function getSicisJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiciQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getSicis($query, $con);
    }

    /**
     * Clears out the collSintegras collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSintegras()
     */
    public function clearSintegras()
    {
        $this->collSintegras = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSintegras collection loaded partially.
     */
    public function resetPartialSintegras($v = true)
    {
        $this->collSintegrasPartial = $v;
    }

    /**
     * Initializes the collSintegras collection.
     *
     * By default this just sets the collSintegras collection to an empty array (like clearcollSintegras());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSintegras($overrideExisting = true)
    {
        if (null !== $this->collSintegras && !$overrideExisting) {
            return;
        }

        $collectionClassName = SintegraTableMap::getTableMap()->getCollectionClassName();

        $this->collSintegras = new $collectionClassName;
        $this->collSintegras->setModel('\ImaTelecomBundle\Model\Sintegra');
    }

    /**
     * Gets an array of ChildSintegra objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompetencia is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSintegra[] List of ChildSintegra objects
     * @throws PropelException
     */
    public function getSintegras(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSintegrasPartial && !$this->isNew();
        if (null === $this->collSintegras || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSintegras) {
                // return empty collection
                $this->initSintegras();
            } else {
                $collSintegras = ChildSintegraQuery::create(null, $criteria)
                    ->filterByCompetencia($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSintegrasPartial && count($collSintegras)) {
                        $this->initSintegras(false);

                        foreach ($collSintegras as $obj) {
                            if (false == $this->collSintegras->contains($obj)) {
                                $this->collSintegras->append($obj);
                            }
                        }

                        $this->collSintegrasPartial = true;
                    }

                    return $collSintegras;
                }

                if ($partial && $this->collSintegras) {
                    foreach ($this->collSintegras as $obj) {
                        if ($obj->isNew()) {
                            $collSintegras[] = $obj;
                        }
                    }
                }

                $this->collSintegras = $collSintegras;
                $this->collSintegrasPartial = false;
            }
        }

        return $this->collSintegras;
    }

    /**
     * Sets a collection of ChildSintegra objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sintegras A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function setSintegras(Collection $sintegras, ConnectionInterface $con = null)
    {
        /** @var ChildSintegra[] $sintegrasToDelete */
        $sintegrasToDelete = $this->getSintegras(new Criteria(), $con)->diff($sintegras);


        $this->sintegrasScheduledForDeletion = $sintegrasToDelete;

        foreach ($sintegrasToDelete as $sintegraRemoved) {
            $sintegraRemoved->setCompetencia(null);
        }

        $this->collSintegras = null;
        foreach ($sintegras as $sintegra) {
            $this->addSintegra($sintegra);
        }

        $this->collSintegras = $sintegras;
        $this->collSintegrasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Sintegra objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Sintegra objects.
     * @throws PropelException
     */
    public function countSintegras(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSintegrasPartial && !$this->isNew();
        if (null === $this->collSintegras || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSintegras) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSintegras());
            }

            $query = ChildSintegraQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompetencia($this)
                ->count($con);
        }

        return count($this->collSintegras);
    }

    /**
     * Method called to associate a ChildSintegra object to this object
     * through the ChildSintegra foreign key attribute.
     *
     * @param  ChildSintegra $l ChildSintegra
     * @return $this|\ImaTelecomBundle\Model\Competencia The current object (for fluent API support)
     */
    public function addSintegra(ChildSintegra $l)
    {
        if ($this->collSintegras === null) {
            $this->initSintegras();
            $this->collSintegrasPartial = true;
        }

        if (!$this->collSintegras->contains($l)) {
            $this->doAddSintegra($l);

            if ($this->sintegrasScheduledForDeletion and $this->sintegrasScheduledForDeletion->contains($l)) {
                $this->sintegrasScheduledForDeletion->remove($this->sintegrasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSintegra $sintegra The ChildSintegra object to add.
     */
    protected function doAddSintegra(ChildSintegra $sintegra)
    {
        $this->collSintegras[]= $sintegra;
        $sintegra->setCompetencia($this);
    }

    /**
     * @param  ChildSintegra $sintegra The ChildSintegra object to remove.
     * @return $this|ChildCompetencia The current object (for fluent API support)
     */
    public function removeSintegra(ChildSintegra $sintegra)
    {
        if ($this->getSintegras()->contains($sintegra)) {
            $pos = $this->collSintegras->search($sintegra);
            $this->collSintegras->remove($pos);
            if (null === $this->sintegrasScheduledForDeletion) {
                $this->sintegrasScheduledForDeletion = clone $this->collSintegras;
                $this->sintegrasScheduledForDeletion->clear();
            }
            $this->sintegrasScheduledForDeletion[]= clone $sintegra;
            $sintegra->setCompetencia(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->ano = null;
        $this->mes = null;
        $this->numero_nf = null;
        $this->periodo_inicial = null;
        $this->periodo_final = null;
        $this->ativo = null;
        $this->descricao = null;
        $this->periodo_futuro = null;
        $this->proxima_competencia = null;
        $this->encerramento = null;
        $this->trabalhar_periodo_futuro = null;
        $this->ultima_data_emissao = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBaixas) {
                foreach ($this->collBaixas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBaixaEstornos) {
                foreach ($this->collBaixaEstornos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBoletos) {
                foreach ($this->collBoletos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBoletoBaixaHistoricos) {
                foreach ($this->collBoletoBaixaHistoricos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDadosFiscals) {
                foreach ($this->collDadosFiscals as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFaturamentos) {
                foreach ($this->collFaturamentos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLancamentoss) {
                foreach ($this->collLancamentoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLancamentosBoletoss) {
                foreach ($this->collLancamentosBoletoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSicis) {
                foreach ($this->collSicis as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSintegras) {
                foreach ($this->collSintegras as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBaixas = null;
        $this->collBaixaEstornos = null;
        $this->collBoletos = null;
        $this->collBoletoBaixaHistoricos = null;
        $this->collDadosFiscals = null;
        $this->collFaturamentos = null;
        $this->collLancamentoss = null;
        $this->collLancamentosBoletoss = null;
        $this->collSicis = null;
        $this->collSintegras = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CompetenciaTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
