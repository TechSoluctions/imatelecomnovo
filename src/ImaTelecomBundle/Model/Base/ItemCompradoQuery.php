<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ItemComprado as ChildItemComprado;
use ImaTelecomBundle\Model\ItemCompradoQuery as ChildItemCompradoQuery;
use ImaTelecomBundle\Model\Map\ItemCompradoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'item_comprado' table.
 *
 *
 *
 * @method     ChildItemCompradoQuery orderByIditemComprado($order = Criteria::ASC) Order by the iditem_comprado column
 * @method     ChildItemCompradoQuery orderByItemDeCompraId($order = Criteria::ASC) Order by the item_de_compra_id column
 * @method     ChildItemCompradoQuery orderByContasPagarId($order = Criteria::ASC) Order by the contas_pagar_id column
 * @method     ChildItemCompradoQuery orderByQtde($order = Criteria::ASC) Order by the qtde column
 * @method     ChildItemCompradoQuery orderByValorUnitario($order = Criteria::ASC) Order by the valor_unitario column
 * @method     ChildItemCompradoQuery orderByObs($order = Criteria::ASC) Order by the obs column
 * @method     ChildItemCompradoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildItemCompradoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildItemCompradoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildItemCompradoQuery groupByIditemComprado() Group by the iditem_comprado column
 * @method     ChildItemCompradoQuery groupByItemDeCompraId() Group by the item_de_compra_id column
 * @method     ChildItemCompradoQuery groupByContasPagarId() Group by the contas_pagar_id column
 * @method     ChildItemCompradoQuery groupByQtde() Group by the qtde column
 * @method     ChildItemCompradoQuery groupByValorUnitario() Group by the valor_unitario column
 * @method     ChildItemCompradoQuery groupByObs() Group by the obs column
 * @method     ChildItemCompradoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildItemCompradoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildItemCompradoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildItemCompradoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildItemCompradoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildItemCompradoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildItemCompradoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildItemCompradoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildItemCompradoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildItemCompradoQuery leftJoinContasPagar($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagar relation
 * @method     ChildItemCompradoQuery rightJoinContasPagar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagar relation
 * @method     ChildItemCompradoQuery innerJoinContasPagar($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagar relation
 *
 * @method     ChildItemCompradoQuery joinWithContasPagar($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagar relation
 *
 * @method     ChildItemCompradoQuery leftJoinWithContasPagar() Adds a LEFT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildItemCompradoQuery rightJoinWithContasPagar() Adds a RIGHT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildItemCompradoQuery innerJoinWithContasPagar() Adds a INNER JOIN clause and with to the query using the ContasPagar relation
 *
 * @method     ChildItemCompradoQuery leftJoinItemDeCompra($relationAlias = null) Adds a LEFT JOIN clause to the query using the ItemDeCompra relation
 * @method     ChildItemCompradoQuery rightJoinItemDeCompra($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ItemDeCompra relation
 * @method     ChildItemCompradoQuery innerJoinItemDeCompra($relationAlias = null) Adds a INNER JOIN clause to the query using the ItemDeCompra relation
 *
 * @method     ChildItemCompradoQuery joinWithItemDeCompra($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ItemDeCompra relation
 *
 * @method     ChildItemCompradoQuery leftJoinWithItemDeCompra() Adds a LEFT JOIN clause and with to the query using the ItemDeCompra relation
 * @method     ChildItemCompradoQuery rightJoinWithItemDeCompra() Adds a RIGHT JOIN clause and with to the query using the ItemDeCompra relation
 * @method     ChildItemCompradoQuery innerJoinWithItemDeCompra() Adds a INNER JOIN clause and with to the query using the ItemDeCompra relation
 *
 * @method     ChildItemCompradoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildItemCompradoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildItemCompradoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildItemCompradoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildItemCompradoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildItemCompradoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildItemCompradoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \ImaTelecomBundle\Model\ContasPagarQuery|\ImaTelecomBundle\Model\ItemDeCompraQuery|\ImaTelecomBundle\Model\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildItemComprado findOne(ConnectionInterface $con = null) Return the first ChildItemComprado matching the query
 * @method     ChildItemComprado findOneOrCreate(ConnectionInterface $con = null) Return the first ChildItemComprado matching the query, or a new ChildItemComprado object populated from the query conditions when no match is found
 *
 * @method     ChildItemComprado findOneByIditemComprado(int $iditem_comprado) Return the first ChildItemComprado filtered by the iditem_comprado column
 * @method     ChildItemComprado findOneByItemDeCompraId(int $item_de_compra_id) Return the first ChildItemComprado filtered by the item_de_compra_id column
 * @method     ChildItemComprado findOneByContasPagarId(int $contas_pagar_id) Return the first ChildItemComprado filtered by the contas_pagar_id column
 * @method     ChildItemComprado findOneByQtde(int $qtde) Return the first ChildItemComprado filtered by the qtde column
 * @method     ChildItemComprado findOneByValorUnitario(double $valor_unitario) Return the first ChildItemComprado filtered by the valor_unitario column
 * @method     ChildItemComprado findOneByObs(string $obs) Return the first ChildItemComprado filtered by the obs column
 * @method     ChildItemComprado findOneByDataCadastro(string $data_cadastro) Return the first ChildItemComprado filtered by the data_cadastro column
 * @method     ChildItemComprado findOneByDataAlterado(string $data_alterado) Return the first ChildItemComprado filtered by the data_alterado column
 * @method     ChildItemComprado findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildItemComprado filtered by the usuario_alterado column *

 * @method     ChildItemComprado requirePk($key, ConnectionInterface $con = null) Return the ChildItemComprado by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemComprado requireOne(ConnectionInterface $con = null) Return the first ChildItemComprado matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildItemComprado requireOneByIditemComprado(int $iditem_comprado) Return the first ChildItemComprado filtered by the iditem_comprado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemComprado requireOneByItemDeCompraId(int $item_de_compra_id) Return the first ChildItemComprado filtered by the item_de_compra_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemComprado requireOneByContasPagarId(int $contas_pagar_id) Return the first ChildItemComprado filtered by the contas_pagar_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemComprado requireOneByQtde(int $qtde) Return the first ChildItemComprado filtered by the qtde column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemComprado requireOneByValorUnitario(double $valor_unitario) Return the first ChildItemComprado filtered by the valor_unitario column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemComprado requireOneByObs(string $obs) Return the first ChildItemComprado filtered by the obs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemComprado requireOneByDataCadastro(string $data_cadastro) Return the first ChildItemComprado filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemComprado requireOneByDataAlterado(string $data_alterado) Return the first ChildItemComprado filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemComprado requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildItemComprado filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildItemComprado[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildItemComprado objects based on current ModelCriteria
 * @method     ChildItemComprado[]|ObjectCollection findByIditemComprado(int $iditem_comprado) Return ChildItemComprado objects filtered by the iditem_comprado column
 * @method     ChildItemComprado[]|ObjectCollection findByItemDeCompraId(int $item_de_compra_id) Return ChildItemComprado objects filtered by the item_de_compra_id column
 * @method     ChildItemComprado[]|ObjectCollection findByContasPagarId(int $contas_pagar_id) Return ChildItemComprado objects filtered by the contas_pagar_id column
 * @method     ChildItemComprado[]|ObjectCollection findByQtde(int $qtde) Return ChildItemComprado objects filtered by the qtde column
 * @method     ChildItemComprado[]|ObjectCollection findByValorUnitario(double $valor_unitario) Return ChildItemComprado objects filtered by the valor_unitario column
 * @method     ChildItemComprado[]|ObjectCollection findByObs(string $obs) Return ChildItemComprado objects filtered by the obs column
 * @method     ChildItemComprado[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildItemComprado objects filtered by the data_cadastro column
 * @method     ChildItemComprado[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildItemComprado objects filtered by the data_alterado column
 * @method     ChildItemComprado[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildItemComprado objects filtered by the usuario_alterado column
 * @method     ChildItemComprado[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ItemCompradoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ItemCompradoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ItemComprado', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildItemCompradoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildItemCompradoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildItemCompradoQuery) {
            return $criteria;
        }
        $query = new ChildItemCompradoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildItemComprado|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ItemCompradoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ItemCompradoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildItemComprado A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT iditem_comprado, item_de_compra_id, contas_pagar_id, qtde, valor_unitario, obs, data_cadastro, data_alterado, usuario_alterado FROM item_comprado WHERE iditem_comprado = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildItemComprado $obj */
            $obj = new ChildItemComprado();
            $obj->hydrate($row);
            ItemCompradoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildItemComprado|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ItemCompradoTableMap::COL_IDITEM_COMPRADO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ItemCompradoTableMap::COL_IDITEM_COMPRADO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the iditem_comprado column
     *
     * Example usage:
     * <code>
     * $query->filterByIditemComprado(1234); // WHERE iditem_comprado = 1234
     * $query->filterByIditemComprado(array(12, 34)); // WHERE iditem_comprado IN (12, 34)
     * $query->filterByIditemComprado(array('min' => 12)); // WHERE iditem_comprado > 12
     * </code>
     *
     * @param     mixed $iditemComprado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByIditemComprado($iditemComprado = null, $comparison = null)
    {
        if (is_array($iditemComprado)) {
            $useMinMax = false;
            if (isset($iditemComprado['min'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_IDITEM_COMPRADO, $iditemComprado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iditemComprado['max'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_IDITEM_COMPRADO, $iditemComprado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemCompradoTableMap::COL_IDITEM_COMPRADO, $iditemComprado, $comparison);
    }

    /**
     * Filter the query on the item_de_compra_id column
     *
     * Example usage:
     * <code>
     * $query->filterByItemDeCompraId(1234); // WHERE item_de_compra_id = 1234
     * $query->filterByItemDeCompraId(array(12, 34)); // WHERE item_de_compra_id IN (12, 34)
     * $query->filterByItemDeCompraId(array('min' => 12)); // WHERE item_de_compra_id > 12
     * </code>
     *
     * @see       filterByItemDeCompra()
     *
     * @param     mixed $itemDeCompraId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByItemDeCompraId($itemDeCompraId = null, $comparison = null)
    {
        if (is_array($itemDeCompraId)) {
            $useMinMax = false;
            if (isset($itemDeCompraId['min'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompraId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($itemDeCompraId['max'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompraId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemCompradoTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompraId, $comparison);
    }

    /**
     * Filter the query on the contas_pagar_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContasPagarId(1234); // WHERE contas_pagar_id = 1234
     * $query->filterByContasPagarId(array(12, 34)); // WHERE contas_pagar_id IN (12, 34)
     * $query->filterByContasPagarId(array('min' => 12)); // WHERE contas_pagar_id > 12
     * </code>
     *
     * @see       filterByContasPagar()
     *
     * @param     mixed $contasPagarId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByContasPagarId($contasPagarId = null, $comparison = null)
    {
        if (is_array($contasPagarId)) {
            $useMinMax = false;
            if (isset($contasPagarId['min'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_CONTAS_PAGAR_ID, $contasPagarId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contasPagarId['max'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_CONTAS_PAGAR_ID, $contasPagarId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemCompradoTableMap::COL_CONTAS_PAGAR_ID, $contasPagarId, $comparison);
    }

    /**
     * Filter the query on the qtde column
     *
     * Example usage:
     * <code>
     * $query->filterByQtde(1234); // WHERE qtde = 1234
     * $query->filterByQtde(array(12, 34)); // WHERE qtde IN (12, 34)
     * $query->filterByQtde(array('min' => 12)); // WHERE qtde > 12
     * </code>
     *
     * @param     mixed $qtde The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByQtde($qtde = null, $comparison = null)
    {
        if (is_array($qtde)) {
            $useMinMax = false;
            if (isset($qtde['min'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_QTDE, $qtde['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($qtde['max'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_QTDE, $qtde['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemCompradoTableMap::COL_QTDE, $qtde, $comparison);
    }

    /**
     * Filter the query on the valor_unitario column
     *
     * Example usage:
     * <code>
     * $query->filterByValorUnitario(1234); // WHERE valor_unitario = 1234
     * $query->filterByValorUnitario(array(12, 34)); // WHERE valor_unitario IN (12, 34)
     * $query->filterByValorUnitario(array('min' => 12)); // WHERE valor_unitario > 12
     * </code>
     *
     * @param     mixed $valorUnitario The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByValorUnitario($valorUnitario = null, $comparison = null)
    {
        if (is_array($valorUnitario)) {
            $useMinMax = false;
            if (isset($valorUnitario['min'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_VALOR_UNITARIO, $valorUnitario['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorUnitario['max'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_VALOR_UNITARIO, $valorUnitario['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemCompradoTableMap::COL_VALOR_UNITARIO, $valorUnitario, $comparison);
    }

    /**
     * Filter the query on the obs column
     *
     * Example usage:
     * <code>
     * $query->filterByObs('fooValue');   // WHERE obs = 'fooValue'
     * $query->filterByObs('%fooValue%', Criteria::LIKE); // WHERE obs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $obs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByObs($obs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($obs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemCompradoTableMap::COL_OBS, $obs, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemCompradoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemCompradoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ItemCompradoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemCompradoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagar object
     *
     * @param \ImaTelecomBundle\Model\ContasPagar|ObjectCollection $contasPagar The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByContasPagar($contasPagar, $comparison = null)
    {
        if ($contasPagar instanceof \ImaTelecomBundle\Model\ContasPagar) {
            return $this
                ->addUsingAlias(ItemCompradoTableMap::COL_CONTAS_PAGAR_ID, $contasPagar->getIdcontasPagar(), $comparison);
        } elseif ($contasPagar instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ItemCompradoTableMap::COL_CONTAS_PAGAR_ID, $contasPagar->toKeyValue('PrimaryKey', 'IdcontasPagar'), $comparison);
        } else {
            throw new PropelException('filterByContasPagar() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function joinContasPagar($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagar');
        }

        return $this;
    }

    /**
     * Use the ContasPagar relation ContasPagar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContasPagar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagar', '\ImaTelecomBundle\Model\ContasPagarQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ItemDeCompra object
     *
     * @param \ImaTelecomBundle\Model\ItemDeCompra|ObjectCollection $itemDeCompra The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByItemDeCompra($itemDeCompra, $comparison = null)
    {
        if ($itemDeCompra instanceof \ImaTelecomBundle\Model\ItemDeCompra) {
            return $this
                ->addUsingAlias(ItemCompradoTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompra->getIditemDeCompra(), $comparison);
        } elseif ($itemDeCompra instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ItemCompradoTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompra->toKeyValue('PrimaryKey', 'IditemDeCompra'), $comparison);
        } else {
            throw new PropelException('filterByItemDeCompra() only accepts arguments of type \ImaTelecomBundle\Model\ItemDeCompra or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ItemDeCompra relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function joinItemDeCompra($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ItemDeCompra');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ItemDeCompra');
        }

        return $this;
    }

    /**
     * Use the ItemDeCompra relation ItemDeCompra object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ItemDeCompraQuery A secondary query class using the current class as primary query
     */
    public function useItemDeCompraQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinItemDeCompra($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ItemDeCompra', '\ImaTelecomBundle\Model\ItemDeCompraQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildItemCompradoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ItemCompradoTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ItemCompradoTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildItemComprado $itemComprado Object to remove from the list of results
     *
     * @return $this|ChildItemCompradoQuery The current query, for fluid interface
     */
    public function prune($itemComprado = null)
    {
        if ($itemComprado) {
            $this->addUsingAlias(ItemCompradoTableMap::COL_IDITEM_COMPRADO, $itemComprado->getIditemComprado(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the item_comprado table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ItemCompradoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ItemCompradoTableMap::clearInstancePool();
            ItemCompradoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ItemCompradoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ItemCompradoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ItemCompradoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ItemCompradoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ItemCompradoQuery
