<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BoletoBaixaHistorico as ChildBoletoBaixaHistorico;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery as ChildBoletoBaixaHistoricoQuery;
use ImaTelecomBundle\Model\Cliente as ChildCliente;
use ImaTelecomBundle\Model\ClienteQuery as ChildClienteQuery;
use ImaTelecomBundle\Model\DadosFiscal as ChildDadosFiscal;
use ImaTelecomBundle\Model\DadosFiscalQuery as ChildDadosFiscalQuery;
use ImaTelecomBundle\Model\Empresa as ChildEmpresa;
use ImaTelecomBundle\Model\EmpresaQuery as ChildEmpresaQuery;
use ImaTelecomBundle\Model\Fornecedor as ChildFornecedor;
use ImaTelecomBundle\Model\FornecedorQuery as ChildFornecedorQuery;
use ImaTelecomBundle\Model\Pessoa as ChildPessoa;
use ImaTelecomBundle\Model\PessoaQuery as ChildPessoaQuery;
use ImaTelecomBundle\Model\Telefone as ChildTelefone;
use ImaTelecomBundle\Model\TelefoneQuery as ChildTelefoneQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BoletoBaixaHistoricoTableMap;
use ImaTelecomBundle\Model\Map\ClienteTableMap;
use ImaTelecomBundle\Model\Map\DadosFiscalTableMap;
use ImaTelecomBundle\Model\Map\EmpresaTableMap;
use ImaTelecomBundle\Model\Map\FornecedorTableMap;
use ImaTelecomBundle\Model\Map\PessoaTableMap;
use ImaTelecomBundle\Model\Map\TelefoneTableMap;
use ImaTelecomBundle\Model\Map\UsuarioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'pessoa' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Pessoa implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\PessoaTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the nome field.
     *
     * @var        string
     */
    protected $nome;

    /**
     * The value for the razao_social field.
     *
     * @var        string
     */
    protected $razao_social;

    /**
     * The value for the rua field.
     *
     * @var        string
     */
    protected $rua;

    /**
     * The value for the bairro field.
     *
     * @var        string
     */
    protected $bairro;

    /**
     * The value for the cep field.
     *
     * @var        string
     */
    protected $cep;

    /**
     * The value for the num field.
     *
     * @var        int
     */
    protected $num;

    /**
     * The value for the uf field.
     *
     * @var        string
     */
    protected $uf;

    /**
     * The value for the cpf_cnpj field.
     *
     * @var        string
     */
    protected $cpf_cnpj;

    /**
     * The value for the rg_inscrest field.
     *
     * @var        string
     */
    protected $rg_inscrest;

    /**
     * The value for the ponto_referencia field.
     *
     * @var        string
     */
    protected $ponto_referencia;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the complemento field.
     *
     * @var        string
     */
    protected $complemento;

    /**
     * The value for the tipo_pessoa field.
     *
     * @var        string
     */
    protected $tipo_pessoa;

    /**
     * The value for the data_nascimento field.
     *
     * @var        DateTime
     */
    protected $data_nascimento;

    /**
     * The value for the data_cadastro field.
     *
     * Note: this column has a database default value of: NULL
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the obs field.
     *
     * @var        string
     */
    protected $obs;

    /**
     * The value for the cidade_id field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $cidade_id;

    /**
     * The value for the data_alterado field.
     *
     * Note: this column has a database default value of: NULL
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the responsavel field.
     *
     * @var        string
     */
    protected $responsavel;

    /**
     * The value for the contato field.
     *
     * @var        string
     */
    protected $contato;

    /**
     * The value for the sexo field.
     *
     * Note: this column has a database default value of: 'm'
     * @var        string
     */
    protected $sexo;

    /**
     * The value for the import_id field.
     *
     * @var        int
     */
    protected $import_id;

    /**
     * The value for the e_fornecedor field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $e_fornecedor;

    /**
     * @var        ObjectCollection|ChildBoletoBaixaHistorico[] Collection to store aggregation of ChildBoletoBaixaHistorico objects.
     */
    protected $collBoletoBaixaHistoricos;
    protected $collBoletoBaixaHistoricosPartial;

    /**
     * @var        ObjectCollection|ChildCliente[] Collection to store aggregation of ChildCliente objects.
     */
    protected $collClientes;
    protected $collClientesPartial;

    /**
     * @var        ObjectCollection|ChildDadosFiscal[] Collection to store aggregation of ChildDadosFiscal objects.
     */
    protected $collDadosFiscals;
    protected $collDadosFiscalsPartial;

    /**
     * @var        ObjectCollection|ChildEmpresa[] Collection to store aggregation of ChildEmpresa objects.
     */
    protected $collEmpresas;
    protected $collEmpresasPartial;

    /**
     * @var        ObjectCollection|ChildFornecedor[] Collection to store aggregation of ChildFornecedor objects.
     */
    protected $collFornecedors;
    protected $collFornecedorsPartial;

    /**
     * @var        ObjectCollection|ChildTelefone[] Collection to store aggregation of ChildTelefone objects.
     */
    protected $collTelefones;
    protected $collTelefonesPartial;

    /**
     * @var        ObjectCollection|ChildUsuario[] Collection to store aggregation of ChildUsuario objects.
     */
    protected $collUsuarios;
    protected $collUsuariosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoletoBaixaHistorico[]
     */
    protected $boletoBaixaHistoricosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCliente[]
     */
    protected $clientesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDadosFiscal[]
     */
    protected $dadosFiscalsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEmpresa[]
     */
    protected $empresasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFornecedor[]
     */
    protected $fornecedorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildTelefone[]
     */
    protected $telefonesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuario[]
     */
    protected $usuariosScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->data_cadastro = PropelDateTime::newInstance(NULL, null, 'DateTime');
        $this->cidade_id = 1;
        $this->data_alterado = PropelDateTime::newInstance(NULL, null, 'DateTime');
        $this->usuario_alterado = 1;
        $this->sexo = 'm';
        $this->e_fornecedor = false;
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Pessoa object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Pessoa</code> instance.  If
     * <code>obj</code> is an instance of <code>Pessoa</code>, delegates to
     * <code>equals(Pessoa)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Pessoa The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [nome] column value.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Get the [razao_social] column value.
     *
     * @return string
     */
    public function getRazaoSocial()
    {
        return $this->razao_social;
    }

    /**
     * Get the [rua] column value.
     *
     * @return string
     */
    public function getRua()
    {
        return $this->rua;
    }

    /**
     * Get the [bairro] column value.
     *
     * @return string
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Get the [cep] column value.
     *
     * @return string
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Get the [num] column value.
     *
     * @return int
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Get the [uf] column value.
     *
     * @return string
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * Get the [cpf_cnpj] column value.
     *
     * @return string
     */
    public function getCpfCnpj()
    {
        return $this->cpf_cnpj;
    }

    /**
     * Get the [rg_inscrest] column value.
     *
     * @return string
     */
    public function getRgInscrest()
    {
        return $this->rg_inscrest;
    }

    /**
     * Get the [ponto_referencia] column value.
     *
     * @return string
     */
    public function getPontoReferencia()
    {
        return $this->ponto_referencia;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [complemento] column value.
     *
     * @return string
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * Get the [tipo_pessoa] column value.
     *
     * @return string
     */
    public function getTipoPessoa()
    {
        return $this->tipo_pessoa;
    }

    /**
     * Get the [optionally formatted] temporal [data_nascimento] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataNascimento($format = NULL)
    {
        if ($format === null) {
            return $this->data_nascimento;
        } else {
            return $this->data_nascimento instanceof \DateTimeInterface ? $this->data_nascimento->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [obs] column value.
     *
     * @return string
     */
    public function getObs()
    {
        return $this->obs;
    }

    /**
     * Get the [cidade_id] column value.
     *
     * @return int
     */
    public function getCidadeId()
    {
        return $this->cidade_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [responsavel] column value.
     *
     * @return string
     */
    public function getResponsavel()
    {
        return $this->responsavel;
    }

    /**
     * Get the [contato] column value.
     *
     * @return string
     */
    public function getContato()
    {
        return $this->contato;
    }

    /**
     * Get the [sexo] column value.
     *
     * @return string
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * Get the [import_id] column value.
     *
     * @return int
     */
    public function getImportId()
    {
        return $this->import_id;
    }

    /**
     * Get the [e_fornecedor] column value.
     *
     * @return boolean
     */
    public function getEFornecedor()
    {
        return $this->e_fornecedor;
    }

    /**
     * Get the [e_fornecedor] column value.
     *
     * @return boolean
     */
    public function isEFornecedor()
    {
        return $this->getEFornecedor();
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[PessoaTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [nome] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setNome($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome !== $v) {
            $this->nome = $v;
            $this->modifiedColumns[PessoaTableMap::COL_NOME] = true;
        }

        return $this;
    } // setNome()

    /**
     * Set the value of [razao_social] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setRazaoSocial($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->razao_social !== $v) {
            $this->razao_social = $v;
            $this->modifiedColumns[PessoaTableMap::COL_RAZAO_SOCIAL] = true;
        }

        return $this;
    } // setRazaoSocial()

    /**
     * Set the value of [rua] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setRua($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->rua !== $v) {
            $this->rua = $v;
            $this->modifiedColumns[PessoaTableMap::COL_RUA] = true;
        }

        return $this;
    } // setRua()

    /**
     * Set the value of [bairro] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setBairro($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bairro !== $v) {
            $this->bairro = $v;
            $this->modifiedColumns[PessoaTableMap::COL_BAIRRO] = true;
        }

        return $this;
    } // setBairro()

    /**
     * Set the value of [cep] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setCep($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cep !== $v) {
            $this->cep = $v;
            $this->modifiedColumns[PessoaTableMap::COL_CEP] = true;
        }

        return $this;
    } // setCep()

    /**
     * Set the value of [num] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setNum($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->num !== $v) {
            $this->num = $v;
            $this->modifiedColumns[PessoaTableMap::COL_NUM] = true;
        }

        return $this;
    } // setNum()

    /**
     * Set the value of [uf] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setUf($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uf !== $v) {
            $this->uf = $v;
            $this->modifiedColumns[PessoaTableMap::COL_UF] = true;
        }

        return $this;
    } // setUf()

    /**
     * Set the value of [cpf_cnpj] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setCpfCnpj($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cpf_cnpj !== $v) {
            $this->cpf_cnpj = $v;
            $this->modifiedColumns[PessoaTableMap::COL_CPF_CNPJ] = true;
        }

        return $this;
    } // setCpfCnpj()

    /**
     * Set the value of [rg_inscrest] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setRgInscrest($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->rg_inscrest !== $v) {
            $this->rg_inscrest = $v;
            $this->modifiedColumns[PessoaTableMap::COL_RG_INSCREST] = true;
        }

        return $this;
    } // setRgInscrest()

    /**
     * Set the value of [ponto_referencia] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setPontoReferencia($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ponto_referencia !== $v) {
            $this->ponto_referencia = $v;
            $this->modifiedColumns[PessoaTableMap::COL_PONTO_REFERENCIA] = true;
        }

        return $this;
    } // setPontoReferencia()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[PessoaTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [complemento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setComplemento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->complemento !== $v) {
            $this->complemento = $v;
            $this->modifiedColumns[PessoaTableMap::COL_COMPLEMENTO] = true;
        }

        return $this;
    } // setComplemento()

    /**
     * Set the value of [tipo_pessoa] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setTipoPessoa($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_pessoa !== $v) {
            $this->tipo_pessoa = $v;
            $this->modifiedColumns[PessoaTableMap::COL_TIPO_PESSOA] = true;
        }

        return $this;
    } // setTipoPessoa()

    /**
     * Sets the value of [data_nascimento] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setDataNascimento($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_nascimento !== null || $dt !== null) {
            if ($this->data_nascimento === null || $dt === null || $dt->format("Y-m-d") !== $this->data_nascimento->format("Y-m-d")) {
                $this->data_nascimento = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PessoaTableMap::COL_DATA_NASCIMENTO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataNascimento()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ( ($dt != $this->data_cadastro) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === NULL) // or the entered value matches the default
                 ) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PessoaTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Set the value of [obs] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setObs($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->obs !== $v) {
            $this->obs = $v;
            $this->modifiedColumns[PessoaTableMap::COL_OBS] = true;
        }

        return $this;
    } // setObs()

    /**
     * Set the value of [cidade_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setCidadeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cidade_id !== $v) {
            $this->cidade_id = $v;
            $this->modifiedColumns[PessoaTableMap::COL_CIDADE_ID] = true;
        }

        return $this;
    } // setCidadeId()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ( ($dt != $this->data_alterado) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === NULL) // or the entered value matches the default
                 ) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PessoaTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[PessoaTableMap::COL_USUARIO_ALTERADO] = true;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Set the value of [responsavel] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setResponsavel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->responsavel !== $v) {
            $this->responsavel = $v;
            $this->modifiedColumns[PessoaTableMap::COL_RESPONSAVEL] = true;
        }

        return $this;
    } // setResponsavel()

    /**
     * Set the value of [contato] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setContato($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->contato !== $v) {
            $this->contato = $v;
            $this->modifiedColumns[PessoaTableMap::COL_CONTATO] = true;
        }

        return $this;
    } // setContato()

    /**
     * Set the value of [sexo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setSexo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sexo !== $v) {
            $this->sexo = $v;
            $this->modifiedColumns[PessoaTableMap::COL_SEXO] = true;
        }

        return $this;
    } // setSexo()

    /**
     * Set the value of [import_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setImportId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->import_id !== $v) {
            $this->import_id = $v;
            $this->modifiedColumns[PessoaTableMap::COL_IMPORT_ID] = true;
        }

        return $this;
    } // setImportId()

    /**
     * Sets the value of the [e_fornecedor] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function setEFornecedor($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->e_fornecedor !== $v) {
            $this->e_fornecedor = $v;
            $this->modifiedColumns[PessoaTableMap::COL_E_FORNECEDOR] = true;
        }

        return $this;
    } // setEFornecedor()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->data_cadastro && $this->data_cadastro->format('Y-m-d H:i:s.u') !== NULL) {
                return false;
            }

            if ($this->cidade_id !== 1) {
                return false;
            }

            if ($this->data_alterado && $this->data_alterado->format('Y-m-d H:i:s.u') !== NULL) {
                return false;
            }

            if ($this->usuario_alterado !== 1) {
                return false;
            }

            if ($this->sexo !== 'm') {
                return false;
            }

            if ($this->e_fornecedor !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PessoaTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PessoaTableMap::translateFieldName('Nome', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nome = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PessoaTableMap::translateFieldName('RazaoSocial', TableMap::TYPE_PHPNAME, $indexType)];
            $this->razao_social = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PessoaTableMap::translateFieldName('Rua', TableMap::TYPE_PHPNAME, $indexType)];
            $this->rua = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PessoaTableMap::translateFieldName('Bairro', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bairro = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PessoaTableMap::translateFieldName('Cep', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cep = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PessoaTableMap::translateFieldName('Num', TableMap::TYPE_PHPNAME, $indexType)];
            $this->num = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PessoaTableMap::translateFieldName('Uf', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uf = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PessoaTableMap::translateFieldName('CpfCnpj', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cpf_cnpj = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PessoaTableMap::translateFieldName('RgInscrest', TableMap::TYPE_PHPNAME, $indexType)];
            $this->rg_inscrest = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : PessoaTableMap::translateFieldName('PontoReferencia', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ponto_referencia = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : PessoaTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : PessoaTableMap::translateFieldName('Complemento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->complemento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : PessoaTableMap::translateFieldName('TipoPessoa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_pessoa = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : PessoaTableMap::translateFieldName('DataNascimento', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->data_nascimento = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : PessoaTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : PessoaTableMap::translateFieldName('Obs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->obs = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : PessoaTableMap::translateFieldName('CidadeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cidade_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : PessoaTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : PessoaTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : PessoaTableMap::translateFieldName('Responsavel', TableMap::TYPE_PHPNAME, $indexType)];
            $this->responsavel = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : PessoaTableMap::translateFieldName('Contato', TableMap::TYPE_PHPNAME, $indexType)];
            $this->contato = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : PessoaTableMap::translateFieldName('Sexo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sexo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : PessoaTableMap::translateFieldName('ImportId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->import_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : PessoaTableMap::translateFieldName('EFornecedor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->e_fornecedor = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 25; // 25 = PessoaTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Pessoa'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PessoaTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPessoaQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collBoletoBaixaHistoricos = null;

            $this->collClientes = null;

            $this->collDadosFiscals = null;

            $this->collEmpresas = null;

            $this->collFornecedors = null;

            $this->collTelefones = null;

            $this->collUsuarios = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Pessoa::setDeleted()
     * @see Pessoa::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PessoaTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPessoaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PessoaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PessoaTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->boletoBaixaHistoricosScheduledForDeletion !== null) {
                if (!$this->boletoBaixaHistoricosScheduledForDeletion->isEmpty()) {
                    foreach ($this->boletoBaixaHistoricosScheduledForDeletion as $boletoBaixaHistorico) {
                        // need to save related object because we set the relation to null
                        $boletoBaixaHistorico->save($con);
                    }
                    $this->boletoBaixaHistoricosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletoBaixaHistoricos !== null) {
                foreach ($this->collBoletoBaixaHistoricos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->clientesScheduledForDeletion !== null) {
                if (!$this->clientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ClienteQuery::create()
                        ->filterByPrimaryKeys($this->clientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->clientesScheduledForDeletion = null;
                }
            }

            if ($this->collClientes !== null) {
                foreach ($this->collClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dadosFiscalsScheduledForDeletion !== null) {
                if (!$this->dadosFiscalsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\DadosFiscalQuery::create()
                        ->filterByPrimaryKeys($this->dadosFiscalsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dadosFiscalsScheduledForDeletion = null;
                }
            }

            if ($this->collDadosFiscals !== null) {
                foreach ($this->collDadosFiscals as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->empresasScheduledForDeletion !== null) {
                if (!$this->empresasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EmpresaQuery::create()
                        ->filterByPrimaryKeys($this->empresasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->empresasScheduledForDeletion = null;
                }
            }

            if ($this->collEmpresas !== null) {
                foreach ($this->collEmpresas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->fornecedorsScheduledForDeletion !== null) {
                if (!$this->fornecedorsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\FornecedorQuery::create()
                        ->filterByPrimaryKeys($this->fornecedorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->fornecedorsScheduledForDeletion = null;
                }
            }

            if ($this->collFornecedors !== null) {
                foreach ($this->collFornecedors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->telefonesScheduledForDeletion !== null) {
                if (!$this->telefonesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\TelefoneQuery::create()
                        ->filterByPrimaryKeys($this->telefonesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->telefonesScheduledForDeletion = null;
                }
            }

            if ($this->collTelefones !== null) {
                foreach ($this->collTelefones as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuariosScheduledForDeletion !== null) {
                if (!$this->usuariosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\UsuarioQuery::create()
                        ->filterByPrimaryKeys($this->usuariosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usuariosScheduledForDeletion = null;
                }
            }

            if ($this->collUsuarios !== null) {
                foreach ($this->collUsuarios as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PessoaTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PessoaTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PessoaTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_NOME)) {
            $modifiedColumns[':p' . $index++]  = 'nome';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_RAZAO_SOCIAL)) {
            $modifiedColumns[':p' . $index++]  = 'razao_social';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_RUA)) {
            $modifiedColumns[':p' . $index++]  = 'rua';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_BAIRRO)) {
            $modifiedColumns[':p' . $index++]  = 'bairro';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_CEP)) {
            $modifiedColumns[':p' . $index++]  = 'cep';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_NUM)) {
            $modifiedColumns[':p' . $index++]  = 'num';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_UF)) {
            $modifiedColumns[':p' . $index++]  = 'uf';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_CPF_CNPJ)) {
            $modifiedColumns[':p' . $index++]  = 'cpf_cnpj';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_RG_INSCREST)) {
            $modifiedColumns[':p' . $index++]  = 'rg_inscrest';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_PONTO_REFERENCIA)) {
            $modifiedColumns[':p' . $index++]  = 'ponto_referencia';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_COMPLEMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'complemento';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_TIPO_PESSOA)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_pessoa';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_DATA_NASCIMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'data_nascimento';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_OBS)) {
            $modifiedColumns[':p' . $index++]  = 'obs';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_CIDADE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cidade_id';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_RESPONSAVEL)) {
            $modifiedColumns[':p' . $index++]  = 'responsavel';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_CONTATO)) {
            $modifiedColumns[':p' . $index++]  = 'contato';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_SEXO)) {
            $modifiedColumns[':p' . $index++]  = 'sexo';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_IMPORT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'import_id';
        }
        if ($this->isColumnModified(PessoaTableMap::COL_E_FORNECEDOR)) {
            $modifiedColumns[':p' . $index++]  = 'e_fornecedor';
        }

        $sql = sprintf(
            'INSERT INTO pessoa (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'nome':
                        $stmt->bindValue($identifier, $this->nome, PDO::PARAM_STR);
                        break;
                    case 'razao_social':
                        $stmt->bindValue($identifier, $this->razao_social, PDO::PARAM_STR);
                        break;
                    case 'rua':
                        $stmt->bindValue($identifier, $this->rua, PDO::PARAM_STR);
                        break;
                    case 'bairro':
                        $stmt->bindValue($identifier, $this->bairro, PDO::PARAM_STR);
                        break;
                    case 'cep':
                        $stmt->bindValue($identifier, $this->cep, PDO::PARAM_STR);
                        break;
                    case 'num':
                        $stmt->bindValue($identifier, $this->num, PDO::PARAM_INT);
                        break;
                    case 'uf':
                        $stmt->bindValue($identifier, $this->uf, PDO::PARAM_STR);
                        break;
                    case 'cpf_cnpj':
                        $stmt->bindValue($identifier, $this->cpf_cnpj, PDO::PARAM_STR);
                        break;
                    case 'rg_inscrest':
                        $stmt->bindValue($identifier, $this->rg_inscrest, PDO::PARAM_STR);
                        break;
                    case 'ponto_referencia':
                        $stmt->bindValue($identifier, $this->ponto_referencia, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'complemento':
                        $stmt->bindValue($identifier, $this->complemento, PDO::PARAM_STR);
                        break;
                    case 'tipo_pessoa':
                        $stmt->bindValue($identifier, $this->tipo_pessoa, PDO::PARAM_STR);
                        break;
                    case 'data_nascimento':
                        $stmt->bindValue($identifier, $this->data_nascimento ? $this->data_nascimento->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'obs':
                        $stmt->bindValue($identifier, $this->obs, PDO::PARAM_STR);
                        break;
                    case 'cidade_id':
                        $stmt->bindValue($identifier, $this->cidade_id, PDO::PARAM_INT);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'responsavel':
                        $stmt->bindValue($identifier, $this->responsavel, PDO::PARAM_STR);
                        break;
                    case 'contato':
                        $stmt->bindValue($identifier, $this->contato, PDO::PARAM_STR);
                        break;
                    case 'sexo':
                        $stmt->bindValue($identifier, $this->sexo, PDO::PARAM_STR);
                        break;
                    case 'import_id':
                        $stmt->bindValue($identifier, $this->import_id, PDO::PARAM_INT);
                        break;
                    case 'e_fornecedor':
                        $stmt->bindValue($identifier, (int) $this->e_fornecedor, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PessoaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNome();
                break;
            case 2:
                return $this->getRazaoSocial();
                break;
            case 3:
                return $this->getRua();
                break;
            case 4:
                return $this->getBairro();
                break;
            case 5:
                return $this->getCep();
                break;
            case 6:
                return $this->getNum();
                break;
            case 7:
                return $this->getUf();
                break;
            case 8:
                return $this->getCpfCnpj();
                break;
            case 9:
                return $this->getRgInscrest();
                break;
            case 10:
                return $this->getPontoReferencia();
                break;
            case 11:
                return $this->getEmail();
                break;
            case 12:
                return $this->getComplemento();
                break;
            case 13:
                return $this->getTipoPessoa();
                break;
            case 14:
                return $this->getDataNascimento();
                break;
            case 15:
                return $this->getDataCadastro();
                break;
            case 16:
                return $this->getObs();
                break;
            case 17:
                return $this->getCidadeId();
                break;
            case 18:
                return $this->getDataAlterado();
                break;
            case 19:
                return $this->getUsuarioAlterado();
                break;
            case 20:
                return $this->getResponsavel();
                break;
            case 21:
                return $this->getContato();
                break;
            case 22:
                return $this->getSexo();
                break;
            case 23:
                return $this->getImportId();
                break;
            case 24:
                return $this->getEFornecedor();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Pessoa'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Pessoa'][$this->hashCode()] = true;
        $keys = PessoaTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNome(),
            $keys[2] => $this->getRazaoSocial(),
            $keys[3] => $this->getRua(),
            $keys[4] => $this->getBairro(),
            $keys[5] => $this->getCep(),
            $keys[6] => $this->getNum(),
            $keys[7] => $this->getUf(),
            $keys[8] => $this->getCpfCnpj(),
            $keys[9] => $this->getRgInscrest(),
            $keys[10] => $this->getPontoReferencia(),
            $keys[11] => $this->getEmail(),
            $keys[12] => $this->getComplemento(),
            $keys[13] => $this->getTipoPessoa(),
            $keys[14] => $this->getDataNascimento(),
            $keys[15] => $this->getDataCadastro(),
            $keys[16] => $this->getObs(),
            $keys[17] => $this->getCidadeId(),
            $keys[18] => $this->getDataAlterado(),
            $keys[19] => $this->getUsuarioAlterado(),
            $keys[20] => $this->getResponsavel(),
            $keys[21] => $this->getContato(),
            $keys[22] => $this->getSexo(),
            $keys[23] => $this->getImportId(),
            $keys[24] => $this->getEFornecedor(),
        );
        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        if ($result[$keys[15]] instanceof \DateTime) {
            $result[$keys[15]] = $result[$keys[15]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collBoletoBaixaHistoricos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoBaixaHistoricos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_baixa_historicos';
                        break;
                    default:
                        $key = 'BoletoBaixaHistoricos';
                }

                $result[$key] = $this->collBoletoBaixaHistoricos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'clientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'clientes';
                        break;
                    default:
                        $key = 'Clientes';
                }

                $result[$key] = $this->collClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDadosFiscals) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dadosFiscals';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'dados_fiscals';
                        break;
                    default:
                        $key = 'DadosFiscals';
                }

                $result[$key] = $this->collDadosFiscals->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEmpresas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'empresas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'empresas';
                        break;
                    default:
                        $key = 'Empresas';
                }

                $result[$key] = $this->collEmpresas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFornecedors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'fornecedors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'fornecedors';
                        break;
                    default:
                        $key = 'Fornecedors';
                }

                $result[$key] = $this->collFornecedors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collTelefones) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'telefones';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'telefones';
                        break;
                    default:
                        $key = 'Telefones';
                }

                $result[$key] = $this->collTelefones->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuarios) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarios';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuarios';
                        break;
                    default:
                        $key = 'Usuarios';
                }

                $result[$key] = $this->collUsuarios->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Pessoa
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PessoaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Pessoa
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNome($value);
                break;
            case 2:
                $this->setRazaoSocial($value);
                break;
            case 3:
                $this->setRua($value);
                break;
            case 4:
                $this->setBairro($value);
                break;
            case 5:
                $this->setCep($value);
                break;
            case 6:
                $this->setNum($value);
                break;
            case 7:
                $this->setUf($value);
                break;
            case 8:
                $this->setCpfCnpj($value);
                break;
            case 9:
                $this->setRgInscrest($value);
                break;
            case 10:
                $this->setPontoReferencia($value);
                break;
            case 11:
                $this->setEmail($value);
                break;
            case 12:
                $this->setComplemento($value);
                break;
            case 13:
                $this->setTipoPessoa($value);
                break;
            case 14:
                $this->setDataNascimento($value);
                break;
            case 15:
                $this->setDataCadastro($value);
                break;
            case 16:
                $this->setObs($value);
                break;
            case 17:
                $this->setCidadeId($value);
                break;
            case 18:
                $this->setDataAlterado($value);
                break;
            case 19:
                $this->setUsuarioAlterado($value);
                break;
            case 20:
                $this->setResponsavel($value);
                break;
            case 21:
                $this->setContato($value);
                break;
            case 22:
                $this->setSexo($value);
                break;
            case 23:
                $this->setImportId($value);
                break;
            case 24:
                $this->setEFornecedor($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PessoaTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNome($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setRazaoSocial($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setRua($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setBairro($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCep($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setNum($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setUf($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setCpfCnpj($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setRgInscrest($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPontoReferencia($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setEmail($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setComplemento($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setTipoPessoa($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setDataNascimento($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setDataCadastro($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setObs($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setCidadeId($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setDataAlterado($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setResponsavel($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setContato($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setSexo($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setImportId($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setEFornecedor($arr[$keys[24]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PessoaTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PessoaTableMap::COL_ID)) {
            $criteria->add(PessoaTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_NOME)) {
            $criteria->add(PessoaTableMap::COL_NOME, $this->nome);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_RAZAO_SOCIAL)) {
            $criteria->add(PessoaTableMap::COL_RAZAO_SOCIAL, $this->razao_social);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_RUA)) {
            $criteria->add(PessoaTableMap::COL_RUA, $this->rua);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_BAIRRO)) {
            $criteria->add(PessoaTableMap::COL_BAIRRO, $this->bairro);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_CEP)) {
            $criteria->add(PessoaTableMap::COL_CEP, $this->cep);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_NUM)) {
            $criteria->add(PessoaTableMap::COL_NUM, $this->num);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_UF)) {
            $criteria->add(PessoaTableMap::COL_UF, $this->uf);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_CPF_CNPJ)) {
            $criteria->add(PessoaTableMap::COL_CPF_CNPJ, $this->cpf_cnpj);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_RG_INSCREST)) {
            $criteria->add(PessoaTableMap::COL_RG_INSCREST, $this->rg_inscrest);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_PONTO_REFERENCIA)) {
            $criteria->add(PessoaTableMap::COL_PONTO_REFERENCIA, $this->ponto_referencia);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_EMAIL)) {
            $criteria->add(PessoaTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_COMPLEMENTO)) {
            $criteria->add(PessoaTableMap::COL_COMPLEMENTO, $this->complemento);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_TIPO_PESSOA)) {
            $criteria->add(PessoaTableMap::COL_TIPO_PESSOA, $this->tipo_pessoa);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_DATA_NASCIMENTO)) {
            $criteria->add(PessoaTableMap::COL_DATA_NASCIMENTO, $this->data_nascimento);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(PessoaTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_OBS)) {
            $criteria->add(PessoaTableMap::COL_OBS, $this->obs);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_CIDADE_ID)) {
            $criteria->add(PessoaTableMap::COL_CIDADE_ID, $this->cidade_id);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(PessoaTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(PessoaTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_RESPONSAVEL)) {
            $criteria->add(PessoaTableMap::COL_RESPONSAVEL, $this->responsavel);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_CONTATO)) {
            $criteria->add(PessoaTableMap::COL_CONTATO, $this->contato);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_SEXO)) {
            $criteria->add(PessoaTableMap::COL_SEXO, $this->sexo);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_IMPORT_ID)) {
            $criteria->add(PessoaTableMap::COL_IMPORT_ID, $this->import_id);
        }
        if ($this->isColumnModified(PessoaTableMap::COL_E_FORNECEDOR)) {
            $criteria->add(PessoaTableMap::COL_E_FORNECEDOR, $this->e_fornecedor);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPessoaQuery::create();
        $criteria->add(PessoaTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Pessoa (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNome($this->getNome());
        $copyObj->setRazaoSocial($this->getRazaoSocial());
        $copyObj->setRua($this->getRua());
        $copyObj->setBairro($this->getBairro());
        $copyObj->setCep($this->getCep());
        $copyObj->setNum($this->getNum());
        $copyObj->setUf($this->getUf());
        $copyObj->setCpfCnpj($this->getCpfCnpj());
        $copyObj->setRgInscrest($this->getRgInscrest());
        $copyObj->setPontoReferencia($this->getPontoReferencia());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setComplemento($this->getComplemento());
        $copyObj->setTipoPessoa($this->getTipoPessoa());
        $copyObj->setDataNascimento($this->getDataNascimento());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setObs($this->getObs());
        $copyObj->setCidadeId($this->getCidadeId());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setResponsavel($this->getResponsavel());
        $copyObj->setContato($this->getContato());
        $copyObj->setSexo($this->getSexo());
        $copyObj->setImportId($this->getImportId());
        $copyObj->setEFornecedor($this->getEFornecedor());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBoletoBaixaHistoricos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoletoBaixaHistorico($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCliente($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDadosFiscals() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDadosFiscal($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEmpresas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEmpresa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFornecedors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFornecedor($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getTelefones() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addTelefone($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuarios() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuario($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Pessoa Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('BoletoBaixaHistorico' == $relationName) {
            return $this->initBoletoBaixaHistoricos();
        }
        if ('Cliente' == $relationName) {
            return $this->initClientes();
        }
        if ('DadosFiscal' == $relationName) {
            return $this->initDadosFiscals();
        }
        if ('Empresa' == $relationName) {
            return $this->initEmpresas();
        }
        if ('Fornecedor' == $relationName) {
            return $this->initFornecedors();
        }
        if ('Telefone' == $relationName) {
            return $this->initTelefones();
        }
        if ('Usuario' == $relationName) {
            return $this->initUsuarios();
        }
    }

    /**
     * Clears out the collBoletoBaixaHistoricos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletoBaixaHistoricos()
     */
    public function clearBoletoBaixaHistoricos()
    {
        $this->collBoletoBaixaHistoricos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletoBaixaHistoricos collection loaded partially.
     */
    public function resetPartialBoletoBaixaHistoricos($v = true)
    {
        $this->collBoletoBaixaHistoricosPartial = $v;
    }

    /**
     * Initializes the collBoletoBaixaHistoricos collection.
     *
     * By default this just sets the collBoletoBaixaHistoricos collection to an empty array (like clearcollBoletoBaixaHistoricos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletoBaixaHistoricos($overrideExisting = true)
    {
        if (null !== $this->collBoletoBaixaHistoricos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoBaixaHistoricoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletoBaixaHistoricos = new $collectionClassName;
        $this->collBoletoBaixaHistoricos->setModel('\ImaTelecomBundle\Model\BoletoBaixaHistorico');
    }

    /**
     * Gets an array of ChildBoletoBaixaHistorico objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPessoa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     * @throws PropelException
     */
    public function getBoletoBaixaHistoricos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                // return empty collection
                $this->initBoletoBaixaHistoricos();
            } else {
                $collBoletoBaixaHistoricos = ChildBoletoBaixaHistoricoQuery::create(null, $criteria)
                    ->filterByPessoa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletoBaixaHistoricosPartial && count($collBoletoBaixaHistoricos)) {
                        $this->initBoletoBaixaHistoricos(false);

                        foreach ($collBoletoBaixaHistoricos as $obj) {
                            if (false == $this->collBoletoBaixaHistoricos->contains($obj)) {
                                $this->collBoletoBaixaHistoricos->append($obj);
                            }
                        }

                        $this->collBoletoBaixaHistoricosPartial = true;
                    }

                    return $collBoletoBaixaHistoricos;
                }

                if ($partial && $this->collBoletoBaixaHistoricos) {
                    foreach ($this->collBoletoBaixaHistoricos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletoBaixaHistoricos[] = $obj;
                        }
                    }
                }

                $this->collBoletoBaixaHistoricos = $collBoletoBaixaHistoricos;
                $this->collBoletoBaixaHistoricosPartial = false;
            }
        }

        return $this->collBoletoBaixaHistoricos;
    }

    /**
     * Sets a collection of ChildBoletoBaixaHistorico objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletoBaixaHistoricos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function setBoletoBaixaHistoricos(Collection $boletoBaixaHistoricos, ConnectionInterface $con = null)
    {
        /** @var ChildBoletoBaixaHistorico[] $boletoBaixaHistoricosToDelete */
        $boletoBaixaHistoricosToDelete = $this->getBoletoBaixaHistoricos(new Criteria(), $con)->diff($boletoBaixaHistoricos);


        $this->boletoBaixaHistoricosScheduledForDeletion = $boletoBaixaHistoricosToDelete;

        foreach ($boletoBaixaHistoricosToDelete as $boletoBaixaHistoricoRemoved) {
            $boletoBaixaHistoricoRemoved->setPessoa(null);
        }

        $this->collBoletoBaixaHistoricos = null;
        foreach ($boletoBaixaHistoricos as $boletoBaixaHistorico) {
            $this->addBoletoBaixaHistorico($boletoBaixaHistorico);
        }

        $this->collBoletoBaixaHistoricos = $boletoBaixaHistoricos;
        $this->collBoletoBaixaHistoricosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BoletoBaixaHistorico objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BoletoBaixaHistorico objects.
     * @throws PropelException
     */
    public function countBoletoBaixaHistoricos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletoBaixaHistoricos());
            }

            $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPessoa($this)
                ->count($con);
        }

        return count($this->collBoletoBaixaHistoricos);
    }

    /**
     * Method called to associate a ChildBoletoBaixaHistorico object to this object
     * through the ChildBoletoBaixaHistorico foreign key attribute.
     *
     * @param  ChildBoletoBaixaHistorico $l ChildBoletoBaixaHistorico
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function addBoletoBaixaHistorico(ChildBoletoBaixaHistorico $l)
    {
        if ($this->collBoletoBaixaHistoricos === null) {
            $this->initBoletoBaixaHistoricos();
            $this->collBoletoBaixaHistoricosPartial = true;
        }

        if (!$this->collBoletoBaixaHistoricos->contains($l)) {
            $this->doAddBoletoBaixaHistorico($l);

            if ($this->boletoBaixaHistoricosScheduledForDeletion and $this->boletoBaixaHistoricosScheduledForDeletion->contains($l)) {
                $this->boletoBaixaHistoricosScheduledForDeletion->remove($this->boletoBaixaHistoricosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to add.
     */
    protected function doAddBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        $this->collBoletoBaixaHistoricos[]= $boletoBaixaHistorico;
        $boletoBaixaHistorico->setPessoa($this);
    }

    /**
     * @param  ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to remove.
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function removeBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        if ($this->getBoletoBaixaHistoricos()->contains($boletoBaixaHistorico)) {
            $pos = $this->collBoletoBaixaHistoricos->search($boletoBaixaHistorico);
            $this->collBoletoBaixaHistoricos->remove($pos);
            if (null === $this->boletoBaixaHistoricosScheduledForDeletion) {
                $this->boletoBaixaHistoricosScheduledForDeletion = clone $this->collBoletoBaixaHistoricos;
                $this->boletoBaixaHistoricosScheduledForDeletion->clear();
            }
            $this->boletoBaixaHistoricosScheduledForDeletion[]= $boletoBaixaHistorico;
            $boletoBaixaHistorico->setPessoa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixaEstorno(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('BaixaEstorno', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinLancamentos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Lancamentos', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }

    /**
     * Clears out the collClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addClientes()
     */
    public function clearClientes()
    {
        $this->collClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collClientes collection loaded partially.
     */
    public function resetPartialClientes($v = true)
    {
        $this->collClientesPartial = $v;
    }

    /**
     * Initializes the collClientes collection.
     *
     * By default this just sets the collClientes collection to an empty array (like clearcollClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initClientes($overrideExisting = true)
    {
        if (null !== $this->collClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = ClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collClientes = new $collectionClassName;
        $this->collClientes->setModel('\ImaTelecomBundle\Model\Cliente');
    }

    /**
     * Gets an array of ChildCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPessoa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCliente[] List of ChildCliente objects
     * @throws PropelException
     */
    public function getClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collClientesPartial && !$this->isNew();
        if (null === $this->collClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collClientes) {
                // return empty collection
                $this->initClientes();
            } else {
                $collClientes = ChildClienteQuery::create(null, $criteria)
                    ->filterByPessoa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collClientesPartial && count($collClientes)) {
                        $this->initClientes(false);

                        foreach ($collClientes as $obj) {
                            if (false == $this->collClientes->contains($obj)) {
                                $this->collClientes->append($obj);
                            }
                        }

                        $this->collClientesPartial = true;
                    }

                    return $collClientes;
                }

                if ($partial && $this->collClientes) {
                    foreach ($this->collClientes as $obj) {
                        if ($obj->isNew()) {
                            $collClientes[] = $obj;
                        }
                    }
                }

                $this->collClientes = $collClientes;
                $this->collClientesPartial = false;
            }
        }

        return $this->collClientes;
    }

    /**
     * Sets a collection of ChildCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $clientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function setClientes(Collection $clientes, ConnectionInterface $con = null)
    {
        /** @var ChildCliente[] $clientesToDelete */
        $clientesToDelete = $this->getClientes(new Criteria(), $con)->diff($clientes);


        $this->clientesScheduledForDeletion = $clientesToDelete;

        foreach ($clientesToDelete as $clienteRemoved) {
            $clienteRemoved->setPessoa(null);
        }

        $this->collClientes = null;
        foreach ($clientes as $cliente) {
            $this->addCliente($cliente);
        }

        $this->collClientes = $clientes;
        $this->collClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Cliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Cliente objects.
     * @throws PropelException
     */
    public function countClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collClientesPartial && !$this->isNew();
        if (null === $this->collClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getClientes());
            }

            $query = ChildClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPessoa($this)
                ->count($con);
        }

        return count($this->collClientes);
    }

    /**
     * Method called to associate a ChildCliente object to this object
     * through the ChildCliente foreign key attribute.
     *
     * @param  ChildCliente $l ChildCliente
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function addCliente(ChildCliente $l)
    {
        if ($this->collClientes === null) {
            $this->initClientes();
            $this->collClientesPartial = true;
        }

        if (!$this->collClientes->contains($l)) {
            $this->doAddCliente($l);

            if ($this->clientesScheduledForDeletion and $this->clientesScheduledForDeletion->contains($l)) {
                $this->clientesScheduledForDeletion->remove($this->clientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCliente $cliente The ChildCliente object to add.
     */
    protected function doAddCliente(ChildCliente $cliente)
    {
        $this->collClientes[]= $cliente;
        $cliente->setPessoa($this);
    }

    /**
     * @param  ChildCliente $cliente The ChildCliente object to remove.
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function removeCliente(ChildCliente $cliente)
    {
        if ($this->getClientes()->contains($cliente)) {
            $pos = $this->collClientes->search($cliente);
            $this->collClientes->remove($pos);
            if (null === $this->clientesScheduledForDeletion) {
                $this->clientesScheduledForDeletion = clone $this->collClientes;
                $this->clientesScheduledForDeletion->clear();
            }
            $this->clientesScheduledForDeletion[]= clone $cliente;
            $cliente->setPessoa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related Clientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCliente[] List of ChildCliente objects
     */
    public function getClientesJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildClienteQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getClientes($query, $con);
    }

    /**
     * Clears out the collDadosFiscals collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDadosFiscals()
     */
    public function clearDadosFiscals()
    {
        $this->collDadosFiscals = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDadosFiscals collection loaded partially.
     */
    public function resetPartialDadosFiscals($v = true)
    {
        $this->collDadosFiscalsPartial = $v;
    }

    /**
     * Initializes the collDadosFiscals collection.
     *
     * By default this just sets the collDadosFiscals collection to an empty array (like clearcollDadosFiscals());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDadosFiscals($overrideExisting = true)
    {
        if (null !== $this->collDadosFiscals && !$overrideExisting) {
            return;
        }

        $collectionClassName = DadosFiscalTableMap::getTableMap()->getCollectionClassName();

        $this->collDadosFiscals = new $collectionClassName;
        $this->collDadosFiscals->setModel('\ImaTelecomBundle\Model\DadosFiscal');
    }

    /**
     * Gets an array of ChildDadosFiscal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPessoa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDadosFiscal[] List of ChildDadosFiscal objects
     * @throws PropelException
     */
    public function getDadosFiscals(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalsPartial && !$this->isNew();
        if (null === $this->collDadosFiscals || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscals) {
                // return empty collection
                $this->initDadosFiscals();
            } else {
                $collDadosFiscals = ChildDadosFiscalQuery::create(null, $criteria)
                    ->filterByPessoa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDadosFiscalsPartial && count($collDadosFiscals)) {
                        $this->initDadosFiscals(false);

                        foreach ($collDadosFiscals as $obj) {
                            if (false == $this->collDadosFiscals->contains($obj)) {
                                $this->collDadosFiscals->append($obj);
                            }
                        }

                        $this->collDadosFiscalsPartial = true;
                    }

                    return $collDadosFiscals;
                }

                if ($partial && $this->collDadosFiscals) {
                    foreach ($this->collDadosFiscals as $obj) {
                        if ($obj->isNew()) {
                            $collDadosFiscals[] = $obj;
                        }
                    }
                }

                $this->collDadosFiscals = $collDadosFiscals;
                $this->collDadosFiscalsPartial = false;
            }
        }

        return $this->collDadosFiscals;
    }

    /**
     * Sets a collection of ChildDadosFiscal objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $dadosFiscals A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function setDadosFiscals(Collection $dadosFiscals, ConnectionInterface $con = null)
    {
        /** @var ChildDadosFiscal[] $dadosFiscalsToDelete */
        $dadosFiscalsToDelete = $this->getDadosFiscals(new Criteria(), $con)->diff($dadosFiscals);


        $this->dadosFiscalsScheduledForDeletion = $dadosFiscalsToDelete;

        foreach ($dadosFiscalsToDelete as $dadosFiscalRemoved) {
            $dadosFiscalRemoved->setPessoa(null);
        }

        $this->collDadosFiscals = null;
        foreach ($dadosFiscals as $dadosFiscal) {
            $this->addDadosFiscal($dadosFiscal);
        }

        $this->collDadosFiscals = $dadosFiscals;
        $this->collDadosFiscalsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DadosFiscal objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related DadosFiscal objects.
     * @throws PropelException
     */
    public function countDadosFiscals(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalsPartial && !$this->isNew();
        if (null === $this->collDadosFiscals || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscals) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDadosFiscals());
            }

            $query = ChildDadosFiscalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPessoa($this)
                ->count($con);
        }

        return count($this->collDadosFiscals);
    }

    /**
     * Method called to associate a ChildDadosFiscal object to this object
     * through the ChildDadosFiscal foreign key attribute.
     *
     * @param  ChildDadosFiscal $l ChildDadosFiscal
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function addDadosFiscal(ChildDadosFiscal $l)
    {
        if ($this->collDadosFiscals === null) {
            $this->initDadosFiscals();
            $this->collDadosFiscalsPartial = true;
        }

        if (!$this->collDadosFiscals->contains($l)) {
            $this->doAddDadosFiscal($l);

            if ($this->dadosFiscalsScheduledForDeletion and $this->dadosFiscalsScheduledForDeletion->contains($l)) {
                $this->dadosFiscalsScheduledForDeletion->remove($this->dadosFiscalsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDadosFiscal $dadosFiscal The ChildDadosFiscal object to add.
     */
    protected function doAddDadosFiscal(ChildDadosFiscal $dadosFiscal)
    {
        $this->collDadosFiscals[]= $dadosFiscal;
        $dadosFiscal->setPessoa($this);
    }

    /**
     * @param  ChildDadosFiscal $dadosFiscal The ChildDadosFiscal object to remove.
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function removeDadosFiscal(ChildDadosFiscal $dadosFiscal)
    {
        if ($this->getDadosFiscals()->contains($dadosFiscal)) {
            $pos = $this->collDadosFiscals->search($dadosFiscal);
            $this->collDadosFiscals->remove($pos);
            if (null === $this->dadosFiscalsScheduledForDeletion) {
                $this->dadosFiscalsScheduledForDeletion = clone $this->collDadosFiscals;
                $this->dadosFiscalsScheduledForDeletion->clear();
            }
            $this->dadosFiscalsScheduledForDeletion[]= clone $dadosFiscal;
            $dadosFiscal->setPessoa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related DadosFiscals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscal[] List of ChildDadosFiscal objects
     */
    public function getDadosFiscalsJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getDadosFiscals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related DadosFiscals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscal[] List of ChildDadosFiscal objects
     */
    public function getDadosFiscalsJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getDadosFiscals($query, $con);
    }

    /**
     * Clears out the collEmpresas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEmpresas()
     */
    public function clearEmpresas()
    {
        $this->collEmpresas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEmpresas collection loaded partially.
     */
    public function resetPartialEmpresas($v = true)
    {
        $this->collEmpresasPartial = $v;
    }

    /**
     * Initializes the collEmpresas collection.
     *
     * By default this just sets the collEmpresas collection to an empty array (like clearcollEmpresas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEmpresas($overrideExisting = true)
    {
        if (null !== $this->collEmpresas && !$overrideExisting) {
            return;
        }

        $collectionClassName = EmpresaTableMap::getTableMap()->getCollectionClassName();

        $this->collEmpresas = new $collectionClassName;
        $this->collEmpresas->setModel('\ImaTelecomBundle\Model\Empresa');
    }

    /**
     * Gets an array of ChildEmpresa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPessoa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEmpresa[] List of ChildEmpresa objects
     * @throws PropelException
     */
    public function getEmpresas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEmpresasPartial && !$this->isNew();
        if (null === $this->collEmpresas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEmpresas) {
                // return empty collection
                $this->initEmpresas();
            } else {
                $collEmpresas = ChildEmpresaQuery::create(null, $criteria)
                    ->filterByPessoa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEmpresasPartial && count($collEmpresas)) {
                        $this->initEmpresas(false);

                        foreach ($collEmpresas as $obj) {
                            if (false == $this->collEmpresas->contains($obj)) {
                                $this->collEmpresas->append($obj);
                            }
                        }

                        $this->collEmpresasPartial = true;
                    }

                    return $collEmpresas;
                }

                if ($partial && $this->collEmpresas) {
                    foreach ($this->collEmpresas as $obj) {
                        if ($obj->isNew()) {
                            $collEmpresas[] = $obj;
                        }
                    }
                }

                $this->collEmpresas = $collEmpresas;
                $this->collEmpresasPartial = false;
            }
        }

        return $this->collEmpresas;
    }

    /**
     * Sets a collection of ChildEmpresa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $empresas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function setEmpresas(Collection $empresas, ConnectionInterface $con = null)
    {
        /** @var ChildEmpresa[] $empresasToDelete */
        $empresasToDelete = $this->getEmpresas(new Criteria(), $con)->diff($empresas);


        $this->empresasScheduledForDeletion = $empresasToDelete;

        foreach ($empresasToDelete as $empresaRemoved) {
            $empresaRemoved->setPessoa(null);
        }

        $this->collEmpresas = null;
        foreach ($empresas as $empresa) {
            $this->addEmpresa($empresa);
        }

        $this->collEmpresas = $empresas;
        $this->collEmpresasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Empresa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Empresa objects.
     * @throws PropelException
     */
    public function countEmpresas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEmpresasPartial && !$this->isNew();
        if (null === $this->collEmpresas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEmpresas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEmpresas());
            }

            $query = ChildEmpresaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPessoa($this)
                ->count($con);
        }

        return count($this->collEmpresas);
    }

    /**
     * Method called to associate a ChildEmpresa object to this object
     * through the ChildEmpresa foreign key attribute.
     *
     * @param  ChildEmpresa $l ChildEmpresa
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function addEmpresa(ChildEmpresa $l)
    {
        if ($this->collEmpresas === null) {
            $this->initEmpresas();
            $this->collEmpresasPartial = true;
        }

        if (!$this->collEmpresas->contains($l)) {
            $this->doAddEmpresa($l);

            if ($this->empresasScheduledForDeletion and $this->empresasScheduledForDeletion->contains($l)) {
                $this->empresasScheduledForDeletion->remove($this->empresasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEmpresa $empresa The ChildEmpresa object to add.
     */
    protected function doAddEmpresa(ChildEmpresa $empresa)
    {
        $this->collEmpresas[]= $empresa;
        $empresa->setPessoa($this);
    }

    /**
     * @param  ChildEmpresa $empresa The ChildEmpresa object to remove.
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function removeEmpresa(ChildEmpresa $empresa)
    {
        if ($this->getEmpresas()->contains($empresa)) {
            $pos = $this->collEmpresas->search($empresa);
            $this->collEmpresas->remove($pos);
            if (null === $this->empresasScheduledForDeletion) {
                $this->empresasScheduledForDeletion = clone $this->collEmpresas;
                $this->empresasScheduledForDeletion->clear();
            }
            $this->empresasScheduledForDeletion[]= clone $empresa;
            $empresa->setPessoa(null);
        }

        return $this;
    }

    /**
     * Clears out the collFornecedors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFornecedors()
     */
    public function clearFornecedors()
    {
        $this->collFornecedors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFornecedors collection loaded partially.
     */
    public function resetPartialFornecedors($v = true)
    {
        $this->collFornecedorsPartial = $v;
    }

    /**
     * Initializes the collFornecedors collection.
     *
     * By default this just sets the collFornecedors collection to an empty array (like clearcollFornecedors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFornecedors($overrideExisting = true)
    {
        if (null !== $this->collFornecedors && !$overrideExisting) {
            return;
        }

        $collectionClassName = FornecedorTableMap::getTableMap()->getCollectionClassName();

        $this->collFornecedors = new $collectionClassName;
        $this->collFornecedors->setModel('\ImaTelecomBundle\Model\Fornecedor');
    }

    /**
     * Gets an array of ChildFornecedor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPessoa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     * @throws PropelException
     */
    public function getFornecedors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFornecedorsPartial && !$this->isNew();
        if (null === $this->collFornecedors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFornecedors) {
                // return empty collection
                $this->initFornecedors();
            } else {
                $collFornecedors = ChildFornecedorQuery::create(null, $criteria)
                    ->filterByPessoa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFornecedorsPartial && count($collFornecedors)) {
                        $this->initFornecedors(false);

                        foreach ($collFornecedors as $obj) {
                            if (false == $this->collFornecedors->contains($obj)) {
                                $this->collFornecedors->append($obj);
                            }
                        }

                        $this->collFornecedorsPartial = true;
                    }

                    return $collFornecedors;
                }

                if ($partial && $this->collFornecedors) {
                    foreach ($this->collFornecedors as $obj) {
                        if ($obj->isNew()) {
                            $collFornecedors[] = $obj;
                        }
                    }
                }

                $this->collFornecedors = $collFornecedors;
                $this->collFornecedorsPartial = false;
            }
        }

        return $this->collFornecedors;
    }

    /**
     * Sets a collection of ChildFornecedor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $fornecedors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function setFornecedors(Collection $fornecedors, ConnectionInterface $con = null)
    {
        /** @var ChildFornecedor[] $fornecedorsToDelete */
        $fornecedorsToDelete = $this->getFornecedors(new Criteria(), $con)->diff($fornecedors);


        $this->fornecedorsScheduledForDeletion = $fornecedorsToDelete;

        foreach ($fornecedorsToDelete as $fornecedorRemoved) {
            $fornecedorRemoved->setPessoa(null);
        }

        $this->collFornecedors = null;
        foreach ($fornecedors as $fornecedor) {
            $this->addFornecedor($fornecedor);
        }

        $this->collFornecedors = $fornecedors;
        $this->collFornecedorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Fornecedor objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Fornecedor objects.
     * @throws PropelException
     */
    public function countFornecedors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFornecedorsPartial && !$this->isNew();
        if (null === $this->collFornecedors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFornecedors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFornecedors());
            }

            $query = ChildFornecedorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPessoa($this)
                ->count($con);
        }

        return count($this->collFornecedors);
    }

    /**
     * Method called to associate a ChildFornecedor object to this object
     * through the ChildFornecedor foreign key attribute.
     *
     * @param  ChildFornecedor $l ChildFornecedor
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function addFornecedor(ChildFornecedor $l)
    {
        if ($this->collFornecedors === null) {
            $this->initFornecedors();
            $this->collFornecedorsPartial = true;
        }

        if (!$this->collFornecedors->contains($l)) {
            $this->doAddFornecedor($l);

            if ($this->fornecedorsScheduledForDeletion and $this->fornecedorsScheduledForDeletion->contains($l)) {
                $this->fornecedorsScheduledForDeletion->remove($this->fornecedorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFornecedor $fornecedor The ChildFornecedor object to add.
     */
    protected function doAddFornecedor(ChildFornecedor $fornecedor)
    {
        $this->collFornecedors[]= $fornecedor;
        $fornecedor->setPessoa($this);
    }

    /**
     * @param  ChildFornecedor $fornecedor The ChildFornecedor object to remove.
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function removeFornecedor(ChildFornecedor $fornecedor)
    {
        if ($this->getFornecedors()->contains($fornecedor)) {
            $pos = $this->collFornecedors->search($fornecedor);
            $this->collFornecedors->remove($pos);
            if (null === $this->fornecedorsScheduledForDeletion) {
                $this->fornecedorsScheduledForDeletion = clone $this->collFornecedors;
                $this->fornecedorsScheduledForDeletion->clear();
            }
            $this->fornecedorsScheduledForDeletion[]= clone $fornecedor;
            $fornecedor->setPessoa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinBancoAgenciaConta(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('BancoAgenciaConta', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }

    /**
     * Clears out the collTelefones collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addTelefones()
     */
    public function clearTelefones()
    {
        $this->collTelefones = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collTelefones collection loaded partially.
     */
    public function resetPartialTelefones($v = true)
    {
        $this->collTelefonesPartial = $v;
    }

    /**
     * Initializes the collTelefones collection.
     *
     * By default this just sets the collTelefones collection to an empty array (like clearcollTelefones());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initTelefones($overrideExisting = true)
    {
        if (null !== $this->collTelefones && !$overrideExisting) {
            return;
        }

        $collectionClassName = TelefoneTableMap::getTableMap()->getCollectionClassName();

        $this->collTelefones = new $collectionClassName;
        $this->collTelefones->setModel('\ImaTelecomBundle\Model\Telefone');
    }

    /**
     * Gets an array of ChildTelefone objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPessoa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildTelefone[] List of ChildTelefone objects
     * @throws PropelException
     */
    public function getTelefones(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collTelefonesPartial && !$this->isNew();
        if (null === $this->collTelefones || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collTelefones) {
                // return empty collection
                $this->initTelefones();
            } else {
                $collTelefones = ChildTelefoneQuery::create(null, $criteria)
                    ->filterByPessoa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collTelefonesPartial && count($collTelefones)) {
                        $this->initTelefones(false);

                        foreach ($collTelefones as $obj) {
                            if (false == $this->collTelefones->contains($obj)) {
                                $this->collTelefones->append($obj);
                            }
                        }

                        $this->collTelefonesPartial = true;
                    }

                    return $collTelefones;
                }

                if ($partial && $this->collTelefones) {
                    foreach ($this->collTelefones as $obj) {
                        if ($obj->isNew()) {
                            $collTelefones[] = $obj;
                        }
                    }
                }

                $this->collTelefones = $collTelefones;
                $this->collTelefonesPartial = false;
            }
        }

        return $this->collTelefones;
    }

    /**
     * Sets a collection of ChildTelefone objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $telefones A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function setTelefones(Collection $telefones, ConnectionInterface $con = null)
    {
        /** @var ChildTelefone[] $telefonesToDelete */
        $telefonesToDelete = $this->getTelefones(new Criteria(), $con)->diff($telefones);


        $this->telefonesScheduledForDeletion = $telefonesToDelete;

        foreach ($telefonesToDelete as $telefoneRemoved) {
            $telefoneRemoved->setPessoa(null);
        }

        $this->collTelefones = null;
        foreach ($telefones as $telefone) {
            $this->addTelefone($telefone);
        }

        $this->collTelefones = $telefones;
        $this->collTelefonesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Telefone objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Telefone objects.
     * @throws PropelException
     */
    public function countTelefones(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collTelefonesPartial && !$this->isNew();
        if (null === $this->collTelefones || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collTelefones) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getTelefones());
            }

            $query = ChildTelefoneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPessoa($this)
                ->count($con);
        }

        return count($this->collTelefones);
    }

    /**
     * Method called to associate a ChildTelefone object to this object
     * through the ChildTelefone foreign key attribute.
     *
     * @param  ChildTelefone $l ChildTelefone
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function addTelefone(ChildTelefone $l)
    {
        if ($this->collTelefones === null) {
            $this->initTelefones();
            $this->collTelefonesPartial = true;
        }

        if (!$this->collTelefones->contains($l)) {
            $this->doAddTelefone($l);

            if ($this->telefonesScheduledForDeletion and $this->telefonesScheduledForDeletion->contains($l)) {
                $this->telefonesScheduledForDeletion->remove($this->telefonesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildTelefone $telefone The ChildTelefone object to add.
     */
    protected function doAddTelefone(ChildTelefone $telefone)
    {
        $this->collTelefones[]= $telefone;
        $telefone->setPessoa($this);
    }

    /**
     * @param  ChildTelefone $telefone The ChildTelefone object to remove.
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function removeTelefone(ChildTelefone $telefone)
    {
        if ($this->getTelefones()->contains($telefone)) {
            $pos = $this->collTelefones->search($telefone);
            $this->collTelefones->remove($pos);
            if (null === $this->telefonesScheduledForDeletion) {
                $this->telefonesScheduledForDeletion = clone $this->collTelefones;
                $this->telefonesScheduledForDeletion->clear();
            }
            $this->telefonesScheduledForDeletion[]= clone $telefone;
            $telefone->setPessoa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Pessoa is new, it will return
     * an empty collection; or if this Pessoa has previously
     * been saved, it will retrieve related Telefones from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Pessoa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildTelefone[] List of ChildTelefone objects
     */
    public function getTelefonesJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildTelefoneQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getTelefones($query, $con);
    }

    /**
     * Clears out the collUsuarios collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuarios()
     */
    public function clearUsuarios()
    {
        $this->collUsuarios = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuarios collection loaded partially.
     */
    public function resetPartialUsuarios($v = true)
    {
        $this->collUsuariosPartial = $v;
    }

    /**
     * Initializes the collUsuarios collection.
     *
     * By default this just sets the collUsuarios collection to an empty array (like clearcollUsuarios());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarios($overrideExisting = true)
    {
        if (null !== $this->collUsuarios && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuarios = new $collectionClassName;
        $this->collUsuarios->setModel('\ImaTelecomBundle\Model\Usuario');
    }

    /**
     * Gets an array of ChildUsuario objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPessoa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuario[] List of ChildUsuario objects
     * @throws PropelException
     */
    public function getUsuarios(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuariosPartial && !$this->isNew();
        if (null === $this->collUsuarios || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUsuarios) {
                // return empty collection
                $this->initUsuarios();
            } else {
                $collUsuarios = ChildUsuarioQuery::create(null, $criteria)
                    ->filterByPessoa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuariosPartial && count($collUsuarios)) {
                        $this->initUsuarios(false);

                        foreach ($collUsuarios as $obj) {
                            if (false == $this->collUsuarios->contains($obj)) {
                                $this->collUsuarios->append($obj);
                            }
                        }

                        $this->collUsuariosPartial = true;
                    }

                    return $collUsuarios;
                }

                if ($partial && $this->collUsuarios) {
                    foreach ($this->collUsuarios as $obj) {
                        if ($obj->isNew()) {
                            $collUsuarios[] = $obj;
                        }
                    }
                }

                $this->collUsuarios = $collUsuarios;
                $this->collUsuariosPartial = false;
            }
        }

        return $this->collUsuarios;
    }

    /**
     * Sets a collection of ChildUsuario objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuarios A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function setUsuarios(Collection $usuarios, ConnectionInterface $con = null)
    {
        /** @var ChildUsuario[] $usuariosToDelete */
        $usuariosToDelete = $this->getUsuarios(new Criteria(), $con)->diff($usuarios);


        $this->usuariosScheduledForDeletion = $usuariosToDelete;

        foreach ($usuariosToDelete as $usuarioRemoved) {
            $usuarioRemoved->setPessoa(null);
        }

        $this->collUsuarios = null;
        foreach ($usuarios as $usuario) {
            $this->addUsuario($usuario);
        }

        $this->collUsuarios = $usuarios;
        $this->collUsuariosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Usuario objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Usuario objects.
     * @throws PropelException
     */
    public function countUsuarios(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuariosPartial && !$this->isNew();
        if (null === $this->collUsuarios || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuarios) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuarios());
            }

            $query = ChildUsuarioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPessoa($this)
                ->count($con);
        }

        return count($this->collUsuarios);
    }

    /**
     * Method called to associate a ChildUsuario object to this object
     * through the ChildUsuario foreign key attribute.
     *
     * @param  ChildUsuario $l ChildUsuario
     * @return $this|\ImaTelecomBundle\Model\Pessoa The current object (for fluent API support)
     */
    public function addUsuario(ChildUsuario $l)
    {
        if ($this->collUsuarios === null) {
            $this->initUsuarios();
            $this->collUsuariosPartial = true;
        }

        if (!$this->collUsuarios->contains($l)) {
            $this->doAddUsuario($l);

            if ($this->usuariosScheduledForDeletion and $this->usuariosScheduledForDeletion->contains($l)) {
                $this->usuariosScheduledForDeletion->remove($this->usuariosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuario $usuario The ChildUsuario object to add.
     */
    protected function doAddUsuario(ChildUsuario $usuario)
    {
        $this->collUsuarios[]= $usuario;
        $usuario->setPessoa($this);
    }

    /**
     * @param  ChildUsuario $usuario The ChildUsuario object to remove.
     * @return $this|ChildPessoa The current object (for fluent API support)
     */
    public function removeUsuario(ChildUsuario $usuario)
    {
        if ($this->getUsuarios()->contains($usuario)) {
            $pos = $this->collUsuarios->search($usuario);
            $this->collUsuarios->remove($pos);
            if (null === $this->usuariosScheduledForDeletion) {
                $this->usuariosScheduledForDeletion = clone $this->collUsuarios;
                $this->usuariosScheduledForDeletion->clear();
            }
            $this->usuariosScheduledForDeletion[]= clone $usuario;
            $usuario->setPessoa(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->nome = null;
        $this->razao_social = null;
        $this->rua = null;
        $this->bairro = null;
        $this->cep = null;
        $this->num = null;
        $this->uf = null;
        $this->cpf_cnpj = null;
        $this->rg_inscrest = null;
        $this->ponto_referencia = null;
        $this->email = null;
        $this->complemento = null;
        $this->tipo_pessoa = null;
        $this->data_nascimento = null;
        $this->data_cadastro = null;
        $this->obs = null;
        $this->cidade_id = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->responsavel = null;
        $this->contato = null;
        $this->sexo = null;
        $this->import_id = null;
        $this->e_fornecedor = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBoletoBaixaHistoricos) {
                foreach ($this->collBoletoBaixaHistoricos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collClientes) {
                foreach ($this->collClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDadosFiscals) {
                foreach ($this->collDadosFiscals as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEmpresas) {
                foreach ($this->collEmpresas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFornecedors) {
                foreach ($this->collFornecedors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collTelefones) {
                foreach ($this->collTelefones as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuarios) {
                foreach ($this->collUsuarios as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBoletoBaixaHistoricos = null;
        $this->collClientes = null;
        $this->collDadosFiscals = null;
        $this->collEmpresas = null;
        $this->collFornecedors = null;
        $this->collTelefones = null;
        $this->collUsuarios = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PessoaTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
