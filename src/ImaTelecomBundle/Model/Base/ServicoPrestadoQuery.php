<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ServicoPrestado as ChildServicoPrestado;
use ImaTelecomBundle\Model\ServicoPrestadoQuery as ChildServicoPrestadoQuery;
use ImaTelecomBundle\Model\Map\ServicoPrestadoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'servico_prestado' table.
 *
 *
 *
 * @method     ChildServicoPrestadoQuery orderByIdservicoPrestado($order = Criteria::ASC) Order by the idservico_prestado column
 * @method     ChildServicoPrestadoQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildServicoPrestadoQuery orderByCodigo($order = Criteria::ASC) Order by the codigo column
 * @method     ChildServicoPrestadoQuery orderByDownload($order = Criteria::ASC) Order by the download column
 * @method     ChildServicoPrestadoQuery orderByUpload($order = Criteria::ASC) Order by the upload column
 * @method     ChildServicoPrestadoQuery orderByFranquia($order = Criteria::ASC) Order by the franquia column
 * @method     ChildServicoPrestadoQuery orderByTecnologia($order = Criteria::ASC) Order by the tecnologia column
 * @method     ChildServicoPrestadoQuery orderByGarantiaBanda($order = Criteria::ASC) Order by the garantia_banda column
 * @method     ChildServicoPrestadoQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildServicoPrestadoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildServicoPrestadoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildServicoPrestadoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildServicoPrestadoQuery orderByPrePago($order = Criteria::ASC) Order by the pre_pago column
 * @method     ChildServicoPrestadoQuery orderByPlanoId($order = Criteria::ASC) Order by the plano_id column
 * @method     ChildServicoPrestadoQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method     ChildServicoPrestadoQuery orderByTipoServicoPrestadoId($order = Criteria::ASC) Order by the tipo_servico_prestado_id column
 *
 * @method     ChildServicoPrestadoQuery groupByIdservicoPrestado() Group by the idservico_prestado column
 * @method     ChildServicoPrestadoQuery groupByNome() Group by the nome column
 * @method     ChildServicoPrestadoQuery groupByCodigo() Group by the codigo column
 * @method     ChildServicoPrestadoQuery groupByDownload() Group by the download column
 * @method     ChildServicoPrestadoQuery groupByUpload() Group by the upload column
 * @method     ChildServicoPrestadoQuery groupByFranquia() Group by the franquia column
 * @method     ChildServicoPrestadoQuery groupByTecnologia() Group by the tecnologia column
 * @method     ChildServicoPrestadoQuery groupByGarantiaBanda() Group by the garantia_banda column
 * @method     ChildServicoPrestadoQuery groupByAtivo() Group by the ativo column
 * @method     ChildServicoPrestadoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildServicoPrestadoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildServicoPrestadoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildServicoPrestadoQuery groupByPrePago() Group by the pre_pago column
 * @method     ChildServicoPrestadoQuery groupByPlanoId() Group by the plano_id column
 * @method     ChildServicoPrestadoQuery groupByTipo() Group by the tipo column
 * @method     ChildServicoPrestadoQuery groupByTipoServicoPrestadoId() Group by the tipo_servico_prestado_id column
 *
 * @method     ChildServicoPrestadoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildServicoPrestadoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildServicoPrestadoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildServicoPrestadoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildServicoPrestadoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildServicoPrestadoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildServicoPrestadoQuery leftJoinPlanos($relationAlias = null) Adds a LEFT JOIN clause to the query using the Planos relation
 * @method     ChildServicoPrestadoQuery rightJoinPlanos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Planos relation
 * @method     ChildServicoPrestadoQuery innerJoinPlanos($relationAlias = null) Adds a INNER JOIN clause to the query using the Planos relation
 *
 * @method     ChildServicoPrestadoQuery joinWithPlanos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Planos relation
 *
 * @method     ChildServicoPrestadoQuery leftJoinWithPlanos() Adds a LEFT JOIN clause and with to the query using the Planos relation
 * @method     ChildServicoPrestadoQuery rightJoinWithPlanos() Adds a RIGHT JOIN clause and with to the query using the Planos relation
 * @method     ChildServicoPrestadoQuery innerJoinWithPlanos() Adds a INNER JOIN clause and with to the query using the Planos relation
 *
 * @method     ChildServicoPrestadoQuery leftJoinTipoServicoPrestado($relationAlias = null) Adds a LEFT JOIN clause to the query using the TipoServicoPrestado relation
 * @method     ChildServicoPrestadoQuery rightJoinTipoServicoPrestado($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TipoServicoPrestado relation
 * @method     ChildServicoPrestadoQuery innerJoinTipoServicoPrestado($relationAlias = null) Adds a INNER JOIN clause to the query using the TipoServicoPrestado relation
 *
 * @method     ChildServicoPrestadoQuery joinWithTipoServicoPrestado($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TipoServicoPrestado relation
 *
 * @method     ChildServicoPrestadoQuery leftJoinWithTipoServicoPrestado() Adds a LEFT JOIN clause and with to the query using the TipoServicoPrestado relation
 * @method     ChildServicoPrestadoQuery rightJoinWithTipoServicoPrestado() Adds a RIGHT JOIN clause and with to the query using the TipoServicoPrestado relation
 * @method     ChildServicoPrestadoQuery innerJoinWithTipoServicoPrestado() Adds a INNER JOIN clause and with to the query using the TipoServicoPrestado relation
 *
 * @method     ChildServicoPrestadoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildServicoPrestadoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildServicoPrestadoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildServicoPrestadoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildServicoPrestadoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildServicoPrestadoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildServicoPrestadoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildServicoPrestadoQuery leftJoinServicoCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildServicoPrestadoQuery rightJoinServicoCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildServicoPrestadoQuery innerJoinServicoCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicoCliente relation
 *
 * @method     ChildServicoPrestadoQuery joinWithServicoCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicoCliente relation
 *
 * @method     ChildServicoPrestadoQuery leftJoinWithServicoCliente() Adds a LEFT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildServicoPrestadoQuery rightJoinWithServicoCliente() Adds a RIGHT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildServicoPrestadoQuery innerJoinWithServicoCliente() Adds a INNER JOIN clause and with to the query using the ServicoCliente relation
 *
 * @method     \ImaTelecomBundle\Model\PlanosQuery|\ImaTelecomBundle\Model\TipoServicoPrestadoQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\ServicoClienteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildServicoPrestado findOne(ConnectionInterface $con = null) Return the first ChildServicoPrestado matching the query
 * @method     ChildServicoPrestado findOneOrCreate(ConnectionInterface $con = null) Return the first ChildServicoPrestado matching the query, or a new ChildServicoPrestado object populated from the query conditions when no match is found
 *
 * @method     ChildServicoPrestado findOneByIdservicoPrestado(int $idservico_prestado) Return the first ChildServicoPrestado filtered by the idservico_prestado column
 * @method     ChildServicoPrestado findOneByNome(string $nome) Return the first ChildServicoPrestado filtered by the nome column
 * @method     ChildServicoPrestado findOneByCodigo(string $codigo) Return the first ChildServicoPrestado filtered by the codigo column
 * @method     ChildServicoPrestado findOneByDownload(int $download) Return the first ChildServicoPrestado filtered by the download column
 * @method     ChildServicoPrestado findOneByUpload(int $upload) Return the first ChildServicoPrestado filtered by the upload column
 * @method     ChildServicoPrestado findOneByFranquia(int $franquia) Return the first ChildServicoPrestado filtered by the franquia column
 * @method     ChildServicoPrestado findOneByTecnologia(string $tecnologia) Return the first ChildServicoPrestado filtered by the tecnologia column
 * @method     ChildServicoPrestado findOneByGarantiaBanda(int $garantia_banda) Return the first ChildServicoPrestado filtered by the garantia_banda column
 * @method     ChildServicoPrestado findOneByAtivo(boolean $ativo) Return the first ChildServicoPrestado filtered by the ativo column
 * @method     ChildServicoPrestado findOneByDataCadastro(string $data_cadastro) Return the first ChildServicoPrestado filtered by the data_cadastro column
 * @method     ChildServicoPrestado findOneByDataAlterado(string $data_alterado) Return the first ChildServicoPrestado filtered by the data_alterado column
 * @method     ChildServicoPrestado findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildServicoPrestado filtered by the usuario_alterado column
 * @method     ChildServicoPrestado findOneByPrePago(boolean $pre_pago) Return the first ChildServicoPrestado filtered by the pre_pago column
 * @method     ChildServicoPrestado findOneByPlanoId(int $plano_id) Return the first ChildServicoPrestado filtered by the plano_id column
 * @method     ChildServicoPrestado findOneByTipo(string $tipo) Return the first ChildServicoPrestado filtered by the tipo column
 * @method     ChildServicoPrestado findOneByTipoServicoPrestadoId(int $tipo_servico_prestado_id) Return the first ChildServicoPrestado filtered by the tipo_servico_prestado_id column *

 * @method     ChildServicoPrestado requirePk($key, ConnectionInterface $con = null) Return the ChildServicoPrestado by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOne(ConnectionInterface $con = null) Return the first ChildServicoPrestado matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicoPrestado requireOneByIdservicoPrestado(int $idservico_prestado) Return the first ChildServicoPrestado filtered by the idservico_prestado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByNome(string $nome) Return the first ChildServicoPrestado filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByCodigo(string $codigo) Return the first ChildServicoPrestado filtered by the codigo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByDownload(int $download) Return the first ChildServicoPrestado filtered by the download column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByUpload(int $upload) Return the first ChildServicoPrestado filtered by the upload column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByFranquia(int $franquia) Return the first ChildServicoPrestado filtered by the franquia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByTecnologia(string $tecnologia) Return the first ChildServicoPrestado filtered by the tecnologia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByGarantiaBanda(int $garantia_banda) Return the first ChildServicoPrestado filtered by the garantia_banda column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByAtivo(boolean $ativo) Return the first ChildServicoPrestado filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByDataCadastro(string $data_cadastro) Return the first ChildServicoPrestado filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByDataAlterado(string $data_alterado) Return the first ChildServicoPrestado filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildServicoPrestado filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByPrePago(boolean $pre_pago) Return the first ChildServicoPrestado filtered by the pre_pago column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByPlanoId(int $plano_id) Return the first ChildServicoPrestado filtered by the plano_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByTipo(string $tipo) Return the first ChildServicoPrestado filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildServicoPrestado requireOneByTipoServicoPrestadoId(int $tipo_servico_prestado_id) Return the first ChildServicoPrestado filtered by the tipo_servico_prestado_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildServicoPrestado[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildServicoPrestado objects based on current ModelCriteria
 * @method     ChildServicoPrestado[]|ObjectCollection findByIdservicoPrestado(int $idservico_prestado) Return ChildServicoPrestado objects filtered by the idservico_prestado column
 * @method     ChildServicoPrestado[]|ObjectCollection findByNome(string $nome) Return ChildServicoPrestado objects filtered by the nome column
 * @method     ChildServicoPrestado[]|ObjectCollection findByCodigo(string $codigo) Return ChildServicoPrestado objects filtered by the codigo column
 * @method     ChildServicoPrestado[]|ObjectCollection findByDownload(int $download) Return ChildServicoPrestado objects filtered by the download column
 * @method     ChildServicoPrestado[]|ObjectCollection findByUpload(int $upload) Return ChildServicoPrestado objects filtered by the upload column
 * @method     ChildServicoPrestado[]|ObjectCollection findByFranquia(int $franquia) Return ChildServicoPrestado objects filtered by the franquia column
 * @method     ChildServicoPrestado[]|ObjectCollection findByTecnologia(string $tecnologia) Return ChildServicoPrestado objects filtered by the tecnologia column
 * @method     ChildServicoPrestado[]|ObjectCollection findByGarantiaBanda(int $garantia_banda) Return ChildServicoPrestado objects filtered by the garantia_banda column
 * @method     ChildServicoPrestado[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildServicoPrestado objects filtered by the ativo column
 * @method     ChildServicoPrestado[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildServicoPrestado objects filtered by the data_cadastro column
 * @method     ChildServicoPrestado[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildServicoPrestado objects filtered by the data_alterado column
 * @method     ChildServicoPrestado[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildServicoPrestado objects filtered by the usuario_alterado column
 * @method     ChildServicoPrestado[]|ObjectCollection findByPrePago(boolean $pre_pago) Return ChildServicoPrestado objects filtered by the pre_pago column
 * @method     ChildServicoPrestado[]|ObjectCollection findByPlanoId(int $plano_id) Return ChildServicoPrestado objects filtered by the plano_id column
 * @method     ChildServicoPrestado[]|ObjectCollection findByTipo(string $tipo) Return ChildServicoPrestado objects filtered by the tipo column
 * @method     ChildServicoPrestado[]|ObjectCollection findByTipoServicoPrestadoId(int $tipo_servico_prestado_id) Return ChildServicoPrestado objects filtered by the tipo_servico_prestado_id column
 * @method     ChildServicoPrestado[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ServicoPrestadoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ServicoPrestadoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ServicoPrestado', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildServicoPrestadoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildServicoPrestadoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildServicoPrestadoQuery) {
            return $criteria;
        }
        $query = new ChildServicoPrestadoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildServicoPrestado|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ServicoPrestadoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ServicoPrestadoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicoPrestado A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idservico_prestado, nome, codigo, download, upload, franquia, tecnologia, garantia_banda, ativo, data_cadastro, data_alterado, usuario_alterado, pre_pago, plano_id, tipo, tipo_servico_prestado_id FROM servico_prestado WHERE idservico_prestado = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildServicoPrestado $obj */
            $obj = new ChildServicoPrestado();
            $obj->hydrate($row);
            ServicoPrestadoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildServicoPrestado|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idservico_prestado column
     *
     * Example usage:
     * <code>
     * $query->filterByIdservicoPrestado(1234); // WHERE idservico_prestado = 1234
     * $query->filterByIdservicoPrestado(array(12, 34)); // WHERE idservico_prestado IN (12, 34)
     * $query->filterByIdservicoPrestado(array('min' => 12)); // WHERE idservico_prestado > 12
     * </code>
     *
     * @param     mixed $idservicoPrestado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByIdservicoPrestado($idservicoPrestado = null, $comparison = null)
    {
        if (is_array($idservicoPrestado)) {
            $useMinMax = false;
            if (isset($idservicoPrestado['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, $idservicoPrestado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idservicoPrestado['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, $idservicoPrestado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, $idservicoPrestado, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the codigo column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigo('fooValue');   // WHERE codigo = 'fooValue'
     * $query->filterByCodigo('%fooValue%', Criteria::LIKE); // WHERE codigo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByCodigo($codigo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_CODIGO, $codigo, $comparison);
    }

    /**
     * Filter the query on the download column
     *
     * Example usage:
     * <code>
     * $query->filterByDownload(1234); // WHERE download = 1234
     * $query->filterByDownload(array(12, 34)); // WHERE download IN (12, 34)
     * $query->filterByDownload(array('min' => 12)); // WHERE download > 12
     * </code>
     *
     * @param     mixed $download The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByDownload($download = null, $comparison = null)
    {
        if (is_array($download)) {
            $useMinMax = false;
            if (isset($download['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_DOWNLOAD, $download['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($download['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_DOWNLOAD, $download['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_DOWNLOAD, $download, $comparison);
    }

    /**
     * Filter the query on the upload column
     *
     * Example usage:
     * <code>
     * $query->filterByUpload(1234); // WHERE upload = 1234
     * $query->filterByUpload(array(12, 34)); // WHERE upload IN (12, 34)
     * $query->filterByUpload(array('min' => 12)); // WHERE upload > 12
     * </code>
     *
     * @param     mixed $upload The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByUpload($upload = null, $comparison = null)
    {
        if (is_array($upload)) {
            $useMinMax = false;
            if (isset($upload['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_UPLOAD, $upload['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($upload['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_UPLOAD, $upload['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_UPLOAD, $upload, $comparison);
    }

    /**
     * Filter the query on the franquia column
     *
     * Example usage:
     * <code>
     * $query->filterByFranquia(1234); // WHERE franquia = 1234
     * $query->filterByFranquia(array(12, 34)); // WHERE franquia IN (12, 34)
     * $query->filterByFranquia(array('min' => 12)); // WHERE franquia > 12
     * </code>
     *
     * @param     mixed $franquia The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByFranquia($franquia = null, $comparison = null)
    {
        if (is_array($franquia)) {
            $useMinMax = false;
            if (isset($franquia['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_FRANQUIA, $franquia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($franquia['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_FRANQUIA, $franquia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_FRANQUIA, $franquia, $comparison);
    }

    /**
     * Filter the query on the tecnologia column
     *
     * Example usage:
     * <code>
     * $query->filterByTecnologia('fooValue');   // WHERE tecnologia = 'fooValue'
     * $query->filterByTecnologia('%fooValue%', Criteria::LIKE); // WHERE tecnologia LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tecnologia The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByTecnologia($tecnologia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tecnologia)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_TECNOLOGIA, $tecnologia, $comparison);
    }

    /**
     * Filter the query on the garantia_banda column
     *
     * Example usage:
     * <code>
     * $query->filterByGarantiaBanda(1234); // WHERE garantia_banda = 1234
     * $query->filterByGarantiaBanda(array(12, 34)); // WHERE garantia_banda IN (12, 34)
     * $query->filterByGarantiaBanda(array('min' => 12)); // WHERE garantia_banda > 12
     * </code>
     *
     * @param     mixed $garantiaBanda The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByGarantiaBanda($garantiaBanda = null, $comparison = null)
    {
        if (is_array($garantiaBanda)) {
            $useMinMax = false;
            if (isset($garantiaBanda['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_GARANTIA_BANDA, $garantiaBanda['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($garantiaBanda['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_GARANTIA_BANDA, $garantiaBanda['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_GARANTIA_BANDA, $garantiaBanda, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the pre_pago column
     *
     * Example usage:
     * <code>
     * $query->filterByPrePago(true); // WHERE pre_pago = true
     * $query->filterByPrePago('yes'); // WHERE pre_pago = true
     * </code>
     *
     * @param     boolean|string $prePago The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByPrePago($prePago = null, $comparison = null)
    {
        if (is_string($prePago)) {
            $prePago = in_array(strtolower($prePago), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_PRE_PAGO, $prePago, $comparison);
    }

    /**
     * Filter the query on the plano_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPlanoId(1234); // WHERE plano_id = 1234
     * $query->filterByPlanoId(array(12, 34)); // WHERE plano_id IN (12, 34)
     * $query->filterByPlanoId(array('min' => 12)); // WHERE plano_id > 12
     * </code>
     *
     * @see       filterByPlanos()
     *
     * @param     mixed $planoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByPlanoId($planoId = null, $comparison = null)
    {
        if (is_array($planoId)) {
            $useMinMax = false;
            if (isset($planoId['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_PLANO_ID, $planoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($planoId['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_PLANO_ID, $planoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_PLANO_ID, $planoId, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the tipo_servico_prestado_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoServicoPrestadoId(1234); // WHERE tipo_servico_prestado_id = 1234
     * $query->filterByTipoServicoPrestadoId(array(12, 34)); // WHERE tipo_servico_prestado_id IN (12, 34)
     * $query->filterByTipoServicoPrestadoId(array('min' => 12)); // WHERE tipo_servico_prestado_id > 12
     * </code>
     *
     * @see       filterByTipoServicoPrestado()
     *
     * @param     mixed $tipoServicoPrestadoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByTipoServicoPrestadoId($tipoServicoPrestadoId = null, $comparison = null)
    {
        if (is_array($tipoServicoPrestadoId)) {
            $useMinMax = false;
            if (isset($tipoServicoPrestadoId['min'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID, $tipoServicoPrestadoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tipoServicoPrestadoId['max'])) {
                $this->addUsingAlias(ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID, $tipoServicoPrestadoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID, $tipoServicoPrestadoId, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Planos object
     *
     * @param \ImaTelecomBundle\Model\Planos|ObjectCollection $planos The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByPlanos($planos, $comparison = null)
    {
        if ($planos instanceof \ImaTelecomBundle\Model\Planos) {
            return $this
                ->addUsingAlias(ServicoPrestadoTableMap::COL_PLANO_ID, $planos->getIdplano(), $comparison);
        } elseif ($planos instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicoPrestadoTableMap::COL_PLANO_ID, $planos->toKeyValue('PrimaryKey', 'Idplano'), $comparison);
        } else {
            throw new PropelException('filterByPlanos() only accepts arguments of type \ImaTelecomBundle\Model\Planos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Planos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function joinPlanos($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Planos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Planos');
        }

        return $this;
    }

    /**
     * Use the Planos relation Planos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\PlanosQuery A secondary query class using the current class as primary query
     */
    public function usePlanosQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPlanos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Planos', '\ImaTelecomBundle\Model\PlanosQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\TipoServicoPrestado object
     *
     * @param \ImaTelecomBundle\Model\TipoServicoPrestado|ObjectCollection $tipoServicoPrestado The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByTipoServicoPrestado($tipoServicoPrestado, $comparison = null)
    {
        if ($tipoServicoPrestado instanceof \ImaTelecomBundle\Model\TipoServicoPrestado) {
            return $this
                ->addUsingAlias(ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID, $tipoServicoPrestado->getIdtipoServicoPrestado(), $comparison);
        } elseif ($tipoServicoPrestado instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID, $tipoServicoPrestado->toKeyValue('PrimaryKey', 'IdtipoServicoPrestado'), $comparison);
        } else {
            throw new PropelException('filterByTipoServicoPrestado() only accepts arguments of type \ImaTelecomBundle\Model\TipoServicoPrestado or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TipoServicoPrestado relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function joinTipoServicoPrestado($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TipoServicoPrestado');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TipoServicoPrestado');
        }

        return $this;
    }

    /**
     * Use the TipoServicoPrestado relation TipoServicoPrestado object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\TipoServicoPrestadoQuery A secondary query class using the current class as primary query
     */
    public function useTipoServicoPrestadoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTipoServicoPrestado($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TipoServicoPrestado', '\ImaTelecomBundle\Model\TipoServicoPrestadoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ServicoPrestadoTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ServicoPrestadoTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ServicoCliente object
     *
     * @param \ImaTelecomBundle\Model\ServicoCliente|ObjectCollection $servicoCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByServicoCliente($servicoCliente, $comparison = null)
    {
        if ($servicoCliente instanceof \ImaTelecomBundle\Model\ServicoCliente) {
            return $this
                ->addUsingAlias(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, $servicoCliente->getServicoPrestadoId(), $comparison);
        } elseif ($servicoCliente instanceof ObjectCollection) {
            return $this
                ->useServicoClienteQuery()
                ->filterByPrimaryKeys($servicoCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicoCliente() only accepts arguments of type \ImaTelecomBundle\Model\ServicoCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicoCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function joinServicoCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicoCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicoCliente');
        }

        return $this;
    }

    /**
     * Use the ServicoCliente relation ServicoCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ServicoClienteQuery A secondary query class using the current class as primary query
     */
    public function useServicoClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicoCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicoCliente', '\ImaTelecomBundle\Model\ServicoClienteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildServicoPrestado $servicoPrestado Object to remove from the list of results
     *
     * @return $this|ChildServicoPrestadoQuery The current query, for fluid interface
     */
    public function prune($servicoPrestado = null)
    {
        if ($servicoPrestado) {
            $this->addUsingAlias(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, $servicoPrestado->getIdservicoPrestado(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the servico_prestado table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoPrestadoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ServicoPrestadoTableMap::clearInstancePool();
            ServicoPrestadoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoPrestadoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ServicoPrestadoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ServicoPrestadoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ServicoPrestadoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ServicoPrestadoQuery
