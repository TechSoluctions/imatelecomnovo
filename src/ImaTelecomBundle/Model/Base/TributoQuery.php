<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Tributo as ChildTributo;
use ImaTelecomBundle\Model\TributoQuery as ChildTributoQuery;
use ImaTelecomBundle\Model\Map\TributoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tributo' table.
 *
 *
 *
 * @method     ChildTributoQuery orderByIdtributo($order = Criteria::ASC) Order by the idtributo column
 * @method     ChildTributoQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildTributoQuery orderByPercentualPadrao($order = Criteria::ASC) Order by the percentual_padrao column
 * @method     ChildTributoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildTributoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildTributoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildTributoQuery groupByIdtributo() Group by the idtributo column
 * @method     ChildTributoQuery groupByNome() Group by the nome column
 * @method     ChildTributoQuery groupByPercentualPadrao() Group by the percentual_padrao column
 * @method     ChildTributoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildTributoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildTributoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildTributoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTributoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTributoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTributoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTributoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTributoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTributoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildTributoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildTributoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildTributoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildTributoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildTributoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildTributoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildTributoQuery leftJoinContasPagarTributos($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagarTributos relation
 * @method     ChildTributoQuery rightJoinContasPagarTributos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagarTributos relation
 * @method     ChildTributoQuery innerJoinContasPagarTributos($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagarTributos relation
 *
 * @method     ChildTributoQuery joinWithContasPagarTributos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagarTributos relation
 *
 * @method     ChildTributoQuery leftJoinWithContasPagarTributos() Adds a LEFT JOIN clause and with to the query using the ContasPagarTributos relation
 * @method     ChildTributoQuery rightJoinWithContasPagarTributos() Adds a RIGHT JOIN clause and with to the query using the ContasPagarTributos relation
 * @method     ChildTributoQuery innerJoinWithContasPagarTributos() Adds a INNER JOIN clause and with to the query using the ContasPagarTributos relation
 *
 * @method     \ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\ContasPagarTributosQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTributo findOne(ConnectionInterface $con = null) Return the first ChildTributo matching the query
 * @method     ChildTributo findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTributo matching the query, or a new ChildTributo object populated from the query conditions when no match is found
 *
 * @method     ChildTributo findOneByIdtributo(int $idtributo) Return the first ChildTributo filtered by the idtributo column
 * @method     ChildTributo findOneByNome(string $nome) Return the first ChildTributo filtered by the nome column
 * @method     ChildTributo findOneByPercentualPadrao(string $percentual_padrao) Return the first ChildTributo filtered by the percentual_padrao column
 * @method     ChildTributo findOneByDataCadastro(string $data_cadastro) Return the first ChildTributo filtered by the data_cadastro column
 * @method     ChildTributo findOneByDataAlterado(string $data_alterado) Return the first ChildTributo filtered by the data_alterado column
 * @method     ChildTributo findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildTributo filtered by the usuario_alterado column *

 * @method     ChildTributo requirePk($key, ConnectionInterface $con = null) Return the ChildTributo by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTributo requireOne(ConnectionInterface $con = null) Return the first ChildTributo matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTributo requireOneByIdtributo(int $idtributo) Return the first ChildTributo filtered by the idtributo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTributo requireOneByNome(string $nome) Return the first ChildTributo filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTributo requireOneByPercentualPadrao(string $percentual_padrao) Return the first ChildTributo filtered by the percentual_padrao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTributo requireOneByDataCadastro(string $data_cadastro) Return the first ChildTributo filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTributo requireOneByDataAlterado(string $data_alterado) Return the first ChildTributo filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTributo requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildTributo filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTributo[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTributo objects based on current ModelCriteria
 * @method     ChildTributo[]|ObjectCollection findByIdtributo(int $idtributo) Return ChildTributo objects filtered by the idtributo column
 * @method     ChildTributo[]|ObjectCollection findByNome(string $nome) Return ChildTributo objects filtered by the nome column
 * @method     ChildTributo[]|ObjectCollection findByPercentualPadrao(string $percentual_padrao) Return ChildTributo objects filtered by the percentual_padrao column
 * @method     ChildTributo[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildTributo objects filtered by the data_cadastro column
 * @method     ChildTributo[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildTributo objects filtered by the data_alterado column
 * @method     ChildTributo[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildTributo objects filtered by the usuario_alterado column
 * @method     ChildTributo[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TributoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\TributoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Tributo', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTributoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTributoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTributoQuery) {
            return $criteria;
        }
        $query = new ChildTributoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTributo|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TributoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TributoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTributo A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idtributo, nome, percentual_padrao, data_cadastro, data_alterado, usuario_alterado FROM tributo WHERE idtributo = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTributo $obj */
            $obj = new ChildTributo();
            $obj->hydrate($row);
            TributoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTributo|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TributoTableMap::COL_IDTRIBUTO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TributoTableMap::COL_IDTRIBUTO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idtributo column
     *
     * Example usage:
     * <code>
     * $query->filterByIdtributo(1234); // WHERE idtributo = 1234
     * $query->filterByIdtributo(array(12, 34)); // WHERE idtributo IN (12, 34)
     * $query->filterByIdtributo(array('min' => 12)); // WHERE idtributo > 12
     * </code>
     *
     * @param     mixed $idtributo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function filterByIdtributo($idtributo = null, $comparison = null)
    {
        if (is_array($idtributo)) {
            $useMinMax = false;
            if (isset($idtributo['min'])) {
                $this->addUsingAlias(TributoTableMap::COL_IDTRIBUTO, $idtributo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idtributo['max'])) {
                $this->addUsingAlias(TributoTableMap::COL_IDTRIBUTO, $idtributo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TributoTableMap::COL_IDTRIBUTO, $idtributo, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TributoTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the percentual_padrao column
     *
     * Example usage:
     * <code>
     * $query->filterByPercentualPadrao(1234); // WHERE percentual_padrao = 1234
     * $query->filterByPercentualPadrao(array(12, 34)); // WHERE percentual_padrao IN (12, 34)
     * $query->filterByPercentualPadrao(array('min' => 12)); // WHERE percentual_padrao > 12
     * </code>
     *
     * @param     mixed $percentualPadrao The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function filterByPercentualPadrao($percentualPadrao = null, $comparison = null)
    {
        if (is_array($percentualPadrao)) {
            $useMinMax = false;
            if (isset($percentualPadrao['min'])) {
                $this->addUsingAlias(TributoTableMap::COL_PERCENTUAL_PADRAO, $percentualPadrao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($percentualPadrao['max'])) {
                $this->addUsingAlias(TributoTableMap::COL_PERCENTUAL_PADRAO, $percentualPadrao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TributoTableMap::COL_PERCENTUAL_PADRAO, $percentualPadrao, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(TributoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(TributoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TributoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(TributoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(TributoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TributoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(TributoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(TributoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TributoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTributoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(TributoTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(TributoTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagarTributos object
     *
     * @param \ImaTelecomBundle\Model\ContasPagarTributos|ObjectCollection $contasPagarTributos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTributoQuery The current query, for fluid interface
     */
    public function filterByContasPagarTributos($contasPagarTributos, $comparison = null)
    {
        if ($contasPagarTributos instanceof \ImaTelecomBundle\Model\ContasPagarTributos) {
            return $this
                ->addUsingAlias(TributoTableMap::COL_IDTRIBUTO, $contasPagarTributos->getTributoId(), $comparison);
        } elseif ($contasPagarTributos instanceof ObjectCollection) {
            return $this
                ->useContasPagarTributosQuery()
                ->filterByPrimaryKeys($contasPagarTributos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContasPagarTributos() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagarTributos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagarTributos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function joinContasPagarTributos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagarTributos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagarTributos');
        }

        return $this;
    }

    /**
     * Use the ContasPagarTributos relation ContasPagarTributos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarTributosQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarTributosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContasPagarTributos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagarTributos', '\ImaTelecomBundle\Model\ContasPagarTributosQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTributo $tributo Object to remove from the list of results
     *
     * @return $this|ChildTributoQuery The current query, for fluid interface
     */
    public function prune($tributo = null)
    {
        if ($tributo) {
            $this->addUsingAlias(TributoTableMap::COL_IDTRIBUTO, $tributo->getIdtributo(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tributo table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TributoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TributoTableMap::clearInstancePool();
            TributoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TributoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TributoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TributoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TributoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TributoQuery
