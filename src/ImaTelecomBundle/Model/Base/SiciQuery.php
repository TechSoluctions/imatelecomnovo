<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Sici as ChildSici;
use ImaTelecomBundle\Model\SiciQuery as ChildSiciQuery;
use ImaTelecomBundle\Model\Map\SiciTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sici' table.
 *
 *
 *
 * @method     ChildSiciQuery orderByIdsici($order = Criteria::ASC) Order by the idsici column
 * @method     ChildSiciQuery orderByAno($order = Criteria::ASC) Order by the ano column
 * @method     ChildSiciQuery orderByMes($order = Criteria::ASC) Order by the mes column
 * @method     ChildSiciQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildSiciQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildSiciQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildSiciQuery orderByNomeArquivo($order = Criteria::ASC) Order by the nome_arquivo column
 * @method     ChildSiciQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildSiciQuery orderByQtdeNotas($order = Criteria::ASC) Order by the qtde_notas column
 * @method     ChildSiciQuery orderByQtdePontos($order = Criteria::ASC) Order by the qtde_pontos column
 * @method     ChildSiciQuery orderByValorTotal($order = Criteria::ASC) Order by the valor_total column
 *
 * @method     ChildSiciQuery groupByIdsici() Group by the idsici column
 * @method     ChildSiciQuery groupByAno() Group by the ano column
 * @method     ChildSiciQuery groupByMes() Group by the mes column
 * @method     ChildSiciQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildSiciQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildSiciQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildSiciQuery groupByNomeArquivo() Group by the nome_arquivo column
 * @method     ChildSiciQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildSiciQuery groupByQtdeNotas() Group by the qtde_notas column
 * @method     ChildSiciQuery groupByQtdePontos() Group by the qtde_pontos column
 * @method     ChildSiciQuery groupByValorTotal() Group by the valor_total column
 *
 * @method     ChildSiciQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiciQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiciQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiciQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiciQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiciQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiciQuery leftJoinCompetencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Competencia relation
 * @method     ChildSiciQuery rightJoinCompetencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Competencia relation
 * @method     ChildSiciQuery innerJoinCompetencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Competencia relation
 *
 * @method     ChildSiciQuery joinWithCompetencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Competencia relation
 *
 * @method     ChildSiciQuery leftJoinWithCompetencia() Adds a LEFT JOIN clause and with to the query using the Competencia relation
 * @method     ChildSiciQuery rightJoinWithCompetencia() Adds a RIGHT JOIN clause and with to the query using the Competencia relation
 * @method     ChildSiciQuery innerJoinWithCompetencia() Adds a INNER JOIN clause and with to the query using the Competencia relation
 *
 * @method     ChildSiciQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildSiciQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildSiciQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildSiciQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildSiciQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildSiciQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildSiciQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \ImaTelecomBundle\Model\CompetenciaQuery|\ImaTelecomBundle\Model\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSici findOne(ConnectionInterface $con = null) Return the first ChildSici matching the query
 * @method     ChildSici findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSici matching the query, or a new ChildSici object populated from the query conditions when no match is found
 *
 * @method     ChildSici findOneByIdsici(int $idsici) Return the first ChildSici filtered by the idsici column
 * @method     ChildSici findOneByAno(string $ano) Return the first ChildSici filtered by the ano column
 * @method     ChildSici findOneByMes(string $mes) Return the first ChildSici filtered by the mes column
 * @method     ChildSici findOneByDataCadastro(string $data_cadastro) Return the first ChildSici filtered by the data_cadastro column
 * @method     ChildSici findOneByDataAlterado(string $data_alterado) Return the first ChildSici filtered by the data_alterado column
 * @method     ChildSici findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildSici filtered by the usuario_alterado column
 * @method     ChildSici findOneByNomeArquivo(string $nome_arquivo) Return the first ChildSici filtered by the nome_arquivo column
 * @method     ChildSici findOneByCompetenciaId(int $competencia_id) Return the first ChildSici filtered by the competencia_id column
 * @method     ChildSici findOneByQtdeNotas(int $qtde_notas) Return the first ChildSici filtered by the qtde_notas column
 * @method     ChildSici findOneByQtdePontos(int $qtde_pontos) Return the first ChildSici filtered by the qtde_pontos column
 * @method     ChildSici findOneByValorTotal(string $valor_total) Return the first ChildSici filtered by the valor_total column *

 * @method     ChildSici requirePk($key, ConnectionInterface $con = null) Return the ChildSici by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOne(ConnectionInterface $con = null) Return the first ChildSici matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSici requireOneByIdsici(int $idsici) Return the first ChildSici filtered by the idsici column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByAno(string $ano) Return the first ChildSici filtered by the ano column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByMes(string $mes) Return the first ChildSici filtered by the mes column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByDataCadastro(string $data_cadastro) Return the first ChildSici filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByDataAlterado(string $data_alterado) Return the first ChildSici filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildSici filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByNomeArquivo(string $nome_arquivo) Return the first ChildSici filtered by the nome_arquivo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByCompetenciaId(int $competencia_id) Return the first ChildSici filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByQtdeNotas(int $qtde_notas) Return the first ChildSici filtered by the qtde_notas column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByQtdePontos(int $qtde_pontos) Return the first ChildSici filtered by the qtde_pontos column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSici requireOneByValorTotal(string $valor_total) Return the first ChildSici filtered by the valor_total column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSici[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSici objects based on current ModelCriteria
 * @method     ChildSici[]|ObjectCollection findByIdsici(int $idsici) Return ChildSici objects filtered by the idsici column
 * @method     ChildSici[]|ObjectCollection findByAno(string $ano) Return ChildSici objects filtered by the ano column
 * @method     ChildSici[]|ObjectCollection findByMes(string $mes) Return ChildSici objects filtered by the mes column
 * @method     ChildSici[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildSici objects filtered by the data_cadastro column
 * @method     ChildSici[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildSici objects filtered by the data_alterado column
 * @method     ChildSici[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildSici objects filtered by the usuario_alterado column
 * @method     ChildSici[]|ObjectCollection findByNomeArquivo(string $nome_arquivo) Return ChildSici objects filtered by the nome_arquivo column
 * @method     ChildSici[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildSici objects filtered by the competencia_id column
 * @method     ChildSici[]|ObjectCollection findByQtdeNotas(int $qtde_notas) Return ChildSici objects filtered by the qtde_notas column
 * @method     ChildSici[]|ObjectCollection findByQtdePontos(int $qtde_pontos) Return ChildSici objects filtered by the qtde_pontos column
 * @method     ChildSici[]|ObjectCollection findByValorTotal(string $valor_total) Return ChildSici objects filtered by the valor_total column
 * @method     ChildSici[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiciQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\SiciQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Sici', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiciQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiciQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiciQuery) {
            return $criteria;
        }
        $query = new ChildSiciQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSici|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiciTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiciTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSici A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idsici, ano, mes, data_cadastro, data_alterado, usuario_alterado, nome_arquivo, competencia_id, qtde_notas, qtde_pontos, valor_total FROM sici WHERE idsici = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSici $obj */
            $obj = new ChildSici();
            $obj->hydrate($row);
            SiciTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSici|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiciTableMap::COL_IDSICI, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiciTableMap::COL_IDSICI, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idsici column
     *
     * Example usage:
     * <code>
     * $query->filterByIdsici(1234); // WHERE idsici = 1234
     * $query->filterByIdsici(array(12, 34)); // WHERE idsici IN (12, 34)
     * $query->filterByIdsici(array('min' => 12)); // WHERE idsici > 12
     * </code>
     *
     * @param     mixed $idsici The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByIdsici($idsici = null, $comparison = null)
    {
        if (is_array($idsici)) {
            $useMinMax = false;
            if (isset($idsici['min'])) {
                $this->addUsingAlias(SiciTableMap::COL_IDSICI, $idsici['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idsici['max'])) {
                $this->addUsingAlias(SiciTableMap::COL_IDSICI, $idsici['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_IDSICI, $idsici, $comparison);
    }

    /**
     * Filter the query on the ano column
     *
     * Example usage:
     * <code>
     * $query->filterByAno('fooValue');   // WHERE ano = 'fooValue'
     * $query->filterByAno('%fooValue%', Criteria::LIKE); // WHERE ano LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ano The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByAno($ano = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ano)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_ANO, $ano, $comparison);
    }

    /**
     * Filter the query on the mes column
     *
     * Example usage:
     * <code>
     * $query->filterByMes('fooValue');   // WHERE mes = 'fooValue'
     * $query->filterByMes('%fooValue%', Criteria::LIKE); // WHERE mes LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mes The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByMes($mes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mes)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_MES, $mes, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(SiciTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(SiciTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(SiciTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(SiciTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(SiciTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(SiciTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the nome_arquivo column
     *
     * Example usage:
     * <code>
     * $query->filterByNomeArquivo('fooValue');   // WHERE nome_arquivo = 'fooValue'
     * $query->filterByNomeArquivo('%fooValue%', Criteria::LIKE); // WHERE nome_arquivo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomeArquivo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByNomeArquivo($nomeArquivo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomeArquivo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_NOME_ARQUIVO, $nomeArquivo, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @see       filterByCompetencia()
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(SiciTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(SiciTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the qtde_notas column
     *
     * Example usage:
     * <code>
     * $query->filterByQtdeNotas(1234); // WHERE qtde_notas = 1234
     * $query->filterByQtdeNotas(array(12, 34)); // WHERE qtde_notas IN (12, 34)
     * $query->filterByQtdeNotas(array('min' => 12)); // WHERE qtde_notas > 12
     * </code>
     *
     * @param     mixed $qtdeNotas The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByQtdeNotas($qtdeNotas = null, $comparison = null)
    {
        if (is_array($qtdeNotas)) {
            $useMinMax = false;
            if (isset($qtdeNotas['min'])) {
                $this->addUsingAlias(SiciTableMap::COL_QTDE_NOTAS, $qtdeNotas['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($qtdeNotas['max'])) {
                $this->addUsingAlias(SiciTableMap::COL_QTDE_NOTAS, $qtdeNotas['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_QTDE_NOTAS, $qtdeNotas, $comparison);
    }

    /**
     * Filter the query on the qtde_pontos column
     *
     * Example usage:
     * <code>
     * $query->filterByQtdePontos(1234); // WHERE qtde_pontos = 1234
     * $query->filterByQtdePontos(array(12, 34)); // WHERE qtde_pontos IN (12, 34)
     * $query->filterByQtdePontos(array('min' => 12)); // WHERE qtde_pontos > 12
     * </code>
     *
     * @param     mixed $qtdePontos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByQtdePontos($qtdePontos = null, $comparison = null)
    {
        if (is_array($qtdePontos)) {
            $useMinMax = false;
            if (isset($qtdePontos['min'])) {
                $this->addUsingAlias(SiciTableMap::COL_QTDE_PONTOS, $qtdePontos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($qtdePontos['max'])) {
                $this->addUsingAlias(SiciTableMap::COL_QTDE_PONTOS, $qtdePontos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_QTDE_PONTOS, $qtdePontos, $comparison);
    }

    /**
     * Filter the query on the valor_total column
     *
     * Example usage:
     * <code>
     * $query->filterByValorTotal(1234); // WHERE valor_total = 1234
     * $query->filterByValorTotal(array(12, 34)); // WHERE valor_total IN (12, 34)
     * $query->filterByValorTotal(array('min' => 12)); // WHERE valor_total > 12
     * </code>
     *
     * @param     mixed $valorTotal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function filterByValorTotal($valorTotal = null, $comparison = null)
    {
        if (is_array($valorTotal)) {
            $useMinMax = false;
            if (isset($valorTotal['min'])) {
                $this->addUsingAlias(SiciTableMap::COL_VALOR_TOTAL, $valorTotal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorTotal['max'])) {
                $this->addUsingAlias(SiciTableMap::COL_VALOR_TOTAL, $valorTotal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiciTableMap::COL_VALOR_TOTAL, $valorTotal, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Competencia object
     *
     * @param \ImaTelecomBundle\Model\Competencia|ObjectCollection $competencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiciQuery The current query, for fluid interface
     */
    public function filterByCompetencia($competencia, $comparison = null)
    {
        if ($competencia instanceof \ImaTelecomBundle\Model\Competencia) {
            return $this
                ->addUsingAlias(SiciTableMap::COL_COMPETENCIA_ID, $competencia->getId(), $comparison);
        } elseif ($competencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiciTableMap::COL_COMPETENCIA_ID, $competencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompetencia() only accepts arguments of type \ImaTelecomBundle\Model\Competencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Competencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function joinCompetencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Competencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Competencia');
        }

        return $this;
    }

    /**
     * Use the Competencia relation Competencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CompetenciaQuery A secondary query class using the current class as primary query
     */
    public function useCompetenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompetencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Competencia', '\ImaTelecomBundle\Model\CompetenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiciQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(SiciTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiciTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSici $sici Object to remove from the list of results
     *
     * @return $this|ChildSiciQuery The current query, for fluid interface
     */
    public function prune($sici = null)
    {
        if ($sici) {
            $this->addUsingAlias(SiciTableMap::COL_IDSICI, $sici->getIdsici(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sici table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiciTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiciTableMap::clearInstancePool();
            SiciTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiciTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiciTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiciTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiciTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiciQuery
