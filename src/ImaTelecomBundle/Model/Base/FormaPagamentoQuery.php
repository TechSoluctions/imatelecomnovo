<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\FormaPagamento as ChildFormaPagamento;
use ImaTelecomBundle\Model\FormaPagamentoQuery as ChildFormaPagamentoQuery;
use ImaTelecomBundle\Model\Map\FormaPagamentoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'forma_pagamento' table.
 *
 *
 *
 * @method     ChildFormaPagamentoQuery orderByIdformaPagamento($order = Criteria::ASC) Order by the idforma_pagamento column
 * @method     ChildFormaPagamentoQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method     ChildFormaPagamentoQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildFormaPagamentoQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildFormaPagamentoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildFormaPagamentoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildFormaPagamentoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildFormaPagamentoQuery orderByTemNumeroDocumento($order = Criteria::ASC) Order by the tem_numero_documento column
 *
 * @method     ChildFormaPagamentoQuery groupByIdformaPagamento() Group by the idforma_pagamento column
 * @method     ChildFormaPagamentoQuery groupByTipo() Group by the tipo column
 * @method     ChildFormaPagamentoQuery groupByNome() Group by the nome column
 * @method     ChildFormaPagamentoQuery groupByAtivo() Group by the ativo column
 * @method     ChildFormaPagamentoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildFormaPagamentoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildFormaPagamentoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildFormaPagamentoQuery groupByTemNumeroDocumento() Group by the tem_numero_documento column
 *
 * @method     ChildFormaPagamentoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFormaPagamentoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFormaPagamentoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFormaPagamentoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFormaPagamentoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFormaPagamentoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFormaPagamentoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildFormaPagamentoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildFormaPagamentoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildFormaPagamentoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildFormaPagamentoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildFormaPagamentoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildFormaPagamentoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildFormaPagamentoQuery leftJoinBaixaMovimento($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaixaMovimento relation
 * @method     ChildFormaPagamentoQuery rightJoinBaixaMovimento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaixaMovimento relation
 * @method     ChildFormaPagamentoQuery innerJoinBaixaMovimento($relationAlias = null) Adds a INNER JOIN clause to the query using the BaixaMovimento relation
 *
 * @method     ChildFormaPagamentoQuery joinWithBaixaMovimento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaixaMovimento relation
 *
 * @method     ChildFormaPagamentoQuery leftJoinWithBaixaMovimento() Adds a LEFT JOIN clause and with to the query using the BaixaMovimento relation
 * @method     ChildFormaPagamentoQuery rightJoinWithBaixaMovimento() Adds a RIGHT JOIN clause and with to the query using the BaixaMovimento relation
 * @method     ChildFormaPagamentoQuery innerJoinWithBaixaMovimento() Adds a INNER JOIN clause and with to the query using the BaixaMovimento relation
 *
 * @method     ChildFormaPagamentoQuery leftJoinCaixaMovimento($relationAlias = null) Adds a LEFT JOIN clause to the query using the CaixaMovimento relation
 * @method     ChildFormaPagamentoQuery rightJoinCaixaMovimento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CaixaMovimento relation
 * @method     ChildFormaPagamentoQuery innerJoinCaixaMovimento($relationAlias = null) Adds a INNER JOIN clause to the query using the CaixaMovimento relation
 *
 * @method     ChildFormaPagamentoQuery joinWithCaixaMovimento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CaixaMovimento relation
 *
 * @method     ChildFormaPagamentoQuery leftJoinWithCaixaMovimento() Adds a LEFT JOIN clause and with to the query using the CaixaMovimento relation
 * @method     ChildFormaPagamentoQuery rightJoinWithCaixaMovimento() Adds a RIGHT JOIN clause and with to the query using the CaixaMovimento relation
 * @method     ChildFormaPagamentoQuery innerJoinWithCaixaMovimento() Adds a INNER JOIN clause and with to the query using the CaixaMovimento relation
 *
 * @method     \ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BaixaMovimentoQuery|\ImaTelecomBundle\Model\CaixaMovimentoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFormaPagamento findOne(ConnectionInterface $con = null) Return the first ChildFormaPagamento matching the query
 * @method     ChildFormaPagamento findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFormaPagamento matching the query, or a new ChildFormaPagamento object populated from the query conditions when no match is found
 *
 * @method     ChildFormaPagamento findOneByIdformaPagamento(int $idforma_pagamento) Return the first ChildFormaPagamento filtered by the idforma_pagamento column
 * @method     ChildFormaPagamento findOneByTipo(string $tipo) Return the first ChildFormaPagamento filtered by the tipo column
 * @method     ChildFormaPagamento findOneByNome(string $nome) Return the first ChildFormaPagamento filtered by the nome column
 * @method     ChildFormaPagamento findOneByAtivo(boolean $ativo) Return the first ChildFormaPagamento filtered by the ativo column
 * @method     ChildFormaPagamento findOneByDataCadastro(string $data_cadastro) Return the first ChildFormaPagamento filtered by the data_cadastro column
 * @method     ChildFormaPagamento findOneByDataAlterado(string $data_alterado) Return the first ChildFormaPagamento filtered by the data_alterado column
 * @method     ChildFormaPagamento findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildFormaPagamento filtered by the usuario_alterado column
 * @method     ChildFormaPagamento findOneByTemNumeroDocumento(boolean $tem_numero_documento) Return the first ChildFormaPagamento filtered by the tem_numero_documento column *

 * @method     ChildFormaPagamento requirePk($key, ConnectionInterface $con = null) Return the ChildFormaPagamento by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFormaPagamento requireOne(ConnectionInterface $con = null) Return the first ChildFormaPagamento matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFormaPagamento requireOneByIdformaPagamento(int $idforma_pagamento) Return the first ChildFormaPagamento filtered by the idforma_pagamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFormaPagamento requireOneByTipo(string $tipo) Return the first ChildFormaPagamento filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFormaPagamento requireOneByNome(string $nome) Return the first ChildFormaPagamento filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFormaPagamento requireOneByAtivo(boolean $ativo) Return the first ChildFormaPagamento filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFormaPagamento requireOneByDataCadastro(string $data_cadastro) Return the first ChildFormaPagamento filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFormaPagamento requireOneByDataAlterado(string $data_alterado) Return the first ChildFormaPagamento filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFormaPagamento requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildFormaPagamento filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFormaPagamento requireOneByTemNumeroDocumento(boolean $tem_numero_documento) Return the first ChildFormaPagamento filtered by the tem_numero_documento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFormaPagamento[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFormaPagamento objects based on current ModelCriteria
 * @method     ChildFormaPagamento[]|ObjectCollection findByIdformaPagamento(int $idforma_pagamento) Return ChildFormaPagamento objects filtered by the idforma_pagamento column
 * @method     ChildFormaPagamento[]|ObjectCollection findByTipo(string $tipo) Return ChildFormaPagamento objects filtered by the tipo column
 * @method     ChildFormaPagamento[]|ObjectCollection findByNome(string $nome) Return ChildFormaPagamento objects filtered by the nome column
 * @method     ChildFormaPagamento[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildFormaPagamento objects filtered by the ativo column
 * @method     ChildFormaPagamento[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildFormaPagamento objects filtered by the data_cadastro column
 * @method     ChildFormaPagamento[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildFormaPagamento objects filtered by the data_alterado column
 * @method     ChildFormaPagamento[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildFormaPagamento objects filtered by the usuario_alterado column
 * @method     ChildFormaPagamento[]|ObjectCollection findByTemNumeroDocumento(boolean $tem_numero_documento) Return ChildFormaPagamento objects filtered by the tem_numero_documento column
 * @method     ChildFormaPagamento[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FormaPagamentoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\FormaPagamentoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\FormaPagamento', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFormaPagamentoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFormaPagamentoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFormaPagamentoQuery) {
            return $criteria;
        }
        $query = new ChildFormaPagamentoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFormaPagamento|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FormaPagamentoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FormaPagamentoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFormaPagamento A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idforma_pagamento, tipo, nome, ativo, data_cadastro, data_alterado, usuario_alterado, tem_numero_documento FROM forma_pagamento WHERE idforma_pagamento = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFormaPagamento $obj */
            $obj = new ChildFormaPagamento();
            $obj->hydrate($row);
            FormaPagamentoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFormaPagamento|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_IDFORMA_PAGAMENTO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_IDFORMA_PAGAMENTO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idforma_pagamento column
     *
     * Example usage:
     * <code>
     * $query->filterByIdformaPagamento(1234); // WHERE idforma_pagamento = 1234
     * $query->filterByIdformaPagamento(array(12, 34)); // WHERE idforma_pagamento IN (12, 34)
     * $query->filterByIdformaPagamento(array('min' => 12)); // WHERE idforma_pagamento > 12
     * </code>
     *
     * @param     mixed $idformaPagamento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByIdformaPagamento($idformaPagamento = null, $comparison = null)
    {
        if (is_array($idformaPagamento)) {
            $useMinMax = false;
            if (isset($idformaPagamento['min'])) {
                $this->addUsingAlias(FormaPagamentoTableMap::COL_IDFORMA_PAGAMENTO, $idformaPagamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idformaPagamento['max'])) {
                $this->addUsingAlias(FormaPagamentoTableMap::COL_IDFORMA_PAGAMENTO, $idformaPagamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_IDFORMA_PAGAMENTO, $idformaPagamento, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(FormaPagamentoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(FormaPagamentoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(FormaPagamentoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(FormaPagamentoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(FormaPagamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(FormaPagamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the tem_numero_documento column
     *
     * Example usage:
     * <code>
     * $query->filterByTemNumeroDocumento(true); // WHERE tem_numero_documento = true
     * $query->filterByTemNumeroDocumento('yes'); // WHERE tem_numero_documento = true
     * </code>
     *
     * @param     boolean|string $temNumeroDocumento The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByTemNumeroDocumento($temNumeroDocumento = null, $comparison = null)
    {
        if (is_string($temNumeroDocumento)) {
            $temNumeroDocumento = in_array(strtolower($temNumeroDocumento), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(FormaPagamentoTableMap::COL_TEM_NUMERO_DOCUMENTO, $temNumeroDocumento, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(FormaPagamentoTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FormaPagamentoTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BaixaMovimento object
     *
     * @param \ImaTelecomBundle\Model\BaixaMovimento|ObjectCollection $baixaMovimento the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByBaixaMovimento($baixaMovimento, $comparison = null)
    {
        if ($baixaMovimento instanceof \ImaTelecomBundle\Model\BaixaMovimento) {
            return $this
                ->addUsingAlias(FormaPagamentoTableMap::COL_IDFORMA_PAGAMENTO, $baixaMovimento->getFormaPagamentoId(), $comparison);
        } elseif ($baixaMovimento instanceof ObjectCollection) {
            return $this
                ->useBaixaMovimentoQuery()
                ->filterByPrimaryKeys($baixaMovimento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixaMovimento() only accepts arguments of type \ImaTelecomBundle\Model\BaixaMovimento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaixaMovimento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function joinBaixaMovimento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaixaMovimento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaixaMovimento');
        }

        return $this;
    }

    /**
     * Use the BaixaMovimento relation BaixaMovimento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaMovimentoQuery A secondary query class using the current class as primary query
     */
    public function useBaixaMovimentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixaMovimento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaixaMovimento', '\ImaTelecomBundle\Model\BaixaMovimentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\CaixaMovimento object
     *
     * @param \ImaTelecomBundle\Model\CaixaMovimento|ObjectCollection $caixaMovimento the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function filterByCaixaMovimento($caixaMovimento, $comparison = null)
    {
        if ($caixaMovimento instanceof \ImaTelecomBundle\Model\CaixaMovimento) {
            return $this
                ->addUsingAlias(FormaPagamentoTableMap::COL_IDFORMA_PAGAMENTO, $caixaMovimento->getFormaPagamentoId(), $comparison);
        } elseif ($caixaMovimento instanceof ObjectCollection) {
            return $this
                ->useCaixaMovimentoQuery()
                ->filterByPrimaryKeys($caixaMovimento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCaixaMovimento() only accepts arguments of type \ImaTelecomBundle\Model\CaixaMovimento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CaixaMovimento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function joinCaixaMovimento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CaixaMovimento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CaixaMovimento');
        }

        return $this;
    }

    /**
     * Use the CaixaMovimento relation CaixaMovimento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CaixaMovimentoQuery A secondary query class using the current class as primary query
     */
    public function useCaixaMovimentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCaixaMovimento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CaixaMovimento', '\ImaTelecomBundle\Model\CaixaMovimentoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFormaPagamento $formaPagamento Object to remove from the list of results
     *
     * @return $this|ChildFormaPagamentoQuery The current query, for fluid interface
     */
    public function prune($formaPagamento = null)
    {
        if ($formaPagamento) {
            $this->addUsingAlias(FormaPagamentoTableMap::COL_IDFORMA_PAGAMENTO, $formaPagamento->getIdformaPagamento(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the forma_pagamento table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FormaPagamentoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FormaPagamentoTableMap::clearInstancePool();
            FormaPagamentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FormaPagamentoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FormaPagamentoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FormaPagamentoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FormaPagamentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FormaPagamentoQuery
