<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Pessoa as ChildPessoa;
use ImaTelecomBundle\Model\PessoaQuery as ChildPessoaQuery;
use ImaTelecomBundle\Model\Map\PessoaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'pessoa' table.
 *
 *
 *
 * @method     ChildPessoaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPessoaQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildPessoaQuery orderByRazaoSocial($order = Criteria::ASC) Order by the razao_social column
 * @method     ChildPessoaQuery orderByRua($order = Criteria::ASC) Order by the rua column
 * @method     ChildPessoaQuery orderByBairro($order = Criteria::ASC) Order by the bairro column
 * @method     ChildPessoaQuery orderByCep($order = Criteria::ASC) Order by the cep column
 * @method     ChildPessoaQuery orderByNum($order = Criteria::ASC) Order by the num column
 * @method     ChildPessoaQuery orderByUf($order = Criteria::ASC) Order by the uf column
 * @method     ChildPessoaQuery orderByCpfCnpj($order = Criteria::ASC) Order by the cpf_cnpj column
 * @method     ChildPessoaQuery orderByRgInscrest($order = Criteria::ASC) Order by the rg_inscrest column
 * @method     ChildPessoaQuery orderByPontoReferencia($order = Criteria::ASC) Order by the ponto_referencia column
 * @method     ChildPessoaQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildPessoaQuery orderByComplemento($order = Criteria::ASC) Order by the complemento column
 * @method     ChildPessoaQuery orderByTipoPessoa($order = Criteria::ASC) Order by the tipo_pessoa column
 * @method     ChildPessoaQuery orderByDataNascimento($order = Criteria::ASC) Order by the data_nascimento column
 * @method     ChildPessoaQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildPessoaQuery orderByObs($order = Criteria::ASC) Order by the obs column
 * @method     ChildPessoaQuery orderByCidadeId($order = Criteria::ASC) Order by the cidade_id column
 * @method     ChildPessoaQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildPessoaQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildPessoaQuery orderByResponsavel($order = Criteria::ASC) Order by the responsavel column
 * @method     ChildPessoaQuery orderByContato($order = Criteria::ASC) Order by the contato column
 * @method     ChildPessoaQuery orderBySexo($order = Criteria::ASC) Order by the sexo column
 * @method     ChildPessoaQuery orderByImportId($order = Criteria::ASC) Order by the import_id column
 * @method     ChildPessoaQuery orderByEFornecedor($order = Criteria::ASC) Order by the e_fornecedor column
 *
 * @method     ChildPessoaQuery groupById() Group by the id column
 * @method     ChildPessoaQuery groupByNome() Group by the nome column
 * @method     ChildPessoaQuery groupByRazaoSocial() Group by the razao_social column
 * @method     ChildPessoaQuery groupByRua() Group by the rua column
 * @method     ChildPessoaQuery groupByBairro() Group by the bairro column
 * @method     ChildPessoaQuery groupByCep() Group by the cep column
 * @method     ChildPessoaQuery groupByNum() Group by the num column
 * @method     ChildPessoaQuery groupByUf() Group by the uf column
 * @method     ChildPessoaQuery groupByCpfCnpj() Group by the cpf_cnpj column
 * @method     ChildPessoaQuery groupByRgInscrest() Group by the rg_inscrest column
 * @method     ChildPessoaQuery groupByPontoReferencia() Group by the ponto_referencia column
 * @method     ChildPessoaQuery groupByEmail() Group by the email column
 * @method     ChildPessoaQuery groupByComplemento() Group by the complemento column
 * @method     ChildPessoaQuery groupByTipoPessoa() Group by the tipo_pessoa column
 * @method     ChildPessoaQuery groupByDataNascimento() Group by the data_nascimento column
 * @method     ChildPessoaQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildPessoaQuery groupByObs() Group by the obs column
 * @method     ChildPessoaQuery groupByCidadeId() Group by the cidade_id column
 * @method     ChildPessoaQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildPessoaQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildPessoaQuery groupByResponsavel() Group by the responsavel column
 * @method     ChildPessoaQuery groupByContato() Group by the contato column
 * @method     ChildPessoaQuery groupBySexo() Group by the sexo column
 * @method     ChildPessoaQuery groupByImportId() Group by the import_id column
 * @method     ChildPessoaQuery groupByEFornecedor() Group by the e_fornecedor column
 *
 * @method     ChildPessoaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPessoaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPessoaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPessoaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPessoaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPessoaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPessoaQuery leftJoinBoletoBaixaHistorico($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildPessoaQuery rightJoinBoletoBaixaHistorico($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildPessoaQuery innerJoinBoletoBaixaHistorico($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildPessoaQuery joinWithBoletoBaixaHistorico($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildPessoaQuery leftJoinWithBoletoBaixaHistorico() Adds a LEFT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildPessoaQuery rightJoinWithBoletoBaixaHistorico() Adds a RIGHT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildPessoaQuery innerJoinWithBoletoBaixaHistorico() Adds a INNER JOIN clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildPessoaQuery leftJoinCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cliente relation
 * @method     ChildPessoaQuery rightJoinCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cliente relation
 * @method     ChildPessoaQuery innerJoinCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the Cliente relation
 *
 * @method     ChildPessoaQuery joinWithCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cliente relation
 *
 * @method     ChildPessoaQuery leftJoinWithCliente() Adds a LEFT JOIN clause and with to the query using the Cliente relation
 * @method     ChildPessoaQuery rightJoinWithCliente() Adds a RIGHT JOIN clause and with to the query using the Cliente relation
 * @method     ChildPessoaQuery innerJoinWithCliente() Adds a INNER JOIN clause and with to the query using the Cliente relation
 *
 * @method     ChildPessoaQuery leftJoinDadosFiscal($relationAlias = null) Adds a LEFT JOIN clause to the query using the DadosFiscal relation
 * @method     ChildPessoaQuery rightJoinDadosFiscal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DadosFiscal relation
 * @method     ChildPessoaQuery innerJoinDadosFiscal($relationAlias = null) Adds a INNER JOIN clause to the query using the DadosFiscal relation
 *
 * @method     ChildPessoaQuery joinWithDadosFiscal($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DadosFiscal relation
 *
 * @method     ChildPessoaQuery leftJoinWithDadosFiscal() Adds a LEFT JOIN clause and with to the query using the DadosFiscal relation
 * @method     ChildPessoaQuery rightJoinWithDadosFiscal() Adds a RIGHT JOIN clause and with to the query using the DadosFiscal relation
 * @method     ChildPessoaQuery innerJoinWithDadosFiscal() Adds a INNER JOIN clause and with to the query using the DadosFiscal relation
 *
 * @method     ChildPessoaQuery leftJoinEmpresa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Empresa relation
 * @method     ChildPessoaQuery rightJoinEmpresa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Empresa relation
 * @method     ChildPessoaQuery innerJoinEmpresa($relationAlias = null) Adds a INNER JOIN clause to the query using the Empresa relation
 *
 * @method     ChildPessoaQuery joinWithEmpresa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Empresa relation
 *
 * @method     ChildPessoaQuery leftJoinWithEmpresa() Adds a LEFT JOIN clause and with to the query using the Empresa relation
 * @method     ChildPessoaQuery rightJoinWithEmpresa() Adds a RIGHT JOIN clause and with to the query using the Empresa relation
 * @method     ChildPessoaQuery innerJoinWithEmpresa() Adds a INNER JOIN clause and with to the query using the Empresa relation
 *
 * @method     ChildPessoaQuery leftJoinFornecedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fornecedor relation
 * @method     ChildPessoaQuery rightJoinFornecedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fornecedor relation
 * @method     ChildPessoaQuery innerJoinFornecedor($relationAlias = null) Adds a INNER JOIN clause to the query using the Fornecedor relation
 *
 * @method     ChildPessoaQuery joinWithFornecedor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fornecedor relation
 *
 * @method     ChildPessoaQuery leftJoinWithFornecedor() Adds a LEFT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildPessoaQuery rightJoinWithFornecedor() Adds a RIGHT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildPessoaQuery innerJoinWithFornecedor() Adds a INNER JOIN clause and with to the query using the Fornecedor relation
 *
 * @method     ChildPessoaQuery leftJoinTelefone($relationAlias = null) Adds a LEFT JOIN clause to the query using the Telefone relation
 * @method     ChildPessoaQuery rightJoinTelefone($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Telefone relation
 * @method     ChildPessoaQuery innerJoinTelefone($relationAlias = null) Adds a INNER JOIN clause to the query using the Telefone relation
 *
 * @method     ChildPessoaQuery joinWithTelefone($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Telefone relation
 *
 * @method     ChildPessoaQuery leftJoinWithTelefone() Adds a LEFT JOIN clause and with to the query using the Telefone relation
 * @method     ChildPessoaQuery rightJoinWithTelefone() Adds a RIGHT JOIN clause and with to the query using the Telefone relation
 * @method     ChildPessoaQuery innerJoinWithTelefone() Adds a INNER JOIN clause and with to the query using the Telefone relation
 *
 * @method     ChildPessoaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildPessoaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildPessoaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildPessoaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildPessoaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildPessoaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildPessoaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery|\ImaTelecomBundle\Model\ClienteQuery|\ImaTelecomBundle\Model\DadosFiscalQuery|\ImaTelecomBundle\Model\EmpresaQuery|\ImaTelecomBundle\Model\FornecedorQuery|\ImaTelecomBundle\Model\TelefoneQuery|\ImaTelecomBundle\Model\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPessoa findOne(ConnectionInterface $con = null) Return the first ChildPessoa matching the query
 * @method     ChildPessoa findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPessoa matching the query, or a new ChildPessoa object populated from the query conditions when no match is found
 *
 * @method     ChildPessoa findOneById(int $id) Return the first ChildPessoa filtered by the id column
 * @method     ChildPessoa findOneByNome(string $nome) Return the first ChildPessoa filtered by the nome column
 * @method     ChildPessoa findOneByRazaoSocial(string $razao_social) Return the first ChildPessoa filtered by the razao_social column
 * @method     ChildPessoa findOneByRua(string $rua) Return the first ChildPessoa filtered by the rua column
 * @method     ChildPessoa findOneByBairro(string $bairro) Return the first ChildPessoa filtered by the bairro column
 * @method     ChildPessoa findOneByCep(string $cep) Return the first ChildPessoa filtered by the cep column
 * @method     ChildPessoa findOneByNum(int $num) Return the first ChildPessoa filtered by the num column
 * @method     ChildPessoa findOneByUf(string $uf) Return the first ChildPessoa filtered by the uf column
 * @method     ChildPessoa findOneByCpfCnpj(string $cpf_cnpj) Return the first ChildPessoa filtered by the cpf_cnpj column
 * @method     ChildPessoa findOneByRgInscrest(string $rg_inscrest) Return the first ChildPessoa filtered by the rg_inscrest column
 * @method     ChildPessoa findOneByPontoReferencia(string $ponto_referencia) Return the first ChildPessoa filtered by the ponto_referencia column
 * @method     ChildPessoa findOneByEmail(string $email) Return the first ChildPessoa filtered by the email column
 * @method     ChildPessoa findOneByComplemento(string $complemento) Return the first ChildPessoa filtered by the complemento column
 * @method     ChildPessoa findOneByTipoPessoa(string $tipo_pessoa) Return the first ChildPessoa filtered by the tipo_pessoa column
 * @method     ChildPessoa findOneByDataNascimento(string $data_nascimento) Return the first ChildPessoa filtered by the data_nascimento column
 * @method     ChildPessoa findOneByDataCadastro(string $data_cadastro) Return the first ChildPessoa filtered by the data_cadastro column
 * @method     ChildPessoa findOneByObs(string $obs) Return the first ChildPessoa filtered by the obs column
 * @method     ChildPessoa findOneByCidadeId(int $cidade_id) Return the first ChildPessoa filtered by the cidade_id column
 * @method     ChildPessoa findOneByDataAlterado(string $data_alterado) Return the first ChildPessoa filtered by the data_alterado column
 * @method     ChildPessoa findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildPessoa filtered by the usuario_alterado column
 * @method     ChildPessoa findOneByResponsavel(string $responsavel) Return the first ChildPessoa filtered by the responsavel column
 * @method     ChildPessoa findOneByContato(string $contato) Return the first ChildPessoa filtered by the contato column
 * @method     ChildPessoa findOneBySexo(string $sexo) Return the first ChildPessoa filtered by the sexo column
 * @method     ChildPessoa findOneByImportId(int $import_id) Return the first ChildPessoa filtered by the import_id column
 * @method     ChildPessoa findOneByEFornecedor(boolean $e_fornecedor) Return the first ChildPessoa filtered by the e_fornecedor column *

 * @method     ChildPessoa requirePk($key, ConnectionInterface $con = null) Return the ChildPessoa by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOne(ConnectionInterface $con = null) Return the first ChildPessoa matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPessoa requireOneById(int $id) Return the first ChildPessoa filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByNome(string $nome) Return the first ChildPessoa filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByRazaoSocial(string $razao_social) Return the first ChildPessoa filtered by the razao_social column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByRua(string $rua) Return the first ChildPessoa filtered by the rua column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByBairro(string $bairro) Return the first ChildPessoa filtered by the bairro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByCep(string $cep) Return the first ChildPessoa filtered by the cep column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByNum(int $num) Return the first ChildPessoa filtered by the num column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByUf(string $uf) Return the first ChildPessoa filtered by the uf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByCpfCnpj(string $cpf_cnpj) Return the first ChildPessoa filtered by the cpf_cnpj column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByRgInscrest(string $rg_inscrest) Return the first ChildPessoa filtered by the rg_inscrest column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByPontoReferencia(string $ponto_referencia) Return the first ChildPessoa filtered by the ponto_referencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByEmail(string $email) Return the first ChildPessoa filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByComplemento(string $complemento) Return the first ChildPessoa filtered by the complemento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByTipoPessoa(string $tipo_pessoa) Return the first ChildPessoa filtered by the tipo_pessoa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByDataNascimento(string $data_nascimento) Return the first ChildPessoa filtered by the data_nascimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByDataCadastro(string $data_cadastro) Return the first ChildPessoa filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByObs(string $obs) Return the first ChildPessoa filtered by the obs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByCidadeId(int $cidade_id) Return the first ChildPessoa filtered by the cidade_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByDataAlterado(string $data_alterado) Return the first ChildPessoa filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildPessoa filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByResponsavel(string $responsavel) Return the first ChildPessoa filtered by the responsavel column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByContato(string $contato) Return the first ChildPessoa filtered by the contato column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneBySexo(string $sexo) Return the first ChildPessoa filtered by the sexo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByImportId(int $import_id) Return the first ChildPessoa filtered by the import_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPessoa requireOneByEFornecedor(boolean $e_fornecedor) Return the first ChildPessoa filtered by the e_fornecedor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPessoa[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPessoa objects based on current ModelCriteria
 * @method     ChildPessoa[]|ObjectCollection findById(int $id) Return ChildPessoa objects filtered by the id column
 * @method     ChildPessoa[]|ObjectCollection findByNome(string $nome) Return ChildPessoa objects filtered by the nome column
 * @method     ChildPessoa[]|ObjectCollection findByRazaoSocial(string $razao_social) Return ChildPessoa objects filtered by the razao_social column
 * @method     ChildPessoa[]|ObjectCollection findByRua(string $rua) Return ChildPessoa objects filtered by the rua column
 * @method     ChildPessoa[]|ObjectCollection findByBairro(string $bairro) Return ChildPessoa objects filtered by the bairro column
 * @method     ChildPessoa[]|ObjectCollection findByCep(string $cep) Return ChildPessoa objects filtered by the cep column
 * @method     ChildPessoa[]|ObjectCollection findByNum(int $num) Return ChildPessoa objects filtered by the num column
 * @method     ChildPessoa[]|ObjectCollection findByUf(string $uf) Return ChildPessoa objects filtered by the uf column
 * @method     ChildPessoa[]|ObjectCollection findByCpfCnpj(string $cpf_cnpj) Return ChildPessoa objects filtered by the cpf_cnpj column
 * @method     ChildPessoa[]|ObjectCollection findByRgInscrest(string $rg_inscrest) Return ChildPessoa objects filtered by the rg_inscrest column
 * @method     ChildPessoa[]|ObjectCollection findByPontoReferencia(string $ponto_referencia) Return ChildPessoa objects filtered by the ponto_referencia column
 * @method     ChildPessoa[]|ObjectCollection findByEmail(string $email) Return ChildPessoa objects filtered by the email column
 * @method     ChildPessoa[]|ObjectCollection findByComplemento(string $complemento) Return ChildPessoa objects filtered by the complemento column
 * @method     ChildPessoa[]|ObjectCollection findByTipoPessoa(string $tipo_pessoa) Return ChildPessoa objects filtered by the tipo_pessoa column
 * @method     ChildPessoa[]|ObjectCollection findByDataNascimento(string $data_nascimento) Return ChildPessoa objects filtered by the data_nascimento column
 * @method     ChildPessoa[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildPessoa objects filtered by the data_cadastro column
 * @method     ChildPessoa[]|ObjectCollection findByObs(string $obs) Return ChildPessoa objects filtered by the obs column
 * @method     ChildPessoa[]|ObjectCollection findByCidadeId(int $cidade_id) Return ChildPessoa objects filtered by the cidade_id column
 * @method     ChildPessoa[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildPessoa objects filtered by the data_alterado column
 * @method     ChildPessoa[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildPessoa objects filtered by the usuario_alterado column
 * @method     ChildPessoa[]|ObjectCollection findByResponsavel(string $responsavel) Return ChildPessoa objects filtered by the responsavel column
 * @method     ChildPessoa[]|ObjectCollection findByContato(string $contato) Return ChildPessoa objects filtered by the contato column
 * @method     ChildPessoa[]|ObjectCollection findBySexo(string $sexo) Return ChildPessoa objects filtered by the sexo column
 * @method     ChildPessoa[]|ObjectCollection findByImportId(int $import_id) Return ChildPessoa objects filtered by the import_id column
 * @method     ChildPessoa[]|ObjectCollection findByEFornecedor(boolean $e_fornecedor) Return ChildPessoa objects filtered by the e_fornecedor column
 * @method     ChildPessoa[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PessoaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\PessoaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Pessoa', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPessoaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPessoaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPessoaQuery) {
            return $criteria;
        }
        $query = new ChildPessoaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPessoa|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PessoaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PessoaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPessoa A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nome, razao_social, rua, bairro, cep, num, uf, cpf_cnpj, rg_inscrest, ponto_referencia, email, complemento, tipo_pessoa, data_nascimento, data_cadastro, obs, cidade_id, data_alterado, usuario_alterado, responsavel, contato, sexo, import_id, e_fornecedor FROM pessoa WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPessoa $obj */
            $obj = new ChildPessoa();
            $obj->hydrate($row);
            PessoaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPessoa|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PessoaTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PessoaTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PessoaTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PessoaTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the razao_social column
     *
     * Example usage:
     * <code>
     * $query->filterByRazaoSocial('fooValue');   // WHERE razao_social = 'fooValue'
     * $query->filterByRazaoSocial('%fooValue%', Criteria::LIKE); // WHERE razao_social LIKE '%fooValue%'
     * </code>
     *
     * @param     string $razaoSocial The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByRazaoSocial($razaoSocial = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($razaoSocial)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_RAZAO_SOCIAL, $razaoSocial, $comparison);
    }

    /**
     * Filter the query on the rua column
     *
     * Example usage:
     * <code>
     * $query->filterByRua('fooValue');   // WHERE rua = 'fooValue'
     * $query->filterByRua('%fooValue%', Criteria::LIKE); // WHERE rua LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rua The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByRua($rua = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rua)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_RUA, $rua, $comparison);
    }

    /**
     * Filter the query on the bairro column
     *
     * Example usage:
     * <code>
     * $query->filterByBairro('fooValue');   // WHERE bairro = 'fooValue'
     * $query->filterByBairro('%fooValue%', Criteria::LIKE); // WHERE bairro LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bairro The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByBairro($bairro = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bairro)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_BAIRRO, $bairro, $comparison);
    }

    /**
     * Filter the query on the cep column
     *
     * Example usage:
     * <code>
     * $query->filterByCep('fooValue');   // WHERE cep = 'fooValue'
     * $query->filterByCep('%fooValue%', Criteria::LIKE); // WHERE cep LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cep The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByCep($cep = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cep)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_CEP, $cep, $comparison);
    }

    /**
     * Filter the query on the num column
     *
     * Example usage:
     * <code>
     * $query->filterByNum(1234); // WHERE num = 1234
     * $query->filterByNum(array(12, 34)); // WHERE num IN (12, 34)
     * $query->filterByNum(array('min' => 12)); // WHERE num > 12
     * </code>
     *
     * @param     mixed $num The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByNum($num = null, $comparison = null)
    {
        if (is_array($num)) {
            $useMinMax = false;
            if (isset($num['min'])) {
                $this->addUsingAlias(PessoaTableMap::COL_NUM, $num['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($num['max'])) {
                $this->addUsingAlias(PessoaTableMap::COL_NUM, $num['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_NUM, $num, $comparison);
    }

    /**
     * Filter the query on the uf column
     *
     * Example usage:
     * <code>
     * $query->filterByUf('fooValue');   // WHERE uf = 'fooValue'
     * $query->filterByUf('%fooValue%', Criteria::LIKE); // WHERE uf LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uf The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByUf($uf = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uf)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_UF, $uf, $comparison);
    }

    /**
     * Filter the query on the cpf_cnpj column
     *
     * Example usage:
     * <code>
     * $query->filterByCpfCnpj('fooValue');   // WHERE cpf_cnpj = 'fooValue'
     * $query->filterByCpfCnpj('%fooValue%', Criteria::LIKE); // WHERE cpf_cnpj LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cpfCnpj The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByCpfCnpj($cpfCnpj = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cpfCnpj)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_CPF_CNPJ, $cpfCnpj, $comparison);
    }

    /**
     * Filter the query on the rg_inscrest column
     *
     * Example usage:
     * <code>
     * $query->filterByRgInscrest('fooValue');   // WHERE rg_inscrest = 'fooValue'
     * $query->filterByRgInscrest('%fooValue%', Criteria::LIKE); // WHERE rg_inscrest LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rgInscrest The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByRgInscrest($rgInscrest = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rgInscrest)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_RG_INSCREST, $rgInscrest, $comparison);
    }

    /**
     * Filter the query on the ponto_referencia column
     *
     * Example usage:
     * <code>
     * $query->filterByPontoReferencia('fooValue');   // WHERE ponto_referencia = 'fooValue'
     * $query->filterByPontoReferencia('%fooValue%', Criteria::LIKE); // WHERE ponto_referencia LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pontoReferencia The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByPontoReferencia($pontoReferencia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pontoReferencia)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_PONTO_REFERENCIA, $pontoReferencia, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the complemento column
     *
     * Example usage:
     * <code>
     * $query->filterByComplemento('fooValue');   // WHERE complemento = 'fooValue'
     * $query->filterByComplemento('%fooValue%', Criteria::LIKE); // WHERE complemento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $complemento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByComplemento($complemento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($complemento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_COMPLEMENTO, $complemento, $comparison);
    }

    /**
     * Filter the query on the tipo_pessoa column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoPessoa('fooValue');   // WHERE tipo_pessoa = 'fooValue'
     * $query->filterByTipoPessoa('%fooValue%', Criteria::LIKE); // WHERE tipo_pessoa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoPessoa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByTipoPessoa($tipoPessoa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoPessoa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_TIPO_PESSOA, $tipoPessoa, $comparison);
    }

    /**
     * Filter the query on the data_nascimento column
     *
     * Example usage:
     * <code>
     * $query->filterByDataNascimento('2011-03-14'); // WHERE data_nascimento = '2011-03-14'
     * $query->filterByDataNascimento('now'); // WHERE data_nascimento = '2011-03-14'
     * $query->filterByDataNascimento(array('max' => 'yesterday')); // WHERE data_nascimento > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataNascimento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByDataNascimento($dataNascimento = null, $comparison = null)
    {
        if (is_array($dataNascimento)) {
            $useMinMax = false;
            if (isset($dataNascimento['min'])) {
                $this->addUsingAlias(PessoaTableMap::COL_DATA_NASCIMENTO, $dataNascimento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataNascimento['max'])) {
                $this->addUsingAlias(PessoaTableMap::COL_DATA_NASCIMENTO, $dataNascimento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_DATA_NASCIMENTO, $dataNascimento, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(PessoaTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(PessoaTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the obs column
     *
     * Example usage:
     * <code>
     * $query->filterByObs('fooValue');   // WHERE obs = 'fooValue'
     * $query->filterByObs('%fooValue%', Criteria::LIKE); // WHERE obs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $obs The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByObs($obs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($obs)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_OBS, $obs, $comparison);
    }

    /**
     * Filter the query on the cidade_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCidadeId(1234); // WHERE cidade_id = 1234
     * $query->filterByCidadeId(array(12, 34)); // WHERE cidade_id IN (12, 34)
     * $query->filterByCidadeId(array('min' => 12)); // WHERE cidade_id > 12
     * </code>
     *
     * @param     mixed $cidadeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByCidadeId($cidadeId = null, $comparison = null)
    {
        if (is_array($cidadeId)) {
            $useMinMax = false;
            if (isset($cidadeId['min'])) {
                $this->addUsingAlias(PessoaTableMap::COL_CIDADE_ID, $cidadeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cidadeId['max'])) {
                $this->addUsingAlias(PessoaTableMap::COL_CIDADE_ID, $cidadeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_CIDADE_ID, $cidadeId, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(PessoaTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(PessoaTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(PessoaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(PessoaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the responsavel column
     *
     * Example usage:
     * <code>
     * $query->filterByResponsavel('fooValue');   // WHERE responsavel = 'fooValue'
     * $query->filterByResponsavel('%fooValue%', Criteria::LIKE); // WHERE responsavel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $responsavel The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByResponsavel($responsavel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($responsavel)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_RESPONSAVEL, $responsavel, $comparison);
    }

    /**
     * Filter the query on the contato column
     *
     * Example usage:
     * <code>
     * $query->filterByContato('fooValue');   // WHERE contato = 'fooValue'
     * $query->filterByContato('%fooValue%', Criteria::LIKE); // WHERE contato LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contato The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByContato($contato = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contato)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_CONTATO, $contato, $comparison);
    }

    /**
     * Filter the query on the sexo column
     *
     * Example usage:
     * <code>
     * $query->filterBySexo('fooValue');   // WHERE sexo = 'fooValue'
     * $query->filterBySexo('%fooValue%', Criteria::LIKE); // WHERE sexo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sexo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterBySexo($sexo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sexo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_SEXO, $sexo, $comparison);
    }

    /**
     * Filter the query on the import_id column
     *
     * Example usage:
     * <code>
     * $query->filterByImportId(1234); // WHERE import_id = 1234
     * $query->filterByImportId(array(12, 34)); // WHERE import_id IN (12, 34)
     * $query->filterByImportId(array('min' => 12)); // WHERE import_id > 12
     * </code>
     *
     * @param     mixed $importId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByImportId($importId = null, $comparison = null)
    {
        if (is_array($importId)) {
            $useMinMax = false;
            if (isset($importId['min'])) {
                $this->addUsingAlias(PessoaTableMap::COL_IMPORT_ID, $importId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importId['max'])) {
                $this->addUsingAlias(PessoaTableMap::COL_IMPORT_ID, $importId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PessoaTableMap::COL_IMPORT_ID, $importId, $comparison);
    }

    /**
     * Filter the query on the e_fornecedor column
     *
     * Example usage:
     * <code>
     * $query->filterByEFornecedor(true); // WHERE e_fornecedor = true
     * $query->filterByEFornecedor('yes'); // WHERE e_fornecedor = true
     * </code>
     *
     * @param     boolean|string $eFornecedor The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByEFornecedor($eFornecedor = null, $comparison = null)
    {
        if (is_string($eFornecedor)) {
            $eFornecedor = in_array(strtolower($eFornecedor), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PessoaTableMap::COL_E_FORNECEDOR, $eFornecedor, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoBaixaHistorico object
     *
     * @param \ImaTelecomBundle\Model\BoletoBaixaHistorico|ObjectCollection $boletoBaixaHistorico the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByBoletoBaixaHistorico($boletoBaixaHistorico, $comparison = null)
    {
        if ($boletoBaixaHistorico instanceof \ImaTelecomBundle\Model\BoletoBaixaHistorico) {
            return $this
                ->addUsingAlias(PessoaTableMap::COL_ID, $boletoBaixaHistorico->getPessoaId(), $comparison);
        } elseif ($boletoBaixaHistorico instanceof ObjectCollection) {
            return $this
                ->useBoletoBaixaHistoricoQuery()
                ->filterByPrimaryKeys($boletoBaixaHistorico->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoBaixaHistorico() only accepts arguments of type \ImaTelecomBundle\Model\BoletoBaixaHistorico or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoBaixaHistorico relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function joinBoletoBaixaHistorico($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoBaixaHistorico');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoBaixaHistorico');
        }

        return $this;
    }

    /**
     * Use the BoletoBaixaHistorico relation BoletoBaixaHistorico object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoBaixaHistoricoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBoletoBaixaHistorico($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoBaixaHistorico', '\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cliente object
     *
     * @param \ImaTelecomBundle\Model\Cliente|ObjectCollection $cliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByCliente($cliente, $comparison = null)
    {
        if ($cliente instanceof \ImaTelecomBundle\Model\Cliente) {
            return $this
                ->addUsingAlias(PessoaTableMap::COL_ID, $cliente->getPessoaId(), $comparison);
        } elseif ($cliente instanceof ObjectCollection) {
            return $this
                ->useClienteQuery()
                ->filterByPrimaryKeys($cliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCliente() only accepts arguments of type \ImaTelecomBundle\Model\Cliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function joinCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cliente');
        }

        return $this;
    }

    /**
     * Use the Cliente relation Cliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteQuery A secondary query class using the current class as primary query
     */
    public function useClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cliente', '\ImaTelecomBundle\Model\ClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\DadosFiscal object
     *
     * @param \ImaTelecomBundle\Model\DadosFiscal|ObjectCollection $dadosFiscal the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByDadosFiscal($dadosFiscal, $comparison = null)
    {
        if ($dadosFiscal instanceof \ImaTelecomBundle\Model\DadosFiscal) {
            return $this
                ->addUsingAlias(PessoaTableMap::COL_IMPORT_ID, $dadosFiscal->getPessoaImportId(), $comparison);
        } elseif ($dadosFiscal instanceof ObjectCollection) {
            return $this
                ->useDadosFiscalQuery()
                ->filterByPrimaryKeys($dadosFiscal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDadosFiscal() only accepts arguments of type \ImaTelecomBundle\Model\DadosFiscal or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DadosFiscal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function joinDadosFiscal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DadosFiscal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DadosFiscal');
        }

        return $this;
    }

    /**
     * Use the DadosFiscal relation DadosFiscal object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\DadosFiscalQuery A secondary query class using the current class as primary query
     */
    public function useDadosFiscalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDadosFiscal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DadosFiscal', '\ImaTelecomBundle\Model\DadosFiscalQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Empresa object
     *
     * @param \ImaTelecomBundle\Model\Empresa|ObjectCollection $empresa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByEmpresa($empresa, $comparison = null)
    {
        if ($empresa instanceof \ImaTelecomBundle\Model\Empresa) {
            return $this
                ->addUsingAlias(PessoaTableMap::COL_ID, $empresa->getPessoaId(), $comparison);
        } elseif ($empresa instanceof ObjectCollection) {
            return $this
                ->useEmpresaQuery()
                ->filterByPrimaryKeys($empresa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEmpresa() only accepts arguments of type \ImaTelecomBundle\Model\Empresa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Empresa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function joinEmpresa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Empresa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Empresa');
        }

        return $this;
    }

    /**
     * Use the Empresa relation Empresa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EmpresaQuery A secondary query class using the current class as primary query
     */
    public function useEmpresaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEmpresa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Empresa', '\ImaTelecomBundle\Model\EmpresaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Fornecedor object
     *
     * @param \ImaTelecomBundle\Model\Fornecedor|ObjectCollection $fornecedor the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByFornecedor($fornecedor, $comparison = null)
    {
        if ($fornecedor instanceof \ImaTelecomBundle\Model\Fornecedor) {
            return $this
                ->addUsingAlias(PessoaTableMap::COL_ID, $fornecedor->getPessoaId(), $comparison);
        } elseif ($fornecedor instanceof ObjectCollection) {
            return $this
                ->useFornecedorQuery()
                ->filterByPrimaryKeys($fornecedor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFornecedor() only accepts arguments of type \ImaTelecomBundle\Model\Fornecedor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fornecedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function joinFornecedor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fornecedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fornecedor');
        }

        return $this;
    }

    /**
     * Use the Fornecedor relation Fornecedor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FornecedorQuery A secondary query class using the current class as primary query
     */
    public function useFornecedorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFornecedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fornecedor', '\ImaTelecomBundle\Model\FornecedorQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Telefone object
     *
     * @param \ImaTelecomBundle\Model\Telefone|ObjectCollection $telefone the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByTelefone($telefone, $comparison = null)
    {
        if ($telefone instanceof \ImaTelecomBundle\Model\Telefone) {
            return $this
                ->addUsingAlias(PessoaTableMap::COL_ID, $telefone->getPessoaId(), $comparison);
        } elseif ($telefone instanceof ObjectCollection) {
            return $this
                ->useTelefoneQuery()
                ->filterByPrimaryKeys($telefone->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByTelefone() only accepts arguments of type \ImaTelecomBundle\Model\Telefone or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Telefone relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function joinTelefone($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Telefone');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Telefone');
        }

        return $this;
    }

    /**
     * Use the Telefone relation Telefone object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\TelefoneQuery A secondary query class using the current class as primary query
     */
    public function useTelefoneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTelefone($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Telefone', '\ImaTelecomBundle\Model\TelefoneQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPessoaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(PessoaTableMap::COL_ID, $usuario->getPessoaId(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            return $this
                ->useUsuarioQuery()
                ->filterByPrimaryKeys($usuario->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPessoa $pessoa Object to remove from the list of results
     *
     * @return $this|ChildPessoaQuery The current query, for fluid interface
     */
    public function prune($pessoa = null)
    {
        if ($pessoa) {
            $this->addUsingAlias(PessoaTableMap::COL_ID, $pessoa->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the pessoa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PessoaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PessoaTableMap::clearInstancePool();
            PessoaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PessoaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PessoaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PessoaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PessoaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PessoaQuery
