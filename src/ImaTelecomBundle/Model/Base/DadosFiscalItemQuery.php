<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\DadosFiscalItem as ChildDadosFiscalItem;
use ImaTelecomBundle\Model\DadosFiscalItemQuery as ChildDadosFiscalItemQuery;
use ImaTelecomBundle\Model\Map\DadosFiscalItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'dados_fiscal_item' table.
 *
 *
 *
 * @method     ChildDadosFiscalItemQuery orderByIddadosFiscalItem($order = Criteria::ASC) Order by the iddados_fiscal_item column
 * @method     ChildDadosFiscalItemQuery orderByBoletoId($order = Criteria::ASC) Order by the boleto_id column
 * @method     ChildDadosFiscalItemQuery orderByBoletoItemId($order = Criteria::ASC) Order by the boleto_item_id column
 * @method     ChildDadosFiscalItemQuery orderByItem($order = Criteria::ASC) Order by the item column
 * @method     ChildDadosFiscalItemQuery orderByNumeroItemNotaFiscal($order = Criteria::ASC) Order by the numero_item_nota_fiscal column
 * @method     ChildDadosFiscalItemQuery orderByValorItem($order = Criteria::ASC) Order by the valor_item column
 * @method     ChildDadosFiscalItemQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildDadosFiscalItemQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildDadosFiscalItemQuery orderByDadosFiscalId($order = Criteria::ASC) Order by the dados_fiscal_id column
 * @method     ChildDadosFiscalItemQuery orderByPosicaoItem($order = Criteria::ASC) Order by the posicao_item column
 * @method     ChildDadosFiscalItemQuery orderByValorScm($order = Criteria::ASC) Order by the valor_scm column
 * @method     ChildDadosFiscalItemQuery orderByValorSva($order = Criteria::ASC) Order by the valor_sva column
 * @method     ChildDadosFiscalItemQuery orderByTemScm($order = Criteria::ASC) Order by the tem_scm column
 * @method     ChildDadosFiscalItemQuery orderByTemSva($order = Criteria::ASC) Order by the tem_sva column
 *
 * @method     ChildDadosFiscalItemQuery groupByIddadosFiscalItem() Group by the iddados_fiscal_item column
 * @method     ChildDadosFiscalItemQuery groupByBoletoId() Group by the boleto_id column
 * @method     ChildDadosFiscalItemQuery groupByBoletoItemId() Group by the boleto_item_id column
 * @method     ChildDadosFiscalItemQuery groupByItem() Group by the item column
 * @method     ChildDadosFiscalItemQuery groupByNumeroItemNotaFiscal() Group by the numero_item_nota_fiscal column
 * @method     ChildDadosFiscalItemQuery groupByValorItem() Group by the valor_item column
 * @method     ChildDadosFiscalItemQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildDadosFiscalItemQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildDadosFiscalItemQuery groupByDadosFiscalId() Group by the dados_fiscal_id column
 * @method     ChildDadosFiscalItemQuery groupByPosicaoItem() Group by the posicao_item column
 * @method     ChildDadosFiscalItemQuery groupByValorScm() Group by the valor_scm column
 * @method     ChildDadosFiscalItemQuery groupByValorSva() Group by the valor_sva column
 * @method     ChildDadosFiscalItemQuery groupByTemScm() Group by the tem_scm column
 * @method     ChildDadosFiscalItemQuery groupByTemSva() Group by the tem_sva column
 *
 * @method     ChildDadosFiscalItemQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDadosFiscalItemQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDadosFiscalItemQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDadosFiscalItemQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDadosFiscalItemQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDadosFiscalItemQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDadosFiscalItemQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildDadosFiscalItemQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildDadosFiscalItemQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildDadosFiscalItemQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildDadosFiscalItemQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildDadosFiscalItemQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildDadosFiscalItemQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildDadosFiscalItemQuery leftJoinBoletoItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoItem relation
 * @method     ChildDadosFiscalItemQuery rightJoinBoletoItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoItem relation
 * @method     ChildDadosFiscalItemQuery innerJoinBoletoItem($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoItem relation
 *
 * @method     ChildDadosFiscalItemQuery joinWithBoletoItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoItem relation
 *
 * @method     ChildDadosFiscalItemQuery leftJoinWithBoletoItem() Adds a LEFT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildDadosFiscalItemQuery rightJoinWithBoletoItem() Adds a RIGHT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildDadosFiscalItemQuery innerJoinWithBoletoItem() Adds a INNER JOIN clause and with to the query using the BoletoItem relation
 *
 * @method     ChildDadosFiscalItemQuery leftJoinDadosFiscal($relationAlias = null) Adds a LEFT JOIN clause to the query using the DadosFiscal relation
 * @method     ChildDadosFiscalItemQuery rightJoinDadosFiscal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DadosFiscal relation
 * @method     ChildDadosFiscalItemQuery innerJoinDadosFiscal($relationAlias = null) Adds a INNER JOIN clause to the query using the DadosFiscal relation
 *
 * @method     ChildDadosFiscalItemQuery joinWithDadosFiscal($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DadosFiscal relation
 *
 * @method     ChildDadosFiscalItemQuery leftJoinWithDadosFiscal() Adds a LEFT JOIN clause and with to the query using the DadosFiscal relation
 * @method     ChildDadosFiscalItemQuery rightJoinWithDadosFiscal() Adds a RIGHT JOIN clause and with to the query using the DadosFiscal relation
 * @method     ChildDadosFiscalItemQuery innerJoinWithDadosFiscal() Adds a INNER JOIN clause and with to the query using the DadosFiscal relation
 *
 * @method     \ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\BoletoItemQuery|\ImaTelecomBundle\Model\DadosFiscalQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildDadosFiscalItem findOne(ConnectionInterface $con = null) Return the first ChildDadosFiscalItem matching the query
 * @method     ChildDadosFiscalItem findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDadosFiscalItem matching the query, or a new ChildDadosFiscalItem object populated from the query conditions when no match is found
 *
 * @method     ChildDadosFiscalItem findOneByIddadosFiscalItem(int $iddados_fiscal_item) Return the first ChildDadosFiscalItem filtered by the iddados_fiscal_item column
 * @method     ChildDadosFiscalItem findOneByBoletoId(int $boleto_id) Return the first ChildDadosFiscalItem filtered by the boleto_id column
 * @method     ChildDadosFiscalItem findOneByBoletoItemId(int $boleto_item_id) Return the first ChildDadosFiscalItem filtered by the boleto_item_id column
 * @method     ChildDadosFiscalItem findOneByItem(string $item) Return the first ChildDadosFiscalItem filtered by the item column
 * @method     ChildDadosFiscalItem findOneByNumeroItemNotaFiscal(int $numero_item_nota_fiscal) Return the first ChildDadosFiscalItem filtered by the numero_item_nota_fiscal column
 * @method     ChildDadosFiscalItem findOneByValorItem(string $valor_item) Return the first ChildDadosFiscalItem filtered by the valor_item column
 * @method     ChildDadosFiscalItem findOneByDataCadastro(string $data_cadastro) Return the first ChildDadosFiscalItem filtered by the data_cadastro column
 * @method     ChildDadosFiscalItem findOneByDataAlterado(string $data_alterado) Return the first ChildDadosFiscalItem filtered by the data_alterado column
 * @method     ChildDadosFiscalItem findOneByDadosFiscalId(int $dados_fiscal_id) Return the first ChildDadosFiscalItem filtered by the dados_fiscal_id column
 * @method     ChildDadosFiscalItem findOneByPosicaoItem(int $posicao_item) Return the first ChildDadosFiscalItem filtered by the posicao_item column
 * @method     ChildDadosFiscalItem findOneByValorScm(string $valor_scm) Return the first ChildDadosFiscalItem filtered by the valor_scm column
 * @method     ChildDadosFiscalItem findOneByValorSva(string $valor_sva) Return the first ChildDadosFiscalItem filtered by the valor_sva column
 * @method     ChildDadosFiscalItem findOneByTemScm(boolean $tem_scm) Return the first ChildDadosFiscalItem filtered by the tem_scm column
 * @method     ChildDadosFiscalItem findOneByTemSva(boolean $tem_sva) Return the first ChildDadosFiscalItem filtered by the tem_sva column *

 * @method     ChildDadosFiscalItem requirePk($key, ConnectionInterface $con = null) Return the ChildDadosFiscalItem by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOne(ConnectionInterface $con = null) Return the first ChildDadosFiscalItem matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDadosFiscalItem requireOneByIddadosFiscalItem(int $iddados_fiscal_item) Return the first ChildDadosFiscalItem filtered by the iddados_fiscal_item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByBoletoId(int $boleto_id) Return the first ChildDadosFiscalItem filtered by the boleto_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByBoletoItemId(int $boleto_item_id) Return the first ChildDadosFiscalItem filtered by the boleto_item_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByItem(string $item) Return the first ChildDadosFiscalItem filtered by the item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByNumeroItemNotaFiscal(int $numero_item_nota_fiscal) Return the first ChildDadosFiscalItem filtered by the numero_item_nota_fiscal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByValorItem(string $valor_item) Return the first ChildDadosFiscalItem filtered by the valor_item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByDataCadastro(string $data_cadastro) Return the first ChildDadosFiscalItem filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByDataAlterado(string $data_alterado) Return the first ChildDadosFiscalItem filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByDadosFiscalId(int $dados_fiscal_id) Return the first ChildDadosFiscalItem filtered by the dados_fiscal_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByPosicaoItem(int $posicao_item) Return the first ChildDadosFiscalItem filtered by the posicao_item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByValorScm(string $valor_scm) Return the first ChildDadosFiscalItem filtered by the valor_scm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByValorSva(string $valor_sva) Return the first ChildDadosFiscalItem filtered by the valor_sva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByTemScm(boolean $tem_scm) Return the first ChildDadosFiscalItem filtered by the tem_scm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscalItem requireOneByTemSva(boolean $tem_sva) Return the first ChildDadosFiscalItem filtered by the tem_sva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDadosFiscalItem[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDadosFiscalItem objects based on current ModelCriteria
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByIddadosFiscalItem(int $iddados_fiscal_item) Return ChildDadosFiscalItem objects filtered by the iddados_fiscal_item column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByBoletoId(int $boleto_id) Return ChildDadosFiscalItem objects filtered by the boleto_id column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByBoletoItemId(int $boleto_item_id) Return ChildDadosFiscalItem objects filtered by the boleto_item_id column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByItem(string $item) Return ChildDadosFiscalItem objects filtered by the item column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByNumeroItemNotaFiscal(int $numero_item_nota_fiscal) Return ChildDadosFiscalItem objects filtered by the numero_item_nota_fiscal column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByValorItem(string $valor_item) Return ChildDadosFiscalItem objects filtered by the valor_item column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildDadosFiscalItem objects filtered by the data_cadastro column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildDadosFiscalItem objects filtered by the data_alterado column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByDadosFiscalId(int $dados_fiscal_id) Return ChildDadosFiscalItem objects filtered by the dados_fiscal_id column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByPosicaoItem(int $posicao_item) Return ChildDadosFiscalItem objects filtered by the posicao_item column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByValorScm(string $valor_scm) Return ChildDadosFiscalItem objects filtered by the valor_scm column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByValorSva(string $valor_sva) Return ChildDadosFiscalItem objects filtered by the valor_sva column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByTemScm(boolean $tem_scm) Return ChildDadosFiscalItem objects filtered by the tem_scm column
 * @method     ChildDadosFiscalItem[]|ObjectCollection findByTemSva(boolean $tem_sva) Return ChildDadosFiscalItem objects filtered by the tem_sva column
 * @method     ChildDadosFiscalItem[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DadosFiscalItemQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\DadosFiscalItemQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\DadosFiscalItem', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDadosFiscalItemQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDadosFiscalItemQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDadosFiscalItemQuery) {
            return $criteria;
        }
        $query = new ChildDadosFiscalItemQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDadosFiscalItem|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DadosFiscalItemTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = DadosFiscalItemTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscalItem A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT iddados_fiscal_item, boleto_id, boleto_item_id, item, numero_item_nota_fiscal, valor_item, data_cadastro, data_alterado, dados_fiscal_id, posicao_item, valor_scm, valor_sva, tem_scm, tem_sva FROM dados_fiscal_item WHERE iddados_fiscal_item = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDadosFiscalItem $obj */
            $obj = new ChildDadosFiscalItem();
            $obj->hydrate($row);
            DadosFiscalItemTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDadosFiscalItem|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the iddados_fiscal_item column
     *
     * Example usage:
     * <code>
     * $query->filterByIddadosFiscalItem(1234); // WHERE iddados_fiscal_item = 1234
     * $query->filterByIddadosFiscalItem(array(12, 34)); // WHERE iddados_fiscal_item IN (12, 34)
     * $query->filterByIddadosFiscalItem(array('min' => 12)); // WHERE iddados_fiscal_item > 12
     * </code>
     *
     * @param     mixed $iddadosFiscalItem The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByIddadosFiscalItem($iddadosFiscalItem = null, $comparison = null)
    {
        if (is_array($iddadosFiscalItem)) {
            $useMinMax = false;
            if (isset($iddadosFiscalItem['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, $iddadosFiscalItem['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iddadosFiscalItem['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, $iddadosFiscalItem['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, $iddadosFiscalItem, $comparison);
    }

    /**
     * Filter the query on the boleto_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBoletoId(1234); // WHERE boleto_id = 1234
     * $query->filterByBoletoId(array(12, 34)); // WHERE boleto_id IN (12, 34)
     * $query->filterByBoletoId(array('min' => 12)); // WHERE boleto_id > 12
     * </code>
     *
     * @see       filterByBoleto()
     *
     * @param     mixed $boletoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByBoletoId($boletoId = null, $comparison = null)
    {
        if (is_array($boletoId)) {
            $useMinMax = false;
            if (isset($boletoId['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ID, $boletoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($boletoId['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ID, $boletoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ID, $boletoId, $comparison);
    }

    /**
     * Filter the query on the boleto_item_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBoletoItemId(1234); // WHERE boleto_item_id = 1234
     * $query->filterByBoletoItemId(array(12, 34)); // WHERE boleto_item_id IN (12, 34)
     * $query->filterByBoletoItemId(array('min' => 12)); // WHERE boleto_item_id > 12
     * </code>
     *
     * @see       filterByBoletoItem()
     *
     * @param     mixed $boletoItemId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByBoletoItemId($boletoItemId = null, $comparison = null)
    {
        if (is_array($boletoItemId)) {
            $useMinMax = false;
            if (isset($boletoItemId['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID, $boletoItemId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($boletoItemId['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID, $boletoItemId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID, $boletoItemId, $comparison);
    }

    /**
     * Filter the query on the item column
     *
     * Example usage:
     * <code>
     * $query->filterByItem('fooValue');   // WHERE item = 'fooValue'
     * $query->filterByItem('%fooValue%', Criteria::LIKE); // WHERE item LIKE '%fooValue%'
     * </code>
     *
     * @param     string $item The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByItem($item = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($item)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_ITEM, $item, $comparison);
    }

    /**
     * Filter the query on the numero_item_nota_fiscal column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroItemNotaFiscal(1234); // WHERE numero_item_nota_fiscal = 1234
     * $query->filterByNumeroItemNotaFiscal(array(12, 34)); // WHERE numero_item_nota_fiscal IN (12, 34)
     * $query->filterByNumeroItemNotaFiscal(array('min' => 12)); // WHERE numero_item_nota_fiscal > 12
     * </code>
     *
     * @param     mixed $numeroItemNotaFiscal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByNumeroItemNotaFiscal($numeroItemNotaFiscal = null, $comparison = null)
    {
        if (is_array($numeroItemNotaFiscal)) {
            $useMinMax = false;
            if (isset($numeroItemNotaFiscal['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL, $numeroItemNotaFiscal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numeroItemNotaFiscal['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL, $numeroItemNotaFiscal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL, $numeroItemNotaFiscal, $comparison);
    }

    /**
     * Filter the query on the valor_item column
     *
     * Example usage:
     * <code>
     * $query->filterByValorItem(1234); // WHERE valor_item = 1234
     * $query->filterByValorItem(array(12, 34)); // WHERE valor_item IN (12, 34)
     * $query->filterByValorItem(array('min' => 12)); // WHERE valor_item > 12
     * </code>
     *
     * @param     mixed $valorItem The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByValorItem($valorItem = null, $comparison = null)
    {
        if (is_array($valorItem)) {
            $useMinMax = false;
            if (isset($valorItem['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_VALOR_ITEM, $valorItem['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorItem['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_VALOR_ITEM, $valorItem['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_VALOR_ITEM, $valorItem, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the dados_fiscal_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDadosFiscalId(1234); // WHERE dados_fiscal_id = 1234
     * $query->filterByDadosFiscalId(array(12, 34)); // WHERE dados_fiscal_id IN (12, 34)
     * $query->filterByDadosFiscalId(array('min' => 12)); // WHERE dados_fiscal_id > 12
     * </code>
     *
     * @see       filterByDadosFiscal()
     *
     * @param     mixed $dadosFiscalId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByDadosFiscalId($dadosFiscalId = null, $comparison = null)
    {
        if (is_array($dadosFiscalId)) {
            $useMinMax = false;
            if (isset($dadosFiscalId['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID, $dadosFiscalId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dadosFiscalId['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID, $dadosFiscalId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID, $dadosFiscalId, $comparison);
    }

    /**
     * Filter the query on the posicao_item column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoItem(1234); // WHERE posicao_item = 1234
     * $query->filterByPosicaoItem(array(12, 34)); // WHERE posicao_item IN (12, 34)
     * $query->filterByPosicaoItem(array('min' => 12)); // WHERE posicao_item > 12
     * </code>
     *
     * @param     mixed $posicaoItem The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByPosicaoItem($posicaoItem = null, $comparison = null)
    {
        if (is_array($posicaoItem)) {
            $useMinMax = false;
            if (isset($posicaoItem['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_POSICAO_ITEM, $posicaoItem['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoItem['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_POSICAO_ITEM, $posicaoItem['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_POSICAO_ITEM, $posicaoItem, $comparison);
    }

    /**
     * Filter the query on the valor_scm column
     *
     * Example usage:
     * <code>
     * $query->filterByValorScm(1234); // WHERE valor_scm = 1234
     * $query->filterByValorScm(array(12, 34)); // WHERE valor_scm IN (12, 34)
     * $query->filterByValorScm(array('min' => 12)); // WHERE valor_scm > 12
     * </code>
     *
     * @param     mixed $valorScm The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByValorScm($valorScm = null, $comparison = null)
    {
        if (is_array($valorScm)) {
            $useMinMax = false;
            if (isset($valorScm['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_VALOR_SCM, $valorScm['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorScm['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_VALOR_SCM, $valorScm['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_VALOR_SCM, $valorScm, $comparison);
    }

    /**
     * Filter the query on the valor_sva column
     *
     * Example usage:
     * <code>
     * $query->filterByValorSva(1234); // WHERE valor_sva = 1234
     * $query->filterByValorSva(array(12, 34)); // WHERE valor_sva IN (12, 34)
     * $query->filterByValorSva(array('min' => 12)); // WHERE valor_sva > 12
     * </code>
     *
     * @param     mixed $valorSva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByValorSva($valorSva = null, $comparison = null)
    {
        if (is_array($valorSva)) {
            $useMinMax = false;
            if (isset($valorSva['min'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_VALOR_SVA, $valorSva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorSva['max'])) {
                $this->addUsingAlias(DadosFiscalItemTableMap::COL_VALOR_SVA, $valorSva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_VALOR_SVA, $valorSva, $comparison);
    }

    /**
     * Filter the query on the tem_scm column
     *
     * Example usage:
     * <code>
     * $query->filterByTemScm(true); // WHERE tem_scm = true
     * $query->filterByTemScm('yes'); // WHERE tem_scm = true
     * </code>
     *
     * @param     boolean|string $temScm The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByTemScm($temScm = null, $comparison = null)
    {
        if (is_string($temScm)) {
            $temScm = in_array(strtolower($temScm), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_TEM_SCM, $temScm, $comparison);
    }

    /**
     * Filter the query on the tem_sva column
     *
     * Example usage:
     * <code>
     * $query->filterByTemSva(true); // WHERE tem_sva = true
     * $query->filterByTemSva('yes'); // WHERE tem_sva = true
     * </code>
     *
     * @param     boolean|string $temSva The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByTemSva($temSva = null, $comparison = null)
    {
        if (is_string($temSva)) {
            $temSva = in_array(strtolower($temSva), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DadosFiscalItemTableMap::COL_TEM_SVA, $temSva, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ID, $boleto->getIdboleto(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ID, $boleto->toKeyValue('PrimaryKey', 'Idboleto'), $comparison);
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoItem object
     *
     * @param \ImaTelecomBundle\Model\BoletoItem|ObjectCollection $boletoItem The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByBoletoItem($boletoItem, $comparison = null)
    {
        if ($boletoItem instanceof \ImaTelecomBundle\Model\BoletoItem) {
            return $this
                ->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID, $boletoItem->getIdboletoItem(), $comparison);
        } elseif ($boletoItem instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID, $boletoItem->toKeyValue('PrimaryKey', 'IdboletoItem'), $comparison);
        } else {
            throw new PropelException('filterByBoletoItem() only accepts arguments of type \ImaTelecomBundle\Model\BoletoItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function joinBoletoItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoItem');
        }

        return $this;
    }

    /**
     * Use the BoletoItem relation BoletoItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoItemQuery A secondary query class using the current class as primary query
     */
    public function useBoletoItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoletoItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoItem', '\ImaTelecomBundle\Model\BoletoItemQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\DadosFiscal object
     *
     * @param \ImaTelecomBundle\Model\DadosFiscal|ObjectCollection $dadosFiscal The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function filterByDadosFiscal($dadosFiscal, $comparison = null)
    {
        if ($dadosFiscal instanceof \ImaTelecomBundle\Model\DadosFiscal) {
            return $this
                ->addUsingAlias(DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID, $dadosFiscal->getId(), $comparison);
        } elseif ($dadosFiscal instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID, $dadosFiscal->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDadosFiscal() only accepts arguments of type \ImaTelecomBundle\Model\DadosFiscal or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DadosFiscal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function joinDadosFiscal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DadosFiscal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DadosFiscal');
        }

        return $this;
    }

    /**
     * Use the DadosFiscal relation DadosFiscal object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\DadosFiscalQuery A secondary query class using the current class as primary query
     */
    public function useDadosFiscalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDadosFiscal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DadosFiscal', '\ImaTelecomBundle\Model\DadosFiscalQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDadosFiscalItem $dadosFiscalItem Object to remove from the list of results
     *
     * @return $this|ChildDadosFiscalItemQuery The current query, for fluid interface
     */
    public function prune($dadosFiscalItem = null)
    {
        if ($dadosFiscalItem) {
            $this->addUsingAlias(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, $dadosFiscalItem->getIddadosFiscalItem(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the dados_fiscal_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalItemTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DadosFiscalItemTableMap::clearInstancePool();
            DadosFiscalItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalItemTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DadosFiscalItemTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DadosFiscalItemTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DadosFiscalItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // DadosFiscalItemQuery
