<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BoletoItem as ChildBoletoItem;
use ImaTelecomBundle\Model\BoletoItemQuery as ChildBoletoItemQuery;
use ImaTelecomBundle\Model\Map\BoletoItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'boleto_item' table.
 *
 *
 *
 * @method     ChildBoletoItemQuery orderByIdboletoItem($order = Criteria::ASC) Order by the idboleto_item column
 * @method     ChildBoletoItemQuery orderByValor($order = Criteria::ASC) Order by the valor column
 * @method     ChildBoletoItemQuery orderByBoletoId($order = Criteria::ASC) Order by the boleto_id column
 * @method     ChildBoletoItemQuery orderByServicoClienteId($order = Criteria::ASC) Order by the servico_cliente_id column
 * @method     ChildBoletoItemQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildBoletoItemQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildBoletoItemQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildBoletoItemQuery orderByImportId($order = Criteria::ASC) Order by the import_id column
 * @method     ChildBoletoItemQuery orderByBaseCalculoScm($order = Criteria::ASC) Order by the base_calculo_scm column
 * @method     ChildBoletoItemQuery orderByBaseCalculoSva($order = Criteria::ASC) Order by the base_calculo_sva column
 *
 * @method     ChildBoletoItemQuery groupByIdboletoItem() Group by the idboleto_item column
 * @method     ChildBoletoItemQuery groupByValor() Group by the valor column
 * @method     ChildBoletoItemQuery groupByBoletoId() Group by the boleto_id column
 * @method     ChildBoletoItemQuery groupByServicoClienteId() Group by the servico_cliente_id column
 * @method     ChildBoletoItemQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildBoletoItemQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildBoletoItemQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildBoletoItemQuery groupByImportId() Group by the import_id column
 * @method     ChildBoletoItemQuery groupByBaseCalculoScm() Group by the base_calculo_scm column
 * @method     ChildBoletoItemQuery groupByBaseCalculoSva() Group by the base_calculo_sva column
 *
 * @method     ChildBoletoItemQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBoletoItemQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBoletoItemQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBoletoItemQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBoletoItemQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBoletoItemQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBoletoItemQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildBoletoItemQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildBoletoItemQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildBoletoItemQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildBoletoItemQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildBoletoItemQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildBoletoItemQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildBoletoItemQuery leftJoinServicoCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildBoletoItemQuery rightJoinServicoCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildBoletoItemQuery innerJoinServicoCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicoCliente relation
 *
 * @method     ChildBoletoItemQuery joinWithServicoCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicoCliente relation
 *
 * @method     ChildBoletoItemQuery leftJoinWithServicoCliente() Adds a LEFT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildBoletoItemQuery rightJoinWithServicoCliente() Adds a RIGHT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildBoletoItemQuery innerJoinWithServicoCliente() Adds a INNER JOIN clause and with to the query using the ServicoCliente relation
 *
 * @method     ChildBoletoItemQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBoletoItemQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBoletoItemQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBoletoItemQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBoletoItemQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBoletoItemQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBoletoItemQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildBoletoItemQuery leftJoinDadosFiscaisGeracao($relationAlias = null) Adds a LEFT JOIN clause to the query using the DadosFiscaisGeracao relation
 * @method     ChildBoletoItemQuery rightJoinDadosFiscaisGeracao($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DadosFiscaisGeracao relation
 * @method     ChildBoletoItemQuery innerJoinDadosFiscaisGeracao($relationAlias = null) Adds a INNER JOIN clause to the query using the DadosFiscaisGeracao relation
 *
 * @method     ChildBoletoItemQuery joinWithDadosFiscaisGeracao($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DadosFiscaisGeracao relation
 *
 * @method     ChildBoletoItemQuery leftJoinWithDadosFiscaisGeracao() Adds a LEFT JOIN clause and with to the query using the DadosFiscaisGeracao relation
 * @method     ChildBoletoItemQuery rightJoinWithDadosFiscaisGeracao() Adds a RIGHT JOIN clause and with to the query using the DadosFiscaisGeracao relation
 * @method     ChildBoletoItemQuery innerJoinWithDadosFiscaisGeracao() Adds a INNER JOIN clause and with to the query using the DadosFiscaisGeracao relation
 *
 * @method     ChildBoletoItemQuery leftJoinDadosFiscalItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the DadosFiscalItem relation
 * @method     ChildBoletoItemQuery rightJoinDadosFiscalItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DadosFiscalItem relation
 * @method     ChildBoletoItemQuery innerJoinDadosFiscalItem($relationAlias = null) Adds a INNER JOIN clause to the query using the DadosFiscalItem relation
 *
 * @method     ChildBoletoItemQuery joinWithDadosFiscalItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DadosFiscalItem relation
 *
 * @method     ChildBoletoItemQuery leftJoinWithDadosFiscalItem() Adds a LEFT JOIN clause and with to the query using the DadosFiscalItem relation
 * @method     ChildBoletoItemQuery rightJoinWithDadosFiscalItem() Adds a RIGHT JOIN clause and with to the query using the DadosFiscalItem relation
 * @method     ChildBoletoItemQuery innerJoinWithDadosFiscalItem() Adds a INNER JOIN clause and with to the query using the DadosFiscalItem relation
 *
 * @method     \ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\ServicoClienteQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\DadosFiscaisGeracaoQuery|\ImaTelecomBundle\Model\DadosFiscalItemQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBoletoItem findOne(ConnectionInterface $con = null) Return the first ChildBoletoItem matching the query
 * @method     ChildBoletoItem findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBoletoItem matching the query, or a new ChildBoletoItem object populated from the query conditions when no match is found
 *
 * @method     ChildBoletoItem findOneByIdboletoItem(int $idboleto_item) Return the first ChildBoletoItem filtered by the idboleto_item column
 * @method     ChildBoletoItem findOneByValor(string $valor) Return the first ChildBoletoItem filtered by the valor column
 * @method     ChildBoletoItem findOneByBoletoId(int $boleto_id) Return the first ChildBoletoItem filtered by the boleto_id column
 * @method     ChildBoletoItem findOneByServicoClienteId(int $servico_cliente_id) Return the first ChildBoletoItem filtered by the servico_cliente_id column
 * @method     ChildBoletoItem findOneByDataCadastro(string $data_cadastro) Return the first ChildBoletoItem filtered by the data_cadastro column
 * @method     ChildBoletoItem findOneByDataAlterado(string $data_alterado) Return the first ChildBoletoItem filtered by the data_alterado column
 * @method     ChildBoletoItem findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBoletoItem filtered by the usuario_alterado column
 * @method     ChildBoletoItem findOneByImportId(int $import_id) Return the first ChildBoletoItem filtered by the import_id column
 * @method     ChildBoletoItem findOneByBaseCalculoScm(string $base_calculo_scm) Return the first ChildBoletoItem filtered by the base_calculo_scm column
 * @method     ChildBoletoItem findOneByBaseCalculoSva(string $base_calculo_sva) Return the first ChildBoletoItem filtered by the base_calculo_sva column *

 * @method     ChildBoletoItem requirePk($key, ConnectionInterface $con = null) Return the ChildBoletoItem by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOne(ConnectionInterface $con = null) Return the first ChildBoletoItem matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBoletoItem requireOneByIdboletoItem(int $idboleto_item) Return the first ChildBoletoItem filtered by the idboleto_item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOneByValor(string $valor) Return the first ChildBoletoItem filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOneByBoletoId(int $boleto_id) Return the first ChildBoletoItem filtered by the boleto_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOneByServicoClienteId(int $servico_cliente_id) Return the first ChildBoletoItem filtered by the servico_cliente_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOneByDataCadastro(string $data_cadastro) Return the first ChildBoletoItem filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOneByDataAlterado(string $data_alterado) Return the first ChildBoletoItem filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBoletoItem filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOneByImportId(int $import_id) Return the first ChildBoletoItem filtered by the import_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOneByBaseCalculoScm(string $base_calculo_scm) Return the first ChildBoletoItem filtered by the base_calculo_scm column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBoletoItem requireOneByBaseCalculoSva(string $base_calculo_sva) Return the first ChildBoletoItem filtered by the base_calculo_sva column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBoletoItem[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBoletoItem objects based on current ModelCriteria
 * @method     ChildBoletoItem[]|ObjectCollection findByIdboletoItem(int $idboleto_item) Return ChildBoletoItem objects filtered by the idboleto_item column
 * @method     ChildBoletoItem[]|ObjectCollection findByValor(string $valor) Return ChildBoletoItem objects filtered by the valor column
 * @method     ChildBoletoItem[]|ObjectCollection findByBoletoId(int $boleto_id) Return ChildBoletoItem objects filtered by the boleto_id column
 * @method     ChildBoletoItem[]|ObjectCollection findByServicoClienteId(int $servico_cliente_id) Return ChildBoletoItem objects filtered by the servico_cliente_id column
 * @method     ChildBoletoItem[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildBoletoItem objects filtered by the data_cadastro column
 * @method     ChildBoletoItem[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildBoletoItem objects filtered by the data_alterado column
 * @method     ChildBoletoItem[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildBoletoItem objects filtered by the usuario_alterado column
 * @method     ChildBoletoItem[]|ObjectCollection findByImportId(int $import_id) Return ChildBoletoItem objects filtered by the import_id column
 * @method     ChildBoletoItem[]|ObjectCollection findByBaseCalculoScm(string $base_calculo_scm) Return ChildBoletoItem objects filtered by the base_calculo_scm column
 * @method     ChildBoletoItem[]|ObjectCollection findByBaseCalculoSva(string $base_calculo_sva) Return ChildBoletoItem objects filtered by the base_calculo_sva column
 * @method     ChildBoletoItem[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BoletoItemQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\BoletoItemQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\BoletoItem', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBoletoItemQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBoletoItemQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBoletoItemQuery) {
            return $criteria;
        }
        $query = new ChildBoletoItemQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBoletoItem|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BoletoItemTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BoletoItemTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoItem A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idboleto_item, valor, boleto_id, servico_cliente_id, data_cadastro, data_alterado, usuario_alterado, import_id, base_calculo_scm, base_calculo_sva FROM boleto_item WHERE idboleto_item = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBoletoItem $obj */
            $obj = new ChildBoletoItem();
            $obj->hydrate($row);
            BoletoItemTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBoletoItem|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BoletoItemTableMap::COL_IDBOLETO_ITEM, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BoletoItemTableMap::COL_IDBOLETO_ITEM, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idboleto_item column
     *
     * Example usage:
     * <code>
     * $query->filterByIdboletoItem(1234); // WHERE idboleto_item = 1234
     * $query->filterByIdboletoItem(array(12, 34)); // WHERE idboleto_item IN (12, 34)
     * $query->filterByIdboletoItem(array('min' => 12)); // WHERE idboleto_item > 12
     * </code>
     *
     * @param     mixed $idboletoItem The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByIdboletoItem($idboletoItem = null, $comparison = null)
    {
        if (is_array($idboletoItem)) {
            $useMinMax = false;
            if (isset($idboletoItem['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_IDBOLETO_ITEM, $idboletoItem['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idboletoItem['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_IDBOLETO_ITEM, $idboletoItem['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_IDBOLETO_ITEM, $idboletoItem, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query on the boleto_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBoletoId(1234); // WHERE boleto_id = 1234
     * $query->filterByBoletoId(array(12, 34)); // WHERE boleto_id IN (12, 34)
     * $query->filterByBoletoId(array('min' => 12)); // WHERE boleto_id > 12
     * </code>
     *
     * @see       filterByBoleto()
     *
     * @param     mixed $boletoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByBoletoId($boletoId = null, $comparison = null)
    {
        if (is_array($boletoId)) {
            $useMinMax = false;
            if (isset($boletoId['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_BOLETO_ID, $boletoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($boletoId['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_BOLETO_ID, $boletoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_BOLETO_ID, $boletoId, $comparison);
    }

    /**
     * Filter the query on the servico_cliente_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServicoClienteId(1234); // WHERE servico_cliente_id = 1234
     * $query->filterByServicoClienteId(array(12, 34)); // WHERE servico_cliente_id IN (12, 34)
     * $query->filterByServicoClienteId(array('min' => 12)); // WHERE servico_cliente_id > 12
     * </code>
     *
     * @see       filterByServicoCliente()
     *
     * @param     mixed $servicoClienteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByServicoClienteId($servicoClienteId = null, $comparison = null)
    {
        if (is_array($servicoClienteId)) {
            $useMinMax = false;
            if (isset($servicoClienteId['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_SERVICO_CLIENTE_ID, $servicoClienteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($servicoClienteId['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_SERVICO_CLIENTE_ID, $servicoClienteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_SERVICO_CLIENTE_ID, $servicoClienteId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the import_id column
     *
     * Example usage:
     * <code>
     * $query->filterByImportId(1234); // WHERE import_id = 1234
     * $query->filterByImportId(array(12, 34)); // WHERE import_id IN (12, 34)
     * $query->filterByImportId(array('min' => 12)); // WHERE import_id > 12
     * </code>
     *
     * @param     mixed $importId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByImportId($importId = null, $comparison = null)
    {
        if (is_array($importId)) {
            $useMinMax = false;
            if (isset($importId['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_IMPORT_ID, $importId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importId['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_IMPORT_ID, $importId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_IMPORT_ID, $importId, $comparison);
    }

    /**
     * Filter the query on the base_calculo_scm column
     *
     * Example usage:
     * <code>
     * $query->filterByBaseCalculoScm(1234); // WHERE base_calculo_scm = 1234
     * $query->filterByBaseCalculoScm(array(12, 34)); // WHERE base_calculo_scm IN (12, 34)
     * $query->filterByBaseCalculoScm(array('min' => 12)); // WHERE base_calculo_scm > 12
     * </code>
     *
     * @param     mixed $baseCalculoScm The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByBaseCalculoScm($baseCalculoScm = null, $comparison = null)
    {
        if (is_array($baseCalculoScm)) {
            $useMinMax = false;
            if (isset($baseCalculoScm['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_BASE_CALCULO_SCM, $baseCalculoScm['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baseCalculoScm['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_BASE_CALCULO_SCM, $baseCalculoScm['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_BASE_CALCULO_SCM, $baseCalculoScm, $comparison);
    }

    /**
     * Filter the query on the base_calculo_sva column
     *
     * Example usage:
     * <code>
     * $query->filterByBaseCalculoSva(1234); // WHERE base_calculo_sva = 1234
     * $query->filterByBaseCalculoSva(array(12, 34)); // WHERE base_calculo_sva IN (12, 34)
     * $query->filterByBaseCalculoSva(array('min' => 12)); // WHERE base_calculo_sva > 12
     * </code>
     *
     * @param     mixed $baseCalculoSva The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByBaseCalculoSva($baseCalculoSva = null, $comparison = null)
    {
        if (is_array($baseCalculoSva)) {
            $useMinMax = false;
            if (isset($baseCalculoSva['min'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_BASE_CALCULO_SVA, $baseCalculoSva['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baseCalculoSva['max'])) {
                $this->addUsingAlias(BoletoItemTableMap::COL_BASE_CALCULO_SVA, $baseCalculoSva['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BoletoItemTableMap::COL_BASE_CALCULO_SVA, $baseCalculoSva, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(BoletoItemTableMap::COL_BOLETO_ID, $boleto->getIdboleto(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoItemTableMap::COL_BOLETO_ID, $boleto->toKeyValue('PrimaryKey', 'Idboleto'), $comparison);
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ServicoCliente object
     *
     * @param \ImaTelecomBundle\Model\ServicoCliente|ObjectCollection $servicoCliente The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByServicoCliente($servicoCliente, $comparison = null)
    {
        if ($servicoCliente instanceof \ImaTelecomBundle\Model\ServicoCliente) {
            return $this
                ->addUsingAlias(BoletoItemTableMap::COL_SERVICO_CLIENTE_ID, $servicoCliente->getIdservicoCliente(), $comparison);
        } elseif ($servicoCliente instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoItemTableMap::COL_SERVICO_CLIENTE_ID, $servicoCliente->toKeyValue('PrimaryKey', 'IdservicoCliente'), $comparison);
        } else {
            throw new PropelException('filterByServicoCliente() only accepts arguments of type \ImaTelecomBundle\Model\ServicoCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicoCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function joinServicoCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicoCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicoCliente');
        }

        return $this;
    }

    /**
     * Use the ServicoCliente relation ServicoCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ServicoClienteQuery A secondary query class using the current class as primary query
     */
    public function useServicoClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicoCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicoCliente', '\ImaTelecomBundle\Model\ServicoClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(BoletoItemTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BoletoItemTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\DadosFiscaisGeracao object
     *
     * @param \ImaTelecomBundle\Model\DadosFiscaisGeracao|ObjectCollection $dadosFiscaisGeracao the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByDadosFiscaisGeracao($dadosFiscaisGeracao, $comparison = null)
    {
        if ($dadosFiscaisGeracao instanceof \ImaTelecomBundle\Model\DadosFiscaisGeracao) {
            return $this
                ->addUsingAlias(BoletoItemTableMap::COL_IDBOLETO_ITEM, $dadosFiscaisGeracao->getBoletoItemId(), $comparison);
        } elseif ($dadosFiscaisGeracao instanceof ObjectCollection) {
            return $this
                ->useDadosFiscaisGeracaoQuery()
                ->filterByPrimaryKeys($dadosFiscaisGeracao->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDadosFiscaisGeracao() only accepts arguments of type \ImaTelecomBundle\Model\DadosFiscaisGeracao or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DadosFiscaisGeracao relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function joinDadosFiscaisGeracao($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DadosFiscaisGeracao');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DadosFiscaisGeracao');
        }

        return $this;
    }

    /**
     * Use the DadosFiscaisGeracao relation DadosFiscaisGeracao object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\DadosFiscaisGeracaoQuery A secondary query class using the current class as primary query
     */
    public function useDadosFiscaisGeracaoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDadosFiscaisGeracao($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DadosFiscaisGeracao', '\ImaTelecomBundle\Model\DadosFiscaisGeracaoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\DadosFiscalItem object
     *
     * @param \ImaTelecomBundle\Model\DadosFiscalItem|ObjectCollection $dadosFiscalItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBoletoItemQuery The current query, for fluid interface
     */
    public function filterByDadosFiscalItem($dadosFiscalItem, $comparison = null)
    {
        if ($dadosFiscalItem instanceof \ImaTelecomBundle\Model\DadosFiscalItem) {
            return $this
                ->addUsingAlias(BoletoItemTableMap::COL_IDBOLETO_ITEM, $dadosFiscalItem->getBoletoItemId(), $comparison);
        } elseif ($dadosFiscalItem instanceof ObjectCollection) {
            return $this
                ->useDadosFiscalItemQuery()
                ->filterByPrimaryKeys($dadosFiscalItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDadosFiscalItem() only accepts arguments of type \ImaTelecomBundle\Model\DadosFiscalItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DadosFiscalItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function joinDadosFiscalItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DadosFiscalItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DadosFiscalItem');
        }

        return $this;
    }

    /**
     * Use the DadosFiscalItem relation DadosFiscalItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\DadosFiscalItemQuery A secondary query class using the current class as primary query
     */
    public function useDadosFiscalItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDadosFiscalItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DadosFiscalItem', '\ImaTelecomBundle\Model\DadosFiscalItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBoletoItem $boletoItem Object to remove from the list of results
     *
     * @return $this|ChildBoletoItemQuery The current query, for fluid interface
     */
    public function prune($boletoItem = null)
    {
        if ($boletoItem) {
            $this->addUsingAlias(BoletoItemTableMap::COL_IDBOLETO_ITEM, $boletoItem->getIdboletoItem(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the boleto_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoItemTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BoletoItemTableMap::clearInstancePool();
            BoletoItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoItemTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BoletoItemTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BoletoItemTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BoletoItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BoletoItemQuery
