<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Lancamentos as ChildLancamentos;
use ImaTelecomBundle\Model\LancamentosQuery as ChildLancamentosQuery;
use ImaTelecomBundle\Model\Map\LancamentosTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'lancamentos' table.
 *
 *
 *
 * @method     ChildLancamentosQuery orderByIdlancamento($order = Criteria::ASC) Order by the idlancamento column
 * @method     ChildLancamentosQuery orderByConteudoArquivoSaida($order = Criteria::ASC) Order by the conteudo_arquivo_saida column
 * @method     ChildLancamentosQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildLancamentosQuery orderByDataGeracao($order = Criteria::ASC) Order by the data_geracao column
 * @method     ChildLancamentosQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildLancamentosQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildLancamentosQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildLancamentosQuery orderByConvenioId($order = Criteria::ASC) Order by the convenio_id column
 * @method     ChildLancamentosQuery orderBySequencialArquivoCobranca($order = Criteria::ASC) Order by the sequencial_arquivo_cobranca column
 *
 * @method     ChildLancamentosQuery groupByIdlancamento() Group by the idlancamento column
 * @method     ChildLancamentosQuery groupByConteudoArquivoSaida() Group by the conteudo_arquivo_saida column
 * @method     ChildLancamentosQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildLancamentosQuery groupByDataGeracao() Group by the data_geracao column
 * @method     ChildLancamentosQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildLancamentosQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildLancamentosQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildLancamentosQuery groupByConvenioId() Group by the convenio_id column
 * @method     ChildLancamentosQuery groupBySequencialArquivoCobranca() Group by the sequencial_arquivo_cobranca column
 *
 * @method     ChildLancamentosQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLancamentosQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLancamentosQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLancamentosQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLancamentosQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLancamentosQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLancamentosQuery leftJoinCompetencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Competencia relation
 * @method     ChildLancamentosQuery rightJoinCompetencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Competencia relation
 * @method     ChildLancamentosQuery innerJoinCompetencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Competencia relation
 *
 * @method     ChildLancamentosQuery joinWithCompetencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Competencia relation
 *
 * @method     ChildLancamentosQuery leftJoinWithCompetencia() Adds a LEFT JOIN clause and with to the query using the Competencia relation
 * @method     ChildLancamentosQuery rightJoinWithCompetencia() Adds a RIGHT JOIN clause and with to the query using the Competencia relation
 * @method     ChildLancamentosQuery innerJoinWithCompetencia() Adds a INNER JOIN clause and with to the query using the Competencia relation
 *
 * @method     ChildLancamentosQuery leftJoinConvenio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Convenio relation
 * @method     ChildLancamentosQuery rightJoinConvenio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Convenio relation
 * @method     ChildLancamentosQuery innerJoinConvenio($relationAlias = null) Adds a INNER JOIN clause to the query using the Convenio relation
 *
 * @method     ChildLancamentosQuery joinWithConvenio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Convenio relation
 *
 * @method     ChildLancamentosQuery leftJoinWithConvenio() Adds a LEFT JOIN clause and with to the query using the Convenio relation
 * @method     ChildLancamentosQuery rightJoinWithConvenio() Adds a RIGHT JOIN clause and with to the query using the Convenio relation
 * @method     ChildLancamentosQuery innerJoinWithConvenio() Adds a INNER JOIN clause and with to the query using the Convenio relation
 *
 * @method     ChildLancamentosQuery leftJoinBoletoBaixaHistorico($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildLancamentosQuery rightJoinBoletoBaixaHistorico($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildLancamentosQuery innerJoinBoletoBaixaHistorico($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildLancamentosQuery joinWithBoletoBaixaHistorico($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildLancamentosQuery leftJoinWithBoletoBaixaHistorico() Adds a LEFT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildLancamentosQuery rightJoinWithBoletoBaixaHistorico() Adds a RIGHT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildLancamentosQuery innerJoinWithBoletoBaixaHistorico() Adds a INNER JOIN clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildLancamentosQuery leftJoinLancamentosBoletos($relationAlias = null) Adds a LEFT JOIN clause to the query using the LancamentosBoletos relation
 * @method     ChildLancamentosQuery rightJoinLancamentosBoletos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LancamentosBoletos relation
 * @method     ChildLancamentosQuery innerJoinLancamentosBoletos($relationAlias = null) Adds a INNER JOIN clause to the query using the LancamentosBoletos relation
 *
 * @method     ChildLancamentosQuery joinWithLancamentosBoletos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LancamentosBoletos relation
 *
 * @method     ChildLancamentosQuery leftJoinWithLancamentosBoletos() Adds a LEFT JOIN clause and with to the query using the LancamentosBoletos relation
 * @method     ChildLancamentosQuery rightJoinWithLancamentosBoletos() Adds a RIGHT JOIN clause and with to the query using the LancamentosBoletos relation
 * @method     ChildLancamentosQuery innerJoinWithLancamentosBoletos() Adds a INNER JOIN clause and with to the query using the LancamentosBoletos relation
 *
 * @method     \ImaTelecomBundle\Model\CompetenciaQuery|\ImaTelecomBundle\Model\ConvenioQuery|\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery|\ImaTelecomBundle\Model\LancamentosBoletosQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLancamentos findOne(ConnectionInterface $con = null) Return the first ChildLancamentos matching the query
 * @method     ChildLancamentos findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLancamentos matching the query, or a new ChildLancamentos object populated from the query conditions when no match is found
 *
 * @method     ChildLancamentos findOneByIdlancamento(int $idlancamento) Return the first ChildLancamentos filtered by the idlancamento column
 * @method     ChildLancamentos findOneByConteudoArquivoSaida(string $conteudo_arquivo_saida) Return the first ChildLancamentos filtered by the conteudo_arquivo_saida column
 * @method     ChildLancamentos findOneByCompetenciaId(int $competencia_id) Return the first ChildLancamentos filtered by the competencia_id column
 * @method     ChildLancamentos findOneByDataGeracao(string $data_geracao) Return the first ChildLancamentos filtered by the data_geracao column
 * @method     ChildLancamentos findOneByDataCadastro(string $data_cadastro) Return the first ChildLancamentos filtered by the data_cadastro column
 * @method     ChildLancamentos findOneByDataAlterado(string $data_alterado) Return the first ChildLancamentos filtered by the data_alterado column
 * @method     ChildLancamentos findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildLancamentos filtered by the usuario_alterado column
 * @method     ChildLancamentos findOneByConvenioId(int $convenio_id) Return the first ChildLancamentos filtered by the convenio_id column
 * @method     ChildLancamentos findOneBySequencialArquivoCobranca(int $sequencial_arquivo_cobranca) Return the first ChildLancamentos filtered by the sequencial_arquivo_cobranca column *

 * @method     ChildLancamentos requirePk($key, ConnectionInterface $con = null) Return the ChildLancamentos by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentos requireOne(ConnectionInterface $con = null) Return the first ChildLancamentos matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLancamentos requireOneByIdlancamento(int $idlancamento) Return the first ChildLancamentos filtered by the idlancamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentos requireOneByConteudoArquivoSaida(string $conteudo_arquivo_saida) Return the first ChildLancamentos filtered by the conteudo_arquivo_saida column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentos requireOneByCompetenciaId(int $competencia_id) Return the first ChildLancamentos filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentos requireOneByDataGeracao(string $data_geracao) Return the first ChildLancamentos filtered by the data_geracao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentos requireOneByDataCadastro(string $data_cadastro) Return the first ChildLancamentos filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentos requireOneByDataAlterado(string $data_alterado) Return the first ChildLancamentos filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentos requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildLancamentos filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentos requireOneByConvenioId(int $convenio_id) Return the first ChildLancamentos filtered by the convenio_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLancamentos requireOneBySequencialArquivoCobranca(int $sequencial_arquivo_cobranca) Return the first ChildLancamentos filtered by the sequencial_arquivo_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLancamentos[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLancamentos objects based on current ModelCriteria
 * @method     ChildLancamentos[]|ObjectCollection findByIdlancamento(int $idlancamento) Return ChildLancamentos objects filtered by the idlancamento column
 * @method     ChildLancamentos[]|ObjectCollection findByConteudoArquivoSaida(string $conteudo_arquivo_saida) Return ChildLancamentos objects filtered by the conteudo_arquivo_saida column
 * @method     ChildLancamentos[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildLancamentos objects filtered by the competencia_id column
 * @method     ChildLancamentos[]|ObjectCollection findByDataGeracao(string $data_geracao) Return ChildLancamentos objects filtered by the data_geracao column
 * @method     ChildLancamentos[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildLancamentos objects filtered by the data_cadastro column
 * @method     ChildLancamentos[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildLancamentos objects filtered by the data_alterado column
 * @method     ChildLancamentos[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildLancamentos objects filtered by the usuario_alterado column
 * @method     ChildLancamentos[]|ObjectCollection findByConvenioId(int $convenio_id) Return ChildLancamentos objects filtered by the convenio_id column
 * @method     ChildLancamentos[]|ObjectCollection findBySequencialArquivoCobranca(int $sequencial_arquivo_cobranca) Return ChildLancamentos objects filtered by the sequencial_arquivo_cobranca column
 * @method     ChildLancamentos[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LancamentosQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\LancamentosQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Lancamentos', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLancamentosQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLancamentosQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLancamentosQuery) {
            return $criteria;
        }
        $query = new ChildLancamentosQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLancamentos|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LancamentosTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LancamentosTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLancamentos A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idlancamento, conteudo_arquivo_saida, competencia_id, data_geracao, data_cadastro, data_alterado, usuario_alterado, convenio_id, sequencial_arquivo_cobranca FROM lancamentos WHERE idlancamento = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLancamentos $obj */
            $obj = new ChildLancamentos();
            $obj->hydrate($row);
            LancamentosTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLancamentos|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LancamentosTableMap::COL_IDLANCAMENTO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LancamentosTableMap::COL_IDLANCAMENTO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idlancamento column
     *
     * Example usage:
     * <code>
     * $query->filterByIdlancamento(1234); // WHERE idlancamento = 1234
     * $query->filterByIdlancamento(array(12, 34)); // WHERE idlancamento IN (12, 34)
     * $query->filterByIdlancamento(array('min' => 12)); // WHERE idlancamento > 12
     * </code>
     *
     * @param     mixed $idlancamento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByIdlancamento($idlancamento = null, $comparison = null)
    {
        if (is_array($idlancamento)) {
            $useMinMax = false;
            if (isset($idlancamento['min'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_IDLANCAMENTO, $idlancamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idlancamento['max'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_IDLANCAMENTO, $idlancamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosTableMap::COL_IDLANCAMENTO, $idlancamento, $comparison);
    }

    /**
     * Filter the query on the conteudo_arquivo_saida column
     *
     * Example usage:
     * <code>
     * $query->filterByConteudoArquivoSaida('fooValue');   // WHERE conteudo_arquivo_saida = 'fooValue'
     * $query->filterByConteudoArquivoSaida('%fooValue%', Criteria::LIKE); // WHERE conteudo_arquivo_saida LIKE '%fooValue%'
     * </code>
     *
     * @param     string $conteudoArquivoSaida The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByConteudoArquivoSaida($conteudoArquivoSaida = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($conteudoArquivoSaida)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosTableMap::COL_CONTEUDO_ARQUIVO_SAIDA, $conteudoArquivoSaida, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @see       filterByCompetencia()
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the data_geracao column
     *
     * Example usage:
     * <code>
     * $query->filterByDataGeracao('fooValue');   // WHERE data_geracao = 'fooValue'
     * $query->filterByDataGeracao('%fooValue%', Criteria::LIKE); // WHERE data_geracao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dataGeracao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByDataGeracao($dataGeracao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dataGeracao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosTableMap::COL_DATA_GERACAO, $dataGeracao, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the convenio_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConvenioId(1234); // WHERE convenio_id = 1234
     * $query->filterByConvenioId(array(12, 34)); // WHERE convenio_id IN (12, 34)
     * $query->filterByConvenioId(array('min' => 12)); // WHERE convenio_id > 12
     * </code>
     *
     * @see       filterByConvenio()
     *
     * @param     mixed $convenioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByConvenioId($convenioId = null, $comparison = null)
    {
        if (is_array($convenioId)) {
            $useMinMax = false;
            if (isset($convenioId['min'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_CONVENIO_ID, $convenioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($convenioId['max'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_CONVENIO_ID, $convenioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosTableMap::COL_CONVENIO_ID, $convenioId, $comparison);
    }

    /**
     * Filter the query on the sequencial_arquivo_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterBySequencialArquivoCobranca(1234); // WHERE sequencial_arquivo_cobranca = 1234
     * $query->filterBySequencialArquivoCobranca(array(12, 34)); // WHERE sequencial_arquivo_cobranca IN (12, 34)
     * $query->filterBySequencialArquivoCobranca(array('min' => 12)); // WHERE sequencial_arquivo_cobranca > 12
     * </code>
     *
     * @param     mixed $sequencialArquivoCobranca The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterBySequencialArquivoCobranca($sequencialArquivoCobranca = null, $comparison = null)
    {
        if (is_array($sequencialArquivoCobranca)) {
            $useMinMax = false;
            if (isset($sequencialArquivoCobranca['min'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA, $sequencialArquivoCobranca['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sequencialArquivoCobranca['max'])) {
                $this->addUsingAlias(LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA, $sequencialArquivoCobranca['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA, $sequencialArquivoCobranca, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Competencia object
     *
     * @param \ImaTelecomBundle\Model\Competencia|ObjectCollection $competencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByCompetencia($competencia, $comparison = null)
    {
        if ($competencia instanceof \ImaTelecomBundle\Model\Competencia) {
            return $this
                ->addUsingAlias(LancamentosTableMap::COL_COMPETENCIA_ID, $competencia->getId(), $comparison);
        } elseif ($competencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LancamentosTableMap::COL_COMPETENCIA_ID, $competencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompetencia() only accepts arguments of type \ImaTelecomBundle\Model\Competencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Competencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function joinCompetencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Competencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Competencia');
        }

        return $this;
    }

    /**
     * Use the Competencia relation Competencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CompetenciaQuery A secondary query class using the current class as primary query
     */
    public function useCompetenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompetencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Competencia', '\ImaTelecomBundle\Model\CompetenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Convenio object
     *
     * @param \ImaTelecomBundle\Model\Convenio|ObjectCollection $convenio The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByConvenio($convenio, $comparison = null)
    {
        if ($convenio instanceof \ImaTelecomBundle\Model\Convenio) {
            return $this
                ->addUsingAlias(LancamentosTableMap::COL_CONVENIO_ID, $convenio->getIdconvenio(), $comparison);
        } elseif ($convenio instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LancamentosTableMap::COL_CONVENIO_ID, $convenio->toKeyValue('PrimaryKey', 'Idconvenio'), $comparison);
        } else {
            throw new PropelException('filterByConvenio() only accepts arguments of type \ImaTelecomBundle\Model\Convenio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Convenio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function joinConvenio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Convenio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Convenio');
        }

        return $this;
    }

    /**
     * Use the Convenio relation Convenio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ConvenioQuery A secondary query class using the current class as primary query
     */
    public function useConvenioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinConvenio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Convenio', '\ImaTelecomBundle\Model\ConvenioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoBaixaHistorico object
     *
     * @param \ImaTelecomBundle\Model\BoletoBaixaHistorico|ObjectCollection $boletoBaixaHistorico the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByBoletoBaixaHistorico($boletoBaixaHistorico, $comparison = null)
    {
        if ($boletoBaixaHistorico instanceof \ImaTelecomBundle\Model\BoletoBaixaHistorico) {
            return $this
                ->addUsingAlias(LancamentosTableMap::COL_IDLANCAMENTO, $boletoBaixaHistorico->getLancamentoId(), $comparison);
        } elseif ($boletoBaixaHistorico instanceof ObjectCollection) {
            return $this
                ->useBoletoBaixaHistoricoQuery()
                ->filterByPrimaryKeys($boletoBaixaHistorico->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoBaixaHistorico() only accepts arguments of type \ImaTelecomBundle\Model\BoletoBaixaHistorico or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoBaixaHistorico relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function joinBoletoBaixaHistorico($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoBaixaHistorico');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoBaixaHistorico');
        }

        return $this;
    }

    /**
     * Use the BoletoBaixaHistorico relation BoletoBaixaHistorico object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoBaixaHistoricoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBoletoBaixaHistorico($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoBaixaHistorico', '\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\LancamentosBoletos object
     *
     * @param \ImaTelecomBundle\Model\LancamentosBoletos|ObjectCollection $lancamentosBoletos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLancamentosQuery The current query, for fluid interface
     */
    public function filterByLancamentosBoletos($lancamentosBoletos, $comparison = null)
    {
        if ($lancamentosBoletos instanceof \ImaTelecomBundle\Model\LancamentosBoletos) {
            return $this
                ->addUsingAlias(LancamentosTableMap::COL_IDLANCAMENTO, $lancamentosBoletos->getLancamentoId(), $comparison);
        } elseif ($lancamentosBoletos instanceof ObjectCollection) {
            return $this
                ->useLancamentosBoletosQuery()
                ->filterByPrimaryKeys($lancamentosBoletos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLancamentosBoletos() only accepts arguments of type \ImaTelecomBundle\Model\LancamentosBoletos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LancamentosBoletos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function joinLancamentosBoletos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LancamentosBoletos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LancamentosBoletos');
        }

        return $this;
    }

    /**
     * Use the LancamentosBoletos relation LancamentosBoletos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\LancamentosBoletosQuery A secondary query class using the current class as primary query
     */
    public function useLancamentosBoletosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLancamentosBoletos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LancamentosBoletos', '\ImaTelecomBundle\Model\LancamentosBoletosQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLancamentos $lancamentos Object to remove from the list of results
     *
     * @return $this|ChildLancamentosQuery The current query, for fluid interface
     */
    public function prune($lancamentos = null)
    {
        if ($lancamentos) {
            $this->addUsingAlias(LancamentosTableMap::COL_IDLANCAMENTO, $lancamentos->getIdlancamento(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the lancamentos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LancamentosTableMap::clearInstancePool();
            LancamentosTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LancamentosTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LancamentosTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LancamentosTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LancamentosQuery
