<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ContaCaixa as ChildContaCaixa;
use ImaTelecomBundle\Model\ContaCaixaQuery as ChildContaCaixaQuery;
use ImaTelecomBundle\Model\Map\ContaCaixaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'conta_caixa' table.
 *
 *
 *
 * @method     ChildContaCaixaQuery orderByIdcontaCaixa($order = Criteria::ASC) Order by the idconta_caixa column
 * @method     ChildContaCaixaQuery orderByDescricao($order = Criteria::ASC) Order by the descricao column
 * @method     ChildContaCaixaQuery orderByBancoAgenciaContaId($order = Criteria::ASC) Order by the banco_agencia_conta_id column
 * @method     ChildContaCaixaQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildContaCaixaQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildContaCaixaQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildContaCaixaQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildContaCaixaQuery groupByIdcontaCaixa() Group by the idconta_caixa column
 * @method     ChildContaCaixaQuery groupByDescricao() Group by the descricao column
 * @method     ChildContaCaixaQuery groupByBancoAgenciaContaId() Group by the banco_agencia_conta_id column
 * @method     ChildContaCaixaQuery groupByAtivo() Group by the ativo column
 * @method     ChildContaCaixaQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildContaCaixaQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildContaCaixaQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildContaCaixaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildContaCaixaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildContaCaixaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildContaCaixaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildContaCaixaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildContaCaixaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildContaCaixaQuery leftJoinBancoAgenciaConta($relationAlias = null) Adds a LEFT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildContaCaixaQuery rightJoinBancoAgenciaConta($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildContaCaixaQuery innerJoinBancoAgenciaConta($relationAlias = null) Adds a INNER JOIN clause to the query using the BancoAgenciaConta relation
 *
 * @method     ChildContaCaixaQuery joinWithBancoAgenciaConta($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildContaCaixaQuery leftJoinWithBancoAgenciaConta() Adds a LEFT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildContaCaixaQuery rightJoinWithBancoAgenciaConta() Adds a RIGHT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildContaCaixaQuery innerJoinWithBancoAgenciaConta() Adds a INNER JOIN clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildContaCaixaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildContaCaixaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildContaCaixaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildContaCaixaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildContaCaixaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildContaCaixaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildContaCaixaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildContaCaixaQuery leftJoinBaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Baixa relation
 * @method     ChildContaCaixaQuery rightJoinBaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Baixa relation
 * @method     ChildContaCaixaQuery innerJoinBaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Baixa relation
 *
 * @method     ChildContaCaixaQuery joinWithBaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Baixa relation
 *
 * @method     ChildContaCaixaQuery leftJoinWithBaixa() Adds a LEFT JOIN clause and with to the query using the Baixa relation
 * @method     ChildContaCaixaQuery rightJoinWithBaixa() Adds a RIGHT JOIN clause and with to the query using the Baixa relation
 * @method     ChildContaCaixaQuery innerJoinWithBaixa() Adds a INNER JOIN clause and with to the query using the Baixa relation
 *
 * @method     ChildContaCaixaQuery leftJoinBaixaEstorno($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildContaCaixaQuery rightJoinBaixaEstorno($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildContaCaixaQuery innerJoinBaixaEstorno($relationAlias = null) Adds a INNER JOIN clause to the query using the BaixaEstorno relation
 *
 * @method     ChildContaCaixaQuery joinWithBaixaEstorno($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildContaCaixaQuery leftJoinWithBaixaEstorno() Adds a LEFT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildContaCaixaQuery rightJoinWithBaixaEstorno() Adds a RIGHT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildContaCaixaQuery innerJoinWithBaixaEstorno() Adds a INNER JOIN clause and with to the query using the BaixaEstorno relation
 *
 * @method     \ImaTelecomBundle\Model\BancoAgenciaContaQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BaixaQuery|\ImaTelecomBundle\Model\BaixaEstornoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildContaCaixa findOne(ConnectionInterface $con = null) Return the first ChildContaCaixa matching the query
 * @method     ChildContaCaixa findOneOrCreate(ConnectionInterface $con = null) Return the first ChildContaCaixa matching the query, or a new ChildContaCaixa object populated from the query conditions when no match is found
 *
 * @method     ChildContaCaixa findOneByIdcontaCaixa(int $idconta_caixa) Return the first ChildContaCaixa filtered by the idconta_caixa column
 * @method     ChildContaCaixa findOneByDescricao(string $descricao) Return the first ChildContaCaixa filtered by the descricao column
 * @method     ChildContaCaixa findOneByBancoAgenciaContaId(int $banco_agencia_conta_id) Return the first ChildContaCaixa filtered by the banco_agencia_conta_id column
 * @method     ChildContaCaixa findOneByAtivo(boolean $ativo) Return the first ChildContaCaixa filtered by the ativo column
 * @method     ChildContaCaixa findOneByDataCadastro(string $data_cadastro) Return the first ChildContaCaixa filtered by the data_cadastro column
 * @method     ChildContaCaixa findOneByDataAlterado(string $data_alterado) Return the first ChildContaCaixa filtered by the data_alterado column
 * @method     ChildContaCaixa findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildContaCaixa filtered by the usuario_alterado column *

 * @method     ChildContaCaixa requirePk($key, ConnectionInterface $con = null) Return the ChildContaCaixa by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContaCaixa requireOne(ConnectionInterface $con = null) Return the first ChildContaCaixa matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContaCaixa requireOneByIdcontaCaixa(int $idconta_caixa) Return the first ChildContaCaixa filtered by the idconta_caixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContaCaixa requireOneByDescricao(string $descricao) Return the first ChildContaCaixa filtered by the descricao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContaCaixa requireOneByBancoAgenciaContaId(int $banco_agencia_conta_id) Return the first ChildContaCaixa filtered by the banco_agencia_conta_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContaCaixa requireOneByAtivo(boolean $ativo) Return the first ChildContaCaixa filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContaCaixa requireOneByDataCadastro(string $data_cadastro) Return the first ChildContaCaixa filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContaCaixa requireOneByDataAlterado(string $data_alterado) Return the first ChildContaCaixa filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContaCaixa requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildContaCaixa filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContaCaixa[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildContaCaixa objects based on current ModelCriteria
 * @method     ChildContaCaixa[]|ObjectCollection findByIdcontaCaixa(int $idconta_caixa) Return ChildContaCaixa objects filtered by the idconta_caixa column
 * @method     ChildContaCaixa[]|ObjectCollection findByDescricao(string $descricao) Return ChildContaCaixa objects filtered by the descricao column
 * @method     ChildContaCaixa[]|ObjectCollection findByBancoAgenciaContaId(int $banco_agencia_conta_id) Return ChildContaCaixa objects filtered by the banco_agencia_conta_id column
 * @method     ChildContaCaixa[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildContaCaixa objects filtered by the ativo column
 * @method     ChildContaCaixa[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildContaCaixa objects filtered by the data_cadastro column
 * @method     ChildContaCaixa[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildContaCaixa objects filtered by the data_alterado column
 * @method     ChildContaCaixa[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildContaCaixa objects filtered by the usuario_alterado column
 * @method     ChildContaCaixa[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ContaCaixaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ContaCaixaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ContaCaixa', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildContaCaixaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildContaCaixaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildContaCaixaQuery) {
            return $criteria;
        }
        $query = new ChildContaCaixaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildContaCaixa|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContaCaixaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ContaCaixaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContaCaixa A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idconta_caixa, descricao, banco_agencia_conta_id, ativo, data_cadastro, data_alterado, usuario_alterado FROM conta_caixa WHERE idconta_caixa = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildContaCaixa $obj */
            $obj = new ChildContaCaixa();
            $obj->hydrate($row);
            ContaCaixaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildContaCaixa|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ContaCaixaTableMap::COL_IDCONTA_CAIXA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ContaCaixaTableMap::COL_IDCONTA_CAIXA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idconta_caixa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcontaCaixa(1234); // WHERE idconta_caixa = 1234
     * $query->filterByIdcontaCaixa(array(12, 34)); // WHERE idconta_caixa IN (12, 34)
     * $query->filterByIdcontaCaixa(array('min' => 12)); // WHERE idconta_caixa > 12
     * </code>
     *
     * @param     mixed $idcontaCaixa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByIdcontaCaixa($idcontaCaixa = null, $comparison = null)
    {
        if (is_array($idcontaCaixa)) {
            $useMinMax = false;
            if (isset($idcontaCaixa['min'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_IDCONTA_CAIXA, $idcontaCaixa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcontaCaixa['max'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_IDCONTA_CAIXA, $idcontaCaixa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContaCaixaTableMap::COL_IDCONTA_CAIXA, $idcontaCaixa, $comparison);
    }

    /**
     * Filter the query on the descricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricao('fooValue');   // WHERE descricao = 'fooValue'
     * $query->filterByDescricao('%fooValue%', Criteria::LIKE); // WHERE descricao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByDescricao($descricao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContaCaixaTableMap::COL_DESCRICAO, $descricao, $comparison);
    }

    /**
     * Filter the query on the banco_agencia_conta_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBancoAgenciaContaId(1234); // WHERE banco_agencia_conta_id = 1234
     * $query->filterByBancoAgenciaContaId(array(12, 34)); // WHERE banco_agencia_conta_id IN (12, 34)
     * $query->filterByBancoAgenciaContaId(array('min' => 12)); // WHERE banco_agencia_conta_id > 12
     * </code>
     *
     * @see       filterByBancoAgenciaConta()
     *
     * @param     mixed $bancoAgenciaContaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByBancoAgenciaContaId($bancoAgenciaContaId = null, $comparison = null)
    {
        if (is_array($bancoAgenciaContaId)) {
            $useMinMax = false;
            if (isset($bancoAgenciaContaId['min'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaContaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bancoAgenciaContaId['max'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaContaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContaCaixaTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaContaId, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContaCaixaTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContaCaixaTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContaCaixaTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ContaCaixaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContaCaixaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BancoAgenciaConta object
     *
     * @param \ImaTelecomBundle\Model\BancoAgenciaConta|ObjectCollection $bancoAgenciaConta The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByBancoAgenciaConta($bancoAgenciaConta, $comparison = null)
    {
        if ($bancoAgenciaConta instanceof \ImaTelecomBundle\Model\BancoAgenciaConta) {
            return $this
                ->addUsingAlias(ContaCaixaTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaConta->getIdbancoAgenciaConta(), $comparison);
        } elseif ($bancoAgenciaConta instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContaCaixaTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaConta->toKeyValue('PrimaryKey', 'IdbancoAgenciaConta'), $comparison);
        } else {
            throw new PropelException('filterByBancoAgenciaConta() only accepts arguments of type \ImaTelecomBundle\Model\BancoAgenciaConta or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BancoAgenciaConta relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function joinBancoAgenciaConta($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BancoAgenciaConta');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BancoAgenciaConta');
        }

        return $this;
    }

    /**
     * Use the BancoAgenciaConta relation BancoAgenciaConta object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoAgenciaContaQuery A secondary query class using the current class as primary query
     */
    public function useBancoAgenciaContaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBancoAgenciaConta($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BancoAgenciaConta', '\ImaTelecomBundle\Model\BancoAgenciaContaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ContaCaixaTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContaCaixaTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Baixa object
     *
     * @param \ImaTelecomBundle\Model\Baixa|ObjectCollection $baixa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByBaixa($baixa, $comparison = null)
    {
        if ($baixa instanceof \ImaTelecomBundle\Model\Baixa) {
            return $this
                ->addUsingAlias(ContaCaixaTableMap::COL_IDCONTA_CAIXA, $baixa->getContaCaixaId(), $comparison);
        } elseif ($baixa instanceof ObjectCollection) {
            return $this
                ->useBaixaQuery()
                ->filterByPrimaryKeys($baixa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixa() only accepts arguments of type \ImaTelecomBundle\Model\Baixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Baixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function joinBaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Baixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Baixa');
        }

        return $this;
    }

    /**
     * Use the Baixa relation Baixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaQuery A secondary query class using the current class as primary query
     */
    public function useBaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Baixa', '\ImaTelecomBundle\Model\BaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BaixaEstorno object
     *
     * @param \ImaTelecomBundle\Model\BaixaEstorno|ObjectCollection $baixaEstorno the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildContaCaixaQuery The current query, for fluid interface
     */
    public function filterByBaixaEstorno($baixaEstorno, $comparison = null)
    {
        if ($baixaEstorno instanceof \ImaTelecomBundle\Model\BaixaEstorno) {
            return $this
                ->addUsingAlias(ContaCaixaTableMap::COL_IDCONTA_CAIXA, $baixaEstorno->getContaCaixaId(), $comparison);
        } elseif ($baixaEstorno instanceof ObjectCollection) {
            return $this
                ->useBaixaEstornoQuery()
                ->filterByPrimaryKeys($baixaEstorno->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixaEstorno() only accepts arguments of type \ImaTelecomBundle\Model\BaixaEstorno or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaixaEstorno relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function joinBaixaEstorno($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaixaEstorno');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaixaEstorno');
        }

        return $this;
    }

    /**
     * Use the BaixaEstorno relation BaixaEstorno object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaEstornoQuery A secondary query class using the current class as primary query
     */
    public function useBaixaEstornoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixaEstorno($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaixaEstorno', '\ImaTelecomBundle\Model\BaixaEstornoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildContaCaixa $contaCaixa Object to remove from the list of results
     *
     * @return $this|ChildContaCaixaQuery The current query, for fluid interface
     */
    public function prune($contaCaixa = null)
    {
        if ($contaCaixa) {
            $this->addUsingAlias(ContaCaixaTableMap::COL_IDCONTA_CAIXA, $contaCaixa->getIdcontaCaixa(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the conta_caixa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContaCaixaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ContaCaixaTableMap::clearInstancePool();
            ContaCaixaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContaCaixaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ContaCaixaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ContaCaixaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ContaCaixaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ContaCaixaQuery
