<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Competencia as ChildCompetencia;
use ImaTelecomBundle\Model\CompetenciaQuery as ChildCompetenciaQuery;
use ImaTelecomBundle\Model\Map\CompetenciaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'competencia' table.
 *
 *
 *
 * @method     ChildCompetenciaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCompetenciaQuery orderByAno($order = Criteria::ASC) Order by the ano column
 * @method     ChildCompetenciaQuery orderByMes($order = Criteria::ASC) Order by the mes column
 * @method     ChildCompetenciaQuery orderByNumeroNf($order = Criteria::ASC) Order by the numero_nf column
 * @method     ChildCompetenciaQuery orderByPeriodoInicial($order = Criteria::ASC) Order by the periodo_inicial column
 * @method     ChildCompetenciaQuery orderByPeriodoFinal($order = Criteria::ASC) Order by the periodo_final column
 * @method     ChildCompetenciaQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildCompetenciaQuery orderByDescricao($order = Criteria::ASC) Order by the descricao column
 * @method     ChildCompetenciaQuery orderByPeriodoFuturo($order = Criteria::ASC) Order by the periodo_futuro column
 * @method     ChildCompetenciaQuery orderByProximaCompetencia($order = Criteria::ASC) Order by the proxima_competencia column
 * @method     ChildCompetenciaQuery orderByEncerramento($order = Criteria::ASC) Order by the encerramento column
 * @method     ChildCompetenciaQuery orderByTrabalharPeriodoFuturo($order = Criteria::ASC) Order by the trabalhar_periodo_futuro column
 * @method     ChildCompetenciaQuery orderByUltimaDataEmissao($order = Criteria::ASC) Order by the ultima_data_emissao column
 *
 * @method     ChildCompetenciaQuery groupById() Group by the id column
 * @method     ChildCompetenciaQuery groupByAno() Group by the ano column
 * @method     ChildCompetenciaQuery groupByMes() Group by the mes column
 * @method     ChildCompetenciaQuery groupByNumeroNf() Group by the numero_nf column
 * @method     ChildCompetenciaQuery groupByPeriodoInicial() Group by the periodo_inicial column
 * @method     ChildCompetenciaQuery groupByPeriodoFinal() Group by the periodo_final column
 * @method     ChildCompetenciaQuery groupByAtivo() Group by the ativo column
 * @method     ChildCompetenciaQuery groupByDescricao() Group by the descricao column
 * @method     ChildCompetenciaQuery groupByPeriodoFuturo() Group by the periodo_futuro column
 * @method     ChildCompetenciaQuery groupByProximaCompetencia() Group by the proxima_competencia column
 * @method     ChildCompetenciaQuery groupByEncerramento() Group by the encerramento column
 * @method     ChildCompetenciaQuery groupByTrabalharPeriodoFuturo() Group by the trabalhar_periodo_futuro column
 * @method     ChildCompetenciaQuery groupByUltimaDataEmissao() Group by the ultima_data_emissao column
 *
 * @method     ChildCompetenciaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCompetenciaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCompetenciaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCompetenciaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCompetenciaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCompetenciaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCompetenciaQuery leftJoinBaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Baixa relation
 * @method     ChildCompetenciaQuery rightJoinBaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Baixa relation
 * @method     ChildCompetenciaQuery innerJoinBaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Baixa relation
 *
 * @method     ChildCompetenciaQuery joinWithBaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Baixa relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithBaixa() Adds a LEFT JOIN clause and with to the query using the Baixa relation
 * @method     ChildCompetenciaQuery rightJoinWithBaixa() Adds a RIGHT JOIN clause and with to the query using the Baixa relation
 * @method     ChildCompetenciaQuery innerJoinWithBaixa() Adds a INNER JOIN clause and with to the query using the Baixa relation
 *
 * @method     ChildCompetenciaQuery leftJoinBaixaEstorno($relationAlias = null) Adds a LEFT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildCompetenciaQuery rightJoinBaixaEstorno($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BaixaEstorno relation
 * @method     ChildCompetenciaQuery innerJoinBaixaEstorno($relationAlias = null) Adds a INNER JOIN clause to the query using the BaixaEstorno relation
 *
 * @method     ChildCompetenciaQuery joinWithBaixaEstorno($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithBaixaEstorno() Adds a LEFT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildCompetenciaQuery rightJoinWithBaixaEstorno() Adds a RIGHT JOIN clause and with to the query using the BaixaEstorno relation
 * @method     ChildCompetenciaQuery innerJoinWithBaixaEstorno() Adds a INNER JOIN clause and with to the query using the BaixaEstorno relation
 *
 * @method     ChildCompetenciaQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildCompetenciaQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildCompetenciaQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildCompetenciaQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildCompetenciaQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildCompetenciaQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildCompetenciaQuery leftJoinBoletoBaixaHistorico($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildCompetenciaQuery rightJoinBoletoBaixaHistorico($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildCompetenciaQuery innerJoinBoletoBaixaHistorico($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildCompetenciaQuery joinWithBoletoBaixaHistorico($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithBoletoBaixaHistorico() Adds a LEFT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildCompetenciaQuery rightJoinWithBoletoBaixaHistorico() Adds a RIGHT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildCompetenciaQuery innerJoinWithBoletoBaixaHistorico() Adds a INNER JOIN clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildCompetenciaQuery leftJoinDadosFiscal($relationAlias = null) Adds a LEFT JOIN clause to the query using the DadosFiscal relation
 * @method     ChildCompetenciaQuery rightJoinDadosFiscal($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DadosFiscal relation
 * @method     ChildCompetenciaQuery innerJoinDadosFiscal($relationAlias = null) Adds a INNER JOIN clause to the query using the DadosFiscal relation
 *
 * @method     ChildCompetenciaQuery joinWithDadosFiscal($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DadosFiscal relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithDadosFiscal() Adds a LEFT JOIN clause and with to the query using the DadosFiscal relation
 * @method     ChildCompetenciaQuery rightJoinWithDadosFiscal() Adds a RIGHT JOIN clause and with to the query using the DadosFiscal relation
 * @method     ChildCompetenciaQuery innerJoinWithDadosFiscal() Adds a INNER JOIN clause and with to the query using the DadosFiscal relation
 *
 * @method     ChildCompetenciaQuery leftJoinFaturamento($relationAlias = null) Adds a LEFT JOIN clause to the query using the Faturamento relation
 * @method     ChildCompetenciaQuery rightJoinFaturamento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Faturamento relation
 * @method     ChildCompetenciaQuery innerJoinFaturamento($relationAlias = null) Adds a INNER JOIN clause to the query using the Faturamento relation
 *
 * @method     ChildCompetenciaQuery joinWithFaturamento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Faturamento relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithFaturamento() Adds a LEFT JOIN clause and with to the query using the Faturamento relation
 * @method     ChildCompetenciaQuery rightJoinWithFaturamento() Adds a RIGHT JOIN clause and with to the query using the Faturamento relation
 * @method     ChildCompetenciaQuery innerJoinWithFaturamento() Adds a INNER JOIN clause and with to the query using the Faturamento relation
 *
 * @method     ChildCompetenciaQuery leftJoinLancamentos($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lancamentos relation
 * @method     ChildCompetenciaQuery rightJoinLancamentos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lancamentos relation
 * @method     ChildCompetenciaQuery innerJoinLancamentos($relationAlias = null) Adds a INNER JOIN clause to the query using the Lancamentos relation
 *
 * @method     ChildCompetenciaQuery joinWithLancamentos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Lancamentos relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithLancamentos() Adds a LEFT JOIN clause and with to the query using the Lancamentos relation
 * @method     ChildCompetenciaQuery rightJoinWithLancamentos() Adds a RIGHT JOIN clause and with to the query using the Lancamentos relation
 * @method     ChildCompetenciaQuery innerJoinWithLancamentos() Adds a INNER JOIN clause and with to the query using the Lancamentos relation
 *
 * @method     ChildCompetenciaQuery leftJoinLancamentosBoletos($relationAlias = null) Adds a LEFT JOIN clause to the query using the LancamentosBoletos relation
 * @method     ChildCompetenciaQuery rightJoinLancamentosBoletos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LancamentosBoletos relation
 * @method     ChildCompetenciaQuery innerJoinLancamentosBoletos($relationAlias = null) Adds a INNER JOIN clause to the query using the LancamentosBoletos relation
 *
 * @method     ChildCompetenciaQuery joinWithLancamentosBoletos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LancamentosBoletos relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithLancamentosBoletos() Adds a LEFT JOIN clause and with to the query using the LancamentosBoletos relation
 * @method     ChildCompetenciaQuery rightJoinWithLancamentosBoletos() Adds a RIGHT JOIN clause and with to the query using the LancamentosBoletos relation
 * @method     ChildCompetenciaQuery innerJoinWithLancamentosBoletos() Adds a INNER JOIN clause and with to the query using the LancamentosBoletos relation
 *
 * @method     ChildCompetenciaQuery leftJoinSici($relationAlias = null) Adds a LEFT JOIN clause to the query using the Sici relation
 * @method     ChildCompetenciaQuery rightJoinSici($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Sici relation
 * @method     ChildCompetenciaQuery innerJoinSici($relationAlias = null) Adds a INNER JOIN clause to the query using the Sici relation
 *
 * @method     ChildCompetenciaQuery joinWithSici($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Sici relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithSici() Adds a LEFT JOIN clause and with to the query using the Sici relation
 * @method     ChildCompetenciaQuery rightJoinWithSici() Adds a RIGHT JOIN clause and with to the query using the Sici relation
 * @method     ChildCompetenciaQuery innerJoinWithSici() Adds a INNER JOIN clause and with to the query using the Sici relation
 *
 * @method     ChildCompetenciaQuery leftJoinSintegra($relationAlias = null) Adds a LEFT JOIN clause to the query using the Sintegra relation
 * @method     ChildCompetenciaQuery rightJoinSintegra($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Sintegra relation
 * @method     ChildCompetenciaQuery innerJoinSintegra($relationAlias = null) Adds a INNER JOIN clause to the query using the Sintegra relation
 *
 * @method     ChildCompetenciaQuery joinWithSintegra($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Sintegra relation
 *
 * @method     ChildCompetenciaQuery leftJoinWithSintegra() Adds a LEFT JOIN clause and with to the query using the Sintegra relation
 * @method     ChildCompetenciaQuery rightJoinWithSintegra() Adds a RIGHT JOIN clause and with to the query using the Sintegra relation
 * @method     ChildCompetenciaQuery innerJoinWithSintegra() Adds a INNER JOIN clause and with to the query using the Sintegra relation
 *
 * @method     \ImaTelecomBundle\Model\BaixaQuery|\ImaTelecomBundle\Model\BaixaEstornoQuery|\ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery|\ImaTelecomBundle\Model\DadosFiscalQuery|\ImaTelecomBundle\Model\FaturamentoQuery|\ImaTelecomBundle\Model\LancamentosQuery|\ImaTelecomBundle\Model\LancamentosBoletosQuery|\ImaTelecomBundle\Model\SiciQuery|\ImaTelecomBundle\Model\SintegraQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCompetencia findOne(ConnectionInterface $con = null) Return the first ChildCompetencia matching the query
 * @method     ChildCompetencia findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCompetencia matching the query, or a new ChildCompetencia object populated from the query conditions when no match is found
 *
 * @method     ChildCompetencia findOneById(int $id) Return the first ChildCompetencia filtered by the id column
 * @method     ChildCompetencia findOneByAno(string $ano) Return the first ChildCompetencia filtered by the ano column
 * @method     ChildCompetencia findOneByMes(string $mes) Return the first ChildCompetencia filtered by the mes column
 * @method     ChildCompetencia findOneByNumeroNf(int $numero_nf) Return the first ChildCompetencia filtered by the numero_nf column
 * @method     ChildCompetencia findOneByPeriodoInicial(string $periodo_inicial) Return the first ChildCompetencia filtered by the periodo_inicial column
 * @method     ChildCompetencia findOneByPeriodoFinal(string $periodo_final) Return the first ChildCompetencia filtered by the periodo_final column
 * @method     ChildCompetencia findOneByAtivo(int $ativo) Return the first ChildCompetencia filtered by the ativo column
 * @method     ChildCompetencia findOneByDescricao(string $descricao) Return the first ChildCompetencia filtered by the descricao column
 * @method     ChildCompetencia findOneByPeriodoFuturo(int $periodo_futuro) Return the first ChildCompetencia filtered by the periodo_futuro column
 * @method     ChildCompetencia findOneByProximaCompetencia(int $proxima_competencia) Return the first ChildCompetencia filtered by the proxima_competencia column
 * @method     ChildCompetencia findOneByEncerramento(int $encerramento) Return the first ChildCompetencia filtered by the encerramento column
 * @method     ChildCompetencia findOneByTrabalharPeriodoFuturo(int $trabalhar_periodo_futuro) Return the first ChildCompetencia filtered by the trabalhar_periodo_futuro column
 * @method     ChildCompetencia findOneByUltimaDataEmissao(string $ultima_data_emissao) Return the first ChildCompetencia filtered by the ultima_data_emissao column *

 * @method     ChildCompetencia requirePk($key, ConnectionInterface $con = null) Return the ChildCompetencia by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOne(ConnectionInterface $con = null) Return the first ChildCompetencia matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompetencia requireOneById(int $id) Return the first ChildCompetencia filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByAno(string $ano) Return the first ChildCompetencia filtered by the ano column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByMes(string $mes) Return the first ChildCompetencia filtered by the mes column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByNumeroNf(int $numero_nf) Return the first ChildCompetencia filtered by the numero_nf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByPeriodoInicial(string $periodo_inicial) Return the first ChildCompetencia filtered by the periodo_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByPeriodoFinal(string $periodo_final) Return the first ChildCompetencia filtered by the periodo_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByAtivo(int $ativo) Return the first ChildCompetencia filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByDescricao(string $descricao) Return the first ChildCompetencia filtered by the descricao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByPeriodoFuturo(int $periodo_futuro) Return the first ChildCompetencia filtered by the periodo_futuro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByProximaCompetencia(int $proxima_competencia) Return the first ChildCompetencia filtered by the proxima_competencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByEncerramento(int $encerramento) Return the first ChildCompetencia filtered by the encerramento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByTrabalharPeriodoFuturo(int $trabalhar_periodo_futuro) Return the first ChildCompetencia filtered by the trabalhar_periodo_futuro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompetencia requireOneByUltimaDataEmissao(string $ultima_data_emissao) Return the first ChildCompetencia filtered by the ultima_data_emissao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompetencia[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCompetencia objects based on current ModelCriteria
 * @method     ChildCompetencia[]|ObjectCollection findById(int $id) Return ChildCompetencia objects filtered by the id column
 * @method     ChildCompetencia[]|ObjectCollection findByAno(string $ano) Return ChildCompetencia objects filtered by the ano column
 * @method     ChildCompetencia[]|ObjectCollection findByMes(string $mes) Return ChildCompetencia objects filtered by the mes column
 * @method     ChildCompetencia[]|ObjectCollection findByNumeroNf(int $numero_nf) Return ChildCompetencia objects filtered by the numero_nf column
 * @method     ChildCompetencia[]|ObjectCollection findByPeriodoInicial(string $periodo_inicial) Return ChildCompetencia objects filtered by the periodo_inicial column
 * @method     ChildCompetencia[]|ObjectCollection findByPeriodoFinal(string $periodo_final) Return ChildCompetencia objects filtered by the periodo_final column
 * @method     ChildCompetencia[]|ObjectCollection findByAtivo(int $ativo) Return ChildCompetencia objects filtered by the ativo column
 * @method     ChildCompetencia[]|ObjectCollection findByDescricao(string $descricao) Return ChildCompetencia objects filtered by the descricao column
 * @method     ChildCompetencia[]|ObjectCollection findByPeriodoFuturo(int $periodo_futuro) Return ChildCompetencia objects filtered by the periodo_futuro column
 * @method     ChildCompetencia[]|ObjectCollection findByProximaCompetencia(int $proxima_competencia) Return ChildCompetencia objects filtered by the proxima_competencia column
 * @method     ChildCompetencia[]|ObjectCollection findByEncerramento(int $encerramento) Return ChildCompetencia objects filtered by the encerramento column
 * @method     ChildCompetencia[]|ObjectCollection findByTrabalharPeriodoFuturo(int $trabalhar_periodo_futuro) Return ChildCompetencia objects filtered by the trabalhar_periodo_futuro column
 * @method     ChildCompetencia[]|ObjectCollection findByUltimaDataEmissao(string $ultima_data_emissao) Return ChildCompetencia objects filtered by the ultima_data_emissao column
 * @method     ChildCompetencia[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CompetenciaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\CompetenciaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Competencia', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCompetenciaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCompetenciaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCompetenciaQuery) {
            return $criteria;
        }
        $query = new ChildCompetenciaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCompetencia|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompetenciaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CompetenciaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompetencia A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, ano, mes, numero_nf, periodo_inicial, periodo_final, ativo, descricao, periodo_futuro, proxima_competencia, encerramento, trabalhar_periodo_futuro, ultima_data_emissao FROM competencia WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCompetencia $obj */
            $obj = new ChildCompetencia();
            $obj->hydrate($row);
            CompetenciaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCompetencia|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CompetenciaTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CompetenciaTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the ano column
     *
     * Example usage:
     * <code>
     * $query->filterByAno('fooValue');   // WHERE ano = 'fooValue'
     * $query->filterByAno('%fooValue%', Criteria::LIKE); // WHERE ano LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ano The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByAno($ano = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ano)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_ANO, $ano, $comparison);
    }

    /**
     * Filter the query on the mes column
     *
     * Example usage:
     * <code>
     * $query->filterByMes('fooValue');   // WHERE mes = 'fooValue'
     * $query->filterByMes('%fooValue%', Criteria::LIKE); // WHERE mes LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mes The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByMes($mes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mes)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_MES, $mes, $comparison);
    }

    /**
     * Filter the query on the numero_nf column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroNf(1234); // WHERE numero_nf = 1234
     * $query->filterByNumeroNf(array(12, 34)); // WHERE numero_nf IN (12, 34)
     * $query->filterByNumeroNf(array('min' => 12)); // WHERE numero_nf > 12
     * </code>
     *
     * @param     mixed $numeroNf The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByNumeroNf($numeroNf = null, $comparison = null)
    {
        if (is_array($numeroNf)) {
            $useMinMax = false;
            if (isset($numeroNf['min'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_NUMERO_NF, $numeroNf['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numeroNf['max'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_NUMERO_NF, $numeroNf['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_NUMERO_NF, $numeroNf, $comparison);
    }

    /**
     * Filter the query on the periodo_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodoInicial('2011-03-14'); // WHERE periodo_inicial = '2011-03-14'
     * $query->filterByPeriodoInicial('now'); // WHERE periodo_inicial = '2011-03-14'
     * $query->filterByPeriodoInicial(array('max' => 'yesterday')); // WHERE periodo_inicial > '2011-03-13'
     * </code>
     *
     * @param     mixed $periodoInicial The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByPeriodoInicial($periodoInicial = null, $comparison = null)
    {
        if (is_array($periodoInicial)) {
            $useMinMax = false;
            if (isset($periodoInicial['min'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_PERIODO_INICIAL, $periodoInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodoInicial['max'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_PERIODO_INICIAL, $periodoInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_PERIODO_INICIAL, $periodoInicial, $comparison);
    }

    /**
     * Filter the query on the periodo_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodoFinal('2011-03-14'); // WHERE periodo_final = '2011-03-14'
     * $query->filterByPeriodoFinal('now'); // WHERE periodo_final = '2011-03-14'
     * $query->filterByPeriodoFinal(array('max' => 'yesterday')); // WHERE periodo_final > '2011-03-13'
     * </code>
     *
     * @param     mixed $periodoFinal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByPeriodoFinal($periodoFinal = null, $comparison = null)
    {
        if (is_array($periodoFinal)) {
            $useMinMax = false;
            if (isset($periodoFinal['min'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_PERIODO_FINAL, $periodoFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodoFinal['max'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_PERIODO_FINAL, $periodoFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_PERIODO_FINAL, $periodoFinal, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(1234); // WHERE ativo = 1234
     * $query->filterByAtivo(array(12, 34)); // WHERE ativo IN (12, 34)
     * $query->filterByAtivo(array('min' => 12)); // WHERE ativo > 12
     * </code>
     *
     * @param     mixed $ativo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_array($ativo)) {
            $useMinMax = false;
            if (isset($ativo['min'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_ATIVO, $ativo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ativo['max'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_ATIVO, $ativo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the descricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricao('fooValue');   // WHERE descricao = 'fooValue'
     * $query->filterByDescricao('%fooValue%', Criteria::LIKE); // WHERE descricao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByDescricao($descricao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_DESCRICAO, $descricao, $comparison);
    }

    /**
     * Filter the query on the periodo_futuro column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodoFuturo(1234); // WHERE periodo_futuro = 1234
     * $query->filterByPeriodoFuturo(array(12, 34)); // WHERE periodo_futuro IN (12, 34)
     * $query->filterByPeriodoFuturo(array('min' => 12)); // WHERE periodo_futuro > 12
     * </code>
     *
     * @param     mixed $periodoFuturo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByPeriodoFuturo($periodoFuturo = null, $comparison = null)
    {
        if (is_array($periodoFuturo)) {
            $useMinMax = false;
            if (isset($periodoFuturo['min'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_PERIODO_FUTURO, $periodoFuturo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodoFuturo['max'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_PERIODO_FUTURO, $periodoFuturo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_PERIODO_FUTURO, $periodoFuturo, $comparison);
    }

    /**
     * Filter the query on the proxima_competencia column
     *
     * Example usage:
     * <code>
     * $query->filterByProximaCompetencia(1234); // WHERE proxima_competencia = 1234
     * $query->filterByProximaCompetencia(array(12, 34)); // WHERE proxima_competencia IN (12, 34)
     * $query->filterByProximaCompetencia(array('min' => 12)); // WHERE proxima_competencia > 12
     * </code>
     *
     * @param     mixed $proximaCompetencia The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByProximaCompetencia($proximaCompetencia = null, $comparison = null)
    {
        if (is_array($proximaCompetencia)) {
            $useMinMax = false;
            if (isset($proximaCompetencia['min'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_PROXIMA_COMPETENCIA, $proximaCompetencia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($proximaCompetencia['max'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_PROXIMA_COMPETENCIA, $proximaCompetencia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_PROXIMA_COMPETENCIA, $proximaCompetencia, $comparison);
    }

    /**
     * Filter the query on the encerramento column
     *
     * Example usage:
     * <code>
     * $query->filterByEncerramento(1234); // WHERE encerramento = 1234
     * $query->filterByEncerramento(array(12, 34)); // WHERE encerramento IN (12, 34)
     * $query->filterByEncerramento(array('min' => 12)); // WHERE encerramento > 12
     * </code>
     *
     * @param     mixed $encerramento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByEncerramento($encerramento = null, $comparison = null)
    {
        if (is_array($encerramento)) {
            $useMinMax = false;
            if (isset($encerramento['min'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_ENCERRAMENTO, $encerramento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($encerramento['max'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_ENCERRAMENTO, $encerramento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_ENCERRAMENTO, $encerramento, $comparison);
    }

    /**
     * Filter the query on the trabalhar_periodo_futuro column
     *
     * Example usage:
     * <code>
     * $query->filterByTrabalharPeriodoFuturo(1234); // WHERE trabalhar_periodo_futuro = 1234
     * $query->filterByTrabalharPeriodoFuturo(array(12, 34)); // WHERE trabalhar_periodo_futuro IN (12, 34)
     * $query->filterByTrabalharPeriodoFuturo(array('min' => 12)); // WHERE trabalhar_periodo_futuro > 12
     * </code>
     *
     * @param     mixed $trabalharPeriodoFuturo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByTrabalharPeriodoFuturo($trabalharPeriodoFuturo = null, $comparison = null)
    {
        if (is_array($trabalharPeriodoFuturo)) {
            $useMinMax = false;
            if (isset($trabalharPeriodoFuturo['min'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO, $trabalharPeriodoFuturo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($trabalharPeriodoFuturo['max'])) {
                $this->addUsingAlias(CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO, $trabalharPeriodoFuturo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO, $trabalharPeriodoFuturo, $comparison);
    }

    /**
     * Filter the query on the ultima_data_emissao column
     *
     * Example usage:
     * <code>
     * $query->filterByUltimaDataEmissao('fooValue');   // WHERE ultima_data_emissao = 'fooValue'
     * $query->filterByUltimaDataEmissao('%fooValue%', Criteria::LIKE); // WHERE ultima_data_emissao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ultimaDataEmissao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByUltimaDataEmissao($ultimaDataEmissao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ultimaDataEmissao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompetenciaTableMap::COL_ULTIMA_DATA_EMISSAO, $ultimaDataEmissao, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Baixa object
     *
     * @param \ImaTelecomBundle\Model\Baixa|ObjectCollection $baixa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByBaixa($baixa, $comparison = null)
    {
        if ($baixa instanceof \ImaTelecomBundle\Model\Baixa) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $baixa->getCompetenciaId(), $comparison);
        } elseif ($baixa instanceof ObjectCollection) {
            return $this
                ->useBaixaQuery()
                ->filterByPrimaryKeys($baixa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixa() only accepts arguments of type \ImaTelecomBundle\Model\Baixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Baixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinBaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Baixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Baixa');
        }

        return $this;
    }

    /**
     * Use the Baixa relation Baixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaQuery A secondary query class using the current class as primary query
     */
    public function useBaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Baixa', '\ImaTelecomBundle\Model\BaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BaixaEstorno object
     *
     * @param \ImaTelecomBundle\Model\BaixaEstorno|ObjectCollection $baixaEstorno the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByBaixaEstorno($baixaEstorno, $comparison = null)
    {
        if ($baixaEstorno instanceof \ImaTelecomBundle\Model\BaixaEstorno) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $baixaEstorno->getCompetenciaId(), $comparison);
        } elseif ($baixaEstorno instanceof ObjectCollection) {
            return $this
                ->useBaixaEstornoQuery()
                ->filterByPrimaryKeys($baixaEstorno->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBaixaEstorno() only accepts arguments of type \ImaTelecomBundle\Model\BaixaEstorno or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BaixaEstorno relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinBaixaEstorno($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BaixaEstorno');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BaixaEstorno');
        }

        return $this;
    }

    /**
     * Use the BaixaEstorno relation BaixaEstorno object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaEstornoQuery A secondary query class using the current class as primary query
     */
    public function useBaixaEstornoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixaEstorno($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BaixaEstorno', '\ImaTelecomBundle\Model\BaixaEstornoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $boleto->getCompetenciaId(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            return $this
                ->useBoletoQuery()
                ->filterByPrimaryKeys($boleto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoBaixaHistorico object
     *
     * @param \ImaTelecomBundle\Model\BoletoBaixaHistorico|ObjectCollection $boletoBaixaHistorico the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByBoletoBaixaHistorico($boletoBaixaHistorico, $comparison = null)
    {
        if ($boletoBaixaHistorico instanceof \ImaTelecomBundle\Model\BoletoBaixaHistorico) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $boletoBaixaHistorico->getCompetenciaId(), $comparison);
        } elseif ($boletoBaixaHistorico instanceof ObjectCollection) {
            return $this
                ->useBoletoBaixaHistoricoQuery()
                ->filterByPrimaryKeys($boletoBaixaHistorico->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoBaixaHistorico() only accepts arguments of type \ImaTelecomBundle\Model\BoletoBaixaHistorico or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoBaixaHistorico relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinBoletoBaixaHistorico($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoBaixaHistorico');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoBaixaHistorico');
        }

        return $this;
    }

    /**
     * Use the BoletoBaixaHistorico relation BoletoBaixaHistorico object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoBaixaHistoricoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoletoBaixaHistorico($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoBaixaHistorico', '\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\DadosFiscal object
     *
     * @param \ImaTelecomBundle\Model\DadosFiscal|ObjectCollection $dadosFiscal the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByDadosFiscal($dadosFiscal, $comparison = null)
    {
        if ($dadosFiscal instanceof \ImaTelecomBundle\Model\DadosFiscal) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $dadosFiscal->getCompetenciaId(), $comparison);
        } elseif ($dadosFiscal instanceof ObjectCollection) {
            return $this
                ->useDadosFiscalQuery()
                ->filterByPrimaryKeys($dadosFiscal->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDadosFiscal() only accepts arguments of type \ImaTelecomBundle\Model\DadosFiscal or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DadosFiscal relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinDadosFiscal($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DadosFiscal');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DadosFiscal');
        }

        return $this;
    }

    /**
     * Use the DadosFiscal relation DadosFiscal object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\DadosFiscalQuery A secondary query class using the current class as primary query
     */
    public function useDadosFiscalQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDadosFiscal($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DadosFiscal', '\ImaTelecomBundle\Model\DadosFiscalQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Faturamento object
     *
     * @param \ImaTelecomBundle\Model\Faturamento|ObjectCollection $faturamento the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByFaturamento($faturamento, $comparison = null)
    {
        if ($faturamento instanceof \ImaTelecomBundle\Model\Faturamento) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $faturamento->getCompetenciaId(), $comparison);
        } elseif ($faturamento instanceof ObjectCollection) {
            return $this
                ->useFaturamentoQuery()
                ->filterByPrimaryKeys($faturamento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFaturamento() only accepts arguments of type \ImaTelecomBundle\Model\Faturamento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Faturamento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinFaturamento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Faturamento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Faturamento');
        }

        return $this;
    }

    /**
     * Use the Faturamento relation Faturamento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FaturamentoQuery A secondary query class using the current class as primary query
     */
    public function useFaturamentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFaturamento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Faturamento', '\ImaTelecomBundle\Model\FaturamentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Lancamentos object
     *
     * @param \ImaTelecomBundle\Model\Lancamentos|ObjectCollection $lancamentos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByLancamentos($lancamentos, $comparison = null)
    {
        if ($lancamentos instanceof \ImaTelecomBundle\Model\Lancamentos) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $lancamentos->getCompetenciaId(), $comparison);
        } elseif ($lancamentos instanceof ObjectCollection) {
            return $this
                ->useLancamentosQuery()
                ->filterByPrimaryKeys($lancamentos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLancamentos() only accepts arguments of type \ImaTelecomBundle\Model\Lancamentos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lancamentos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinLancamentos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lancamentos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lancamentos');
        }

        return $this;
    }

    /**
     * Use the Lancamentos relation Lancamentos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\LancamentosQuery A secondary query class using the current class as primary query
     */
    public function useLancamentosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLancamentos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lancamentos', '\ImaTelecomBundle\Model\LancamentosQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\LancamentosBoletos object
     *
     * @param \ImaTelecomBundle\Model\LancamentosBoletos|ObjectCollection $lancamentosBoletos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterByLancamentosBoletos($lancamentosBoletos, $comparison = null)
    {
        if ($lancamentosBoletos instanceof \ImaTelecomBundle\Model\LancamentosBoletos) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $lancamentosBoletos->getCompetenciaId(), $comparison);
        } elseif ($lancamentosBoletos instanceof ObjectCollection) {
            return $this
                ->useLancamentosBoletosQuery()
                ->filterByPrimaryKeys($lancamentosBoletos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLancamentosBoletos() only accepts arguments of type \ImaTelecomBundle\Model\LancamentosBoletos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LancamentosBoletos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinLancamentosBoletos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LancamentosBoletos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LancamentosBoletos');
        }

        return $this;
    }

    /**
     * Use the LancamentosBoletos relation LancamentosBoletos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\LancamentosBoletosQuery A secondary query class using the current class as primary query
     */
    public function useLancamentosBoletosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLancamentosBoletos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LancamentosBoletos', '\ImaTelecomBundle\Model\LancamentosBoletosQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Sici object
     *
     * @param \ImaTelecomBundle\Model\Sici|ObjectCollection $sici the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterBySici($sici, $comparison = null)
    {
        if ($sici instanceof \ImaTelecomBundle\Model\Sici) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $sici->getCompetenciaId(), $comparison);
        } elseif ($sici instanceof ObjectCollection) {
            return $this
                ->useSiciQuery()
                ->filterByPrimaryKeys($sici->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySici() only accepts arguments of type \ImaTelecomBundle\Model\Sici or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Sici relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinSici($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Sici');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Sici');
        }

        return $this;
    }

    /**
     * Use the Sici relation Sici object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\SiciQuery A secondary query class using the current class as primary query
     */
    public function useSiciQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSici($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Sici', '\ImaTelecomBundle\Model\SiciQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Sintegra object
     *
     * @param \ImaTelecomBundle\Model\Sintegra|ObjectCollection $sintegra the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompetenciaQuery The current query, for fluid interface
     */
    public function filterBySintegra($sintegra, $comparison = null)
    {
        if ($sintegra instanceof \ImaTelecomBundle\Model\Sintegra) {
            return $this
                ->addUsingAlias(CompetenciaTableMap::COL_ID, $sintegra->getCompetenciaId(), $comparison);
        } elseif ($sintegra instanceof ObjectCollection) {
            return $this
                ->useSintegraQuery()
                ->filterByPrimaryKeys($sintegra->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySintegra() only accepts arguments of type \ImaTelecomBundle\Model\Sintegra or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Sintegra relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function joinSintegra($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Sintegra');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Sintegra');
        }

        return $this;
    }

    /**
     * Use the Sintegra relation Sintegra object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\SintegraQuery A secondary query class using the current class as primary query
     */
    public function useSintegraQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSintegra($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Sintegra', '\ImaTelecomBundle\Model\SintegraQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCompetencia $competencia Object to remove from the list of results
     *
     * @return $this|ChildCompetenciaQuery The current query, for fluid interface
     */
    public function prune($competencia = null)
    {
        if ($competencia) {
            $this->addUsingAlias(CompetenciaTableMap::COL_ID, $competencia->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the competencia table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompetenciaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CompetenciaTableMap::clearInstancePool();
            CompetenciaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompetenciaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CompetenciaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CompetenciaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CompetenciaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CompetenciaQuery
