<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Cidade as ChildCidade;
use ImaTelecomBundle\Model\CidadeQuery as ChildCidadeQuery;
use ImaTelecomBundle\Model\EnderecoAgencia as ChildEnderecoAgencia;
use ImaTelecomBundle\Model\EnderecoAgenciaQuery as ChildEnderecoAgenciaQuery;
use ImaTelecomBundle\Model\EnderecoCliente as ChildEnderecoCliente;
use ImaTelecomBundle\Model\EnderecoClienteQuery as ChildEnderecoClienteQuery;
use ImaTelecomBundle\Model\EnderecoServCliente as ChildEnderecoServCliente;
use ImaTelecomBundle\Model\EnderecoServClienteQuery as ChildEnderecoServClienteQuery;
use ImaTelecomBundle\Model\Map\CidadeTableMap;
use ImaTelecomBundle\Model\Map\EnderecoAgenciaTableMap;
use ImaTelecomBundle\Model\Map\EnderecoClienteTableMap;
use ImaTelecomBundle\Model\Map\EnderecoServClienteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'cidade' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Cidade implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\CidadeTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idcidade field.
     *
     * @var        int
     */
    protected $idcidade;

    /**
     * The value for the uf field.
     *
     * @var        string
     */
    protected $uf;

    /**
     * The value for the municipio field.
     *
     * @var        string
     */
    protected $municipio;

    /**
     * The value for the codigo field.
     *
     * @var        string
     */
    protected $codigo;

    /**
     * @var        ObjectCollection|ChildEnderecoAgencia[] Collection to store aggregation of ChildEnderecoAgencia objects.
     */
    protected $collEnderecoAgencias;
    protected $collEnderecoAgenciasPartial;

    /**
     * @var        ObjectCollection|ChildEnderecoCliente[] Collection to store aggregation of ChildEnderecoCliente objects.
     */
    protected $collEnderecoClientes;
    protected $collEnderecoClientesPartial;

    /**
     * @var        ObjectCollection|ChildEnderecoServCliente[] Collection to store aggregation of ChildEnderecoServCliente objects.
     */
    protected $collEnderecoServClientes;
    protected $collEnderecoServClientesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEnderecoAgencia[]
     */
    protected $enderecoAgenciasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEnderecoCliente[]
     */
    protected $enderecoClientesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEnderecoServCliente[]
     */
    protected $enderecoServClientesScheduledForDeletion = null;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Cidade object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Cidade</code> instance.  If
     * <code>obj</code> is an instance of <code>Cidade</code>, delegates to
     * <code>equals(Cidade)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Cidade The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idcidade] column value.
     *
     * @return int
     */
    public function getIdcidade()
    {
        return $this->idcidade;
    }

    /**
     * Get the [uf] column value.
     *
     * @return string
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * Get the [municipio] column value.
     *
     * @return string
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Get the [codigo] column value.
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set the value of [idcidade] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Cidade The current object (for fluent API support)
     */
    public function setIdcidade($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idcidade !== $v) {
            $this->idcidade = $v;
            $this->modifiedColumns[CidadeTableMap::COL_IDCIDADE] = true;
        }

        return $this;
    } // setIdcidade()

    /**
     * Set the value of [uf] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Cidade The current object (for fluent API support)
     */
    public function setUf($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->uf !== $v) {
            $this->uf = $v;
            $this->modifiedColumns[CidadeTableMap::COL_UF] = true;
        }

        return $this;
    } // setUf()

    /**
     * Set the value of [municipio] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Cidade The current object (for fluent API support)
     */
    public function setMunicipio($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->municipio !== $v) {
            $this->municipio = $v;
            $this->modifiedColumns[CidadeTableMap::COL_MUNICIPIO] = true;
        }

        return $this;
    } // setMunicipio()

    /**
     * Set the value of [codigo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Cidade The current object (for fluent API support)
     */
    public function setCodigo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo !== $v) {
            $this->codigo = $v;
            $this->modifiedColumns[CidadeTableMap::COL_CODIGO] = true;
        }

        return $this;
    } // setCodigo()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CidadeTableMap::translateFieldName('Idcidade', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idcidade = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CidadeTableMap::translateFieldName('Uf', TableMap::TYPE_PHPNAME, $indexType)];
            $this->uf = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CidadeTableMap::translateFieldName('Municipio', TableMap::TYPE_PHPNAME, $indexType)];
            $this->municipio = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CidadeTableMap::translateFieldName('Codigo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codigo = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 4; // 4 = CidadeTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Cidade'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CidadeTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCidadeQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collEnderecoAgencias = null;

            $this->collEnderecoClientes = null;

            $this->collEnderecoServClientes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Cidade::setDeleted()
     * @see Cidade::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CidadeTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCidadeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CidadeTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CidadeTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->enderecoAgenciasScheduledForDeletion !== null) {
                if (!$this->enderecoAgenciasScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EnderecoAgenciaQuery::create()
                        ->filterByPrimaryKeys($this->enderecoAgenciasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->enderecoAgenciasScheduledForDeletion = null;
                }
            }

            if ($this->collEnderecoAgencias !== null) {
                foreach ($this->collEnderecoAgencias as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->enderecoClientesScheduledForDeletion !== null) {
                if (!$this->enderecoClientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EnderecoClienteQuery::create()
                        ->filterByPrimaryKeys($this->enderecoClientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->enderecoClientesScheduledForDeletion = null;
                }
            }

            if ($this->collEnderecoClientes !== null) {
                foreach ($this->collEnderecoClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->enderecoServClientesScheduledForDeletion !== null) {
                if (!$this->enderecoServClientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\EnderecoServClienteQuery::create()
                        ->filterByPrimaryKeys($this->enderecoServClientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->enderecoServClientesScheduledForDeletion = null;
                }
            }

            if ($this->collEnderecoServClientes !== null) {
                foreach ($this->collEnderecoServClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CidadeTableMap::COL_IDCIDADE] = true;
        if (null !== $this->idcidade) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CidadeTableMap::COL_IDCIDADE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CidadeTableMap::COL_IDCIDADE)) {
            $modifiedColumns[':p' . $index++]  = 'idcidade';
        }
        if ($this->isColumnModified(CidadeTableMap::COL_UF)) {
            $modifiedColumns[':p' . $index++]  = 'uf';
        }
        if ($this->isColumnModified(CidadeTableMap::COL_MUNICIPIO)) {
            $modifiedColumns[':p' . $index++]  = 'municipio';
        }
        if ($this->isColumnModified(CidadeTableMap::COL_CODIGO)) {
            $modifiedColumns[':p' . $index++]  = 'codigo';
        }

        $sql = sprintf(
            'INSERT INTO cidade (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idcidade':
                        $stmt->bindValue($identifier, $this->idcidade, PDO::PARAM_INT);
                        break;
                    case 'uf':
                        $stmt->bindValue($identifier, $this->uf, PDO::PARAM_STR);
                        break;
                    case 'municipio':
                        $stmt->bindValue($identifier, $this->municipio, PDO::PARAM_STR);
                        break;
                    case 'codigo':
                        $stmt->bindValue($identifier, $this->codigo, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdcidade($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CidadeTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdcidade();
                break;
            case 1:
                return $this->getUf();
                break;
            case 2:
                return $this->getMunicipio();
                break;
            case 3:
                return $this->getCodigo();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Cidade'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Cidade'][$this->hashCode()] = true;
        $keys = CidadeTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdcidade(),
            $keys[1] => $this->getUf(),
            $keys[2] => $this->getMunicipio(),
            $keys[3] => $this->getCodigo(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collEnderecoAgencias) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'enderecoAgencias';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'endereco_agencias';
                        break;
                    default:
                        $key = 'EnderecoAgencias';
                }

                $result[$key] = $this->collEnderecoAgencias->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEnderecoClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'enderecoClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'endereco_clientes';
                        break;
                    default:
                        $key = 'EnderecoClientes';
                }

                $result[$key] = $this->collEnderecoClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEnderecoServClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'enderecoServClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'endereco_serv_clientes';
                        break;
                    default:
                        $key = 'EnderecoServClientes';
                }

                $result[$key] = $this->collEnderecoServClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Cidade
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CidadeTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Cidade
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdcidade($value);
                break;
            case 1:
                $this->setUf($value);
                break;
            case 2:
                $this->setMunicipio($value);
                break;
            case 3:
                $this->setCodigo($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CidadeTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdcidade($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUf($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setMunicipio($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCodigo($arr[$keys[3]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Cidade The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CidadeTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CidadeTableMap::COL_IDCIDADE)) {
            $criteria->add(CidadeTableMap::COL_IDCIDADE, $this->idcidade);
        }
        if ($this->isColumnModified(CidadeTableMap::COL_UF)) {
            $criteria->add(CidadeTableMap::COL_UF, $this->uf);
        }
        if ($this->isColumnModified(CidadeTableMap::COL_MUNICIPIO)) {
            $criteria->add(CidadeTableMap::COL_MUNICIPIO, $this->municipio);
        }
        if ($this->isColumnModified(CidadeTableMap::COL_CODIGO)) {
            $criteria->add(CidadeTableMap::COL_CODIGO, $this->codigo);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCidadeQuery::create();
        $criteria->add(CidadeTableMap::COL_IDCIDADE, $this->idcidade);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdcidade();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdcidade();
    }

    /**
     * Generic method to set the primary key (idcidade column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdcidade($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdcidade();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Cidade (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUf($this->getUf());
        $copyObj->setMunicipio($this->getMunicipio());
        $copyObj->setCodigo($this->getCodigo());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getEnderecoAgencias() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEnderecoAgencia($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEnderecoClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEnderecoCliente($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEnderecoServClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEnderecoServCliente($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdcidade(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Cidade Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('EnderecoAgencia' == $relationName) {
            return $this->initEnderecoAgencias();
        }
        if ('EnderecoCliente' == $relationName) {
            return $this->initEnderecoClientes();
        }
        if ('EnderecoServCliente' == $relationName) {
            return $this->initEnderecoServClientes();
        }
    }

    /**
     * Clears out the collEnderecoAgencias collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEnderecoAgencias()
     */
    public function clearEnderecoAgencias()
    {
        $this->collEnderecoAgencias = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEnderecoAgencias collection loaded partially.
     */
    public function resetPartialEnderecoAgencias($v = true)
    {
        $this->collEnderecoAgenciasPartial = $v;
    }

    /**
     * Initializes the collEnderecoAgencias collection.
     *
     * By default this just sets the collEnderecoAgencias collection to an empty array (like clearcollEnderecoAgencias());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEnderecoAgencias($overrideExisting = true)
    {
        if (null !== $this->collEnderecoAgencias && !$overrideExisting) {
            return;
        }

        $collectionClassName = EnderecoAgenciaTableMap::getTableMap()->getCollectionClassName();

        $this->collEnderecoAgencias = new $collectionClassName;
        $this->collEnderecoAgencias->setModel('\ImaTelecomBundle\Model\EnderecoAgencia');
    }

    /**
     * Gets an array of ChildEnderecoAgencia objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCidade is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEnderecoAgencia[] List of ChildEnderecoAgencia objects
     * @throws PropelException
     */
    public function getEnderecoAgencias(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoAgenciasPartial && !$this->isNew();
        if (null === $this->collEnderecoAgencias || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEnderecoAgencias) {
                // return empty collection
                $this->initEnderecoAgencias();
            } else {
                $collEnderecoAgencias = ChildEnderecoAgenciaQuery::create(null, $criteria)
                    ->filterByCidade($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEnderecoAgenciasPartial && count($collEnderecoAgencias)) {
                        $this->initEnderecoAgencias(false);

                        foreach ($collEnderecoAgencias as $obj) {
                            if (false == $this->collEnderecoAgencias->contains($obj)) {
                                $this->collEnderecoAgencias->append($obj);
                            }
                        }

                        $this->collEnderecoAgenciasPartial = true;
                    }

                    return $collEnderecoAgencias;
                }

                if ($partial && $this->collEnderecoAgencias) {
                    foreach ($this->collEnderecoAgencias as $obj) {
                        if ($obj->isNew()) {
                            $collEnderecoAgencias[] = $obj;
                        }
                    }
                }

                $this->collEnderecoAgencias = $collEnderecoAgencias;
                $this->collEnderecoAgenciasPartial = false;
            }
        }

        return $this->collEnderecoAgencias;
    }

    /**
     * Sets a collection of ChildEnderecoAgencia objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $enderecoAgencias A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCidade The current object (for fluent API support)
     */
    public function setEnderecoAgencias(Collection $enderecoAgencias, ConnectionInterface $con = null)
    {
        /** @var ChildEnderecoAgencia[] $enderecoAgenciasToDelete */
        $enderecoAgenciasToDelete = $this->getEnderecoAgencias(new Criteria(), $con)->diff($enderecoAgencias);


        $this->enderecoAgenciasScheduledForDeletion = $enderecoAgenciasToDelete;

        foreach ($enderecoAgenciasToDelete as $enderecoAgenciaRemoved) {
            $enderecoAgenciaRemoved->setCidade(null);
        }

        $this->collEnderecoAgencias = null;
        foreach ($enderecoAgencias as $enderecoAgencia) {
            $this->addEnderecoAgencia($enderecoAgencia);
        }

        $this->collEnderecoAgencias = $enderecoAgencias;
        $this->collEnderecoAgenciasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EnderecoAgencia objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EnderecoAgencia objects.
     * @throws PropelException
     */
    public function countEnderecoAgencias(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoAgenciasPartial && !$this->isNew();
        if (null === $this->collEnderecoAgencias || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEnderecoAgencias) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEnderecoAgencias());
            }

            $query = ChildEnderecoAgenciaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCidade($this)
                ->count($con);
        }

        return count($this->collEnderecoAgencias);
    }

    /**
     * Method called to associate a ChildEnderecoAgencia object to this object
     * through the ChildEnderecoAgencia foreign key attribute.
     *
     * @param  ChildEnderecoAgencia $l ChildEnderecoAgencia
     * @return $this|\ImaTelecomBundle\Model\Cidade The current object (for fluent API support)
     */
    public function addEnderecoAgencia(ChildEnderecoAgencia $l)
    {
        if ($this->collEnderecoAgencias === null) {
            $this->initEnderecoAgencias();
            $this->collEnderecoAgenciasPartial = true;
        }

        if (!$this->collEnderecoAgencias->contains($l)) {
            $this->doAddEnderecoAgencia($l);

            if ($this->enderecoAgenciasScheduledForDeletion and $this->enderecoAgenciasScheduledForDeletion->contains($l)) {
                $this->enderecoAgenciasScheduledForDeletion->remove($this->enderecoAgenciasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEnderecoAgencia $enderecoAgencia The ChildEnderecoAgencia object to add.
     */
    protected function doAddEnderecoAgencia(ChildEnderecoAgencia $enderecoAgencia)
    {
        $this->collEnderecoAgencias[]= $enderecoAgencia;
        $enderecoAgencia->setCidade($this);
    }

    /**
     * @param  ChildEnderecoAgencia $enderecoAgencia The ChildEnderecoAgencia object to remove.
     * @return $this|ChildCidade The current object (for fluent API support)
     */
    public function removeEnderecoAgencia(ChildEnderecoAgencia $enderecoAgencia)
    {
        if ($this->getEnderecoAgencias()->contains($enderecoAgencia)) {
            $pos = $this->collEnderecoAgencias->search($enderecoAgencia);
            $this->collEnderecoAgencias->remove($pos);
            if (null === $this->enderecoAgenciasScheduledForDeletion) {
                $this->enderecoAgenciasScheduledForDeletion = clone $this->collEnderecoAgencias;
                $this->enderecoAgenciasScheduledForDeletion->clear();
            }
            $this->enderecoAgenciasScheduledForDeletion[]= clone $enderecoAgencia;
            $enderecoAgencia->setCidade(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cidade is new, it will return
     * an empty collection; or if this Cidade has previously
     * been saved, it will retrieve related EnderecoAgencias from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cidade.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoAgencia[] List of ChildEnderecoAgencia objects
     */
    public function getEnderecoAgenciasJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoAgenciaQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getEnderecoAgencias($query, $con);
    }

    /**
     * Clears out the collEnderecoClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEnderecoClientes()
     */
    public function clearEnderecoClientes()
    {
        $this->collEnderecoClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEnderecoClientes collection loaded partially.
     */
    public function resetPartialEnderecoClientes($v = true)
    {
        $this->collEnderecoClientesPartial = $v;
    }

    /**
     * Initializes the collEnderecoClientes collection.
     *
     * By default this just sets the collEnderecoClientes collection to an empty array (like clearcollEnderecoClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEnderecoClientes($overrideExisting = true)
    {
        if (null !== $this->collEnderecoClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = EnderecoClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collEnderecoClientes = new $collectionClassName;
        $this->collEnderecoClientes->setModel('\ImaTelecomBundle\Model\EnderecoCliente');
    }

    /**
     * Gets an array of ChildEnderecoCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCidade is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEnderecoCliente[] List of ChildEnderecoCliente objects
     * @throws PropelException
     */
    public function getEnderecoClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEnderecoClientes) {
                // return empty collection
                $this->initEnderecoClientes();
            } else {
                $collEnderecoClientes = ChildEnderecoClienteQuery::create(null, $criteria)
                    ->filterByCidade($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEnderecoClientesPartial && count($collEnderecoClientes)) {
                        $this->initEnderecoClientes(false);

                        foreach ($collEnderecoClientes as $obj) {
                            if (false == $this->collEnderecoClientes->contains($obj)) {
                                $this->collEnderecoClientes->append($obj);
                            }
                        }

                        $this->collEnderecoClientesPartial = true;
                    }

                    return $collEnderecoClientes;
                }

                if ($partial && $this->collEnderecoClientes) {
                    foreach ($this->collEnderecoClientes as $obj) {
                        if ($obj->isNew()) {
                            $collEnderecoClientes[] = $obj;
                        }
                    }
                }

                $this->collEnderecoClientes = $collEnderecoClientes;
                $this->collEnderecoClientesPartial = false;
            }
        }

        return $this->collEnderecoClientes;
    }

    /**
     * Sets a collection of ChildEnderecoCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $enderecoClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCidade The current object (for fluent API support)
     */
    public function setEnderecoClientes(Collection $enderecoClientes, ConnectionInterface $con = null)
    {
        /** @var ChildEnderecoCliente[] $enderecoClientesToDelete */
        $enderecoClientesToDelete = $this->getEnderecoClientes(new Criteria(), $con)->diff($enderecoClientes);


        $this->enderecoClientesScheduledForDeletion = $enderecoClientesToDelete;

        foreach ($enderecoClientesToDelete as $enderecoClienteRemoved) {
            $enderecoClienteRemoved->setCidade(null);
        }

        $this->collEnderecoClientes = null;
        foreach ($enderecoClientes as $enderecoCliente) {
            $this->addEnderecoCliente($enderecoCliente);
        }

        $this->collEnderecoClientes = $enderecoClientes;
        $this->collEnderecoClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EnderecoCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EnderecoCliente objects.
     * @throws PropelException
     */
    public function countEnderecoClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEnderecoClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEnderecoClientes());
            }

            $query = ChildEnderecoClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCidade($this)
                ->count($con);
        }

        return count($this->collEnderecoClientes);
    }

    /**
     * Method called to associate a ChildEnderecoCliente object to this object
     * through the ChildEnderecoCliente foreign key attribute.
     *
     * @param  ChildEnderecoCliente $l ChildEnderecoCliente
     * @return $this|\ImaTelecomBundle\Model\Cidade The current object (for fluent API support)
     */
    public function addEnderecoCliente(ChildEnderecoCliente $l)
    {
        if ($this->collEnderecoClientes === null) {
            $this->initEnderecoClientes();
            $this->collEnderecoClientesPartial = true;
        }

        if (!$this->collEnderecoClientes->contains($l)) {
            $this->doAddEnderecoCliente($l);

            if ($this->enderecoClientesScheduledForDeletion and $this->enderecoClientesScheduledForDeletion->contains($l)) {
                $this->enderecoClientesScheduledForDeletion->remove($this->enderecoClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEnderecoCliente $enderecoCliente The ChildEnderecoCliente object to add.
     */
    protected function doAddEnderecoCliente(ChildEnderecoCliente $enderecoCliente)
    {
        $this->collEnderecoClientes[]= $enderecoCliente;
        $enderecoCliente->setCidade($this);
    }

    /**
     * @param  ChildEnderecoCliente $enderecoCliente The ChildEnderecoCliente object to remove.
     * @return $this|ChildCidade The current object (for fluent API support)
     */
    public function removeEnderecoCliente(ChildEnderecoCliente $enderecoCliente)
    {
        if ($this->getEnderecoClientes()->contains($enderecoCliente)) {
            $pos = $this->collEnderecoClientes->search($enderecoCliente);
            $this->collEnderecoClientes->remove($pos);
            if (null === $this->enderecoClientesScheduledForDeletion) {
                $this->enderecoClientesScheduledForDeletion = clone $this->collEnderecoClientes;
                $this->enderecoClientesScheduledForDeletion->clear();
            }
            $this->enderecoClientesScheduledForDeletion[]= clone $enderecoCliente;
            $enderecoCliente->setCidade(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cidade is new, it will return
     * an empty collection; or if this Cidade has previously
     * been saved, it will retrieve related EnderecoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cidade.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoCliente[] List of ChildEnderecoCliente objects
     */
    public function getEnderecoClientesJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoClienteQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getEnderecoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cidade is new, it will return
     * an empty collection; or if this Cidade has previously
     * been saved, it will retrieve related EnderecoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cidade.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoCliente[] List of ChildEnderecoCliente objects
     */
    public function getEnderecoClientesJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoClienteQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getEnderecoClientes($query, $con);
    }

    /**
     * Clears out the collEnderecoServClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEnderecoServClientes()
     */
    public function clearEnderecoServClientes()
    {
        $this->collEnderecoServClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEnderecoServClientes collection loaded partially.
     */
    public function resetPartialEnderecoServClientes($v = true)
    {
        $this->collEnderecoServClientesPartial = $v;
    }

    /**
     * Initializes the collEnderecoServClientes collection.
     *
     * By default this just sets the collEnderecoServClientes collection to an empty array (like clearcollEnderecoServClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEnderecoServClientes($overrideExisting = true)
    {
        if (null !== $this->collEnderecoServClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = EnderecoServClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collEnderecoServClientes = new $collectionClassName;
        $this->collEnderecoServClientes->setModel('\ImaTelecomBundle\Model\EnderecoServCliente');
    }

    /**
     * Gets an array of ChildEnderecoServCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCidade is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEnderecoServCliente[] List of ChildEnderecoServCliente objects
     * @throws PropelException
     */
    public function getEnderecoServClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoServClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoServClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collEnderecoServClientes) {
                // return empty collection
                $this->initEnderecoServClientes();
            } else {
                $collEnderecoServClientes = ChildEnderecoServClienteQuery::create(null, $criteria)
                    ->filterByCidade($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEnderecoServClientesPartial && count($collEnderecoServClientes)) {
                        $this->initEnderecoServClientes(false);

                        foreach ($collEnderecoServClientes as $obj) {
                            if (false == $this->collEnderecoServClientes->contains($obj)) {
                                $this->collEnderecoServClientes->append($obj);
                            }
                        }

                        $this->collEnderecoServClientesPartial = true;
                    }

                    return $collEnderecoServClientes;
                }

                if ($partial && $this->collEnderecoServClientes) {
                    foreach ($this->collEnderecoServClientes as $obj) {
                        if ($obj->isNew()) {
                            $collEnderecoServClientes[] = $obj;
                        }
                    }
                }

                $this->collEnderecoServClientes = $collEnderecoServClientes;
                $this->collEnderecoServClientesPartial = false;
            }
        }

        return $this->collEnderecoServClientes;
    }

    /**
     * Sets a collection of ChildEnderecoServCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $enderecoServClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCidade The current object (for fluent API support)
     */
    public function setEnderecoServClientes(Collection $enderecoServClientes, ConnectionInterface $con = null)
    {
        /** @var ChildEnderecoServCliente[] $enderecoServClientesToDelete */
        $enderecoServClientesToDelete = $this->getEnderecoServClientes(new Criteria(), $con)->diff($enderecoServClientes);


        $this->enderecoServClientesScheduledForDeletion = $enderecoServClientesToDelete;

        foreach ($enderecoServClientesToDelete as $enderecoServClienteRemoved) {
            $enderecoServClienteRemoved->setCidade(null);
        }

        $this->collEnderecoServClientes = null;
        foreach ($enderecoServClientes as $enderecoServCliente) {
            $this->addEnderecoServCliente($enderecoServCliente);
        }

        $this->collEnderecoServClientes = $enderecoServClientes;
        $this->collEnderecoServClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EnderecoServCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EnderecoServCliente objects.
     * @throws PropelException
     */
    public function countEnderecoServClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEnderecoServClientesPartial && !$this->isNew();
        if (null === $this->collEnderecoServClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEnderecoServClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEnderecoServClientes());
            }

            $query = ChildEnderecoServClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCidade($this)
                ->count($con);
        }

        return count($this->collEnderecoServClientes);
    }

    /**
     * Method called to associate a ChildEnderecoServCliente object to this object
     * through the ChildEnderecoServCliente foreign key attribute.
     *
     * @param  ChildEnderecoServCliente $l ChildEnderecoServCliente
     * @return $this|\ImaTelecomBundle\Model\Cidade The current object (for fluent API support)
     */
    public function addEnderecoServCliente(ChildEnderecoServCliente $l)
    {
        if ($this->collEnderecoServClientes === null) {
            $this->initEnderecoServClientes();
            $this->collEnderecoServClientesPartial = true;
        }

        if (!$this->collEnderecoServClientes->contains($l)) {
            $this->doAddEnderecoServCliente($l);

            if ($this->enderecoServClientesScheduledForDeletion and $this->enderecoServClientesScheduledForDeletion->contains($l)) {
                $this->enderecoServClientesScheduledForDeletion->remove($this->enderecoServClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEnderecoServCliente $enderecoServCliente The ChildEnderecoServCliente object to add.
     */
    protected function doAddEnderecoServCliente(ChildEnderecoServCliente $enderecoServCliente)
    {
        $this->collEnderecoServClientes[]= $enderecoServCliente;
        $enderecoServCliente->setCidade($this);
    }

    /**
     * @param  ChildEnderecoServCliente $enderecoServCliente The ChildEnderecoServCliente object to remove.
     * @return $this|ChildCidade The current object (for fluent API support)
     */
    public function removeEnderecoServCliente(ChildEnderecoServCliente $enderecoServCliente)
    {
        if ($this->getEnderecoServClientes()->contains($enderecoServCliente)) {
            $pos = $this->collEnderecoServClientes->search($enderecoServCliente);
            $this->collEnderecoServClientes->remove($pos);
            if (null === $this->enderecoServClientesScheduledForDeletion) {
                $this->enderecoServClientesScheduledForDeletion = clone $this->collEnderecoServClientes;
                $this->enderecoServClientesScheduledForDeletion->clear();
            }
            $this->enderecoServClientesScheduledForDeletion[]= clone $enderecoServCliente;
            $enderecoServCliente->setCidade(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cidade is new, it will return
     * an empty collection; or if this Cidade has previously
     * been saved, it will retrieve related EnderecoServClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cidade.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoServCliente[] List of ChildEnderecoServCliente objects
     */
    public function getEnderecoServClientesJoinServicoCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoServClienteQuery::create(null, $criteria);
        $query->joinWith('ServicoCliente', $joinBehavior);

        return $this->getEnderecoServClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Cidade is new, it will return
     * an empty collection; or if this Cidade has previously
     * been saved, it will retrieve related EnderecoServClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Cidade.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEnderecoServCliente[] List of ChildEnderecoServCliente objects
     */
    public function getEnderecoServClientesJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEnderecoServClienteQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getEnderecoServClientes($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->idcidade = null;
        $this->uf = null;
        $this->municipio = null;
        $this->codigo = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collEnderecoAgencias) {
                foreach ($this->collEnderecoAgencias as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEnderecoClientes) {
                foreach ($this->collEnderecoClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEnderecoServClientes) {
                foreach ($this->collEnderecoServClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collEnderecoAgencias = null;
        $this->collEnderecoClientes = null;
        $this->collEnderecoServClientes = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CidadeTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
