<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ItemDeCompra as ChildItemDeCompra;
use ImaTelecomBundle\Model\ItemDeCompraQuery as ChildItemDeCompraQuery;
use ImaTelecomBundle\Model\Map\ItemDeCompraTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'item_de_compra' table.
 *
 *
 *
 * @method     ChildItemDeCompraQuery orderByIditemDeCompra($order = Criteria::ASC) Order by the iditem_de_compra column
 * @method     ChildItemDeCompraQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildItemDeCompraQuery orderByUnidade($order = Criteria::ASC) Order by the unidade column
 * @method     ChildItemDeCompraQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildItemDeCompraQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method     ChildItemDeCompraQuery orderByImobilizado($order = Criteria::ASC) Order by the imobilizado column
 * @method     ChildItemDeCompraQuery orderByClassificacaoFinanceiraId($order = Criteria::ASC) Order by the classificacao_financeira_id column
 * @method     ChildItemDeCompraQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildItemDeCompraQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildItemDeCompraQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildItemDeCompraQuery groupByIditemDeCompra() Group by the iditem_de_compra column
 * @method     ChildItemDeCompraQuery groupByNome() Group by the nome column
 * @method     ChildItemDeCompraQuery groupByUnidade() Group by the unidade column
 * @method     ChildItemDeCompraQuery groupByAtivo() Group by the ativo column
 * @method     ChildItemDeCompraQuery groupByTipo() Group by the tipo column
 * @method     ChildItemDeCompraQuery groupByImobilizado() Group by the imobilizado column
 * @method     ChildItemDeCompraQuery groupByClassificacaoFinanceiraId() Group by the classificacao_financeira_id column
 * @method     ChildItemDeCompraQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildItemDeCompraQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildItemDeCompraQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildItemDeCompraQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildItemDeCompraQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildItemDeCompraQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildItemDeCompraQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildItemDeCompraQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildItemDeCompraQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildItemDeCompraQuery leftJoinClassificacaoFinanceira($relationAlias = null) Adds a LEFT JOIN clause to the query using the ClassificacaoFinanceira relation
 * @method     ChildItemDeCompraQuery rightJoinClassificacaoFinanceira($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ClassificacaoFinanceira relation
 * @method     ChildItemDeCompraQuery innerJoinClassificacaoFinanceira($relationAlias = null) Adds a INNER JOIN clause to the query using the ClassificacaoFinanceira relation
 *
 * @method     ChildItemDeCompraQuery joinWithClassificacaoFinanceira($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ClassificacaoFinanceira relation
 *
 * @method     ChildItemDeCompraQuery leftJoinWithClassificacaoFinanceira() Adds a LEFT JOIN clause and with to the query using the ClassificacaoFinanceira relation
 * @method     ChildItemDeCompraQuery rightJoinWithClassificacaoFinanceira() Adds a RIGHT JOIN clause and with to the query using the ClassificacaoFinanceira relation
 * @method     ChildItemDeCompraQuery innerJoinWithClassificacaoFinanceira() Adds a INNER JOIN clause and with to the query using the ClassificacaoFinanceira relation
 *
 * @method     ChildItemDeCompraQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildItemDeCompraQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildItemDeCompraQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildItemDeCompraQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildItemDeCompraQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildItemDeCompraQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildItemDeCompraQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildItemDeCompraQuery leftJoinEstoque($relationAlias = null) Adds a LEFT JOIN clause to the query using the Estoque relation
 * @method     ChildItemDeCompraQuery rightJoinEstoque($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Estoque relation
 * @method     ChildItemDeCompraQuery innerJoinEstoque($relationAlias = null) Adds a INNER JOIN clause to the query using the Estoque relation
 *
 * @method     ChildItemDeCompraQuery joinWithEstoque($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Estoque relation
 *
 * @method     ChildItemDeCompraQuery leftJoinWithEstoque() Adds a LEFT JOIN clause and with to the query using the Estoque relation
 * @method     ChildItemDeCompraQuery rightJoinWithEstoque() Adds a RIGHT JOIN clause and with to the query using the Estoque relation
 * @method     ChildItemDeCompraQuery innerJoinWithEstoque() Adds a INNER JOIN clause and with to the query using the Estoque relation
 *
 * @method     ChildItemDeCompraQuery leftJoinItemComprado($relationAlias = null) Adds a LEFT JOIN clause to the query using the ItemComprado relation
 * @method     ChildItemDeCompraQuery rightJoinItemComprado($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ItemComprado relation
 * @method     ChildItemDeCompraQuery innerJoinItemComprado($relationAlias = null) Adds a INNER JOIN clause to the query using the ItemComprado relation
 *
 * @method     ChildItemDeCompraQuery joinWithItemComprado($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ItemComprado relation
 *
 * @method     ChildItemDeCompraQuery leftJoinWithItemComprado() Adds a LEFT JOIN clause and with to the query using the ItemComprado relation
 * @method     ChildItemDeCompraQuery rightJoinWithItemComprado() Adds a RIGHT JOIN clause and with to the query using the ItemComprado relation
 * @method     ChildItemDeCompraQuery innerJoinWithItemComprado() Adds a INNER JOIN clause and with to the query using the ItemComprado relation
 *
 * @method     \ImaTelecomBundle\Model\ClassificacaoFinanceiraQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\EstoqueQuery|\ImaTelecomBundle\Model\ItemCompradoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildItemDeCompra findOne(ConnectionInterface $con = null) Return the first ChildItemDeCompra matching the query
 * @method     ChildItemDeCompra findOneOrCreate(ConnectionInterface $con = null) Return the first ChildItemDeCompra matching the query, or a new ChildItemDeCompra object populated from the query conditions when no match is found
 *
 * @method     ChildItemDeCompra findOneByIditemDeCompra(int $iditem_de_compra) Return the first ChildItemDeCompra filtered by the iditem_de_compra column
 * @method     ChildItemDeCompra findOneByNome(string $nome) Return the first ChildItemDeCompra filtered by the nome column
 * @method     ChildItemDeCompra findOneByUnidade(string $unidade) Return the first ChildItemDeCompra filtered by the unidade column
 * @method     ChildItemDeCompra findOneByAtivo(boolean $ativo) Return the first ChildItemDeCompra filtered by the ativo column
 * @method     ChildItemDeCompra findOneByTipo(string $tipo) Return the first ChildItemDeCompra filtered by the tipo column
 * @method     ChildItemDeCompra findOneByImobilizado(boolean $imobilizado) Return the first ChildItemDeCompra filtered by the imobilizado column
 * @method     ChildItemDeCompra findOneByClassificacaoFinanceiraId(int $classificacao_financeira_id) Return the first ChildItemDeCompra filtered by the classificacao_financeira_id column
 * @method     ChildItemDeCompra findOneByDataCadastro(string $data_cadastro) Return the first ChildItemDeCompra filtered by the data_cadastro column
 * @method     ChildItemDeCompra findOneByDataAlterado(string $data_alterado) Return the first ChildItemDeCompra filtered by the data_alterado column
 * @method     ChildItemDeCompra findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildItemDeCompra filtered by the usuario_alterado column *

 * @method     ChildItemDeCompra requirePk($key, ConnectionInterface $con = null) Return the ChildItemDeCompra by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOne(ConnectionInterface $con = null) Return the first ChildItemDeCompra matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildItemDeCompra requireOneByIditemDeCompra(int $iditem_de_compra) Return the first ChildItemDeCompra filtered by the iditem_de_compra column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOneByNome(string $nome) Return the first ChildItemDeCompra filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOneByUnidade(string $unidade) Return the first ChildItemDeCompra filtered by the unidade column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOneByAtivo(boolean $ativo) Return the first ChildItemDeCompra filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOneByTipo(string $tipo) Return the first ChildItemDeCompra filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOneByImobilizado(boolean $imobilizado) Return the first ChildItemDeCompra filtered by the imobilizado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOneByClassificacaoFinanceiraId(int $classificacao_financeira_id) Return the first ChildItemDeCompra filtered by the classificacao_financeira_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOneByDataCadastro(string $data_cadastro) Return the first ChildItemDeCompra filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOneByDataAlterado(string $data_alterado) Return the first ChildItemDeCompra filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemDeCompra requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildItemDeCompra filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildItemDeCompra[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildItemDeCompra objects based on current ModelCriteria
 * @method     ChildItemDeCompra[]|ObjectCollection findByIditemDeCompra(int $iditem_de_compra) Return ChildItemDeCompra objects filtered by the iditem_de_compra column
 * @method     ChildItemDeCompra[]|ObjectCollection findByNome(string $nome) Return ChildItemDeCompra objects filtered by the nome column
 * @method     ChildItemDeCompra[]|ObjectCollection findByUnidade(string $unidade) Return ChildItemDeCompra objects filtered by the unidade column
 * @method     ChildItemDeCompra[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildItemDeCompra objects filtered by the ativo column
 * @method     ChildItemDeCompra[]|ObjectCollection findByTipo(string $tipo) Return ChildItemDeCompra objects filtered by the tipo column
 * @method     ChildItemDeCompra[]|ObjectCollection findByImobilizado(boolean $imobilizado) Return ChildItemDeCompra objects filtered by the imobilizado column
 * @method     ChildItemDeCompra[]|ObjectCollection findByClassificacaoFinanceiraId(int $classificacao_financeira_id) Return ChildItemDeCompra objects filtered by the classificacao_financeira_id column
 * @method     ChildItemDeCompra[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildItemDeCompra objects filtered by the data_cadastro column
 * @method     ChildItemDeCompra[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildItemDeCompra objects filtered by the data_alterado column
 * @method     ChildItemDeCompra[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildItemDeCompra objects filtered by the usuario_alterado column
 * @method     ChildItemDeCompra[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ItemDeCompraQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ItemDeCompraQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ItemDeCompra', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildItemDeCompraQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildItemDeCompraQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildItemDeCompraQuery) {
            return $criteria;
        }
        $query = new ChildItemDeCompraQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildItemDeCompra|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ItemDeCompraTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ItemDeCompraTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildItemDeCompra A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT iditem_de_compra, nome, unidade, ativo, tipo, imobilizado, classificacao_financeira_id, data_cadastro, data_alterado, usuario_alterado FROM item_de_compra WHERE iditem_de_compra = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildItemDeCompra $obj */
            $obj = new ChildItemDeCompra();
            $obj->hydrate($row);
            ItemDeCompraTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildItemDeCompra|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_IDITEM_DE_COMPRA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_IDITEM_DE_COMPRA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the iditem_de_compra column
     *
     * Example usage:
     * <code>
     * $query->filterByIditemDeCompra(1234); // WHERE iditem_de_compra = 1234
     * $query->filterByIditemDeCompra(array(12, 34)); // WHERE iditem_de_compra IN (12, 34)
     * $query->filterByIditemDeCompra(array('min' => 12)); // WHERE iditem_de_compra > 12
     * </code>
     *
     * @param     mixed $iditemDeCompra The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByIditemDeCompra($iditemDeCompra = null, $comparison = null)
    {
        if (is_array($iditemDeCompra)) {
            $useMinMax = false;
            if (isset($iditemDeCompra['min'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_IDITEM_DE_COMPRA, $iditemDeCompra['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iditemDeCompra['max'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_IDITEM_DE_COMPRA, $iditemDeCompra['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_IDITEM_DE_COMPRA, $iditemDeCompra, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the unidade column
     *
     * Example usage:
     * <code>
     * $query->filterByUnidade('fooValue');   // WHERE unidade = 'fooValue'
     * $query->filterByUnidade('%fooValue%', Criteria::LIKE); // WHERE unidade LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unidade The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByUnidade($unidade = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unidade)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_UNIDADE, $unidade, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the imobilizado column
     *
     * Example usage:
     * <code>
     * $query->filterByImobilizado(true); // WHERE imobilizado = true
     * $query->filterByImobilizado('yes'); // WHERE imobilizado = true
     * </code>
     *
     * @param     boolean|string $imobilizado The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByImobilizado($imobilizado = null, $comparison = null)
    {
        if (is_string($imobilizado)) {
            $imobilizado = in_array(strtolower($imobilizado), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_IMOBILIZADO, $imobilizado, $comparison);
    }

    /**
     * Filter the query on the classificacao_financeira_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClassificacaoFinanceiraId(1234); // WHERE classificacao_financeira_id = 1234
     * $query->filterByClassificacaoFinanceiraId(array(12, 34)); // WHERE classificacao_financeira_id IN (12, 34)
     * $query->filterByClassificacaoFinanceiraId(array('min' => 12)); // WHERE classificacao_financeira_id > 12
     * </code>
     *
     * @see       filterByClassificacaoFinanceira()
     *
     * @param     mixed $classificacaoFinanceiraId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByClassificacaoFinanceiraId($classificacaoFinanceiraId = null, $comparison = null)
    {
        if (is_array($classificacaoFinanceiraId)) {
            $useMinMax = false;
            if (isset($classificacaoFinanceiraId['min'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_CLASSIFICACAO_FINANCEIRA_ID, $classificacaoFinanceiraId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($classificacaoFinanceiraId['max'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_CLASSIFICACAO_FINANCEIRA_ID, $classificacaoFinanceiraId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_CLASSIFICACAO_FINANCEIRA_ID, $classificacaoFinanceiraId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ItemDeCompraTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemDeCompraTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ClassificacaoFinanceira object
     *
     * @param \ImaTelecomBundle\Model\ClassificacaoFinanceira|ObjectCollection $classificacaoFinanceira The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByClassificacaoFinanceira($classificacaoFinanceira, $comparison = null)
    {
        if ($classificacaoFinanceira instanceof \ImaTelecomBundle\Model\ClassificacaoFinanceira) {
            return $this
                ->addUsingAlias(ItemDeCompraTableMap::COL_CLASSIFICACAO_FINANCEIRA_ID, $classificacaoFinanceira->getIdclassificacaoFinanceira(), $comparison);
        } elseif ($classificacaoFinanceira instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ItemDeCompraTableMap::COL_CLASSIFICACAO_FINANCEIRA_ID, $classificacaoFinanceira->toKeyValue('PrimaryKey', 'IdclassificacaoFinanceira'), $comparison);
        } else {
            throw new PropelException('filterByClassificacaoFinanceira() only accepts arguments of type \ImaTelecomBundle\Model\ClassificacaoFinanceira or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ClassificacaoFinanceira relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function joinClassificacaoFinanceira($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ClassificacaoFinanceira');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ClassificacaoFinanceira');
        }

        return $this;
    }

    /**
     * Use the ClassificacaoFinanceira relation ClassificacaoFinanceira object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClassificacaoFinanceiraQuery A secondary query class using the current class as primary query
     */
    public function useClassificacaoFinanceiraQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinClassificacaoFinanceira($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ClassificacaoFinanceira', '\ImaTelecomBundle\Model\ClassificacaoFinanceiraQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ItemDeCompraTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ItemDeCompraTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Estoque object
     *
     * @param \ImaTelecomBundle\Model\Estoque|ObjectCollection $estoque the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByEstoque($estoque, $comparison = null)
    {
        if ($estoque instanceof \ImaTelecomBundle\Model\Estoque) {
            return $this
                ->addUsingAlias(ItemDeCompraTableMap::COL_IDITEM_DE_COMPRA, $estoque->getItemDeCompraId(), $comparison);
        } elseif ($estoque instanceof ObjectCollection) {
            return $this
                ->useEstoqueQuery()
                ->filterByPrimaryKeys($estoque->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEstoque() only accepts arguments of type \ImaTelecomBundle\Model\Estoque or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Estoque relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function joinEstoque($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Estoque');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Estoque');
        }

        return $this;
    }

    /**
     * Use the Estoque relation Estoque object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstoque($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Estoque', '\ImaTelecomBundle\Model\EstoqueQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ItemComprado object
     *
     * @param \ImaTelecomBundle\Model\ItemComprado|ObjectCollection $itemComprado the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function filterByItemComprado($itemComprado, $comparison = null)
    {
        if ($itemComprado instanceof \ImaTelecomBundle\Model\ItemComprado) {
            return $this
                ->addUsingAlias(ItemDeCompraTableMap::COL_IDITEM_DE_COMPRA, $itemComprado->getItemDeCompraId(), $comparison);
        } elseif ($itemComprado instanceof ObjectCollection) {
            return $this
                ->useItemCompradoQuery()
                ->filterByPrimaryKeys($itemComprado->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByItemComprado() only accepts arguments of type \ImaTelecomBundle\Model\ItemComprado or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ItemComprado relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function joinItemComprado($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ItemComprado');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ItemComprado');
        }

        return $this;
    }

    /**
     * Use the ItemComprado relation ItemComprado object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ItemCompradoQuery A secondary query class using the current class as primary query
     */
    public function useItemCompradoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinItemComprado($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ItemComprado', '\ImaTelecomBundle\Model\ItemCompradoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildItemDeCompra $itemDeCompra Object to remove from the list of results
     *
     * @return $this|ChildItemDeCompraQuery The current query, for fluid interface
     */
    public function prune($itemDeCompra = null)
    {
        if ($itemDeCompra) {
            $this->addUsingAlias(ItemDeCompraTableMap::COL_IDITEM_DE_COMPRA, $itemDeCompra->getIditemDeCompra(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the item_de_compra table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ItemDeCompraTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ItemDeCompraTableMap::clearInstancePool();
            ItemDeCompraTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ItemDeCompraTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ItemDeCompraTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ItemDeCompraTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ItemDeCompraTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ItemDeCompraQuery
