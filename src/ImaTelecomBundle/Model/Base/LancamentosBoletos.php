<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Competencia as ChildCompetencia;
use ImaTelecomBundle\Model\CompetenciaQuery as ChildCompetenciaQuery;
use ImaTelecomBundle\Model\Convenio as ChildConvenio;
use ImaTelecomBundle\Model\ConvenioQuery as ChildConvenioQuery;
use ImaTelecomBundle\Model\Lancamentos as ChildLancamentos;
use ImaTelecomBundle\Model\LancamentosBoletosQuery as ChildLancamentosBoletosQuery;
use ImaTelecomBundle\Model\LancamentosQuery as ChildLancamentosQuery;
use ImaTelecomBundle\Model\Map\LancamentosBoletosTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'lancamentos_boletos' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class LancamentosBoletos implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\LancamentosBoletosTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idlancamentos_boletos field.
     *
     * @var        int
     */
    protected $idlancamentos_boletos;

    /**
     * The value for the lancamento_id field.
     *
     * @var        int
     */
    protected $lancamento_id;

    /**
     * The value for the boleto_id field.
     *
     * @var        int
     */
    protected $boleto_id;

    /**
     * The value for the conteudo_linhas_lancamento_boleto field.
     *
     * @var        string
     */
    protected $conteudo_linhas_lancamento_boleto;

    /**
     * The value for the data_cadastrado field.
     *
     * @var        DateTime
     */
    protected $data_cadastrado;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the data_geracao field.
     *
     * @var        string
     */
    protected $data_geracao;

    /**
     * The value for the competencia_id field.
     *
     * @var        int
     */
    protected $competencia_id;

    /**
     * The value for the convenio_id field.
     *
     * @var        int
     */
    protected $convenio_id;

    /**
     * @var        ChildBoleto
     */
    protected $aBoleto;

    /**
     * @var        ChildCompetencia
     */
    protected $aCompetencia;

    /**
     * @var        ChildConvenio
     */
    protected $aConvenio;

    /**
     * @var        ChildLancamentos
     */
    protected $aLancamentos;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\LancamentosBoletos object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>LancamentosBoletos</code> instance.  If
     * <code>obj</code> is an instance of <code>LancamentosBoletos</code>, delegates to
     * <code>equals(LancamentosBoletos)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|LancamentosBoletos The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idlancamentos_boletos] column value.
     *
     * @return int
     */
    public function getIdlancamentosBoletos()
    {
        return $this->idlancamentos_boletos;
    }

    /**
     * Get the [lancamento_id] column value.
     *
     * @return int
     */
    public function getLancamentoId()
    {
        return $this->lancamento_id;
    }

    /**
     * Get the [boleto_id] column value.
     *
     * @return int
     */
    public function getBoletoId()
    {
        return $this->boleto_id;
    }

    /**
     * Get the [conteudo_linhas_lancamento_boleto] column value.
     *
     * @return string
     */
    public function getConteudoLinhasLancamentoBoleto()
    {
        return $this->conteudo_linhas_lancamento_boleto;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastrado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastrado($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastrado;
        } else {
            return $this->data_cadastrado instanceof \DateTimeInterface ? $this->data_cadastrado->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [data_geracao] column value.
     *
     * @return string
     */
    public function getDataGeracao()
    {
        return $this->data_geracao;
    }

    /**
     * Get the [competencia_id] column value.
     *
     * @return int
     */
    public function getCompetenciaId()
    {
        return $this->competencia_id;
    }

    /**
     * Get the [convenio_id] column value.
     *
     * @return int
     */
    public function getConvenioId()
    {
        return $this->convenio_id;
    }

    /**
     * Set the value of [idlancamentos_boletos] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     */
    public function setIdlancamentosBoletos($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idlancamentos_boletos !== $v) {
            $this->idlancamentos_boletos = $v;
            $this->modifiedColumns[LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS] = true;
        }

        return $this;
    } // setIdlancamentosBoletos()

    /**
     * Set the value of [lancamento_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     */
    public function setLancamentoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->lancamento_id !== $v) {
            $this->lancamento_id = $v;
            $this->modifiedColumns[LancamentosBoletosTableMap::COL_LANCAMENTO_ID] = true;
        }

        if ($this->aLancamentos !== null && $this->aLancamentos->getIdlancamento() !== $v) {
            $this->aLancamentos = null;
        }

        return $this;
    } // setLancamentoId()

    /**
     * Set the value of [boleto_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     */
    public function setBoletoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->boleto_id !== $v) {
            $this->boleto_id = $v;
            $this->modifiedColumns[LancamentosBoletosTableMap::COL_BOLETO_ID] = true;
        }

        if ($this->aBoleto !== null && $this->aBoleto->getIdboleto() !== $v) {
            $this->aBoleto = null;
        }

        return $this;
    } // setBoletoId()

    /**
     * Set the value of [conteudo_linhas_lancamento_boleto] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     */
    public function setConteudoLinhasLancamentoBoleto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->conteudo_linhas_lancamento_boleto !== $v) {
            $this->conteudo_linhas_lancamento_boleto = $v;
            $this->modifiedColumns[LancamentosBoletosTableMap::COL_CONTEUDO_LINHAS_LANCAMENTO_BOLETO] = true;
        }

        return $this;
    } // setConteudoLinhasLancamentoBoleto()

    /**
     * Sets the value of [data_cadastrado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     */
    public function setDataCadastrado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastrado !== null || $dt !== null) {
            if ($this->data_cadastrado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastrado->format("Y-m-d H:i:s.u")) {
                $this->data_cadastrado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[LancamentosBoletosTableMap::COL_DATA_CADASTRADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastrado()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[LancamentosBoletosTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [data_geracao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     */
    public function setDataGeracao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->data_geracao !== $v) {
            $this->data_geracao = $v;
            $this->modifiedColumns[LancamentosBoletosTableMap::COL_DATA_GERACAO] = true;
        }

        return $this;
    } // setDataGeracao()

    /**
     * Set the value of [competencia_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     */
    public function setCompetenciaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->competencia_id !== $v) {
            $this->competencia_id = $v;
            $this->modifiedColumns[LancamentosBoletosTableMap::COL_COMPETENCIA_ID] = true;
        }

        if ($this->aCompetencia !== null && $this->aCompetencia->getId() !== $v) {
            $this->aCompetencia = null;
        }

        return $this;
    } // setCompetenciaId()

    /**
     * Set the value of [convenio_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     */
    public function setConvenioId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->convenio_id !== $v) {
            $this->convenio_id = $v;
            $this->modifiedColumns[LancamentosBoletosTableMap::COL_CONVENIO_ID] = true;
        }

        if ($this->aConvenio !== null && $this->aConvenio->getIdconvenio() !== $v) {
            $this->aConvenio = null;
        }

        return $this;
    } // setConvenioId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : LancamentosBoletosTableMap::translateFieldName('IdlancamentosBoletos', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idlancamentos_boletos = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : LancamentosBoletosTableMap::translateFieldName('LancamentoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->lancamento_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : LancamentosBoletosTableMap::translateFieldName('BoletoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->boleto_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : LancamentosBoletosTableMap::translateFieldName('ConteudoLinhasLancamentoBoleto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->conteudo_linhas_lancamento_boleto = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : LancamentosBoletosTableMap::translateFieldName('DataCadastrado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastrado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : LancamentosBoletosTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : LancamentosBoletosTableMap::translateFieldName('DataGeracao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->data_geracao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : LancamentosBoletosTableMap::translateFieldName('CompetenciaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->competencia_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : LancamentosBoletosTableMap::translateFieldName('ConvenioId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->convenio_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = LancamentosBoletosTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\LancamentosBoletos'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aLancamentos !== null && $this->lancamento_id !== $this->aLancamentos->getIdlancamento()) {
            $this->aLancamentos = null;
        }
        if ($this->aBoleto !== null && $this->boleto_id !== $this->aBoleto->getIdboleto()) {
            $this->aBoleto = null;
        }
        if ($this->aCompetencia !== null && $this->competencia_id !== $this->aCompetencia->getId()) {
            $this->aCompetencia = null;
        }
        if ($this->aConvenio !== null && $this->convenio_id !== $this->aConvenio->getIdconvenio()) {
            $this->aConvenio = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LancamentosBoletosTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildLancamentosBoletosQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBoleto = null;
            $this->aCompetencia = null;
            $this->aConvenio = null;
            $this->aLancamentos = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see LancamentosBoletos::setDeleted()
     * @see LancamentosBoletos::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosBoletosTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildLancamentosBoletosQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosBoletosTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                LancamentosBoletosTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBoleto !== null) {
                if ($this->aBoleto->isModified() || $this->aBoleto->isNew()) {
                    $affectedRows += $this->aBoleto->save($con);
                }
                $this->setBoleto($this->aBoleto);
            }

            if ($this->aCompetencia !== null) {
                if ($this->aCompetencia->isModified() || $this->aCompetencia->isNew()) {
                    $affectedRows += $this->aCompetencia->save($con);
                }
                $this->setCompetencia($this->aCompetencia);
            }

            if ($this->aConvenio !== null) {
                if ($this->aConvenio->isModified() || $this->aConvenio->isNew()) {
                    $affectedRows += $this->aConvenio->save($con);
                }
                $this->setConvenio($this->aConvenio);
            }

            if ($this->aLancamentos !== null) {
                if ($this->aLancamentos->isModified() || $this->aLancamentos->isNew()) {
                    $affectedRows += $this->aLancamentos->save($con);
                }
                $this->setLancamentos($this->aLancamentos);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS] = true;
        if (null !== $this->idlancamentos_boletos) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS)) {
            $modifiedColumns[':p' . $index++]  = 'idlancamentos_boletos';
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_LANCAMENTO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'lancamento_id';
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_BOLETO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'boleto_id';
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_CONTEUDO_LINHAS_LANCAMENTO_BOLETO)) {
            $modifiedColumns[':p' . $index++]  = 'conteudo_linhas_lancamento_boleto';
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_DATA_CADASTRADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastrado';
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_DATA_GERACAO)) {
            $modifiedColumns[':p' . $index++]  = 'data_geracao';
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_COMPETENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'competencia_id';
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_CONVENIO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'convenio_id';
        }

        $sql = sprintf(
            'INSERT INTO lancamentos_boletos (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idlancamentos_boletos':
                        $stmt->bindValue($identifier, $this->idlancamentos_boletos, PDO::PARAM_INT);
                        break;
                    case 'lancamento_id':
                        $stmt->bindValue($identifier, $this->lancamento_id, PDO::PARAM_INT);
                        break;
                    case 'boleto_id':
                        $stmt->bindValue($identifier, $this->boleto_id, PDO::PARAM_INT);
                        break;
                    case 'conteudo_linhas_lancamento_boleto':
                        $stmt->bindValue($identifier, $this->conteudo_linhas_lancamento_boleto, PDO::PARAM_STR);
                        break;
                    case 'data_cadastrado':
                        $stmt->bindValue($identifier, $this->data_cadastrado ? $this->data_cadastrado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_geracao':
                        $stmt->bindValue($identifier, $this->data_geracao, PDO::PARAM_STR);
                        break;
                    case 'competencia_id':
                        $stmt->bindValue($identifier, $this->competencia_id, PDO::PARAM_INT);
                        break;
                    case 'convenio_id':
                        $stmt->bindValue($identifier, $this->convenio_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdlancamentosBoletos($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = LancamentosBoletosTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdlancamentosBoletos();
                break;
            case 1:
                return $this->getLancamentoId();
                break;
            case 2:
                return $this->getBoletoId();
                break;
            case 3:
                return $this->getConteudoLinhasLancamentoBoleto();
                break;
            case 4:
                return $this->getDataCadastrado();
                break;
            case 5:
                return $this->getDataAlterado();
                break;
            case 6:
                return $this->getDataGeracao();
                break;
            case 7:
                return $this->getCompetenciaId();
                break;
            case 8:
                return $this->getConvenioId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['LancamentosBoletos'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['LancamentosBoletos'][$this->hashCode()] = true;
        $keys = LancamentosBoletosTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdlancamentosBoletos(),
            $keys[1] => $this->getLancamentoId(),
            $keys[2] => $this->getBoletoId(),
            $keys[3] => $this->getConteudoLinhasLancamentoBoleto(),
            $keys[4] => $this->getDataCadastrado(),
            $keys[5] => $this->getDataAlterado(),
            $keys[6] => $this->getDataGeracao(),
            $keys[7] => $this->getCompetenciaId(),
            $keys[8] => $this->getConvenioId(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBoleto) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boleto';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto';
                        break;
                    default:
                        $key = 'Boleto';
                }

                $result[$key] = $this->aBoleto->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCompetencia) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'competencia';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'competencia';
                        break;
                    default:
                        $key = 'Competencia';
                }

                $result[$key] = $this->aCompetencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aConvenio) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'convenio';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'convenio';
                        break;
                    default:
                        $key = 'Convenio';
                }

                $result[$key] = $this->aConvenio->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLancamentos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lancamentos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lancamentos';
                        break;
                    default:
                        $key = 'Lancamentos';
                }

                $result[$key] = $this->aLancamentos->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = LancamentosBoletosTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdlancamentosBoletos($value);
                break;
            case 1:
                $this->setLancamentoId($value);
                break;
            case 2:
                $this->setBoletoId($value);
                break;
            case 3:
                $this->setConteudoLinhasLancamentoBoleto($value);
                break;
            case 4:
                $this->setDataCadastrado($value);
                break;
            case 5:
                $this->setDataAlterado($value);
                break;
            case 6:
                $this->setDataGeracao($value);
                break;
            case 7:
                $this->setCompetenciaId($value);
                break;
            case 8:
                $this->setConvenioId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = LancamentosBoletosTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdlancamentosBoletos($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setLancamentoId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setBoletoId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setConteudoLinhasLancamentoBoleto($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDataCadastrado($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDataAlterado($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDataGeracao($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCompetenciaId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setConvenioId($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(LancamentosBoletosTableMap::DATABASE_NAME);

        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS)) {
            $criteria->add(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, $this->idlancamentos_boletos);
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_LANCAMENTO_ID)) {
            $criteria->add(LancamentosBoletosTableMap::COL_LANCAMENTO_ID, $this->lancamento_id);
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_BOLETO_ID)) {
            $criteria->add(LancamentosBoletosTableMap::COL_BOLETO_ID, $this->boleto_id);
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_CONTEUDO_LINHAS_LANCAMENTO_BOLETO)) {
            $criteria->add(LancamentosBoletosTableMap::COL_CONTEUDO_LINHAS_LANCAMENTO_BOLETO, $this->conteudo_linhas_lancamento_boleto);
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_DATA_CADASTRADO)) {
            $criteria->add(LancamentosBoletosTableMap::COL_DATA_CADASTRADO, $this->data_cadastrado);
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(LancamentosBoletosTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_DATA_GERACAO)) {
            $criteria->add(LancamentosBoletosTableMap::COL_DATA_GERACAO, $this->data_geracao);
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_COMPETENCIA_ID)) {
            $criteria->add(LancamentosBoletosTableMap::COL_COMPETENCIA_ID, $this->competencia_id);
        }
        if ($this->isColumnModified(LancamentosBoletosTableMap::COL_CONVENIO_ID)) {
            $criteria->add(LancamentosBoletosTableMap::COL_CONVENIO_ID, $this->convenio_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildLancamentosBoletosQuery::create();
        $criteria->add(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, $this->idlancamentos_boletos);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdlancamentosBoletos();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdlancamentosBoletos();
    }

    /**
     * Generic method to set the primary key (idlancamentos_boletos column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdlancamentosBoletos($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdlancamentosBoletos();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\LancamentosBoletos (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLancamentoId($this->getLancamentoId());
        $copyObj->setBoletoId($this->getBoletoId());
        $copyObj->setConteudoLinhasLancamentoBoleto($this->getConteudoLinhasLancamentoBoleto());
        $copyObj->setDataCadastrado($this->getDataCadastrado());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setDataGeracao($this->getDataGeracao());
        $copyObj->setCompetenciaId($this->getCompetenciaId());
        $copyObj->setConvenioId($this->getConvenioId());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdlancamentosBoletos(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\LancamentosBoletos Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBoleto object.
     *
     * @param  ChildBoleto $v
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBoleto(ChildBoleto $v = null)
    {
        if ($v === null) {
            $this->setBoletoId(NULL);
        } else {
            $this->setBoletoId($v->getIdboleto());
        }

        $this->aBoleto = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBoleto object, it will not be re-added.
        if ($v !== null) {
            $v->addLancamentosBoletos($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBoleto object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBoleto The associated ChildBoleto object.
     * @throws PropelException
     */
    public function getBoleto(ConnectionInterface $con = null)
    {
        if ($this->aBoleto === null && ($this->boleto_id !== null)) {
            $this->aBoleto = ChildBoletoQuery::create()->findPk($this->boleto_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBoleto->addLancamentosBoletoss($this);
             */
        }

        return $this->aBoleto;
    }

    /**
     * Declares an association between this object and a ChildCompetencia object.
     *
     * @param  ChildCompetencia $v
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCompetencia(ChildCompetencia $v = null)
    {
        if ($v === null) {
            $this->setCompetenciaId(NULL);
        } else {
            $this->setCompetenciaId($v->getId());
        }

        $this->aCompetencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCompetencia object, it will not be re-added.
        if ($v !== null) {
            $v->addLancamentosBoletos($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCompetencia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCompetencia The associated ChildCompetencia object.
     * @throws PropelException
     */
    public function getCompetencia(ConnectionInterface $con = null)
    {
        if ($this->aCompetencia === null && ($this->competencia_id !== null)) {
            $this->aCompetencia = ChildCompetenciaQuery::create()->findPk($this->competencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCompetencia->addLancamentosBoletoss($this);
             */
        }

        return $this->aCompetencia;
    }

    /**
     * Declares an association between this object and a ChildConvenio object.
     *
     * @param  ChildConvenio $v
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     * @throws PropelException
     */
    public function setConvenio(ChildConvenio $v = null)
    {
        if ($v === null) {
            $this->setConvenioId(NULL);
        } else {
            $this->setConvenioId($v->getIdconvenio());
        }

        $this->aConvenio = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildConvenio object, it will not be re-added.
        if ($v !== null) {
            $v->addLancamentosBoletos($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildConvenio object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildConvenio The associated ChildConvenio object.
     * @throws PropelException
     */
    public function getConvenio(ConnectionInterface $con = null)
    {
        if ($this->aConvenio === null && ($this->convenio_id !== null)) {
            $this->aConvenio = ChildConvenioQuery::create()->findPk($this->convenio_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aConvenio->addLancamentosBoletoss($this);
             */
        }

        return $this->aConvenio;
    }

    /**
     * Declares an association between this object and a ChildLancamentos object.
     *
     * @param  ChildLancamentos $v
     * @return $this|\ImaTelecomBundle\Model\LancamentosBoletos The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLancamentos(ChildLancamentos $v = null)
    {
        if ($v === null) {
            $this->setLancamentoId(NULL);
        } else {
            $this->setLancamentoId($v->getIdlancamento());
        }

        $this->aLancamentos = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildLancamentos object, it will not be re-added.
        if ($v !== null) {
            $v->addLancamentosBoletos($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildLancamentos object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildLancamentos The associated ChildLancamentos object.
     * @throws PropelException
     */
    public function getLancamentos(ConnectionInterface $con = null)
    {
        if ($this->aLancamentos === null && ($this->lancamento_id !== null)) {
            $this->aLancamentos = ChildLancamentosQuery::create()->findPk($this->lancamento_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLancamentos->addLancamentosBoletoss($this);
             */
        }

        return $this->aLancamentos;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBoleto) {
            $this->aBoleto->removeLancamentosBoletos($this);
        }
        if (null !== $this->aCompetencia) {
            $this->aCompetencia->removeLancamentosBoletos($this);
        }
        if (null !== $this->aConvenio) {
            $this->aConvenio->removeLancamentosBoletos($this);
        }
        if (null !== $this->aLancamentos) {
            $this->aLancamentos->removeLancamentosBoletos($this);
        }
        $this->idlancamentos_boletos = null;
        $this->lancamento_id = null;
        $this->boleto_id = null;
        $this->conteudo_linhas_lancamento_boleto = null;
        $this->data_cadastrado = null;
        $this->data_alterado = null;
        $this->data_geracao = null;
        $this->competencia_id = null;
        $this->convenio_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aBoleto = null;
        $this->aCompetencia = null;
        $this->aConvenio = null;
        $this->aLancamentos = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(LancamentosBoletosTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
