<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Convenio as ChildConvenio;
use ImaTelecomBundle\Model\ConvenioQuery as ChildConvenioQuery;
use ImaTelecomBundle\Model\Map\ConvenioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'convenio' table.
 *
 *
 *
 * @method     ChildConvenioQuery orderByIdconvenio($order = Criteria::ASC) Order by the idconvenio column
 * @method     ChildConvenioQuery orderByCodigoConvenio($order = Criteria::ASC) Order by the codigo_convenio column
 * @method     ChildConvenioQuery orderByDigitoVerificador($order = Criteria::ASC) Order by the digito_verificador column
 * @method     ChildConvenioQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildConvenioQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildConvenioQuery orderByCarteira($order = Criteria::ASC) Order by the carteira column
 * @method     ChildConvenioQuery orderByTipoCarteira($order = Criteria::ASC) Order by the tipo_carteira column
 * @method     ChildConvenioQuery orderByCnpj($order = Criteria::ASC) Order by the cnpj column
 * @method     ChildConvenioQuery orderByTipoCobranca($order = Criteria::ASC) Order by the tipo_cobranca column
 * @method     ChildConvenioQuery orderByTipoNossoNumero($order = Criteria::ASC) Order by the tipo_nosso_numero column
 * @method     ChildConvenioQuery orderByLimiteInferior($order = Criteria::ASC) Order by the limite_inferior column
 * @method     ChildConvenioQuery orderByLimiteSuperior($order = Criteria::ASC) Order by the limite_superior column
 * @method     ChildConvenioQuery orderByUltimoSequencial($order = Criteria::ASC) Order by the ultimo_sequencial column
 * @method     ChildConvenioQuery orderByValorCodigoBarra($order = Criteria::ASC) Order by the valor_codigo_barra column
 * @method     ChildConvenioQuery orderByUltimoSequencialArquivoCobranca($order = Criteria::ASC) Order by the ultimo_sequencial_arquivo_cobranca column
 * @method     ChildConvenioQuery orderByUltimoSequencialDebitoAutomatico($order = Criteria::ASC) Order by the ultimo_sequencial_debito_automatico column
 * @method     ChildConvenioQuery orderByEmpresaId($order = Criteria::ASC) Order by the empresa_id column
 * @method     ChildConvenioQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildConvenioQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildConvenioQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildConvenioQuery groupByIdconvenio() Group by the idconvenio column
 * @method     ChildConvenioQuery groupByCodigoConvenio() Group by the codigo_convenio column
 * @method     ChildConvenioQuery groupByDigitoVerificador() Group by the digito_verificador column
 * @method     ChildConvenioQuery groupByNome() Group by the nome column
 * @method     ChildConvenioQuery groupByAtivo() Group by the ativo column
 * @method     ChildConvenioQuery groupByCarteira() Group by the carteira column
 * @method     ChildConvenioQuery groupByTipoCarteira() Group by the tipo_carteira column
 * @method     ChildConvenioQuery groupByCnpj() Group by the cnpj column
 * @method     ChildConvenioQuery groupByTipoCobranca() Group by the tipo_cobranca column
 * @method     ChildConvenioQuery groupByTipoNossoNumero() Group by the tipo_nosso_numero column
 * @method     ChildConvenioQuery groupByLimiteInferior() Group by the limite_inferior column
 * @method     ChildConvenioQuery groupByLimiteSuperior() Group by the limite_superior column
 * @method     ChildConvenioQuery groupByUltimoSequencial() Group by the ultimo_sequencial column
 * @method     ChildConvenioQuery groupByValorCodigoBarra() Group by the valor_codigo_barra column
 * @method     ChildConvenioQuery groupByUltimoSequencialArquivoCobranca() Group by the ultimo_sequencial_arquivo_cobranca column
 * @method     ChildConvenioQuery groupByUltimoSequencialDebitoAutomatico() Group by the ultimo_sequencial_debito_automatico column
 * @method     ChildConvenioQuery groupByEmpresaId() Group by the empresa_id column
 * @method     ChildConvenioQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildConvenioQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildConvenioQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildConvenioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildConvenioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildConvenioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildConvenioQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildConvenioQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildConvenioQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildConvenioQuery leftJoinEmpresa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Empresa relation
 * @method     ChildConvenioQuery rightJoinEmpresa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Empresa relation
 * @method     ChildConvenioQuery innerJoinEmpresa($relationAlias = null) Adds a INNER JOIN clause to the query using the Empresa relation
 *
 * @method     ChildConvenioQuery joinWithEmpresa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Empresa relation
 *
 * @method     ChildConvenioQuery leftJoinWithEmpresa() Adds a LEFT JOIN clause and with to the query using the Empresa relation
 * @method     ChildConvenioQuery rightJoinWithEmpresa() Adds a RIGHT JOIN clause and with to the query using the Empresa relation
 * @method     ChildConvenioQuery innerJoinWithEmpresa() Adds a INNER JOIN clause and with to the query using the Empresa relation
 *
 * @method     ChildConvenioQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildConvenioQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildConvenioQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildConvenioQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildConvenioQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildConvenioQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildConvenioQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildConvenioQuery leftJoinBancoAgenciaConta($relationAlias = null) Adds a LEFT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildConvenioQuery rightJoinBancoAgenciaConta($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildConvenioQuery innerJoinBancoAgenciaConta($relationAlias = null) Adds a INNER JOIN clause to the query using the BancoAgenciaConta relation
 *
 * @method     ChildConvenioQuery joinWithBancoAgenciaConta($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildConvenioQuery leftJoinWithBancoAgenciaConta() Adds a LEFT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildConvenioQuery rightJoinWithBancoAgenciaConta() Adds a RIGHT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildConvenioQuery innerJoinWithBancoAgenciaConta() Adds a INNER JOIN clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildConvenioQuery leftJoinLancamentos($relationAlias = null) Adds a LEFT JOIN clause to the query using the Lancamentos relation
 * @method     ChildConvenioQuery rightJoinLancamentos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Lancamentos relation
 * @method     ChildConvenioQuery innerJoinLancamentos($relationAlias = null) Adds a INNER JOIN clause to the query using the Lancamentos relation
 *
 * @method     ChildConvenioQuery joinWithLancamentos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Lancamentos relation
 *
 * @method     ChildConvenioQuery leftJoinWithLancamentos() Adds a LEFT JOIN clause and with to the query using the Lancamentos relation
 * @method     ChildConvenioQuery rightJoinWithLancamentos() Adds a RIGHT JOIN clause and with to the query using the Lancamentos relation
 * @method     ChildConvenioQuery innerJoinWithLancamentos() Adds a INNER JOIN clause and with to the query using the Lancamentos relation
 *
 * @method     ChildConvenioQuery leftJoinLancamentosBoletos($relationAlias = null) Adds a LEFT JOIN clause to the query using the LancamentosBoletos relation
 * @method     ChildConvenioQuery rightJoinLancamentosBoletos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LancamentosBoletos relation
 * @method     ChildConvenioQuery innerJoinLancamentosBoletos($relationAlias = null) Adds a INNER JOIN clause to the query using the LancamentosBoletos relation
 *
 * @method     ChildConvenioQuery joinWithLancamentosBoletos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LancamentosBoletos relation
 *
 * @method     ChildConvenioQuery leftJoinWithLancamentosBoletos() Adds a LEFT JOIN clause and with to the query using the LancamentosBoletos relation
 * @method     ChildConvenioQuery rightJoinWithLancamentosBoletos() Adds a RIGHT JOIN clause and with to the query using the LancamentosBoletos relation
 * @method     ChildConvenioQuery innerJoinWithLancamentosBoletos() Adds a INNER JOIN clause and with to the query using the LancamentosBoletos relation
 *
 * @method     \ImaTelecomBundle\Model\EmpresaQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BancoAgenciaContaQuery|\ImaTelecomBundle\Model\LancamentosQuery|\ImaTelecomBundle\Model\LancamentosBoletosQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildConvenio findOne(ConnectionInterface $con = null) Return the first ChildConvenio matching the query
 * @method     ChildConvenio findOneOrCreate(ConnectionInterface $con = null) Return the first ChildConvenio matching the query, or a new ChildConvenio object populated from the query conditions when no match is found
 *
 * @method     ChildConvenio findOneByIdconvenio(int $idconvenio) Return the first ChildConvenio filtered by the idconvenio column
 * @method     ChildConvenio findOneByCodigoConvenio(string $codigo_convenio) Return the first ChildConvenio filtered by the codigo_convenio column
 * @method     ChildConvenio findOneByDigitoVerificador(string $digito_verificador) Return the first ChildConvenio filtered by the digito_verificador column
 * @method     ChildConvenio findOneByNome(string $nome) Return the first ChildConvenio filtered by the nome column
 * @method     ChildConvenio findOneByAtivo(boolean $ativo) Return the first ChildConvenio filtered by the ativo column
 * @method     ChildConvenio findOneByCarteira(string $carteira) Return the first ChildConvenio filtered by the carteira column
 * @method     ChildConvenio findOneByTipoCarteira(string $tipo_carteira) Return the first ChildConvenio filtered by the tipo_carteira column
 * @method     ChildConvenio findOneByCnpj(string $cnpj) Return the first ChildConvenio filtered by the cnpj column
 * @method     ChildConvenio findOneByTipoCobranca(string $tipo_cobranca) Return the first ChildConvenio filtered by the tipo_cobranca column
 * @method     ChildConvenio findOneByTipoNossoNumero(string $tipo_nosso_numero) Return the first ChildConvenio filtered by the tipo_nosso_numero column
 * @method     ChildConvenio findOneByLimiteInferior(string $limite_inferior) Return the first ChildConvenio filtered by the limite_inferior column
 * @method     ChildConvenio findOneByLimiteSuperior(string $limite_superior) Return the first ChildConvenio filtered by the limite_superior column
 * @method     ChildConvenio findOneByUltimoSequencial(string $ultimo_sequencial) Return the first ChildConvenio filtered by the ultimo_sequencial column
 * @method     ChildConvenio findOneByValorCodigoBarra(string $valor_codigo_barra) Return the first ChildConvenio filtered by the valor_codigo_barra column
 * @method     ChildConvenio findOneByUltimoSequencialArquivoCobranca(string $ultimo_sequencial_arquivo_cobranca) Return the first ChildConvenio filtered by the ultimo_sequencial_arquivo_cobranca column
 * @method     ChildConvenio findOneByUltimoSequencialDebitoAutomatico(string $ultimo_sequencial_debito_automatico) Return the first ChildConvenio filtered by the ultimo_sequencial_debito_automatico column
 * @method     ChildConvenio findOneByEmpresaId(int $empresa_id) Return the first ChildConvenio filtered by the empresa_id column
 * @method     ChildConvenio findOneByDataCadastro(string $data_cadastro) Return the first ChildConvenio filtered by the data_cadastro column
 * @method     ChildConvenio findOneByDataAlterado(string $data_alterado) Return the first ChildConvenio filtered by the data_alterado column
 * @method     ChildConvenio findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildConvenio filtered by the usuario_alterado column *

 * @method     ChildConvenio requirePk($key, ConnectionInterface $con = null) Return the ChildConvenio by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOne(ConnectionInterface $con = null) Return the first ChildConvenio matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildConvenio requireOneByIdconvenio(int $idconvenio) Return the first ChildConvenio filtered by the idconvenio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByCodigoConvenio(string $codigo_convenio) Return the first ChildConvenio filtered by the codigo_convenio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByDigitoVerificador(string $digito_verificador) Return the first ChildConvenio filtered by the digito_verificador column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByNome(string $nome) Return the first ChildConvenio filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByAtivo(boolean $ativo) Return the first ChildConvenio filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByCarteira(string $carteira) Return the first ChildConvenio filtered by the carteira column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByTipoCarteira(string $tipo_carteira) Return the first ChildConvenio filtered by the tipo_carteira column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByCnpj(string $cnpj) Return the first ChildConvenio filtered by the cnpj column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByTipoCobranca(string $tipo_cobranca) Return the first ChildConvenio filtered by the tipo_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByTipoNossoNumero(string $tipo_nosso_numero) Return the first ChildConvenio filtered by the tipo_nosso_numero column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByLimiteInferior(string $limite_inferior) Return the first ChildConvenio filtered by the limite_inferior column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByLimiteSuperior(string $limite_superior) Return the first ChildConvenio filtered by the limite_superior column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByUltimoSequencial(string $ultimo_sequencial) Return the first ChildConvenio filtered by the ultimo_sequencial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByValorCodigoBarra(string $valor_codigo_barra) Return the first ChildConvenio filtered by the valor_codigo_barra column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByUltimoSequencialArquivoCobranca(string $ultimo_sequencial_arquivo_cobranca) Return the first ChildConvenio filtered by the ultimo_sequencial_arquivo_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByUltimoSequencialDebitoAutomatico(string $ultimo_sequencial_debito_automatico) Return the first ChildConvenio filtered by the ultimo_sequencial_debito_automatico column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByEmpresaId(int $empresa_id) Return the first ChildConvenio filtered by the empresa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByDataCadastro(string $data_cadastro) Return the first ChildConvenio filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByDataAlterado(string $data_alterado) Return the first ChildConvenio filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildConvenio requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildConvenio filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildConvenio[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildConvenio objects based on current ModelCriteria
 * @method     ChildConvenio[]|ObjectCollection findByIdconvenio(int $idconvenio) Return ChildConvenio objects filtered by the idconvenio column
 * @method     ChildConvenio[]|ObjectCollection findByCodigoConvenio(string $codigo_convenio) Return ChildConvenio objects filtered by the codigo_convenio column
 * @method     ChildConvenio[]|ObjectCollection findByDigitoVerificador(string $digito_verificador) Return ChildConvenio objects filtered by the digito_verificador column
 * @method     ChildConvenio[]|ObjectCollection findByNome(string $nome) Return ChildConvenio objects filtered by the nome column
 * @method     ChildConvenio[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildConvenio objects filtered by the ativo column
 * @method     ChildConvenio[]|ObjectCollection findByCarteira(string $carteira) Return ChildConvenio objects filtered by the carteira column
 * @method     ChildConvenio[]|ObjectCollection findByTipoCarteira(string $tipo_carteira) Return ChildConvenio objects filtered by the tipo_carteira column
 * @method     ChildConvenio[]|ObjectCollection findByCnpj(string $cnpj) Return ChildConvenio objects filtered by the cnpj column
 * @method     ChildConvenio[]|ObjectCollection findByTipoCobranca(string $tipo_cobranca) Return ChildConvenio objects filtered by the tipo_cobranca column
 * @method     ChildConvenio[]|ObjectCollection findByTipoNossoNumero(string $tipo_nosso_numero) Return ChildConvenio objects filtered by the tipo_nosso_numero column
 * @method     ChildConvenio[]|ObjectCollection findByLimiteInferior(string $limite_inferior) Return ChildConvenio objects filtered by the limite_inferior column
 * @method     ChildConvenio[]|ObjectCollection findByLimiteSuperior(string $limite_superior) Return ChildConvenio objects filtered by the limite_superior column
 * @method     ChildConvenio[]|ObjectCollection findByUltimoSequencial(string $ultimo_sequencial) Return ChildConvenio objects filtered by the ultimo_sequencial column
 * @method     ChildConvenio[]|ObjectCollection findByValorCodigoBarra(string $valor_codigo_barra) Return ChildConvenio objects filtered by the valor_codigo_barra column
 * @method     ChildConvenio[]|ObjectCollection findByUltimoSequencialArquivoCobranca(string $ultimo_sequencial_arquivo_cobranca) Return ChildConvenio objects filtered by the ultimo_sequencial_arquivo_cobranca column
 * @method     ChildConvenio[]|ObjectCollection findByUltimoSequencialDebitoAutomatico(string $ultimo_sequencial_debito_automatico) Return ChildConvenio objects filtered by the ultimo_sequencial_debito_automatico column
 * @method     ChildConvenio[]|ObjectCollection findByEmpresaId(int $empresa_id) Return ChildConvenio objects filtered by the empresa_id column
 * @method     ChildConvenio[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildConvenio objects filtered by the data_cadastro column
 * @method     ChildConvenio[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildConvenio objects filtered by the data_alterado column
 * @method     ChildConvenio[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildConvenio objects filtered by the usuario_alterado column
 * @method     ChildConvenio[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ConvenioQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ConvenioQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Convenio', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildConvenioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildConvenioQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildConvenioQuery) {
            return $criteria;
        }
        $query = new ChildConvenioQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildConvenio|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ConvenioTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ConvenioTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildConvenio A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idconvenio, codigo_convenio, digito_verificador, nome, ativo, carteira, tipo_carteira, cnpj, tipo_cobranca, tipo_nosso_numero, limite_inferior, limite_superior, ultimo_sequencial, valor_codigo_barra, ultimo_sequencial_arquivo_cobranca, ultimo_sequencial_debito_automatico, empresa_id, data_cadastro, data_alterado, usuario_alterado FROM convenio WHERE idconvenio = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildConvenio $obj */
            $obj = new ChildConvenio();
            $obj->hydrate($row);
            ConvenioTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildConvenio|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ConvenioTableMap::COL_IDCONVENIO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ConvenioTableMap::COL_IDCONVENIO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idconvenio column
     *
     * Example usage:
     * <code>
     * $query->filterByIdconvenio(1234); // WHERE idconvenio = 1234
     * $query->filterByIdconvenio(array(12, 34)); // WHERE idconvenio IN (12, 34)
     * $query->filterByIdconvenio(array('min' => 12)); // WHERE idconvenio > 12
     * </code>
     *
     * @param     mixed $idconvenio The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByIdconvenio($idconvenio = null, $comparison = null)
    {
        if (is_array($idconvenio)) {
            $useMinMax = false;
            if (isset($idconvenio['min'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_IDCONVENIO, $idconvenio['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idconvenio['max'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_IDCONVENIO, $idconvenio['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_IDCONVENIO, $idconvenio, $comparison);
    }

    /**
     * Filter the query on the codigo_convenio column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigoConvenio('fooValue');   // WHERE codigo_convenio = 'fooValue'
     * $query->filterByCodigoConvenio('%fooValue%', Criteria::LIKE); // WHERE codigo_convenio LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigoConvenio The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByCodigoConvenio($codigoConvenio = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigoConvenio)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_CODIGO_CONVENIO, $codigoConvenio, $comparison);
    }

    /**
     * Filter the query on the digito_verificador column
     *
     * Example usage:
     * <code>
     * $query->filterByDigitoVerificador('fooValue');   // WHERE digito_verificador = 'fooValue'
     * $query->filterByDigitoVerificador('%fooValue%', Criteria::LIKE); // WHERE digito_verificador LIKE '%fooValue%'
     * </code>
     *
     * @param     string $digitoVerificador The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByDigitoVerificador($digitoVerificador = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($digitoVerificador)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_DIGITO_VERIFICADOR, $digitoVerificador, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the carteira column
     *
     * Example usage:
     * <code>
     * $query->filterByCarteira('fooValue');   // WHERE carteira = 'fooValue'
     * $query->filterByCarteira('%fooValue%', Criteria::LIKE); // WHERE carteira LIKE '%fooValue%'
     * </code>
     *
     * @param     string $carteira The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByCarteira($carteira = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($carteira)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_CARTEIRA, $carteira, $comparison);
    }

    /**
     * Filter the query on the tipo_carteira column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoCarteira('fooValue');   // WHERE tipo_carteira = 'fooValue'
     * $query->filterByTipoCarteira('%fooValue%', Criteria::LIKE); // WHERE tipo_carteira LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoCarteira The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByTipoCarteira($tipoCarteira = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoCarteira)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_TIPO_CARTEIRA, $tipoCarteira, $comparison);
    }

    /**
     * Filter the query on the cnpj column
     *
     * Example usage:
     * <code>
     * $query->filterByCnpj('fooValue');   // WHERE cnpj = 'fooValue'
     * $query->filterByCnpj('%fooValue%', Criteria::LIKE); // WHERE cnpj LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cnpj The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByCnpj($cnpj = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cnpj)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_CNPJ, $cnpj, $comparison);
    }

    /**
     * Filter the query on the tipo_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoCobranca('fooValue');   // WHERE tipo_cobranca = 'fooValue'
     * $query->filterByTipoCobranca('%fooValue%', Criteria::LIKE); // WHERE tipo_cobranca LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoCobranca The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByTipoCobranca($tipoCobranca = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoCobranca)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_TIPO_COBRANCA, $tipoCobranca, $comparison);
    }

    /**
     * Filter the query on the tipo_nosso_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoNossoNumero('fooValue');   // WHERE tipo_nosso_numero = 'fooValue'
     * $query->filterByTipoNossoNumero('%fooValue%', Criteria::LIKE); // WHERE tipo_nosso_numero LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoNossoNumero The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByTipoNossoNumero($tipoNossoNumero = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoNossoNumero)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_TIPO_NOSSO_NUMERO, $tipoNossoNumero, $comparison);
    }

    /**
     * Filter the query on the limite_inferior column
     *
     * Example usage:
     * <code>
     * $query->filterByLimiteInferior('fooValue');   // WHERE limite_inferior = 'fooValue'
     * $query->filterByLimiteInferior('%fooValue%', Criteria::LIKE); // WHERE limite_inferior LIKE '%fooValue%'
     * </code>
     *
     * @param     string $limiteInferior The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByLimiteInferior($limiteInferior = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($limiteInferior)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_LIMITE_INFERIOR, $limiteInferior, $comparison);
    }

    /**
     * Filter the query on the limite_superior column
     *
     * Example usage:
     * <code>
     * $query->filterByLimiteSuperior('fooValue');   // WHERE limite_superior = 'fooValue'
     * $query->filterByLimiteSuperior('%fooValue%', Criteria::LIKE); // WHERE limite_superior LIKE '%fooValue%'
     * </code>
     *
     * @param     string $limiteSuperior The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByLimiteSuperior($limiteSuperior = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($limiteSuperior)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_LIMITE_SUPERIOR, $limiteSuperior, $comparison);
    }

    /**
     * Filter the query on the ultimo_sequencial column
     *
     * Example usage:
     * <code>
     * $query->filterByUltimoSequencial('fooValue');   // WHERE ultimo_sequencial = 'fooValue'
     * $query->filterByUltimoSequencial('%fooValue%', Criteria::LIKE); // WHERE ultimo_sequencial LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ultimoSequencial The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByUltimoSequencial($ultimoSequencial = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ultimoSequencial)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL, $ultimoSequencial, $comparison);
    }

    /**
     * Filter the query on the valor_codigo_barra column
     *
     * Example usage:
     * <code>
     * $query->filterByValorCodigoBarra('fooValue');   // WHERE valor_codigo_barra = 'fooValue'
     * $query->filterByValorCodigoBarra('%fooValue%', Criteria::LIKE); // WHERE valor_codigo_barra LIKE '%fooValue%'
     * </code>
     *
     * @param     string $valorCodigoBarra The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByValorCodigoBarra($valorCodigoBarra = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($valorCodigoBarra)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_VALOR_CODIGO_BARRA, $valorCodigoBarra, $comparison);
    }

    /**
     * Filter the query on the ultimo_sequencial_arquivo_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByUltimoSequencialArquivoCobranca('fooValue');   // WHERE ultimo_sequencial_arquivo_cobranca = 'fooValue'
     * $query->filterByUltimoSequencialArquivoCobranca('%fooValue%', Criteria::LIKE); // WHERE ultimo_sequencial_arquivo_cobranca LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ultimoSequencialArquivoCobranca The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByUltimoSequencialArquivoCobranca($ultimoSequencialArquivoCobranca = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ultimoSequencialArquivoCobranca)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_ARQUIVO_COBRANCA, $ultimoSequencialArquivoCobranca, $comparison);
    }

    /**
     * Filter the query on the ultimo_sequencial_debito_automatico column
     *
     * Example usage:
     * <code>
     * $query->filterByUltimoSequencialDebitoAutomatico('fooValue');   // WHERE ultimo_sequencial_debito_automatico = 'fooValue'
     * $query->filterByUltimoSequencialDebitoAutomatico('%fooValue%', Criteria::LIKE); // WHERE ultimo_sequencial_debito_automatico LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ultimoSequencialDebitoAutomatico The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByUltimoSequencialDebitoAutomatico($ultimoSequencialDebitoAutomatico = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ultimoSequencialDebitoAutomatico)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_DEBITO_AUTOMATICO, $ultimoSequencialDebitoAutomatico, $comparison);
    }

    /**
     * Filter the query on the empresa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEmpresaId(1234); // WHERE empresa_id = 1234
     * $query->filterByEmpresaId(array(12, 34)); // WHERE empresa_id IN (12, 34)
     * $query->filterByEmpresaId(array('min' => 12)); // WHERE empresa_id > 12
     * </code>
     *
     * @see       filterByEmpresa()
     *
     * @param     mixed $empresaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByEmpresaId($empresaId = null, $comparison = null)
    {
        if (is_array($empresaId)) {
            $useMinMax = false;
            if (isset($empresaId['min'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_EMPRESA_ID, $empresaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($empresaId['max'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_EMPRESA_ID, $empresaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_EMPRESA_ID, $empresaId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ConvenioTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConvenioTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Empresa object
     *
     * @param \ImaTelecomBundle\Model\Empresa|ObjectCollection $empresa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByEmpresa($empresa, $comparison = null)
    {
        if ($empresa instanceof \ImaTelecomBundle\Model\Empresa) {
            return $this
                ->addUsingAlias(ConvenioTableMap::COL_EMPRESA_ID, $empresa->getIdempresa(), $comparison);
        } elseif ($empresa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ConvenioTableMap::COL_EMPRESA_ID, $empresa->toKeyValue('PrimaryKey', 'Idempresa'), $comparison);
        } else {
            throw new PropelException('filterByEmpresa() only accepts arguments of type \ImaTelecomBundle\Model\Empresa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Empresa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function joinEmpresa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Empresa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Empresa');
        }

        return $this;
    }

    /**
     * Use the Empresa relation Empresa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EmpresaQuery A secondary query class using the current class as primary query
     */
    public function useEmpresaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEmpresa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Empresa', '\ImaTelecomBundle\Model\EmpresaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ConvenioTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ConvenioTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BancoAgenciaConta object
     *
     * @param \ImaTelecomBundle\Model\BancoAgenciaConta|ObjectCollection $bancoAgenciaConta the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByBancoAgenciaConta($bancoAgenciaConta, $comparison = null)
    {
        if ($bancoAgenciaConta instanceof \ImaTelecomBundle\Model\BancoAgenciaConta) {
            return $this
                ->addUsingAlias(ConvenioTableMap::COL_IDCONVENIO, $bancoAgenciaConta->getConvenioId(), $comparison);
        } elseif ($bancoAgenciaConta instanceof ObjectCollection) {
            return $this
                ->useBancoAgenciaContaQuery()
                ->filterByPrimaryKeys($bancoAgenciaConta->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBancoAgenciaConta() only accepts arguments of type \ImaTelecomBundle\Model\BancoAgenciaConta or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BancoAgenciaConta relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function joinBancoAgenciaConta($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BancoAgenciaConta');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BancoAgenciaConta');
        }

        return $this;
    }

    /**
     * Use the BancoAgenciaConta relation BancoAgenciaConta object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoAgenciaContaQuery A secondary query class using the current class as primary query
     */
    public function useBancoAgenciaContaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBancoAgenciaConta($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BancoAgenciaConta', '\ImaTelecomBundle\Model\BancoAgenciaContaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Lancamentos object
     *
     * @param \ImaTelecomBundle\Model\Lancamentos|ObjectCollection $lancamentos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByLancamentos($lancamentos, $comparison = null)
    {
        if ($lancamentos instanceof \ImaTelecomBundle\Model\Lancamentos) {
            return $this
                ->addUsingAlias(ConvenioTableMap::COL_IDCONVENIO, $lancamentos->getConvenioId(), $comparison);
        } elseif ($lancamentos instanceof ObjectCollection) {
            return $this
                ->useLancamentosQuery()
                ->filterByPrimaryKeys($lancamentos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLancamentos() only accepts arguments of type \ImaTelecomBundle\Model\Lancamentos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Lancamentos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function joinLancamentos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Lancamentos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Lancamentos');
        }

        return $this;
    }

    /**
     * Use the Lancamentos relation Lancamentos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\LancamentosQuery A secondary query class using the current class as primary query
     */
    public function useLancamentosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLancamentos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Lancamentos', '\ImaTelecomBundle\Model\LancamentosQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\LancamentosBoletos object
     *
     * @param \ImaTelecomBundle\Model\LancamentosBoletos|ObjectCollection $lancamentosBoletos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildConvenioQuery The current query, for fluid interface
     */
    public function filterByLancamentosBoletos($lancamentosBoletos, $comparison = null)
    {
        if ($lancamentosBoletos instanceof \ImaTelecomBundle\Model\LancamentosBoletos) {
            return $this
                ->addUsingAlias(ConvenioTableMap::COL_IDCONVENIO, $lancamentosBoletos->getConvenioId(), $comparison);
        } elseif ($lancamentosBoletos instanceof ObjectCollection) {
            return $this
                ->useLancamentosBoletosQuery()
                ->filterByPrimaryKeys($lancamentosBoletos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLancamentosBoletos() only accepts arguments of type \ImaTelecomBundle\Model\LancamentosBoletos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LancamentosBoletos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function joinLancamentosBoletos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LancamentosBoletos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LancamentosBoletos');
        }

        return $this;
    }

    /**
     * Use the LancamentosBoletos relation LancamentosBoletos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\LancamentosBoletosQuery A secondary query class using the current class as primary query
     */
    public function useLancamentosBoletosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLancamentosBoletos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LancamentosBoletos', '\ImaTelecomBundle\Model\LancamentosBoletosQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildConvenio $convenio Object to remove from the list of results
     *
     * @return $this|ChildConvenioQuery The current query, for fluid interface
     */
    public function prune($convenio = null)
    {
        if ($convenio) {
            $this->addUsingAlias(ConvenioTableMap::COL_IDCONVENIO, $convenio->getIdconvenio(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the convenio table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConvenioTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ConvenioTableMap::clearInstancePool();
            ConvenioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConvenioTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ConvenioTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ConvenioTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ConvenioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ConvenioQuery
