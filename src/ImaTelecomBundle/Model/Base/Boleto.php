<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Baixa as ChildBaixa;
use ImaTelecomBundle\Model\BaixaEstorno as ChildBaixaEstorno;
use ImaTelecomBundle\Model\BaixaEstornoQuery as ChildBaixaEstornoQuery;
use ImaTelecomBundle\Model\BaixaQuery as ChildBaixaQuery;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoBaixaHistorico as ChildBoletoBaixaHistorico;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery as ChildBoletoBaixaHistoricoQuery;
use ImaTelecomBundle\Model\BoletoItem as ChildBoletoItem;
use ImaTelecomBundle\Model\BoletoItemQuery as ChildBoletoItemQuery;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Cliente as ChildCliente;
use ImaTelecomBundle\Model\ClienteQuery as ChildClienteQuery;
use ImaTelecomBundle\Model\Competencia as ChildCompetencia;
use ImaTelecomBundle\Model\CompetenciaQuery as ChildCompetenciaQuery;
use ImaTelecomBundle\Model\DadosFiscal as ChildDadosFiscal;
use ImaTelecomBundle\Model\DadosFiscalItem as ChildDadosFiscalItem;
use ImaTelecomBundle\Model\DadosFiscalItemQuery as ChildDadosFiscalItemQuery;
use ImaTelecomBundle\Model\DadosFiscalQuery as ChildDadosFiscalQuery;
use ImaTelecomBundle\Model\Fornecedor as ChildFornecedor;
use ImaTelecomBundle\Model\FornecedorQuery as ChildFornecedorQuery;
use ImaTelecomBundle\Model\LancamentosBoletos as ChildLancamentosBoletos;
use ImaTelecomBundle\Model\LancamentosBoletosQuery as ChildLancamentosBoletosQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BoletoBaixaHistoricoTableMap;
use ImaTelecomBundle\Model\Map\BoletoItemTableMap;
use ImaTelecomBundle\Model\Map\BoletoTableMap;
use ImaTelecomBundle\Model\Map\DadosFiscalItemTableMap;
use ImaTelecomBundle\Model\Map\DadosFiscalTableMap;
use ImaTelecomBundle\Model\Map\LancamentosBoletosTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'boleto' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Boleto implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\BoletoTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idboleto field.
     *
     * @var        int
     */
    protected $idboleto;

    /**
     * The value for the valor field.
     *
     * @var        double
     */
    protected $valor;

    /**
     * The value for the vencimento field.
     *
     * @var        DateTime
     */
    protected $vencimento;

    /**
     * The value for the cliente_id field.
     *
     * @var        int
     */
    protected $cliente_id;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the import_id field.
     *
     * @var        int
     */
    protected $import_id;

    /**
     * The value for the competencia_id field.
     *
     * @var        int
     */
    protected $competencia_id;

    /**
     * The value for the nosso_numero field.
     *
     * @var        string
     */
    protected $nosso_numero;

    /**
     * The value for the numero_documento field.
     *
     * @var        string
     */
    protected $numero_documento;

    /**
     * The value for the registrado field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $registrado;

    /**
     * The value for the valor_original field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_original;

    /**
     * The value for the valor_multa field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_multa;

    /**
     * The value for the valor_juros field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_juros;

    /**
     * The value for the valor_desconto field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_desconto;

    /**
     * The value for the valor_pago field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_pago;

    /**
     * The value for the linha_digitavel field.
     *
     * @var        string
     */
    protected $linha_digitavel;

    /**
     * The value for the data_pagamento field.
     *
     * @var        DateTime
     */
    protected $data_pagamento;

    /**
     * The value for the esta_pago field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $esta_pago;

    /**
     * The value for the valor_abatimento field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_abatimento;

    /**
     * The value for the valor_iof field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_iof;

    /**
     * The value for the valor_liquido field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_liquido;

    /**
     * The value for the valor_outras_despesas field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_outras_despesas;

    /**
     * The value for the valor_outros_creditos field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_outros_creditos;

    /**
     * The value for the nota_scm_gerada field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $nota_scm_gerada;

    /**
     * The value for the nota_scm_cancelada field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $nota_scm_cancelada;

    /**
     * The value for the base_calculo_scm field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $base_calculo_scm;

    /**
     * The value for the base_calculo_sva field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $base_calculo_sva;

    /**
     * The value for the nota_sva_gerada field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $nota_sva_gerada;

    /**
     * The value for the nota_sva_cancelada field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $nota_sva_cancelada;

    /**
     * The value for the tipo_conta field.
     *
     * Note: this column has a database default value of: 'a_receber'
     * @var        string
     */
    protected $tipo_conta;

    /**
     * The value for the baixa_id field.
     *
     * @var        int
     */
    protected $baixa_id;

    /**
     * The value for the baixa_estorno_id field.
     *
     * @var        int
     */
    protected $baixa_estorno_id;

    /**
     * The value for the fornecedor_id field.
     *
     * @var        int
     */
    protected $fornecedor_id;

    /**
     * @var        ChildBaixaEstorno
     */
    protected $aBaixaEstorno;

    /**
     * @var        ChildBaixa
     */
    protected $aBaixa;

    /**
     * @var        ChildCliente
     */
    protected $aCliente;

    /**
     * @var        ChildCompetencia
     */
    protected $aCompetencia;

    /**
     * @var        ChildFornecedor
     */
    protected $aFornecedor;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildBoletoBaixaHistorico[] Collection to store aggregation of ChildBoletoBaixaHistorico objects.
     */
    protected $collBoletoBaixaHistoricos;
    protected $collBoletoBaixaHistoricosPartial;

    /**
     * @var        ObjectCollection|ChildBoletoItem[] Collection to store aggregation of ChildBoletoItem objects.
     */
    protected $collBoletoItems;
    protected $collBoletoItemsPartial;

    /**
     * @var        ObjectCollection|ChildDadosFiscal[] Collection to store aggregation of ChildDadosFiscal objects.
     */
    protected $collDadosFiscals;
    protected $collDadosFiscalsPartial;

    /**
     * @var        ObjectCollection|ChildDadosFiscalItem[] Collection to store aggregation of ChildDadosFiscalItem objects.
     */
    protected $collDadosFiscalItems;
    protected $collDadosFiscalItemsPartial;

    /**
     * @var        ObjectCollection|ChildLancamentosBoletos[] Collection to store aggregation of ChildLancamentosBoletos objects.
     */
    protected $collLancamentosBoletoss;
    protected $collLancamentosBoletossPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoletoBaixaHistorico[]
     */
    protected $boletoBaixaHistoricosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoletoItem[]
     */
    protected $boletoItemsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDadosFiscal[]
     */
    protected $dadosFiscalsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildDadosFiscalItem[]
     */
    protected $dadosFiscalItemsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLancamentosBoletos[]
     */
    protected $lancamentosBoletossScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->registrado = false;
        $this->valor_original = '0.00';
        $this->valor_multa = '0.00';
        $this->valor_juros = '0.00';
        $this->valor_desconto = '0.00';
        $this->valor_pago = '0.00';
        $this->esta_pago = false;
        $this->valor_abatimento = '0.00';
        $this->valor_iof = '0.00';
        $this->valor_liquido = '0.00';
        $this->valor_outras_despesas = '0.00';
        $this->valor_outros_creditos = '0.00';
        $this->nota_scm_gerada = false;
        $this->nota_scm_cancelada = false;
        $this->base_calculo_scm = '0.00';
        $this->base_calculo_sva = '0.00';
        $this->nota_sva_gerada = false;
        $this->nota_sva_cancelada = false;
        $this->tipo_conta = 'a_receber';
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Boleto object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Boleto</code> instance.  If
     * <code>obj</code> is an instance of <code>Boleto</code>, delegates to
     * <code>equals(Boleto)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Boleto The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idboleto] column value.
     *
     * @return int
     */
    public function getIdboleto()
    {
        return $this->idboleto;
    }

    /**
     * Get the [valor] column value.
     *
     * @return double
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Get the [optionally formatted] temporal [vencimento] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getVencimento($format = NULL)
    {
        if ($format === null) {
            return $this->vencimento;
        } else {
            return $this->vencimento instanceof \DateTimeInterface ? $this->vencimento->format($format) : null;
        }
    }

    /**
     * Get the [cliente_id] column value.
     *
     * @return int
     */
    public function getClienteId()
    {
        return $this->cliente_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [import_id] column value.
     *
     * @return int
     */
    public function getImportId()
    {
        return $this->import_id;
    }

    /**
     * Get the [competencia_id] column value.
     *
     * @return int
     */
    public function getCompetenciaId()
    {
        return $this->competencia_id;
    }

    /**
     * Get the [nosso_numero] column value.
     *
     * @return string
     */
    public function getNossoNumero()
    {
        return $this->nosso_numero;
    }

    /**
     * Get the [numero_documento] column value.
     *
     * @return string
     */
    public function getNumeroDocumento()
    {
        return $this->numero_documento;
    }

    /**
     * Get the [registrado] column value.
     *
     * @return boolean
     */
    public function getRegistrado()
    {
        return $this->registrado;
    }

    /**
     * Get the [registrado] column value.
     *
     * @return boolean
     */
    public function isRegistrado()
    {
        return $this->getRegistrado();
    }

    /**
     * Get the [valor_original] column value.
     *
     * @return string
     */
    public function getValorOriginal()
    {
        return $this->valor_original;
    }

    /**
     * Get the [valor_multa] column value.
     *
     * @return string
     */
    public function getValorMulta()
    {
        return $this->valor_multa;
    }

    /**
     * Get the [valor_juros] column value.
     *
     * @return string
     */
    public function getValorJuros()
    {
        return $this->valor_juros;
    }

    /**
     * Get the [valor_desconto] column value.
     *
     * @return string
     */
    public function getValorDesconto()
    {
        return $this->valor_desconto;
    }

    /**
     * Get the [valor_pago] column value.
     *
     * @return string
     */
    public function getValorPago()
    {
        return $this->valor_pago;
    }

    /**
     * Get the [linha_digitavel] column value.
     *
     * @return string
     */
    public function getLinhaDigitavel()
    {
        return $this->linha_digitavel;
    }

    /**
     * Get the [optionally formatted] temporal [data_pagamento] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataPagamento($format = NULL)
    {
        if ($format === null) {
            return $this->data_pagamento;
        } else {
            return $this->data_pagamento instanceof \DateTimeInterface ? $this->data_pagamento->format($format) : null;
        }
    }

    /**
     * Get the [esta_pago] column value.
     *
     * @return boolean
     */
    public function getEstaPago()
    {
        return $this->esta_pago;
    }

    /**
     * Get the [esta_pago] column value.
     *
     * @return boolean
     */
    public function isEstaPago()
    {
        return $this->getEstaPago();
    }

    /**
     * Get the [valor_abatimento] column value.
     *
     * @return string
     */
    public function getValorAbatimento()
    {
        return $this->valor_abatimento;
    }

    /**
     * Get the [valor_iof] column value.
     *
     * @return string
     */
    public function getValorIof()
    {
        return $this->valor_iof;
    }

    /**
     * Get the [valor_liquido] column value.
     *
     * @return string
     */
    public function getValorLiquido()
    {
        return $this->valor_liquido;
    }

    /**
     * Get the [valor_outras_despesas] column value.
     *
     * @return string
     */
    public function getValorOutrasDespesas()
    {
        return $this->valor_outras_despesas;
    }

    /**
     * Get the [valor_outros_creditos] column value.
     *
     * @return string
     */
    public function getValorOutrosCreditos()
    {
        return $this->valor_outros_creditos;
    }

    /**
     * Get the [nota_scm_gerada] column value.
     *
     * @return boolean
     */
    public function getNotaScmGerada()
    {
        return $this->nota_scm_gerada;
    }

    /**
     * Get the [nota_scm_gerada] column value.
     *
     * @return boolean
     */
    public function isNotaScmGerada()
    {
        return $this->getNotaScmGerada();
    }

    /**
     * Get the [nota_scm_cancelada] column value.
     *
     * @return boolean
     */
    public function getNotaScmCancelada()
    {
        return $this->nota_scm_cancelada;
    }

    /**
     * Get the [nota_scm_cancelada] column value.
     *
     * @return boolean
     */
    public function isNotaScmCancelada()
    {
        return $this->getNotaScmCancelada();
    }

    /**
     * Get the [base_calculo_scm] column value.
     *
     * @return string
     */
    public function getBaseCalculoScm()
    {
        return $this->base_calculo_scm;
    }

    /**
     * Get the [base_calculo_sva] column value.
     *
     * @return string
     */
    public function getBaseCalculoSva()
    {
        return $this->base_calculo_sva;
    }

    /**
     * Get the [nota_sva_gerada] column value.
     *
     * @return boolean
     */
    public function getNotaSvaGerada()
    {
        return $this->nota_sva_gerada;
    }

    /**
     * Get the [nota_sva_gerada] column value.
     *
     * @return boolean
     */
    public function isNotaSvaGerada()
    {
        return $this->getNotaSvaGerada();
    }

    /**
     * Get the [nota_sva_cancelada] column value.
     *
     * @return boolean
     */
    public function getNotaSvaCancelada()
    {
        return $this->nota_sva_cancelada;
    }

    /**
     * Get the [nota_sva_cancelada] column value.
     *
     * @return boolean
     */
    public function isNotaSvaCancelada()
    {
        return $this->getNotaSvaCancelada();
    }

    /**
     * Get the [tipo_conta] column value.
     *
     * @return string
     */
    public function getTipoConta()
    {
        return $this->tipo_conta;
    }

    /**
     * Get the [baixa_id] column value.
     *
     * @return int
     */
    public function getBaixaId()
    {
        return $this->baixa_id;
    }

    /**
     * Get the [baixa_estorno_id] column value.
     *
     * @return int
     */
    public function getBaixaEstornoId()
    {
        return $this->baixa_estorno_id;
    }

    /**
     * Get the [fornecedor_id] column value.
     *
     * @return int
     */
    public function getFornecedorId()
    {
        return $this->fornecedor_id;
    }

    /**
     * Set the value of [idboleto] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setIdboleto($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idboleto !== $v) {
            $this->idboleto = $v;
            $this->modifiedColumns[BoletoTableMap::COL_IDBOLETO] = true;
        }

        return $this;
    } // setIdboleto()

    /**
     * Set the value of [valor] column.
     *
     * @param double $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValor($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->valor !== $v) {
            $this->valor = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR] = true;
        }

        return $this;
    } // setValor()

    /**
     * Sets the value of [vencimento] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setVencimento($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->vencimento !== null || $dt !== null) {
            if ($this->vencimento === null || $dt === null || $dt->format("Y-m-d") !== $this->vencimento->format("Y-m-d")) {
                $this->vencimento = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BoletoTableMap::COL_VENCIMENTO] = true;
            }
        } // if either are not null

        return $this;
    } // setVencimento()

    /**
     * Set the value of [cliente_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setClienteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cliente_id !== $v) {
            $this->cliente_id = $v;
            $this->modifiedColumns[BoletoTableMap::COL_CLIENTE_ID] = true;
        }

        if ($this->aCliente !== null && $this->aCliente->getIdcliente() !== $v) {
            $this->aCliente = null;
        }

        return $this;
    } // setClienteId()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BoletoTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BoletoTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[BoletoTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Set the value of [import_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setImportId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->import_id !== $v) {
            $this->import_id = $v;
            $this->modifiedColumns[BoletoTableMap::COL_IMPORT_ID] = true;
        }

        return $this;
    } // setImportId()

    /**
     * Set the value of [competencia_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setCompetenciaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->competencia_id !== $v) {
            $this->competencia_id = $v;
            $this->modifiedColumns[BoletoTableMap::COL_COMPETENCIA_ID] = true;
        }

        if ($this->aCompetencia !== null && $this->aCompetencia->getId() !== $v) {
            $this->aCompetencia = null;
        }

        return $this;
    } // setCompetenciaId()

    /**
     * Set the value of [nosso_numero] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setNossoNumero($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nosso_numero !== $v) {
            $this->nosso_numero = $v;
            $this->modifiedColumns[BoletoTableMap::COL_NOSSO_NUMERO] = true;
        }

        return $this;
    } // setNossoNumero()

    /**
     * Set the value of [numero_documento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setNumeroDocumento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->numero_documento !== $v) {
            $this->numero_documento = $v;
            $this->modifiedColumns[BoletoTableMap::COL_NUMERO_DOCUMENTO] = true;
        }

        return $this;
    } // setNumeroDocumento()

    /**
     * Sets the value of the [registrado] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setRegistrado($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->registrado !== $v) {
            $this->registrado = $v;
            $this->modifiedColumns[BoletoTableMap::COL_REGISTRADO] = true;
        }

        return $this;
    } // setRegistrado()

    /**
     * Set the value of [valor_original] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorOriginal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_original !== $v) {
            $this->valor_original = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_ORIGINAL] = true;
        }

        return $this;
    } // setValorOriginal()

    /**
     * Set the value of [valor_multa] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorMulta($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_multa !== $v) {
            $this->valor_multa = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_MULTA] = true;
        }

        return $this;
    } // setValorMulta()

    /**
     * Set the value of [valor_juros] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorJuros($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_juros !== $v) {
            $this->valor_juros = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_JUROS] = true;
        }

        return $this;
    } // setValorJuros()

    /**
     * Set the value of [valor_desconto] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorDesconto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_desconto !== $v) {
            $this->valor_desconto = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_DESCONTO] = true;
        }

        return $this;
    } // setValorDesconto()

    /**
     * Set the value of [valor_pago] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorPago($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_pago !== $v) {
            $this->valor_pago = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_PAGO] = true;
        }

        return $this;
    } // setValorPago()

    /**
     * Set the value of [linha_digitavel] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setLinhaDigitavel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->linha_digitavel !== $v) {
            $this->linha_digitavel = $v;
            $this->modifiedColumns[BoletoTableMap::COL_LINHA_DIGITAVEL] = true;
        }

        return $this;
    } // setLinhaDigitavel()

    /**
     * Sets the value of [data_pagamento] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setDataPagamento($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_pagamento !== null || $dt !== null) {
            if ($this->data_pagamento === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_pagamento->format("Y-m-d H:i:s.u")) {
                $this->data_pagamento = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BoletoTableMap::COL_DATA_PAGAMENTO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataPagamento()

    /**
     * Sets the value of the [esta_pago] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setEstaPago($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->esta_pago !== $v) {
            $this->esta_pago = $v;
            $this->modifiedColumns[BoletoTableMap::COL_ESTA_PAGO] = true;
        }

        return $this;
    } // setEstaPago()

    /**
     * Set the value of [valor_abatimento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorAbatimento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_abatimento !== $v) {
            $this->valor_abatimento = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_ABATIMENTO] = true;
        }

        return $this;
    } // setValorAbatimento()

    /**
     * Set the value of [valor_iof] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorIof($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_iof !== $v) {
            $this->valor_iof = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_IOF] = true;
        }

        return $this;
    } // setValorIof()

    /**
     * Set the value of [valor_liquido] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorLiquido($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_liquido !== $v) {
            $this->valor_liquido = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_LIQUIDO] = true;
        }

        return $this;
    } // setValorLiquido()

    /**
     * Set the value of [valor_outras_despesas] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorOutrasDespesas($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_outras_despesas !== $v) {
            $this->valor_outras_despesas = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS] = true;
        }

        return $this;
    } // setValorOutrasDespesas()

    /**
     * Set the value of [valor_outros_creditos] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setValorOutrosCreditos($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_outros_creditos !== $v) {
            $this->valor_outros_creditos = $v;
            $this->modifiedColumns[BoletoTableMap::COL_VALOR_OUTROS_CREDITOS] = true;
        }

        return $this;
    } // setValorOutrosCreditos()

    /**
     * Sets the value of the [nota_scm_gerada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setNotaScmGerada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->nota_scm_gerada !== $v) {
            $this->nota_scm_gerada = $v;
            $this->modifiedColumns[BoletoTableMap::COL_NOTA_SCM_GERADA] = true;
        }

        return $this;
    } // setNotaScmGerada()

    /**
     * Sets the value of the [nota_scm_cancelada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setNotaScmCancelada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->nota_scm_cancelada !== $v) {
            $this->nota_scm_cancelada = $v;
            $this->modifiedColumns[BoletoTableMap::COL_NOTA_SCM_CANCELADA] = true;
        }

        return $this;
    } // setNotaScmCancelada()

    /**
     * Set the value of [base_calculo_scm] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setBaseCalculoScm($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->base_calculo_scm !== $v) {
            $this->base_calculo_scm = $v;
            $this->modifiedColumns[BoletoTableMap::COL_BASE_CALCULO_SCM] = true;
        }

        return $this;
    } // setBaseCalculoScm()

    /**
     * Set the value of [base_calculo_sva] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setBaseCalculoSva($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->base_calculo_sva !== $v) {
            $this->base_calculo_sva = $v;
            $this->modifiedColumns[BoletoTableMap::COL_BASE_CALCULO_SVA] = true;
        }

        return $this;
    } // setBaseCalculoSva()

    /**
     * Sets the value of the [nota_sva_gerada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setNotaSvaGerada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->nota_sva_gerada !== $v) {
            $this->nota_sva_gerada = $v;
            $this->modifiedColumns[BoletoTableMap::COL_NOTA_SVA_GERADA] = true;
        }

        return $this;
    } // setNotaSvaGerada()

    /**
     * Sets the value of the [nota_sva_cancelada] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setNotaSvaCancelada($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->nota_sva_cancelada !== $v) {
            $this->nota_sva_cancelada = $v;
            $this->modifiedColumns[BoletoTableMap::COL_NOTA_SVA_CANCELADA] = true;
        }

        return $this;
    } // setNotaSvaCancelada()

    /**
     * Set the value of [tipo_conta] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setTipoConta($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_conta !== $v) {
            $this->tipo_conta = $v;
            $this->modifiedColumns[BoletoTableMap::COL_TIPO_CONTA] = true;
        }

        return $this;
    } // setTipoConta()

    /**
     * Set the value of [baixa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setBaixaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->baixa_id !== $v) {
            $this->baixa_id = $v;
            $this->modifiedColumns[BoletoTableMap::COL_BAIXA_ID] = true;
        }

        if ($this->aBaixa !== null && $this->aBaixa->getIdbaixa() !== $v) {
            $this->aBaixa = null;
        }

        return $this;
    } // setBaixaId()

    /**
     * Set the value of [baixa_estorno_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setBaixaEstornoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->baixa_estorno_id !== $v) {
            $this->baixa_estorno_id = $v;
            $this->modifiedColumns[BoletoTableMap::COL_BAIXA_ESTORNO_ID] = true;
        }

        if ($this->aBaixaEstorno !== null && $this->aBaixaEstorno->getIdbaixaEstorno() !== $v) {
            $this->aBaixaEstorno = null;
        }

        return $this;
    } // setBaixaEstornoId()

    /**
     * Set the value of [fornecedor_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function setFornecedorId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->fornecedor_id !== $v) {
            $this->fornecedor_id = $v;
            $this->modifiedColumns[BoletoTableMap::COL_FORNECEDOR_ID] = true;
        }

        if ($this->aFornecedor !== null && $this->aFornecedor->getIdfornecedor() !== $v) {
            $this->aFornecedor = null;
        }

        return $this;
    } // setFornecedorId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->registrado !== false) {
                return false;
            }

            if ($this->valor_original !== '0.00') {
                return false;
            }

            if ($this->valor_multa !== '0.00') {
                return false;
            }

            if ($this->valor_juros !== '0.00') {
                return false;
            }

            if ($this->valor_desconto !== '0.00') {
                return false;
            }

            if ($this->valor_pago !== '0.00') {
                return false;
            }

            if ($this->esta_pago !== false) {
                return false;
            }

            if ($this->valor_abatimento !== '0.00') {
                return false;
            }

            if ($this->valor_iof !== '0.00') {
                return false;
            }

            if ($this->valor_liquido !== '0.00') {
                return false;
            }

            if ($this->valor_outras_despesas !== '0.00') {
                return false;
            }

            if ($this->valor_outros_creditos !== '0.00') {
                return false;
            }

            if ($this->nota_scm_gerada !== false) {
                return false;
            }

            if ($this->nota_scm_cancelada !== false) {
                return false;
            }

            if ($this->base_calculo_scm !== '0.00') {
                return false;
            }

            if ($this->base_calculo_sva !== '0.00') {
                return false;
            }

            if ($this->nota_sva_gerada !== false) {
                return false;
            }

            if ($this->nota_sva_cancelada !== false) {
                return false;
            }

            if ($this->tipo_conta !== 'a_receber') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : BoletoTableMap::translateFieldName('Idboleto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idboleto = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : BoletoTableMap::translateFieldName('Valor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : BoletoTableMap::translateFieldName('Vencimento', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->vencimento = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : BoletoTableMap::translateFieldName('ClienteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cliente_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : BoletoTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : BoletoTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : BoletoTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : BoletoTableMap::translateFieldName('ImportId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->import_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : BoletoTableMap::translateFieldName('CompetenciaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->competencia_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : BoletoTableMap::translateFieldName('NossoNumero', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nosso_numero = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : BoletoTableMap::translateFieldName('NumeroDocumento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->numero_documento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : BoletoTableMap::translateFieldName('Registrado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->registrado = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : BoletoTableMap::translateFieldName('ValorOriginal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_original = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : BoletoTableMap::translateFieldName('ValorMulta', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_multa = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : BoletoTableMap::translateFieldName('ValorJuros', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_juros = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : BoletoTableMap::translateFieldName('ValorDesconto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_desconto = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : BoletoTableMap::translateFieldName('ValorPago', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_pago = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : BoletoTableMap::translateFieldName('LinhaDigitavel', TableMap::TYPE_PHPNAME, $indexType)];
            $this->linha_digitavel = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : BoletoTableMap::translateFieldName('DataPagamento', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_pagamento = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : BoletoTableMap::translateFieldName('EstaPago', TableMap::TYPE_PHPNAME, $indexType)];
            $this->esta_pago = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : BoletoTableMap::translateFieldName('ValorAbatimento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_abatimento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : BoletoTableMap::translateFieldName('ValorIof', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_iof = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : BoletoTableMap::translateFieldName('ValorLiquido', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_liquido = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : BoletoTableMap::translateFieldName('ValorOutrasDespesas', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_outras_despesas = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : BoletoTableMap::translateFieldName('ValorOutrosCreditos', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_outros_creditos = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : BoletoTableMap::translateFieldName('NotaScmGerada', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nota_scm_gerada = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : BoletoTableMap::translateFieldName('NotaScmCancelada', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nota_scm_cancelada = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : BoletoTableMap::translateFieldName('BaseCalculoScm', TableMap::TYPE_PHPNAME, $indexType)];
            $this->base_calculo_scm = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : BoletoTableMap::translateFieldName('BaseCalculoSva', TableMap::TYPE_PHPNAME, $indexType)];
            $this->base_calculo_sva = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : BoletoTableMap::translateFieldName('NotaSvaGerada', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nota_sva_gerada = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : BoletoTableMap::translateFieldName('NotaSvaCancelada', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nota_sva_cancelada = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : BoletoTableMap::translateFieldName('TipoConta', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_conta = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : BoletoTableMap::translateFieldName('BaixaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->baixa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : BoletoTableMap::translateFieldName('BaixaEstornoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->baixa_estorno_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : BoletoTableMap::translateFieldName('FornecedorId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fornecedor_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 35; // 35 = BoletoTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Boleto'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCliente !== null && $this->cliente_id !== $this->aCliente->getIdcliente()) {
            $this->aCliente = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
        if ($this->aCompetencia !== null && $this->competencia_id !== $this->aCompetencia->getId()) {
            $this->aCompetencia = null;
        }
        if ($this->aBaixa !== null && $this->baixa_id !== $this->aBaixa->getIdbaixa()) {
            $this->aBaixa = null;
        }
        if ($this->aBaixaEstorno !== null && $this->baixa_estorno_id !== $this->aBaixaEstorno->getIdbaixaEstorno()) {
            $this->aBaixaEstorno = null;
        }
        if ($this->aFornecedor !== null && $this->fornecedor_id !== $this->aFornecedor->getIdfornecedor()) {
            $this->aFornecedor = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BoletoTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildBoletoQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBaixaEstorno = null;
            $this->aBaixa = null;
            $this->aCliente = null;
            $this->aCompetencia = null;
            $this->aFornecedor = null;
            $this->aUsuario = null;
            $this->collBoletoBaixaHistoricos = null;

            $this->collBoletoItems = null;

            $this->collDadosFiscals = null;

            $this->collDadosFiscalItems = null;

            $this->collLancamentosBoletoss = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Boleto::setDeleted()
     * @see Boleto::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildBoletoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BoletoTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBaixaEstorno !== null) {
                if ($this->aBaixaEstorno->isModified() || $this->aBaixaEstorno->isNew()) {
                    $affectedRows += $this->aBaixaEstorno->save($con);
                }
                $this->setBaixaEstorno($this->aBaixaEstorno);
            }

            if ($this->aBaixa !== null) {
                if ($this->aBaixa->isModified() || $this->aBaixa->isNew()) {
                    $affectedRows += $this->aBaixa->save($con);
                }
                $this->setBaixa($this->aBaixa);
            }

            if ($this->aCliente !== null) {
                if ($this->aCliente->isModified() || $this->aCliente->isNew()) {
                    $affectedRows += $this->aCliente->save($con);
                }
                $this->setCliente($this->aCliente);
            }

            if ($this->aCompetencia !== null) {
                if ($this->aCompetencia->isModified() || $this->aCompetencia->isNew()) {
                    $affectedRows += $this->aCompetencia->save($con);
                }
                $this->setCompetencia($this->aCompetencia);
            }

            if ($this->aFornecedor !== null) {
                if ($this->aFornecedor->isModified() || $this->aFornecedor->isNew()) {
                    $affectedRows += $this->aFornecedor->save($con);
                }
                $this->setFornecedor($this->aFornecedor);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->boletoBaixaHistoricosScheduledForDeletion !== null) {
                if (!$this->boletoBaixaHistoricosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery::create()
                        ->filterByPrimaryKeys($this->boletoBaixaHistoricosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->boletoBaixaHistoricosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletoBaixaHistoricos !== null) {
                foreach ($this->collBoletoBaixaHistoricos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->boletoItemsScheduledForDeletion !== null) {
                if (!$this->boletoItemsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\BoletoItemQuery::create()
                        ->filterByPrimaryKeys($this->boletoItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->boletoItemsScheduledForDeletion = null;
                }
            }

            if ($this->collBoletoItems !== null) {
                foreach ($this->collBoletoItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dadosFiscalsScheduledForDeletion !== null) {
                if (!$this->dadosFiscalsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\DadosFiscalQuery::create()
                        ->filterByPrimaryKeys($this->dadosFiscalsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dadosFiscalsScheduledForDeletion = null;
                }
            }

            if ($this->collDadosFiscals !== null) {
                foreach ($this->collDadosFiscals as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dadosFiscalItemsScheduledForDeletion !== null) {
                if (!$this->dadosFiscalItemsScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\DadosFiscalItemQuery::create()
                        ->filterByPrimaryKeys($this->dadosFiscalItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dadosFiscalItemsScheduledForDeletion = null;
                }
            }

            if ($this->collDadosFiscalItems !== null) {
                foreach ($this->collDadosFiscalItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lancamentosBoletossScheduledForDeletion !== null) {
                if (!$this->lancamentosBoletossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\LancamentosBoletosQuery::create()
                        ->filterByPrimaryKeys($this->lancamentosBoletossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lancamentosBoletossScheduledForDeletion = null;
                }
            }

            if ($this->collLancamentosBoletoss !== null) {
                foreach ($this->collLancamentosBoletoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[BoletoTableMap::COL_IDBOLETO] = true;
        if (null !== $this->idboleto) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BoletoTableMap::COL_IDBOLETO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BoletoTableMap::COL_IDBOLETO)) {
            $modifiedColumns[':p' . $index++]  = 'idboleto';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR)) {
            $modifiedColumns[':p' . $index++]  = 'valor';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VENCIMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'vencimento';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_CLIENTE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cliente_id';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_IMPORT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'import_id';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_COMPETENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'competencia_id';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOSSO_NUMERO)) {
            $modifiedColumns[':p' . $index++]  = 'nosso_numero';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NUMERO_DOCUMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'numero_documento';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_REGISTRADO)) {
            $modifiedColumns[':p' . $index++]  = 'registrado';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_ORIGINAL)) {
            $modifiedColumns[':p' . $index++]  = 'valor_original';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_MULTA)) {
            $modifiedColumns[':p' . $index++]  = 'valor_multa';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_JUROS)) {
            $modifiedColumns[':p' . $index++]  = 'valor_juros';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_DESCONTO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_desconto';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_PAGO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_pago';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_LINHA_DIGITAVEL)) {
            $modifiedColumns[':p' . $index++]  = 'linha_digitavel';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_DATA_PAGAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'data_pagamento';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_ESTA_PAGO)) {
            $modifiedColumns[':p' . $index++]  = 'esta_pago';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_ABATIMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_abatimento';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_IOF)) {
            $modifiedColumns[':p' . $index++]  = 'valor_iof';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_LIQUIDO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_liquido';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS)) {
            $modifiedColumns[':p' . $index++]  = 'valor_outras_despesas';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_OUTROS_CREDITOS)) {
            $modifiedColumns[':p' . $index++]  = 'valor_outros_creditos';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOTA_SCM_GERADA)) {
            $modifiedColumns[':p' . $index++]  = 'nota_scm_gerada';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOTA_SCM_CANCELADA)) {
            $modifiedColumns[':p' . $index++]  = 'nota_scm_cancelada';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_BASE_CALCULO_SCM)) {
            $modifiedColumns[':p' . $index++]  = 'base_calculo_scm';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_BASE_CALCULO_SVA)) {
            $modifiedColumns[':p' . $index++]  = 'base_calculo_sva';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOTA_SVA_GERADA)) {
            $modifiedColumns[':p' . $index++]  = 'nota_sva_gerada';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOTA_SVA_CANCELADA)) {
            $modifiedColumns[':p' . $index++]  = 'nota_sva_cancelada';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_TIPO_CONTA)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_conta';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_BAIXA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'baixa_id';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_BAIXA_ESTORNO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'baixa_estorno_id';
        }
        if ($this->isColumnModified(BoletoTableMap::COL_FORNECEDOR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'fornecedor_id';
        }

        $sql = sprintf(
            'INSERT INTO boleto (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idboleto':
                        $stmt->bindValue($identifier, $this->idboleto, PDO::PARAM_INT);
                        break;
                    case 'valor':
                        $stmt->bindValue($identifier, $this->valor, PDO::PARAM_STR);
                        break;
                    case 'vencimento':
                        $stmt->bindValue($identifier, $this->vencimento ? $this->vencimento->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'cliente_id':
                        $stmt->bindValue($identifier, $this->cliente_id, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'import_id':
                        $stmt->bindValue($identifier, $this->import_id, PDO::PARAM_INT);
                        break;
                    case 'competencia_id':
                        $stmt->bindValue($identifier, $this->competencia_id, PDO::PARAM_INT);
                        break;
                    case 'nosso_numero':
                        $stmt->bindValue($identifier, $this->nosso_numero, PDO::PARAM_STR);
                        break;
                    case 'numero_documento':
                        $stmt->bindValue($identifier, $this->numero_documento, PDO::PARAM_STR);
                        break;
                    case 'registrado':
                        $stmt->bindValue($identifier, (int) $this->registrado, PDO::PARAM_INT);
                        break;
                    case 'valor_original':
                        $stmt->bindValue($identifier, $this->valor_original, PDO::PARAM_STR);
                        break;
                    case 'valor_multa':
                        $stmt->bindValue($identifier, $this->valor_multa, PDO::PARAM_STR);
                        break;
                    case 'valor_juros':
                        $stmt->bindValue($identifier, $this->valor_juros, PDO::PARAM_STR);
                        break;
                    case 'valor_desconto':
                        $stmt->bindValue($identifier, $this->valor_desconto, PDO::PARAM_STR);
                        break;
                    case 'valor_pago':
                        $stmt->bindValue($identifier, $this->valor_pago, PDO::PARAM_STR);
                        break;
                    case 'linha_digitavel':
                        $stmt->bindValue($identifier, $this->linha_digitavel, PDO::PARAM_STR);
                        break;
                    case 'data_pagamento':
                        $stmt->bindValue($identifier, $this->data_pagamento ? $this->data_pagamento->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'esta_pago':
                        $stmt->bindValue($identifier, (int) $this->esta_pago, PDO::PARAM_INT);
                        break;
                    case 'valor_abatimento':
                        $stmt->bindValue($identifier, $this->valor_abatimento, PDO::PARAM_STR);
                        break;
                    case 'valor_iof':
                        $stmt->bindValue($identifier, $this->valor_iof, PDO::PARAM_STR);
                        break;
                    case 'valor_liquido':
                        $stmt->bindValue($identifier, $this->valor_liquido, PDO::PARAM_STR);
                        break;
                    case 'valor_outras_despesas':
                        $stmt->bindValue($identifier, $this->valor_outras_despesas, PDO::PARAM_STR);
                        break;
                    case 'valor_outros_creditos':
                        $stmt->bindValue($identifier, $this->valor_outros_creditos, PDO::PARAM_STR);
                        break;
                    case 'nota_scm_gerada':
                        $stmt->bindValue($identifier, (int) $this->nota_scm_gerada, PDO::PARAM_INT);
                        break;
                    case 'nota_scm_cancelada':
                        $stmt->bindValue($identifier, (int) $this->nota_scm_cancelada, PDO::PARAM_INT);
                        break;
                    case 'base_calculo_scm':
                        $stmt->bindValue($identifier, $this->base_calculo_scm, PDO::PARAM_STR);
                        break;
                    case 'base_calculo_sva':
                        $stmt->bindValue($identifier, $this->base_calculo_sva, PDO::PARAM_STR);
                        break;
                    case 'nota_sva_gerada':
                        $stmt->bindValue($identifier, (int) $this->nota_sva_gerada, PDO::PARAM_INT);
                        break;
                    case 'nota_sva_cancelada':
                        $stmt->bindValue($identifier, (int) $this->nota_sva_cancelada, PDO::PARAM_INT);
                        break;
                    case 'tipo_conta':
                        $stmt->bindValue($identifier, $this->tipo_conta, PDO::PARAM_STR);
                        break;
                    case 'baixa_id':
                        $stmt->bindValue($identifier, $this->baixa_id, PDO::PARAM_INT);
                        break;
                    case 'baixa_estorno_id':
                        $stmt->bindValue($identifier, $this->baixa_estorno_id, PDO::PARAM_INT);
                        break;
                    case 'fornecedor_id':
                        $stmt->bindValue($identifier, $this->fornecedor_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdboleto($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BoletoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdboleto();
                break;
            case 1:
                return $this->getValor();
                break;
            case 2:
                return $this->getVencimento();
                break;
            case 3:
                return $this->getClienteId();
                break;
            case 4:
                return $this->getDataCadastro();
                break;
            case 5:
                return $this->getDataAlterado();
                break;
            case 6:
                return $this->getUsuarioAlterado();
                break;
            case 7:
                return $this->getImportId();
                break;
            case 8:
                return $this->getCompetenciaId();
                break;
            case 9:
                return $this->getNossoNumero();
                break;
            case 10:
                return $this->getNumeroDocumento();
                break;
            case 11:
                return $this->getRegistrado();
                break;
            case 12:
                return $this->getValorOriginal();
                break;
            case 13:
                return $this->getValorMulta();
                break;
            case 14:
                return $this->getValorJuros();
                break;
            case 15:
                return $this->getValorDesconto();
                break;
            case 16:
                return $this->getValorPago();
                break;
            case 17:
                return $this->getLinhaDigitavel();
                break;
            case 18:
                return $this->getDataPagamento();
                break;
            case 19:
                return $this->getEstaPago();
                break;
            case 20:
                return $this->getValorAbatimento();
                break;
            case 21:
                return $this->getValorIof();
                break;
            case 22:
                return $this->getValorLiquido();
                break;
            case 23:
                return $this->getValorOutrasDespesas();
                break;
            case 24:
                return $this->getValorOutrosCreditos();
                break;
            case 25:
                return $this->getNotaScmGerada();
                break;
            case 26:
                return $this->getNotaScmCancelada();
                break;
            case 27:
                return $this->getBaseCalculoScm();
                break;
            case 28:
                return $this->getBaseCalculoSva();
                break;
            case 29:
                return $this->getNotaSvaGerada();
                break;
            case 30:
                return $this->getNotaSvaCancelada();
                break;
            case 31:
                return $this->getTipoConta();
                break;
            case 32:
                return $this->getBaixaId();
                break;
            case 33:
                return $this->getBaixaEstornoId();
                break;
            case 34:
                return $this->getFornecedorId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Boleto'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Boleto'][$this->hashCode()] = true;
        $keys = BoletoTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdboleto(),
            $keys[1] => $this->getValor(),
            $keys[2] => $this->getVencimento(),
            $keys[3] => $this->getClienteId(),
            $keys[4] => $this->getDataCadastro(),
            $keys[5] => $this->getDataAlterado(),
            $keys[6] => $this->getUsuarioAlterado(),
            $keys[7] => $this->getImportId(),
            $keys[8] => $this->getCompetenciaId(),
            $keys[9] => $this->getNossoNumero(),
            $keys[10] => $this->getNumeroDocumento(),
            $keys[11] => $this->getRegistrado(),
            $keys[12] => $this->getValorOriginal(),
            $keys[13] => $this->getValorMulta(),
            $keys[14] => $this->getValorJuros(),
            $keys[15] => $this->getValorDesconto(),
            $keys[16] => $this->getValorPago(),
            $keys[17] => $this->getLinhaDigitavel(),
            $keys[18] => $this->getDataPagamento(),
            $keys[19] => $this->getEstaPago(),
            $keys[20] => $this->getValorAbatimento(),
            $keys[21] => $this->getValorIof(),
            $keys[22] => $this->getValorLiquido(),
            $keys[23] => $this->getValorOutrasDespesas(),
            $keys[24] => $this->getValorOutrosCreditos(),
            $keys[25] => $this->getNotaScmGerada(),
            $keys[26] => $this->getNotaScmCancelada(),
            $keys[27] => $this->getBaseCalculoScm(),
            $keys[28] => $this->getBaseCalculoSva(),
            $keys[29] => $this->getNotaSvaGerada(),
            $keys[30] => $this->getNotaSvaCancelada(),
            $keys[31] => $this->getTipoConta(),
            $keys[32] => $this->getBaixaId(),
            $keys[33] => $this->getBaixaEstornoId(),
            $keys[34] => $this->getFornecedorId(),
        );
        if ($result[$keys[2]] instanceof \DateTime) {
            $result[$keys[2]] = $result[$keys[2]]->format('c');
        }

        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTime) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBaixaEstorno) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixaEstorno';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixa_estorno';
                        break;
                    default:
                        $key = 'BaixaEstorno';
                }

                $result[$key] = $this->aBaixaEstorno->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBaixa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixa';
                        break;
                    default:
                        $key = 'Baixa';
                }

                $result[$key] = $this->aBaixa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCliente) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'cliente';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cliente';
                        break;
                    default:
                        $key = 'Cliente';
                }

                $result[$key] = $this->aCliente->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCompetencia) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'competencia';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'competencia';
                        break;
                    default:
                        $key = 'Competencia';
                }

                $result[$key] = $this->aCompetencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aFornecedor) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'fornecedor';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'fornecedor';
                        break;
                    default:
                        $key = 'Fornecedor';
                }

                $result[$key] = $this->aFornecedor->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBoletoBaixaHistoricos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoBaixaHistoricos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_baixa_historicos';
                        break;
                    default:
                        $key = 'BoletoBaixaHistoricos';
                }

                $result[$key] = $this->collBoletoBaixaHistoricos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collBoletoItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_items';
                        break;
                    default:
                        $key = 'BoletoItems';
                }

                $result[$key] = $this->collBoletoItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDadosFiscals) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dadosFiscals';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'dados_fiscals';
                        break;
                    default:
                        $key = 'DadosFiscals';
                }

                $result[$key] = $this->collDadosFiscals->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDadosFiscalItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dadosFiscalItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'dados_fiscal_items';
                        break;
                    default:
                        $key = 'DadosFiscalItems';
                }

                $result[$key] = $this->collDadosFiscalItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLancamentosBoletoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lancamentosBoletoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lancamentos_boletoss';
                        break;
                    default:
                        $key = 'LancamentosBoletoss';
                }

                $result[$key] = $this->collLancamentosBoletoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Boleto
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BoletoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Boleto
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdboleto($value);
                break;
            case 1:
                $this->setValor($value);
                break;
            case 2:
                $this->setVencimento($value);
                break;
            case 3:
                $this->setClienteId($value);
                break;
            case 4:
                $this->setDataCadastro($value);
                break;
            case 5:
                $this->setDataAlterado($value);
                break;
            case 6:
                $this->setUsuarioAlterado($value);
                break;
            case 7:
                $this->setImportId($value);
                break;
            case 8:
                $this->setCompetenciaId($value);
                break;
            case 9:
                $this->setNossoNumero($value);
                break;
            case 10:
                $this->setNumeroDocumento($value);
                break;
            case 11:
                $this->setRegistrado($value);
                break;
            case 12:
                $this->setValorOriginal($value);
                break;
            case 13:
                $this->setValorMulta($value);
                break;
            case 14:
                $this->setValorJuros($value);
                break;
            case 15:
                $this->setValorDesconto($value);
                break;
            case 16:
                $this->setValorPago($value);
                break;
            case 17:
                $this->setLinhaDigitavel($value);
                break;
            case 18:
                $this->setDataPagamento($value);
                break;
            case 19:
                $this->setEstaPago($value);
                break;
            case 20:
                $this->setValorAbatimento($value);
                break;
            case 21:
                $this->setValorIof($value);
                break;
            case 22:
                $this->setValorLiquido($value);
                break;
            case 23:
                $this->setValorOutrasDespesas($value);
                break;
            case 24:
                $this->setValorOutrosCreditos($value);
                break;
            case 25:
                $this->setNotaScmGerada($value);
                break;
            case 26:
                $this->setNotaScmCancelada($value);
                break;
            case 27:
                $this->setBaseCalculoScm($value);
                break;
            case 28:
                $this->setBaseCalculoSva($value);
                break;
            case 29:
                $this->setNotaSvaGerada($value);
                break;
            case 30:
                $this->setNotaSvaCancelada($value);
                break;
            case 31:
                $this->setTipoConta($value);
                break;
            case 32:
                $this->setBaixaId($value);
                break;
            case 33:
                $this->setBaixaEstornoId($value);
                break;
            case 34:
                $this->setFornecedorId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = BoletoTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdboleto($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setValor($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setVencimento($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setClienteId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDataCadastro($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDataAlterado($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setImportId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setCompetenciaId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setNossoNumero($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setNumeroDocumento($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setRegistrado($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setValorOriginal($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setValorMulta($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setValorJuros($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setValorDesconto($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setValorPago($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setLinhaDigitavel($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setDataPagamento($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setEstaPago($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setValorAbatimento($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setValorIof($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setValorLiquido($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setValorOutrasDespesas($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setValorOutrosCreditos($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setNotaScmGerada($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setNotaScmCancelada($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setBaseCalculoScm($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setBaseCalculoSva($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setNotaSvaGerada($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setNotaSvaCancelada($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setTipoConta($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setBaixaId($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setBaixaEstornoId($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setFornecedorId($arr[$keys[34]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BoletoTableMap::DATABASE_NAME);

        if ($this->isColumnModified(BoletoTableMap::COL_IDBOLETO)) {
            $criteria->add(BoletoTableMap::COL_IDBOLETO, $this->idboleto);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR)) {
            $criteria->add(BoletoTableMap::COL_VALOR, $this->valor);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VENCIMENTO)) {
            $criteria->add(BoletoTableMap::COL_VENCIMENTO, $this->vencimento);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_CLIENTE_ID)) {
            $criteria->add(BoletoTableMap::COL_CLIENTE_ID, $this->cliente_id);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(BoletoTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(BoletoTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(BoletoTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_IMPORT_ID)) {
            $criteria->add(BoletoTableMap::COL_IMPORT_ID, $this->import_id);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_COMPETENCIA_ID)) {
            $criteria->add(BoletoTableMap::COL_COMPETENCIA_ID, $this->competencia_id);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOSSO_NUMERO)) {
            $criteria->add(BoletoTableMap::COL_NOSSO_NUMERO, $this->nosso_numero);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NUMERO_DOCUMENTO)) {
            $criteria->add(BoletoTableMap::COL_NUMERO_DOCUMENTO, $this->numero_documento);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_REGISTRADO)) {
            $criteria->add(BoletoTableMap::COL_REGISTRADO, $this->registrado);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_ORIGINAL)) {
            $criteria->add(BoletoTableMap::COL_VALOR_ORIGINAL, $this->valor_original);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_MULTA)) {
            $criteria->add(BoletoTableMap::COL_VALOR_MULTA, $this->valor_multa);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_JUROS)) {
            $criteria->add(BoletoTableMap::COL_VALOR_JUROS, $this->valor_juros);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_DESCONTO)) {
            $criteria->add(BoletoTableMap::COL_VALOR_DESCONTO, $this->valor_desconto);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_PAGO)) {
            $criteria->add(BoletoTableMap::COL_VALOR_PAGO, $this->valor_pago);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_LINHA_DIGITAVEL)) {
            $criteria->add(BoletoTableMap::COL_LINHA_DIGITAVEL, $this->linha_digitavel);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_DATA_PAGAMENTO)) {
            $criteria->add(BoletoTableMap::COL_DATA_PAGAMENTO, $this->data_pagamento);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_ESTA_PAGO)) {
            $criteria->add(BoletoTableMap::COL_ESTA_PAGO, $this->esta_pago);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_ABATIMENTO)) {
            $criteria->add(BoletoTableMap::COL_VALOR_ABATIMENTO, $this->valor_abatimento);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_IOF)) {
            $criteria->add(BoletoTableMap::COL_VALOR_IOF, $this->valor_iof);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_LIQUIDO)) {
            $criteria->add(BoletoTableMap::COL_VALOR_LIQUIDO, $this->valor_liquido);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS)) {
            $criteria->add(BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS, $this->valor_outras_despesas);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_VALOR_OUTROS_CREDITOS)) {
            $criteria->add(BoletoTableMap::COL_VALOR_OUTROS_CREDITOS, $this->valor_outros_creditos);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOTA_SCM_GERADA)) {
            $criteria->add(BoletoTableMap::COL_NOTA_SCM_GERADA, $this->nota_scm_gerada);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOTA_SCM_CANCELADA)) {
            $criteria->add(BoletoTableMap::COL_NOTA_SCM_CANCELADA, $this->nota_scm_cancelada);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_BASE_CALCULO_SCM)) {
            $criteria->add(BoletoTableMap::COL_BASE_CALCULO_SCM, $this->base_calculo_scm);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_BASE_CALCULO_SVA)) {
            $criteria->add(BoletoTableMap::COL_BASE_CALCULO_SVA, $this->base_calculo_sva);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOTA_SVA_GERADA)) {
            $criteria->add(BoletoTableMap::COL_NOTA_SVA_GERADA, $this->nota_sva_gerada);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_NOTA_SVA_CANCELADA)) {
            $criteria->add(BoletoTableMap::COL_NOTA_SVA_CANCELADA, $this->nota_sva_cancelada);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_TIPO_CONTA)) {
            $criteria->add(BoletoTableMap::COL_TIPO_CONTA, $this->tipo_conta);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_BAIXA_ID)) {
            $criteria->add(BoletoTableMap::COL_BAIXA_ID, $this->baixa_id);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_BAIXA_ESTORNO_ID)) {
            $criteria->add(BoletoTableMap::COL_BAIXA_ESTORNO_ID, $this->baixa_estorno_id);
        }
        if ($this->isColumnModified(BoletoTableMap::COL_FORNECEDOR_ID)) {
            $criteria->add(BoletoTableMap::COL_FORNECEDOR_ID, $this->fornecedor_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildBoletoQuery::create();
        $criteria->add(BoletoTableMap::COL_IDBOLETO, $this->idboleto);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdboleto();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdboleto();
    }

    /**
     * Generic method to set the primary key (idboleto column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdboleto($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdboleto();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Boleto (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setValor($this->getValor());
        $copyObj->setVencimento($this->getVencimento());
        $copyObj->setClienteId($this->getClienteId());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setImportId($this->getImportId());
        $copyObj->setCompetenciaId($this->getCompetenciaId());
        $copyObj->setNossoNumero($this->getNossoNumero());
        $copyObj->setNumeroDocumento($this->getNumeroDocumento());
        $copyObj->setRegistrado($this->getRegistrado());
        $copyObj->setValorOriginal($this->getValorOriginal());
        $copyObj->setValorMulta($this->getValorMulta());
        $copyObj->setValorJuros($this->getValorJuros());
        $copyObj->setValorDesconto($this->getValorDesconto());
        $copyObj->setValorPago($this->getValorPago());
        $copyObj->setLinhaDigitavel($this->getLinhaDigitavel());
        $copyObj->setDataPagamento($this->getDataPagamento());
        $copyObj->setEstaPago($this->getEstaPago());
        $copyObj->setValorAbatimento($this->getValorAbatimento());
        $copyObj->setValorIof($this->getValorIof());
        $copyObj->setValorLiquido($this->getValorLiquido());
        $copyObj->setValorOutrasDespesas($this->getValorOutrasDespesas());
        $copyObj->setValorOutrosCreditos($this->getValorOutrosCreditos());
        $copyObj->setNotaScmGerada($this->getNotaScmGerada());
        $copyObj->setNotaScmCancelada($this->getNotaScmCancelada());
        $copyObj->setBaseCalculoScm($this->getBaseCalculoScm());
        $copyObj->setBaseCalculoSva($this->getBaseCalculoSva());
        $copyObj->setNotaSvaGerada($this->getNotaSvaGerada());
        $copyObj->setNotaSvaCancelada($this->getNotaSvaCancelada());
        $copyObj->setTipoConta($this->getTipoConta());
        $copyObj->setBaixaId($this->getBaixaId());
        $copyObj->setBaixaEstornoId($this->getBaixaEstornoId());
        $copyObj->setFornecedorId($this->getFornecedorId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBoletoBaixaHistoricos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoletoBaixaHistorico($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getBoletoItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoletoItem($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDadosFiscals() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDadosFiscal($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDadosFiscalItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDadosFiscalItem($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLancamentosBoletoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLancamentosBoletos($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdboleto(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Boleto Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBaixaEstorno object.
     *
     * @param  ChildBaixaEstorno $v
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBaixaEstorno(ChildBaixaEstorno $v = null)
    {
        if ($v === null) {
            $this->setBaixaEstornoId(NULL);
        } else {
            $this->setBaixaEstornoId($v->getIdbaixaEstorno());
        }

        $this->aBaixaEstorno = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBaixaEstorno object, it will not be re-added.
        if ($v !== null) {
            $v->addBoleto($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBaixaEstorno object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBaixaEstorno The associated ChildBaixaEstorno object.
     * @throws PropelException
     */
    public function getBaixaEstorno(ConnectionInterface $con = null)
    {
        if ($this->aBaixaEstorno === null && ($this->baixa_estorno_id !== null)) {
            $this->aBaixaEstorno = ChildBaixaEstornoQuery::create()->findPk($this->baixa_estorno_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBaixaEstorno->addBoletos($this);
             */
        }

        return $this->aBaixaEstorno;
    }

    /**
     * Declares an association between this object and a ChildBaixa object.
     *
     * @param  ChildBaixa $v
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBaixa(ChildBaixa $v = null)
    {
        if ($v === null) {
            $this->setBaixaId(NULL);
        } else {
            $this->setBaixaId($v->getIdbaixa());
        }

        $this->aBaixa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBaixa object, it will not be re-added.
        if ($v !== null) {
            $v->addBoleto($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBaixa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBaixa The associated ChildBaixa object.
     * @throws PropelException
     */
    public function getBaixa(ConnectionInterface $con = null)
    {
        if ($this->aBaixa === null && ($this->baixa_id !== null)) {
            $this->aBaixa = ChildBaixaQuery::create()->findPk($this->baixa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBaixa->addBoletos($this);
             */
        }

        return $this->aBaixa;
    }

    /**
     * Declares an association between this object and a ChildCliente object.
     *
     * @param  ChildCliente $v
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCliente(ChildCliente $v = null)
    {
        if ($v === null) {
            $this->setClienteId(NULL);
        } else {
            $this->setClienteId($v->getIdcliente());
        }

        $this->aCliente = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCliente object, it will not be re-added.
        if ($v !== null) {
            $v->addBoleto($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCliente object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCliente The associated ChildCliente object.
     * @throws PropelException
     */
    public function getCliente(ConnectionInterface $con = null)
    {
        if ($this->aCliente === null && ($this->cliente_id !== null)) {
            $this->aCliente = ChildClienteQuery::create()->findPk($this->cliente_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCliente->addBoletos($this);
             */
        }

        return $this->aCliente;
    }

    /**
     * Declares an association between this object and a ChildCompetencia object.
     *
     * @param  ChildCompetencia $v
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCompetencia(ChildCompetencia $v = null)
    {
        if ($v === null) {
            $this->setCompetenciaId(NULL);
        } else {
            $this->setCompetenciaId($v->getId());
        }

        $this->aCompetencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCompetencia object, it will not be re-added.
        if ($v !== null) {
            $v->addBoleto($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCompetencia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCompetencia The associated ChildCompetencia object.
     * @throws PropelException
     */
    public function getCompetencia(ConnectionInterface $con = null)
    {
        if ($this->aCompetencia === null && ($this->competencia_id !== null)) {
            $this->aCompetencia = ChildCompetenciaQuery::create()->findPk($this->competencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCompetencia->addBoletos($this);
             */
        }

        return $this->aCompetencia;
    }

    /**
     * Declares an association between this object and a ChildFornecedor object.
     *
     * @param  ChildFornecedor $v
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFornecedor(ChildFornecedor $v = null)
    {
        if ($v === null) {
            $this->setFornecedorId(NULL);
        } else {
            $this->setFornecedorId($v->getIdfornecedor());
        }

        $this->aFornecedor = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildFornecedor object, it will not be re-added.
        if ($v !== null) {
            $v->addBoleto($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildFornecedor object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildFornecedor The associated ChildFornecedor object.
     * @throws PropelException
     */
    public function getFornecedor(ConnectionInterface $con = null)
    {
        if ($this->aFornecedor === null && ($this->fornecedor_id !== null)) {
            $this->aFornecedor = ChildFornecedorQuery::create()->findPk($this->fornecedor_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aFornecedor->addBoletos($this);
             */
        }

        return $this->aFornecedor;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addBoleto($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addBoletos($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('BoletoBaixaHistorico' == $relationName) {
            return $this->initBoletoBaixaHistoricos();
        }
        if ('BoletoItem' == $relationName) {
            return $this->initBoletoItems();
        }
        if ('DadosFiscal' == $relationName) {
            return $this->initDadosFiscals();
        }
        if ('DadosFiscalItem' == $relationName) {
            return $this->initDadosFiscalItems();
        }
        if ('LancamentosBoletos' == $relationName) {
            return $this->initLancamentosBoletoss();
        }
    }

    /**
     * Clears out the collBoletoBaixaHistoricos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletoBaixaHistoricos()
     */
    public function clearBoletoBaixaHistoricos()
    {
        $this->collBoletoBaixaHistoricos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletoBaixaHistoricos collection loaded partially.
     */
    public function resetPartialBoletoBaixaHistoricos($v = true)
    {
        $this->collBoletoBaixaHistoricosPartial = $v;
    }

    /**
     * Initializes the collBoletoBaixaHistoricos collection.
     *
     * By default this just sets the collBoletoBaixaHistoricos collection to an empty array (like clearcollBoletoBaixaHistoricos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletoBaixaHistoricos($overrideExisting = true)
    {
        if (null !== $this->collBoletoBaixaHistoricos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoBaixaHistoricoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletoBaixaHistoricos = new $collectionClassName;
        $this->collBoletoBaixaHistoricos->setModel('\ImaTelecomBundle\Model\BoletoBaixaHistorico');
    }

    /**
     * Gets an array of ChildBoletoBaixaHistorico objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBoleto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     * @throws PropelException
     */
    public function getBoletoBaixaHistoricos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                // return empty collection
                $this->initBoletoBaixaHistoricos();
            } else {
                $collBoletoBaixaHistoricos = ChildBoletoBaixaHistoricoQuery::create(null, $criteria)
                    ->filterByBoleto($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletoBaixaHistoricosPartial && count($collBoletoBaixaHistoricos)) {
                        $this->initBoletoBaixaHistoricos(false);

                        foreach ($collBoletoBaixaHistoricos as $obj) {
                            if (false == $this->collBoletoBaixaHistoricos->contains($obj)) {
                                $this->collBoletoBaixaHistoricos->append($obj);
                            }
                        }

                        $this->collBoletoBaixaHistoricosPartial = true;
                    }

                    return $collBoletoBaixaHistoricos;
                }

                if ($partial && $this->collBoletoBaixaHistoricos) {
                    foreach ($this->collBoletoBaixaHistoricos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletoBaixaHistoricos[] = $obj;
                        }
                    }
                }

                $this->collBoletoBaixaHistoricos = $collBoletoBaixaHistoricos;
                $this->collBoletoBaixaHistoricosPartial = false;
            }
        }

        return $this->collBoletoBaixaHistoricos;
    }

    /**
     * Sets a collection of ChildBoletoBaixaHistorico objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletoBaixaHistoricos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function setBoletoBaixaHistoricos(Collection $boletoBaixaHistoricos, ConnectionInterface $con = null)
    {
        /** @var ChildBoletoBaixaHistorico[] $boletoBaixaHistoricosToDelete */
        $boletoBaixaHistoricosToDelete = $this->getBoletoBaixaHistoricos(new Criteria(), $con)->diff($boletoBaixaHistoricos);


        $this->boletoBaixaHistoricosScheduledForDeletion = $boletoBaixaHistoricosToDelete;

        foreach ($boletoBaixaHistoricosToDelete as $boletoBaixaHistoricoRemoved) {
            $boletoBaixaHistoricoRemoved->setBoleto(null);
        }

        $this->collBoletoBaixaHistoricos = null;
        foreach ($boletoBaixaHistoricos as $boletoBaixaHistorico) {
            $this->addBoletoBaixaHistorico($boletoBaixaHistorico);
        }

        $this->collBoletoBaixaHistoricos = $boletoBaixaHistoricos;
        $this->collBoletoBaixaHistoricosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BoletoBaixaHistorico objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BoletoBaixaHistorico objects.
     * @throws PropelException
     */
    public function countBoletoBaixaHistoricos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletoBaixaHistoricos());
            }

            $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBoleto($this)
                ->count($con);
        }

        return count($this->collBoletoBaixaHistoricos);
    }

    /**
     * Method called to associate a ChildBoletoBaixaHistorico object to this object
     * through the ChildBoletoBaixaHistorico foreign key attribute.
     *
     * @param  ChildBoletoBaixaHistorico $l ChildBoletoBaixaHistorico
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function addBoletoBaixaHistorico(ChildBoletoBaixaHistorico $l)
    {
        if ($this->collBoletoBaixaHistoricos === null) {
            $this->initBoletoBaixaHistoricos();
            $this->collBoletoBaixaHistoricosPartial = true;
        }

        if (!$this->collBoletoBaixaHistoricos->contains($l)) {
            $this->doAddBoletoBaixaHistorico($l);

            if ($this->boletoBaixaHistoricosScheduledForDeletion and $this->boletoBaixaHistoricosScheduledForDeletion->contains($l)) {
                $this->boletoBaixaHistoricosScheduledForDeletion->remove($this->boletoBaixaHistoricosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to add.
     */
    protected function doAddBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        $this->collBoletoBaixaHistoricos[]= $boletoBaixaHistorico;
        $boletoBaixaHistorico->setBoleto($this);
    }

    /**
     * @param  ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to remove.
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function removeBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        if ($this->getBoletoBaixaHistoricos()->contains($boletoBaixaHistorico)) {
            $pos = $this->collBoletoBaixaHistoricos->search($boletoBaixaHistorico);
            $this->collBoletoBaixaHistoricos->remove($pos);
            if (null === $this->boletoBaixaHistoricosScheduledForDeletion) {
                $this->boletoBaixaHistoricosScheduledForDeletion = clone $this->collBoletoBaixaHistoricos;
                $this->boletoBaixaHistoricosScheduledForDeletion->clear();
            }
            $this->boletoBaixaHistoricosScheduledForDeletion[]= clone $boletoBaixaHistorico;
            $boletoBaixaHistorico->setBoleto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixaEstorno(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('BaixaEstorno', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinLancamentos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Lancamentos', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }

    /**
     * Clears out the collBoletoItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletoItems()
     */
    public function clearBoletoItems()
    {
        $this->collBoletoItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletoItems collection loaded partially.
     */
    public function resetPartialBoletoItems($v = true)
    {
        $this->collBoletoItemsPartial = $v;
    }

    /**
     * Initializes the collBoletoItems collection.
     *
     * By default this just sets the collBoletoItems collection to an empty array (like clearcollBoletoItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletoItems($overrideExisting = true)
    {
        if (null !== $this->collBoletoItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoItemTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletoItems = new $collectionClassName;
        $this->collBoletoItems->setModel('\ImaTelecomBundle\Model\BoletoItem');
    }

    /**
     * Gets an array of ChildBoletoItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBoleto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoletoItem[] List of ChildBoletoItem objects
     * @throws PropelException
     */
    public function getBoletoItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoItemsPartial && !$this->isNew();
        if (null === $this->collBoletoItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletoItems) {
                // return empty collection
                $this->initBoletoItems();
            } else {
                $collBoletoItems = ChildBoletoItemQuery::create(null, $criteria)
                    ->filterByBoleto($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletoItemsPartial && count($collBoletoItems)) {
                        $this->initBoletoItems(false);

                        foreach ($collBoletoItems as $obj) {
                            if (false == $this->collBoletoItems->contains($obj)) {
                                $this->collBoletoItems->append($obj);
                            }
                        }

                        $this->collBoletoItemsPartial = true;
                    }

                    return $collBoletoItems;
                }

                if ($partial && $this->collBoletoItems) {
                    foreach ($this->collBoletoItems as $obj) {
                        if ($obj->isNew()) {
                            $collBoletoItems[] = $obj;
                        }
                    }
                }

                $this->collBoletoItems = $collBoletoItems;
                $this->collBoletoItemsPartial = false;
            }
        }

        return $this->collBoletoItems;
    }

    /**
     * Sets a collection of ChildBoletoItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletoItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function setBoletoItems(Collection $boletoItems, ConnectionInterface $con = null)
    {
        /** @var ChildBoletoItem[] $boletoItemsToDelete */
        $boletoItemsToDelete = $this->getBoletoItems(new Criteria(), $con)->diff($boletoItems);


        $this->boletoItemsScheduledForDeletion = $boletoItemsToDelete;

        foreach ($boletoItemsToDelete as $boletoItemRemoved) {
            $boletoItemRemoved->setBoleto(null);
        }

        $this->collBoletoItems = null;
        foreach ($boletoItems as $boletoItem) {
            $this->addBoletoItem($boletoItem);
        }

        $this->collBoletoItems = $boletoItems;
        $this->collBoletoItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BoletoItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BoletoItem objects.
     * @throws PropelException
     */
    public function countBoletoItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoItemsPartial && !$this->isNew();
        if (null === $this->collBoletoItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletoItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletoItems());
            }

            $query = ChildBoletoItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBoleto($this)
                ->count($con);
        }

        return count($this->collBoletoItems);
    }

    /**
     * Method called to associate a ChildBoletoItem object to this object
     * through the ChildBoletoItem foreign key attribute.
     *
     * @param  ChildBoletoItem $l ChildBoletoItem
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function addBoletoItem(ChildBoletoItem $l)
    {
        if ($this->collBoletoItems === null) {
            $this->initBoletoItems();
            $this->collBoletoItemsPartial = true;
        }

        if (!$this->collBoletoItems->contains($l)) {
            $this->doAddBoletoItem($l);

            if ($this->boletoItemsScheduledForDeletion and $this->boletoItemsScheduledForDeletion->contains($l)) {
                $this->boletoItemsScheduledForDeletion->remove($this->boletoItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoletoItem $boletoItem The ChildBoletoItem object to add.
     */
    protected function doAddBoletoItem(ChildBoletoItem $boletoItem)
    {
        $this->collBoletoItems[]= $boletoItem;
        $boletoItem->setBoleto($this);
    }

    /**
     * @param  ChildBoletoItem $boletoItem The ChildBoletoItem object to remove.
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function removeBoletoItem(ChildBoletoItem $boletoItem)
    {
        if ($this->getBoletoItems()->contains($boletoItem)) {
            $pos = $this->collBoletoItems->search($boletoItem);
            $this->collBoletoItems->remove($pos);
            if (null === $this->boletoItemsScheduledForDeletion) {
                $this->boletoItemsScheduledForDeletion = clone $this->collBoletoItems;
                $this->boletoItemsScheduledForDeletion->clear();
            }
            $this->boletoItemsScheduledForDeletion[]= clone $boletoItem;
            $boletoItem->setBoleto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related BoletoItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoItem[] List of ChildBoletoItem objects
     */
    public function getBoletoItemsJoinServicoCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoItemQuery::create(null, $criteria);
        $query->joinWith('ServicoCliente', $joinBehavior);

        return $this->getBoletoItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related BoletoItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoItem[] List of ChildBoletoItem objects
     */
    public function getBoletoItemsJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoItemQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletoItems($query, $con);
    }

    /**
     * Clears out the collDadosFiscals collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDadosFiscals()
     */
    public function clearDadosFiscals()
    {
        $this->collDadosFiscals = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDadosFiscals collection loaded partially.
     */
    public function resetPartialDadosFiscals($v = true)
    {
        $this->collDadosFiscalsPartial = $v;
    }

    /**
     * Initializes the collDadosFiscals collection.
     *
     * By default this just sets the collDadosFiscals collection to an empty array (like clearcollDadosFiscals());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDadosFiscals($overrideExisting = true)
    {
        if (null !== $this->collDadosFiscals && !$overrideExisting) {
            return;
        }

        $collectionClassName = DadosFiscalTableMap::getTableMap()->getCollectionClassName();

        $this->collDadosFiscals = new $collectionClassName;
        $this->collDadosFiscals->setModel('\ImaTelecomBundle\Model\DadosFiscal');
    }

    /**
     * Gets an array of ChildDadosFiscal objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBoleto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDadosFiscal[] List of ChildDadosFiscal objects
     * @throws PropelException
     */
    public function getDadosFiscals(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalsPartial && !$this->isNew();
        if (null === $this->collDadosFiscals || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscals) {
                // return empty collection
                $this->initDadosFiscals();
            } else {
                $collDadosFiscals = ChildDadosFiscalQuery::create(null, $criteria)
                    ->filterByBoleto($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDadosFiscalsPartial && count($collDadosFiscals)) {
                        $this->initDadosFiscals(false);

                        foreach ($collDadosFiscals as $obj) {
                            if (false == $this->collDadosFiscals->contains($obj)) {
                                $this->collDadosFiscals->append($obj);
                            }
                        }

                        $this->collDadosFiscalsPartial = true;
                    }

                    return $collDadosFiscals;
                }

                if ($partial && $this->collDadosFiscals) {
                    foreach ($this->collDadosFiscals as $obj) {
                        if ($obj->isNew()) {
                            $collDadosFiscals[] = $obj;
                        }
                    }
                }

                $this->collDadosFiscals = $collDadosFiscals;
                $this->collDadosFiscalsPartial = false;
            }
        }

        return $this->collDadosFiscals;
    }

    /**
     * Sets a collection of ChildDadosFiscal objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $dadosFiscals A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function setDadosFiscals(Collection $dadosFiscals, ConnectionInterface $con = null)
    {
        /** @var ChildDadosFiscal[] $dadosFiscalsToDelete */
        $dadosFiscalsToDelete = $this->getDadosFiscals(new Criteria(), $con)->diff($dadosFiscals);


        $this->dadosFiscalsScheduledForDeletion = $dadosFiscalsToDelete;

        foreach ($dadosFiscalsToDelete as $dadosFiscalRemoved) {
            $dadosFiscalRemoved->setBoleto(null);
        }

        $this->collDadosFiscals = null;
        foreach ($dadosFiscals as $dadosFiscal) {
            $this->addDadosFiscal($dadosFiscal);
        }

        $this->collDadosFiscals = $dadosFiscals;
        $this->collDadosFiscalsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DadosFiscal objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related DadosFiscal objects.
     * @throws PropelException
     */
    public function countDadosFiscals(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalsPartial && !$this->isNew();
        if (null === $this->collDadosFiscals || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscals) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDadosFiscals());
            }

            $query = ChildDadosFiscalQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBoleto($this)
                ->count($con);
        }

        return count($this->collDadosFiscals);
    }

    /**
     * Method called to associate a ChildDadosFiscal object to this object
     * through the ChildDadosFiscal foreign key attribute.
     *
     * @param  ChildDadosFiscal $l ChildDadosFiscal
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function addDadosFiscal(ChildDadosFiscal $l)
    {
        if ($this->collDadosFiscals === null) {
            $this->initDadosFiscals();
            $this->collDadosFiscalsPartial = true;
        }

        if (!$this->collDadosFiscals->contains($l)) {
            $this->doAddDadosFiscal($l);

            if ($this->dadosFiscalsScheduledForDeletion and $this->dadosFiscalsScheduledForDeletion->contains($l)) {
                $this->dadosFiscalsScheduledForDeletion->remove($this->dadosFiscalsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDadosFiscal $dadosFiscal The ChildDadosFiscal object to add.
     */
    protected function doAddDadosFiscal(ChildDadosFiscal $dadosFiscal)
    {
        $this->collDadosFiscals[]= $dadosFiscal;
        $dadosFiscal->setBoleto($this);
    }

    /**
     * @param  ChildDadosFiscal $dadosFiscal The ChildDadosFiscal object to remove.
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function removeDadosFiscal(ChildDadosFiscal $dadosFiscal)
    {
        if ($this->getDadosFiscals()->contains($dadosFiscal)) {
            $pos = $this->collDadosFiscals->search($dadosFiscal);
            $this->collDadosFiscals->remove($pos);
            if (null === $this->dadosFiscalsScheduledForDeletion) {
                $this->dadosFiscalsScheduledForDeletion = clone $this->collDadosFiscals;
                $this->dadosFiscalsScheduledForDeletion->clear();
            }
            $this->dadosFiscalsScheduledForDeletion[]= clone $dadosFiscal;
            $dadosFiscal->setBoleto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related DadosFiscals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscal[] List of ChildDadosFiscal objects
     */
    public function getDadosFiscalsJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getDadosFiscals($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related DadosFiscals from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscal[] List of ChildDadosFiscal objects
     */
    public function getDadosFiscalsJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getDadosFiscals($query, $con);
    }

    /**
     * Clears out the collDadosFiscalItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDadosFiscalItems()
     */
    public function clearDadosFiscalItems()
    {
        $this->collDadosFiscalItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDadosFiscalItems collection loaded partially.
     */
    public function resetPartialDadosFiscalItems($v = true)
    {
        $this->collDadosFiscalItemsPartial = $v;
    }

    /**
     * Initializes the collDadosFiscalItems collection.
     *
     * By default this just sets the collDadosFiscalItems collection to an empty array (like clearcollDadosFiscalItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDadosFiscalItems($overrideExisting = true)
    {
        if (null !== $this->collDadosFiscalItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = DadosFiscalItemTableMap::getTableMap()->getCollectionClassName();

        $this->collDadosFiscalItems = new $collectionClassName;
        $this->collDadosFiscalItems->setModel('\ImaTelecomBundle\Model\DadosFiscalItem');
    }

    /**
     * Gets an array of ChildDadosFiscalItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBoleto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildDadosFiscalItem[] List of ChildDadosFiscalItem objects
     * @throws PropelException
     */
    public function getDadosFiscalItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalItemsPartial && !$this->isNew();
        if (null === $this->collDadosFiscalItems || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscalItems) {
                // return empty collection
                $this->initDadosFiscalItems();
            } else {
                $collDadosFiscalItems = ChildDadosFiscalItemQuery::create(null, $criteria)
                    ->filterByBoleto($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDadosFiscalItemsPartial && count($collDadosFiscalItems)) {
                        $this->initDadosFiscalItems(false);

                        foreach ($collDadosFiscalItems as $obj) {
                            if (false == $this->collDadosFiscalItems->contains($obj)) {
                                $this->collDadosFiscalItems->append($obj);
                            }
                        }

                        $this->collDadosFiscalItemsPartial = true;
                    }

                    return $collDadosFiscalItems;
                }

                if ($partial && $this->collDadosFiscalItems) {
                    foreach ($this->collDadosFiscalItems as $obj) {
                        if ($obj->isNew()) {
                            $collDadosFiscalItems[] = $obj;
                        }
                    }
                }

                $this->collDadosFiscalItems = $collDadosFiscalItems;
                $this->collDadosFiscalItemsPartial = false;
            }
        }

        return $this->collDadosFiscalItems;
    }

    /**
     * Sets a collection of ChildDadosFiscalItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $dadosFiscalItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function setDadosFiscalItems(Collection $dadosFiscalItems, ConnectionInterface $con = null)
    {
        /** @var ChildDadosFiscalItem[] $dadosFiscalItemsToDelete */
        $dadosFiscalItemsToDelete = $this->getDadosFiscalItems(new Criteria(), $con)->diff($dadosFiscalItems);


        $this->dadosFiscalItemsScheduledForDeletion = $dadosFiscalItemsToDelete;

        foreach ($dadosFiscalItemsToDelete as $dadosFiscalItemRemoved) {
            $dadosFiscalItemRemoved->setBoleto(null);
        }

        $this->collDadosFiscalItems = null;
        foreach ($dadosFiscalItems as $dadosFiscalItem) {
            $this->addDadosFiscalItem($dadosFiscalItem);
        }

        $this->collDadosFiscalItems = $dadosFiscalItems;
        $this->collDadosFiscalItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related DadosFiscalItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related DadosFiscalItem objects.
     * @throws PropelException
     */
    public function countDadosFiscalItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDadosFiscalItemsPartial && !$this->isNew();
        if (null === $this->collDadosFiscalItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDadosFiscalItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDadosFiscalItems());
            }

            $query = ChildDadosFiscalItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBoleto($this)
                ->count($con);
        }

        return count($this->collDadosFiscalItems);
    }

    /**
     * Method called to associate a ChildDadosFiscalItem object to this object
     * through the ChildDadosFiscalItem foreign key attribute.
     *
     * @param  ChildDadosFiscalItem $l ChildDadosFiscalItem
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function addDadosFiscalItem(ChildDadosFiscalItem $l)
    {
        if ($this->collDadosFiscalItems === null) {
            $this->initDadosFiscalItems();
            $this->collDadosFiscalItemsPartial = true;
        }

        if (!$this->collDadosFiscalItems->contains($l)) {
            $this->doAddDadosFiscalItem($l);

            if ($this->dadosFiscalItemsScheduledForDeletion and $this->dadosFiscalItemsScheduledForDeletion->contains($l)) {
                $this->dadosFiscalItemsScheduledForDeletion->remove($this->dadosFiscalItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildDadosFiscalItem $dadosFiscalItem The ChildDadosFiscalItem object to add.
     */
    protected function doAddDadosFiscalItem(ChildDadosFiscalItem $dadosFiscalItem)
    {
        $this->collDadosFiscalItems[]= $dadosFiscalItem;
        $dadosFiscalItem->setBoleto($this);
    }

    /**
     * @param  ChildDadosFiscalItem $dadosFiscalItem The ChildDadosFiscalItem object to remove.
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function removeDadosFiscalItem(ChildDadosFiscalItem $dadosFiscalItem)
    {
        if ($this->getDadosFiscalItems()->contains($dadosFiscalItem)) {
            $pos = $this->collDadosFiscalItems->search($dadosFiscalItem);
            $this->collDadosFiscalItems->remove($pos);
            if (null === $this->dadosFiscalItemsScheduledForDeletion) {
                $this->dadosFiscalItemsScheduledForDeletion = clone $this->collDadosFiscalItems;
                $this->dadosFiscalItemsScheduledForDeletion->clear();
            }
            $this->dadosFiscalItemsScheduledForDeletion[]= clone $dadosFiscalItem;
            $dadosFiscalItem->setBoleto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related DadosFiscalItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscalItem[] List of ChildDadosFiscalItem objects
     */
    public function getDadosFiscalItemsJoinBoletoItem(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalItemQuery::create(null, $criteria);
        $query->joinWith('BoletoItem', $joinBehavior);

        return $this->getDadosFiscalItems($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related DadosFiscalItems from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildDadosFiscalItem[] List of ChildDadosFiscalItem objects
     */
    public function getDadosFiscalItemsJoinDadosFiscal(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildDadosFiscalItemQuery::create(null, $criteria);
        $query->joinWith('DadosFiscal', $joinBehavior);

        return $this->getDadosFiscalItems($query, $con);
    }

    /**
     * Clears out the collLancamentosBoletoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLancamentosBoletoss()
     */
    public function clearLancamentosBoletoss()
    {
        $this->collLancamentosBoletoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLancamentosBoletoss collection loaded partially.
     */
    public function resetPartialLancamentosBoletoss($v = true)
    {
        $this->collLancamentosBoletossPartial = $v;
    }

    /**
     * Initializes the collLancamentosBoletoss collection.
     *
     * By default this just sets the collLancamentosBoletoss collection to an empty array (like clearcollLancamentosBoletoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLancamentosBoletoss($overrideExisting = true)
    {
        if (null !== $this->collLancamentosBoletoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = LancamentosBoletosTableMap::getTableMap()->getCollectionClassName();

        $this->collLancamentosBoletoss = new $collectionClassName;
        $this->collLancamentosBoletoss->setModel('\ImaTelecomBundle\Model\LancamentosBoletos');
    }

    /**
     * Gets an array of ChildLancamentosBoletos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBoleto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     * @throws PropelException
     */
    public function getLancamentosBoletoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentosBoletossPartial && !$this->isNew();
        if (null === $this->collLancamentosBoletoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLancamentosBoletoss) {
                // return empty collection
                $this->initLancamentosBoletoss();
            } else {
                $collLancamentosBoletoss = ChildLancamentosBoletosQuery::create(null, $criteria)
                    ->filterByBoleto($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLancamentosBoletossPartial && count($collLancamentosBoletoss)) {
                        $this->initLancamentosBoletoss(false);

                        foreach ($collLancamentosBoletoss as $obj) {
                            if (false == $this->collLancamentosBoletoss->contains($obj)) {
                                $this->collLancamentosBoletoss->append($obj);
                            }
                        }

                        $this->collLancamentosBoletossPartial = true;
                    }

                    return $collLancamentosBoletoss;
                }

                if ($partial && $this->collLancamentosBoletoss) {
                    foreach ($this->collLancamentosBoletoss as $obj) {
                        if ($obj->isNew()) {
                            $collLancamentosBoletoss[] = $obj;
                        }
                    }
                }

                $this->collLancamentosBoletoss = $collLancamentosBoletoss;
                $this->collLancamentosBoletossPartial = false;
            }
        }

        return $this->collLancamentosBoletoss;
    }

    /**
     * Sets a collection of ChildLancamentosBoletos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lancamentosBoletoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function setLancamentosBoletoss(Collection $lancamentosBoletoss, ConnectionInterface $con = null)
    {
        /** @var ChildLancamentosBoletos[] $lancamentosBoletossToDelete */
        $lancamentosBoletossToDelete = $this->getLancamentosBoletoss(new Criteria(), $con)->diff($lancamentosBoletoss);


        $this->lancamentosBoletossScheduledForDeletion = $lancamentosBoletossToDelete;

        foreach ($lancamentosBoletossToDelete as $lancamentosBoletosRemoved) {
            $lancamentosBoletosRemoved->setBoleto(null);
        }

        $this->collLancamentosBoletoss = null;
        foreach ($lancamentosBoletoss as $lancamentosBoletos) {
            $this->addLancamentosBoletos($lancamentosBoletos);
        }

        $this->collLancamentosBoletoss = $lancamentosBoletoss;
        $this->collLancamentosBoletossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LancamentosBoletos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related LancamentosBoletos objects.
     * @throws PropelException
     */
    public function countLancamentosBoletoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentosBoletossPartial && !$this->isNew();
        if (null === $this->collLancamentosBoletoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLancamentosBoletoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLancamentosBoletoss());
            }

            $query = ChildLancamentosBoletosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBoleto($this)
                ->count($con);
        }

        return count($this->collLancamentosBoletoss);
    }

    /**
     * Method called to associate a ChildLancamentosBoletos object to this object
     * through the ChildLancamentosBoletos foreign key attribute.
     *
     * @param  ChildLancamentosBoletos $l ChildLancamentosBoletos
     * @return $this|\ImaTelecomBundle\Model\Boleto The current object (for fluent API support)
     */
    public function addLancamentosBoletos(ChildLancamentosBoletos $l)
    {
        if ($this->collLancamentosBoletoss === null) {
            $this->initLancamentosBoletoss();
            $this->collLancamentosBoletossPartial = true;
        }

        if (!$this->collLancamentosBoletoss->contains($l)) {
            $this->doAddLancamentosBoletos($l);

            if ($this->lancamentosBoletossScheduledForDeletion and $this->lancamentosBoletossScheduledForDeletion->contains($l)) {
                $this->lancamentosBoletossScheduledForDeletion->remove($this->lancamentosBoletossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLancamentosBoletos $lancamentosBoletos The ChildLancamentosBoletos object to add.
     */
    protected function doAddLancamentosBoletos(ChildLancamentosBoletos $lancamentosBoletos)
    {
        $this->collLancamentosBoletoss[]= $lancamentosBoletos;
        $lancamentosBoletos->setBoleto($this);
    }

    /**
     * @param  ChildLancamentosBoletos $lancamentosBoletos The ChildLancamentosBoletos object to remove.
     * @return $this|ChildBoleto The current object (for fluent API support)
     */
    public function removeLancamentosBoletos(ChildLancamentosBoletos $lancamentosBoletos)
    {
        if ($this->getLancamentosBoletoss()->contains($lancamentosBoletos)) {
            $pos = $this->collLancamentosBoletoss->search($lancamentosBoletos);
            $this->collLancamentosBoletoss->remove($pos);
            if (null === $this->lancamentosBoletossScheduledForDeletion) {
                $this->lancamentosBoletossScheduledForDeletion = clone $this->collLancamentosBoletoss;
                $this->lancamentosBoletossScheduledForDeletion->clear();
            }
            $this->lancamentosBoletossScheduledForDeletion[]= clone $lancamentosBoletos;
            $lancamentosBoletos->setBoleto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinConvenio(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Convenio', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Boleto is new, it will return
     * an empty collection; or if this Boleto has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Boleto.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinLancamentos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Lancamentos', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBaixaEstorno) {
            $this->aBaixaEstorno->removeBoleto($this);
        }
        if (null !== $this->aBaixa) {
            $this->aBaixa->removeBoleto($this);
        }
        if (null !== $this->aCliente) {
            $this->aCliente->removeBoleto($this);
        }
        if (null !== $this->aCompetencia) {
            $this->aCompetencia->removeBoleto($this);
        }
        if (null !== $this->aFornecedor) {
            $this->aFornecedor->removeBoleto($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeBoleto($this);
        }
        $this->idboleto = null;
        $this->valor = null;
        $this->vencimento = null;
        $this->cliente_id = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->import_id = null;
        $this->competencia_id = null;
        $this->nosso_numero = null;
        $this->numero_documento = null;
        $this->registrado = null;
        $this->valor_original = null;
        $this->valor_multa = null;
        $this->valor_juros = null;
        $this->valor_desconto = null;
        $this->valor_pago = null;
        $this->linha_digitavel = null;
        $this->data_pagamento = null;
        $this->esta_pago = null;
        $this->valor_abatimento = null;
        $this->valor_iof = null;
        $this->valor_liquido = null;
        $this->valor_outras_despesas = null;
        $this->valor_outros_creditos = null;
        $this->nota_scm_gerada = null;
        $this->nota_scm_cancelada = null;
        $this->base_calculo_scm = null;
        $this->base_calculo_sva = null;
        $this->nota_sva_gerada = null;
        $this->nota_sva_cancelada = null;
        $this->tipo_conta = null;
        $this->baixa_id = null;
        $this->baixa_estorno_id = null;
        $this->fornecedor_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBoletoBaixaHistoricos) {
                foreach ($this->collBoletoBaixaHistoricos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collBoletoItems) {
                foreach ($this->collBoletoItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDadosFiscals) {
                foreach ($this->collDadosFiscals as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDadosFiscalItems) {
                foreach ($this->collDadosFiscalItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLancamentosBoletoss) {
                foreach ($this->collLancamentosBoletoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBoletoBaixaHistoricos = null;
        $this->collBoletoItems = null;
        $this->collDadosFiscals = null;
        $this->collDadosFiscalItems = null;
        $this->collLancamentosBoletoss = null;
        $this->aBaixaEstorno = null;
        $this->aBaixa = null;
        $this->aCliente = null;
        $this->aCompetencia = null;
        $this->aFornecedor = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BoletoTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
