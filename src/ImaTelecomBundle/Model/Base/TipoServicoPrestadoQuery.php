<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\TipoServicoPrestado as ChildTipoServicoPrestado;
use ImaTelecomBundle\Model\TipoServicoPrestadoQuery as ChildTipoServicoPrestadoQuery;
use ImaTelecomBundle\Model\Map\TipoServicoPrestadoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tipo_servico_prestado' table.
 *
 *
 *
 * @method     ChildTipoServicoPrestadoQuery orderByIdtipoServicoPrestado($order = Criteria::ASC) Order by the idtipo_servico_prestado column
 * @method     ChildTipoServicoPrestadoQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method     ChildTipoServicoPrestadoQuery orderByPrePago($order = Criteria::ASC) Order by the pre_pago column
 * @method     ChildTipoServicoPrestadoQuery orderByNaturezaOperacao($order = Criteria::ASC) Order by the natureza_operacao column
 * @method     ChildTipoServicoPrestadoQuery orderByItemListaServico($order = Criteria::ASC) Order by the item_lista_servico column
 * @method     ChildTipoServicoPrestadoQuery orderByCodigoTributacao($order = Criteria::ASC) Order by the codigo_tributacao column
 * @method     ChildTipoServicoPrestadoQuery orderByAliquota($order = Criteria::ASC) Order by the aliquota column
 * @method     ChildTipoServicoPrestadoQuery orderByUnidadeNegocio($order = Criteria::ASC) Order by the unidade_negocio column
 *
 * @method     ChildTipoServicoPrestadoQuery groupByIdtipoServicoPrestado() Group by the idtipo_servico_prestado column
 * @method     ChildTipoServicoPrestadoQuery groupByTipo() Group by the tipo column
 * @method     ChildTipoServicoPrestadoQuery groupByPrePago() Group by the pre_pago column
 * @method     ChildTipoServicoPrestadoQuery groupByNaturezaOperacao() Group by the natureza_operacao column
 * @method     ChildTipoServicoPrestadoQuery groupByItemListaServico() Group by the item_lista_servico column
 * @method     ChildTipoServicoPrestadoQuery groupByCodigoTributacao() Group by the codigo_tributacao column
 * @method     ChildTipoServicoPrestadoQuery groupByAliquota() Group by the aliquota column
 * @method     ChildTipoServicoPrestadoQuery groupByUnidadeNegocio() Group by the unidade_negocio column
 *
 * @method     ChildTipoServicoPrestadoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTipoServicoPrestadoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTipoServicoPrestadoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTipoServicoPrestadoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTipoServicoPrestadoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTipoServicoPrestadoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTipoServicoPrestadoQuery leftJoinServicoPrestado($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicoPrestado relation
 * @method     ChildTipoServicoPrestadoQuery rightJoinServicoPrestado($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicoPrestado relation
 * @method     ChildTipoServicoPrestadoQuery innerJoinServicoPrestado($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicoPrestado relation
 *
 * @method     ChildTipoServicoPrestadoQuery joinWithServicoPrestado($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicoPrestado relation
 *
 * @method     ChildTipoServicoPrestadoQuery leftJoinWithServicoPrestado() Adds a LEFT JOIN clause and with to the query using the ServicoPrestado relation
 * @method     ChildTipoServicoPrestadoQuery rightJoinWithServicoPrestado() Adds a RIGHT JOIN clause and with to the query using the ServicoPrestado relation
 * @method     ChildTipoServicoPrestadoQuery innerJoinWithServicoPrestado() Adds a INNER JOIN clause and with to the query using the ServicoPrestado relation
 *
 * @method     \ImaTelecomBundle\Model\ServicoPrestadoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTipoServicoPrestado findOne(ConnectionInterface $con = null) Return the first ChildTipoServicoPrestado matching the query
 * @method     ChildTipoServicoPrestado findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTipoServicoPrestado matching the query, or a new ChildTipoServicoPrestado object populated from the query conditions when no match is found
 *
 * @method     ChildTipoServicoPrestado findOneByIdtipoServicoPrestado(int $idtipo_servico_prestado) Return the first ChildTipoServicoPrestado filtered by the idtipo_servico_prestado column
 * @method     ChildTipoServicoPrestado findOneByTipo(string $tipo) Return the first ChildTipoServicoPrestado filtered by the tipo column
 * @method     ChildTipoServicoPrestado findOneByPrePago(boolean $pre_pago) Return the first ChildTipoServicoPrestado filtered by the pre_pago column
 * @method     ChildTipoServicoPrestado findOneByNaturezaOperacao(string $natureza_operacao) Return the first ChildTipoServicoPrestado filtered by the natureza_operacao column
 * @method     ChildTipoServicoPrestado findOneByItemListaServico(string $item_lista_servico) Return the first ChildTipoServicoPrestado filtered by the item_lista_servico column
 * @method     ChildTipoServicoPrestado findOneByCodigoTributacao(string $codigo_tributacao) Return the first ChildTipoServicoPrestado filtered by the codigo_tributacao column
 * @method     ChildTipoServicoPrestado findOneByAliquota(double $aliquota) Return the first ChildTipoServicoPrestado filtered by the aliquota column
 * @method     ChildTipoServicoPrestado findOneByUnidadeNegocio(int $unidade_negocio) Return the first ChildTipoServicoPrestado filtered by the unidade_negocio column *

 * @method     ChildTipoServicoPrestado requirePk($key, ConnectionInterface $con = null) Return the ChildTipoServicoPrestado by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoServicoPrestado requireOne(ConnectionInterface $con = null) Return the first ChildTipoServicoPrestado matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTipoServicoPrestado requireOneByIdtipoServicoPrestado(int $idtipo_servico_prestado) Return the first ChildTipoServicoPrestado filtered by the idtipo_servico_prestado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoServicoPrestado requireOneByTipo(string $tipo) Return the first ChildTipoServicoPrestado filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoServicoPrestado requireOneByPrePago(boolean $pre_pago) Return the first ChildTipoServicoPrestado filtered by the pre_pago column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoServicoPrestado requireOneByNaturezaOperacao(string $natureza_operacao) Return the first ChildTipoServicoPrestado filtered by the natureza_operacao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoServicoPrestado requireOneByItemListaServico(string $item_lista_servico) Return the first ChildTipoServicoPrestado filtered by the item_lista_servico column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoServicoPrestado requireOneByCodigoTributacao(string $codigo_tributacao) Return the first ChildTipoServicoPrestado filtered by the codigo_tributacao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoServicoPrestado requireOneByAliquota(double $aliquota) Return the first ChildTipoServicoPrestado filtered by the aliquota column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoServicoPrestado requireOneByUnidadeNegocio(int $unidade_negocio) Return the first ChildTipoServicoPrestado filtered by the unidade_negocio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTipoServicoPrestado[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTipoServicoPrestado objects based on current ModelCriteria
 * @method     ChildTipoServicoPrestado[]|ObjectCollection findByIdtipoServicoPrestado(int $idtipo_servico_prestado) Return ChildTipoServicoPrestado objects filtered by the idtipo_servico_prestado column
 * @method     ChildTipoServicoPrestado[]|ObjectCollection findByTipo(string $tipo) Return ChildTipoServicoPrestado objects filtered by the tipo column
 * @method     ChildTipoServicoPrestado[]|ObjectCollection findByPrePago(boolean $pre_pago) Return ChildTipoServicoPrestado objects filtered by the pre_pago column
 * @method     ChildTipoServicoPrestado[]|ObjectCollection findByNaturezaOperacao(string $natureza_operacao) Return ChildTipoServicoPrestado objects filtered by the natureza_operacao column
 * @method     ChildTipoServicoPrestado[]|ObjectCollection findByItemListaServico(string $item_lista_servico) Return ChildTipoServicoPrestado objects filtered by the item_lista_servico column
 * @method     ChildTipoServicoPrestado[]|ObjectCollection findByCodigoTributacao(string $codigo_tributacao) Return ChildTipoServicoPrestado objects filtered by the codigo_tributacao column
 * @method     ChildTipoServicoPrestado[]|ObjectCollection findByAliquota(double $aliquota) Return ChildTipoServicoPrestado objects filtered by the aliquota column
 * @method     ChildTipoServicoPrestado[]|ObjectCollection findByUnidadeNegocio(int $unidade_negocio) Return ChildTipoServicoPrestado objects filtered by the unidade_negocio column
 * @method     ChildTipoServicoPrestado[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TipoServicoPrestadoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\TipoServicoPrestadoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\TipoServicoPrestado', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTipoServicoPrestadoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTipoServicoPrestadoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTipoServicoPrestadoQuery) {
            return $criteria;
        }
        $query = new ChildTipoServicoPrestadoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTipoServicoPrestado|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TipoServicoPrestadoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TipoServicoPrestadoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTipoServicoPrestado A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idtipo_servico_prestado, tipo, pre_pago, natureza_operacao, item_lista_servico, codigo_tributacao, aliquota, unidade_negocio FROM tipo_servico_prestado WHERE idtipo_servico_prestado = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTipoServicoPrestado $obj */
            $obj = new ChildTipoServicoPrestado();
            $obj->hydrate($row);
            TipoServicoPrestadoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTipoServicoPrestado|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idtipo_servico_prestado column
     *
     * Example usage:
     * <code>
     * $query->filterByIdtipoServicoPrestado(1234); // WHERE idtipo_servico_prestado = 1234
     * $query->filterByIdtipoServicoPrestado(array(12, 34)); // WHERE idtipo_servico_prestado IN (12, 34)
     * $query->filterByIdtipoServicoPrestado(array('min' => 12)); // WHERE idtipo_servico_prestado > 12
     * </code>
     *
     * @param     mixed $idtipoServicoPrestado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByIdtipoServicoPrestado($idtipoServicoPrestado = null, $comparison = null)
    {
        if (is_array($idtipoServicoPrestado)) {
            $useMinMax = false;
            if (isset($idtipoServicoPrestado['min'])) {
                $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, $idtipoServicoPrestado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idtipoServicoPrestado['max'])) {
                $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, $idtipoServicoPrestado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, $idtipoServicoPrestado, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the pre_pago column
     *
     * Example usage:
     * <code>
     * $query->filterByPrePago(true); // WHERE pre_pago = true
     * $query->filterByPrePago('yes'); // WHERE pre_pago = true
     * </code>
     *
     * @param     boolean|string $prePago The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByPrePago($prePago = null, $comparison = null)
    {
        if (is_string($prePago)) {
            $prePago = in_array(strtolower($prePago), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_PRE_PAGO, $prePago, $comparison);
    }

    /**
     * Filter the query on the natureza_operacao column
     *
     * Example usage:
     * <code>
     * $query->filterByNaturezaOperacao('fooValue');   // WHERE natureza_operacao = 'fooValue'
     * $query->filterByNaturezaOperacao('%fooValue%', Criteria::LIKE); // WHERE natureza_operacao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $naturezaOperacao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByNaturezaOperacao($naturezaOperacao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($naturezaOperacao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_NATUREZA_OPERACAO, $naturezaOperacao, $comparison);
    }

    /**
     * Filter the query on the item_lista_servico column
     *
     * Example usage:
     * <code>
     * $query->filterByItemListaServico('fooValue');   // WHERE item_lista_servico = 'fooValue'
     * $query->filterByItemListaServico('%fooValue%', Criteria::LIKE); // WHERE item_lista_servico LIKE '%fooValue%'
     * </code>
     *
     * @param     string $itemListaServico The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByItemListaServico($itemListaServico = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($itemListaServico)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_ITEM_LISTA_SERVICO, $itemListaServico, $comparison);
    }

    /**
     * Filter the query on the codigo_tributacao column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigoTributacao('fooValue');   // WHERE codigo_tributacao = 'fooValue'
     * $query->filterByCodigoTributacao('%fooValue%', Criteria::LIKE); // WHERE codigo_tributacao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigoTributacao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByCodigoTributacao($codigoTributacao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigoTributacao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_CODIGO_TRIBUTACAO, $codigoTributacao, $comparison);
    }

    /**
     * Filter the query on the aliquota column
     *
     * Example usage:
     * <code>
     * $query->filterByAliquota(1234); // WHERE aliquota = 1234
     * $query->filterByAliquota(array(12, 34)); // WHERE aliquota IN (12, 34)
     * $query->filterByAliquota(array('min' => 12)); // WHERE aliquota > 12
     * </code>
     *
     * @param     mixed $aliquota The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByAliquota($aliquota = null, $comparison = null)
    {
        if (is_array($aliquota)) {
            $useMinMax = false;
            if (isset($aliquota['min'])) {
                $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_ALIQUOTA, $aliquota['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($aliquota['max'])) {
                $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_ALIQUOTA, $aliquota['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_ALIQUOTA, $aliquota, $comparison);
    }

    /**
     * Filter the query on the unidade_negocio column
     *
     * Example usage:
     * <code>
     * $query->filterByUnidadeNegocio(1234); // WHERE unidade_negocio = 1234
     * $query->filterByUnidadeNegocio(array(12, 34)); // WHERE unidade_negocio IN (12, 34)
     * $query->filterByUnidadeNegocio(array('min' => 12)); // WHERE unidade_negocio > 12
     * </code>
     *
     * @param     mixed $unidadeNegocio The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByUnidadeNegocio($unidadeNegocio = null, $comparison = null)
    {
        if (is_array($unidadeNegocio)) {
            $useMinMax = false;
            if (isset($unidadeNegocio['min'])) {
                $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO, $unidadeNegocio['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unidadeNegocio['max'])) {
                $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO, $unidadeNegocio['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO, $unidadeNegocio, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ServicoPrestado object
     *
     * @param \ImaTelecomBundle\Model\ServicoPrestado|ObjectCollection $servicoPrestado the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function filterByServicoPrestado($servicoPrestado, $comparison = null)
    {
        if ($servicoPrestado instanceof \ImaTelecomBundle\Model\ServicoPrestado) {
            return $this
                ->addUsingAlias(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, $servicoPrestado->getTipoServicoPrestadoId(), $comparison);
        } elseif ($servicoPrestado instanceof ObjectCollection) {
            return $this
                ->useServicoPrestadoQuery()
                ->filterByPrimaryKeys($servicoPrestado->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicoPrestado() only accepts arguments of type \ImaTelecomBundle\Model\ServicoPrestado or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicoPrestado relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function joinServicoPrestado($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicoPrestado');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicoPrestado');
        }

        return $this;
    }

    /**
     * Use the ServicoPrestado relation ServicoPrestado object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ServicoPrestadoQuery A secondary query class using the current class as primary query
     */
    public function useServicoPrestadoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicoPrestado($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicoPrestado', '\ImaTelecomBundle\Model\ServicoPrestadoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTipoServicoPrestado $tipoServicoPrestado Object to remove from the list of results
     *
     * @return $this|ChildTipoServicoPrestadoQuery The current query, for fluid interface
     */
    public function prune($tipoServicoPrestado = null)
    {
        if ($tipoServicoPrestado) {
            $this->addUsingAlias(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, $tipoServicoPrestado->getIdtipoServicoPrestado(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tipo_servico_prestado table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoServicoPrestadoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TipoServicoPrestadoTableMap::clearInstancePool();
            TipoServicoPrestadoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoServicoPrestadoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TipoServicoPrestadoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TipoServicoPrestadoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TipoServicoPrestadoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TipoServicoPrestadoQuery
