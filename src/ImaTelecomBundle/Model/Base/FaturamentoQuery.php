<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Faturamento as ChildFaturamento;
use ImaTelecomBundle\Model\FaturamentoQuery as ChildFaturamentoQuery;
use ImaTelecomBundle\Model\Map\FaturamentoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'faturamento' table.
 *
 *
 *
 * @method     ChildFaturamentoQuery orderByIdfaturamento($order = Criteria::ASC) Order by the idfaturamento column
 * @method     ChildFaturamentoQuery orderByDescricao($order = Criteria::ASC) Order by the descricao column
 * @method     ChildFaturamentoQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildFaturamentoQuery orderByDataFaturamento($order = Criteria::ASC) Order by the data_faturamento column
 * @method     ChildFaturamentoQuery orderByQtdeTotalContratos($order = Criteria::ASC) Order by the qtde_total_contratos column
 * @method     ChildFaturamentoQuery orderByValorTotalContratos($order = Criteria::ASC) Order by the valor_total_contratos column
 * @method     ChildFaturamentoQuery orderByQtdeTotalContratosPj($order = Criteria::ASC) Order by the qtde_total_contratos_pj column
 * @method     ChildFaturamentoQuery orderByValorTotalContratosPj($order = Criteria::ASC) Order by the valor_total_contratos_pj column
 * @method     ChildFaturamentoQuery orderByQtdeTotalContratosPf($order = Criteria::ASC) Order by the qtde_total_contratos_pf column
 * @method     ChildFaturamentoQuery orderByValorTotalContratosPf($order = Criteria::ASC) Order by the valor_total_contratos_pf column
 * @method     ChildFaturamentoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildFaturamentoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildFaturamentoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildFaturamentoQuery groupByIdfaturamento() Group by the idfaturamento column
 * @method     ChildFaturamentoQuery groupByDescricao() Group by the descricao column
 * @method     ChildFaturamentoQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildFaturamentoQuery groupByDataFaturamento() Group by the data_faturamento column
 * @method     ChildFaturamentoQuery groupByQtdeTotalContratos() Group by the qtde_total_contratos column
 * @method     ChildFaturamentoQuery groupByValorTotalContratos() Group by the valor_total_contratos column
 * @method     ChildFaturamentoQuery groupByQtdeTotalContratosPj() Group by the qtde_total_contratos_pj column
 * @method     ChildFaturamentoQuery groupByValorTotalContratosPj() Group by the valor_total_contratos_pj column
 * @method     ChildFaturamentoQuery groupByQtdeTotalContratosPf() Group by the qtde_total_contratos_pf column
 * @method     ChildFaturamentoQuery groupByValorTotalContratosPf() Group by the valor_total_contratos_pf column
 * @method     ChildFaturamentoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildFaturamentoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildFaturamentoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildFaturamentoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFaturamentoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFaturamentoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFaturamentoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFaturamentoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFaturamentoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFaturamentoQuery leftJoinCompetencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Competencia relation
 * @method     ChildFaturamentoQuery rightJoinCompetencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Competencia relation
 * @method     ChildFaturamentoQuery innerJoinCompetencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Competencia relation
 *
 * @method     ChildFaturamentoQuery joinWithCompetencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Competencia relation
 *
 * @method     ChildFaturamentoQuery leftJoinWithCompetencia() Adds a LEFT JOIN clause and with to the query using the Competencia relation
 * @method     ChildFaturamentoQuery rightJoinWithCompetencia() Adds a RIGHT JOIN clause and with to the query using the Competencia relation
 * @method     ChildFaturamentoQuery innerJoinWithCompetencia() Adds a INNER JOIN clause and with to the query using the Competencia relation
 *
 * @method     \ImaTelecomBundle\Model\CompetenciaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFaturamento findOne(ConnectionInterface $con = null) Return the first ChildFaturamento matching the query
 * @method     ChildFaturamento findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFaturamento matching the query, or a new ChildFaturamento object populated from the query conditions when no match is found
 *
 * @method     ChildFaturamento findOneByIdfaturamento(int $idfaturamento) Return the first ChildFaturamento filtered by the idfaturamento column
 * @method     ChildFaturamento findOneByDescricao(string $descricao) Return the first ChildFaturamento filtered by the descricao column
 * @method     ChildFaturamento findOneByCompetenciaId(int $competencia_id) Return the first ChildFaturamento filtered by the competencia_id column
 * @method     ChildFaturamento findOneByDataFaturamento(string $data_faturamento) Return the first ChildFaturamento filtered by the data_faturamento column
 * @method     ChildFaturamento findOneByQtdeTotalContratos(int $qtde_total_contratos) Return the first ChildFaturamento filtered by the qtde_total_contratos column
 * @method     ChildFaturamento findOneByValorTotalContratos(string $valor_total_contratos) Return the first ChildFaturamento filtered by the valor_total_contratos column
 * @method     ChildFaturamento findOneByQtdeTotalContratosPj(int $qtde_total_contratos_pj) Return the first ChildFaturamento filtered by the qtde_total_contratos_pj column
 * @method     ChildFaturamento findOneByValorTotalContratosPj(string $valor_total_contratos_pj) Return the first ChildFaturamento filtered by the valor_total_contratos_pj column
 * @method     ChildFaturamento findOneByQtdeTotalContratosPf(int $qtde_total_contratos_pf) Return the first ChildFaturamento filtered by the qtde_total_contratos_pf column
 * @method     ChildFaturamento findOneByValorTotalContratosPf(string $valor_total_contratos_pf) Return the first ChildFaturamento filtered by the valor_total_contratos_pf column
 * @method     ChildFaturamento findOneByDataCadastro(string $data_cadastro) Return the first ChildFaturamento filtered by the data_cadastro column
 * @method     ChildFaturamento findOneByDataAlterado(string $data_alterado) Return the first ChildFaturamento filtered by the data_alterado column
 * @method     ChildFaturamento findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildFaturamento filtered by the usuario_alterado column *

 * @method     ChildFaturamento requirePk($key, ConnectionInterface $con = null) Return the ChildFaturamento by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOne(ConnectionInterface $con = null) Return the first ChildFaturamento matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFaturamento requireOneByIdfaturamento(int $idfaturamento) Return the first ChildFaturamento filtered by the idfaturamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByDescricao(string $descricao) Return the first ChildFaturamento filtered by the descricao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByCompetenciaId(int $competencia_id) Return the first ChildFaturamento filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByDataFaturamento(string $data_faturamento) Return the first ChildFaturamento filtered by the data_faturamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByQtdeTotalContratos(int $qtde_total_contratos) Return the first ChildFaturamento filtered by the qtde_total_contratos column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByValorTotalContratos(string $valor_total_contratos) Return the first ChildFaturamento filtered by the valor_total_contratos column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByQtdeTotalContratosPj(int $qtde_total_contratos_pj) Return the first ChildFaturamento filtered by the qtde_total_contratos_pj column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByValorTotalContratosPj(string $valor_total_contratos_pj) Return the first ChildFaturamento filtered by the valor_total_contratos_pj column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByQtdeTotalContratosPf(int $qtde_total_contratos_pf) Return the first ChildFaturamento filtered by the qtde_total_contratos_pf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByValorTotalContratosPf(string $valor_total_contratos_pf) Return the first ChildFaturamento filtered by the valor_total_contratos_pf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByDataCadastro(string $data_cadastro) Return the first ChildFaturamento filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByDataAlterado(string $data_alterado) Return the first ChildFaturamento filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFaturamento requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildFaturamento filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFaturamento[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFaturamento objects based on current ModelCriteria
 * @method     ChildFaturamento[]|ObjectCollection findByIdfaturamento(int $idfaturamento) Return ChildFaturamento objects filtered by the idfaturamento column
 * @method     ChildFaturamento[]|ObjectCollection findByDescricao(string $descricao) Return ChildFaturamento objects filtered by the descricao column
 * @method     ChildFaturamento[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildFaturamento objects filtered by the competencia_id column
 * @method     ChildFaturamento[]|ObjectCollection findByDataFaturamento(string $data_faturamento) Return ChildFaturamento objects filtered by the data_faturamento column
 * @method     ChildFaturamento[]|ObjectCollection findByQtdeTotalContratos(int $qtde_total_contratos) Return ChildFaturamento objects filtered by the qtde_total_contratos column
 * @method     ChildFaturamento[]|ObjectCollection findByValorTotalContratos(string $valor_total_contratos) Return ChildFaturamento objects filtered by the valor_total_contratos column
 * @method     ChildFaturamento[]|ObjectCollection findByQtdeTotalContratosPj(int $qtde_total_contratos_pj) Return ChildFaturamento objects filtered by the qtde_total_contratos_pj column
 * @method     ChildFaturamento[]|ObjectCollection findByValorTotalContratosPj(string $valor_total_contratos_pj) Return ChildFaturamento objects filtered by the valor_total_contratos_pj column
 * @method     ChildFaturamento[]|ObjectCollection findByQtdeTotalContratosPf(int $qtde_total_contratos_pf) Return ChildFaturamento objects filtered by the qtde_total_contratos_pf column
 * @method     ChildFaturamento[]|ObjectCollection findByValorTotalContratosPf(string $valor_total_contratos_pf) Return ChildFaturamento objects filtered by the valor_total_contratos_pf column
 * @method     ChildFaturamento[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildFaturamento objects filtered by the data_cadastro column
 * @method     ChildFaturamento[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildFaturamento objects filtered by the data_alterado column
 * @method     ChildFaturamento[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildFaturamento objects filtered by the usuario_alterado column
 * @method     ChildFaturamento[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FaturamentoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\FaturamentoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Faturamento', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFaturamentoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFaturamentoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFaturamentoQuery) {
            return $criteria;
        }
        $query = new ChildFaturamentoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFaturamento|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FaturamentoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FaturamentoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFaturamento A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idfaturamento, descricao, competencia_id, data_faturamento, qtde_total_contratos, valor_total_contratos, qtde_total_contratos_pj, valor_total_contratos_pj, qtde_total_contratos_pf, valor_total_contratos_pf, data_cadastro, data_alterado, usuario_alterado FROM faturamento WHERE idfaturamento = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFaturamento $obj */
            $obj = new ChildFaturamento();
            $obj->hydrate($row);
            FaturamentoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFaturamento|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FaturamentoTableMap::COL_IDFATURAMENTO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FaturamentoTableMap::COL_IDFATURAMENTO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idfaturamento column
     *
     * Example usage:
     * <code>
     * $query->filterByIdfaturamento(1234); // WHERE idfaturamento = 1234
     * $query->filterByIdfaturamento(array(12, 34)); // WHERE idfaturamento IN (12, 34)
     * $query->filterByIdfaturamento(array('min' => 12)); // WHERE idfaturamento > 12
     * </code>
     *
     * @param     mixed $idfaturamento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByIdfaturamento($idfaturamento = null, $comparison = null)
    {
        if (is_array($idfaturamento)) {
            $useMinMax = false;
            if (isset($idfaturamento['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_IDFATURAMENTO, $idfaturamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idfaturamento['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_IDFATURAMENTO, $idfaturamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_IDFATURAMENTO, $idfaturamento, $comparison);
    }

    /**
     * Filter the query on the descricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricao('fooValue');   // WHERE descricao = 'fooValue'
     * $query->filterByDescricao('%fooValue%', Criteria::LIKE); // WHERE descricao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByDescricao($descricao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_DESCRICAO, $descricao, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @see       filterByCompetencia()
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the data_faturamento column
     *
     * Example usage:
     * <code>
     * $query->filterByDataFaturamento('2011-03-14'); // WHERE data_faturamento = '2011-03-14'
     * $query->filterByDataFaturamento('now'); // WHERE data_faturamento = '2011-03-14'
     * $query->filterByDataFaturamento(array('max' => 'yesterday')); // WHERE data_faturamento > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataFaturamento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByDataFaturamento($dataFaturamento = null, $comparison = null)
    {
        if (is_array($dataFaturamento)) {
            $useMinMax = false;
            if (isset($dataFaturamento['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_DATA_FATURAMENTO, $dataFaturamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataFaturamento['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_DATA_FATURAMENTO, $dataFaturamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_DATA_FATURAMENTO, $dataFaturamento, $comparison);
    }

    /**
     * Filter the query on the qtde_total_contratos column
     *
     * Example usage:
     * <code>
     * $query->filterByQtdeTotalContratos(1234); // WHERE qtde_total_contratos = 1234
     * $query->filterByQtdeTotalContratos(array(12, 34)); // WHERE qtde_total_contratos IN (12, 34)
     * $query->filterByQtdeTotalContratos(array('min' => 12)); // WHERE qtde_total_contratos > 12
     * </code>
     *
     * @param     mixed $qtdeTotalContratos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByQtdeTotalContratos($qtdeTotalContratos = null, $comparison = null)
    {
        if (is_array($qtdeTotalContratos)) {
            $useMinMax = false;
            if (isset($qtdeTotalContratos['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS, $qtdeTotalContratos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($qtdeTotalContratos['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS, $qtdeTotalContratos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS, $qtdeTotalContratos, $comparison);
    }

    /**
     * Filter the query on the valor_total_contratos column
     *
     * Example usage:
     * <code>
     * $query->filterByValorTotalContratos(1234); // WHERE valor_total_contratos = 1234
     * $query->filterByValorTotalContratos(array(12, 34)); // WHERE valor_total_contratos IN (12, 34)
     * $query->filterByValorTotalContratos(array('min' => 12)); // WHERE valor_total_contratos > 12
     * </code>
     *
     * @param     mixed $valorTotalContratos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByValorTotalContratos($valorTotalContratos = null, $comparison = null)
    {
        if (is_array($valorTotalContratos)) {
            $useMinMax = false;
            if (isset($valorTotalContratos['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS, $valorTotalContratos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorTotalContratos['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS, $valorTotalContratos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS, $valorTotalContratos, $comparison);
    }

    /**
     * Filter the query on the qtde_total_contratos_pj column
     *
     * Example usage:
     * <code>
     * $query->filterByQtdeTotalContratosPj(1234); // WHERE qtde_total_contratos_pj = 1234
     * $query->filterByQtdeTotalContratosPj(array(12, 34)); // WHERE qtde_total_contratos_pj IN (12, 34)
     * $query->filterByQtdeTotalContratosPj(array('min' => 12)); // WHERE qtde_total_contratos_pj > 12
     * </code>
     *
     * @param     mixed $qtdeTotalContratosPj The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByQtdeTotalContratosPj($qtdeTotalContratosPj = null, $comparison = null)
    {
        if (is_array($qtdeTotalContratosPj)) {
            $useMinMax = false;
            if (isset($qtdeTotalContratosPj['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ, $qtdeTotalContratosPj['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($qtdeTotalContratosPj['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ, $qtdeTotalContratosPj['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ, $qtdeTotalContratosPj, $comparison);
    }

    /**
     * Filter the query on the valor_total_contratos_pj column
     *
     * Example usage:
     * <code>
     * $query->filterByValorTotalContratosPj(1234); // WHERE valor_total_contratos_pj = 1234
     * $query->filterByValorTotalContratosPj(array(12, 34)); // WHERE valor_total_contratos_pj IN (12, 34)
     * $query->filterByValorTotalContratosPj(array('min' => 12)); // WHERE valor_total_contratos_pj > 12
     * </code>
     *
     * @param     mixed $valorTotalContratosPj The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByValorTotalContratosPj($valorTotalContratosPj = null, $comparison = null)
    {
        if (is_array($valorTotalContratosPj)) {
            $useMinMax = false;
            if (isset($valorTotalContratosPj['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ, $valorTotalContratosPj['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorTotalContratosPj['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ, $valorTotalContratosPj['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ, $valorTotalContratosPj, $comparison);
    }

    /**
     * Filter the query on the qtde_total_contratos_pf column
     *
     * Example usage:
     * <code>
     * $query->filterByQtdeTotalContratosPf(1234); // WHERE qtde_total_contratos_pf = 1234
     * $query->filterByQtdeTotalContratosPf(array(12, 34)); // WHERE qtde_total_contratos_pf IN (12, 34)
     * $query->filterByQtdeTotalContratosPf(array('min' => 12)); // WHERE qtde_total_contratos_pf > 12
     * </code>
     *
     * @param     mixed $qtdeTotalContratosPf The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByQtdeTotalContratosPf($qtdeTotalContratosPf = null, $comparison = null)
    {
        if (is_array($qtdeTotalContratosPf)) {
            $useMinMax = false;
            if (isset($qtdeTotalContratosPf['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF, $qtdeTotalContratosPf['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($qtdeTotalContratosPf['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF, $qtdeTotalContratosPf['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF, $qtdeTotalContratosPf, $comparison);
    }

    /**
     * Filter the query on the valor_total_contratos_pf column
     *
     * Example usage:
     * <code>
     * $query->filterByValorTotalContratosPf(1234); // WHERE valor_total_contratos_pf = 1234
     * $query->filterByValorTotalContratosPf(array(12, 34)); // WHERE valor_total_contratos_pf IN (12, 34)
     * $query->filterByValorTotalContratosPf(array('min' => 12)); // WHERE valor_total_contratos_pf > 12
     * </code>
     *
     * @param     mixed $valorTotalContratosPf The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByValorTotalContratosPf($valorTotalContratosPf = null, $comparison = null)
    {
        if (is_array($valorTotalContratosPf)) {
            $useMinMax = false;
            if (isset($valorTotalContratosPf['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF, $valorTotalContratosPf['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorTotalContratosPf['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF, $valorTotalContratosPf['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF, $valorTotalContratosPf, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(FaturamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FaturamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Competencia object
     *
     * @param \ImaTelecomBundle\Model\Competencia|ObjectCollection $competencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFaturamentoQuery The current query, for fluid interface
     */
    public function filterByCompetencia($competencia, $comparison = null)
    {
        if ($competencia instanceof \ImaTelecomBundle\Model\Competencia) {
            return $this
                ->addUsingAlias(FaturamentoTableMap::COL_COMPETENCIA_ID, $competencia->getId(), $comparison);
        } elseif ($competencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FaturamentoTableMap::COL_COMPETENCIA_ID, $competencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompetencia() only accepts arguments of type \ImaTelecomBundle\Model\Competencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Competencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function joinCompetencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Competencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Competencia');
        }

        return $this;
    }

    /**
     * Use the Competencia relation Competencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CompetenciaQuery A secondary query class using the current class as primary query
     */
    public function useCompetenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompetencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Competencia', '\ImaTelecomBundle\Model\CompetenciaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFaturamento $faturamento Object to remove from the list of results
     *
     * @return $this|ChildFaturamentoQuery The current query, for fluid interface
     */
    public function prune($faturamento = null)
    {
        if ($faturamento) {
            $this->addUsingAlias(FaturamentoTableMap::COL_IDFATURAMENTO, $faturamento->getIdfaturamento(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the faturamento table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FaturamentoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FaturamentoTableMap::clearInstancePool();
            FaturamentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FaturamentoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FaturamentoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FaturamentoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FaturamentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FaturamentoQuery
