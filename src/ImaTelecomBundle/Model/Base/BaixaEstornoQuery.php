<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BaixaEstorno as ChildBaixaEstorno;
use ImaTelecomBundle\Model\BaixaEstornoQuery as ChildBaixaEstornoQuery;
use ImaTelecomBundle\Model\Map\BaixaEstornoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'baixa_estorno' table.
 *
 *
 *
 * @method     ChildBaixaEstornoQuery orderByIdbaixaEstorno($order = Criteria::ASC) Order by the idbaixa_estorno column
 * @method     ChildBaixaEstornoQuery orderByBaixaId($order = Criteria::ASC) Order by the baixa_id column
 * @method     ChildBaixaEstornoQuery orderByContaCaixaId($order = Criteria::ASC) Order by the conta_caixa_id column
 * @method     ChildBaixaEstornoQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildBaixaEstornoQuery orderByUsuarioEstorno($order = Criteria::ASC) Order by the usuario_estorno column
 * @method     ChildBaixaEstornoQuery orderByDataEstorno($order = Criteria::ASC) Order by the data_estorno column
 * @method     ChildBaixaEstornoQuery orderByDescricaoMovimento($order = Criteria::ASC) Order by the descricao_movimento column
 * @method     ChildBaixaEstornoQuery orderByValorOriginal($order = Criteria::ASC) Order by the valor_original column
 * @method     ChildBaixaEstornoQuery orderByValorMulta($order = Criteria::ASC) Order by the valor_multa column
 * @method     ChildBaixaEstornoQuery orderByValorJuros($order = Criteria::ASC) Order by the valor_juros column
 * @method     ChildBaixaEstornoQuery orderByValorDesconto($order = Criteria::ASC) Order by the valor_desconto column
 * @method     ChildBaixaEstornoQuery orderByValorTotal($order = Criteria::ASC) Order by the valor_total column
 * @method     ChildBaixaEstornoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildBaixaEstornoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 *
 * @method     ChildBaixaEstornoQuery groupByIdbaixaEstorno() Group by the idbaixa_estorno column
 * @method     ChildBaixaEstornoQuery groupByBaixaId() Group by the baixa_id column
 * @method     ChildBaixaEstornoQuery groupByContaCaixaId() Group by the conta_caixa_id column
 * @method     ChildBaixaEstornoQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildBaixaEstornoQuery groupByUsuarioEstorno() Group by the usuario_estorno column
 * @method     ChildBaixaEstornoQuery groupByDataEstorno() Group by the data_estorno column
 * @method     ChildBaixaEstornoQuery groupByDescricaoMovimento() Group by the descricao_movimento column
 * @method     ChildBaixaEstornoQuery groupByValorOriginal() Group by the valor_original column
 * @method     ChildBaixaEstornoQuery groupByValorMulta() Group by the valor_multa column
 * @method     ChildBaixaEstornoQuery groupByValorJuros() Group by the valor_juros column
 * @method     ChildBaixaEstornoQuery groupByValorDesconto() Group by the valor_desconto column
 * @method     ChildBaixaEstornoQuery groupByValorTotal() Group by the valor_total column
 * @method     ChildBaixaEstornoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildBaixaEstornoQuery groupByDataAlterado() Group by the data_alterado column
 *
 * @method     ChildBaixaEstornoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBaixaEstornoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBaixaEstornoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBaixaEstornoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBaixaEstornoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBaixaEstornoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBaixaEstornoQuery leftJoinBaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Baixa relation
 * @method     ChildBaixaEstornoQuery rightJoinBaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Baixa relation
 * @method     ChildBaixaEstornoQuery innerJoinBaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the Baixa relation
 *
 * @method     ChildBaixaEstornoQuery joinWithBaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Baixa relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinWithBaixa() Adds a LEFT JOIN clause and with to the query using the Baixa relation
 * @method     ChildBaixaEstornoQuery rightJoinWithBaixa() Adds a RIGHT JOIN clause and with to the query using the Baixa relation
 * @method     ChildBaixaEstornoQuery innerJoinWithBaixa() Adds a INNER JOIN clause and with to the query using the Baixa relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinCompetencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Competencia relation
 * @method     ChildBaixaEstornoQuery rightJoinCompetencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Competencia relation
 * @method     ChildBaixaEstornoQuery innerJoinCompetencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Competencia relation
 *
 * @method     ChildBaixaEstornoQuery joinWithCompetencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Competencia relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinWithCompetencia() Adds a LEFT JOIN clause and with to the query using the Competencia relation
 * @method     ChildBaixaEstornoQuery rightJoinWithCompetencia() Adds a RIGHT JOIN clause and with to the query using the Competencia relation
 * @method     ChildBaixaEstornoQuery innerJoinWithCompetencia() Adds a INNER JOIN clause and with to the query using the Competencia relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinContaCaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContaCaixa relation
 * @method     ChildBaixaEstornoQuery rightJoinContaCaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContaCaixa relation
 * @method     ChildBaixaEstornoQuery innerJoinContaCaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the ContaCaixa relation
 *
 * @method     ChildBaixaEstornoQuery joinWithContaCaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContaCaixa relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinWithContaCaixa() Adds a LEFT JOIN clause and with to the query using the ContaCaixa relation
 * @method     ChildBaixaEstornoQuery rightJoinWithContaCaixa() Adds a RIGHT JOIN clause and with to the query using the ContaCaixa relation
 * @method     ChildBaixaEstornoQuery innerJoinWithContaCaixa() Adds a INNER JOIN clause and with to the query using the ContaCaixa relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBaixaEstornoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBaixaEstornoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBaixaEstornoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBaixaEstornoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBaixaEstornoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildBaixaEstornoQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildBaixaEstornoQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildBaixaEstornoQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildBaixaEstornoQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildBaixaEstornoQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinBoletoBaixaHistorico($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildBaixaEstornoQuery rightJoinBoletoBaixaHistorico($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoBaixaHistorico relation
 * @method     ChildBaixaEstornoQuery innerJoinBoletoBaixaHistorico($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildBaixaEstornoQuery joinWithBoletoBaixaHistorico($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     ChildBaixaEstornoQuery leftJoinWithBoletoBaixaHistorico() Adds a LEFT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildBaixaEstornoQuery rightJoinWithBoletoBaixaHistorico() Adds a RIGHT JOIN clause and with to the query using the BoletoBaixaHistorico relation
 * @method     ChildBaixaEstornoQuery innerJoinWithBoletoBaixaHistorico() Adds a INNER JOIN clause and with to the query using the BoletoBaixaHistorico relation
 *
 * @method     \ImaTelecomBundle\Model\BaixaQuery|\ImaTelecomBundle\Model\CompetenciaQuery|\ImaTelecomBundle\Model\ContaCaixaQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBaixaEstorno findOne(ConnectionInterface $con = null) Return the first ChildBaixaEstorno matching the query
 * @method     ChildBaixaEstorno findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBaixaEstorno matching the query, or a new ChildBaixaEstorno object populated from the query conditions when no match is found
 *
 * @method     ChildBaixaEstorno findOneByIdbaixaEstorno(int $idbaixa_estorno) Return the first ChildBaixaEstorno filtered by the idbaixa_estorno column
 * @method     ChildBaixaEstorno findOneByBaixaId(int $baixa_id) Return the first ChildBaixaEstorno filtered by the baixa_id column
 * @method     ChildBaixaEstorno findOneByContaCaixaId(int $conta_caixa_id) Return the first ChildBaixaEstorno filtered by the conta_caixa_id column
 * @method     ChildBaixaEstorno findOneByCompetenciaId(int $competencia_id) Return the first ChildBaixaEstorno filtered by the competencia_id column
 * @method     ChildBaixaEstorno findOneByUsuarioEstorno(int $usuario_estorno) Return the first ChildBaixaEstorno filtered by the usuario_estorno column
 * @method     ChildBaixaEstorno findOneByDataEstorno(string $data_estorno) Return the first ChildBaixaEstorno filtered by the data_estorno column
 * @method     ChildBaixaEstorno findOneByDescricaoMovimento(string $descricao_movimento) Return the first ChildBaixaEstorno filtered by the descricao_movimento column
 * @method     ChildBaixaEstorno findOneByValorOriginal(string $valor_original) Return the first ChildBaixaEstorno filtered by the valor_original column
 * @method     ChildBaixaEstorno findOneByValorMulta(string $valor_multa) Return the first ChildBaixaEstorno filtered by the valor_multa column
 * @method     ChildBaixaEstorno findOneByValorJuros(string $valor_juros) Return the first ChildBaixaEstorno filtered by the valor_juros column
 * @method     ChildBaixaEstorno findOneByValorDesconto(string $valor_desconto) Return the first ChildBaixaEstorno filtered by the valor_desconto column
 * @method     ChildBaixaEstorno findOneByValorTotal(string $valor_total) Return the first ChildBaixaEstorno filtered by the valor_total column
 * @method     ChildBaixaEstorno findOneByDataCadastro(string $data_cadastro) Return the first ChildBaixaEstorno filtered by the data_cadastro column
 * @method     ChildBaixaEstorno findOneByDataAlterado(string $data_alterado) Return the first ChildBaixaEstorno filtered by the data_alterado column *

 * @method     ChildBaixaEstorno requirePk($key, ConnectionInterface $con = null) Return the ChildBaixaEstorno by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOne(ConnectionInterface $con = null) Return the first ChildBaixaEstorno matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBaixaEstorno requireOneByIdbaixaEstorno(int $idbaixa_estorno) Return the first ChildBaixaEstorno filtered by the idbaixa_estorno column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByBaixaId(int $baixa_id) Return the first ChildBaixaEstorno filtered by the baixa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByContaCaixaId(int $conta_caixa_id) Return the first ChildBaixaEstorno filtered by the conta_caixa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByCompetenciaId(int $competencia_id) Return the first ChildBaixaEstorno filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByUsuarioEstorno(int $usuario_estorno) Return the first ChildBaixaEstorno filtered by the usuario_estorno column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByDataEstorno(string $data_estorno) Return the first ChildBaixaEstorno filtered by the data_estorno column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByDescricaoMovimento(string $descricao_movimento) Return the first ChildBaixaEstorno filtered by the descricao_movimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByValorOriginal(string $valor_original) Return the first ChildBaixaEstorno filtered by the valor_original column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByValorMulta(string $valor_multa) Return the first ChildBaixaEstorno filtered by the valor_multa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByValorJuros(string $valor_juros) Return the first ChildBaixaEstorno filtered by the valor_juros column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByValorDesconto(string $valor_desconto) Return the first ChildBaixaEstorno filtered by the valor_desconto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByValorTotal(string $valor_total) Return the first ChildBaixaEstorno filtered by the valor_total column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByDataCadastro(string $data_cadastro) Return the first ChildBaixaEstorno filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBaixaEstorno requireOneByDataAlterado(string $data_alterado) Return the first ChildBaixaEstorno filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBaixaEstorno[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBaixaEstorno objects based on current ModelCriteria
 * @method     ChildBaixaEstorno[]|ObjectCollection findByIdbaixaEstorno(int $idbaixa_estorno) Return ChildBaixaEstorno objects filtered by the idbaixa_estorno column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByBaixaId(int $baixa_id) Return ChildBaixaEstorno objects filtered by the baixa_id column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByContaCaixaId(int $conta_caixa_id) Return ChildBaixaEstorno objects filtered by the conta_caixa_id column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildBaixaEstorno objects filtered by the competencia_id column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByUsuarioEstorno(int $usuario_estorno) Return ChildBaixaEstorno objects filtered by the usuario_estorno column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByDataEstorno(string $data_estorno) Return ChildBaixaEstorno objects filtered by the data_estorno column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByDescricaoMovimento(string $descricao_movimento) Return ChildBaixaEstorno objects filtered by the descricao_movimento column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByValorOriginal(string $valor_original) Return ChildBaixaEstorno objects filtered by the valor_original column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByValorMulta(string $valor_multa) Return ChildBaixaEstorno objects filtered by the valor_multa column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByValorJuros(string $valor_juros) Return ChildBaixaEstorno objects filtered by the valor_juros column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByValorDesconto(string $valor_desconto) Return ChildBaixaEstorno objects filtered by the valor_desconto column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByValorTotal(string $valor_total) Return ChildBaixaEstorno objects filtered by the valor_total column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildBaixaEstorno objects filtered by the data_cadastro column
 * @method     ChildBaixaEstorno[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildBaixaEstorno objects filtered by the data_alterado column
 * @method     ChildBaixaEstorno[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BaixaEstornoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\BaixaEstornoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\BaixaEstorno', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBaixaEstornoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBaixaEstornoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBaixaEstornoQuery) {
            return $criteria;
        }
        $query = new ChildBaixaEstornoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBaixaEstorno|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BaixaEstornoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BaixaEstornoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaEstorno A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idbaixa_estorno, baixa_id, conta_caixa_id, competencia_id, usuario_estorno, data_estorno, descricao_movimento, valor_original, valor_multa, valor_juros, valor_desconto, valor_total, data_cadastro, data_alterado FROM baixa_estorno WHERE idbaixa_estorno = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBaixaEstorno $obj */
            $obj = new ChildBaixaEstorno();
            $obj->hydrate($row);
            BaixaEstornoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBaixaEstorno|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idbaixa_estorno column
     *
     * Example usage:
     * <code>
     * $query->filterByIdbaixaEstorno(1234); // WHERE idbaixa_estorno = 1234
     * $query->filterByIdbaixaEstorno(array(12, 34)); // WHERE idbaixa_estorno IN (12, 34)
     * $query->filterByIdbaixaEstorno(array('min' => 12)); // WHERE idbaixa_estorno > 12
     * </code>
     *
     * @param     mixed $idbaixaEstorno The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByIdbaixaEstorno($idbaixaEstorno = null, $comparison = null)
    {
        if (is_array($idbaixaEstorno)) {
            $useMinMax = false;
            if (isset($idbaixaEstorno['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $idbaixaEstorno['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idbaixaEstorno['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $idbaixaEstorno['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $idbaixaEstorno, $comparison);
    }

    /**
     * Filter the query on the baixa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBaixaId(1234); // WHERE baixa_id = 1234
     * $query->filterByBaixaId(array(12, 34)); // WHERE baixa_id IN (12, 34)
     * $query->filterByBaixaId(array('min' => 12)); // WHERE baixa_id > 12
     * </code>
     *
     * @see       filterByBaixa()
     *
     * @param     mixed $baixaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByBaixaId($baixaId = null, $comparison = null)
    {
        if (is_array($baixaId)) {
            $useMinMax = false;
            if (isset($baixaId['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_BAIXA_ID, $baixaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baixaId['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_BAIXA_ID, $baixaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_BAIXA_ID, $baixaId, $comparison);
    }

    /**
     * Filter the query on the conta_caixa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContaCaixaId(1234); // WHERE conta_caixa_id = 1234
     * $query->filterByContaCaixaId(array(12, 34)); // WHERE conta_caixa_id IN (12, 34)
     * $query->filterByContaCaixaId(array('min' => 12)); // WHERE conta_caixa_id > 12
     * </code>
     *
     * @see       filterByContaCaixa()
     *
     * @param     mixed $contaCaixaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByContaCaixaId($contaCaixaId = null, $comparison = null)
    {
        if (is_array($contaCaixaId)) {
            $useMinMax = false;
            if (isset($contaCaixaId['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_CONTA_CAIXA_ID, $contaCaixaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contaCaixaId['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_CONTA_CAIXA_ID, $contaCaixaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_CONTA_CAIXA_ID, $contaCaixaId, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @see       filterByCompetencia()
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the usuario_estorno column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioEstorno(1234); // WHERE usuario_estorno = 1234
     * $query->filterByUsuarioEstorno(array(12, 34)); // WHERE usuario_estorno IN (12, 34)
     * $query->filterByUsuarioEstorno(array('min' => 12)); // WHERE usuario_estorno > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioEstorno The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByUsuarioEstorno($usuarioEstorno = null, $comparison = null)
    {
        if (is_array($usuarioEstorno)) {
            $useMinMax = false;
            if (isset($usuarioEstorno['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_USUARIO_ESTORNO, $usuarioEstorno['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioEstorno['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_USUARIO_ESTORNO, $usuarioEstorno['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_USUARIO_ESTORNO, $usuarioEstorno, $comparison);
    }

    /**
     * Filter the query on the data_estorno column
     *
     * Example usage:
     * <code>
     * $query->filterByDataEstorno('2011-03-14'); // WHERE data_estorno = '2011-03-14'
     * $query->filterByDataEstorno('now'); // WHERE data_estorno = '2011-03-14'
     * $query->filterByDataEstorno(array('max' => 'yesterday')); // WHERE data_estorno > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataEstorno The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByDataEstorno($dataEstorno = null, $comparison = null)
    {
        if (is_array($dataEstorno)) {
            $useMinMax = false;
            if (isset($dataEstorno['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_DATA_ESTORNO, $dataEstorno['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataEstorno['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_DATA_ESTORNO, $dataEstorno['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_DATA_ESTORNO, $dataEstorno, $comparison);
    }

    /**
     * Filter the query on the descricao_movimento column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricaoMovimento('fooValue');   // WHERE descricao_movimento = 'fooValue'
     * $query->filterByDescricaoMovimento('%fooValue%', Criteria::LIKE); // WHERE descricao_movimento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricaoMovimento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByDescricaoMovimento($descricaoMovimento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricaoMovimento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_DESCRICAO_MOVIMENTO, $descricaoMovimento, $comparison);
    }

    /**
     * Filter the query on the valor_original column
     *
     * Example usage:
     * <code>
     * $query->filterByValorOriginal(1234); // WHERE valor_original = 1234
     * $query->filterByValorOriginal(array(12, 34)); // WHERE valor_original IN (12, 34)
     * $query->filterByValorOriginal(array('min' => 12)); // WHERE valor_original > 12
     * </code>
     *
     * @param     mixed $valorOriginal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByValorOriginal($valorOriginal = null, $comparison = null)
    {
        if (is_array($valorOriginal)) {
            $useMinMax = false;
            if (isset($valorOriginal['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_ORIGINAL, $valorOriginal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorOriginal['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_ORIGINAL, $valorOriginal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_ORIGINAL, $valorOriginal, $comparison);
    }

    /**
     * Filter the query on the valor_multa column
     *
     * Example usage:
     * <code>
     * $query->filterByValorMulta(1234); // WHERE valor_multa = 1234
     * $query->filterByValorMulta(array(12, 34)); // WHERE valor_multa IN (12, 34)
     * $query->filterByValorMulta(array('min' => 12)); // WHERE valor_multa > 12
     * </code>
     *
     * @param     mixed $valorMulta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByValorMulta($valorMulta = null, $comparison = null)
    {
        if (is_array($valorMulta)) {
            $useMinMax = false;
            if (isset($valorMulta['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_MULTA, $valorMulta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorMulta['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_MULTA, $valorMulta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_MULTA, $valorMulta, $comparison);
    }

    /**
     * Filter the query on the valor_juros column
     *
     * Example usage:
     * <code>
     * $query->filterByValorJuros(1234); // WHERE valor_juros = 1234
     * $query->filterByValorJuros(array(12, 34)); // WHERE valor_juros IN (12, 34)
     * $query->filterByValorJuros(array('min' => 12)); // WHERE valor_juros > 12
     * </code>
     *
     * @param     mixed $valorJuros The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByValorJuros($valorJuros = null, $comparison = null)
    {
        if (is_array($valorJuros)) {
            $useMinMax = false;
            if (isset($valorJuros['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_JUROS, $valorJuros['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorJuros['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_JUROS, $valorJuros['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_JUROS, $valorJuros, $comparison);
    }

    /**
     * Filter the query on the valor_desconto column
     *
     * Example usage:
     * <code>
     * $query->filterByValorDesconto(1234); // WHERE valor_desconto = 1234
     * $query->filterByValorDesconto(array(12, 34)); // WHERE valor_desconto IN (12, 34)
     * $query->filterByValorDesconto(array('min' => 12)); // WHERE valor_desconto > 12
     * </code>
     *
     * @param     mixed $valorDesconto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByValorDesconto($valorDesconto = null, $comparison = null)
    {
        if (is_array($valorDesconto)) {
            $useMinMax = false;
            if (isset($valorDesconto['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_DESCONTO, $valorDesconto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorDesconto['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_DESCONTO, $valorDesconto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_DESCONTO, $valorDesconto, $comparison);
    }

    /**
     * Filter the query on the valor_total column
     *
     * Example usage:
     * <code>
     * $query->filterByValorTotal(1234); // WHERE valor_total = 1234
     * $query->filterByValorTotal(array(12, 34)); // WHERE valor_total IN (12, 34)
     * $query->filterByValorTotal(array('min' => 12)); // WHERE valor_total > 12
     * </code>
     *
     * @param     mixed $valorTotal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByValorTotal($valorTotal = null, $comparison = null)
    {
        if (is_array($valorTotal)) {
            $useMinMax = false;
            if (isset($valorTotal['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_TOTAL, $valorTotal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorTotal['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_TOTAL, $valorTotal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_VALOR_TOTAL, $valorTotal, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(BaixaEstornoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BaixaEstornoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Baixa object
     *
     * @param \ImaTelecomBundle\Model\Baixa|ObjectCollection $baixa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByBaixa($baixa, $comparison = null)
    {
        if ($baixa instanceof \ImaTelecomBundle\Model\Baixa) {
            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_BAIXA_ID, $baixa->getIdbaixa(), $comparison);
        } elseif ($baixa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_BAIXA_ID, $baixa->toKeyValue('PrimaryKey', 'Idbaixa'), $comparison);
        } else {
            throw new PropelException('filterByBaixa() only accepts arguments of type \ImaTelecomBundle\Model\Baixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Baixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function joinBaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Baixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Baixa');
        }

        return $this;
    }

    /**
     * Use the Baixa relation Baixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BaixaQuery A secondary query class using the current class as primary query
     */
    public function useBaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Baixa', '\ImaTelecomBundle\Model\BaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Competencia object
     *
     * @param \ImaTelecomBundle\Model\Competencia|ObjectCollection $competencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByCompetencia($competencia, $comparison = null)
    {
        if ($competencia instanceof \ImaTelecomBundle\Model\Competencia) {
            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_COMPETENCIA_ID, $competencia->getId(), $comparison);
        } elseif ($competencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_COMPETENCIA_ID, $competencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompetencia() only accepts arguments of type \ImaTelecomBundle\Model\Competencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Competencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function joinCompetencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Competencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Competencia');
        }

        return $this;
    }

    /**
     * Use the Competencia relation Competencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CompetenciaQuery A secondary query class using the current class as primary query
     */
    public function useCompetenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompetencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Competencia', '\ImaTelecomBundle\Model\CompetenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContaCaixa object
     *
     * @param \ImaTelecomBundle\Model\ContaCaixa|ObjectCollection $contaCaixa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByContaCaixa($contaCaixa, $comparison = null)
    {
        if ($contaCaixa instanceof \ImaTelecomBundle\Model\ContaCaixa) {
            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_CONTA_CAIXA_ID, $contaCaixa->getIdcontaCaixa(), $comparison);
        } elseif ($contaCaixa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_CONTA_CAIXA_ID, $contaCaixa->toKeyValue('PrimaryKey', 'IdcontaCaixa'), $comparison);
        } else {
            throw new PropelException('filterByContaCaixa() only accepts arguments of type \ImaTelecomBundle\Model\ContaCaixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContaCaixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function joinContaCaixa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContaCaixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContaCaixa');
        }

        return $this;
    }

    /**
     * Use the ContaCaixa relation ContaCaixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContaCaixaQuery A secondary query class using the current class as primary query
     */
    public function useContaCaixaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContaCaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContaCaixa', '\ImaTelecomBundle\Model\ContaCaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_USUARIO_ESTORNO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_USUARIO_ESTORNO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $boleto->getBaixaEstornoId(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            return $this
                ->useBoletoQuery()
                ->filterByPrimaryKeys($boleto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoBaixaHistorico object
     *
     * @param \ImaTelecomBundle\Model\BoletoBaixaHistorico|ObjectCollection $boletoBaixaHistorico the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function filterByBoletoBaixaHistorico($boletoBaixaHistorico, $comparison = null)
    {
        if ($boletoBaixaHistorico instanceof \ImaTelecomBundle\Model\BoletoBaixaHistorico) {
            return $this
                ->addUsingAlias(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $boletoBaixaHistorico->getBaixaEstornoId(), $comparison);
        } elseif ($boletoBaixaHistorico instanceof ObjectCollection) {
            return $this
                ->useBoletoBaixaHistoricoQuery()
                ->filterByPrimaryKeys($boletoBaixaHistorico->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoletoBaixaHistorico() only accepts arguments of type \ImaTelecomBundle\Model\BoletoBaixaHistorico or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoBaixaHistorico relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function joinBoletoBaixaHistorico($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoBaixaHistorico');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoBaixaHistorico');
        }

        return $this;
    }

    /**
     * Use the BoletoBaixaHistorico relation BoletoBaixaHistorico object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoBaixaHistoricoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBoletoBaixaHistorico($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoBaixaHistorico', '\ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBaixaEstorno $baixaEstorno Object to remove from the list of results
     *
     * @return $this|ChildBaixaEstornoQuery The current query, for fluid interface
     */
    public function prune($baixaEstorno = null)
    {
        if ($baixaEstorno) {
            $this->addUsingAlias(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, $baixaEstorno->getIdbaixaEstorno(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the baixa_estorno table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaEstornoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BaixaEstornoTableMap::clearInstancePool();
            BaixaEstornoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaEstornoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BaixaEstornoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BaixaEstornoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BaixaEstornoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BaixaEstornoQuery
