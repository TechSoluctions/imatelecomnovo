<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\EnderecoCliente as ChildEnderecoCliente;
use ImaTelecomBundle\Model\EnderecoClienteQuery as ChildEnderecoClienteQuery;
use ImaTelecomBundle\Model\Map\EnderecoClienteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'endereco_cliente' table.
 *
 *
 *
 * @method     ChildEnderecoClienteQuery orderByIdenderecoCliente($order = Criteria::ASC) Order by the idendereco_cliente column
 * @method     ChildEnderecoClienteQuery orderByClienteId($order = Criteria::ASC) Order by the cliente_id column
 * @method     ChildEnderecoClienteQuery orderByRua($order = Criteria::ASC) Order by the rua column
 * @method     ChildEnderecoClienteQuery orderByBairro($order = Criteria::ASC) Order by the bairro column
 * @method     ChildEnderecoClienteQuery orderByNum($order = Criteria::ASC) Order by the num column
 * @method     ChildEnderecoClienteQuery orderByComplemento($order = Criteria::ASC) Order by the complemento column
 * @method     ChildEnderecoClienteQuery orderByCidadeId($order = Criteria::ASC) Order by the cidade_id column
 * @method     ChildEnderecoClienteQuery orderByCep($order = Criteria::ASC) Order by the cep column
 * @method     ChildEnderecoClienteQuery orderByUf($order = Criteria::ASC) Order by the uf column
 * @method     ChildEnderecoClienteQuery orderByPontoReferencia($order = Criteria::ASC) Order by the ponto_referencia column
 * @method     ChildEnderecoClienteQuery orderByContato($order = Criteria::ASC) Order by the contato column
 * @method     ChildEnderecoClienteQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildEnderecoClienteQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildEnderecoClienteQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildEnderecoClienteQuery orderByImportId($order = Criteria::ASC) Order by the import_id column
 *
 * @method     ChildEnderecoClienteQuery groupByIdenderecoCliente() Group by the idendereco_cliente column
 * @method     ChildEnderecoClienteQuery groupByClienteId() Group by the cliente_id column
 * @method     ChildEnderecoClienteQuery groupByRua() Group by the rua column
 * @method     ChildEnderecoClienteQuery groupByBairro() Group by the bairro column
 * @method     ChildEnderecoClienteQuery groupByNum() Group by the num column
 * @method     ChildEnderecoClienteQuery groupByComplemento() Group by the complemento column
 * @method     ChildEnderecoClienteQuery groupByCidadeId() Group by the cidade_id column
 * @method     ChildEnderecoClienteQuery groupByCep() Group by the cep column
 * @method     ChildEnderecoClienteQuery groupByUf() Group by the uf column
 * @method     ChildEnderecoClienteQuery groupByPontoReferencia() Group by the ponto_referencia column
 * @method     ChildEnderecoClienteQuery groupByContato() Group by the contato column
 * @method     ChildEnderecoClienteQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildEnderecoClienteQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildEnderecoClienteQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildEnderecoClienteQuery groupByImportId() Group by the import_id column
 *
 * @method     ChildEnderecoClienteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEnderecoClienteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEnderecoClienteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEnderecoClienteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEnderecoClienteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEnderecoClienteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEnderecoClienteQuery leftJoinCidade($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cidade relation
 * @method     ChildEnderecoClienteQuery rightJoinCidade($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cidade relation
 * @method     ChildEnderecoClienteQuery innerJoinCidade($relationAlias = null) Adds a INNER JOIN clause to the query using the Cidade relation
 *
 * @method     ChildEnderecoClienteQuery joinWithCidade($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cidade relation
 *
 * @method     ChildEnderecoClienteQuery leftJoinWithCidade() Adds a LEFT JOIN clause and with to the query using the Cidade relation
 * @method     ChildEnderecoClienteQuery rightJoinWithCidade() Adds a RIGHT JOIN clause and with to the query using the Cidade relation
 * @method     ChildEnderecoClienteQuery innerJoinWithCidade() Adds a INNER JOIN clause and with to the query using the Cidade relation
 *
 * @method     ChildEnderecoClienteQuery leftJoinCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cliente relation
 * @method     ChildEnderecoClienteQuery rightJoinCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cliente relation
 * @method     ChildEnderecoClienteQuery innerJoinCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the Cliente relation
 *
 * @method     ChildEnderecoClienteQuery joinWithCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cliente relation
 *
 * @method     ChildEnderecoClienteQuery leftJoinWithCliente() Adds a LEFT JOIN clause and with to the query using the Cliente relation
 * @method     ChildEnderecoClienteQuery rightJoinWithCliente() Adds a RIGHT JOIN clause and with to the query using the Cliente relation
 * @method     ChildEnderecoClienteQuery innerJoinWithCliente() Adds a INNER JOIN clause and with to the query using the Cliente relation
 *
 * @method     ChildEnderecoClienteQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildEnderecoClienteQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildEnderecoClienteQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildEnderecoClienteQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildEnderecoClienteQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEnderecoClienteQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEnderecoClienteQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildEnderecoClienteQuery leftJoinContrato($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contrato relation
 * @method     ChildEnderecoClienteQuery rightJoinContrato($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contrato relation
 * @method     ChildEnderecoClienteQuery innerJoinContrato($relationAlias = null) Adds a INNER JOIN clause to the query using the Contrato relation
 *
 * @method     ChildEnderecoClienteQuery joinWithContrato($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Contrato relation
 *
 * @method     ChildEnderecoClienteQuery leftJoinWithContrato() Adds a LEFT JOIN clause and with to the query using the Contrato relation
 * @method     ChildEnderecoClienteQuery rightJoinWithContrato() Adds a RIGHT JOIN clause and with to the query using the Contrato relation
 * @method     ChildEnderecoClienteQuery innerJoinWithContrato() Adds a INNER JOIN clause and with to the query using the Contrato relation
 *
 * @method     \ImaTelecomBundle\Model\CidadeQuery|\ImaTelecomBundle\Model\ClienteQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\ContratoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEnderecoCliente findOne(ConnectionInterface $con = null) Return the first ChildEnderecoCliente matching the query
 * @method     ChildEnderecoCliente findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEnderecoCliente matching the query, or a new ChildEnderecoCliente object populated from the query conditions when no match is found
 *
 * @method     ChildEnderecoCliente findOneByIdenderecoCliente(int $idendereco_cliente) Return the first ChildEnderecoCliente filtered by the idendereco_cliente column
 * @method     ChildEnderecoCliente findOneByClienteId(int $cliente_id) Return the first ChildEnderecoCliente filtered by the cliente_id column
 * @method     ChildEnderecoCliente findOneByRua(string $rua) Return the first ChildEnderecoCliente filtered by the rua column
 * @method     ChildEnderecoCliente findOneByBairro(string $bairro) Return the first ChildEnderecoCliente filtered by the bairro column
 * @method     ChildEnderecoCliente findOneByNum(int $num) Return the first ChildEnderecoCliente filtered by the num column
 * @method     ChildEnderecoCliente findOneByComplemento(string $complemento) Return the first ChildEnderecoCliente filtered by the complemento column
 * @method     ChildEnderecoCliente findOneByCidadeId(int $cidade_id) Return the first ChildEnderecoCliente filtered by the cidade_id column
 * @method     ChildEnderecoCliente findOneByCep(string $cep) Return the first ChildEnderecoCliente filtered by the cep column
 * @method     ChildEnderecoCliente findOneByUf(string $uf) Return the first ChildEnderecoCliente filtered by the uf column
 * @method     ChildEnderecoCliente findOneByPontoReferencia(string $ponto_referencia) Return the first ChildEnderecoCliente filtered by the ponto_referencia column
 * @method     ChildEnderecoCliente findOneByContato(string $contato) Return the first ChildEnderecoCliente filtered by the contato column
 * @method     ChildEnderecoCliente findOneByDataCadastro(string $data_cadastro) Return the first ChildEnderecoCliente filtered by the data_cadastro column
 * @method     ChildEnderecoCliente findOneByDataAlterado(string $data_alterado) Return the first ChildEnderecoCliente filtered by the data_alterado column
 * @method     ChildEnderecoCliente findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEnderecoCliente filtered by the usuario_alterado column
 * @method     ChildEnderecoCliente findOneByImportId(int $import_id) Return the first ChildEnderecoCliente filtered by the import_id column *

 * @method     ChildEnderecoCliente requirePk($key, ConnectionInterface $con = null) Return the ChildEnderecoCliente by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOne(ConnectionInterface $con = null) Return the first ChildEnderecoCliente matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEnderecoCliente requireOneByIdenderecoCliente(int $idendereco_cliente) Return the first ChildEnderecoCliente filtered by the idendereco_cliente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByClienteId(int $cliente_id) Return the first ChildEnderecoCliente filtered by the cliente_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByRua(string $rua) Return the first ChildEnderecoCliente filtered by the rua column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByBairro(string $bairro) Return the first ChildEnderecoCliente filtered by the bairro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByNum(int $num) Return the first ChildEnderecoCliente filtered by the num column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByComplemento(string $complemento) Return the first ChildEnderecoCliente filtered by the complemento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByCidadeId(int $cidade_id) Return the first ChildEnderecoCliente filtered by the cidade_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByCep(string $cep) Return the first ChildEnderecoCliente filtered by the cep column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByUf(string $uf) Return the first ChildEnderecoCliente filtered by the uf column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByPontoReferencia(string $ponto_referencia) Return the first ChildEnderecoCliente filtered by the ponto_referencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByContato(string $contato) Return the first ChildEnderecoCliente filtered by the contato column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByDataCadastro(string $data_cadastro) Return the first ChildEnderecoCliente filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByDataAlterado(string $data_alterado) Return the first ChildEnderecoCliente filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEnderecoCliente filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEnderecoCliente requireOneByImportId(int $import_id) Return the first ChildEnderecoCliente filtered by the import_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEnderecoCliente[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEnderecoCliente objects based on current ModelCriteria
 * @method     ChildEnderecoCliente[]|ObjectCollection findByIdenderecoCliente(int $idendereco_cliente) Return ChildEnderecoCliente objects filtered by the idendereco_cliente column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByClienteId(int $cliente_id) Return ChildEnderecoCliente objects filtered by the cliente_id column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByRua(string $rua) Return ChildEnderecoCliente objects filtered by the rua column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByBairro(string $bairro) Return ChildEnderecoCliente objects filtered by the bairro column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByNum(int $num) Return ChildEnderecoCliente objects filtered by the num column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByComplemento(string $complemento) Return ChildEnderecoCliente objects filtered by the complemento column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByCidadeId(int $cidade_id) Return ChildEnderecoCliente objects filtered by the cidade_id column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByCep(string $cep) Return ChildEnderecoCliente objects filtered by the cep column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByUf(string $uf) Return ChildEnderecoCliente objects filtered by the uf column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByPontoReferencia(string $ponto_referencia) Return ChildEnderecoCliente objects filtered by the ponto_referencia column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByContato(string $contato) Return ChildEnderecoCliente objects filtered by the contato column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildEnderecoCliente objects filtered by the data_cadastro column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildEnderecoCliente objects filtered by the data_alterado column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildEnderecoCliente objects filtered by the usuario_alterado column
 * @method     ChildEnderecoCliente[]|ObjectCollection findByImportId(int $import_id) Return ChildEnderecoCliente objects filtered by the import_id column
 * @method     ChildEnderecoCliente[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EnderecoClienteQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\EnderecoClienteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\EnderecoCliente', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEnderecoClienteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEnderecoClienteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEnderecoClienteQuery) {
            return $criteria;
        }
        $query = new ChildEnderecoClienteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEnderecoCliente|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EnderecoClienteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EnderecoClienteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEnderecoCliente A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idendereco_cliente, cliente_id, rua, bairro, num, complemento, cidade_id, cep, uf, ponto_referencia, contato, data_cadastro, data_alterado, usuario_alterado, import_id FROM endereco_cliente WHERE idendereco_cliente = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEnderecoCliente $obj */
            $obj = new ChildEnderecoCliente();
            $obj->hydrate($row);
            EnderecoClienteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEnderecoCliente|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_IDENDERECO_CLIENTE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_IDENDERECO_CLIENTE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idendereco_cliente column
     *
     * Example usage:
     * <code>
     * $query->filterByIdenderecoCliente(1234); // WHERE idendereco_cliente = 1234
     * $query->filterByIdenderecoCliente(array(12, 34)); // WHERE idendereco_cliente IN (12, 34)
     * $query->filterByIdenderecoCliente(array('min' => 12)); // WHERE idendereco_cliente > 12
     * </code>
     *
     * @param     mixed $idenderecoCliente The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByIdenderecoCliente($idenderecoCliente = null, $comparison = null)
    {
        if (is_array($idenderecoCliente)) {
            $useMinMax = false;
            if (isset($idenderecoCliente['min'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_IDENDERECO_CLIENTE, $idenderecoCliente['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idenderecoCliente['max'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_IDENDERECO_CLIENTE, $idenderecoCliente['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_IDENDERECO_CLIENTE, $idenderecoCliente, $comparison);
    }

    /**
     * Filter the query on the cliente_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClienteId(1234); // WHERE cliente_id = 1234
     * $query->filterByClienteId(array(12, 34)); // WHERE cliente_id IN (12, 34)
     * $query->filterByClienteId(array('min' => 12)); // WHERE cliente_id > 12
     * </code>
     *
     * @see       filterByCliente()
     *
     * @param     mixed $clienteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByClienteId($clienteId = null, $comparison = null)
    {
        if (is_array($clienteId)) {
            $useMinMax = false;
            if (isset($clienteId['min'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_CLIENTE_ID, $clienteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clienteId['max'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_CLIENTE_ID, $clienteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_CLIENTE_ID, $clienteId, $comparison);
    }

    /**
     * Filter the query on the rua column
     *
     * Example usage:
     * <code>
     * $query->filterByRua('fooValue');   // WHERE rua = 'fooValue'
     * $query->filterByRua('%fooValue%', Criteria::LIKE); // WHERE rua LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rua The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByRua($rua = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rua)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_RUA, $rua, $comparison);
    }

    /**
     * Filter the query on the bairro column
     *
     * Example usage:
     * <code>
     * $query->filterByBairro('fooValue');   // WHERE bairro = 'fooValue'
     * $query->filterByBairro('%fooValue%', Criteria::LIKE); // WHERE bairro LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bairro The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByBairro($bairro = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bairro)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_BAIRRO, $bairro, $comparison);
    }

    /**
     * Filter the query on the num column
     *
     * Example usage:
     * <code>
     * $query->filterByNum(1234); // WHERE num = 1234
     * $query->filterByNum(array(12, 34)); // WHERE num IN (12, 34)
     * $query->filterByNum(array('min' => 12)); // WHERE num > 12
     * </code>
     *
     * @param     mixed $num The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByNum($num = null, $comparison = null)
    {
        if (is_array($num)) {
            $useMinMax = false;
            if (isset($num['min'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_NUM, $num['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($num['max'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_NUM, $num['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_NUM, $num, $comparison);
    }

    /**
     * Filter the query on the complemento column
     *
     * Example usage:
     * <code>
     * $query->filterByComplemento('fooValue');   // WHERE complemento = 'fooValue'
     * $query->filterByComplemento('%fooValue%', Criteria::LIKE); // WHERE complemento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $complemento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByComplemento($complemento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($complemento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_COMPLEMENTO, $complemento, $comparison);
    }

    /**
     * Filter the query on the cidade_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCidadeId(1234); // WHERE cidade_id = 1234
     * $query->filterByCidadeId(array(12, 34)); // WHERE cidade_id IN (12, 34)
     * $query->filterByCidadeId(array('min' => 12)); // WHERE cidade_id > 12
     * </code>
     *
     * @see       filterByCidade()
     *
     * @param     mixed $cidadeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByCidadeId($cidadeId = null, $comparison = null)
    {
        if (is_array($cidadeId)) {
            $useMinMax = false;
            if (isset($cidadeId['min'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_CIDADE_ID, $cidadeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cidadeId['max'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_CIDADE_ID, $cidadeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_CIDADE_ID, $cidadeId, $comparison);
    }

    /**
     * Filter the query on the cep column
     *
     * Example usage:
     * <code>
     * $query->filterByCep('fooValue');   // WHERE cep = 'fooValue'
     * $query->filterByCep('%fooValue%', Criteria::LIKE); // WHERE cep LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cep The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByCep($cep = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cep)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_CEP, $cep, $comparison);
    }

    /**
     * Filter the query on the uf column
     *
     * Example usage:
     * <code>
     * $query->filterByUf('fooValue');   // WHERE uf = 'fooValue'
     * $query->filterByUf('%fooValue%', Criteria::LIKE); // WHERE uf LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uf The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByUf($uf = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uf)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_UF, $uf, $comparison);
    }

    /**
     * Filter the query on the ponto_referencia column
     *
     * Example usage:
     * <code>
     * $query->filterByPontoReferencia('fooValue');   // WHERE ponto_referencia = 'fooValue'
     * $query->filterByPontoReferencia('%fooValue%', Criteria::LIKE); // WHERE ponto_referencia LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pontoReferencia The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByPontoReferencia($pontoReferencia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pontoReferencia)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_PONTO_REFERENCIA, $pontoReferencia, $comparison);
    }

    /**
     * Filter the query on the contato column
     *
     * Example usage:
     * <code>
     * $query->filterByContato('fooValue');   // WHERE contato = 'fooValue'
     * $query->filterByContato('%fooValue%', Criteria::LIKE); // WHERE contato LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contato The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByContato($contato = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contato)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_CONTATO, $contato, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the import_id column
     *
     * Example usage:
     * <code>
     * $query->filterByImportId(1234); // WHERE import_id = 1234
     * $query->filterByImportId(array(12, 34)); // WHERE import_id IN (12, 34)
     * $query->filterByImportId(array('min' => 12)); // WHERE import_id > 12
     * </code>
     *
     * @param     mixed $importId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByImportId($importId = null, $comparison = null)
    {
        if (is_array($importId)) {
            $useMinMax = false;
            if (isset($importId['min'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_IMPORT_ID, $importId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importId['max'])) {
                $this->addUsingAlias(EnderecoClienteTableMap::COL_IMPORT_ID, $importId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EnderecoClienteTableMap::COL_IMPORT_ID, $importId, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cidade object
     *
     * @param \ImaTelecomBundle\Model\Cidade|ObjectCollection $cidade The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByCidade($cidade, $comparison = null)
    {
        if ($cidade instanceof \ImaTelecomBundle\Model\Cidade) {
            return $this
                ->addUsingAlias(EnderecoClienteTableMap::COL_CIDADE_ID, $cidade->getIdcidade(), $comparison);
        } elseif ($cidade instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EnderecoClienteTableMap::COL_CIDADE_ID, $cidade->toKeyValue('PrimaryKey', 'Idcidade'), $comparison);
        } else {
            throw new PropelException('filterByCidade() only accepts arguments of type \ImaTelecomBundle\Model\Cidade or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cidade relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function joinCidade($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cidade');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cidade');
        }

        return $this;
    }

    /**
     * Use the Cidade relation Cidade object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CidadeQuery A secondary query class using the current class as primary query
     */
    public function useCidadeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCidade($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cidade', '\ImaTelecomBundle\Model\CidadeQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cliente object
     *
     * @param \ImaTelecomBundle\Model\Cliente|ObjectCollection $cliente The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByCliente($cliente, $comparison = null)
    {
        if ($cliente instanceof \ImaTelecomBundle\Model\Cliente) {
            return $this
                ->addUsingAlias(EnderecoClienteTableMap::COL_CLIENTE_ID, $cliente->getIdcliente(), $comparison);
        } elseif ($cliente instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EnderecoClienteTableMap::COL_CLIENTE_ID, $cliente->toKeyValue('PrimaryKey', 'Idcliente'), $comparison);
        } else {
            throw new PropelException('filterByCliente() only accepts arguments of type \ImaTelecomBundle\Model\Cliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function joinCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cliente');
        }

        return $this;
    }

    /**
     * Use the Cliente relation Cliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteQuery A secondary query class using the current class as primary query
     */
    public function useClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cliente', '\ImaTelecomBundle\Model\ClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(EnderecoClienteTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EnderecoClienteTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Contrato object
     *
     * @param \ImaTelecomBundle\Model\Contrato|ObjectCollection $contrato the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function filterByContrato($contrato, $comparison = null)
    {
        if ($contrato instanceof \ImaTelecomBundle\Model\Contrato) {
            return $this
                ->addUsingAlias(EnderecoClienteTableMap::COL_IDENDERECO_CLIENTE, $contrato->getEnderecoClienteId(), $comparison);
        } elseif ($contrato instanceof ObjectCollection) {
            return $this
                ->useContratoQuery()
                ->filterByPrimaryKeys($contrato->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContrato() only accepts arguments of type \ImaTelecomBundle\Model\Contrato or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contrato relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function joinContrato($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contrato');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contrato');
        }

        return $this;
    }

    /**
     * Use the Contrato relation Contrato object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContratoQuery A secondary query class using the current class as primary query
     */
    public function useContratoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContrato($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contrato', '\ImaTelecomBundle\Model\ContratoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEnderecoCliente $enderecoCliente Object to remove from the list of results
     *
     * @return $this|ChildEnderecoClienteQuery The current query, for fluid interface
     */
    public function prune($enderecoCliente = null)
    {
        if ($enderecoCliente) {
            $this->addUsingAlias(EnderecoClienteTableMap::COL_IDENDERECO_CLIENTE, $enderecoCliente->getIdenderecoCliente(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the endereco_cliente table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EnderecoClienteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EnderecoClienteTableMap::clearInstancePool();
            EnderecoClienteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EnderecoClienteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EnderecoClienteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EnderecoClienteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EnderecoClienteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EnderecoClienteQuery
