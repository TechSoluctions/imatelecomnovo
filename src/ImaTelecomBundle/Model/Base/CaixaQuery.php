<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Caixa as ChildCaixa;
use ImaTelecomBundle\Model\CaixaQuery as ChildCaixaQuery;
use ImaTelecomBundle\Model\Map\CaixaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'caixa' table.
 *
 *
 *
 * @method     ChildCaixaQuery orderByIdcaixa($order = Criteria::ASC) Order by the idcaixa column
 * @method     ChildCaixaQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method     ChildCaixaQuery orderByDataPagamento($order = Criteria::ASC) Order by the data_pagamento column
 * @method     ChildCaixaQuery orderByDataBaixa($order = Criteria::ASC) Order by the data_baixa column
 * @method     ChildCaixaQuery orderByUsuarioBaixa($order = Criteria::ASC) Order by the usuario_baixa column
 * @method     ChildCaixaQuery orderByValorPago($order = Criteria::ASC) Order by the valor_pago column
 * @method     ChildCaixaQuery orderByValorJuros($order = Criteria::ASC) Order by the valor_juros column
 * @method     ChildCaixaQuery orderByValorDesconto($order = Criteria::ASC) Order by the valor_desconto column
 * @method     ChildCaixaQuery orderByObservacao($order = Criteria::ASC) Order by the observacao column
 * @method     ChildCaixaQuery orderByFormaPagamentoId($order = Criteria::ASC) Order by the forma_pagamento_id column
 *
 * @method     ChildCaixaQuery groupByIdcaixa() Group by the idcaixa column
 * @method     ChildCaixaQuery groupByTipo() Group by the tipo column
 * @method     ChildCaixaQuery groupByDataPagamento() Group by the data_pagamento column
 * @method     ChildCaixaQuery groupByDataBaixa() Group by the data_baixa column
 * @method     ChildCaixaQuery groupByUsuarioBaixa() Group by the usuario_baixa column
 * @method     ChildCaixaQuery groupByValorPago() Group by the valor_pago column
 * @method     ChildCaixaQuery groupByValorJuros() Group by the valor_juros column
 * @method     ChildCaixaQuery groupByValorDesconto() Group by the valor_desconto column
 * @method     ChildCaixaQuery groupByObservacao() Group by the observacao column
 * @method     ChildCaixaQuery groupByFormaPagamentoId() Group by the forma_pagamento_id column
 *
 * @method     ChildCaixaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCaixaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCaixaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCaixaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCaixaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCaixaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCaixaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildCaixaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildCaixaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildCaixaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildCaixaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildCaixaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildCaixaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildCaixaQuery leftJoinCaixaMovimento($relationAlias = null) Adds a LEFT JOIN clause to the query using the CaixaMovimento relation
 * @method     ChildCaixaQuery rightJoinCaixaMovimento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CaixaMovimento relation
 * @method     ChildCaixaQuery innerJoinCaixaMovimento($relationAlias = null) Adds a INNER JOIN clause to the query using the CaixaMovimento relation
 *
 * @method     ChildCaixaQuery joinWithCaixaMovimento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CaixaMovimento relation
 *
 * @method     ChildCaixaQuery leftJoinWithCaixaMovimento() Adds a LEFT JOIN clause and with to the query using the CaixaMovimento relation
 * @method     ChildCaixaQuery rightJoinWithCaixaMovimento() Adds a RIGHT JOIN clause and with to the query using the CaixaMovimento relation
 * @method     ChildCaixaQuery innerJoinWithCaixaMovimento() Adds a INNER JOIN clause and with to the query using the CaixaMovimento relation
 *
 * @method     ChildCaixaQuery leftJoinContasPagarParcelas($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagarParcelas relation
 * @method     ChildCaixaQuery rightJoinContasPagarParcelas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagarParcelas relation
 * @method     ChildCaixaQuery innerJoinContasPagarParcelas($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagarParcelas relation
 *
 * @method     ChildCaixaQuery joinWithContasPagarParcelas($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagarParcelas relation
 *
 * @method     ChildCaixaQuery leftJoinWithContasPagarParcelas() Adds a LEFT JOIN clause and with to the query using the ContasPagarParcelas relation
 * @method     ChildCaixaQuery rightJoinWithContasPagarParcelas() Adds a RIGHT JOIN clause and with to the query using the ContasPagarParcelas relation
 * @method     ChildCaixaQuery innerJoinWithContasPagarParcelas() Adds a INNER JOIN clause and with to the query using the ContasPagarParcelas relation
 *
 * @method     \ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\CaixaMovimentoQuery|\ImaTelecomBundle\Model\ContasPagarParcelasQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCaixa findOne(ConnectionInterface $con = null) Return the first ChildCaixa matching the query
 * @method     ChildCaixa findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCaixa matching the query, or a new ChildCaixa object populated from the query conditions when no match is found
 *
 * @method     ChildCaixa findOneByIdcaixa(int $idcaixa) Return the first ChildCaixa filtered by the idcaixa column
 * @method     ChildCaixa findOneByTipo(string $tipo) Return the first ChildCaixa filtered by the tipo column
 * @method     ChildCaixa findOneByDataPagamento(string $data_pagamento) Return the first ChildCaixa filtered by the data_pagamento column
 * @method     ChildCaixa findOneByDataBaixa(string $data_baixa) Return the first ChildCaixa filtered by the data_baixa column
 * @method     ChildCaixa findOneByUsuarioBaixa(int $usuario_baixa) Return the first ChildCaixa filtered by the usuario_baixa column
 * @method     ChildCaixa findOneByValorPago(string $valor_pago) Return the first ChildCaixa filtered by the valor_pago column
 * @method     ChildCaixa findOneByValorJuros(string $valor_juros) Return the first ChildCaixa filtered by the valor_juros column
 * @method     ChildCaixa findOneByValorDesconto(string $valor_desconto) Return the first ChildCaixa filtered by the valor_desconto column
 * @method     ChildCaixa findOneByObservacao(string $observacao) Return the first ChildCaixa filtered by the observacao column
 * @method     ChildCaixa findOneByFormaPagamentoId(int $forma_pagamento_id) Return the first ChildCaixa filtered by the forma_pagamento_id column *

 * @method     ChildCaixa requirePk($key, ConnectionInterface $con = null) Return the ChildCaixa by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOne(ConnectionInterface $con = null) Return the first ChildCaixa matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCaixa requireOneByIdcaixa(int $idcaixa) Return the first ChildCaixa filtered by the idcaixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOneByTipo(string $tipo) Return the first ChildCaixa filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOneByDataPagamento(string $data_pagamento) Return the first ChildCaixa filtered by the data_pagamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOneByDataBaixa(string $data_baixa) Return the first ChildCaixa filtered by the data_baixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOneByUsuarioBaixa(int $usuario_baixa) Return the first ChildCaixa filtered by the usuario_baixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOneByValorPago(string $valor_pago) Return the first ChildCaixa filtered by the valor_pago column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOneByValorJuros(string $valor_juros) Return the first ChildCaixa filtered by the valor_juros column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOneByValorDesconto(string $valor_desconto) Return the first ChildCaixa filtered by the valor_desconto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOneByObservacao(string $observacao) Return the first ChildCaixa filtered by the observacao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCaixa requireOneByFormaPagamentoId(int $forma_pagamento_id) Return the first ChildCaixa filtered by the forma_pagamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCaixa[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCaixa objects based on current ModelCriteria
 * @method     ChildCaixa[]|ObjectCollection findByIdcaixa(int $idcaixa) Return ChildCaixa objects filtered by the idcaixa column
 * @method     ChildCaixa[]|ObjectCollection findByTipo(string $tipo) Return ChildCaixa objects filtered by the tipo column
 * @method     ChildCaixa[]|ObjectCollection findByDataPagamento(string $data_pagamento) Return ChildCaixa objects filtered by the data_pagamento column
 * @method     ChildCaixa[]|ObjectCollection findByDataBaixa(string $data_baixa) Return ChildCaixa objects filtered by the data_baixa column
 * @method     ChildCaixa[]|ObjectCollection findByUsuarioBaixa(int $usuario_baixa) Return ChildCaixa objects filtered by the usuario_baixa column
 * @method     ChildCaixa[]|ObjectCollection findByValorPago(string $valor_pago) Return ChildCaixa objects filtered by the valor_pago column
 * @method     ChildCaixa[]|ObjectCollection findByValorJuros(string $valor_juros) Return ChildCaixa objects filtered by the valor_juros column
 * @method     ChildCaixa[]|ObjectCollection findByValorDesconto(string $valor_desconto) Return ChildCaixa objects filtered by the valor_desconto column
 * @method     ChildCaixa[]|ObjectCollection findByObservacao(string $observacao) Return ChildCaixa objects filtered by the observacao column
 * @method     ChildCaixa[]|ObjectCollection findByFormaPagamentoId(int $forma_pagamento_id) Return ChildCaixa objects filtered by the forma_pagamento_id column
 * @method     ChildCaixa[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CaixaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\CaixaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Caixa', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCaixaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCaixaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCaixaQuery) {
            return $criteria;
        }
        $query = new ChildCaixaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCaixa|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CaixaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CaixaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCaixa A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcaixa, tipo, data_pagamento, data_baixa, usuario_baixa, valor_pago, valor_juros, valor_desconto, observacao, forma_pagamento_id FROM caixa WHERE idcaixa = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCaixa $obj */
            $obj = new ChildCaixa();
            $obj->hydrate($row);
            CaixaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCaixa|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CaixaTableMap::COL_IDCAIXA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CaixaTableMap::COL_IDCAIXA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcaixa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcaixa(1234); // WHERE idcaixa = 1234
     * $query->filterByIdcaixa(array(12, 34)); // WHERE idcaixa IN (12, 34)
     * $query->filterByIdcaixa(array('min' => 12)); // WHERE idcaixa > 12
     * </code>
     *
     * @param     mixed $idcaixa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByIdcaixa($idcaixa = null, $comparison = null)
    {
        if (is_array($idcaixa)) {
            $useMinMax = false;
            if (isset($idcaixa['min'])) {
                $this->addUsingAlias(CaixaTableMap::COL_IDCAIXA, $idcaixa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcaixa['max'])) {
                $this->addUsingAlias(CaixaTableMap::COL_IDCAIXA, $idcaixa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_IDCAIXA, $idcaixa, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the data_pagamento column
     *
     * Example usage:
     * <code>
     * $query->filterByDataPagamento('2011-03-14'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByDataPagamento('now'); // WHERE data_pagamento = '2011-03-14'
     * $query->filterByDataPagamento(array('max' => 'yesterday')); // WHERE data_pagamento > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataPagamento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByDataPagamento($dataPagamento = null, $comparison = null)
    {
        if (is_array($dataPagamento)) {
            $useMinMax = false;
            if (isset($dataPagamento['min'])) {
                $this->addUsingAlias(CaixaTableMap::COL_DATA_PAGAMENTO, $dataPagamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataPagamento['max'])) {
                $this->addUsingAlias(CaixaTableMap::COL_DATA_PAGAMENTO, $dataPagamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_DATA_PAGAMENTO, $dataPagamento, $comparison);
    }

    /**
     * Filter the query on the data_baixa column
     *
     * Example usage:
     * <code>
     * $query->filterByDataBaixa('2011-03-14'); // WHERE data_baixa = '2011-03-14'
     * $query->filterByDataBaixa('now'); // WHERE data_baixa = '2011-03-14'
     * $query->filterByDataBaixa(array('max' => 'yesterday')); // WHERE data_baixa > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataBaixa The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByDataBaixa($dataBaixa = null, $comparison = null)
    {
        if (is_array($dataBaixa)) {
            $useMinMax = false;
            if (isset($dataBaixa['min'])) {
                $this->addUsingAlias(CaixaTableMap::COL_DATA_BAIXA, $dataBaixa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataBaixa['max'])) {
                $this->addUsingAlias(CaixaTableMap::COL_DATA_BAIXA, $dataBaixa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_DATA_BAIXA, $dataBaixa, $comparison);
    }

    /**
     * Filter the query on the usuario_baixa column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioBaixa(1234); // WHERE usuario_baixa = 1234
     * $query->filterByUsuarioBaixa(array(12, 34)); // WHERE usuario_baixa IN (12, 34)
     * $query->filterByUsuarioBaixa(array('min' => 12)); // WHERE usuario_baixa > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioBaixa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByUsuarioBaixa($usuarioBaixa = null, $comparison = null)
    {
        if (is_array($usuarioBaixa)) {
            $useMinMax = false;
            if (isset($usuarioBaixa['min'])) {
                $this->addUsingAlias(CaixaTableMap::COL_USUARIO_BAIXA, $usuarioBaixa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioBaixa['max'])) {
                $this->addUsingAlias(CaixaTableMap::COL_USUARIO_BAIXA, $usuarioBaixa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_USUARIO_BAIXA, $usuarioBaixa, $comparison);
    }

    /**
     * Filter the query on the valor_pago column
     *
     * Example usage:
     * <code>
     * $query->filterByValorPago(1234); // WHERE valor_pago = 1234
     * $query->filterByValorPago(array(12, 34)); // WHERE valor_pago IN (12, 34)
     * $query->filterByValorPago(array('min' => 12)); // WHERE valor_pago > 12
     * </code>
     *
     * @param     mixed $valorPago The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByValorPago($valorPago = null, $comparison = null)
    {
        if (is_array($valorPago)) {
            $useMinMax = false;
            if (isset($valorPago['min'])) {
                $this->addUsingAlias(CaixaTableMap::COL_VALOR_PAGO, $valorPago['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorPago['max'])) {
                $this->addUsingAlias(CaixaTableMap::COL_VALOR_PAGO, $valorPago['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_VALOR_PAGO, $valorPago, $comparison);
    }

    /**
     * Filter the query on the valor_juros column
     *
     * Example usage:
     * <code>
     * $query->filterByValorJuros(1234); // WHERE valor_juros = 1234
     * $query->filterByValorJuros(array(12, 34)); // WHERE valor_juros IN (12, 34)
     * $query->filterByValorJuros(array('min' => 12)); // WHERE valor_juros > 12
     * </code>
     *
     * @param     mixed $valorJuros The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByValorJuros($valorJuros = null, $comparison = null)
    {
        if (is_array($valorJuros)) {
            $useMinMax = false;
            if (isset($valorJuros['min'])) {
                $this->addUsingAlias(CaixaTableMap::COL_VALOR_JUROS, $valorJuros['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorJuros['max'])) {
                $this->addUsingAlias(CaixaTableMap::COL_VALOR_JUROS, $valorJuros['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_VALOR_JUROS, $valorJuros, $comparison);
    }

    /**
     * Filter the query on the valor_desconto column
     *
     * Example usage:
     * <code>
     * $query->filterByValorDesconto(1234); // WHERE valor_desconto = 1234
     * $query->filterByValorDesconto(array(12, 34)); // WHERE valor_desconto IN (12, 34)
     * $query->filterByValorDesconto(array('min' => 12)); // WHERE valor_desconto > 12
     * </code>
     *
     * @param     mixed $valorDesconto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByValorDesconto($valorDesconto = null, $comparison = null)
    {
        if (is_array($valorDesconto)) {
            $useMinMax = false;
            if (isset($valorDesconto['min'])) {
                $this->addUsingAlias(CaixaTableMap::COL_VALOR_DESCONTO, $valorDesconto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorDesconto['max'])) {
                $this->addUsingAlias(CaixaTableMap::COL_VALOR_DESCONTO, $valorDesconto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_VALOR_DESCONTO, $valorDesconto, $comparison);
    }

    /**
     * Filter the query on the observacao column
     *
     * Example usage:
     * <code>
     * $query->filterByObservacao('fooValue');   // WHERE observacao = 'fooValue'
     * $query->filterByObservacao('%fooValue%', Criteria::LIKE); // WHERE observacao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observacao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByObservacao($observacao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observacao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_OBSERVACAO, $observacao, $comparison);
    }

    /**
     * Filter the query on the forma_pagamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFormaPagamentoId(1234); // WHERE forma_pagamento_id = 1234
     * $query->filterByFormaPagamentoId(array(12, 34)); // WHERE forma_pagamento_id IN (12, 34)
     * $query->filterByFormaPagamentoId(array('min' => 12)); // WHERE forma_pagamento_id > 12
     * </code>
     *
     * @param     mixed $formaPagamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByFormaPagamentoId($formaPagamentoId = null, $comparison = null)
    {
        if (is_array($formaPagamentoId)) {
            $useMinMax = false;
            if (isset($formaPagamentoId['min'])) {
                $this->addUsingAlias(CaixaTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($formaPagamentoId['max'])) {
                $this->addUsingAlias(CaixaTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CaixaTableMap::COL_FORMA_PAGAMENTO_ID, $formaPagamentoId, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(CaixaTableMap::COL_USUARIO_BAIXA, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CaixaTableMap::COL_USUARIO_BAIXA, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\CaixaMovimento object
     *
     * @param \ImaTelecomBundle\Model\CaixaMovimento|ObjectCollection $caixaMovimento the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByCaixaMovimento($caixaMovimento, $comparison = null)
    {
        if ($caixaMovimento instanceof \ImaTelecomBundle\Model\CaixaMovimento) {
            return $this
                ->addUsingAlias(CaixaTableMap::COL_IDCAIXA, $caixaMovimento->getCaixaId(), $comparison);
        } elseif ($caixaMovimento instanceof ObjectCollection) {
            return $this
                ->useCaixaMovimentoQuery()
                ->filterByPrimaryKeys($caixaMovimento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCaixaMovimento() only accepts arguments of type \ImaTelecomBundle\Model\CaixaMovimento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CaixaMovimento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function joinCaixaMovimento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CaixaMovimento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CaixaMovimento');
        }

        return $this;
    }

    /**
     * Use the CaixaMovimento relation CaixaMovimento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CaixaMovimentoQuery A secondary query class using the current class as primary query
     */
    public function useCaixaMovimentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCaixaMovimento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CaixaMovimento', '\ImaTelecomBundle\Model\CaixaMovimentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagarParcelas object
     *
     * @param \ImaTelecomBundle\Model\ContasPagarParcelas|ObjectCollection $contasPagarParcelas the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCaixaQuery The current query, for fluid interface
     */
    public function filterByContasPagarParcelas($contasPagarParcelas, $comparison = null)
    {
        if ($contasPagarParcelas instanceof \ImaTelecomBundle\Model\ContasPagarParcelas) {
            return $this
                ->addUsingAlias(CaixaTableMap::COL_IDCAIXA, $contasPagarParcelas->getCaixaId(), $comparison);
        } elseif ($contasPagarParcelas instanceof ObjectCollection) {
            return $this
                ->useContasPagarParcelasQuery()
                ->filterByPrimaryKeys($contasPagarParcelas->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContasPagarParcelas() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagarParcelas or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagarParcelas relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function joinContasPagarParcelas($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagarParcelas');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagarParcelas');
        }

        return $this;
    }

    /**
     * Use the ContasPagarParcelas relation ContasPagarParcelas object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarParcelasQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarParcelasQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContasPagarParcelas($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagarParcelas', '\ImaTelecomBundle\Model\ContasPagarParcelasQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCaixa $caixa Object to remove from the list of results
     *
     * @return $this|ChildCaixaQuery The current query, for fluid interface
     */
    public function prune($caixa = null)
    {
        if ($caixa) {
            $this->addUsingAlias(CaixaTableMap::COL_IDCAIXA, $caixa->getIdcaixa(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the caixa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CaixaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CaixaTableMap::clearInstancePool();
            CaixaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CaixaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CaixaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CaixaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CaixaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CaixaQuery
