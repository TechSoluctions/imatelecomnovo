<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Estoque as ChildEstoque;
use ImaTelecomBundle\Model\EstoqueQuery as ChildEstoqueQuery;
use ImaTelecomBundle\Model\Map\EstoqueTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'estoque' table.
 *
 *
 *
 * @method     ChildEstoqueQuery orderByIdestoque($order = Criteria::ASC) Order by the idestoque column
 * @method     ChildEstoqueQuery orderByItemDeCompraId($order = Criteria::ASC) Order by the item_de_compra_id column
 * @method     ChildEstoqueQuery orderByUnidade($order = Criteria::ASC) Order by the unidade column
 * @method     ChildEstoqueQuery orderByQtde($order = Criteria::ASC) Order by the qtde column
 * @method     ChildEstoqueQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildEstoqueQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildEstoqueQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildEstoqueQuery groupByIdestoque() Group by the idestoque column
 * @method     ChildEstoqueQuery groupByItemDeCompraId() Group by the item_de_compra_id column
 * @method     ChildEstoqueQuery groupByUnidade() Group by the unidade column
 * @method     ChildEstoqueQuery groupByQtde() Group by the qtde column
 * @method     ChildEstoqueQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildEstoqueQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildEstoqueQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildEstoqueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEstoqueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEstoqueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEstoqueQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEstoqueQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEstoqueQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEstoqueQuery leftJoinItemDeCompra($relationAlias = null) Adds a LEFT JOIN clause to the query using the ItemDeCompra relation
 * @method     ChildEstoqueQuery rightJoinItemDeCompra($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ItemDeCompra relation
 * @method     ChildEstoqueQuery innerJoinItemDeCompra($relationAlias = null) Adds a INNER JOIN clause to the query using the ItemDeCompra relation
 *
 * @method     ChildEstoqueQuery joinWithItemDeCompra($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ItemDeCompra relation
 *
 * @method     ChildEstoqueQuery leftJoinWithItemDeCompra() Adds a LEFT JOIN clause and with to the query using the ItemDeCompra relation
 * @method     ChildEstoqueQuery rightJoinWithItemDeCompra() Adds a RIGHT JOIN clause and with to the query using the ItemDeCompra relation
 * @method     ChildEstoqueQuery innerJoinWithItemDeCompra() Adds a INNER JOIN clause and with to the query using the ItemDeCompra relation
 *
 * @method     ChildEstoqueQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildEstoqueQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildEstoqueQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildEstoqueQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildEstoqueQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEstoqueQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEstoqueQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildEstoqueQuery leftJoinClienteEstoqueItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the ClienteEstoqueItem relation
 * @method     ChildEstoqueQuery rightJoinClienteEstoqueItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ClienteEstoqueItem relation
 * @method     ChildEstoqueQuery innerJoinClienteEstoqueItem($relationAlias = null) Adds a INNER JOIN clause to the query using the ClienteEstoqueItem relation
 *
 * @method     ChildEstoqueQuery joinWithClienteEstoqueItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ClienteEstoqueItem relation
 *
 * @method     ChildEstoqueQuery leftJoinWithClienteEstoqueItem() Adds a LEFT JOIN clause and with to the query using the ClienteEstoqueItem relation
 * @method     ChildEstoqueQuery rightJoinWithClienteEstoqueItem() Adds a RIGHT JOIN clause and with to the query using the ClienteEstoqueItem relation
 * @method     ChildEstoqueQuery innerJoinWithClienteEstoqueItem() Adds a INNER JOIN clause and with to the query using the ClienteEstoqueItem relation
 *
 * @method     ChildEstoqueQuery leftJoinEstoqueLancamentoItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the EstoqueLancamentoItem relation
 * @method     ChildEstoqueQuery rightJoinEstoqueLancamentoItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EstoqueLancamentoItem relation
 * @method     ChildEstoqueQuery innerJoinEstoqueLancamentoItem($relationAlias = null) Adds a INNER JOIN clause to the query using the EstoqueLancamentoItem relation
 *
 * @method     ChildEstoqueQuery joinWithEstoqueLancamentoItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EstoqueLancamentoItem relation
 *
 * @method     ChildEstoqueQuery leftJoinWithEstoqueLancamentoItem() Adds a LEFT JOIN clause and with to the query using the EstoqueLancamentoItem relation
 * @method     ChildEstoqueQuery rightJoinWithEstoqueLancamentoItem() Adds a RIGHT JOIN clause and with to the query using the EstoqueLancamentoItem relation
 * @method     ChildEstoqueQuery innerJoinWithEstoqueLancamentoItem() Adds a INNER JOIN clause and with to the query using the EstoqueLancamentoItem relation
 *
 * @method     \ImaTelecomBundle\Model\ItemDeCompraQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\ClienteEstoqueItemQuery|\ImaTelecomBundle\Model\EstoqueLancamentoItemQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEstoque findOne(ConnectionInterface $con = null) Return the first ChildEstoque matching the query
 * @method     ChildEstoque findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEstoque matching the query, or a new ChildEstoque object populated from the query conditions when no match is found
 *
 * @method     ChildEstoque findOneByIdestoque(int $idestoque) Return the first ChildEstoque filtered by the idestoque column
 * @method     ChildEstoque findOneByItemDeCompraId(int $item_de_compra_id) Return the first ChildEstoque filtered by the item_de_compra_id column
 * @method     ChildEstoque findOneByUnidade(string $unidade) Return the first ChildEstoque filtered by the unidade column
 * @method     ChildEstoque findOneByQtde(double $qtde) Return the first ChildEstoque filtered by the qtde column
 * @method     ChildEstoque findOneByDataCadastro(string $data_cadastro) Return the first ChildEstoque filtered by the data_cadastro column
 * @method     ChildEstoque findOneByDataAlterado(string $data_alterado) Return the first ChildEstoque filtered by the data_alterado column
 * @method     ChildEstoque findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEstoque filtered by the usuario_alterado column *

 * @method     ChildEstoque requirePk($key, ConnectionInterface $con = null) Return the ChildEstoque by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoque requireOne(ConnectionInterface $con = null) Return the first ChildEstoque matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEstoque requireOneByIdestoque(int $idestoque) Return the first ChildEstoque filtered by the idestoque column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoque requireOneByItemDeCompraId(int $item_de_compra_id) Return the first ChildEstoque filtered by the item_de_compra_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoque requireOneByUnidade(string $unidade) Return the first ChildEstoque filtered by the unidade column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoque requireOneByQtde(double $qtde) Return the first ChildEstoque filtered by the qtde column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoque requireOneByDataCadastro(string $data_cadastro) Return the first ChildEstoque filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoque requireOneByDataAlterado(string $data_alterado) Return the first ChildEstoque filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoque requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEstoque filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEstoque[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEstoque objects based on current ModelCriteria
 * @method     ChildEstoque[]|ObjectCollection findByIdestoque(int $idestoque) Return ChildEstoque objects filtered by the idestoque column
 * @method     ChildEstoque[]|ObjectCollection findByItemDeCompraId(int $item_de_compra_id) Return ChildEstoque objects filtered by the item_de_compra_id column
 * @method     ChildEstoque[]|ObjectCollection findByUnidade(string $unidade) Return ChildEstoque objects filtered by the unidade column
 * @method     ChildEstoque[]|ObjectCollection findByQtde(double $qtde) Return ChildEstoque objects filtered by the qtde column
 * @method     ChildEstoque[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildEstoque objects filtered by the data_cadastro column
 * @method     ChildEstoque[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildEstoque objects filtered by the data_alterado column
 * @method     ChildEstoque[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildEstoque objects filtered by the usuario_alterado column
 * @method     ChildEstoque[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EstoqueQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\EstoqueQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Estoque', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEstoqueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEstoqueQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEstoqueQuery) {
            return $criteria;
        }
        $query = new ChildEstoqueQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEstoque|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EstoqueTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EstoqueTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoque A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idestoque, item_de_compra_id, unidade, qtde, data_cadastro, data_alterado, usuario_alterado FROM estoque WHERE idestoque = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEstoque $obj */
            $obj = new ChildEstoque();
            $obj->hydrate($row);
            EstoqueTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEstoque|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EstoqueTableMap::COL_IDESTOQUE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EstoqueTableMap::COL_IDESTOQUE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idestoque column
     *
     * Example usage:
     * <code>
     * $query->filterByIdestoque(1234); // WHERE idestoque = 1234
     * $query->filterByIdestoque(array(12, 34)); // WHERE idestoque IN (12, 34)
     * $query->filterByIdestoque(array('min' => 12)); // WHERE idestoque > 12
     * </code>
     *
     * @param     mixed $idestoque The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByIdestoque($idestoque = null, $comparison = null)
    {
        if (is_array($idestoque)) {
            $useMinMax = false;
            if (isset($idestoque['min'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_IDESTOQUE, $idestoque['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idestoque['max'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_IDESTOQUE, $idestoque['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueTableMap::COL_IDESTOQUE, $idestoque, $comparison);
    }

    /**
     * Filter the query on the item_de_compra_id column
     *
     * Example usage:
     * <code>
     * $query->filterByItemDeCompraId(1234); // WHERE item_de_compra_id = 1234
     * $query->filterByItemDeCompraId(array(12, 34)); // WHERE item_de_compra_id IN (12, 34)
     * $query->filterByItemDeCompraId(array('min' => 12)); // WHERE item_de_compra_id > 12
     * </code>
     *
     * @see       filterByItemDeCompra()
     *
     * @param     mixed $itemDeCompraId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByItemDeCompraId($itemDeCompraId = null, $comparison = null)
    {
        if (is_array($itemDeCompraId)) {
            $useMinMax = false;
            if (isset($itemDeCompraId['min'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompraId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($itemDeCompraId['max'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompraId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompraId, $comparison);
    }

    /**
     * Filter the query on the unidade column
     *
     * Example usage:
     * <code>
     * $query->filterByUnidade('fooValue');   // WHERE unidade = 'fooValue'
     * $query->filterByUnidade('%fooValue%', Criteria::LIKE); // WHERE unidade LIKE '%fooValue%'
     * </code>
     *
     * @param     string $unidade The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByUnidade($unidade = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($unidade)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueTableMap::COL_UNIDADE, $unidade, $comparison);
    }

    /**
     * Filter the query on the qtde column
     *
     * Example usage:
     * <code>
     * $query->filterByQtde(1234); // WHERE qtde = 1234
     * $query->filterByQtde(array(12, 34)); // WHERE qtde IN (12, 34)
     * $query->filterByQtde(array('min' => 12)); // WHERE qtde > 12
     * </code>
     *
     * @param     mixed $qtde The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByQtde($qtde = null, $comparison = null)
    {
        if (is_array($qtde)) {
            $useMinMax = false;
            if (isset($qtde['min'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_QTDE, $qtde['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($qtde['max'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_QTDE, $qtde['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueTableMap::COL_QTDE, $qtde, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(EstoqueTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ItemDeCompra object
     *
     * @param \ImaTelecomBundle\Model\ItemDeCompra|ObjectCollection $itemDeCompra The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByItemDeCompra($itemDeCompra, $comparison = null)
    {
        if ($itemDeCompra instanceof \ImaTelecomBundle\Model\ItemDeCompra) {
            return $this
                ->addUsingAlias(EstoqueTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompra->getIditemDeCompra(), $comparison);
        } elseif ($itemDeCompra instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EstoqueTableMap::COL_ITEM_DE_COMPRA_ID, $itemDeCompra->toKeyValue('PrimaryKey', 'IditemDeCompra'), $comparison);
        } else {
            throw new PropelException('filterByItemDeCompra() only accepts arguments of type \ImaTelecomBundle\Model\ItemDeCompra or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ItemDeCompra relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function joinItemDeCompra($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ItemDeCompra');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ItemDeCompra');
        }

        return $this;
    }

    /**
     * Use the ItemDeCompra relation ItemDeCompra object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ItemDeCompraQuery A secondary query class using the current class as primary query
     */
    public function useItemDeCompraQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinItemDeCompra($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ItemDeCompra', '\ImaTelecomBundle\Model\ItemDeCompraQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(EstoqueTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EstoqueTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ClienteEstoqueItem object
     *
     * @param \ImaTelecomBundle\Model\ClienteEstoqueItem|ObjectCollection $clienteEstoqueItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByClienteEstoqueItem($clienteEstoqueItem, $comparison = null)
    {
        if ($clienteEstoqueItem instanceof \ImaTelecomBundle\Model\ClienteEstoqueItem) {
            return $this
                ->addUsingAlias(EstoqueTableMap::COL_IDESTOQUE, $clienteEstoqueItem->getEstoqueId(), $comparison);
        } elseif ($clienteEstoqueItem instanceof ObjectCollection) {
            return $this
                ->useClienteEstoqueItemQuery()
                ->filterByPrimaryKeys($clienteEstoqueItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByClienteEstoqueItem() only accepts arguments of type \ImaTelecomBundle\Model\ClienteEstoqueItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ClienteEstoqueItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function joinClienteEstoqueItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ClienteEstoqueItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ClienteEstoqueItem');
        }

        return $this;
    }

    /**
     * Use the ClienteEstoqueItem relation ClienteEstoqueItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteEstoqueItemQuery A secondary query class using the current class as primary query
     */
    public function useClienteEstoqueItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinClienteEstoqueItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ClienteEstoqueItem', '\ImaTelecomBundle\Model\ClienteEstoqueItemQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EstoqueLancamentoItem object
     *
     * @param \ImaTelecomBundle\Model\EstoqueLancamentoItem|ObjectCollection $estoqueLancamentoItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEstoqueQuery The current query, for fluid interface
     */
    public function filterByEstoqueLancamentoItem($estoqueLancamentoItem, $comparison = null)
    {
        if ($estoqueLancamentoItem instanceof \ImaTelecomBundle\Model\EstoqueLancamentoItem) {
            return $this
                ->addUsingAlias(EstoqueTableMap::COL_IDESTOQUE, $estoqueLancamentoItem->getEstoqueId(), $comparison);
        } elseif ($estoqueLancamentoItem instanceof ObjectCollection) {
            return $this
                ->useEstoqueLancamentoItemQuery()
                ->filterByPrimaryKeys($estoqueLancamentoItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEstoqueLancamentoItem() only accepts arguments of type \ImaTelecomBundle\Model\EstoqueLancamentoItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EstoqueLancamentoItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function joinEstoqueLancamentoItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EstoqueLancamentoItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EstoqueLancamentoItem');
        }

        return $this;
    }

    /**
     * Use the EstoqueLancamentoItem relation EstoqueLancamentoItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueLancamentoItemQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueLancamentoItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstoqueLancamentoItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EstoqueLancamentoItem', '\ImaTelecomBundle\Model\EstoqueLancamentoItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEstoque $estoque Object to remove from the list of results
     *
     * @return $this|ChildEstoqueQuery The current query, for fluid interface
     */
    public function prune($estoque = null)
    {
        if ($estoque) {
            $this->addUsingAlias(EstoqueTableMap::COL_IDESTOQUE, $estoque->getIdestoque(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the estoque table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EstoqueTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EstoqueTableMap::clearInstancePool();
            EstoqueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EstoqueTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EstoqueTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EstoqueTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EstoqueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EstoqueQuery
