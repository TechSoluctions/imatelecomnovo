<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Banco as ChildBanco;
use ImaTelecomBundle\Model\BancoAgencia as ChildBancoAgencia;
use ImaTelecomBundle\Model\BancoAgenciaConta as ChildBancoAgenciaConta;
use ImaTelecomBundle\Model\BancoAgenciaContaQuery as ChildBancoAgenciaContaQuery;
use ImaTelecomBundle\Model\BancoAgenciaQuery as ChildBancoAgenciaQuery;
use ImaTelecomBundle\Model\BancoQuery as ChildBancoQuery;
use ImaTelecomBundle\Model\ContaCaixa as ChildContaCaixa;
use ImaTelecomBundle\Model\ContaCaixaQuery as ChildContaCaixaQuery;
use ImaTelecomBundle\Model\Convenio as ChildConvenio;
use ImaTelecomBundle\Model\ConvenioQuery as ChildConvenioQuery;
use ImaTelecomBundle\Model\Fornecedor as ChildFornecedor;
use ImaTelecomBundle\Model\FornecedorQuery as ChildFornecedorQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BancoAgenciaContaTableMap;
use ImaTelecomBundle\Model\Map\ContaCaixaTableMap;
use ImaTelecomBundle\Model\Map\FornecedorTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'banco_agencia_conta' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class BancoAgenciaConta implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\BancoAgenciaContaTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idbanco_agencia_conta field.
     *
     * @var        int
     */
    protected $idbanco_agencia_conta;

    /**
     * The value for the banco_id field.
     *
     * @var        int
     */
    protected $banco_id;

    /**
     * The value for the banco_agencia_id field.
     *
     * @var        int
     */
    protected $banco_agencia_id;

    /**
     * The value for the tipo_conta field.
     *
     * @var        string
     */
    protected $tipo_conta;

    /**
     * The value for the numero_conta field.
     *
     * @var        string
     */
    protected $numero_conta;

    /**
     * The value for the digito_verificador field.
     *
     * @var        string
     */
    protected $digito_verificador;

    /**
     * The value for the convenio_id field.
     *
     * @var        int
     */
    protected $convenio_id;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * @var        ChildBancoAgencia
     */
    protected $aBancoAgencia;

    /**
     * @var        ChildBanco
     */
    protected $aBanco;

    /**
     * @var        ChildConvenio
     */
    protected $aConvenio;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildContaCaixa[] Collection to store aggregation of ChildContaCaixa objects.
     */
    protected $collContaCaixas;
    protected $collContaCaixasPartial;

    /**
     * @var        ObjectCollection|ChildFornecedor[] Collection to store aggregation of ChildFornecedor objects.
     */
    protected $collFornecedors;
    protected $collFornecedorsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContaCaixa[]
     */
    protected $contaCaixasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFornecedor[]
     */
    protected $fornecedorsScheduledForDeletion = null;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\BancoAgenciaConta object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>BancoAgenciaConta</code> instance.  If
     * <code>obj</code> is an instance of <code>BancoAgenciaConta</code>, delegates to
     * <code>equals(BancoAgenciaConta)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|BancoAgenciaConta The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idbanco_agencia_conta] column value.
     *
     * @return int
     */
    public function getIdbancoAgenciaConta()
    {
        return $this->idbanco_agencia_conta;
    }

    /**
     * Get the [banco_id] column value.
     *
     * @return int
     */
    public function getBancoId()
    {
        return $this->banco_id;
    }

    /**
     * Get the [banco_agencia_id] column value.
     *
     * @return int
     */
    public function getBancoAgenciaId()
    {
        return $this->banco_agencia_id;
    }

    /**
     * Get the [tipo_conta] column value.
     *
     * @return string
     */
    public function getTipoConta()
    {
        return $this->tipo_conta;
    }

    /**
     * Get the [numero_conta] column value.
     *
     * @return string
     */
    public function getNumeroConta()
    {
        return $this->numero_conta;
    }

    /**
     * Get the [digito_verificador] column value.
     *
     * @return string
     */
    public function getDigitoVerificador()
    {
        return $this->digito_verificador;
    }

    /**
     * Get the [convenio_id] column value.
     *
     * @return int
     */
    public function getConvenioId()
    {
        return $this->convenio_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Set the value of [idbanco_agencia_conta] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setIdbancoAgenciaConta($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idbanco_agencia_conta !== $v) {
            $this->idbanco_agencia_conta = $v;
            $this->modifiedColumns[BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA] = true;
        }

        return $this;
    } // setIdbancoAgenciaConta()

    /**
     * Set the value of [banco_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setBancoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->banco_id !== $v) {
            $this->banco_id = $v;
            $this->modifiedColumns[BancoAgenciaContaTableMap::COL_BANCO_ID] = true;
        }

        if ($this->aBanco !== null && $this->aBanco->getIdbanco() !== $v) {
            $this->aBanco = null;
        }

        return $this;
    } // setBancoId()

    /**
     * Set the value of [banco_agencia_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setBancoAgenciaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->banco_agencia_id !== $v) {
            $this->banco_agencia_id = $v;
            $this->modifiedColumns[BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID] = true;
        }

        if ($this->aBancoAgencia !== null && $this->aBancoAgencia->getIdbancoAgencia() !== $v) {
            $this->aBancoAgencia = null;
        }

        return $this;
    } // setBancoAgenciaId()

    /**
     * Set the value of [tipo_conta] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setTipoConta($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_conta !== $v) {
            $this->tipo_conta = $v;
            $this->modifiedColumns[BancoAgenciaContaTableMap::COL_TIPO_CONTA] = true;
        }

        return $this;
    } // setTipoConta()

    /**
     * Set the value of [numero_conta] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setNumeroConta($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->numero_conta !== $v) {
            $this->numero_conta = $v;
            $this->modifiedColumns[BancoAgenciaContaTableMap::COL_NUMERO_CONTA] = true;
        }

        return $this;
    } // setNumeroConta()

    /**
     * Set the value of [digito_verificador] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setDigitoVerificador($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->digito_verificador !== $v) {
            $this->digito_verificador = $v;
            $this->modifiedColumns[BancoAgenciaContaTableMap::COL_DIGITO_VERIFICADOR] = true;
        }

        return $this;
    } // setDigitoVerificador()

    /**
     * Set the value of [convenio_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setConvenioId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->convenio_id !== $v) {
            $this->convenio_id = $v;
            $this->modifiedColumns[BancoAgenciaContaTableMap::COL_CONVENIO_ID] = true;
        }

        if ($this->aConvenio !== null && $this->aConvenio->getIdconvenio() !== $v) {
            $this->aConvenio = null;
        }

        return $this;
    } // setConvenioId()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BancoAgenciaContaTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BancoAgenciaContaTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : BancoAgenciaContaTableMap::translateFieldName('IdbancoAgenciaConta', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idbanco_agencia_conta = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : BancoAgenciaContaTableMap::translateFieldName('BancoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->banco_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : BancoAgenciaContaTableMap::translateFieldName('BancoAgenciaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->banco_agencia_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : BancoAgenciaContaTableMap::translateFieldName('TipoConta', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_conta = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : BancoAgenciaContaTableMap::translateFieldName('NumeroConta', TableMap::TYPE_PHPNAME, $indexType)];
            $this->numero_conta = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : BancoAgenciaContaTableMap::translateFieldName('DigitoVerificador', TableMap::TYPE_PHPNAME, $indexType)];
            $this->digito_verificador = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : BancoAgenciaContaTableMap::translateFieldName('ConvenioId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->convenio_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : BancoAgenciaContaTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : BancoAgenciaContaTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : BancoAgenciaContaTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = BancoAgenciaContaTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\BancoAgenciaConta'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aBanco !== null && $this->banco_id !== $this->aBanco->getIdbanco()) {
            $this->aBanco = null;
        }
        if ($this->aBancoAgencia !== null && $this->banco_agencia_id !== $this->aBancoAgencia->getIdbancoAgencia()) {
            $this->aBancoAgencia = null;
        }
        if ($this->aConvenio !== null && $this->convenio_id !== $this->aConvenio->getIdconvenio()) {
            $this->aConvenio = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BancoAgenciaContaTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildBancoAgenciaContaQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBancoAgencia = null;
            $this->aBanco = null;
            $this->aConvenio = null;
            $this->aUsuario = null;
            $this->collContaCaixas = null;

            $this->collFornecedors = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see BancoAgenciaConta::setDeleted()
     * @see BancoAgenciaConta::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaContaTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildBancoAgenciaContaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaContaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BancoAgenciaContaTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBancoAgencia !== null) {
                if ($this->aBancoAgencia->isModified() || $this->aBancoAgencia->isNew()) {
                    $affectedRows += $this->aBancoAgencia->save($con);
                }
                $this->setBancoAgencia($this->aBancoAgencia);
            }

            if ($this->aBanco !== null) {
                if ($this->aBanco->isModified() || $this->aBanco->isNew()) {
                    $affectedRows += $this->aBanco->save($con);
                }
                $this->setBanco($this->aBanco);
            }

            if ($this->aConvenio !== null) {
                if ($this->aConvenio->isModified() || $this->aConvenio->isNew()) {
                    $affectedRows += $this->aConvenio->save($con);
                }
                $this->setConvenio($this->aConvenio);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->contaCaixasScheduledForDeletion !== null) {
                if (!$this->contaCaixasScheduledForDeletion->isEmpty()) {
                    foreach ($this->contaCaixasScheduledForDeletion as $contaCaixa) {
                        // need to save related object because we set the relation to null
                        $contaCaixa->save($con);
                    }
                    $this->contaCaixasScheduledForDeletion = null;
                }
            }

            if ($this->collContaCaixas !== null) {
                foreach ($this->collContaCaixas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->fornecedorsScheduledForDeletion !== null) {
                if (!$this->fornecedorsScheduledForDeletion->isEmpty()) {
                    foreach ($this->fornecedorsScheduledForDeletion as $fornecedor) {
                        // need to save related object because we set the relation to null
                        $fornecedor->save($con);
                    }
                    $this->fornecedorsScheduledForDeletion = null;
                }
            }

            if ($this->collFornecedors !== null) {
                foreach ($this->collFornecedors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA] = true;
        if (null !== $this->idbanco_agencia_conta) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA)) {
            $modifiedColumns[':p' . $index++]  = 'idbanco_agencia_conta';
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_BANCO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'banco_id';
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'banco_agencia_id';
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_TIPO_CONTA)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_conta';
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_NUMERO_CONTA)) {
            $modifiedColumns[':p' . $index++]  = 'numero_conta';
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_DIGITO_VERIFICADOR)) {
            $modifiedColumns[':p' . $index++]  = 'digito_verificador';
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_CONVENIO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'convenio_id';
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }

        $sql = sprintf(
            'INSERT INTO banco_agencia_conta (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idbanco_agencia_conta':
                        $stmt->bindValue($identifier, $this->idbanco_agencia_conta, PDO::PARAM_INT);
                        break;
                    case 'banco_id':
                        $stmt->bindValue($identifier, $this->banco_id, PDO::PARAM_INT);
                        break;
                    case 'banco_agencia_id':
                        $stmt->bindValue($identifier, $this->banco_agencia_id, PDO::PARAM_INT);
                        break;
                    case 'tipo_conta':
                        $stmt->bindValue($identifier, $this->tipo_conta, PDO::PARAM_STR);
                        break;
                    case 'numero_conta':
                        $stmt->bindValue($identifier, $this->numero_conta, PDO::PARAM_STR);
                        break;
                    case 'digito_verificador':
                        $stmt->bindValue($identifier, $this->digito_verificador, PDO::PARAM_STR);
                        break;
                    case 'convenio_id':
                        $stmt->bindValue($identifier, $this->convenio_id, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdbancoAgenciaConta($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BancoAgenciaContaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdbancoAgenciaConta();
                break;
            case 1:
                return $this->getBancoId();
                break;
            case 2:
                return $this->getBancoAgenciaId();
                break;
            case 3:
                return $this->getTipoConta();
                break;
            case 4:
                return $this->getNumeroConta();
                break;
            case 5:
                return $this->getDigitoVerificador();
                break;
            case 6:
                return $this->getConvenioId();
                break;
            case 7:
                return $this->getDataCadastro();
                break;
            case 8:
                return $this->getDataAlterado();
                break;
            case 9:
                return $this->getUsuarioAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['BancoAgenciaConta'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['BancoAgenciaConta'][$this->hashCode()] = true;
        $keys = BancoAgenciaContaTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdbancoAgenciaConta(),
            $keys[1] => $this->getBancoId(),
            $keys[2] => $this->getBancoAgenciaId(),
            $keys[3] => $this->getTipoConta(),
            $keys[4] => $this->getNumeroConta(),
            $keys[5] => $this->getDigitoVerificador(),
            $keys[6] => $this->getConvenioId(),
            $keys[7] => $this->getDataCadastro(),
            $keys[8] => $this->getDataAlterado(),
            $keys[9] => $this->getUsuarioAlterado(),
        );
        if ($result[$keys[7]] instanceof \DateTime) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTime) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBancoAgencia) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'bancoAgencia';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'banco_agencia';
                        break;
                    default:
                        $key = 'BancoAgencia';
                }

                $result[$key] = $this->aBancoAgencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBanco) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'banco';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'banco';
                        break;
                    default:
                        $key = 'Banco';
                }

                $result[$key] = $this->aBanco->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aConvenio) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'convenio';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'convenio';
                        break;
                    default:
                        $key = 'Convenio';
                }

                $result[$key] = $this->aConvenio->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collContaCaixas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contaCaixas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'conta_caixas';
                        break;
                    default:
                        $key = 'ContaCaixas';
                }

                $result[$key] = $this->collContaCaixas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFornecedors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'fornecedors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'fornecedors';
                        break;
                    default:
                        $key = 'Fornecedors';
                }

                $result[$key] = $this->collFornecedors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BancoAgenciaContaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdbancoAgenciaConta($value);
                break;
            case 1:
                $this->setBancoId($value);
                break;
            case 2:
                $this->setBancoAgenciaId($value);
                break;
            case 3:
                $this->setTipoConta($value);
                break;
            case 4:
                $this->setNumeroConta($value);
                break;
            case 5:
                $this->setDigitoVerificador($value);
                break;
            case 6:
                $this->setConvenioId($value);
                break;
            case 7:
                $this->setDataCadastro($value);
                break;
            case 8:
                $this->setDataAlterado($value);
                break;
            case 9:
                $this->setUsuarioAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = BancoAgenciaContaTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdbancoAgenciaConta($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBancoId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setBancoAgenciaId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTipoConta($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setNumeroConta($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDigitoVerificador($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setConvenioId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDataCadastro($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDataAlterado($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BancoAgenciaContaTableMap::DATABASE_NAME);

        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $this->idbanco_agencia_conta);
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_BANCO_ID)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_BANCO_ID, $this->banco_id);
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID, $this->banco_agencia_id);
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_TIPO_CONTA)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_TIPO_CONTA, $this->tipo_conta);
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_NUMERO_CONTA)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_NUMERO_CONTA, $this->numero_conta);
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_DIGITO_VERIFICADOR)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_DIGITO_VERIFICADOR, $this->digito_verificador);
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_CONVENIO_ID)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_CONVENIO_ID, $this->convenio_id);
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildBancoAgenciaContaQuery::create();
        $criteria->add(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $this->idbanco_agencia_conta);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdbancoAgenciaConta();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdbancoAgenciaConta();
    }

    /**
     * Generic method to set the primary key (idbanco_agencia_conta column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdbancoAgenciaConta($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdbancoAgenciaConta();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\BancoAgenciaConta (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBancoId($this->getBancoId());
        $copyObj->setBancoAgenciaId($this->getBancoAgenciaId());
        $copyObj->setTipoConta($this->getTipoConta());
        $copyObj->setNumeroConta($this->getNumeroConta());
        $copyObj->setDigitoVerificador($this->getDigitoVerificador());
        $copyObj->setConvenioId($this->getConvenioId());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getContaCaixas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContaCaixa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFornecedors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFornecedor($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdbancoAgenciaConta(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\BancoAgenciaConta Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBancoAgencia object.
     *
     * @param  ChildBancoAgencia $v
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBancoAgencia(ChildBancoAgencia $v = null)
    {
        if ($v === null) {
            $this->setBancoAgenciaId(NULL);
        } else {
            $this->setBancoAgenciaId($v->getIdbancoAgencia());
        }

        $this->aBancoAgencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBancoAgencia object, it will not be re-added.
        if ($v !== null) {
            $v->addBancoAgenciaConta($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBancoAgencia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBancoAgencia The associated ChildBancoAgencia object.
     * @throws PropelException
     */
    public function getBancoAgencia(ConnectionInterface $con = null)
    {
        if ($this->aBancoAgencia === null && ($this->banco_agencia_id !== null)) {
            $this->aBancoAgencia = ChildBancoAgenciaQuery::create()->findPk($this->banco_agencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBancoAgencia->addBancoAgenciaContas($this);
             */
        }

        return $this->aBancoAgencia;
    }

    /**
     * Declares an association between this object and a ChildBanco object.
     *
     * @param  ChildBanco $v
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBanco(ChildBanco $v = null)
    {
        if ($v === null) {
            $this->setBancoId(NULL);
        } else {
            $this->setBancoId($v->getIdbanco());
        }

        $this->aBanco = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBanco object, it will not be re-added.
        if ($v !== null) {
            $v->addBancoAgenciaConta($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBanco object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBanco The associated ChildBanco object.
     * @throws PropelException
     */
    public function getBanco(ConnectionInterface $con = null)
    {
        if ($this->aBanco === null && ($this->banco_id !== null)) {
            $this->aBanco = ChildBancoQuery::create()->findPk($this->banco_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBanco->addBancoAgenciaContas($this);
             */
        }

        return $this->aBanco;
    }

    /**
     * Declares an association between this object and a ChildConvenio object.
     *
     * @param  ChildConvenio $v
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     * @throws PropelException
     */
    public function setConvenio(ChildConvenio $v = null)
    {
        if ($v === null) {
            $this->setConvenioId(NULL);
        } else {
            $this->setConvenioId($v->getIdconvenio());
        }

        $this->aConvenio = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildConvenio object, it will not be re-added.
        if ($v !== null) {
            $v->addBancoAgenciaConta($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildConvenio object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildConvenio The associated ChildConvenio object.
     * @throws PropelException
     */
    public function getConvenio(ConnectionInterface $con = null)
    {
        if ($this->aConvenio === null && ($this->convenio_id !== null)) {
            $this->aConvenio = ChildConvenioQuery::create()->findPk($this->convenio_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aConvenio->addBancoAgenciaContas($this);
             */
        }

        return $this->aConvenio;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addBancoAgenciaConta($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addBancoAgenciaContas($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ContaCaixa' == $relationName) {
            return $this->initContaCaixas();
        }
        if ('Fornecedor' == $relationName) {
            return $this->initFornecedors();
        }
    }

    /**
     * Clears out the collContaCaixas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContaCaixas()
     */
    public function clearContaCaixas()
    {
        $this->collContaCaixas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContaCaixas collection loaded partially.
     */
    public function resetPartialContaCaixas($v = true)
    {
        $this->collContaCaixasPartial = $v;
    }

    /**
     * Initializes the collContaCaixas collection.
     *
     * By default this just sets the collContaCaixas collection to an empty array (like clearcollContaCaixas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContaCaixas($overrideExisting = true)
    {
        if (null !== $this->collContaCaixas && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContaCaixaTableMap::getTableMap()->getCollectionClassName();

        $this->collContaCaixas = new $collectionClassName;
        $this->collContaCaixas->setModel('\ImaTelecomBundle\Model\ContaCaixa');
    }

    /**
     * Gets an array of ChildContaCaixa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBancoAgenciaConta is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContaCaixa[] List of ChildContaCaixa objects
     * @throws PropelException
     */
    public function getContaCaixas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContaCaixasPartial && !$this->isNew();
        if (null === $this->collContaCaixas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContaCaixas) {
                // return empty collection
                $this->initContaCaixas();
            } else {
                $collContaCaixas = ChildContaCaixaQuery::create(null, $criteria)
                    ->filterByBancoAgenciaConta($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContaCaixasPartial && count($collContaCaixas)) {
                        $this->initContaCaixas(false);

                        foreach ($collContaCaixas as $obj) {
                            if (false == $this->collContaCaixas->contains($obj)) {
                                $this->collContaCaixas->append($obj);
                            }
                        }

                        $this->collContaCaixasPartial = true;
                    }

                    return $collContaCaixas;
                }

                if ($partial && $this->collContaCaixas) {
                    foreach ($this->collContaCaixas as $obj) {
                        if ($obj->isNew()) {
                            $collContaCaixas[] = $obj;
                        }
                    }
                }

                $this->collContaCaixas = $collContaCaixas;
                $this->collContaCaixasPartial = false;
            }
        }

        return $this->collContaCaixas;
    }

    /**
     * Sets a collection of ChildContaCaixa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contaCaixas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBancoAgenciaConta The current object (for fluent API support)
     */
    public function setContaCaixas(Collection $contaCaixas, ConnectionInterface $con = null)
    {
        /** @var ChildContaCaixa[] $contaCaixasToDelete */
        $contaCaixasToDelete = $this->getContaCaixas(new Criteria(), $con)->diff($contaCaixas);


        $this->contaCaixasScheduledForDeletion = $contaCaixasToDelete;

        foreach ($contaCaixasToDelete as $contaCaixaRemoved) {
            $contaCaixaRemoved->setBancoAgenciaConta(null);
        }

        $this->collContaCaixas = null;
        foreach ($contaCaixas as $contaCaixa) {
            $this->addContaCaixa($contaCaixa);
        }

        $this->collContaCaixas = $contaCaixas;
        $this->collContaCaixasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ContaCaixa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ContaCaixa objects.
     * @throws PropelException
     */
    public function countContaCaixas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContaCaixasPartial && !$this->isNew();
        if (null === $this->collContaCaixas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContaCaixas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContaCaixas());
            }

            $query = ChildContaCaixaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBancoAgenciaConta($this)
                ->count($con);
        }

        return count($this->collContaCaixas);
    }

    /**
     * Method called to associate a ChildContaCaixa object to this object
     * through the ChildContaCaixa foreign key attribute.
     *
     * @param  ChildContaCaixa $l ChildContaCaixa
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function addContaCaixa(ChildContaCaixa $l)
    {
        if ($this->collContaCaixas === null) {
            $this->initContaCaixas();
            $this->collContaCaixasPartial = true;
        }

        if (!$this->collContaCaixas->contains($l)) {
            $this->doAddContaCaixa($l);

            if ($this->contaCaixasScheduledForDeletion and $this->contaCaixasScheduledForDeletion->contains($l)) {
                $this->contaCaixasScheduledForDeletion->remove($this->contaCaixasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContaCaixa $contaCaixa The ChildContaCaixa object to add.
     */
    protected function doAddContaCaixa(ChildContaCaixa $contaCaixa)
    {
        $this->collContaCaixas[]= $contaCaixa;
        $contaCaixa->setBancoAgenciaConta($this);
    }

    /**
     * @param  ChildContaCaixa $contaCaixa The ChildContaCaixa object to remove.
     * @return $this|ChildBancoAgenciaConta The current object (for fluent API support)
     */
    public function removeContaCaixa(ChildContaCaixa $contaCaixa)
    {
        if ($this->getContaCaixas()->contains($contaCaixa)) {
            $pos = $this->collContaCaixas->search($contaCaixa);
            $this->collContaCaixas->remove($pos);
            if (null === $this->contaCaixasScheduledForDeletion) {
                $this->contaCaixasScheduledForDeletion = clone $this->collContaCaixas;
                $this->contaCaixasScheduledForDeletion->clear();
            }
            $this->contaCaixasScheduledForDeletion[]= $contaCaixa;
            $contaCaixa->setBancoAgenciaConta(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BancoAgenciaConta is new, it will return
     * an empty collection; or if this BancoAgenciaConta has previously
     * been saved, it will retrieve related ContaCaixas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BancoAgenciaConta.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContaCaixa[] List of ChildContaCaixa objects
     */
    public function getContaCaixasJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContaCaixaQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getContaCaixas($query, $con);
    }

    /**
     * Clears out the collFornecedors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFornecedors()
     */
    public function clearFornecedors()
    {
        $this->collFornecedors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFornecedors collection loaded partially.
     */
    public function resetPartialFornecedors($v = true)
    {
        $this->collFornecedorsPartial = $v;
    }

    /**
     * Initializes the collFornecedors collection.
     *
     * By default this just sets the collFornecedors collection to an empty array (like clearcollFornecedors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFornecedors($overrideExisting = true)
    {
        if (null !== $this->collFornecedors && !$overrideExisting) {
            return;
        }

        $collectionClassName = FornecedorTableMap::getTableMap()->getCollectionClassName();

        $this->collFornecedors = new $collectionClassName;
        $this->collFornecedors->setModel('\ImaTelecomBundle\Model\Fornecedor');
    }

    /**
     * Gets an array of ChildFornecedor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildBancoAgenciaConta is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     * @throws PropelException
     */
    public function getFornecedors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFornecedorsPartial && !$this->isNew();
        if (null === $this->collFornecedors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collFornecedors) {
                // return empty collection
                $this->initFornecedors();
            } else {
                $collFornecedors = ChildFornecedorQuery::create(null, $criteria)
                    ->filterByBancoAgenciaConta($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFornecedorsPartial && count($collFornecedors)) {
                        $this->initFornecedors(false);

                        foreach ($collFornecedors as $obj) {
                            if (false == $this->collFornecedors->contains($obj)) {
                                $this->collFornecedors->append($obj);
                            }
                        }

                        $this->collFornecedorsPartial = true;
                    }

                    return $collFornecedors;
                }

                if ($partial && $this->collFornecedors) {
                    foreach ($this->collFornecedors as $obj) {
                        if ($obj->isNew()) {
                            $collFornecedors[] = $obj;
                        }
                    }
                }

                $this->collFornecedors = $collFornecedors;
                $this->collFornecedorsPartial = false;
            }
        }

        return $this->collFornecedors;
    }

    /**
     * Sets a collection of ChildFornecedor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $fornecedors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildBancoAgenciaConta The current object (for fluent API support)
     */
    public function setFornecedors(Collection $fornecedors, ConnectionInterface $con = null)
    {
        /** @var ChildFornecedor[] $fornecedorsToDelete */
        $fornecedorsToDelete = $this->getFornecedors(new Criteria(), $con)->diff($fornecedors);


        $this->fornecedorsScheduledForDeletion = $fornecedorsToDelete;

        foreach ($fornecedorsToDelete as $fornecedorRemoved) {
            $fornecedorRemoved->setBancoAgenciaConta(null);
        }

        $this->collFornecedors = null;
        foreach ($fornecedors as $fornecedor) {
            $this->addFornecedor($fornecedor);
        }

        $this->collFornecedors = $fornecedors;
        $this->collFornecedorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Fornecedor objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Fornecedor objects.
     * @throws PropelException
     */
    public function countFornecedors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFornecedorsPartial && !$this->isNew();
        if (null === $this->collFornecedors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFornecedors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFornecedors());
            }

            $query = ChildFornecedorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByBancoAgenciaConta($this)
                ->count($con);
        }

        return count($this->collFornecedors);
    }

    /**
     * Method called to associate a ChildFornecedor object to this object
     * through the ChildFornecedor foreign key attribute.
     *
     * @param  ChildFornecedor $l ChildFornecedor
     * @return $this|\ImaTelecomBundle\Model\BancoAgenciaConta The current object (for fluent API support)
     */
    public function addFornecedor(ChildFornecedor $l)
    {
        if ($this->collFornecedors === null) {
            $this->initFornecedors();
            $this->collFornecedorsPartial = true;
        }

        if (!$this->collFornecedors->contains($l)) {
            $this->doAddFornecedor($l);

            if ($this->fornecedorsScheduledForDeletion and $this->fornecedorsScheduledForDeletion->contains($l)) {
                $this->fornecedorsScheduledForDeletion->remove($this->fornecedorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFornecedor $fornecedor The ChildFornecedor object to add.
     */
    protected function doAddFornecedor(ChildFornecedor $fornecedor)
    {
        $this->collFornecedors[]= $fornecedor;
        $fornecedor->setBancoAgenciaConta($this);
    }

    /**
     * @param  ChildFornecedor $fornecedor The ChildFornecedor object to remove.
     * @return $this|ChildBancoAgenciaConta The current object (for fluent API support)
     */
    public function removeFornecedor(ChildFornecedor $fornecedor)
    {
        if ($this->getFornecedors()->contains($fornecedor)) {
            $pos = $this->collFornecedors->search($fornecedor);
            $this->collFornecedors->remove($pos);
            if (null === $this->fornecedorsScheduledForDeletion) {
                $this->fornecedorsScheduledForDeletion = clone $this->collFornecedors;
                $this->fornecedorsScheduledForDeletion->clear();
            }
            $this->fornecedorsScheduledForDeletion[]= $fornecedor;
            $fornecedor->setBancoAgenciaConta(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BancoAgenciaConta is new, it will return
     * an empty collection; or if this BancoAgenciaConta has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BancoAgenciaConta.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BancoAgenciaConta is new, it will return
     * an empty collection; or if this BancoAgenciaConta has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BancoAgenciaConta.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this BancoAgenciaConta is new, it will return
     * an empty collection; or if this BancoAgenciaConta has previously
     * been saved, it will retrieve related Fornecedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in BancoAgenciaConta.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFornecedor[] List of ChildFornecedor objects
     */
    public function getFornecedorsJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFornecedorQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getFornecedors($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBancoAgencia) {
            $this->aBancoAgencia->removeBancoAgenciaConta($this);
        }
        if (null !== $this->aBanco) {
            $this->aBanco->removeBancoAgenciaConta($this);
        }
        if (null !== $this->aConvenio) {
            $this->aConvenio->removeBancoAgenciaConta($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeBancoAgenciaConta($this);
        }
        $this->idbanco_agencia_conta = null;
        $this->banco_id = null;
        $this->banco_agencia_id = null;
        $this->tipo_conta = null;
        $this->numero_conta = null;
        $this->digito_verificador = null;
        $this->convenio_id = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collContaCaixas) {
                foreach ($this->collContaCaixas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFornecedors) {
                foreach ($this->collFornecedors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collContaCaixas = null;
        $this->collFornecedors = null;
        $this->aBancoAgencia = null;
        $this->aBanco = null;
        $this->aConvenio = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BancoAgenciaContaTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
