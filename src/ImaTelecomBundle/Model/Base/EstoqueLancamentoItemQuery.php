<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\EstoqueLancamentoItem as ChildEstoqueLancamentoItem;
use ImaTelecomBundle\Model\EstoqueLancamentoItemQuery as ChildEstoqueLancamentoItemQuery;
use ImaTelecomBundle\Model\Map\EstoqueLancamentoItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'estoque_lancamento_item' table.
 *
 *
 *
 * @method     ChildEstoqueLancamentoItemQuery orderByIdestoqueLacamentoItem($order = Criteria::ASC) Order by the idestoque_lacamento_item column
 * @method     ChildEstoqueLancamentoItemQuery orderByLancamentoId($order = Criteria::ASC) Order by the lancamento_id column
 * @method     ChildEstoqueLancamentoItemQuery orderByEstoqueId($order = Criteria::ASC) Order by the estoque_id column
 * @method     ChildEstoqueLancamentoItemQuery orderByQtde($order = Criteria::ASC) Order by the qtde column
 * @method     ChildEstoqueLancamentoItemQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildEstoqueLancamentoItemQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildEstoqueLancamentoItemQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildEstoqueLancamentoItemQuery groupByIdestoqueLacamentoItem() Group by the idestoque_lacamento_item column
 * @method     ChildEstoqueLancamentoItemQuery groupByLancamentoId() Group by the lancamento_id column
 * @method     ChildEstoqueLancamentoItemQuery groupByEstoqueId() Group by the estoque_id column
 * @method     ChildEstoqueLancamentoItemQuery groupByQtde() Group by the qtde column
 * @method     ChildEstoqueLancamentoItemQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildEstoqueLancamentoItemQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildEstoqueLancamentoItemQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildEstoqueLancamentoItemQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEstoqueLancamentoItemQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEstoqueLancamentoItemQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEstoqueLancamentoItemQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEstoqueLancamentoItemQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEstoqueLancamentoItemQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEstoqueLancamentoItemQuery leftJoinEstoque($relationAlias = null) Adds a LEFT JOIN clause to the query using the Estoque relation
 * @method     ChildEstoqueLancamentoItemQuery rightJoinEstoque($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Estoque relation
 * @method     ChildEstoqueLancamentoItemQuery innerJoinEstoque($relationAlias = null) Adds a INNER JOIN clause to the query using the Estoque relation
 *
 * @method     ChildEstoqueLancamentoItemQuery joinWithEstoque($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Estoque relation
 *
 * @method     ChildEstoqueLancamentoItemQuery leftJoinWithEstoque() Adds a LEFT JOIN clause and with to the query using the Estoque relation
 * @method     ChildEstoqueLancamentoItemQuery rightJoinWithEstoque() Adds a RIGHT JOIN clause and with to the query using the Estoque relation
 * @method     ChildEstoqueLancamentoItemQuery innerJoinWithEstoque() Adds a INNER JOIN clause and with to the query using the Estoque relation
 *
 * @method     ChildEstoqueLancamentoItemQuery leftJoinEstoqueLancamento($relationAlias = null) Adds a LEFT JOIN clause to the query using the EstoqueLancamento relation
 * @method     ChildEstoqueLancamentoItemQuery rightJoinEstoqueLancamento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EstoqueLancamento relation
 * @method     ChildEstoqueLancamentoItemQuery innerJoinEstoqueLancamento($relationAlias = null) Adds a INNER JOIN clause to the query using the EstoqueLancamento relation
 *
 * @method     ChildEstoqueLancamentoItemQuery joinWithEstoqueLancamento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EstoqueLancamento relation
 *
 * @method     ChildEstoqueLancamentoItemQuery leftJoinWithEstoqueLancamento() Adds a LEFT JOIN clause and with to the query using the EstoqueLancamento relation
 * @method     ChildEstoqueLancamentoItemQuery rightJoinWithEstoqueLancamento() Adds a RIGHT JOIN clause and with to the query using the EstoqueLancamento relation
 * @method     ChildEstoqueLancamentoItemQuery innerJoinWithEstoqueLancamento() Adds a INNER JOIN clause and with to the query using the EstoqueLancamento relation
 *
 * @method     ChildEstoqueLancamentoItemQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildEstoqueLancamentoItemQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildEstoqueLancamentoItemQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildEstoqueLancamentoItemQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildEstoqueLancamentoItemQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEstoqueLancamentoItemQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEstoqueLancamentoItemQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \ImaTelecomBundle\Model\EstoqueQuery|\ImaTelecomBundle\Model\EstoqueLancamentoQuery|\ImaTelecomBundle\Model\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEstoqueLancamentoItem findOne(ConnectionInterface $con = null) Return the first ChildEstoqueLancamentoItem matching the query
 * @method     ChildEstoqueLancamentoItem findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEstoqueLancamentoItem matching the query, or a new ChildEstoqueLancamentoItem object populated from the query conditions when no match is found
 *
 * @method     ChildEstoqueLancamentoItem findOneByIdestoqueLacamentoItem(int $idestoque_lacamento_item) Return the first ChildEstoqueLancamentoItem filtered by the idestoque_lacamento_item column
 * @method     ChildEstoqueLancamentoItem findOneByLancamentoId(int $lancamento_id) Return the first ChildEstoqueLancamentoItem filtered by the lancamento_id column
 * @method     ChildEstoqueLancamentoItem findOneByEstoqueId(int $estoque_id) Return the first ChildEstoqueLancamentoItem filtered by the estoque_id column
 * @method     ChildEstoqueLancamentoItem findOneByQtde(double $qtde) Return the first ChildEstoqueLancamentoItem filtered by the qtde column
 * @method     ChildEstoqueLancamentoItem findOneByDataCadastro(string $data_cadastro) Return the first ChildEstoqueLancamentoItem filtered by the data_cadastro column
 * @method     ChildEstoqueLancamentoItem findOneByDataAlterado(string $data_alterado) Return the first ChildEstoqueLancamentoItem filtered by the data_alterado column
 * @method     ChildEstoqueLancamentoItem findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEstoqueLancamentoItem filtered by the usuario_alterado column *

 * @method     ChildEstoqueLancamentoItem requirePk($key, ConnectionInterface $con = null) Return the ChildEstoqueLancamentoItem by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamentoItem requireOne(ConnectionInterface $con = null) Return the first ChildEstoqueLancamentoItem matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEstoqueLancamentoItem requireOneByIdestoqueLacamentoItem(int $idestoque_lacamento_item) Return the first ChildEstoqueLancamentoItem filtered by the idestoque_lacamento_item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamentoItem requireOneByLancamentoId(int $lancamento_id) Return the first ChildEstoqueLancamentoItem filtered by the lancamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamentoItem requireOneByEstoqueId(int $estoque_id) Return the first ChildEstoqueLancamentoItem filtered by the estoque_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamentoItem requireOneByQtde(double $qtde) Return the first ChildEstoqueLancamentoItem filtered by the qtde column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamentoItem requireOneByDataCadastro(string $data_cadastro) Return the first ChildEstoqueLancamentoItem filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamentoItem requireOneByDataAlterado(string $data_alterado) Return the first ChildEstoqueLancamentoItem filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamentoItem requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEstoqueLancamentoItem filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEstoqueLancamentoItem[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEstoqueLancamentoItem objects based on current ModelCriteria
 * @method     ChildEstoqueLancamentoItem[]|ObjectCollection findByIdestoqueLacamentoItem(int $idestoque_lacamento_item) Return ChildEstoqueLancamentoItem objects filtered by the idestoque_lacamento_item column
 * @method     ChildEstoqueLancamentoItem[]|ObjectCollection findByLancamentoId(int $lancamento_id) Return ChildEstoqueLancamentoItem objects filtered by the lancamento_id column
 * @method     ChildEstoqueLancamentoItem[]|ObjectCollection findByEstoqueId(int $estoque_id) Return ChildEstoqueLancamentoItem objects filtered by the estoque_id column
 * @method     ChildEstoqueLancamentoItem[]|ObjectCollection findByQtde(double $qtde) Return ChildEstoqueLancamentoItem objects filtered by the qtde column
 * @method     ChildEstoqueLancamentoItem[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildEstoqueLancamentoItem objects filtered by the data_cadastro column
 * @method     ChildEstoqueLancamentoItem[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildEstoqueLancamentoItem objects filtered by the data_alterado column
 * @method     ChildEstoqueLancamentoItem[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildEstoqueLancamentoItem objects filtered by the usuario_alterado column
 * @method     ChildEstoqueLancamentoItem[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EstoqueLancamentoItemQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\EstoqueLancamentoItemQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\EstoqueLancamentoItem', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEstoqueLancamentoItemQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEstoqueLancamentoItemQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEstoqueLancamentoItemQuery) {
            return $criteria;
        }
        $query = new ChildEstoqueLancamentoItemQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEstoqueLancamentoItem|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EstoqueLancamentoItemTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EstoqueLancamentoItemTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoqueLancamentoItem A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idestoque_lacamento_item, lancamento_id, estoque_id, qtde, data_cadastro, data_alterado, usuario_alterado FROM estoque_lancamento_item WHERE idestoque_lacamento_item = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEstoqueLancamentoItem $obj */
            $obj = new ChildEstoqueLancamentoItem();
            $obj->hydrate($row);
            EstoqueLancamentoItemTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEstoqueLancamentoItem|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_IDESTOQUE_LACAMENTO_ITEM, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_IDESTOQUE_LACAMENTO_ITEM, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idestoque_lacamento_item column
     *
     * Example usage:
     * <code>
     * $query->filterByIdestoqueLacamentoItem(1234); // WHERE idestoque_lacamento_item = 1234
     * $query->filterByIdestoqueLacamentoItem(array(12, 34)); // WHERE idestoque_lacamento_item IN (12, 34)
     * $query->filterByIdestoqueLacamentoItem(array('min' => 12)); // WHERE idestoque_lacamento_item > 12
     * </code>
     *
     * @param     mixed $idestoqueLacamentoItem The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByIdestoqueLacamentoItem($idestoqueLacamentoItem = null, $comparison = null)
    {
        if (is_array($idestoqueLacamentoItem)) {
            $useMinMax = false;
            if (isset($idestoqueLacamentoItem['min'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_IDESTOQUE_LACAMENTO_ITEM, $idestoqueLacamentoItem['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idestoqueLacamentoItem['max'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_IDESTOQUE_LACAMENTO_ITEM, $idestoqueLacamentoItem['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_IDESTOQUE_LACAMENTO_ITEM, $idestoqueLacamentoItem, $comparison);
    }

    /**
     * Filter the query on the lancamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLancamentoId(1234); // WHERE lancamento_id = 1234
     * $query->filterByLancamentoId(array(12, 34)); // WHERE lancamento_id IN (12, 34)
     * $query->filterByLancamentoId(array('min' => 12)); // WHERE lancamento_id > 12
     * </code>
     *
     * @see       filterByEstoqueLancamento()
     *
     * @param     mixed $lancamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByLancamentoId($lancamentoId = null, $comparison = null)
    {
        if (is_array($lancamentoId)) {
            $useMinMax = false;
            if (isset($lancamentoId['min'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_LANCAMENTO_ID, $lancamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lancamentoId['max'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_LANCAMENTO_ID, $lancamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_LANCAMENTO_ID, $lancamentoId, $comparison);
    }

    /**
     * Filter the query on the estoque_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEstoqueId(1234); // WHERE estoque_id = 1234
     * $query->filterByEstoqueId(array(12, 34)); // WHERE estoque_id IN (12, 34)
     * $query->filterByEstoqueId(array('min' => 12)); // WHERE estoque_id > 12
     * </code>
     *
     * @see       filterByEstoque()
     *
     * @param     mixed $estoqueId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByEstoqueId($estoqueId = null, $comparison = null)
    {
        if (is_array($estoqueId)) {
            $useMinMax = false;
            if (isset($estoqueId['min'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_ESTOQUE_ID, $estoqueId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($estoqueId['max'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_ESTOQUE_ID, $estoqueId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_ESTOQUE_ID, $estoqueId, $comparison);
    }

    /**
     * Filter the query on the qtde column
     *
     * Example usage:
     * <code>
     * $query->filterByQtde(1234); // WHERE qtde = 1234
     * $query->filterByQtde(array(12, 34)); // WHERE qtde IN (12, 34)
     * $query->filterByQtde(array('min' => 12)); // WHERE qtde > 12
     * </code>
     *
     * @param     mixed $qtde The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByQtde($qtde = null, $comparison = null)
    {
        if (is_array($qtde)) {
            $useMinMax = false;
            if (isset($qtde['min'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_QTDE, $qtde['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($qtde['max'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_QTDE, $qtde['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_QTDE, $qtde, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Estoque object
     *
     * @param \ImaTelecomBundle\Model\Estoque|ObjectCollection $estoque The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByEstoque($estoque, $comparison = null)
    {
        if ($estoque instanceof \ImaTelecomBundle\Model\Estoque) {
            return $this
                ->addUsingAlias(EstoqueLancamentoItemTableMap::COL_ESTOQUE_ID, $estoque->getIdestoque(), $comparison);
        } elseif ($estoque instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EstoqueLancamentoItemTableMap::COL_ESTOQUE_ID, $estoque->toKeyValue('PrimaryKey', 'Idestoque'), $comparison);
        } else {
            throw new PropelException('filterByEstoque() only accepts arguments of type \ImaTelecomBundle\Model\Estoque or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Estoque relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function joinEstoque($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Estoque');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Estoque');
        }

        return $this;
    }

    /**
     * Use the Estoque relation Estoque object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstoque($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Estoque', '\ImaTelecomBundle\Model\EstoqueQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EstoqueLancamento object
     *
     * @param \ImaTelecomBundle\Model\EstoqueLancamento|ObjectCollection $estoqueLancamento The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByEstoqueLancamento($estoqueLancamento, $comparison = null)
    {
        if ($estoqueLancamento instanceof \ImaTelecomBundle\Model\EstoqueLancamento) {
            return $this
                ->addUsingAlias(EstoqueLancamentoItemTableMap::COL_LANCAMENTO_ID, $estoqueLancamento->getIdestoqueLancamento(), $comparison);
        } elseif ($estoqueLancamento instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EstoqueLancamentoItemTableMap::COL_LANCAMENTO_ID, $estoqueLancamento->toKeyValue('PrimaryKey', 'IdestoqueLancamento'), $comparison);
        } else {
            throw new PropelException('filterByEstoqueLancamento() only accepts arguments of type \ImaTelecomBundle\Model\EstoqueLancamento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EstoqueLancamento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function joinEstoqueLancamento($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EstoqueLancamento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EstoqueLancamento');
        }

        return $this;
    }

    /**
     * Use the EstoqueLancamento relation EstoqueLancamento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueLancamentoQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueLancamentoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstoqueLancamento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EstoqueLancamento', '\ImaTelecomBundle\Model\EstoqueLancamentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(EstoqueLancamentoItemTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EstoqueLancamentoItemTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEstoqueLancamentoItem $estoqueLancamentoItem Object to remove from the list of results
     *
     * @return $this|ChildEstoqueLancamentoItemQuery The current query, for fluid interface
     */
    public function prune($estoqueLancamentoItem = null)
    {
        if ($estoqueLancamentoItem) {
            $this->addUsingAlias(EstoqueLancamentoItemTableMap::COL_IDESTOQUE_LACAMENTO_ITEM, $estoqueLancamentoItem->getIdestoqueLacamentoItem(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the estoque_lancamento_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EstoqueLancamentoItemTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EstoqueLancamentoItemTableMap::clearInstancePool();
            EstoqueLancamentoItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EstoqueLancamentoItemTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EstoqueLancamentoItemTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EstoqueLancamentoItemTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EstoqueLancamentoItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EstoqueLancamentoItemQuery
