<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Banco as ChildBanco;
use ImaTelecomBundle\Model\BancoQuery as ChildBancoQuery;
use ImaTelecomBundle\Model\Map\BancoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'banco' table.
 *
 *
 *
 * @method     ChildBancoQuery orderByIdbanco($order = Criteria::ASC) Order by the idbanco column
 * @method     ChildBancoQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildBancoQuery orderByCodigoFebraban($order = Criteria::ASC) Order by the codigo_febraban column
 * @method     ChildBancoQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildBancoQuery orderByLayoutBancarioId($order = Criteria::ASC) Order by the layout_bancario_id column
 * @method     ChildBancoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildBancoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildBancoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildBancoQuery groupByIdbanco() Group by the idbanco column
 * @method     ChildBancoQuery groupByNome() Group by the nome column
 * @method     ChildBancoQuery groupByCodigoFebraban() Group by the codigo_febraban column
 * @method     ChildBancoQuery groupByAtivo() Group by the ativo column
 * @method     ChildBancoQuery groupByLayoutBancarioId() Group by the layout_bancario_id column
 * @method     ChildBancoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildBancoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildBancoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildBancoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBancoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBancoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBancoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBancoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBancoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBancoQuery leftJoinLayoutBancario($relationAlias = null) Adds a LEFT JOIN clause to the query using the LayoutBancario relation
 * @method     ChildBancoQuery rightJoinLayoutBancario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LayoutBancario relation
 * @method     ChildBancoQuery innerJoinLayoutBancario($relationAlias = null) Adds a INNER JOIN clause to the query using the LayoutBancario relation
 *
 * @method     ChildBancoQuery joinWithLayoutBancario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LayoutBancario relation
 *
 * @method     ChildBancoQuery leftJoinWithLayoutBancario() Adds a LEFT JOIN clause and with to the query using the LayoutBancario relation
 * @method     ChildBancoQuery rightJoinWithLayoutBancario() Adds a RIGHT JOIN clause and with to the query using the LayoutBancario relation
 * @method     ChildBancoQuery innerJoinWithLayoutBancario() Adds a INNER JOIN clause and with to the query using the LayoutBancario relation
 *
 * @method     ChildBancoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBancoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBancoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBancoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBancoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBancoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBancoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildBancoQuery leftJoinBancoAgenciaConta($relationAlias = null) Adds a LEFT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildBancoQuery rightJoinBancoAgenciaConta($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildBancoQuery innerJoinBancoAgenciaConta($relationAlias = null) Adds a INNER JOIN clause to the query using the BancoAgenciaConta relation
 *
 * @method     ChildBancoQuery joinWithBancoAgenciaConta($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildBancoQuery leftJoinWithBancoAgenciaConta() Adds a LEFT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildBancoQuery rightJoinWithBancoAgenciaConta() Adds a RIGHT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildBancoQuery innerJoinWithBancoAgenciaConta() Adds a INNER JOIN clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     \ImaTelecomBundle\Model\LayoutBancarioQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BancoAgenciaContaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBanco findOne(ConnectionInterface $con = null) Return the first ChildBanco matching the query
 * @method     ChildBanco findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBanco matching the query, or a new ChildBanco object populated from the query conditions when no match is found
 *
 * @method     ChildBanco findOneByIdbanco(int $idbanco) Return the first ChildBanco filtered by the idbanco column
 * @method     ChildBanco findOneByNome(string $nome) Return the first ChildBanco filtered by the nome column
 * @method     ChildBanco findOneByCodigoFebraban(string $codigo_febraban) Return the first ChildBanco filtered by the codigo_febraban column
 * @method     ChildBanco findOneByAtivo(int $ativo) Return the first ChildBanco filtered by the ativo column
 * @method     ChildBanco findOneByLayoutBancarioId(int $layout_bancario_id) Return the first ChildBanco filtered by the layout_bancario_id column
 * @method     ChildBanco findOneByDataCadastro(string $data_cadastro) Return the first ChildBanco filtered by the data_cadastro column
 * @method     ChildBanco findOneByDataAlterado(string $data_alterado) Return the first ChildBanco filtered by the data_alterado column
 * @method     ChildBanco findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBanco filtered by the usuario_alterado column *

 * @method     ChildBanco requirePk($key, ConnectionInterface $con = null) Return the ChildBanco by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanco requireOne(ConnectionInterface $con = null) Return the first ChildBanco matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBanco requireOneByIdbanco(int $idbanco) Return the first ChildBanco filtered by the idbanco column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanco requireOneByNome(string $nome) Return the first ChildBanco filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanco requireOneByCodigoFebraban(string $codigo_febraban) Return the first ChildBanco filtered by the codigo_febraban column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanco requireOneByAtivo(int $ativo) Return the first ChildBanco filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanco requireOneByLayoutBancarioId(int $layout_bancario_id) Return the first ChildBanco filtered by the layout_bancario_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanco requireOneByDataCadastro(string $data_cadastro) Return the first ChildBanco filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanco requireOneByDataAlterado(string $data_alterado) Return the first ChildBanco filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBanco requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBanco filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBanco[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBanco objects based on current ModelCriteria
 * @method     ChildBanco[]|ObjectCollection findByIdbanco(int $idbanco) Return ChildBanco objects filtered by the idbanco column
 * @method     ChildBanco[]|ObjectCollection findByNome(string $nome) Return ChildBanco objects filtered by the nome column
 * @method     ChildBanco[]|ObjectCollection findByCodigoFebraban(string $codigo_febraban) Return ChildBanco objects filtered by the codigo_febraban column
 * @method     ChildBanco[]|ObjectCollection findByAtivo(int $ativo) Return ChildBanco objects filtered by the ativo column
 * @method     ChildBanco[]|ObjectCollection findByLayoutBancarioId(int $layout_bancario_id) Return ChildBanco objects filtered by the layout_bancario_id column
 * @method     ChildBanco[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildBanco objects filtered by the data_cadastro column
 * @method     ChildBanco[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildBanco objects filtered by the data_alterado column
 * @method     ChildBanco[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildBanco objects filtered by the usuario_alterado column
 * @method     ChildBanco[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BancoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\BancoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Banco', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBancoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBancoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBancoQuery) {
            return $criteria;
        }
        $query = new ChildBancoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBanco|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BancoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BancoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBanco A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idbanco, nome, codigo_febraban, ativo, layout_bancario_id, data_cadastro, data_alterado, usuario_alterado FROM banco WHERE idbanco = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBanco $obj */
            $obj = new ChildBanco();
            $obj->hydrate($row);
            BancoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBanco|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BancoTableMap::COL_IDBANCO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BancoTableMap::COL_IDBANCO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idbanco column
     *
     * Example usage:
     * <code>
     * $query->filterByIdbanco(1234); // WHERE idbanco = 1234
     * $query->filterByIdbanco(array(12, 34)); // WHERE idbanco IN (12, 34)
     * $query->filterByIdbanco(array('min' => 12)); // WHERE idbanco > 12
     * </code>
     *
     * @param     mixed $idbanco The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByIdbanco($idbanco = null, $comparison = null)
    {
        if (is_array($idbanco)) {
            $useMinMax = false;
            if (isset($idbanco['min'])) {
                $this->addUsingAlias(BancoTableMap::COL_IDBANCO, $idbanco['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idbanco['max'])) {
                $this->addUsingAlias(BancoTableMap::COL_IDBANCO, $idbanco['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoTableMap::COL_IDBANCO, $idbanco, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the codigo_febraban column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigoFebraban('fooValue');   // WHERE codigo_febraban = 'fooValue'
     * $query->filterByCodigoFebraban('%fooValue%', Criteria::LIKE); // WHERE codigo_febraban LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigoFebraban The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByCodigoFebraban($codigoFebraban = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigoFebraban)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoTableMap::COL_CODIGO_FEBRABAN, $codigoFebraban, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(1234); // WHERE ativo = 1234
     * $query->filterByAtivo(array(12, 34)); // WHERE ativo IN (12, 34)
     * $query->filterByAtivo(array('min' => 12)); // WHERE ativo > 12
     * </code>
     *
     * @param     mixed $ativo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_array($ativo)) {
            $useMinMax = false;
            if (isset($ativo['min'])) {
                $this->addUsingAlias(BancoTableMap::COL_ATIVO, $ativo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ativo['max'])) {
                $this->addUsingAlias(BancoTableMap::COL_ATIVO, $ativo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the layout_bancario_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLayoutBancarioId(1234); // WHERE layout_bancario_id = 1234
     * $query->filterByLayoutBancarioId(array(12, 34)); // WHERE layout_bancario_id IN (12, 34)
     * $query->filterByLayoutBancarioId(array('min' => 12)); // WHERE layout_bancario_id > 12
     * </code>
     *
     * @see       filterByLayoutBancario()
     *
     * @param     mixed $layoutBancarioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByLayoutBancarioId($layoutBancarioId = null, $comparison = null)
    {
        if (is_array($layoutBancarioId)) {
            $useMinMax = false;
            if (isset($layoutBancarioId['min'])) {
                $this->addUsingAlias(BancoTableMap::COL_LAYOUT_BANCARIO_ID, $layoutBancarioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($layoutBancarioId['max'])) {
                $this->addUsingAlias(BancoTableMap::COL_LAYOUT_BANCARIO_ID, $layoutBancarioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoTableMap::COL_LAYOUT_BANCARIO_ID, $layoutBancarioId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(BancoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(BancoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(BancoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(BancoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(BancoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(BancoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\LayoutBancario object
     *
     * @param \ImaTelecomBundle\Model\LayoutBancario|ObjectCollection $layoutBancario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoQuery The current query, for fluid interface
     */
    public function filterByLayoutBancario($layoutBancario, $comparison = null)
    {
        if ($layoutBancario instanceof \ImaTelecomBundle\Model\LayoutBancario) {
            return $this
                ->addUsingAlias(BancoTableMap::COL_LAYOUT_BANCARIO_ID, $layoutBancario->getIdlayoutBancario(), $comparison);
        } elseif ($layoutBancario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BancoTableMap::COL_LAYOUT_BANCARIO_ID, $layoutBancario->toKeyValue('PrimaryKey', 'IdlayoutBancario'), $comparison);
        } else {
            throw new PropelException('filterByLayoutBancario() only accepts arguments of type \ImaTelecomBundle\Model\LayoutBancario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LayoutBancario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function joinLayoutBancario($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LayoutBancario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LayoutBancario');
        }

        return $this;
    }

    /**
     * Use the LayoutBancario relation LayoutBancario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\LayoutBancarioQuery A secondary query class using the current class as primary query
     */
    public function useLayoutBancarioQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLayoutBancario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LayoutBancario', '\ImaTelecomBundle\Model\LayoutBancarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(BancoTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BancoTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BancoAgenciaConta object
     *
     * @param \ImaTelecomBundle\Model\BancoAgenciaConta|ObjectCollection $bancoAgenciaConta the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBancoQuery The current query, for fluid interface
     */
    public function filterByBancoAgenciaConta($bancoAgenciaConta, $comparison = null)
    {
        if ($bancoAgenciaConta instanceof \ImaTelecomBundle\Model\BancoAgenciaConta) {
            return $this
                ->addUsingAlias(BancoTableMap::COL_IDBANCO, $bancoAgenciaConta->getBancoId(), $comparison);
        } elseif ($bancoAgenciaConta instanceof ObjectCollection) {
            return $this
                ->useBancoAgenciaContaQuery()
                ->filterByPrimaryKeys($bancoAgenciaConta->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBancoAgenciaConta() only accepts arguments of type \ImaTelecomBundle\Model\BancoAgenciaConta or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BancoAgenciaConta relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function joinBancoAgenciaConta($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BancoAgenciaConta');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BancoAgenciaConta');
        }

        return $this;
    }

    /**
     * Use the BancoAgenciaConta relation BancoAgenciaConta object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoAgenciaContaQuery A secondary query class using the current class as primary query
     */
    public function useBancoAgenciaContaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBancoAgenciaConta($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BancoAgenciaConta', '\ImaTelecomBundle\Model\BancoAgenciaContaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBanco $banco Object to remove from the list of results
     *
     * @return $this|ChildBancoQuery The current query, for fluid interface
     */
    public function prune($banco = null)
    {
        if ($banco) {
            $this->addUsingAlias(BancoTableMap::COL_IDBANCO, $banco->getIdbanco(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the banco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BancoTableMap::clearInstancePool();
            BancoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BancoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BancoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BancoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BancoQuery
