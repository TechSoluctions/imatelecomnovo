<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ViewFaturamentosMensalSuc as ChildViewFaturamentosMensalSuc;
use ImaTelecomBundle\Model\ViewFaturamentosMensalSucQuery as ChildViewFaturamentosMensalSucQuery;
use ImaTelecomBundle\Model\Map\ViewFaturamentosMensalSucTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'view_faturamentos_mensal_suc' table.
 *
 *
 *
 * @method     ChildViewFaturamentosMensalSucQuery orderByIdviewFaturamentosMensal($order = Criteria::ASC) Order by the idview_faturamentos_mensal column
 * @method     ChildViewFaturamentosMensalSucQuery orderByBoleto($order = Criteria::ASC) Order by the boleto column
 * @method     ChildViewFaturamentosMensalSucQuery orderByPagamentoParcelas($order = Criteria::ASC) Order by the pagamento_parcelas column
 * @method     ChildViewFaturamentosMensalSucQuery orderByVencimento($order = Criteria::ASC) Order by the vencimento column
 * @method     ChildViewFaturamentosMensalSucQuery orderByServicoContratado($order = Criteria::ASC) Order by the servico_contratado column
 * @method     ChildViewFaturamentosMensalSucQuery orderByValorParcela($order = Criteria::ASC) Order by the valor_parcela column
 * @method     ChildViewFaturamentosMensalSucQuery orderByCliente($order = Criteria::ASC) Order by the cliente column
 * @method     ChildViewFaturamentosMensalSucQuery orderByNome($order = Criteria::ASC) Order by the nome column
 *
 * @method     ChildViewFaturamentosMensalSucQuery groupByIdviewFaturamentosMensal() Group by the idview_faturamentos_mensal column
 * @method     ChildViewFaturamentosMensalSucQuery groupByBoleto() Group by the boleto column
 * @method     ChildViewFaturamentosMensalSucQuery groupByPagamentoParcelas() Group by the pagamento_parcelas column
 * @method     ChildViewFaturamentosMensalSucQuery groupByVencimento() Group by the vencimento column
 * @method     ChildViewFaturamentosMensalSucQuery groupByServicoContratado() Group by the servico_contratado column
 * @method     ChildViewFaturamentosMensalSucQuery groupByValorParcela() Group by the valor_parcela column
 * @method     ChildViewFaturamentosMensalSucQuery groupByCliente() Group by the cliente column
 * @method     ChildViewFaturamentosMensalSucQuery groupByNome() Group by the nome column
 *
 * @method     ChildViewFaturamentosMensalSucQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildViewFaturamentosMensalSucQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildViewFaturamentosMensalSucQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildViewFaturamentosMensalSucQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildViewFaturamentosMensalSucQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildViewFaturamentosMensalSucQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildViewFaturamentosMensalSuc findOne(ConnectionInterface $con = null) Return the first ChildViewFaturamentosMensalSuc matching the query
 * @method     ChildViewFaturamentosMensalSuc findOneOrCreate(ConnectionInterface $con = null) Return the first ChildViewFaturamentosMensalSuc matching the query, or a new ChildViewFaturamentosMensalSuc object populated from the query conditions when no match is found
 *
 * @method     ChildViewFaturamentosMensalSuc findOneByIdviewFaturamentosMensal(int $idview_faturamentos_mensal) Return the first ChildViewFaturamentosMensalSuc filtered by the idview_faturamentos_mensal column
 * @method     ChildViewFaturamentosMensalSuc findOneByBoleto(int $boleto) Return the first ChildViewFaturamentosMensalSuc filtered by the boleto column
 * @method     ChildViewFaturamentosMensalSuc findOneByPagamentoParcelas(int $pagamento_parcelas) Return the first ChildViewFaturamentosMensalSuc filtered by the pagamento_parcelas column
 * @method     ChildViewFaturamentosMensalSuc findOneByVencimento(string $vencimento) Return the first ChildViewFaturamentosMensalSuc filtered by the vencimento column
 * @method     ChildViewFaturamentosMensalSuc findOneByServicoContratado(int $servico_contratado) Return the first ChildViewFaturamentosMensalSuc filtered by the servico_contratado column
 * @method     ChildViewFaturamentosMensalSuc findOneByValorParcela(string $valor_parcela) Return the first ChildViewFaturamentosMensalSuc filtered by the valor_parcela column
 * @method     ChildViewFaturamentosMensalSuc findOneByCliente(int $cliente) Return the first ChildViewFaturamentosMensalSuc filtered by the cliente column
 * @method     ChildViewFaturamentosMensalSuc findOneByNome(string $nome) Return the first ChildViewFaturamentosMensalSuc filtered by the nome column *

 * @method     ChildViewFaturamentosMensalSuc requirePk($key, ConnectionInterface $con = null) Return the ChildViewFaturamentosMensalSuc by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewFaturamentosMensalSuc requireOne(ConnectionInterface $con = null) Return the first ChildViewFaturamentosMensalSuc matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildViewFaturamentosMensalSuc requireOneByIdviewFaturamentosMensal(int $idview_faturamentos_mensal) Return the first ChildViewFaturamentosMensalSuc filtered by the idview_faturamentos_mensal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewFaturamentosMensalSuc requireOneByBoleto(int $boleto) Return the first ChildViewFaturamentosMensalSuc filtered by the boleto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewFaturamentosMensalSuc requireOneByPagamentoParcelas(int $pagamento_parcelas) Return the first ChildViewFaturamentosMensalSuc filtered by the pagamento_parcelas column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewFaturamentosMensalSuc requireOneByVencimento(string $vencimento) Return the first ChildViewFaturamentosMensalSuc filtered by the vencimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewFaturamentosMensalSuc requireOneByServicoContratado(int $servico_contratado) Return the first ChildViewFaturamentosMensalSuc filtered by the servico_contratado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewFaturamentosMensalSuc requireOneByValorParcela(string $valor_parcela) Return the first ChildViewFaturamentosMensalSuc filtered by the valor_parcela column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewFaturamentosMensalSuc requireOneByCliente(int $cliente) Return the first ChildViewFaturamentosMensalSuc filtered by the cliente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildViewFaturamentosMensalSuc requireOneByNome(string $nome) Return the first ChildViewFaturamentosMensalSuc filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildViewFaturamentosMensalSuc[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildViewFaturamentosMensalSuc objects based on current ModelCriteria
 * @method     ChildViewFaturamentosMensalSuc[]|ObjectCollection findByIdviewFaturamentosMensal(int $idview_faturamentos_mensal) Return ChildViewFaturamentosMensalSuc objects filtered by the idview_faturamentos_mensal column
 * @method     ChildViewFaturamentosMensalSuc[]|ObjectCollection findByBoleto(int $boleto) Return ChildViewFaturamentosMensalSuc objects filtered by the boleto column
 * @method     ChildViewFaturamentosMensalSuc[]|ObjectCollection findByPagamentoParcelas(int $pagamento_parcelas) Return ChildViewFaturamentosMensalSuc objects filtered by the pagamento_parcelas column
 * @method     ChildViewFaturamentosMensalSuc[]|ObjectCollection findByVencimento(string $vencimento) Return ChildViewFaturamentosMensalSuc objects filtered by the vencimento column
 * @method     ChildViewFaturamentosMensalSuc[]|ObjectCollection findByServicoContratado(int $servico_contratado) Return ChildViewFaturamentosMensalSuc objects filtered by the servico_contratado column
 * @method     ChildViewFaturamentosMensalSuc[]|ObjectCollection findByValorParcela(string $valor_parcela) Return ChildViewFaturamentosMensalSuc objects filtered by the valor_parcela column
 * @method     ChildViewFaturamentosMensalSuc[]|ObjectCollection findByCliente(int $cliente) Return ChildViewFaturamentosMensalSuc objects filtered by the cliente column
 * @method     ChildViewFaturamentosMensalSuc[]|ObjectCollection findByNome(string $nome) Return ChildViewFaturamentosMensalSuc objects filtered by the nome column
 * @method     ChildViewFaturamentosMensalSuc[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ViewFaturamentosMensalSucQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ViewFaturamentosMensalSucQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ViewFaturamentosMensalSuc', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildViewFaturamentosMensalSucQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildViewFaturamentosMensalSucQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildViewFaturamentosMensalSucQuery) {
            return $criteria;
        }
        $query = new ChildViewFaturamentosMensalSucQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildViewFaturamentosMensalSuc|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ViewFaturamentosMensalSucTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ViewFaturamentosMensalSucTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildViewFaturamentosMensalSuc A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idview_faturamentos_mensal, boleto, pagamento_parcelas, vencimento, servico_contratado, valor_parcela, cliente, nome FROM view_faturamentos_mensal_suc WHERE idview_faturamentos_mensal = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildViewFaturamentosMensalSuc $obj */
            $obj = new ChildViewFaturamentosMensalSuc();
            $obj->hydrate($row);
            ViewFaturamentosMensalSucTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildViewFaturamentosMensalSuc|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idview_faturamentos_mensal column
     *
     * Example usage:
     * <code>
     * $query->filterByIdviewFaturamentosMensal(1234); // WHERE idview_faturamentos_mensal = 1234
     * $query->filterByIdviewFaturamentosMensal(array(12, 34)); // WHERE idview_faturamentos_mensal IN (12, 34)
     * $query->filterByIdviewFaturamentosMensal(array('min' => 12)); // WHERE idview_faturamentos_mensal > 12
     * </code>
     *
     * @param     mixed $idviewFaturamentosMensal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByIdviewFaturamentosMensal($idviewFaturamentosMensal = null, $comparison = null)
    {
        if (is_array($idviewFaturamentosMensal)) {
            $useMinMax = false;
            if (isset($idviewFaturamentosMensal['min'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL, $idviewFaturamentosMensal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idviewFaturamentosMensal['max'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL, $idviewFaturamentosMensal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL, $idviewFaturamentosMensal, $comparison);
    }

    /**
     * Filter the query on the boleto column
     *
     * Example usage:
     * <code>
     * $query->filterByBoleto(1234); // WHERE boleto = 1234
     * $query->filterByBoleto(array(12, 34)); // WHERE boleto IN (12, 34)
     * $query->filterByBoleto(array('min' => 12)); // WHERE boleto > 12
     * </code>
     *
     * @param     mixed $boleto The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto = null, $comparison = null)
    {
        if (is_array($boleto)) {
            $useMinMax = false;
            if (isset($boleto['min'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_BOLETO, $boleto['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($boleto['max'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_BOLETO, $boleto['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_BOLETO, $boleto, $comparison);
    }

    /**
     * Filter the query on the pagamento_parcelas column
     *
     * Example usage:
     * <code>
     * $query->filterByPagamentoParcelas(1234); // WHERE pagamento_parcelas = 1234
     * $query->filterByPagamentoParcelas(array(12, 34)); // WHERE pagamento_parcelas IN (12, 34)
     * $query->filterByPagamentoParcelas(array('min' => 12)); // WHERE pagamento_parcelas > 12
     * </code>
     *
     * @param     mixed $pagamentoParcelas The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByPagamentoParcelas($pagamentoParcelas = null, $comparison = null)
    {
        if (is_array($pagamentoParcelas)) {
            $useMinMax = false;
            if (isset($pagamentoParcelas['min'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_PAGAMENTO_PARCELAS, $pagamentoParcelas['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pagamentoParcelas['max'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_PAGAMENTO_PARCELAS, $pagamentoParcelas['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_PAGAMENTO_PARCELAS, $pagamentoParcelas, $comparison);
    }

    /**
     * Filter the query on the vencimento column
     *
     * Example usage:
     * <code>
     * $query->filterByVencimento('fooValue');   // WHERE vencimento = 'fooValue'
     * $query->filterByVencimento('%fooValue%', Criteria::LIKE); // WHERE vencimento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $vencimento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByVencimento($vencimento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vencimento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_VENCIMENTO, $vencimento, $comparison);
    }

    /**
     * Filter the query on the servico_contratado column
     *
     * Example usage:
     * <code>
     * $query->filterByServicoContratado(1234); // WHERE servico_contratado = 1234
     * $query->filterByServicoContratado(array(12, 34)); // WHERE servico_contratado IN (12, 34)
     * $query->filterByServicoContratado(array('min' => 12)); // WHERE servico_contratado > 12
     * </code>
     *
     * @param     mixed $servicoContratado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByServicoContratado($servicoContratado = null, $comparison = null)
    {
        if (is_array($servicoContratado)) {
            $useMinMax = false;
            if (isset($servicoContratado['min'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_SERVICO_CONTRATADO, $servicoContratado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($servicoContratado['max'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_SERVICO_CONTRATADO, $servicoContratado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_SERVICO_CONTRATADO, $servicoContratado, $comparison);
    }

    /**
     * Filter the query on the valor_parcela column
     *
     * Example usage:
     * <code>
     * $query->filterByValorParcela(1234); // WHERE valor_parcela = 1234
     * $query->filterByValorParcela(array(12, 34)); // WHERE valor_parcela IN (12, 34)
     * $query->filterByValorParcela(array('min' => 12)); // WHERE valor_parcela > 12
     * </code>
     *
     * @param     mixed $valorParcela The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByValorParcela($valorParcela = null, $comparison = null)
    {
        if (is_array($valorParcela)) {
            $useMinMax = false;
            if (isset($valorParcela['min'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_VALOR_PARCELA, $valorParcela['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorParcela['max'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_VALOR_PARCELA, $valorParcela['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_VALOR_PARCELA, $valorParcela, $comparison);
    }

    /**
     * Filter the query on the cliente column
     *
     * Example usage:
     * <code>
     * $query->filterByCliente(1234); // WHERE cliente = 1234
     * $query->filterByCliente(array(12, 34)); // WHERE cliente IN (12, 34)
     * $query->filterByCliente(array('min' => 12)); // WHERE cliente > 12
     * </code>
     *
     * @param     mixed $cliente The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByCliente($cliente = null, $comparison = null)
    {
        if (is_array($cliente)) {
            $useMinMax = false;
            if (isset($cliente['min'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_CLIENTE, $cliente['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cliente['max'])) {
                $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_CLIENTE, $cliente['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_CLIENTE, $cliente, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildViewFaturamentosMensalSuc $viewFaturamentosMensalSuc Object to remove from the list of results
     *
     * @return $this|ChildViewFaturamentosMensalSucQuery The current query, for fluid interface
     */
    public function prune($viewFaturamentosMensalSuc = null)
    {
        if ($viewFaturamentosMensalSuc) {
            $this->addUsingAlias(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL, $viewFaturamentosMensalSuc->getIdviewFaturamentosMensal(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the view_faturamentos_mensal_suc table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ViewFaturamentosMensalSucTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ViewFaturamentosMensalSucTableMap::clearInstancePool();
            ViewFaturamentosMensalSucTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ViewFaturamentosMensalSucTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ViewFaturamentosMensalSucTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ViewFaturamentosMensalSucTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ViewFaturamentosMensalSucTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ViewFaturamentosMensalSucQuery
