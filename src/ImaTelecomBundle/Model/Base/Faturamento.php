<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Competencia as ChildCompetencia;
use ImaTelecomBundle\Model\CompetenciaQuery as ChildCompetenciaQuery;
use ImaTelecomBundle\Model\FaturamentoQuery as ChildFaturamentoQuery;
use ImaTelecomBundle\Model\Map\FaturamentoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'faturamento' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Faturamento implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\FaturamentoTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idfaturamento field.
     *
     * @var        int
     */
    protected $idfaturamento;

    /**
     * The value for the descricao field.
     *
     * @var        string
     */
    protected $descricao;

    /**
     * The value for the competencia_id field.
     *
     * @var        int
     */
    protected $competencia_id;

    /**
     * The value for the data_faturamento field.
     *
     * @var        DateTime
     */
    protected $data_faturamento;

    /**
     * The value for the qtde_total_contratos field.
     *
     * @var        int
     */
    protected $qtde_total_contratos;

    /**
     * The value for the valor_total_contratos field.
     *
     * @var        string
     */
    protected $valor_total_contratos;

    /**
     * The value for the qtde_total_contratos_pj field.
     *
     * @var        int
     */
    protected $qtde_total_contratos_pj;

    /**
     * The value for the valor_total_contratos_pj field.
     *
     * @var        string
     */
    protected $valor_total_contratos_pj;

    /**
     * The value for the qtde_total_contratos_pf field.
     *
     * @var        int
     */
    protected $qtde_total_contratos_pf;

    /**
     * The value for the valor_total_contratos_pf field.
     *
     * @var        string
     */
    protected $valor_total_contratos_pf;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * @var        ChildCompetencia
     */
    protected $aCompetencia;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Faturamento object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Faturamento</code> instance.  If
     * <code>obj</code> is an instance of <code>Faturamento</code>, delegates to
     * <code>equals(Faturamento)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Faturamento The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idfaturamento] column value.
     *
     * @return int
     */
    public function getIdfaturamento()
    {
        return $this->idfaturamento;
    }

    /**
     * Get the [descricao] column value.
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Get the [competencia_id] column value.
     *
     * @return int
     */
    public function getCompetenciaId()
    {
        return $this->competencia_id;
    }

    /**
     * Get the [optionally formatted] temporal [data_faturamento] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataFaturamento($format = NULL)
    {
        if ($format === null) {
            return $this->data_faturamento;
        } else {
            return $this->data_faturamento instanceof \DateTimeInterface ? $this->data_faturamento->format($format) : null;
        }
    }

    /**
     * Get the [qtde_total_contratos] column value.
     *
     * @return int
     */
    public function getQtdeTotalContratos()
    {
        return $this->qtde_total_contratos;
    }

    /**
     * Get the [valor_total_contratos] column value.
     *
     * @return string
     */
    public function getValorTotalContratos()
    {
        return $this->valor_total_contratos;
    }

    /**
     * Get the [qtde_total_contratos_pj] column value.
     *
     * @return int
     */
    public function getQtdeTotalContratosPj()
    {
        return $this->qtde_total_contratos_pj;
    }

    /**
     * Get the [valor_total_contratos_pj] column value.
     *
     * @return string
     */
    public function getValorTotalContratosPj()
    {
        return $this->valor_total_contratos_pj;
    }

    /**
     * Get the [qtde_total_contratos_pf] column value.
     *
     * @return int
     */
    public function getQtdeTotalContratosPf()
    {
        return $this->qtde_total_contratos_pf;
    }

    /**
     * Get the [valor_total_contratos_pf] column value.
     *
     * @return string
     */
    public function getValorTotalContratosPf()
    {
        return $this->valor_total_contratos_pf;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Set the value of [idfaturamento] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setIdfaturamento($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idfaturamento !== $v) {
            $this->idfaturamento = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_IDFATURAMENTO] = true;
        }

        return $this;
    } // setIdfaturamento()

    /**
     * Set the value of [descricao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setDescricao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao !== $v) {
            $this->descricao = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_DESCRICAO] = true;
        }

        return $this;
    } // setDescricao()

    /**
     * Set the value of [competencia_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setCompetenciaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->competencia_id !== $v) {
            $this->competencia_id = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_COMPETENCIA_ID] = true;
        }

        if ($this->aCompetencia !== null && $this->aCompetencia->getId() !== $v) {
            $this->aCompetencia = null;
        }

        return $this;
    } // setCompetenciaId()

    /**
     * Sets the value of [data_faturamento] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setDataFaturamento($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_faturamento !== null || $dt !== null) {
            if ($this->data_faturamento === null || $dt === null || $dt->format("Y-m-d") !== $this->data_faturamento->format("Y-m-d")) {
                $this->data_faturamento = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FaturamentoTableMap::COL_DATA_FATURAMENTO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataFaturamento()

    /**
     * Set the value of [qtde_total_contratos] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setQtdeTotalContratos($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->qtde_total_contratos !== $v) {
            $this->qtde_total_contratos = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS] = true;
        }

        return $this;
    } // setQtdeTotalContratos()

    /**
     * Set the value of [valor_total_contratos] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setValorTotalContratos($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_total_contratos !== $v) {
            $this->valor_total_contratos = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS] = true;
        }

        return $this;
    } // setValorTotalContratos()

    /**
     * Set the value of [qtde_total_contratos_pj] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setQtdeTotalContratosPj($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->qtde_total_contratos_pj !== $v) {
            $this->qtde_total_contratos_pj = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ] = true;
        }

        return $this;
    } // setQtdeTotalContratosPj()

    /**
     * Set the value of [valor_total_contratos_pj] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setValorTotalContratosPj($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_total_contratos_pj !== $v) {
            $this->valor_total_contratos_pj = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ] = true;
        }

        return $this;
    } // setValorTotalContratosPj()

    /**
     * Set the value of [qtde_total_contratos_pf] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setQtdeTotalContratosPf($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->qtde_total_contratos_pf !== $v) {
            $this->qtde_total_contratos_pf = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF] = true;
        }

        return $this;
    } // setQtdeTotalContratosPf()

    /**
     * Set the value of [valor_total_contratos_pf] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setValorTotalContratosPf($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_total_contratos_pf !== $v) {
            $this->valor_total_contratos_pf = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF] = true;
        }

        return $this;
    } // setValorTotalContratosPf()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FaturamentoTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FaturamentoTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[FaturamentoTableMap::COL_USUARIO_ALTERADO] = true;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : FaturamentoTableMap::translateFieldName('Idfaturamento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idfaturamento = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : FaturamentoTableMap::translateFieldName('Descricao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : FaturamentoTableMap::translateFieldName('CompetenciaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->competencia_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : FaturamentoTableMap::translateFieldName('DataFaturamento', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->data_faturamento = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : FaturamentoTableMap::translateFieldName('QtdeTotalContratos', TableMap::TYPE_PHPNAME, $indexType)];
            $this->qtde_total_contratos = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : FaturamentoTableMap::translateFieldName('ValorTotalContratos', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_total_contratos = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : FaturamentoTableMap::translateFieldName('QtdeTotalContratosPj', TableMap::TYPE_PHPNAME, $indexType)];
            $this->qtde_total_contratos_pj = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : FaturamentoTableMap::translateFieldName('ValorTotalContratosPj', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_total_contratos_pj = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : FaturamentoTableMap::translateFieldName('QtdeTotalContratosPf', TableMap::TYPE_PHPNAME, $indexType)];
            $this->qtde_total_contratos_pf = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : FaturamentoTableMap::translateFieldName('ValorTotalContratosPf', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_total_contratos_pf = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : FaturamentoTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : FaturamentoTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : FaturamentoTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 13; // 13 = FaturamentoTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Faturamento'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCompetencia !== null && $this->competencia_id !== $this->aCompetencia->getId()) {
            $this->aCompetencia = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FaturamentoTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildFaturamentoQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCompetencia = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Faturamento::setDeleted()
     * @see Faturamento::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FaturamentoTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildFaturamentoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FaturamentoTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FaturamentoTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCompetencia !== null) {
                if ($this->aCompetencia->isModified() || $this->aCompetencia->isNew()) {
                    $affectedRows += $this->aCompetencia->save($con);
                }
                $this->setCompetencia($this->aCompetencia);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[FaturamentoTableMap::COL_IDFATURAMENTO] = true;
        if (null !== $this->idfaturamento) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . FaturamentoTableMap::COL_IDFATURAMENTO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FaturamentoTableMap::COL_IDFATURAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'idfaturamento';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_DESCRICAO)) {
            $modifiedColumns[':p' . $index++]  = 'descricao';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_COMPETENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'competencia_id';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_DATA_FATURAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'data_faturamento';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS)) {
            $modifiedColumns[':p' . $index++]  = 'qtde_total_contratos';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS)) {
            $modifiedColumns[':p' . $index++]  = 'valor_total_contratos';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ)) {
            $modifiedColumns[':p' . $index++]  = 'qtde_total_contratos_pj';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ)) {
            $modifiedColumns[':p' . $index++]  = 'valor_total_contratos_pj';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF)) {
            $modifiedColumns[':p' . $index++]  = 'qtde_total_contratos_pf';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF)) {
            $modifiedColumns[':p' . $index++]  = 'valor_total_contratos_pf';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }

        $sql = sprintf(
            'INSERT INTO faturamento (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idfaturamento':
                        $stmt->bindValue($identifier, $this->idfaturamento, PDO::PARAM_INT);
                        break;
                    case 'descricao':
                        $stmt->bindValue($identifier, $this->descricao, PDO::PARAM_STR);
                        break;
                    case 'competencia_id':
                        $stmt->bindValue($identifier, $this->competencia_id, PDO::PARAM_INT);
                        break;
                    case 'data_faturamento':
                        $stmt->bindValue($identifier, $this->data_faturamento ? $this->data_faturamento->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'qtde_total_contratos':
                        $stmt->bindValue($identifier, $this->qtde_total_contratos, PDO::PARAM_INT);
                        break;
                    case 'valor_total_contratos':
                        $stmt->bindValue($identifier, $this->valor_total_contratos, PDO::PARAM_STR);
                        break;
                    case 'qtde_total_contratos_pj':
                        $stmt->bindValue($identifier, $this->qtde_total_contratos_pj, PDO::PARAM_INT);
                        break;
                    case 'valor_total_contratos_pj':
                        $stmt->bindValue($identifier, $this->valor_total_contratos_pj, PDO::PARAM_STR);
                        break;
                    case 'qtde_total_contratos_pf':
                        $stmt->bindValue($identifier, $this->qtde_total_contratos_pf, PDO::PARAM_INT);
                        break;
                    case 'valor_total_contratos_pf':
                        $stmt->bindValue($identifier, $this->valor_total_contratos_pf, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdfaturamento($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FaturamentoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdfaturamento();
                break;
            case 1:
                return $this->getDescricao();
                break;
            case 2:
                return $this->getCompetenciaId();
                break;
            case 3:
                return $this->getDataFaturamento();
                break;
            case 4:
                return $this->getQtdeTotalContratos();
                break;
            case 5:
                return $this->getValorTotalContratos();
                break;
            case 6:
                return $this->getQtdeTotalContratosPj();
                break;
            case 7:
                return $this->getValorTotalContratosPj();
                break;
            case 8:
                return $this->getQtdeTotalContratosPf();
                break;
            case 9:
                return $this->getValorTotalContratosPf();
                break;
            case 10:
                return $this->getDataCadastro();
                break;
            case 11:
                return $this->getDataAlterado();
                break;
            case 12:
                return $this->getUsuarioAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Faturamento'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Faturamento'][$this->hashCode()] = true;
        $keys = FaturamentoTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdfaturamento(),
            $keys[1] => $this->getDescricao(),
            $keys[2] => $this->getCompetenciaId(),
            $keys[3] => $this->getDataFaturamento(),
            $keys[4] => $this->getQtdeTotalContratos(),
            $keys[5] => $this->getValorTotalContratos(),
            $keys[6] => $this->getQtdeTotalContratosPj(),
            $keys[7] => $this->getValorTotalContratosPj(),
            $keys[8] => $this->getQtdeTotalContratosPf(),
            $keys[9] => $this->getValorTotalContratosPf(),
            $keys[10] => $this->getDataCadastro(),
            $keys[11] => $this->getDataAlterado(),
            $keys[12] => $this->getUsuarioAlterado(),
        );
        if ($result[$keys[3]] instanceof \DateTime) {
            $result[$keys[3]] = $result[$keys[3]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTime) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTime) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCompetencia) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'competencia';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'competencia';
                        break;
                    default:
                        $key = 'Competencia';
                }

                $result[$key] = $this->aCompetencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Faturamento
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FaturamentoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Faturamento
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdfaturamento($value);
                break;
            case 1:
                $this->setDescricao($value);
                break;
            case 2:
                $this->setCompetenciaId($value);
                break;
            case 3:
                $this->setDataFaturamento($value);
                break;
            case 4:
                $this->setQtdeTotalContratos($value);
                break;
            case 5:
                $this->setValorTotalContratos($value);
                break;
            case 6:
                $this->setQtdeTotalContratosPj($value);
                break;
            case 7:
                $this->setValorTotalContratosPj($value);
                break;
            case 8:
                $this->setQtdeTotalContratosPf($value);
                break;
            case 9:
                $this->setValorTotalContratosPf($value);
                break;
            case 10:
                $this->setDataCadastro($value);
                break;
            case 11:
                $this->setDataAlterado($value);
                break;
            case 12:
                $this->setUsuarioAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = FaturamentoTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdfaturamento($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDescricao($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCompetenciaId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDataFaturamento($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setQtdeTotalContratos($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setValorTotalContratos($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setQtdeTotalContratosPj($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setValorTotalContratosPj($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setQtdeTotalContratosPf($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setValorTotalContratosPf($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDataCadastro($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setDataAlterado($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[12]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(FaturamentoTableMap::DATABASE_NAME);

        if ($this->isColumnModified(FaturamentoTableMap::COL_IDFATURAMENTO)) {
            $criteria->add(FaturamentoTableMap::COL_IDFATURAMENTO, $this->idfaturamento);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_DESCRICAO)) {
            $criteria->add(FaturamentoTableMap::COL_DESCRICAO, $this->descricao);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_COMPETENCIA_ID)) {
            $criteria->add(FaturamentoTableMap::COL_COMPETENCIA_ID, $this->competencia_id);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_DATA_FATURAMENTO)) {
            $criteria->add(FaturamentoTableMap::COL_DATA_FATURAMENTO, $this->data_faturamento);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS)) {
            $criteria->add(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS, $this->qtde_total_contratos);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS)) {
            $criteria->add(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS, $this->valor_total_contratos);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ)) {
            $criteria->add(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ, $this->qtde_total_contratos_pj);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ)) {
            $criteria->add(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ, $this->valor_total_contratos_pj);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF)) {
            $criteria->add(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF, $this->qtde_total_contratos_pf);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF)) {
            $criteria->add(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF, $this->valor_total_contratos_pf);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(FaturamentoTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(FaturamentoTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(FaturamentoTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(FaturamentoTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildFaturamentoQuery::create();
        $criteria->add(FaturamentoTableMap::COL_IDFATURAMENTO, $this->idfaturamento);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdfaturamento();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdfaturamento();
    }

    /**
     * Generic method to set the primary key (idfaturamento column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdfaturamento($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdfaturamento();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Faturamento (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDescricao($this->getDescricao());
        $copyObj->setCompetenciaId($this->getCompetenciaId());
        $copyObj->setDataFaturamento($this->getDataFaturamento());
        $copyObj->setQtdeTotalContratos($this->getQtdeTotalContratos());
        $copyObj->setValorTotalContratos($this->getValorTotalContratos());
        $copyObj->setQtdeTotalContratosPj($this->getQtdeTotalContratosPj());
        $copyObj->setValorTotalContratosPj($this->getValorTotalContratosPj());
        $copyObj->setQtdeTotalContratosPf($this->getQtdeTotalContratosPf());
        $copyObj->setValorTotalContratosPf($this->getValorTotalContratosPf());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdfaturamento(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Faturamento Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCompetencia object.
     *
     * @param  ChildCompetencia $v
     * @return $this|\ImaTelecomBundle\Model\Faturamento The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCompetencia(ChildCompetencia $v = null)
    {
        if ($v === null) {
            $this->setCompetenciaId(NULL);
        } else {
            $this->setCompetenciaId($v->getId());
        }

        $this->aCompetencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCompetencia object, it will not be re-added.
        if ($v !== null) {
            $v->addFaturamento($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCompetencia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCompetencia The associated ChildCompetencia object.
     * @throws PropelException
     */
    public function getCompetencia(ConnectionInterface $con = null)
    {
        if ($this->aCompetencia === null && ($this->competencia_id !== null)) {
            $this->aCompetencia = ChildCompetenciaQuery::create()->findPk($this->competencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCompetencia->addFaturamentos($this);
             */
        }

        return $this->aCompetencia;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCompetencia) {
            $this->aCompetencia->removeFaturamento($this);
        }
        $this->idfaturamento = null;
        $this->descricao = null;
        $this->competencia_id = null;
        $this->data_faturamento = null;
        $this->qtde_total_contratos = null;
        $this->valor_total_contratos = null;
        $this->qtde_total_contratos_pj = null;
        $this->valor_total_contratos_pj = null;
        $this->qtde_total_contratos_pf = null;
        $this->valor_total_contratos_pf = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aCompetencia = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(FaturamentoTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
