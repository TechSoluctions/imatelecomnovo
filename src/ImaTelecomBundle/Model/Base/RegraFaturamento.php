<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Contrato as ChildContrato;
use ImaTelecomBundle\Model\ContratoQuery as ChildContratoQuery;
use ImaTelecomBundle\Model\RegraFaturamento as ChildRegraFaturamento;
use ImaTelecomBundle\Model\RegraFaturamentoQuery as ChildRegraFaturamentoQuery;
use ImaTelecomBundle\Model\Map\ContratoTableMap;
use ImaTelecomBundle\Model\Map\RegraFaturamentoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'regra_faturamento' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class RegraFaturamento implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\RegraFaturamentoTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idregra_faturamento field.
     *
     * @var        int
     */
    protected $idregra_faturamento;

    /**
     * The value for the descricao field.
     *
     * @var        string
     */
    protected $descricao;

    /**
     * The value for the periodo1_dia_vencimento_cobranca field.
     *
     * @var        int
     */
    protected $periodo1_dia_vencimento_cobranca;

    /**
     * The value for the periodo1_dia_inicial_cobranca field.
     *
     * @var        int
     */
    protected $periodo1_dia_inicial_cobranca;

    /**
     * The value for the periodo1_dia_final_cobranca field.
     *
     * @var        int
     */
    protected $periodo1_dia_final_cobranca;

    /**
     * The value for the mes_subsequente field.
     *
     * @var        string
     */
    protected $mes_subsequente;

    /**
     * The value for the periodo2_dia_vencimento_cobranca field.
     *
     * @var        int
     */
    protected $periodo2_dia_vencimento_cobranca;

    /**
     * The value for the periodo2_dia_inicial_cobranca field.
     *
     * @var        int
     */
    protected $periodo2_dia_inicial_cobranca;

    /**
     * The value for the periodo2_dia_final_cobranca field.
     *
     * @var        int
     */
    protected $periodo2_dia_final_cobranca;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * @var        ObjectCollection|ChildContrato[] Collection to store aggregation of ChildContrato objects.
     */
    protected $collContratos;
    protected $collContratosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContrato[]
     */
    protected $contratosScheduledForDeletion = null;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\RegraFaturamento object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>RegraFaturamento</code> instance.  If
     * <code>obj</code> is an instance of <code>RegraFaturamento</code>, delegates to
     * <code>equals(RegraFaturamento)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|RegraFaturamento The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idregra_faturamento] column value.
     *
     * @return int
     */
    public function getIdregraFaturamento()
    {
        return $this->idregra_faturamento;
    }

    /**
     * Get the [descricao] column value.
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Get the [periodo1_dia_vencimento_cobranca] column value.
     *
     * @return int
     */
    public function getPeriodo1DiaVencimentoCobranca()
    {
        return $this->periodo1_dia_vencimento_cobranca;
    }

    /**
     * Get the [periodo1_dia_inicial_cobranca] column value.
     *
     * @return int
     */
    public function getPeriodo1DiaInicialCobranca()
    {
        return $this->periodo1_dia_inicial_cobranca;
    }

    /**
     * Get the [periodo1_dia_final_cobranca] column value.
     *
     * @return int
     */
    public function getPeriodo1DiaFinalCobranca()
    {
        return $this->periodo1_dia_final_cobranca;
    }

    /**
     * Get the [mes_subsequente] column value.
     *
     * @return string
     */
    public function getMesSubsequente()
    {
        return $this->mes_subsequente;
    }

    /**
     * Get the [periodo2_dia_vencimento_cobranca] column value.
     *
     * @return int
     */
    public function getPeriodo2DiaVencimentoCobranca()
    {
        return $this->periodo2_dia_vencimento_cobranca;
    }

    /**
     * Get the [periodo2_dia_inicial_cobranca] column value.
     *
     * @return int
     */
    public function getPeriodo2DiaInicialCobranca()
    {
        return $this->periodo2_dia_inicial_cobranca;
    }

    /**
     * Get the [periodo2_dia_final_cobranca] column value.
     *
     * @return int
     */
    public function getPeriodo2DiaFinalCobranca()
    {
        return $this->periodo2_dia_final_cobranca;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Set the value of [idregra_faturamento] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setIdregraFaturamento($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idregra_faturamento !== $v) {
            $this->idregra_faturamento = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO] = true;
        }

        return $this;
    } // setIdregraFaturamento()

    /**
     * Set the value of [descricao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setDescricao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao !== $v) {
            $this->descricao = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_DESCRICAO] = true;
        }

        return $this;
    } // setDescricao()

    /**
     * Set the value of [periodo1_dia_vencimento_cobranca] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setPeriodo1DiaVencimentoCobranca($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->periodo1_dia_vencimento_cobranca !== $v) {
            $this->periodo1_dia_vencimento_cobranca = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA] = true;
        }

        return $this;
    } // setPeriodo1DiaVencimentoCobranca()

    /**
     * Set the value of [periodo1_dia_inicial_cobranca] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setPeriodo1DiaInicialCobranca($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->periodo1_dia_inicial_cobranca !== $v) {
            $this->periodo1_dia_inicial_cobranca = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA] = true;
        }

        return $this;
    } // setPeriodo1DiaInicialCobranca()

    /**
     * Set the value of [periodo1_dia_final_cobranca] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setPeriodo1DiaFinalCobranca($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->periodo1_dia_final_cobranca !== $v) {
            $this->periodo1_dia_final_cobranca = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA] = true;
        }

        return $this;
    } // setPeriodo1DiaFinalCobranca()

    /**
     * Set the value of [mes_subsequente] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setMesSubsequente($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mes_subsequente !== $v) {
            $this->mes_subsequente = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_MES_SUBSEQUENTE] = true;
        }

        return $this;
    } // setMesSubsequente()

    /**
     * Set the value of [periodo2_dia_vencimento_cobranca] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setPeriodo2DiaVencimentoCobranca($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->periodo2_dia_vencimento_cobranca !== $v) {
            $this->periodo2_dia_vencimento_cobranca = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA] = true;
        }

        return $this;
    } // setPeriodo2DiaVencimentoCobranca()

    /**
     * Set the value of [periodo2_dia_inicial_cobranca] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setPeriodo2DiaInicialCobranca($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->periodo2_dia_inicial_cobranca !== $v) {
            $this->periodo2_dia_inicial_cobranca = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA] = true;
        }

        return $this;
    } // setPeriodo2DiaInicialCobranca()

    /**
     * Set the value of [periodo2_dia_final_cobranca] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setPeriodo2DiaFinalCobranca($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->periodo2_dia_final_cobranca !== $v) {
            $this->periodo2_dia_final_cobranca = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA] = true;
        }

        return $this;
    } // setPeriodo2DiaFinalCobranca()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[RegraFaturamentoTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[RegraFaturamentoTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[RegraFaturamentoTableMap::COL_USUARIO_ALTERADO] = true;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : RegraFaturamentoTableMap::translateFieldName('IdregraFaturamento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idregra_faturamento = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : RegraFaturamentoTableMap::translateFieldName('Descricao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : RegraFaturamentoTableMap::translateFieldName('Periodo1DiaVencimentoCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->periodo1_dia_vencimento_cobranca = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : RegraFaturamentoTableMap::translateFieldName('Periodo1DiaInicialCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->periodo1_dia_inicial_cobranca = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : RegraFaturamentoTableMap::translateFieldName('Periodo1DiaFinalCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->periodo1_dia_final_cobranca = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : RegraFaturamentoTableMap::translateFieldName('MesSubsequente', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mes_subsequente = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : RegraFaturamentoTableMap::translateFieldName('Periodo2DiaVencimentoCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->periodo2_dia_vencimento_cobranca = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : RegraFaturamentoTableMap::translateFieldName('Periodo2DiaInicialCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->periodo2_dia_inicial_cobranca = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : RegraFaturamentoTableMap::translateFieldName('Periodo2DiaFinalCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->periodo2_dia_final_cobranca = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : RegraFaturamentoTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : RegraFaturamentoTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : RegraFaturamentoTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = RegraFaturamentoTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\RegraFaturamento'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RegraFaturamentoTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildRegraFaturamentoQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collContratos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see RegraFaturamento::setDeleted()
     * @see RegraFaturamento::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(RegraFaturamentoTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildRegraFaturamentoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(RegraFaturamentoTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                RegraFaturamentoTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->contratosScheduledForDeletion !== null) {
                if (!$this->contratosScheduledForDeletion->isEmpty()) {
                    foreach ($this->contratosScheduledForDeletion as $contrato) {
                        // need to save related object because we set the relation to null
                        $contrato->save($con);
                    }
                    $this->contratosScheduledForDeletion = null;
                }
            }

            if ($this->collContratos !== null) {
                foreach ($this->collContratos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO] = true;
        if (null !== $this->idregra_faturamento) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'idregra_faturamento';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_DESCRICAO)) {
            $modifiedColumns[':p' . $index++]  = 'descricao';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'periodo1_dia_vencimento_cobranca';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'periodo1_dia_inicial_cobranca';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'periodo1_dia_final_cobranca';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_MES_SUBSEQUENTE)) {
            $modifiedColumns[':p' . $index++]  = 'mes_subsequente';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'periodo2_dia_vencimento_cobranca';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'periodo2_dia_inicial_cobranca';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'periodo2_dia_final_cobranca';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }

        $sql = sprintf(
            'INSERT INTO regra_faturamento (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idregra_faturamento':
                        $stmt->bindValue($identifier, $this->idregra_faturamento, PDO::PARAM_INT);
                        break;
                    case 'descricao':
                        $stmt->bindValue($identifier, $this->descricao, PDO::PARAM_STR);
                        break;
                    case 'periodo1_dia_vencimento_cobranca':
                        $stmt->bindValue($identifier, $this->periodo1_dia_vencimento_cobranca, PDO::PARAM_INT);
                        break;
                    case 'periodo1_dia_inicial_cobranca':
                        $stmt->bindValue($identifier, $this->periodo1_dia_inicial_cobranca, PDO::PARAM_INT);
                        break;
                    case 'periodo1_dia_final_cobranca':
                        $stmt->bindValue($identifier, $this->periodo1_dia_final_cobranca, PDO::PARAM_INT);
                        break;
                    case 'mes_subsequente':
                        $stmt->bindValue($identifier, $this->mes_subsequente, PDO::PARAM_STR);
                        break;
                    case 'periodo2_dia_vencimento_cobranca':
                        $stmt->bindValue($identifier, $this->periodo2_dia_vencimento_cobranca, PDO::PARAM_INT);
                        break;
                    case 'periodo2_dia_inicial_cobranca':
                        $stmt->bindValue($identifier, $this->periodo2_dia_inicial_cobranca, PDO::PARAM_INT);
                        break;
                    case 'periodo2_dia_final_cobranca':
                        $stmt->bindValue($identifier, $this->periodo2_dia_final_cobranca, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdregraFaturamento($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = RegraFaturamentoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdregraFaturamento();
                break;
            case 1:
                return $this->getDescricao();
                break;
            case 2:
                return $this->getPeriodo1DiaVencimentoCobranca();
                break;
            case 3:
                return $this->getPeriodo1DiaInicialCobranca();
                break;
            case 4:
                return $this->getPeriodo1DiaFinalCobranca();
                break;
            case 5:
                return $this->getMesSubsequente();
                break;
            case 6:
                return $this->getPeriodo2DiaVencimentoCobranca();
                break;
            case 7:
                return $this->getPeriodo2DiaInicialCobranca();
                break;
            case 8:
                return $this->getPeriodo2DiaFinalCobranca();
                break;
            case 9:
                return $this->getDataCadastro();
                break;
            case 10:
                return $this->getDataAlterado();
                break;
            case 11:
                return $this->getUsuarioAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['RegraFaturamento'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['RegraFaturamento'][$this->hashCode()] = true;
        $keys = RegraFaturamentoTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdregraFaturamento(),
            $keys[1] => $this->getDescricao(),
            $keys[2] => $this->getPeriodo1DiaVencimentoCobranca(),
            $keys[3] => $this->getPeriodo1DiaInicialCobranca(),
            $keys[4] => $this->getPeriodo1DiaFinalCobranca(),
            $keys[5] => $this->getMesSubsequente(),
            $keys[6] => $this->getPeriodo2DiaVencimentoCobranca(),
            $keys[7] => $this->getPeriodo2DiaInicialCobranca(),
            $keys[8] => $this->getPeriodo2DiaFinalCobranca(),
            $keys[9] => $this->getDataCadastro(),
            $keys[10] => $this->getDataAlterado(),
            $keys[11] => $this->getUsuarioAlterado(),
        );
        if ($result[$keys[9]] instanceof \DateTime) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTime) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collContratos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contratos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contratos';
                        break;
                    default:
                        $key = 'Contratos';
                }

                $result[$key] = $this->collContratos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = RegraFaturamentoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdregraFaturamento($value);
                break;
            case 1:
                $this->setDescricao($value);
                break;
            case 2:
                $this->setPeriodo1DiaVencimentoCobranca($value);
                break;
            case 3:
                $this->setPeriodo1DiaInicialCobranca($value);
                break;
            case 4:
                $this->setPeriodo1DiaFinalCobranca($value);
                break;
            case 5:
                $this->setMesSubsequente($value);
                break;
            case 6:
                $this->setPeriodo2DiaVencimentoCobranca($value);
                break;
            case 7:
                $this->setPeriodo2DiaInicialCobranca($value);
                break;
            case 8:
                $this->setPeriodo2DiaFinalCobranca($value);
                break;
            case 9:
                $this->setDataCadastro($value);
                break;
            case 10:
                $this->setDataAlterado($value);
                break;
            case 11:
                $this->setUsuarioAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = RegraFaturamentoTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdregraFaturamento($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDescricao($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPeriodo1DiaVencimentoCobranca($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPeriodo1DiaInicialCobranca($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPeriodo1DiaFinalCobranca($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setMesSubsequente($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setPeriodo2DiaVencimentoCobranca($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setPeriodo2DiaInicialCobranca($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPeriodo2DiaFinalCobranca($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDataCadastro($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDataAlterado($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(RegraFaturamentoTableMap::DATABASE_NAME);

        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO)) {
            $criteria->add(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, $this->idregra_faturamento);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_DESCRICAO)) {
            $criteria->add(RegraFaturamentoTableMap::COL_DESCRICAO, $this->descricao);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA)) {
            $criteria->add(RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA, $this->periodo1_dia_vencimento_cobranca);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA)) {
            $criteria->add(RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA, $this->periodo1_dia_inicial_cobranca);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA)) {
            $criteria->add(RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA, $this->periodo1_dia_final_cobranca);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_MES_SUBSEQUENTE)) {
            $criteria->add(RegraFaturamentoTableMap::COL_MES_SUBSEQUENTE, $this->mes_subsequente);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA)) {
            $criteria->add(RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA, $this->periodo2_dia_vencimento_cobranca);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA)) {
            $criteria->add(RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA, $this->periodo2_dia_inicial_cobranca);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA)) {
            $criteria->add(RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA, $this->periodo2_dia_final_cobranca);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(RegraFaturamentoTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(RegraFaturamentoTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(RegraFaturamentoTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(RegraFaturamentoTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildRegraFaturamentoQuery::create();
        $criteria->add(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, $this->idregra_faturamento);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdregraFaturamento();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdregraFaturamento();
    }

    /**
     * Generic method to set the primary key (idregra_faturamento column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdregraFaturamento($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdregraFaturamento();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\RegraFaturamento (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDescricao($this->getDescricao());
        $copyObj->setPeriodo1DiaVencimentoCobranca($this->getPeriodo1DiaVencimentoCobranca());
        $copyObj->setPeriodo1DiaInicialCobranca($this->getPeriodo1DiaInicialCobranca());
        $copyObj->setPeriodo1DiaFinalCobranca($this->getPeriodo1DiaFinalCobranca());
        $copyObj->setMesSubsequente($this->getMesSubsequente());
        $copyObj->setPeriodo2DiaVencimentoCobranca($this->getPeriodo2DiaVencimentoCobranca());
        $copyObj->setPeriodo2DiaInicialCobranca($this->getPeriodo2DiaInicialCobranca());
        $copyObj->setPeriodo2DiaFinalCobranca($this->getPeriodo2DiaFinalCobranca());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getContratos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContrato($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdregraFaturamento(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\RegraFaturamento Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Contrato' == $relationName) {
            return $this->initContratos();
        }
    }

    /**
     * Clears out the collContratos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContratos()
     */
    public function clearContratos()
    {
        $this->collContratos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContratos collection loaded partially.
     */
    public function resetPartialContratos($v = true)
    {
        $this->collContratosPartial = $v;
    }

    /**
     * Initializes the collContratos collection.
     *
     * By default this just sets the collContratos collection to an empty array (like clearcollContratos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContratos($overrideExisting = true)
    {
        if (null !== $this->collContratos && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContratoTableMap::getTableMap()->getCollectionClassName();

        $this->collContratos = new $collectionClassName;
        $this->collContratos->setModel('\ImaTelecomBundle\Model\Contrato');
    }

    /**
     * Gets an array of ChildContrato objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildRegraFaturamento is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContrato[] List of ChildContrato objects
     * @throws PropelException
     */
    public function getContratos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContratosPartial && !$this->isNew();
        if (null === $this->collContratos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContratos) {
                // return empty collection
                $this->initContratos();
            } else {
                $collContratos = ChildContratoQuery::create(null, $criteria)
                    ->filterByRegraFaturamento($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContratosPartial && count($collContratos)) {
                        $this->initContratos(false);

                        foreach ($collContratos as $obj) {
                            if (false == $this->collContratos->contains($obj)) {
                                $this->collContratos->append($obj);
                            }
                        }

                        $this->collContratosPartial = true;
                    }

                    return $collContratos;
                }

                if ($partial && $this->collContratos) {
                    foreach ($this->collContratos as $obj) {
                        if ($obj->isNew()) {
                            $collContratos[] = $obj;
                        }
                    }
                }

                $this->collContratos = $collContratos;
                $this->collContratosPartial = false;
            }
        }

        return $this->collContratos;
    }

    /**
     * Sets a collection of ChildContrato objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contratos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildRegraFaturamento The current object (for fluent API support)
     */
    public function setContratos(Collection $contratos, ConnectionInterface $con = null)
    {
        /** @var ChildContrato[] $contratosToDelete */
        $contratosToDelete = $this->getContratos(new Criteria(), $con)->diff($contratos);


        $this->contratosScheduledForDeletion = $contratosToDelete;

        foreach ($contratosToDelete as $contratoRemoved) {
            $contratoRemoved->setRegraFaturamento(null);
        }

        $this->collContratos = null;
        foreach ($contratos as $contrato) {
            $this->addContrato($contrato);
        }

        $this->collContratos = $contratos;
        $this->collContratosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Contrato objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Contrato objects.
     * @throws PropelException
     */
    public function countContratos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContratosPartial && !$this->isNew();
        if (null === $this->collContratos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContratos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContratos());
            }

            $query = ChildContratoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByRegraFaturamento($this)
                ->count($con);
        }

        return count($this->collContratos);
    }

    /**
     * Method called to associate a ChildContrato object to this object
     * through the ChildContrato foreign key attribute.
     *
     * @param  ChildContrato $l ChildContrato
     * @return $this|\ImaTelecomBundle\Model\RegraFaturamento The current object (for fluent API support)
     */
    public function addContrato(ChildContrato $l)
    {
        if ($this->collContratos === null) {
            $this->initContratos();
            $this->collContratosPartial = true;
        }

        if (!$this->collContratos->contains($l)) {
            $this->doAddContrato($l);

            if ($this->contratosScheduledForDeletion and $this->contratosScheduledForDeletion->contains($l)) {
                $this->contratosScheduledForDeletion->remove($this->contratosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContrato $contrato The ChildContrato object to add.
     */
    protected function doAddContrato(ChildContrato $contrato)
    {
        $this->collContratos[]= $contrato;
        $contrato->setRegraFaturamento($this);
    }

    /**
     * @param  ChildContrato $contrato The ChildContrato object to remove.
     * @return $this|ChildRegraFaturamento The current object (for fluent API support)
     */
    public function removeContrato(ChildContrato $contrato)
    {
        if ($this->getContratos()->contains($contrato)) {
            $pos = $this->collContratos->search($contrato);
            $this->collContratos->remove($pos);
            if (null === $this->contratosScheduledForDeletion) {
                $this->contratosScheduledForDeletion = clone $this->collContratos;
                $this->contratosScheduledForDeletion->clear();
            }
            $this->contratosScheduledForDeletion[]= $contrato;
            $contrato->setRegraFaturamento(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this RegraFaturamento is new, it will return
     * an empty collection; or if this RegraFaturamento has previously
     * been saved, it will retrieve related Contratos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in RegraFaturamento.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContrato[] List of ChildContrato objects
     */
    public function getContratosJoinEnderecoCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContratoQuery::create(null, $criteria);
        $query->joinWith('EnderecoCliente', $joinBehavior);

        return $this->getContratos($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->idregra_faturamento = null;
        $this->descricao = null;
        $this->periodo1_dia_vencimento_cobranca = null;
        $this->periodo1_dia_inicial_cobranca = null;
        $this->periodo1_dia_final_cobranca = null;
        $this->mes_subsequente = null;
        $this->periodo2_dia_vencimento_cobranca = null;
        $this->periodo2_dia_inicial_cobranca = null;
        $this->periodo2_dia_final_cobranca = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collContratos) {
                foreach ($this->collContratos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collContratos = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(RegraFaturamentoTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
