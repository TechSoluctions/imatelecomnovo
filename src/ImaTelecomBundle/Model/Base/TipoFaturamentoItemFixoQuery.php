<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\TipoFaturamentoItemFixo as ChildTipoFaturamentoItemFixo;
use ImaTelecomBundle\Model\TipoFaturamentoItemFixoQuery as ChildTipoFaturamentoItemFixoQuery;
use ImaTelecomBundle\Model\Map\TipoFaturamentoItemFixoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tipo_faturamento_item_fixo' table.
 *
 *
 *
 * @method     ChildTipoFaturamentoItemFixoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildTipoFaturamentoItemFixoQuery orderByTipoFaturamentoId($order = Criteria::ASC) Order by the tipo_faturamento_id column
 * @method     ChildTipoFaturamentoItemFixoQuery orderByItemFixoId($order = Criteria::ASC) Order by the item_fixo_id column
 * @method     ChildTipoFaturamentoItemFixoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildTipoFaturamentoItemFixoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildTipoFaturamentoItemFixoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildTipoFaturamentoItemFixoQuery groupById() Group by the id column
 * @method     ChildTipoFaturamentoItemFixoQuery groupByTipoFaturamentoId() Group by the tipo_faturamento_id column
 * @method     ChildTipoFaturamentoItemFixoQuery groupByItemFixoId() Group by the item_fixo_id column
 * @method     ChildTipoFaturamentoItemFixoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildTipoFaturamentoItemFixoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildTipoFaturamentoItemFixoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildTipoFaturamentoItemFixoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTipoFaturamentoItemFixoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTipoFaturamentoItemFixoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTipoFaturamentoItemFixoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTipoFaturamentoItemFixoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTipoFaturamentoItemFixoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTipoFaturamentoItemFixo findOne(ConnectionInterface $con = null) Return the first ChildTipoFaturamentoItemFixo matching the query
 * @method     ChildTipoFaturamentoItemFixo findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTipoFaturamentoItemFixo matching the query, or a new ChildTipoFaturamentoItemFixo object populated from the query conditions when no match is found
 *
 * @method     ChildTipoFaturamentoItemFixo findOneById(int $id) Return the first ChildTipoFaturamentoItemFixo filtered by the id column
 * @method     ChildTipoFaturamentoItemFixo findOneByTipoFaturamentoId(int $tipo_faturamento_id) Return the first ChildTipoFaturamentoItemFixo filtered by the tipo_faturamento_id column
 * @method     ChildTipoFaturamentoItemFixo findOneByItemFixoId(int $item_fixo_id) Return the first ChildTipoFaturamentoItemFixo filtered by the item_fixo_id column
 * @method     ChildTipoFaturamentoItemFixo findOneByDataCadastro(string $data_cadastro) Return the first ChildTipoFaturamentoItemFixo filtered by the data_cadastro column
 * @method     ChildTipoFaturamentoItemFixo findOneByDataAlterado(string $data_alterado) Return the first ChildTipoFaturamentoItemFixo filtered by the data_alterado column
 * @method     ChildTipoFaturamentoItemFixo findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildTipoFaturamentoItemFixo filtered by the usuario_alterado column *

 * @method     ChildTipoFaturamentoItemFixo requirePk($key, ConnectionInterface $con = null) Return the ChildTipoFaturamentoItemFixo by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoFaturamentoItemFixo requireOne(ConnectionInterface $con = null) Return the first ChildTipoFaturamentoItemFixo matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTipoFaturamentoItemFixo requireOneById(int $id) Return the first ChildTipoFaturamentoItemFixo filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoFaturamentoItemFixo requireOneByTipoFaturamentoId(int $tipo_faturamento_id) Return the first ChildTipoFaturamentoItemFixo filtered by the tipo_faturamento_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoFaturamentoItemFixo requireOneByItemFixoId(int $item_fixo_id) Return the first ChildTipoFaturamentoItemFixo filtered by the item_fixo_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoFaturamentoItemFixo requireOneByDataCadastro(string $data_cadastro) Return the first ChildTipoFaturamentoItemFixo filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoFaturamentoItemFixo requireOneByDataAlterado(string $data_alterado) Return the first ChildTipoFaturamentoItemFixo filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoFaturamentoItemFixo requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildTipoFaturamentoItemFixo filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTipoFaturamentoItemFixo[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTipoFaturamentoItemFixo objects based on current ModelCriteria
 * @method     ChildTipoFaturamentoItemFixo[]|ObjectCollection findById(int $id) Return ChildTipoFaturamentoItemFixo objects filtered by the id column
 * @method     ChildTipoFaturamentoItemFixo[]|ObjectCollection findByTipoFaturamentoId(int $tipo_faturamento_id) Return ChildTipoFaturamentoItemFixo objects filtered by the tipo_faturamento_id column
 * @method     ChildTipoFaturamentoItemFixo[]|ObjectCollection findByItemFixoId(int $item_fixo_id) Return ChildTipoFaturamentoItemFixo objects filtered by the item_fixo_id column
 * @method     ChildTipoFaturamentoItemFixo[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildTipoFaturamentoItemFixo objects filtered by the data_cadastro column
 * @method     ChildTipoFaturamentoItemFixo[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildTipoFaturamentoItemFixo objects filtered by the data_alterado column
 * @method     ChildTipoFaturamentoItemFixo[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildTipoFaturamentoItemFixo objects filtered by the usuario_alterado column
 * @method     ChildTipoFaturamentoItemFixo[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TipoFaturamentoItemFixoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\TipoFaturamentoItemFixoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\TipoFaturamentoItemFixo', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTipoFaturamentoItemFixoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTipoFaturamentoItemFixoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTipoFaturamentoItemFixoQuery) {
            return $criteria;
        }
        $query = new ChildTipoFaturamentoItemFixoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTipoFaturamentoItemFixo|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TipoFaturamentoItemFixoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TipoFaturamentoItemFixoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTipoFaturamentoItemFixo A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, tipo_faturamento_id, item_fixo_id, data_cadastro, data_alterado, usuario_alterado FROM tipo_faturamento_item_fixo WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTipoFaturamentoItemFixo $obj */
            $obj = new ChildTipoFaturamentoItemFixo();
            $obj->hydrate($row);
            TipoFaturamentoItemFixoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTipoFaturamentoItemFixo|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTipoFaturamentoItemFixoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTipoFaturamentoItemFixoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoFaturamentoItemFixoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the tipo_faturamento_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoFaturamentoId(1234); // WHERE tipo_faturamento_id = 1234
     * $query->filterByTipoFaturamentoId(array(12, 34)); // WHERE tipo_faturamento_id IN (12, 34)
     * $query->filterByTipoFaturamentoId(array('min' => 12)); // WHERE tipo_faturamento_id > 12
     * </code>
     *
     * @param     mixed $tipoFaturamentoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoFaturamentoItemFixoQuery The current query, for fluid interface
     */
    public function filterByTipoFaturamentoId($tipoFaturamentoId = null, $comparison = null)
    {
        if (is_array($tipoFaturamentoId)) {
            $useMinMax = false;
            if (isset($tipoFaturamentoId['min'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_TIPO_FATURAMENTO_ID, $tipoFaturamentoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tipoFaturamentoId['max'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_TIPO_FATURAMENTO_ID, $tipoFaturamentoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_TIPO_FATURAMENTO_ID, $tipoFaturamentoId, $comparison);
    }

    /**
     * Filter the query on the item_fixo_id column
     *
     * Example usage:
     * <code>
     * $query->filterByItemFixoId(1234); // WHERE item_fixo_id = 1234
     * $query->filterByItemFixoId(array(12, 34)); // WHERE item_fixo_id IN (12, 34)
     * $query->filterByItemFixoId(array('min' => 12)); // WHERE item_fixo_id > 12
     * </code>
     *
     * @param     mixed $itemFixoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoFaturamentoItemFixoQuery The current query, for fluid interface
     */
    public function filterByItemFixoId($itemFixoId = null, $comparison = null)
    {
        if (is_array($itemFixoId)) {
            $useMinMax = false;
            if (isset($itemFixoId['min'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_ITEM_FIXO_ID, $itemFixoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($itemFixoId['max'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_ITEM_FIXO_ID, $itemFixoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_ITEM_FIXO_ID, $itemFixoId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoFaturamentoItemFixoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoFaturamentoItemFixoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoFaturamentoItemFixoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTipoFaturamentoItemFixo $tipoFaturamentoItemFixo Object to remove from the list of results
     *
     * @return $this|ChildTipoFaturamentoItemFixoQuery The current query, for fluid interface
     */
    public function prune($tipoFaturamentoItemFixo = null)
    {
        if ($tipoFaturamentoItemFixo) {
            $this->addUsingAlias(TipoFaturamentoItemFixoTableMap::COL_ID, $tipoFaturamentoItemFixo->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tipo_faturamento_item_fixo table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoFaturamentoItemFixoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TipoFaturamentoItemFixoTableMap::clearInstancePool();
            TipoFaturamentoItemFixoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoFaturamentoItemFixoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TipoFaturamentoItemFixoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TipoFaturamentoItemFixoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TipoFaturamentoItemFixoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TipoFaturamentoItemFixoQuery
