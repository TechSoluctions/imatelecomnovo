<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\CronTask as ChildCronTask;
use ImaTelecomBundle\Model\CronTaskQuery as ChildCronTaskQuery;
use ImaTelecomBundle\Model\Map\CronTaskTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'cron_task' table.
 *
 *
 *
 * @method     ChildCronTaskQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCronTaskQuery orderByNome($order = Criteria::ASC) Order by the nome column
 * @method     ChildCronTaskQuery orderByDescricao($order = Criteria::ASC) Order by the descricao column
 * @method     ChildCronTaskQuery orderByMinuto($order = Criteria::ASC) Order by the minuto column
 * @method     ChildCronTaskQuery orderByHora($order = Criteria::ASC) Order by the hora column
 * @method     ChildCronTaskQuery orderByDia($order = Criteria::ASC) Order by the dia column
 * @method     ChildCronTaskQuery orderByMes($order = Criteria::ASC) Order by the mes column
 * @method     ChildCronTaskQuery orderByDiaSemana($order = Criteria::ASC) Order by the dia_semana column
 * @method     ChildCronTaskQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method     ChildCronTaskQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildCronTaskQuery orderByPrioridade($order = Criteria::ASC) Order by the prioridade column
 * @method     ChildCronTaskQuery orderByPrimeiraExecucao($order = Criteria::ASC) Order by the primeira_execucao column
 * @method     ChildCronTaskQuery orderByUltimaExecucao($order = Criteria::ASC) Order by the ultima_execucao column
 * @method     ChildCronTaskQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildCronTaskQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildCronTaskQuery orderByCronTaskGrupoId($order = Criteria::ASC) Order by the cron_task_grupo_id column
 * @method     ChildCronTaskQuery orderBySegundo($order = Criteria::ASC) Order by the segundo column
 * @method     ChildCronTaskQuery orderByAno($order = Criteria::ASC) Order by the ano column
 * @method     ChildCronTaskQuery orderByTipoExecucaoArquivo($order = Criteria::ASC) Order by the tipo_execucao_arquivo column
 * @method     ChildCronTaskQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildCronTaskQuery groupById() Group by the id column
 * @method     ChildCronTaskQuery groupByNome() Group by the nome column
 * @method     ChildCronTaskQuery groupByDescricao() Group by the descricao column
 * @method     ChildCronTaskQuery groupByMinuto() Group by the minuto column
 * @method     ChildCronTaskQuery groupByHora() Group by the hora column
 * @method     ChildCronTaskQuery groupByDia() Group by the dia column
 * @method     ChildCronTaskQuery groupByMes() Group by the mes column
 * @method     ChildCronTaskQuery groupByDiaSemana() Group by the dia_semana column
 * @method     ChildCronTaskQuery groupByTipo() Group by the tipo column
 * @method     ChildCronTaskQuery groupByAtivo() Group by the ativo column
 * @method     ChildCronTaskQuery groupByPrioridade() Group by the prioridade column
 * @method     ChildCronTaskQuery groupByPrimeiraExecucao() Group by the primeira_execucao column
 * @method     ChildCronTaskQuery groupByUltimaExecucao() Group by the ultima_execucao column
 * @method     ChildCronTaskQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildCronTaskQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildCronTaskQuery groupByCronTaskGrupoId() Group by the cron_task_grupo_id column
 * @method     ChildCronTaskQuery groupBySegundo() Group by the segundo column
 * @method     ChildCronTaskQuery groupByAno() Group by the ano column
 * @method     ChildCronTaskQuery groupByTipoExecucaoArquivo() Group by the tipo_execucao_arquivo column
 * @method     ChildCronTaskQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildCronTaskQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCronTaskQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCronTaskQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCronTaskQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCronTaskQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCronTaskQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCronTaskQuery leftJoinCronTaskGrupo($relationAlias = null) Adds a LEFT JOIN clause to the query using the CronTaskGrupo relation
 * @method     ChildCronTaskQuery rightJoinCronTaskGrupo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CronTaskGrupo relation
 * @method     ChildCronTaskQuery innerJoinCronTaskGrupo($relationAlias = null) Adds a INNER JOIN clause to the query using the CronTaskGrupo relation
 *
 * @method     ChildCronTaskQuery joinWithCronTaskGrupo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CronTaskGrupo relation
 *
 * @method     ChildCronTaskQuery leftJoinWithCronTaskGrupo() Adds a LEFT JOIN clause and with to the query using the CronTaskGrupo relation
 * @method     ChildCronTaskQuery rightJoinWithCronTaskGrupo() Adds a RIGHT JOIN clause and with to the query using the CronTaskGrupo relation
 * @method     ChildCronTaskQuery innerJoinWithCronTaskGrupo() Adds a INNER JOIN clause and with to the query using the CronTaskGrupo relation
 *
 * @method     ChildCronTaskQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildCronTaskQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildCronTaskQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildCronTaskQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildCronTaskQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildCronTaskQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildCronTaskQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildCronTaskQuery leftJoinCronTaskArquivos($relationAlias = null) Adds a LEFT JOIN clause to the query using the CronTaskArquivos relation
 * @method     ChildCronTaskQuery rightJoinCronTaskArquivos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CronTaskArquivos relation
 * @method     ChildCronTaskQuery innerJoinCronTaskArquivos($relationAlias = null) Adds a INNER JOIN clause to the query using the CronTaskArquivos relation
 *
 * @method     ChildCronTaskQuery joinWithCronTaskArquivos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CronTaskArquivos relation
 *
 * @method     ChildCronTaskQuery leftJoinWithCronTaskArquivos() Adds a LEFT JOIN clause and with to the query using the CronTaskArquivos relation
 * @method     ChildCronTaskQuery rightJoinWithCronTaskArquivos() Adds a RIGHT JOIN clause and with to the query using the CronTaskArquivos relation
 * @method     ChildCronTaskQuery innerJoinWithCronTaskArquivos() Adds a INNER JOIN clause and with to the query using the CronTaskArquivos relation
 *
 * @method     ChildCronTaskQuery leftJoinCronTaskLogExecucao($relationAlias = null) Adds a LEFT JOIN clause to the query using the CronTaskLogExecucao relation
 * @method     ChildCronTaskQuery rightJoinCronTaskLogExecucao($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CronTaskLogExecucao relation
 * @method     ChildCronTaskQuery innerJoinCronTaskLogExecucao($relationAlias = null) Adds a INNER JOIN clause to the query using the CronTaskLogExecucao relation
 *
 * @method     ChildCronTaskQuery joinWithCronTaskLogExecucao($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CronTaskLogExecucao relation
 *
 * @method     ChildCronTaskQuery leftJoinWithCronTaskLogExecucao() Adds a LEFT JOIN clause and with to the query using the CronTaskLogExecucao relation
 * @method     ChildCronTaskQuery rightJoinWithCronTaskLogExecucao() Adds a RIGHT JOIN clause and with to the query using the CronTaskLogExecucao relation
 * @method     ChildCronTaskQuery innerJoinWithCronTaskLogExecucao() Adds a INNER JOIN clause and with to the query using the CronTaskLogExecucao relation
 *
 * @method     \ImaTelecomBundle\Model\CronTaskGrupoQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\CronTaskArquivosQuery|\ImaTelecomBundle\Model\CronTaskLogExecucaoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCronTask findOne(ConnectionInterface $con = null) Return the first ChildCronTask matching the query
 * @method     ChildCronTask findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCronTask matching the query, or a new ChildCronTask object populated from the query conditions when no match is found
 *
 * @method     ChildCronTask findOneById(int $id) Return the first ChildCronTask filtered by the id column
 * @method     ChildCronTask findOneByNome(string $nome) Return the first ChildCronTask filtered by the nome column
 * @method     ChildCronTask findOneByDescricao(string $descricao) Return the first ChildCronTask filtered by the descricao column
 * @method     ChildCronTask findOneByMinuto(string $minuto) Return the first ChildCronTask filtered by the minuto column
 * @method     ChildCronTask findOneByHora(string $hora) Return the first ChildCronTask filtered by the hora column
 * @method     ChildCronTask findOneByDia(string $dia) Return the first ChildCronTask filtered by the dia column
 * @method     ChildCronTask findOneByMes(string $mes) Return the first ChildCronTask filtered by the mes column
 * @method     ChildCronTask findOneByDiaSemana(string $dia_semana) Return the first ChildCronTask filtered by the dia_semana column
 * @method     ChildCronTask findOneByTipo(string $tipo) Return the first ChildCronTask filtered by the tipo column
 * @method     ChildCronTask findOneByAtivo(boolean $ativo) Return the first ChildCronTask filtered by the ativo column
 * @method     ChildCronTask findOneByPrioridade(string $prioridade) Return the first ChildCronTask filtered by the prioridade column
 * @method     ChildCronTask findOneByPrimeiraExecucao(string $primeira_execucao) Return the first ChildCronTask filtered by the primeira_execucao column
 * @method     ChildCronTask findOneByUltimaExecucao(string $ultima_execucao) Return the first ChildCronTask filtered by the ultima_execucao column
 * @method     ChildCronTask findOneByDataCadastro(string $data_cadastro) Return the first ChildCronTask filtered by the data_cadastro column
 * @method     ChildCronTask findOneByDataAlterado(string $data_alterado) Return the first ChildCronTask filtered by the data_alterado column
 * @method     ChildCronTask findOneByCronTaskGrupoId(int $cron_task_grupo_id) Return the first ChildCronTask filtered by the cron_task_grupo_id column
 * @method     ChildCronTask findOneBySegundo(string $segundo) Return the first ChildCronTask filtered by the segundo column
 * @method     ChildCronTask findOneByAno(string $ano) Return the first ChildCronTask filtered by the ano column
 * @method     ChildCronTask findOneByTipoExecucaoArquivo(string $tipo_execucao_arquivo) Return the first ChildCronTask filtered by the tipo_execucao_arquivo column
 * @method     ChildCronTask findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildCronTask filtered by the usuario_alterado column *

 * @method     ChildCronTask requirePk($key, ConnectionInterface $con = null) Return the ChildCronTask by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOne(ConnectionInterface $con = null) Return the first ChildCronTask matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCronTask requireOneById(int $id) Return the first ChildCronTask filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByNome(string $nome) Return the first ChildCronTask filtered by the nome column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByDescricao(string $descricao) Return the first ChildCronTask filtered by the descricao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByMinuto(string $minuto) Return the first ChildCronTask filtered by the minuto column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByHora(string $hora) Return the first ChildCronTask filtered by the hora column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByDia(string $dia) Return the first ChildCronTask filtered by the dia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByMes(string $mes) Return the first ChildCronTask filtered by the mes column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByDiaSemana(string $dia_semana) Return the first ChildCronTask filtered by the dia_semana column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByTipo(string $tipo) Return the first ChildCronTask filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByAtivo(boolean $ativo) Return the first ChildCronTask filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByPrioridade(string $prioridade) Return the first ChildCronTask filtered by the prioridade column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByPrimeiraExecucao(string $primeira_execucao) Return the first ChildCronTask filtered by the primeira_execucao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByUltimaExecucao(string $ultima_execucao) Return the first ChildCronTask filtered by the ultima_execucao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByDataCadastro(string $data_cadastro) Return the first ChildCronTask filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByDataAlterado(string $data_alterado) Return the first ChildCronTask filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByCronTaskGrupoId(int $cron_task_grupo_id) Return the first ChildCronTask filtered by the cron_task_grupo_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneBySegundo(string $segundo) Return the first ChildCronTask filtered by the segundo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByAno(string $ano) Return the first ChildCronTask filtered by the ano column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByTipoExecucaoArquivo(string $tipo_execucao_arquivo) Return the first ChildCronTask filtered by the tipo_execucao_arquivo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCronTask requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildCronTask filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCronTask[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCronTask objects based on current ModelCriteria
 * @method     ChildCronTask[]|ObjectCollection findById(int $id) Return ChildCronTask objects filtered by the id column
 * @method     ChildCronTask[]|ObjectCollection findByNome(string $nome) Return ChildCronTask objects filtered by the nome column
 * @method     ChildCronTask[]|ObjectCollection findByDescricao(string $descricao) Return ChildCronTask objects filtered by the descricao column
 * @method     ChildCronTask[]|ObjectCollection findByMinuto(string $minuto) Return ChildCronTask objects filtered by the minuto column
 * @method     ChildCronTask[]|ObjectCollection findByHora(string $hora) Return ChildCronTask objects filtered by the hora column
 * @method     ChildCronTask[]|ObjectCollection findByDia(string $dia) Return ChildCronTask objects filtered by the dia column
 * @method     ChildCronTask[]|ObjectCollection findByMes(string $mes) Return ChildCronTask objects filtered by the mes column
 * @method     ChildCronTask[]|ObjectCollection findByDiaSemana(string $dia_semana) Return ChildCronTask objects filtered by the dia_semana column
 * @method     ChildCronTask[]|ObjectCollection findByTipo(string $tipo) Return ChildCronTask objects filtered by the tipo column
 * @method     ChildCronTask[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildCronTask objects filtered by the ativo column
 * @method     ChildCronTask[]|ObjectCollection findByPrioridade(string $prioridade) Return ChildCronTask objects filtered by the prioridade column
 * @method     ChildCronTask[]|ObjectCollection findByPrimeiraExecucao(string $primeira_execucao) Return ChildCronTask objects filtered by the primeira_execucao column
 * @method     ChildCronTask[]|ObjectCollection findByUltimaExecucao(string $ultima_execucao) Return ChildCronTask objects filtered by the ultima_execucao column
 * @method     ChildCronTask[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildCronTask objects filtered by the data_cadastro column
 * @method     ChildCronTask[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildCronTask objects filtered by the data_alterado column
 * @method     ChildCronTask[]|ObjectCollection findByCronTaskGrupoId(int $cron_task_grupo_id) Return ChildCronTask objects filtered by the cron_task_grupo_id column
 * @method     ChildCronTask[]|ObjectCollection findBySegundo(string $segundo) Return ChildCronTask objects filtered by the segundo column
 * @method     ChildCronTask[]|ObjectCollection findByAno(string $ano) Return ChildCronTask objects filtered by the ano column
 * @method     ChildCronTask[]|ObjectCollection findByTipoExecucaoArquivo(string $tipo_execucao_arquivo) Return ChildCronTask objects filtered by the tipo_execucao_arquivo column
 * @method     ChildCronTask[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildCronTask objects filtered by the usuario_alterado column
 * @method     ChildCronTask[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CronTaskQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\CronTaskQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\CronTask', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCronTaskQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCronTaskQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCronTaskQuery) {
            return $criteria;
        }
        $query = new ChildCronTaskQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCronTask|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CronTaskTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CronTaskTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCronTask A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, nome, descricao, minuto, hora, dia, mes, dia_semana, tipo, ativo, prioridade, primeira_execucao, ultima_execucao, data_cadastro, data_alterado, cron_task_grupo_id, segundo, ano, tipo_execucao_arquivo, usuario_alterado FROM cron_task WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCronTask $obj */
            $obj = new ChildCronTask();
            $obj->hydrate($row);
            CronTaskTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCronTask|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CronTaskTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CronTaskTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the nome column
     *
     * Example usage:
     * <code>
     * $query->filterByNome('fooValue');   // WHERE nome = 'fooValue'
     * $query->filterByNome('%fooValue%', Criteria::LIKE); // WHERE nome LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nome The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByNome($nome = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nome)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_NOME, $nome, $comparison);
    }

    /**
     * Filter the query on the descricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricao('fooValue');   // WHERE descricao = 'fooValue'
     * $query->filterByDescricao('%fooValue%', Criteria::LIKE); // WHERE descricao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByDescricao($descricao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_DESCRICAO, $descricao, $comparison);
    }

    /**
     * Filter the query on the minuto column
     *
     * Example usage:
     * <code>
     * $query->filterByMinuto('fooValue');   // WHERE minuto = 'fooValue'
     * $query->filterByMinuto('%fooValue%', Criteria::LIKE); // WHERE minuto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $minuto The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByMinuto($minuto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($minuto)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_MINUTO, $minuto, $comparison);
    }

    /**
     * Filter the query on the hora column
     *
     * Example usage:
     * <code>
     * $query->filterByHora('fooValue');   // WHERE hora = 'fooValue'
     * $query->filterByHora('%fooValue%', Criteria::LIKE); // WHERE hora LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hora The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByHora($hora = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hora)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_HORA, $hora, $comparison);
    }

    /**
     * Filter the query on the dia column
     *
     * Example usage:
     * <code>
     * $query->filterByDia('fooValue');   // WHERE dia = 'fooValue'
     * $query->filterByDia('%fooValue%', Criteria::LIKE); // WHERE dia LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dia The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByDia($dia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dia)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_DIA, $dia, $comparison);
    }

    /**
     * Filter the query on the mes column
     *
     * Example usage:
     * <code>
     * $query->filterByMes('fooValue');   // WHERE mes = 'fooValue'
     * $query->filterByMes('%fooValue%', Criteria::LIKE); // WHERE mes LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mes The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByMes($mes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mes)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_MES, $mes, $comparison);
    }

    /**
     * Filter the query on the dia_semana column
     *
     * Example usage:
     * <code>
     * $query->filterByDiaSemana('fooValue');   // WHERE dia_semana = 'fooValue'
     * $query->filterByDiaSemana('%fooValue%', Criteria::LIKE); // WHERE dia_semana LIKE '%fooValue%'
     * </code>
     *
     * @param     string $diaSemana The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByDiaSemana($diaSemana = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($diaSemana)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_DIA_SEMANA, $diaSemana, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the prioridade column
     *
     * Example usage:
     * <code>
     * $query->filterByPrioridade('fooValue');   // WHERE prioridade = 'fooValue'
     * $query->filterByPrioridade('%fooValue%', Criteria::LIKE); // WHERE prioridade LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prioridade The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByPrioridade($prioridade = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prioridade)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_PRIORIDADE, $prioridade, $comparison);
    }

    /**
     * Filter the query on the primeira_execucao column
     *
     * Example usage:
     * <code>
     * $query->filterByPrimeiraExecucao('2011-03-14'); // WHERE primeira_execucao = '2011-03-14'
     * $query->filterByPrimeiraExecucao('now'); // WHERE primeira_execucao = '2011-03-14'
     * $query->filterByPrimeiraExecucao(array('max' => 'yesterday')); // WHERE primeira_execucao > '2011-03-13'
     * </code>
     *
     * @param     mixed $primeiraExecucao The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByPrimeiraExecucao($primeiraExecucao = null, $comparison = null)
    {
        if (is_array($primeiraExecucao)) {
            $useMinMax = false;
            if (isset($primeiraExecucao['min'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_PRIMEIRA_EXECUCAO, $primeiraExecucao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($primeiraExecucao['max'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_PRIMEIRA_EXECUCAO, $primeiraExecucao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_PRIMEIRA_EXECUCAO, $primeiraExecucao, $comparison);
    }

    /**
     * Filter the query on the ultima_execucao column
     *
     * Example usage:
     * <code>
     * $query->filterByUltimaExecucao('2011-03-14'); // WHERE ultima_execucao = '2011-03-14'
     * $query->filterByUltimaExecucao('now'); // WHERE ultima_execucao = '2011-03-14'
     * $query->filterByUltimaExecucao(array('max' => 'yesterday')); // WHERE ultima_execucao > '2011-03-13'
     * </code>
     *
     * @param     mixed $ultimaExecucao The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByUltimaExecucao($ultimaExecucao = null, $comparison = null)
    {
        if (is_array($ultimaExecucao)) {
            $useMinMax = false;
            if (isset($ultimaExecucao['min'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_ULTIMA_EXECUCAO, $ultimaExecucao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ultimaExecucao['max'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_ULTIMA_EXECUCAO, $ultimaExecucao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_ULTIMA_EXECUCAO, $ultimaExecucao, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the cron_task_grupo_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCronTaskGrupoId(1234); // WHERE cron_task_grupo_id = 1234
     * $query->filterByCronTaskGrupoId(array(12, 34)); // WHERE cron_task_grupo_id IN (12, 34)
     * $query->filterByCronTaskGrupoId(array('min' => 12)); // WHERE cron_task_grupo_id > 12
     * </code>
     *
     * @see       filterByCronTaskGrupo()
     *
     * @param     mixed $cronTaskGrupoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByCronTaskGrupoId($cronTaskGrupoId = null, $comparison = null)
    {
        if (is_array($cronTaskGrupoId)) {
            $useMinMax = false;
            if (isset($cronTaskGrupoId['min'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_CRON_TASK_GRUPO_ID, $cronTaskGrupoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cronTaskGrupoId['max'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_CRON_TASK_GRUPO_ID, $cronTaskGrupoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_CRON_TASK_GRUPO_ID, $cronTaskGrupoId, $comparison);
    }

    /**
     * Filter the query on the segundo column
     *
     * Example usage:
     * <code>
     * $query->filterBySegundo('fooValue');   // WHERE segundo = 'fooValue'
     * $query->filterBySegundo('%fooValue%', Criteria::LIKE); // WHERE segundo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $segundo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterBySegundo($segundo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($segundo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_SEGUNDO, $segundo, $comparison);
    }

    /**
     * Filter the query on the ano column
     *
     * Example usage:
     * <code>
     * $query->filterByAno('fooValue');   // WHERE ano = 'fooValue'
     * $query->filterByAno('%fooValue%', Criteria::LIKE); // WHERE ano LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ano The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByAno($ano = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ano)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_ANO, $ano, $comparison);
    }

    /**
     * Filter the query on the tipo_execucao_arquivo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoExecucaoArquivo('fooValue');   // WHERE tipo_execucao_arquivo = 'fooValue'
     * $query->filterByTipoExecucaoArquivo('%fooValue%', Criteria::LIKE); // WHERE tipo_execucao_arquivo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoExecucaoArquivo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByTipoExecucaoArquivo($tipoExecucaoArquivo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoExecucaoArquivo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_TIPO_EXECUCAO_ARQUIVO, $tipoExecucaoArquivo, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(CronTaskTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CronTaskTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\CronTaskGrupo object
     *
     * @param \ImaTelecomBundle\Model\CronTaskGrupo|ObjectCollection $cronTaskGrupo The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByCronTaskGrupo($cronTaskGrupo, $comparison = null)
    {
        if ($cronTaskGrupo instanceof \ImaTelecomBundle\Model\CronTaskGrupo) {
            return $this
                ->addUsingAlias(CronTaskTableMap::COL_CRON_TASK_GRUPO_ID, $cronTaskGrupo->getId(), $comparison);
        } elseif ($cronTaskGrupo instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CronTaskTableMap::COL_CRON_TASK_GRUPO_ID, $cronTaskGrupo->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCronTaskGrupo() only accepts arguments of type \ImaTelecomBundle\Model\CronTaskGrupo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CronTaskGrupo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function joinCronTaskGrupo($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CronTaskGrupo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CronTaskGrupo');
        }

        return $this;
    }

    /**
     * Use the CronTaskGrupo relation CronTaskGrupo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CronTaskGrupoQuery A secondary query class using the current class as primary query
     */
    public function useCronTaskGrupoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCronTaskGrupo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CronTaskGrupo', '\ImaTelecomBundle\Model\CronTaskGrupoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(CronTaskTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CronTaskTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\CronTaskArquivos object
     *
     * @param \ImaTelecomBundle\Model\CronTaskArquivos|ObjectCollection $cronTaskArquivos the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByCronTaskArquivos($cronTaskArquivos, $comparison = null)
    {
        if ($cronTaskArquivos instanceof \ImaTelecomBundle\Model\CronTaskArquivos) {
            return $this
                ->addUsingAlias(CronTaskTableMap::COL_ID, $cronTaskArquivos->getCronTaskId(), $comparison);
        } elseif ($cronTaskArquivos instanceof ObjectCollection) {
            return $this
                ->useCronTaskArquivosQuery()
                ->filterByPrimaryKeys($cronTaskArquivos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCronTaskArquivos() only accepts arguments of type \ImaTelecomBundle\Model\CronTaskArquivos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CronTaskArquivos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function joinCronTaskArquivos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CronTaskArquivos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CronTaskArquivos');
        }

        return $this;
    }

    /**
     * Use the CronTaskArquivos relation CronTaskArquivos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CronTaskArquivosQuery A secondary query class using the current class as primary query
     */
    public function useCronTaskArquivosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCronTaskArquivos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CronTaskArquivos', '\ImaTelecomBundle\Model\CronTaskArquivosQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\CronTaskLogExecucao object
     *
     * @param \ImaTelecomBundle\Model\CronTaskLogExecucao|ObjectCollection $cronTaskLogExecucao the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCronTaskQuery The current query, for fluid interface
     */
    public function filterByCronTaskLogExecucao($cronTaskLogExecucao, $comparison = null)
    {
        if ($cronTaskLogExecucao instanceof \ImaTelecomBundle\Model\CronTaskLogExecucao) {
            return $this
                ->addUsingAlias(CronTaskTableMap::COL_ID, $cronTaskLogExecucao->getCronTaskId(), $comparison);
        } elseif ($cronTaskLogExecucao instanceof ObjectCollection) {
            return $this
                ->useCronTaskLogExecucaoQuery()
                ->filterByPrimaryKeys($cronTaskLogExecucao->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCronTaskLogExecucao() only accepts arguments of type \ImaTelecomBundle\Model\CronTaskLogExecucao or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CronTaskLogExecucao relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function joinCronTaskLogExecucao($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CronTaskLogExecucao');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CronTaskLogExecucao');
        }

        return $this;
    }

    /**
     * Use the CronTaskLogExecucao relation CronTaskLogExecucao object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CronTaskLogExecucaoQuery A secondary query class using the current class as primary query
     */
    public function useCronTaskLogExecucaoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCronTaskLogExecucao($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CronTaskLogExecucao', '\ImaTelecomBundle\Model\CronTaskLogExecucaoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCronTask $cronTask Object to remove from the list of results
     *
     * @return $this|ChildCronTaskQuery The current query, for fluid interface
     */
    public function prune($cronTask = null)
    {
        if ($cronTask) {
            $this->addUsingAlias(CronTaskTableMap::COL_ID, $cronTask->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the cron_task table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CronTaskTableMap::clearInstancePool();
            CronTaskTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CronTaskTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CronTaskTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CronTaskTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CronTaskQuery
