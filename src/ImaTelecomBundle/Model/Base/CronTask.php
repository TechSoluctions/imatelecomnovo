<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\CronTask as ChildCronTask;
use ImaTelecomBundle\Model\CronTaskArquivos as ChildCronTaskArquivos;
use ImaTelecomBundle\Model\CronTaskArquivosQuery as ChildCronTaskArquivosQuery;
use ImaTelecomBundle\Model\CronTaskGrupo as ChildCronTaskGrupo;
use ImaTelecomBundle\Model\CronTaskGrupoQuery as ChildCronTaskGrupoQuery;
use ImaTelecomBundle\Model\CronTaskLogExecucao as ChildCronTaskLogExecucao;
use ImaTelecomBundle\Model\CronTaskLogExecucaoQuery as ChildCronTaskLogExecucaoQuery;
use ImaTelecomBundle\Model\CronTaskQuery as ChildCronTaskQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\CronTaskArquivosTableMap;
use ImaTelecomBundle\Model\Map\CronTaskLogExecucaoTableMap;
use ImaTelecomBundle\Model\Map\CronTaskTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'cron_task' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class CronTask implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\CronTaskTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the nome field.
     *
     * @var        string
     */
    protected $nome;

    /**
     * The value for the descricao field.
     *
     * @var        string
     */
    protected $descricao;

    /**
     * The value for the minuto field.
     *
     * @var        string
     */
    protected $minuto;

    /**
     * The value for the hora field.
     *
     * @var        string
     */
    protected $hora;

    /**
     * The value for the dia field.
     *
     * @var        string
     */
    protected $dia;

    /**
     * The value for the mes field.
     *
     * @var        string
     */
    protected $mes;

    /**
     * The value for the dia_semana field.
     *
     * @var        string
     */
    protected $dia_semana;

    /**
     * The value for the tipo field.
     *
     * Note: this column has a database default value of: 'nenhum'
     * @var        string
     */
    protected $tipo;

    /**
     * The value for the ativo field.
     *
     * @var        boolean
     */
    protected $ativo;

    /**
     * The value for the prioridade field.
     *
     * Note: this column has a database default value of: 'nenhuma'
     * @var        string
     */
    protected $prioridade;

    /**
     * The value for the primeira_execucao field.
     *
     * Note: this column has a database default value of: NULL
     * @var        DateTime
     */
    protected $primeira_execucao;

    /**
     * The value for the ultima_execucao field.
     *
     * Note: this column has a database default value of: NULL
     * @var        DateTime
     */
    protected $ultima_execucao;

    /**
     * The value for the data_cadastro field.
     *
     * Note: this column has a database default value of: NULL
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * Note: this column has a database default value of: NULL
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the cron_task_grupo_id field.
     *
     * @var        int
     */
    protected $cron_task_grupo_id;

    /**
     * The value for the segundo field.
     *
     * @var        string
     */
    protected $segundo;

    /**
     * The value for the ano field.
     *
     * @var        string
     */
    protected $ano;

    /**
     * The value for the tipo_execucao_arquivo field.
     *
     * Note: this column has a database default value of: 'unica'
     * @var        string
     */
    protected $tipo_execucao_arquivo;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * @var        ChildCronTaskGrupo
     */
    protected $aCronTaskGrupo;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildCronTaskArquivos[] Collection to store aggregation of ChildCronTaskArquivos objects.
     */
    protected $collCronTaskArquivoss;
    protected $collCronTaskArquivossPartial;

    /**
     * @var        ObjectCollection|ChildCronTaskLogExecucao[] Collection to store aggregation of ChildCronTaskLogExecucao objects.
     */
    protected $collCronTaskLogExecucaos;
    protected $collCronTaskLogExecucaosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCronTaskArquivos[]
     */
    protected $cronTaskArquivossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCronTaskLogExecucao[]
     */
    protected $cronTaskLogExecucaosScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->tipo = 'nenhum';
        $this->prioridade = 'nenhuma';
        $this->primeira_execucao = PropelDateTime::newInstance(NULL, null, 'DateTime');
        $this->ultima_execucao = PropelDateTime::newInstance(NULL, null, 'DateTime');
        $this->data_cadastro = PropelDateTime::newInstance(NULL, null, 'DateTime');
        $this->data_alterado = PropelDateTime::newInstance(NULL, null, 'DateTime');
        $this->tipo_execucao_arquivo = 'unica';
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\CronTask object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>CronTask</code> instance.  If
     * <code>obj</code> is an instance of <code>CronTask</code>, delegates to
     * <code>equals(CronTask)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|CronTask The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [nome] column value.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Get the [descricao] column value.
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Get the [minuto] column value.
     *
     * @return string
     */
    public function getMinuto()
    {
        return $this->minuto;
    }

    /**
     * Get the [hora] column value.
     *
     * @return string
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Get the [dia] column value.
     *
     * @return string
     */
    public function getDia()
    {
        return $this->dia;
    }

    /**
     * Get the [mes] column value.
     *
     * @return string
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Get the [dia_semana] column value.
     *
     * @return string
     */
    public function getDiaSemana()
    {
        return $this->dia_semana;
    }

    /**
     * Get the [tipo] column value.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->getAtivo();
    }

    /**
     * Get the [prioridade] column value.
     *
     * @return string
     */
    public function getPrioridade()
    {
        return $this->prioridade;
    }

    /**
     * Get the [optionally formatted] temporal [primeira_execucao] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPrimeiraExecucao($format = NULL)
    {
        if ($format === null) {
            return $this->primeira_execucao;
        } else {
            return $this->primeira_execucao instanceof \DateTimeInterface ? $this->primeira_execucao->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [ultima_execucao] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUltimaExecucao($format = NULL)
    {
        if ($format === null) {
            return $this->ultima_execucao;
        } else {
            return $this->ultima_execucao instanceof \DateTimeInterface ? $this->ultima_execucao->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [cron_task_grupo_id] column value.
     *
     * @return int
     */
    public function getCronTaskGrupoId()
    {
        return $this->cron_task_grupo_id;
    }

    /**
     * Get the [segundo] column value.
     *
     * @return string
     */
    public function getSegundo()
    {
        return $this->segundo;
    }

    /**
     * Get the [ano] column value.
     *
     * @return string
     */
    public function getAno()
    {
        return $this->ano;
    }

    /**
     * Get the [tipo_execucao_arquivo] column value.
     *
     * @return string
     */
    public function getTipoExecucaoArquivo()
    {
        return $this->tipo_execucao_arquivo;
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [nome] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setNome($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome !== $v) {
            $this->nome = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_NOME] = true;
        }

        return $this;
    } // setNome()

    /**
     * Set the value of [descricao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setDescricao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao !== $v) {
            $this->descricao = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_DESCRICAO] = true;
        }

        return $this;
    } // setDescricao()

    /**
     * Set the value of [minuto] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setMinuto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->minuto !== $v) {
            $this->minuto = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_MINUTO] = true;
        }

        return $this;
    } // setMinuto()

    /**
     * Set the value of [hora] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setHora($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->hora !== $v) {
            $this->hora = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_HORA] = true;
        }

        return $this;
    } // setHora()

    /**
     * Set the value of [dia] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setDia($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dia !== $v) {
            $this->dia = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_DIA] = true;
        }

        return $this;
    } // setDia()

    /**
     * Set the value of [mes] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setMes($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mes !== $v) {
            $this->mes = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_MES] = true;
        }

        return $this;
    } // setMes()

    /**
     * Set the value of [dia_semana] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setDiaSemana($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dia_semana !== $v) {
            $this->dia_semana = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_DIA_SEMANA] = true;
        }

        return $this;
    } // setDiaSemana()

    /**
     * Set the value of [tipo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setTipo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo !== $v) {
            $this->tipo = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_TIPO] = true;
        }

        return $this;
    } // setTipo()

    /**
     * Sets the value of the [ativo] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setAtivo($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ativo !== $v) {
            $this->ativo = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_ATIVO] = true;
        }

        return $this;
    } // setAtivo()

    /**
     * Set the value of [prioridade] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setPrioridade($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prioridade !== $v) {
            $this->prioridade = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_PRIORIDADE] = true;
        }

        return $this;
    } // setPrioridade()

    /**
     * Sets the value of [primeira_execucao] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setPrimeiraExecucao($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->primeira_execucao !== null || $dt !== null) {
            if ( ($dt != $this->primeira_execucao) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === NULL) // or the entered value matches the default
                 ) {
                $this->primeira_execucao = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CronTaskTableMap::COL_PRIMEIRA_EXECUCAO] = true;
            }
        } // if either are not null

        return $this;
    } // setPrimeiraExecucao()

    /**
     * Sets the value of [ultima_execucao] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setUltimaExecucao($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->ultima_execucao !== null || $dt !== null) {
            if ( ($dt != $this->ultima_execucao) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === NULL) // or the entered value matches the default
                 ) {
                $this->ultima_execucao = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CronTaskTableMap::COL_ULTIMA_EXECUCAO] = true;
            }
        } // if either are not null

        return $this;
    } // setUltimaExecucao()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ( ($dt != $this->data_cadastro) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === NULL) // or the entered value matches the default
                 ) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CronTaskTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ( ($dt != $this->data_alterado) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s.u') === NULL) // or the entered value matches the default
                 ) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CronTaskTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [cron_task_grupo_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setCronTaskGrupoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->cron_task_grupo_id !== $v) {
            $this->cron_task_grupo_id = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_CRON_TASK_GRUPO_ID] = true;
        }

        if ($this->aCronTaskGrupo !== null && $this->aCronTaskGrupo->getId() !== $v) {
            $this->aCronTaskGrupo = null;
        }

        return $this;
    } // setCronTaskGrupoId()

    /**
     * Set the value of [segundo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setSegundo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->segundo !== $v) {
            $this->segundo = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_SEGUNDO] = true;
        }

        return $this;
    } // setSegundo()

    /**
     * Set the value of [ano] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setAno($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ano !== $v) {
            $this->ano = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_ANO] = true;
        }

        return $this;
    } // setAno()

    /**
     * Set the value of [tipo_execucao_arquivo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setTipoExecucaoArquivo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo_execucao_arquivo !== $v) {
            $this->tipo_execucao_arquivo = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_TIPO_EXECUCAO_ARQUIVO] = true;
        }

        return $this;
    } // setTipoExecucaoArquivo()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[CronTaskTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->tipo !== 'nenhum') {
                return false;
            }

            if ($this->prioridade !== 'nenhuma') {
                return false;
            }

            if ($this->primeira_execucao && $this->primeira_execucao->format('Y-m-d H:i:s.u') !== NULL) {
                return false;
            }

            if ($this->ultima_execucao && $this->ultima_execucao->format('Y-m-d H:i:s.u') !== NULL) {
                return false;
            }

            if ($this->data_cadastro && $this->data_cadastro->format('Y-m-d H:i:s.u') !== NULL) {
                return false;
            }

            if ($this->data_alterado && $this->data_alterado->format('Y-m-d H:i:s.u') !== NULL) {
                return false;
            }

            if ($this->tipo_execucao_arquivo !== 'unica') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CronTaskTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CronTaskTableMap::translateFieldName('Nome', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nome = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CronTaskTableMap::translateFieldName('Descricao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CronTaskTableMap::translateFieldName('Minuto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->minuto = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CronTaskTableMap::translateFieldName('Hora', TableMap::TYPE_PHPNAME, $indexType)];
            $this->hora = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CronTaskTableMap::translateFieldName('Dia', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dia = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CronTaskTableMap::translateFieldName('Mes', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mes = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CronTaskTableMap::translateFieldName('DiaSemana', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dia_semana = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CronTaskTableMap::translateFieldName('Tipo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CronTaskTableMap::translateFieldName('Ativo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ativo = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CronTaskTableMap::translateFieldName('Prioridade', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prioridade = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CronTaskTableMap::translateFieldName('PrimeiraExecucao', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->primeira_execucao = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : CronTaskTableMap::translateFieldName('UltimaExecucao', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->ultima_execucao = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : CronTaskTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : CronTaskTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : CronTaskTableMap::translateFieldName('CronTaskGrupoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cron_task_grupo_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : CronTaskTableMap::translateFieldName('Segundo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->segundo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : CronTaskTableMap::translateFieldName('Ano', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ano = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : CronTaskTableMap::translateFieldName('TipoExecucaoArquivo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_execucao_arquivo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : CronTaskTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 20; // 20 = CronTaskTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\CronTask'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCronTaskGrupo !== null && $this->cron_task_grupo_id !== $this->aCronTaskGrupo->getId()) {
            $this->aCronTaskGrupo = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CronTaskTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCronTaskQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCronTaskGrupo = null;
            $this->aUsuario = null;
            $this->collCronTaskArquivoss = null;

            $this->collCronTaskLogExecucaos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see CronTask::setDeleted()
     * @see CronTask::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCronTaskQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CronTaskTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCronTaskGrupo !== null) {
                if ($this->aCronTaskGrupo->isModified() || $this->aCronTaskGrupo->isNew()) {
                    $affectedRows += $this->aCronTaskGrupo->save($con);
                }
                $this->setCronTaskGrupo($this->aCronTaskGrupo);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->cronTaskArquivossScheduledForDeletion !== null) {
                if (!$this->cronTaskArquivossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\CronTaskArquivosQuery::create()
                        ->filterByPrimaryKeys($this->cronTaskArquivossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->cronTaskArquivossScheduledForDeletion = null;
                }
            }

            if ($this->collCronTaskArquivoss !== null) {
                foreach ($this->collCronTaskArquivoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->cronTaskLogExecucaosScheduledForDeletion !== null) {
                if (!$this->cronTaskLogExecucaosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\CronTaskLogExecucaoQuery::create()
                        ->filterByPrimaryKeys($this->cronTaskLogExecucaosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->cronTaskLogExecucaosScheduledForDeletion = null;
                }
            }

            if ($this->collCronTaskLogExecucaos !== null) {
                foreach ($this->collCronTaskLogExecucaos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CronTaskTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CronTaskTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CronTaskTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_NOME)) {
            $modifiedColumns[':p' . $index++]  = 'nome';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DESCRICAO)) {
            $modifiedColumns[':p' . $index++]  = 'descricao';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_MINUTO)) {
            $modifiedColumns[':p' . $index++]  = 'minuto';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_HORA)) {
            $modifiedColumns[':p' . $index++]  = 'hora';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DIA)) {
            $modifiedColumns[':p' . $index++]  = 'dia';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_MES)) {
            $modifiedColumns[':p' . $index++]  = 'mes';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DIA_SEMANA)) {
            $modifiedColumns[':p' . $index++]  = 'dia_semana';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_TIPO)) {
            $modifiedColumns[':p' . $index++]  = 'tipo';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_ATIVO)) {
            $modifiedColumns[':p' . $index++]  = 'ativo';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_PRIORIDADE)) {
            $modifiedColumns[':p' . $index++]  = 'prioridade';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_PRIMEIRA_EXECUCAO)) {
            $modifiedColumns[':p' . $index++]  = 'primeira_execucao';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_ULTIMA_EXECUCAO)) {
            $modifiedColumns[':p' . $index++]  = 'ultima_execucao';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_CRON_TASK_GRUPO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'cron_task_grupo_id';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_SEGUNDO)) {
            $modifiedColumns[':p' . $index++]  = 'segundo';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_ANO)) {
            $modifiedColumns[':p' . $index++]  = 'ano';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_TIPO_EXECUCAO_ARQUIVO)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_execucao_arquivo';
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }

        $sql = sprintf(
            'INSERT INTO cron_task (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'nome':
                        $stmt->bindValue($identifier, $this->nome, PDO::PARAM_STR);
                        break;
                    case 'descricao':
                        $stmt->bindValue($identifier, $this->descricao, PDO::PARAM_STR);
                        break;
                    case 'minuto':
                        $stmt->bindValue($identifier, $this->minuto, PDO::PARAM_STR);
                        break;
                    case 'hora':
                        $stmt->bindValue($identifier, $this->hora, PDO::PARAM_STR);
                        break;
                    case 'dia':
                        $stmt->bindValue($identifier, $this->dia, PDO::PARAM_STR);
                        break;
                    case 'mes':
                        $stmt->bindValue($identifier, $this->mes, PDO::PARAM_STR);
                        break;
                    case 'dia_semana':
                        $stmt->bindValue($identifier, $this->dia_semana, PDO::PARAM_STR);
                        break;
                    case 'tipo':
                        $stmt->bindValue($identifier, $this->tipo, PDO::PARAM_STR);
                        break;
                    case 'ativo':
                        $stmt->bindValue($identifier, (int) $this->ativo, PDO::PARAM_INT);
                        break;
                    case 'prioridade':
                        $stmt->bindValue($identifier, $this->prioridade, PDO::PARAM_STR);
                        break;
                    case 'primeira_execucao':
                        $stmt->bindValue($identifier, $this->primeira_execucao ? $this->primeira_execucao->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'ultima_execucao':
                        $stmt->bindValue($identifier, $this->ultima_execucao ? $this->ultima_execucao->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'cron_task_grupo_id':
                        $stmt->bindValue($identifier, $this->cron_task_grupo_id, PDO::PARAM_INT);
                        break;
                    case 'segundo':
                        $stmt->bindValue($identifier, $this->segundo, PDO::PARAM_STR);
                        break;
                    case 'ano':
                        $stmt->bindValue($identifier, $this->ano, PDO::PARAM_STR);
                        break;
                    case 'tipo_execucao_arquivo':
                        $stmt->bindValue($identifier, $this->tipo_execucao_arquivo, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CronTaskTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNome();
                break;
            case 2:
                return $this->getDescricao();
                break;
            case 3:
                return $this->getMinuto();
                break;
            case 4:
                return $this->getHora();
                break;
            case 5:
                return $this->getDia();
                break;
            case 6:
                return $this->getMes();
                break;
            case 7:
                return $this->getDiaSemana();
                break;
            case 8:
                return $this->getTipo();
                break;
            case 9:
                return $this->getAtivo();
                break;
            case 10:
                return $this->getPrioridade();
                break;
            case 11:
                return $this->getPrimeiraExecucao();
                break;
            case 12:
                return $this->getUltimaExecucao();
                break;
            case 13:
                return $this->getDataCadastro();
                break;
            case 14:
                return $this->getDataAlterado();
                break;
            case 15:
                return $this->getCronTaskGrupoId();
                break;
            case 16:
                return $this->getSegundo();
                break;
            case 17:
                return $this->getAno();
                break;
            case 18:
                return $this->getTipoExecucaoArquivo();
                break;
            case 19:
                return $this->getUsuarioAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['CronTask'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CronTask'][$this->hashCode()] = true;
        $keys = CronTaskTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNome(),
            $keys[2] => $this->getDescricao(),
            $keys[3] => $this->getMinuto(),
            $keys[4] => $this->getHora(),
            $keys[5] => $this->getDia(),
            $keys[6] => $this->getMes(),
            $keys[7] => $this->getDiaSemana(),
            $keys[8] => $this->getTipo(),
            $keys[9] => $this->getAtivo(),
            $keys[10] => $this->getPrioridade(),
            $keys[11] => $this->getPrimeiraExecucao(),
            $keys[12] => $this->getUltimaExecucao(),
            $keys[13] => $this->getDataCadastro(),
            $keys[14] => $this->getDataAlterado(),
            $keys[15] => $this->getCronTaskGrupoId(),
            $keys[16] => $this->getSegundo(),
            $keys[17] => $this->getAno(),
            $keys[18] => $this->getTipoExecucaoArquivo(),
            $keys[19] => $this->getUsuarioAlterado(),
        );
        if ($result[$keys[11]] instanceof \DateTime) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        if ($result[$keys[12]] instanceof \DateTime) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTime) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        if ($result[$keys[14]] instanceof \DateTime) {
            $result[$keys[14]] = $result[$keys[14]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCronTaskGrupo) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'cronTaskGrupo';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cron_task_grupo';
                        break;
                    default:
                        $key = 'CronTaskGrupo';
                }

                $result[$key] = $this->aCronTaskGrupo->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCronTaskArquivoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'cronTaskArquivoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cron_task_arquivoss';
                        break;
                    default:
                        $key = 'CronTaskArquivoss';
                }

                $result[$key] = $this->collCronTaskArquivoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCronTaskLogExecucaos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'cronTaskLogExecucaos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'cron_task_log_execucaos';
                        break;
                    default:
                        $key = 'CronTaskLogExecucaos';
                }

                $result[$key] = $this->collCronTaskLogExecucaos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\CronTask
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CronTaskTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\CronTask
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNome($value);
                break;
            case 2:
                $this->setDescricao($value);
                break;
            case 3:
                $this->setMinuto($value);
                break;
            case 4:
                $this->setHora($value);
                break;
            case 5:
                $this->setDia($value);
                break;
            case 6:
                $this->setMes($value);
                break;
            case 7:
                $this->setDiaSemana($value);
                break;
            case 8:
                $this->setTipo($value);
                break;
            case 9:
                $this->setAtivo($value);
                break;
            case 10:
                $this->setPrioridade($value);
                break;
            case 11:
                $this->setPrimeiraExecucao($value);
                break;
            case 12:
                $this->setUltimaExecucao($value);
                break;
            case 13:
                $this->setDataCadastro($value);
                break;
            case 14:
                $this->setDataAlterado($value);
                break;
            case 15:
                $this->setCronTaskGrupoId($value);
                break;
            case 16:
                $this->setSegundo($value);
                break;
            case 17:
                $this->setAno($value);
                break;
            case 18:
                $this->setTipoExecucaoArquivo($value);
                break;
            case 19:
                $this->setUsuarioAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CronTaskTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNome($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDescricao($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setMinuto($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setHora($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDia($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setMes($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDiaSemana($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTipo($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setAtivo($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPrioridade($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setPrimeiraExecucao($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUltimaExecucao($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setDataCadastro($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setDataAlterado($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setCronTaskGrupoId($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setSegundo($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setAno($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setTipoExecucaoArquivo($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[19]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CronTaskTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CronTaskTableMap::COL_ID)) {
            $criteria->add(CronTaskTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_NOME)) {
            $criteria->add(CronTaskTableMap::COL_NOME, $this->nome);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DESCRICAO)) {
            $criteria->add(CronTaskTableMap::COL_DESCRICAO, $this->descricao);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_MINUTO)) {
            $criteria->add(CronTaskTableMap::COL_MINUTO, $this->minuto);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_HORA)) {
            $criteria->add(CronTaskTableMap::COL_HORA, $this->hora);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DIA)) {
            $criteria->add(CronTaskTableMap::COL_DIA, $this->dia);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_MES)) {
            $criteria->add(CronTaskTableMap::COL_MES, $this->mes);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DIA_SEMANA)) {
            $criteria->add(CronTaskTableMap::COL_DIA_SEMANA, $this->dia_semana);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_TIPO)) {
            $criteria->add(CronTaskTableMap::COL_TIPO, $this->tipo);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_ATIVO)) {
            $criteria->add(CronTaskTableMap::COL_ATIVO, $this->ativo);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_PRIORIDADE)) {
            $criteria->add(CronTaskTableMap::COL_PRIORIDADE, $this->prioridade);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_PRIMEIRA_EXECUCAO)) {
            $criteria->add(CronTaskTableMap::COL_PRIMEIRA_EXECUCAO, $this->primeira_execucao);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_ULTIMA_EXECUCAO)) {
            $criteria->add(CronTaskTableMap::COL_ULTIMA_EXECUCAO, $this->ultima_execucao);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(CronTaskTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(CronTaskTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_CRON_TASK_GRUPO_ID)) {
            $criteria->add(CronTaskTableMap::COL_CRON_TASK_GRUPO_ID, $this->cron_task_grupo_id);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_SEGUNDO)) {
            $criteria->add(CronTaskTableMap::COL_SEGUNDO, $this->segundo);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_ANO)) {
            $criteria->add(CronTaskTableMap::COL_ANO, $this->ano);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_TIPO_EXECUCAO_ARQUIVO)) {
            $criteria->add(CronTaskTableMap::COL_TIPO_EXECUCAO_ARQUIVO, $this->tipo_execucao_arquivo);
        }
        if ($this->isColumnModified(CronTaskTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(CronTaskTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCronTaskQuery::create();
        $criteria->add(CronTaskTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\CronTask (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNome($this->getNome());
        $copyObj->setDescricao($this->getDescricao());
        $copyObj->setMinuto($this->getMinuto());
        $copyObj->setHora($this->getHora());
        $copyObj->setDia($this->getDia());
        $copyObj->setMes($this->getMes());
        $copyObj->setDiaSemana($this->getDiaSemana());
        $copyObj->setTipo($this->getTipo());
        $copyObj->setAtivo($this->getAtivo());
        $copyObj->setPrioridade($this->getPrioridade());
        $copyObj->setPrimeiraExecucao($this->getPrimeiraExecucao());
        $copyObj->setUltimaExecucao($this->getUltimaExecucao());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setCronTaskGrupoId($this->getCronTaskGrupoId());
        $copyObj->setSegundo($this->getSegundo());
        $copyObj->setAno($this->getAno());
        $copyObj->setTipoExecucaoArquivo($this->getTipoExecucaoArquivo());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCronTaskArquivoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCronTaskArquivos($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCronTaskLogExecucaos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCronTaskLogExecucao($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\CronTask Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCronTaskGrupo object.
     *
     * @param  ChildCronTaskGrupo $v
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCronTaskGrupo(ChildCronTaskGrupo $v = null)
    {
        if ($v === null) {
            $this->setCronTaskGrupoId(NULL);
        } else {
            $this->setCronTaskGrupoId($v->getId());
        }

        $this->aCronTaskGrupo = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCronTaskGrupo object, it will not be re-added.
        if ($v !== null) {
            $v->addCronTask($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCronTaskGrupo object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCronTaskGrupo The associated ChildCronTaskGrupo object.
     * @throws PropelException
     */
    public function getCronTaskGrupo(ConnectionInterface $con = null)
    {
        if ($this->aCronTaskGrupo === null && ($this->cron_task_grupo_id !== null)) {
            $this->aCronTaskGrupo = ChildCronTaskGrupoQuery::create()->findPk($this->cron_task_grupo_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCronTaskGrupo->addCronTasks($this);
             */
        }

        return $this->aCronTaskGrupo;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addCronTask($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addCronTasks($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CronTaskArquivos' == $relationName) {
            return $this->initCronTaskArquivoss();
        }
        if ('CronTaskLogExecucao' == $relationName) {
            return $this->initCronTaskLogExecucaos();
        }
    }

    /**
     * Clears out the collCronTaskArquivoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCronTaskArquivoss()
     */
    public function clearCronTaskArquivoss()
    {
        $this->collCronTaskArquivoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCronTaskArquivoss collection loaded partially.
     */
    public function resetPartialCronTaskArquivoss($v = true)
    {
        $this->collCronTaskArquivossPartial = $v;
    }

    /**
     * Initializes the collCronTaskArquivoss collection.
     *
     * By default this just sets the collCronTaskArquivoss collection to an empty array (like clearcollCronTaskArquivoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCronTaskArquivoss($overrideExisting = true)
    {
        if (null !== $this->collCronTaskArquivoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = CronTaskArquivosTableMap::getTableMap()->getCollectionClassName();

        $this->collCronTaskArquivoss = new $collectionClassName;
        $this->collCronTaskArquivoss->setModel('\ImaTelecomBundle\Model\CronTaskArquivos');
    }

    /**
     * Gets an array of ChildCronTaskArquivos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCronTask is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCronTaskArquivos[] List of ChildCronTaskArquivos objects
     * @throws PropelException
     */
    public function getCronTaskArquivoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCronTaskArquivossPartial && !$this->isNew();
        if (null === $this->collCronTaskArquivoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCronTaskArquivoss) {
                // return empty collection
                $this->initCronTaskArquivoss();
            } else {
                $collCronTaskArquivoss = ChildCronTaskArquivosQuery::create(null, $criteria)
                    ->filterByCronTask($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCronTaskArquivossPartial && count($collCronTaskArquivoss)) {
                        $this->initCronTaskArquivoss(false);

                        foreach ($collCronTaskArquivoss as $obj) {
                            if (false == $this->collCronTaskArquivoss->contains($obj)) {
                                $this->collCronTaskArquivoss->append($obj);
                            }
                        }

                        $this->collCronTaskArquivossPartial = true;
                    }

                    return $collCronTaskArquivoss;
                }

                if ($partial && $this->collCronTaskArquivoss) {
                    foreach ($this->collCronTaskArquivoss as $obj) {
                        if ($obj->isNew()) {
                            $collCronTaskArquivoss[] = $obj;
                        }
                    }
                }

                $this->collCronTaskArquivoss = $collCronTaskArquivoss;
                $this->collCronTaskArquivossPartial = false;
            }
        }

        return $this->collCronTaskArquivoss;
    }

    /**
     * Sets a collection of ChildCronTaskArquivos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $cronTaskArquivoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCronTask The current object (for fluent API support)
     */
    public function setCronTaskArquivoss(Collection $cronTaskArquivoss, ConnectionInterface $con = null)
    {
        /** @var ChildCronTaskArquivos[] $cronTaskArquivossToDelete */
        $cronTaskArquivossToDelete = $this->getCronTaskArquivoss(new Criteria(), $con)->diff($cronTaskArquivoss);


        $this->cronTaskArquivossScheduledForDeletion = $cronTaskArquivossToDelete;

        foreach ($cronTaskArquivossToDelete as $cronTaskArquivosRemoved) {
            $cronTaskArquivosRemoved->setCronTask(null);
        }

        $this->collCronTaskArquivoss = null;
        foreach ($cronTaskArquivoss as $cronTaskArquivos) {
            $this->addCronTaskArquivos($cronTaskArquivos);
        }

        $this->collCronTaskArquivoss = $cronTaskArquivoss;
        $this->collCronTaskArquivossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CronTaskArquivos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CronTaskArquivos objects.
     * @throws PropelException
     */
    public function countCronTaskArquivoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCronTaskArquivossPartial && !$this->isNew();
        if (null === $this->collCronTaskArquivoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCronTaskArquivoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCronTaskArquivoss());
            }

            $query = ChildCronTaskArquivosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCronTask($this)
                ->count($con);
        }

        return count($this->collCronTaskArquivoss);
    }

    /**
     * Method called to associate a ChildCronTaskArquivos object to this object
     * through the ChildCronTaskArquivos foreign key attribute.
     *
     * @param  ChildCronTaskArquivos $l ChildCronTaskArquivos
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function addCronTaskArquivos(ChildCronTaskArquivos $l)
    {
        if ($this->collCronTaskArquivoss === null) {
            $this->initCronTaskArquivoss();
            $this->collCronTaskArquivossPartial = true;
        }

        if (!$this->collCronTaskArquivoss->contains($l)) {
            $this->doAddCronTaskArquivos($l);

            if ($this->cronTaskArquivossScheduledForDeletion and $this->cronTaskArquivossScheduledForDeletion->contains($l)) {
                $this->cronTaskArquivossScheduledForDeletion->remove($this->cronTaskArquivossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCronTaskArquivos $cronTaskArquivos The ChildCronTaskArquivos object to add.
     */
    protected function doAddCronTaskArquivos(ChildCronTaskArquivos $cronTaskArquivos)
    {
        $this->collCronTaskArquivoss[]= $cronTaskArquivos;
        $cronTaskArquivos->setCronTask($this);
    }

    /**
     * @param  ChildCronTaskArquivos $cronTaskArquivos The ChildCronTaskArquivos object to remove.
     * @return $this|ChildCronTask The current object (for fluent API support)
     */
    public function removeCronTaskArquivos(ChildCronTaskArquivos $cronTaskArquivos)
    {
        if ($this->getCronTaskArquivoss()->contains($cronTaskArquivos)) {
            $pos = $this->collCronTaskArquivoss->search($cronTaskArquivos);
            $this->collCronTaskArquivoss->remove($pos);
            if (null === $this->cronTaskArquivossScheduledForDeletion) {
                $this->cronTaskArquivossScheduledForDeletion = clone $this->collCronTaskArquivoss;
                $this->cronTaskArquivossScheduledForDeletion->clear();
            }
            $this->cronTaskArquivossScheduledForDeletion[]= clone $cronTaskArquivos;
            $cronTaskArquivos->setCronTask(null);
        }

        return $this;
    }

    /**
     * Clears out the collCronTaskLogExecucaos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCronTaskLogExecucaos()
     */
    public function clearCronTaskLogExecucaos()
    {
        $this->collCronTaskLogExecucaos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCronTaskLogExecucaos collection loaded partially.
     */
    public function resetPartialCronTaskLogExecucaos($v = true)
    {
        $this->collCronTaskLogExecucaosPartial = $v;
    }

    /**
     * Initializes the collCronTaskLogExecucaos collection.
     *
     * By default this just sets the collCronTaskLogExecucaos collection to an empty array (like clearcollCronTaskLogExecucaos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCronTaskLogExecucaos($overrideExisting = true)
    {
        if (null !== $this->collCronTaskLogExecucaos && !$overrideExisting) {
            return;
        }

        $collectionClassName = CronTaskLogExecucaoTableMap::getTableMap()->getCollectionClassName();

        $this->collCronTaskLogExecucaos = new $collectionClassName;
        $this->collCronTaskLogExecucaos->setModel('\ImaTelecomBundle\Model\CronTaskLogExecucao');
    }

    /**
     * Gets an array of ChildCronTaskLogExecucao objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCronTask is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCronTaskLogExecucao[] List of ChildCronTaskLogExecucao objects
     * @throws PropelException
     */
    public function getCronTaskLogExecucaos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCronTaskLogExecucaosPartial && !$this->isNew();
        if (null === $this->collCronTaskLogExecucaos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCronTaskLogExecucaos) {
                // return empty collection
                $this->initCronTaskLogExecucaos();
            } else {
                $collCronTaskLogExecucaos = ChildCronTaskLogExecucaoQuery::create(null, $criteria)
                    ->filterByCronTask($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCronTaskLogExecucaosPartial && count($collCronTaskLogExecucaos)) {
                        $this->initCronTaskLogExecucaos(false);

                        foreach ($collCronTaskLogExecucaos as $obj) {
                            if (false == $this->collCronTaskLogExecucaos->contains($obj)) {
                                $this->collCronTaskLogExecucaos->append($obj);
                            }
                        }

                        $this->collCronTaskLogExecucaosPartial = true;
                    }

                    return $collCronTaskLogExecucaos;
                }

                if ($partial && $this->collCronTaskLogExecucaos) {
                    foreach ($this->collCronTaskLogExecucaos as $obj) {
                        if ($obj->isNew()) {
                            $collCronTaskLogExecucaos[] = $obj;
                        }
                    }
                }

                $this->collCronTaskLogExecucaos = $collCronTaskLogExecucaos;
                $this->collCronTaskLogExecucaosPartial = false;
            }
        }

        return $this->collCronTaskLogExecucaos;
    }

    /**
     * Sets a collection of ChildCronTaskLogExecucao objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $cronTaskLogExecucaos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCronTask The current object (for fluent API support)
     */
    public function setCronTaskLogExecucaos(Collection $cronTaskLogExecucaos, ConnectionInterface $con = null)
    {
        /** @var ChildCronTaskLogExecucao[] $cronTaskLogExecucaosToDelete */
        $cronTaskLogExecucaosToDelete = $this->getCronTaskLogExecucaos(new Criteria(), $con)->diff($cronTaskLogExecucaos);


        $this->cronTaskLogExecucaosScheduledForDeletion = $cronTaskLogExecucaosToDelete;

        foreach ($cronTaskLogExecucaosToDelete as $cronTaskLogExecucaoRemoved) {
            $cronTaskLogExecucaoRemoved->setCronTask(null);
        }

        $this->collCronTaskLogExecucaos = null;
        foreach ($cronTaskLogExecucaos as $cronTaskLogExecucao) {
            $this->addCronTaskLogExecucao($cronTaskLogExecucao);
        }

        $this->collCronTaskLogExecucaos = $cronTaskLogExecucaos;
        $this->collCronTaskLogExecucaosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CronTaskLogExecucao objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CronTaskLogExecucao objects.
     * @throws PropelException
     */
    public function countCronTaskLogExecucaos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCronTaskLogExecucaosPartial && !$this->isNew();
        if (null === $this->collCronTaskLogExecucaos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCronTaskLogExecucaos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCronTaskLogExecucaos());
            }

            $query = ChildCronTaskLogExecucaoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCronTask($this)
                ->count($con);
        }

        return count($this->collCronTaskLogExecucaos);
    }

    /**
     * Method called to associate a ChildCronTaskLogExecucao object to this object
     * through the ChildCronTaskLogExecucao foreign key attribute.
     *
     * @param  ChildCronTaskLogExecucao $l ChildCronTaskLogExecucao
     * @return $this|\ImaTelecomBundle\Model\CronTask The current object (for fluent API support)
     */
    public function addCronTaskLogExecucao(ChildCronTaskLogExecucao $l)
    {
        if ($this->collCronTaskLogExecucaos === null) {
            $this->initCronTaskLogExecucaos();
            $this->collCronTaskLogExecucaosPartial = true;
        }

        if (!$this->collCronTaskLogExecucaos->contains($l)) {
            $this->doAddCronTaskLogExecucao($l);

            if ($this->cronTaskLogExecucaosScheduledForDeletion and $this->cronTaskLogExecucaosScheduledForDeletion->contains($l)) {
                $this->cronTaskLogExecucaosScheduledForDeletion->remove($this->cronTaskLogExecucaosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCronTaskLogExecucao $cronTaskLogExecucao The ChildCronTaskLogExecucao object to add.
     */
    protected function doAddCronTaskLogExecucao(ChildCronTaskLogExecucao $cronTaskLogExecucao)
    {
        $this->collCronTaskLogExecucaos[]= $cronTaskLogExecucao;
        $cronTaskLogExecucao->setCronTask($this);
    }

    /**
     * @param  ChildCronTaskLogExecucao $cronTaskLogExecucao The ChildCronTaskLogExecucao object to remove.
     * @return $this|ChildCronTask The current object (for fluent API support)
     */
    public function removeCronTaskLogExecucao(ChildCronTaskLogExecucao $cronTaskLogExecucao)
    {
        if ($this->getCronTaskLogExecucaos()->contains($cronTaskLogExecucao)) {
            $pos = $this->collCronTaskLogExecucaos->search($cronTaskLogExecucao);
            $this->collCronTaskLogExecucaos->remove($pos);
            if (null === $this->cronTaskLogExecucaosScheduledForDeletion) {
                $this->cronTaskLogExecucaosScheduledForDeletion = clone $this->collCronTaskLogExecucaos;
                $this->cronTaskLogExecucaosScheduledForDeletion->clear();
            }
            $this->cronTaskLogExecucaosScheduledForDeletion[]= clone $cronTaskLogExecucao;
            $cronTaskLogExecucao->setCronTask(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCronTaskGrupo) {
            $this->aCronTaskGrupo->removeCronTask($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeCronTask($this);
        }
        $this->id = null;
        $this->nome = null;
        $this->descricao = null;
        $this->minuto = null;
        $this->hora = null;
        $this->dia = null;
        $this->mes = null;
        $this->dia_semana = null;
        $this->tipo = null;
        $this->ativo = null;
        $this->prioridade = null;
        $this->primeira_execucao = null;
        $this->ultima_execucao = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->cron_task_grupo_id = null;
        $this->segundo = null;
        $this->ano = null;
        $this->tipo_execucao_arquivo = null;
        $this->usuario_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCronTaskArquivoss) {
                foreach ($this->collCronTaskArquivoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCronTaskLogExecucaos) {
                foreach ($this->collCronTaskLogExecucaos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCronTaskArquivoss = null;
        $this->collCronTaskLogExecucaos = null;
        $this->aCronTaskGrupo = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CronTaskTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
