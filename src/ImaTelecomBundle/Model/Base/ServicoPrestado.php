<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Planos as ChildPlanos;
use ImaTelecomBundle\Model\PlanosQuery as ChildPlanosQuery;
use ImaTelecomBundle\Model\ServicoCliente as ChildServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery as ChildServicoClienteQuery;
use ImaTelecomBundle\Model\ServicoPrestado as ChildServicoPrestado;
use ImaTelecomBundle\Model\ServicoPrestadoQuery as ChildServicoPrestadoQuery;
use ImaTelecomBundle\Model\TipoServicoPrestado as ChildTipoServicoPrestado;
use ImaTelecomBundle\Model\TipoServicoPrestadoQuery as ChildTipoServicoPrestadoQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\ServicoClienteTableMap;
use ImaTelecomBundle\Model\Map\ServicoPrestadoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'servico_prestado' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class ServicoPrestado implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\ServicoPrestadoTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idservico_prestado field.
     *
     * @var        int
     */
    protected $idservico_prestado;

    /**
     * The value for the nome field.
     *
     * @var        string
     */
    protected $nome;

    /**
     * The value for the codigo field.
     *
     * @var        string
     */
    protected $codigo;

    /**
     * The value for the download field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $download;

    /**
     * The value for the upload field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $upload;

    /**
     * The value for the franquia field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $franquia;

    /**
     * The value for the tecnologia field.
     *
     * @var        string
     */
    protected $tecnologia;

    /**
     * The value for the garantia_banda field.
     *
     * @var        int
     */
    protected $garantia_banda;

    /**
     * The value for the ativo field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $ativo;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the pre_pago field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $pre_pago;

    /**
     * The value for the plano_id field.
     *
     * @var        int
     */
    protected $plano_id;

    /**
     * The value for the tipo field.
     *
     * Note: this column has a database default value of: 'internet'
     * @var        string
     */
    protected $tipo;

    /**
     * The value for the tipo_servico_prestado_id field.
     *
     * @var        int
     */
    protected $tipo_servico_prestado_id;

    /**
     * @var        ChildPlanos
     */
    protected $aPlanos;

    /**
     * @var        ChildTipoServicoPrestado
     */
    protected $aTipoServicoPrestado;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildServicoCliente[] Collection to store aggregation of ChildServicoCliente objects.
     */
    protected $collServicoClientes;
    protected $collServicoClientesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicoCliente[]
     */
    protected $servicoClientesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->download = 0;
        $this->upload = 0;
        $this->franquia = 0;
        $this->ativo = false;
        $this->pre_pago = false;
        $this->tipo = 'internet';
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\ServicoPrestado object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ServicoPrestado</code> instance.  If
     * <code>obj</code> is an instance of <code>ServicoPrestado</code>, delegates to
     * <code>equals(ServicoPrestado)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ServicoPrestado The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idservico_prestado] column value.
     *
     * @return int
     */
    public function getIdservicoPrestado()
    {
        return $this->idservico_prestado;
    }

    /**
     * Get the [nome] column value.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Get the [codigo] column value.
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Get the [download] column value.
     *
     * @return int
     */
    public function getDownload()
    {
        return $this->download;
    }

    /**
     * Get the [upload] column value.
     *
     * @return int
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /**
     * Get the [franquia] column value.
     *
     * @return int
     */
    public function getFranquia()
    {
        return $this->franquia;
    }

    /**
     * Get the [tecnologia] column value.
     *
     * @return string
     */
    public function getTecnologia()
    {
        return $this->tecnologia;
    }

    /**
     * Get the [garantia_banda] column value.
     *
     * @return int
     */
    public function getGarantiaBanda()
    {
        return $this->garantia_banda;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Get the [ativo] column value.
     *
     * @return boolean
     */
    public function isAtivo()
    {
        return $this->getAtivo();
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [pre_pago] column value.
     *
     * @return boolean
     */
    public function getPrePago()
    {
        return $this->pre_pago;
    }

    /**
     * Get the [pre_pago] column value.
     *
     * @return boolean
     */
    public function isPrePago()
    {
        return $this->getPrePago();
    }

    /**
     * Get the [plano_id] column value.
     *
     * @return int
     */
    public function getPlanoId()
    {
        return $this->plano_id;
    }

    /**
     * Get the [tipo] column value.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Get the [tipo_servico_prestado_id] column value.
     *
     * @return int
     */
    public function getTipoServicoPrestadoId()
    {
        return $this->tipo_servico_prestado_id;
    }

    /**
     * Set the value of [idservico_prestado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setIdservicoPrestado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idservico_prestado !== $v) {
            $this->idservico_prestado = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO] = true;
        }

        return $this;
    } // setIdservicoPrestado()

    /**
     * Set the value of [nome] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setNome($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nome !== $v) {
            $this->nome = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_NOME] = true;
        }

        return $this;
    } // setNome()

    /**
     * Set the value of [codigo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setCodigo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo !== $v) {
            $this->codigo = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_CODIGO] = true;
        }

        return $this;
    } // setCodigo()

    /**
     * Set the value of [download] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setDownload($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->download !== $v) {
            $this->download = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_DOWNLOAD] = true;
        }

        return $this;
    } // setDownload()

    /**
     * Set the value of [upload] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setUpload($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->upload !== $v) {
            $this->upload = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_UPLOAD] = true;
        }

        return $this;
    } // setUpload()

    /**
     * Set the value of [franquia] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setFranquia($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->franquia !== $v) {
            $this->franquia = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_FRANQUIA] = true;
        }

        return $this;
    } // setFranquia()

    /**
     * Set the value of [tecnologia] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setTecnologia($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tecnologia !== $v) {
            $this->tecnologia = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_TECNOLOGIA] = true;
        }

        return $this;
    } // setTecnologia()

    /**
     * Set the value of [garantia_banda] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setGarantiaBanda($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->garantia_banda !== $v) {
            $this->garantia_banda = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_GARANTIA_BANDA] = true;
        }

        return $this;
    } // setGarantiaBanda()

    /**
     * Sets the value of the [ativo] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setAtivo($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ativo !== $v) {
            $this->ativo = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_ATIVO] = true;
        }

        return $this;
    } // setAtivo()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicoPrestadoTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ServicoPrestadoTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Sets the value of the [pre_pago] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setPrePago($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->pre_pago !== $v) {
            $this->pre_pago = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_PRE_PAGO] = true;
        }

        return $this;
    } // setPrePago()

    /**
     * Set the value of [plano_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setPlanoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->plano_id !== $v) {
            $this->plano_id = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_PLANO_ID] = true;
        }

        if ($this->aPlanos !== null && $this->aPlanos->getIdplano() !== $v) {
            $this->aPlanos = null;
        }

        return $this;
    } // setPlanoId()

    /**
     * Set the value of [tipo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setTipo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo !== $v) {
            $this->tipo = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_TIPO] = true;
        }

        return $this;
    } // setTipo()

    /**
     * Set the value of [tipo_servico_prestado_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function setTipoServicoPrestadoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->tipo_servico_prestado_id !== $v) {
            $this->tipo_servico_prestado_id = $v;
            $this->modifiedColumns[ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID] = true;
        }

        if ($this->aTipoServicoPrestado !== null && $this->aTipoServicoPrestado->getIdtipoServicoPrestado() !== $v) {
            $this->aTipoServicoPrestado = null;
        }

        return $this;
    } // setTipoServicoPrestadoId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->download !== 0) {
                return false;
            }

            if ($this->upload !== 0) {
                return false;
            }

            if ($this->franquia !== 0) {
                return false;
            }

            if ($this->ativo !== false) {
                return false;
            }

            if ($this->pre_pago !== false) {
                return false;
            }

            if ($this->tipo !== 'internet') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ServicoPrestadoTableMap::translateFieldName('IdservicoPrestado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idservico_prestado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ServicoPrestadoTableMap::translateFieldName('Nome', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nome = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ServicoPrestadoTableMap::translateFieldName('Codigo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codigo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ServicoPrestadoTableMap::translateFieldName('Download', TableMap::TYPE_PHPNAME, $indexType)];
            $this->download = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ServicoPrestadoTableMap::translateFieldName('Upload', TableMap::TYPE_PHPNAME, $indexType)];
            $this->upload = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ServicoPrestadoTableMap::translateFieldName('Franquia', TableMap::TYPE_PHPNAME, $indexType)];
            $this->franquia = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ServicoPrestadoTableMap::translateFieldName('Tecnologia', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tecnologia = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ServicoPrestadoTableMap::translateFieldName('GarantiaBanda', TableMap::TYPE_PHPNAME, $indexType)];
            $this->garantia_banda = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ServicoPrestadoTableMap::translateFieldName('Ativo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ativo = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ServicoPrestadoTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ServicoPrestadoTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ServicoPrestadoTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ServicoPrestadoTableMap::translateFieldName('PrePago', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pre_pago = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ServicoPrestadoTableMap::translateFieldName('PlanoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->plano_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ServicoPrestadoTableMap::translateFieldName('Tipo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ServicoPrestadoTableMap::translateFieldName('TipoServicoPrestadoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo_servico_prestado_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = ServicoPrestadoTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\ServicoPrestado'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
        if ($this->aPlanos !== null && $this->plano_id !== $this->aPlanos->getIdplano()) {
            $this->aPlanos = null;
        }
        if ($this->aTipoServicoPrestado !== null && $this->tipo_servico_prestado_id !== $this->aTipoServicoPrestado->getIdtipoServicoPrestado()) {
            $this->aTipoServicoPrestado = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ServicoPrestadoTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildServicoPrestadoQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPlanos = null;
            $this->aTipoServicoPrestado = null;
            $this->aUsuario = null;
            $this->collServicoClientes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ServicoPrestado::setDeleted()
     * @see ServicoPrestado::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoPrestadoTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildServicoPrestadoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoPrestadoTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ServicoPrestadoTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPlanos !== null) {
                if ($this->aPlanos->isModified() || $this->aPlanos->isNew()) {
                    $affectedRows += $this->aPlanos->save($con);
                }
                $this->setPlanos($this->aPlanos);
            }

            if ($this->aTipoServicoPrestado !== null) {
                if ($this->aTipoServicoPrestado->isModified() || $this->aTipoServicoPrestado->isNew()) {
                    $affectedRows += $this->aTipoServicoPrestado->save($con);
                }
                $this->setTipoServicoPrestado($this->aTipoServicoPrestado);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->servicoClientesScheduledForDeletion !== null) {
                if (!$this->servicoClientesScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ServicoClienteQuery::create()
                        ->filterByPrimaryKeys($this->servicoClientesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicoClientesScheduledForDeletion = null;
                }
            }

            if ($this->collServicoClientes !== null) {
                foreach ($this->collServicoClientes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO] = true;
        if (null !== $this->idservico_prestado) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO)) {
            $modifiedColumns[':p' . $index++]  = 'idservico_prestado';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_NOME)) {
            $modifiedColumns[':p' . $index++]  = 'nome';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_CODIGO)) {
            $modifiedColumns[':p' . $index++]  = 'codigo';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_DOWNLOAD)) {
            $modifiedColumns[':p' . $index++]  = 'download';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_UPLOAD)) {
            $modifiedColumns[':p' . $index++]  = 'upload';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_FRANQUIA)) {
            $modifiedColumns[':p' . $index++]  = 'franquia';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_TECNOLOGIA)) {
            $modifiedColumns[':p' . $index++]  = 'tecnologia';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_GARANTIA_BANDA)) {
            $modifiedColumns[':p' . $index++]  = 'garantia_banda';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_ATIVO)) {
            $modifiedColumns[':p' . $index++]  = 'ativo';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_PRE_PAGO)) {
            $modifiedColumns[':p' . $index++]  = 'pre_pago';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_PLANO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'plano_id';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_TIPO)) {
            $modifiedColumns[':p' . $index++]  = 'tipo';
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'tipo_servico_prestado_id';
        }

        $sql = sprintf(
            'INSERT INTO servico_prestado (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idservico_prestado':
                        $stmt->bindValue($identifier, $this->idservico_prestado, PDO::PARAM_INT);
                        break;
                    case 'nome':
                        $stmt->bindValue($identifier, $this->nome, PDO::PARAM_STR);
                        break;
                    case 'codigo':
                        $stmt->bindValue($identifier, $this->codigo, PDO::PARAM_STR);
                        break;
                    case 'download':
                        $stmt->bindValue($identifier, $this->download, PDO::PARAM_INT);
                        break;
                    case 'upload':
                        $stmt->bindValue($identifier, $this->upload, PDO::PARAM_INT);
                        break;
                    case 'franquia':
                        $stmt->bindValue($identifier, $this->franquia, PDO::PARAM_INT);
                        break;
                    case 'tecnologia':
                        $stmt->bindValue($identifier, $this->tecnologia, PDO::PARAM_STR);
                        break;
                    case 'garantia_banda':
                        $stmt->bindValue($identifier, $this->garantia_banda, PDO::PARAM_INT);
                        break;
                    case 'ativo':
                        $stmt->bindValue($identifier, (int) $this->ativo, PDO::PARAM_INT);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'pre_pago':
                        $stmt->bindValue($identifier, (int) $this->pre_pago, PDO::PARAM_INT);
                        break;
                    case 'plano_id':
                        $stmt->bindValue($identifier, $this->plano_id, PDO::PARAM_INT);
                        break;
                    case 'tipo':
                        $stmt->bindValue($identifier, $this->tipo, PDO::PARAM_STR);
                        break;
                    case 'tipo_servico_prestado_id':
                        $stmt->bindValue($identifier, $this->tipo_servico_prestado_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdservicoPrestado($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ServicoPrestadoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdservicoPrestado();
                break;
            case 1:
                return $this->getNome();
                break;
            case 2:
                return $this->getCodigo();
                break;
            case 3:
                return $this->getDownload();
                break;
            case 4:
                return $this->getUpload();
                break;
            case 5:
                return $this->getFranquia();
                break;
            case 6:
                return $this->getTecnologia();
                break;
            case 7:
                return $this->getGarantiaBanda();
                break;
            case 8:
                return $this->getAtivo();
                break;
            case 9:
                return $this->getDataCadastro();
                break;
            case 10:
                return $this->getDataAlterado();
                break;
            case 11:
                return $this->getUsuarioAlterado();
                break;
            case 12:
                return $this->getPrePago();
                break;
            case 13:
                return $this->getPlanoId();
                break;
            case 14:
                return $this->getTipo();
                break;
            case 15:
                return $this->getTipoServicoPrestadoId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ServicoPrestado'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ServicoPrestado'][$this->hashCode()] = true;
        $keys = ServicoPrestadoTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdservicoPrestado(),
            $keys[1] => $this->getNome(),
            $keys[2] => $this->getCodigo(),
            $keys[3] => $this->getDownload(),
            $keys[4] => $this->getUpload(),
            $keys[5] => $this->getFranquia(),
            $keys[6] => $this->getTecnologia(),
            $keys[7] => $this->getGarantiaBanda(),
            $keys[8] => $this->getAtivo(),
            $keys[9] => $this->getDataCadastro(),
            $keys[10] => $this->getDataAlterado(),
            $keys[11] => $this->getUsuarioAlterado(),
            $keys[12] => $this->getPrePago(),
            $keys[13] => $this->getPlanoId(),
            $keys[14] => $this->getTipo(),
            $keys[15] => $this->getTipoServicoPrestadoId(),
        );
        if ($result[$keys[9]] instanceof \DateTime) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTime) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPlanos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'planos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'planos';
                        break;
                    default:
                        $key = 'Planos';
                }

                $result[$key] = $this->aPlanos->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTipoServicoPrestado) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'tipoServicoPrestado';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'tipo_servico_prestado';
                        break;
                    default:
                        $key = 'TipoServicoPrestado';
                }

                $result[$key] = $this->aTipoServicoPrestado->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collServicoClientes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicoClientes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'servico_clientes';
                        break;
                    default:
                        $key = 'ServicoClientes';
                }

                $result[$key] = $this->collServicoClientes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ServicoPrestadoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdservicoPrestado($value);
                break;
            case 1:
                $this->setNome($value);
                break;
            case 2:
                $this->setCodigo($value);
                break;
            case 3:
                $this->setDownload($value);
                break;
            case 4:
                $this->setUpload($value);
                break;
            case 5:
                $this->setFranquia($value);
                break;
            case 6:
                $this->setTecnologia($value);
                break;
            case 7:
                $this->setGarantiaBanda($value);
                break;
            case 8:
                $this->setAtivo($value);
                break;
            case 9:
                $this->setDataCadastro($value);
                break;
            case 10:
                $this->setDataAlterado($value);
                break;
            case 11:
                $this->setUsuarioAlterado($value);
                break;
            case 12:
                $this->setPrePago($value);
                break;
            case 13:
                $this->setPlanoId($value);
                break;
            case 14:
                $this->setTipo($value);
                break;
            case 15:
                $this->setTipoServicoPrestadoId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ServicoPrestadoTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdservicoPrestado($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNome($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCodigo($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDownload($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUpload($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setFranquia($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setTecnologia($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setGarantiaBanda($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setAtivo($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDataCadastro($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDataAlterado($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setPrePago($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setPlanoId($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setTipo($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setTipoServicoPrestadoId($arr[$keys[15]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ServicoPrestadoTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO)) {
            $criteria->add(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, $this->idservico_prestado);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_NOME)) {
            $criteria->add(ServicoPrestadoTableMap::COL_NOME, $this->nome);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_CODIGO)) {
            $criteria->add(ServicoPrestadoTableMap::COL_CODIGO, $this->codigo);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_DOWNLOAD)) {
            $criteria->add(ServicoPrestadoTableMap::COL_DOWNLOAD, $this->download);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_UPLOAD)) {
            $criteria->add(ServicoPrestadoTableMap::COL_UPLOAD, $this->upload);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_FRANQUIA)) {
            $criteria->add(ServicoPrestadoTableMap::COL_FRANQUIA, $this->franquia);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_TECNOLOGIA)) {
            $criteria->add(ServicoPrestadoTableMap::COL_TECNOLOGIA, $this->tecnologia);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_GARANTIA_BANDA)) {
            $criteria->add(ServicoPrestadoTableMap::COL_GARANTIA_BANDA, $this->garantia_banda);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_ATIVO)) {
            $criteria->add(ServicoPrestadoTableMap::COL_ATIVO, $this->ativo);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(ServicoPrestadoTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(ServicoPrestadoTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(ServicoPrestadoTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_PRE_PAGO)) {
            $criteria->add(ServicoPrestadoTableMap::COL_PRE_PAGO, $this->pre_pago);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_PLANO_ID)) {
            $criteria->add(ServicoPrestadoTableMap::COL_PLANO_ID, $this->plano_id);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_TIPO)) {
            $criteria->add(ServicoPrestadoTableMap::COL_TIPO, $this->tipo);
        }
        if ($this->isColumnModified(ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID)) {
            $criteria->add(ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID, $this->tipo_servico_prestado_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildServicoPrestadoQuery::create();
        $criteria->add(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, $this->idservico_prestado);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdservicoPrestado();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdservicoPrestado();
    }

    /**
     * Generic method to set the primary key (idservico_prestado column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdservicoPrestado($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdservicoPrestado();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\ServicoPrestado (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNome($this->getNome());
        $copyObj->setCodigo($this->getCodigo());
        $copyObj->setDownload($this->getDownload());
        $copyObj->setUpload($this->getUpload());
        $copyObj->setFranquia($this->getFranquia());
        $copyObj->setTecnologia($this->getTecnologia());
        $copyObj->setGarantiaBanda($this->getGarantiaBanda());
        $copyObj->setAtivo($this->getAtivo());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setPrePago($this->getPrePago());
        $copyObj->setPlanoId($this->getPlanoId());
        $copyObj->setTipo($this->getTipo());
        $copyObj->setTipoServicoPrestadoId($this->getTipoServicoPrestadoId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getServicoClientes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicoCliente($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdservicoPrestado(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\ServicoPrestado Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildPlanos object.
     *
     * @param  ChildPlanos $v
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPlanos(ChildPlanos $v = null)
    {
        if ($v === null) {
            $this->setPlanoId(NULL);
        } else {
            $this->setPlanoId($v->getIdplano());
        }

        $this->aPlanos = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPlanos object, it will not be re-added.
        if ($v !== null) {
            $v->addServicoPrestado($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPlanos object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPlanos The associated ChildPlanos object.
     * @throws PropelException
     */
    public function getPlanos(ConnectionInterface $con = null)
    {
        if ($this->aPlanos === null && ($this->plano_id !== null)) {
            $this->aPlanos = ChildPlanosQuery::create()->findPk($this->plano_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPlanos->addServicoPrestados($this);
             */
        }

        return $this->aPlanos;
    }

    /**
     * Declares an association between this object and a ChildTipoServicoPrestado object.
     *
     * @param  ChildTipoServicoPrestado $v
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTipoServicoPrestado(ChildTipoServicoPrestado $v = null)
    {
        if ($v === null) {
            $this->setTipoServicoPrestadoId(NULL);
        } else {
            $this->setTipoServicoPrestadoId($v->getIdtipoServicoPrestado());
        }

        $this->aTipoServicoPrestado = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildTipoServicoPrestado object, it will not be re-added.
        if ($v !== null) {
            $v->addServicoPrestado($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildTipoServicoPrestado object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildTipoServicoPrestado The associated ChildTipoServicoPrestado object.
     * @throws PropelException
     */
    public function getTipoServicoPrestado(ConnectionInterface $con = null)
    {
        if ($this->aTipoServicoPrestado === null && ($this->tipo_servico_prestado_id !== null)) {
            $this->aTipoServicoPrestado = ChildTipoServicoPrestadoQuery::create()->findPk($this->tipo_servico_prestado_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTipoServicoPrestado->addServicoPrestados($this);
             */
        }

        return $this->aTipoServicoPrestado;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addServicoPrestado($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addServicoPrestados($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ServicoCliente' == $relationName) {
            return $this->initServicoClientes();
        }
    }

    /**
     * Clears out the collServicoClientes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicoClientes()
     */
    public function clearServicoClientes()
    {
        $this->collServicoClientes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicoClientes collection loaded partially.
     */
    public function resetPartialServicoClientes($v = true)
    {
        $this->collServicoClientesPartial = $v;
    }

    /**
     * Initializes the collServicoClientes collection.
     *
     * By default this just sets the collServicoClientes collection to an empty array (like clearcollServicoClientes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicoClientes($overrideExisting = true)
    {
        if (null !== $this->collServicoClientes && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicoClienteTableMap::getTableMap()->getCollectionClassName();

        $this->collServicoClientes = new $collectionClassName;
        $this->collServicoClientes->setModel('\ImaTelecomBundle\Model\ServicoCliente');
    }

    /**
     * Gets an array of ChildServicoCliente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildServicoPrestado is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     * @throws PropelException
     */
    public function getServicoClientes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoClientesPartial && !$this->isNew();
        if (null === $this->collServicoClientes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicoClientes) {
                // return empty collection
                $this->initServicoClientes();
            } else {
                $collServicoClientes = ChildServicoClienteQuery::create(null, $criteria)
                    ->filterByServicoPrestado($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicoClientesPartial && count($collServicoClientes)) {
                        $this->initServicoClientes(false);

                        foreach ($collServicoClientes as $obj) {
                            if (false == $this->collServicoClientes->contains($obj)) {
                                $this->collServicoClientes->append($obj);
                            }
                        }

                        $this->collServicoClientesPartial = true;
                    }

                    return $collServicoClientes;
                }

                if ($partial && $this->collServicoClientes) {
                    foreach ($this->collServicoClientes as $obj) {
                        if ($obj->isNew()) {
                            $collServicoClientes[] = $obj;
                        }
                    }
                }

                $this->collServicoClientes = $collServicoClientes;
                $this->collServicoClientesPartial = false;
            }
        }

        return $this->collServicoClientes;
    }

    /**
     * Sets a collection of ChildServicoCliente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicoClientes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildServicoPrestado The current object (for fluent API support)
     */
    public function setServicoClientes(Collection $servicoClientes, ConnectionInterface $con = null)
    {
        /** @var ChildServicoCliente[] $servicoClientesToDelete */
        $servicoClientesToDelete = $this->getServicoClientes(new Criteria(), $con)->diff($servicoClientes);


        $this->servicoClientesScheduledForDeletion = $servicoClientesToDelete;

        foreach ($servicoClientesToDelete as $servicoClienteRemoved) {
            $servicoClienteRemoved->setServicoPrestado(null);
        }

        $this->collServicoClientes = null;
        foreach ($servicoClientes as $servicoCliente) {
            $this->addServicoCliente($servicoCliente);
        }

        $this->collServicoClientes = $servicoClientes;
        $this->collServicoClientesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ServicoCliente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ServicoCliente objects.
     * @throws PropelException
     */
    public function countServicoClientes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoClientesPartial && !$this->isNew();
        if (null === $this->collServicoClientes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicoClientes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicoClientes());
            }

            $query = ChildServicoClienteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByServicoPrestado($this)
                ->count($con);
        }

        return count($this->collServicoClientes);
    }

    /**
     * Method called to associate a ChildServicoCliente object to this object
     * through the ChildServicoCliente foreign key attribute.
     *
     * @param  ChildServicoCliente $l ChildServicoCliente
     * @return $this|\ImaTelecomBundle\Model\ServicoPrestado The current object (for fluent API support)
     */
    public function addServicoCliente(ChildServicoCliente $l)
    {
        if ($this->collServicoClientes === null) {
            $this->initServicoClientes();
            $this->collServicoClientesPartial = true;
        }

        if (!$this->collServicoClientes->contains($l)) {
            $this->doAddServicoCliente($l);

            if ($this->servicoClientesScheduledForDeletion and $this->servicoClientesScheduledForDeletion->contains($l)) {
                $this->servicoClientesScheduledForDeletion->remove($this->servicoClientesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicoCliente $servicoCliente The ChildServicoCliente object to add.
     */
    protected function doAddServicoCliente(ChildServicoCliente $servicoCliente)
    {
        $this->collServicoClientes[]= $servicoCliente;
        $servicoCliente->setServicoPrestado($this);
    }

    /**
     * @param  ChildServicoCliente $servicoCliente The ChildServicoCliente object to remove.
     * @return $this|ChildServicoPrestado The current object (for fluent API support)
     */
    public function removeServicoCliente(ChildServicoCliente $servicoCliente)
    {
        if ($this->getServicoClientes()->contains($servicoCliente)) {
            $pos = $this->collServicoClientes->search($servicoCliente);
            $this->collServicoClientes->remove($pos);
            if (null === $this->servicoClientesScheduledForDeletion) {
                $this->servicoClientesScheduledForDeletion = clone $this->collServicoClientes;
                $this->servicoClientesScheduledForDeletion->clear();
            }
            $this->servicoClientesScheduledForDeletion[]= clone $servicoCliente;
            $servicoCliente->setServicoPrestado(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ServicoPrestado is new, it will return
     * an empty collection; or if this ServicoPrestado has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ServicoPrestado.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinCliente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('Cliente', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ServicoPrestado is new, it will return
     * an empty collection; or if this ServicoPrestado has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ServicoPrestado.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinContrato(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('Contrato', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ServicoPrestado is new, it will return
     * an empty collection; or if this ServicoPrestado has previously
     * been saved, it will retrieve related ServicoClientes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ServicoPrestado.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoCliente[] List of ChildServicoCliente objects
     */
    public function getServicoClientesJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoClienteQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getServicoClientes($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aPlanos) {
            $this->aPlanos->removeServicoPrestado($this);
        }
        if (null !== $this->aTipoServicoPrestado) {
            $this->aTipoServicoPrestado->removeServicoPrestado($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeServicoPrestado($this);
        }
        $this->idservico_prestado = null;
        $this->nome = null;
        $this->codigo = null;
        $this->download = null;
        $this->upload = null;
        $this->franquia = null;
        $this->tecnologia = null;
        $this->garantia_banda = null;
        $this->ativo = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->pre_pago = null;
        $this->plano_id = null;
        $this->tipo = null;
        $this->tipo_servico_prestado_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collServicoClientes) {
                foreach ($this->collServicoClientes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collServicoClientes = null;
        $this->aPlanos = null;
        $this->aTipoServicoPrestado = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ServicoPrestadoTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
