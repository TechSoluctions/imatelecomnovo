<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Cliente as ChildCliente;
use ImaTelecomBundle\Model\ClienteQuery as ChildClienteQuery;
use ImaTelecomBundle\Model\Map\ClienteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'cliente' table.
 *
 *
 *
 * @method     ChildClienteQuery orderByIdcliente($order = Criteria::ASC) Order by the idcliente column
 * @method     ChildClienteQuery orderByPessoaId($order = Criteria::ASC) Order by the pessoa_id column
 * @method     ChildClienteQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildClienteQuery orderByDataContrato($order = Criteria::ASC) Order by the data_contrato column
 * @method     ChildClienteQuery orderByDataCancelado($order = Criteria::ASC) Order by the data_cancelado column
 * @method     ChildClienteQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildClienteQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildClienteQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildClienteQuery orderByImportId($order = Criteria::ASC) Order by the import_id column
 *
 * @method     ChildClienteQuery groupByIdcliente() Group by the idcliente column
 * @method     ChildClienteQuery groupByPessoaId() Group by the pessoa_id column
 * @method     ChildClienteQuery groupByAtivo() Group by the ativo column
 * @method     ChildClienteQuery groupByDataContrato() Group by the data_contrato column
 * @method     ChildClienteQuery groupByDataCancelado() Group by the data_cancelado column
 * @method     ChildClienteQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildClienteQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildClienteQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildClienteQuery groupByImportId() Group by the import_id column
 *
 * @method     ChildClienteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildClienteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildClienteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildClienteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildClienteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildClienteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildClienteQuery leftJoinPessoa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pessoa relation
 * @method     ChildClienteQuery rightJoinPessoa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pessoa relation
 * @method     ChildClienteQuery innerJoinPessoa($relationAlias = null) Adds a INNER JOIN clause to the query using the Pessoa relation
 *
 * @method     ChildClienteQuery joinWithPessoa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pessoa relation
 *
 * @method     ChildClienteQuery leftJoinWithPessoa() Adds a LEFT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildClienteQuery rightJoinWithPessoa() Adds a RIGHT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildClienteQuery innerJoinWithPessoa() Adds a INNER JOIN clause and with to the query using the Pessoa relation
 *
 * @method     ChildClienteQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildClienteQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildClienteQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildClienteQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildClienteQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildClienteQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildClienteQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildClienteQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildClienteQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildClienteQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildClienteQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildClienteQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildClienteQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildClienteQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildClienteQuery leftJoinClienteEstoqueItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the ClienteEstoqueItem relation
 * @method     ChildClienteQuery rightJoinClienteEstoqueItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ClienteEstoqueItem relation
 * @method     ChildClienteQuery innerJoinClienteEstoqueItem($relationAlias = null) Adds a INNER JOIN clause to the query using the ClienteEstoqueItem relation
 *
 * @method     ChildClienteQuery joinWithClienteEstoqueItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ClienteEstoqueItem relation
 *
 * @method     ChildClienteQuery leftJoinWithClienteEstoqueItem() Adds a LEFT JOIN clause and with to the query using the ClienteEstoqueItem relation
 * @method     ChildClienteQuery rightJoinWithClienteEstoqueItem() Adds a RIGHT JOIN clause and with to the query using the ClienteEstoqueItem relation
 * @method     ChildClienteQuery innerJoinWithClienteEstoqueItem() Adds a INNER JOIN clause and with to the query using the ClienteEstoqueItem relation
 *
 * @method     ChildClienteQuery leftJoinEnderecoCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the EnderecoCliente relation
 * @method     ChildClienteQuery rightJoinEnderecoCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EnderecoCliente relation
 * @method     ChildClienteQuery innerJoinEnderecoCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the EnderecoCliente relation
 *
 * @method     ChildClienteQuery joinWithEnderecoCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EnderecoCliente relation
 *
 * @method     ChildClienteQuery leftJoinWithEnderecoCliente() Adds a LEFT JOIN clause and with to the query using the EnderecoCliente relation
 * @method     ChildClienteQuery rightJoinWithEnderecoCliente() Adds a RIGHT JOIN clause and with to the query using the EnderecoCliente relation
 * @method     ChildClienteQuery innerJoinWithEnderecoCliente() Adds a INNER JOIN clause and with to the query using the EnderecoCliente relation
 *
 * @method     ChildClienteQuery leftJoinEstoqueLancamento($relationAlias = null) Adds a LEFT JOIN clause to the query using the EstoqueLancamento relation
 * @method     ChildClienteQuery rightJoinEstoqueLancamento($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EstoqueLancamento relation
 * @method     ChildClienteQuery innerJoinEstoqueLancamento($relationAlias = null) Adds a INNER JOIN clause to the query using the EstoqueLancamento relation
 *
 * @method     ChildClienteQuery joinWithEstoqueLancamento($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EstoqueLancamento relation
 *
 * @method     ChildClienteQuery leftJoinWithEstoqueLancamento() Adds a LEFT JOIN clause and with to the query using the EstoqueLancamento relation
 * @method     ChildClienteQuery rightJoinWithEstoqueLancamento() Adds a RIGHT JOIN clause and with to the query using the EstoqueLancamento relation
 * @method     ChildClienteQuery innerJoinWithEstoqueLancamento() Adds a INNER JOIN clause and with to the query using the EstoqueLancamento relation
 *
 * @method     ChildClienteQuery leftJoinFornecedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fornecedor relation
 * @method     ChildClienteQuery rightJoinFornecedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fornecedor relation
 * @method     ChildClienteQuery innerJoinFornecedor($relationAlias = null) Adds a INNER JOIN clause to the query using the Fornecedor relation
 *
 * @method     ChildClienteQuery joinWithFornecedor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fornecedor relation
 *
 * @method     ChildClienteQuery leftJoinWithFornecedor() Adds a LEFT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildClienteQuery rightJoinWithFornecedor() Adds a RIGHT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildClienteQuery innerJoinWithFornecedor() Adds a INNER JOIN clause and with to the query using the Fornecedor relation
 *
 * @method     ChildClienteQuery leftJoinServicoCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildClienteQuery rightJoinServicoCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ServicoCliente relation
 * @method     ChildClienteQuery innerJoinServicoCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the ServicoCliente relation
 *
 * @method     ChildClienteQuery joinWithServicoCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ServicoCliente relation
 *
 * @method     ChildClienteQuery leftJoinWithServicoCliente() Adds a LEFT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildClienteQuery rightJoinWithServicoCliente() Adds a RIGHT JOIN clause and with to the query using the ServicoCliente relation
 * @method     ChildClienteQuery innerJoinWithServicoCliente() Adds a INNER JOIN clause and with to the query using the ServicoCliente relation
 *
 * @method     \ImaTelecomBundle\Model\PessoaQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\ClienteEstoqueItemQuery|\ImaTelecomBundle\Model\EnderecoClienteQuery|\ImaTelecomBundle\Model\EstoqueLancamentoQuery|\ImaTelecomBundle\Model\FornecedorQuery|\ImaTelecomBundle\Model\ServicoClienteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCliente findOne(ConnectionInterface $con = null) Return the first ChildCliente matching the query
 * @method     ChildCliente findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCliente matching the query, or a new ChildCliente object populated from the query conditions when no match is found
 *
 * @method     ChildCliente findOneByIdcliente(int $idcliente) Return the first ChildCliente filtered by the idcliente column
 * @method     ChildCliente findOneByPessoaId(int $pessoa_id) Return the first ChildCliente filtered by the pessoa_id column
 * @method     ChildCliente findOneByAtivo(boolean $ativo) Return the first ChildCliente filtered by the ativo column
 * @method     ChildCliente findOneByDataContrato(string $data_contrato) Return the first ChildCliente filtered by the data_contrato column
 * @method     ChildCliente findOneByDataCancelado(string $data_cancelado) Return the first ChildCliente filtered by the data_cancelado column
 * @method     ChildCliente findOneByDataCadastro(string $data_cadastro) Return the first ChildCliente filtered by the data_cadastro column
 * @method     ChildCliente findOneByDataAlterado(string $data_alterado) Return the first ChildCliente filtered by the data_alterado column
 * @method     ChildCliente findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildCliente filtered by the usuario_alterado column
 * @method     ChildCliente findOneByImportId(int $import_id) Return the first ChildCliente filtered by the import_id column *

 * @method     ChildCliente requirePk($key, ConnectionInterface $con = null) Return the ChildCliente by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCliente requireOne(ConnectionInterface $con = null) Return the first ChildCliente matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCliente requireOneByIdcliente(int $idcliente) Return the first ChildCliente filtered by the idcliente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCliente requireOneByPessoaId(int $pessoa_id) Return the first ChildCliente filtered by the pessoa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCliente requireOneByAtivo(boolean $ativo) Return the first ChildCliente filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCliente requireOneByDataContrato(string $data_contrato) Return the first ChildCliente filtered by the data_contrato column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCliente requireOneByDataCancelado(string $data_cancelado) Return the first ChildCliente filtered by the data_cancelado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCliente requireOneByDataCadastro(string $data_cadastro) Return the first ChildCliente filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCliente requireOneByDataAlterado(string $data_alterado) Return the first ChildCliente filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCliente requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildCliente filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCliente requireOneByImportId(int $import_id) Return the first ChildCliente filtered by the import_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCliente[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCliente objects based on current ModelCriteria
 * @method     ChildCliente[]|ObjectCollection findByIdcliente(int $idcliente) Return ChildCliente objects filtered by the idcliente column
 * @method     ChildCliente[]|ObjectCollection findByPessoaId(int $pessoa_id) Return ChildCliente objects filtered by the pessoa_id column
 * @method     ChildCliente[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildCliente objects filtered by the ativo column
 * @method     ChildCliente[]|ObjectCollection findByDataContrato(string $data_contrato) Return ChildCliente objects filtered by the data_contrato column
 * @method     ChildCliente[]|ObjectCollection findByDataCancelado(string $data_cancelado) Return ChildCliente objects filtered by the data_cancelado column
 * @method     ChildCliente[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildCliente objects filtered by the data_cadastro column
 * @method     ChildCliente[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildCliente objects filtered by the data_alterado column
 * @method     ChildCliente[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildCliente objects filtered by the usuario_alterado column
 * @method     ChildCliente[]|ObjectCollection findByImportId(int $import_id) Return ChildCliente objects filtered by the import_id column
 * @method     ChildCliente[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ClienteQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ClienteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Cliente', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildClienteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildClienteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildClienteQuery) {
            return $criteria;
        }
        $query = new ChildClienteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCliente|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ClienteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ClienteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCliente A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcliente, pessoa_id, ativo, data_contrato, data_cancelado, data_cadastro, data_alterado, usuario_alterado, import_id FROM cliente WHERE idcliente = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCliente $obj */
            $obj = new ChildCliente();
            $obj->hydrate($row);
            ClienteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCliente|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcliente column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcliente(1234); // WHERE idcliente = 1234
     * $query->filterByIdcliente(array(12, 34)); // WHERE idcliente IN (12, 34)
     * $query->filterByIdcliente(array('min' => 12)); // WHERE idcliente > 12
     * </code>
     *
     * @param     mixed $idcliente The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByIdcliente($idcliente = null, $comparison = null)
    {
        if (is_array($idcliente)) {
            $useMinMax = false;
            if (isset($idcliente['min'])) {
                $this->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $idcliente['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcliente['max'])) {
                $this->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $idcliente['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $idcliente, $comparison);
    }

    /**
     * Filter the query on the pessoa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPessoaId(1234); // WHERE pessoa_id = 1234
     * $query->filterByPessoaId(array(12, 34)); // WHERE pessoa_id IN (12, 34)
     * $query->filterByPessoaId(array('min' => 12)); // WHERE pessoa_id > 12
     * </code>
     *
     * @see       filterByPessoa()
     *
     * @param     mixed $pessoaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByPessoaId($pessoaId = null, $comparison = null)
    {
        if (is_array($pessoaId)) {
            $useMinMax = false;
            if (isset($pessoaId['min'])) {
                $this->addUsingAlias(ClienteTableMap::COL_PESSOA_ID, $pessoaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pessoaId['max'])) {
                $this->addUsingAlias(ClienteTableMap::COL_PESSOA_ID, $pessoaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteTableMap::COL_PESSOA_ID, $pessoaId, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ClienteTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the data_contrato column
     *
     * Example usage:
     * <code>
     * $query->filterByDataContrato('2011-03-14'); // WHERE data_contrato = '2011-03-14'
     * $query->filterByDataContrato('now'); // WHERE data_contrato = '2011-03-14'
     * $query->filterByDataContrato(array('max' => 'yesterday')); // WHERE data_contrato > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataContrato The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByDataContrato($dataContrato = null, $comparison = null)
    {
        if (is_array($dataContrato)) {
            $useMinMax = false;
            if (isset($dataContrato['min'])) {
                $this->addUsingAlias(ClienteTableMap::COL_DATA_CONTRATO, $dataContrato['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataContrato['max'])) {
                $this->addUsingAlias(ClienteTableMap::COL_DATA_CONTRATO, $dataContrato['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteTableMap::COL_DATA_CONTRATO, $dataContrato, $comparison);
    }

    /**
     * Filter the query on the data_cancelado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCancelado('2011-03-14'); // WHERE data_cancelado = '2011-03-14'
     * $query->filterByDataCancelado('now'); // WHERE data_cancelado = '2011-03-14'
     * $query->filterByDataCancelado(array('max' => 'yesterday')); // WHERE data_cancelado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCancelado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByDataCancelado($dataCancelado = null, $comparison = null)
    {
        if (is_array($dataCancelado)) {
            $useMinMax = false;
            if (isset($dataCancelado['min'])) {
                $this->addUsingAlias(ClienteTableMap::COL_DATA_CANCELADO, $dataCancelado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCancelado['max'])) {
                $this->addUsingAlias(ClienteTableMap::COL_DATA_CANCELADO, $dataCancelado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteTableMap::COL_DATA_CANCELADO, $dataCancelado, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ClienteTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ClienteTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ClienteTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ClienteTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ClienteTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ClienteTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the import_id column
     *
     * Example usage:
     * <code>
     * $query->filterByImportId(1234); // WHERE import_id = 1234
     * $query->filterByImportId(array(12, 34)); // WHERE import_id IN (12, 34)
     * $query->filterByImportId(array('min' => 12)); // WHERE import_id > 12
     * </code>
     *
     * @param     mixed $importId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function filterByImportId($importId = null, $comparison = null)
    {
        if (is_array($importId)) {
            $useMinMax = false;
            if (isset($importId['min'])) {
                $this->addUsingAlias(ClienteTableMap::COL_IMPORT_ID, $importId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($importId['max'])) {
                $this->addUsingAlias(ClienteTableMap::COL_IMPORT_ID, $importId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ClienteTableMap::COL_IMPORT_ID, $importId, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Pessoa object
     *
     * @param \ImaTelecomBundle\Model\Pessoa|ObjectCollection $pessoa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClienteQuery The current query, for fluid interface
     */
    public function filterByPessoa($pessoa, $comparison = null)
    {
        if ($pessoa instanceof \ImaTelecomBundle\Model\Pessoa) {
            return $this
                ->addUsingAlias(ClienteTableMap::COL_PESSOA_ID, $pessoa->getId(), $comparison);
        } elseif ($pessoa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ClienteTableMap::COL_PESSOA_ID, $pessoa->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPessoa() only accepts arguments of type \ImaTelecomBundle\Model\Pessoa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pessoa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function joinPessoa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pessoa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pessoa');
        }

        return $this;
    }

    /**
     * Use the Pessoa relation Pessoa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\PessoaQuery A secondary query class using the current class as primary query
     */
    public function usePessoaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPessoa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pessoa', '\ImaTelecomBundle\Model\PessoaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildClienteQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(ClienteTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ClienteTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildClienteQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $boleto->getClienteId(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            return $this
                ->useBoletoQuery()
                ->filterByPrimaryKeys($boleto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ClienteEstoqueItem object
     *
     * @param \ImaTelecomBundle\Model\ClienteEstoqueItem|ObjectCollection $clienteEstoqueItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildClienteQuery The current query, for fluid interface
     */
    public function filterByClienteEstoqueItem($clienteEstoqueItem, $comparison = null)
    {
        if ($clienteEstoqueItem instanceof \ImaTelecomBundle\Model\ClienteEstoqueItem) {
            return $this
                ->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $clienteEstoqueItem->getClienteId(), $comparison);
        } elseif ($clienteEstoqueItem instanceof ObjectCollection) {
            return $this
                ->useClienteEstoqueItemQuery()
                ->filterByPrimaryKeys($clienteEstoqueItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByClienteEstoqueItem() only accepts arguments of type \ImaTelecomBundle\Model\ClienteEstoqueItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ClienteEstoqueItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function joinClienteEstoqueItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ClienteEstoqueItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ClienteEstoqueItem');
        }

        return $this;
    }

    /**
     * Use the ClienteEstoqueItem relation ClienteEstoqueItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteEstoqueItemQuery A secondary query class using the current class as primary query
     */
    public function useClienteEstoqueItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinClienteEstoqueItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ClienteEstoqueItem', '\ImaTelecomBundle\Model\ClienteEstoqueItemQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EnderecoCliente object
     *
     * @param \ImaTelecomBundle\Model\EnderecoCliente|ObjectCollection $enderecoCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildClienteQuery The current query, for fluid interface
     */
    public function filterByEnderecoCliente($enderecoCliente, $comparison = null)
    {
        if ($enderecoCliente instanceof \ImaTelecomBundle\Model\EnderecoCliente) {
            return $this
                ->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $enderecoCliente->getClienteId(), $comparison);
        } elseif ($enderecoCliente instanceof ObjectCollection) {
            return $this
                ->useEnderecoClienteQuery()
                ->filterByPrimaryKeys($enderecoCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEnderecoCliente() only accepts arguments of type \ImaTelecomBundle\Model\EnderecoCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EnderecoCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function joinEnderecoCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EnderecoCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EnderecoCliente');
        }

        return $this;
    }

    /**
     * Use the EnderecoCliente relation EnderecoCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EnderecoClienteQuery A secondary query class using the current class as primary query
     */
    public function useEnderecoClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEnderecoCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EnderecoCliente', '\ImaTelecomBundle\Model\EnderecoClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EstoqueLancamento object
     *
     * @param \ImaTelecomBundle\Model\EstoqueLancamento|ObjectCollection $estoqueLancamento the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildClienteQuery The current query, for fluid interface
     */
    public function filterByEstoqueLancamento($estoqueLancamento, $comparison = null)
    {
        if ($estoqueLancamento instanceof \ImaTelecomBundle\Model\EstoqueLancamento) {
            return $this
                ->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $estoqueLancamento->getClienteId(), $comparison);
        } elseif ($estoqueLancamento instanceof ObjectCollection) {
            return $this
                ->useEstoqueLancamentoQuery()
                ->filterByPrimaryKeys($estoqueLancamento->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEstoqueLancamento() only accepts arguments of type \ImaTelecomBundle\Model\EstoqueLancamento or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EstoqueLancamento relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function joinEstoqueLancamento($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EstoqueLancamento');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EstoqueLancamento');
        }

        return $this;
    }

    /**
     * Use the EstoqueLancamento relation EstoqueLancamento object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueLancamentoQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueLancamentoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinEstoqueLancamento($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EstoqueLancamento', '\ImaTelecomBundle\Model\EstoqueLancamentoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Fornecedor object
     *
     * @param \ImaTelecomBundle\Model\Fornecedor|ObjectCollection $fornecedor the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildClienteQuery The current query, for fluid interface
     */
    public function filterByFornecedor($fornecedor, $comparison = null)
    {
        if ($fornecedor instanceof \ImaTelecomBundle\Model\Fornecedor) {
            return $this
                ->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $fornecedor->getClienteId(), $comparison);
        } elseif ($fornecedor instanceof ObjectCollection) {
            return $this
                ->useFornecedorQuery()
                ->filterByPrimaryKeys($fornecedor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFornecedor() only accepts arguments of type \ImaTelecomBundle\Model\Fornecedor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fornecedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function joinFornecedor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fornecedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fornecedor');
        }

        return $this;
    }

    /**
     * Use the Fornecedor relation Fornecedor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FornecedorQuery A secondary query class using the current class as primary query
     */
    public function useFornecedorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFornecedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fornecedor', '\ImaTelecomBundle\Model\FornecedorQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ServicoCliente object
     *
     * @param \ImaTelecomBundle\Model\ServicoCliente|ObjectCollection $servicoCliente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildClienteQuery The current query, for fluid interface
     */
    public function filterByServicoCliente($servicoCliente, $comparison = null)
    {
        if ($servicoCliente instanceof \ImaTelecomBundle\Model\ServicoCliente) {
            return $this
                ->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $servicoCliente->getClienteId(), $comparison);
        } elseif ($servicoCliente instanceof ObjectCollection) {
            return $this
                ->useServicoClienteQuery()
                ->filterByPrimaryKeys($servicoCliente->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByServicoCliente() only accepts arguments of type \ImaTelecomBundle\Model\ServicoCliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ServicoCliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function joinServicoCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ServicoCliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ServicoCliente');
        }

        return $this;
    }

    /**
     * Use the ServicoCliente relation ServicoCliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ServicoClienteQuery A secondary query class using the current class as primary query
     */
    public function useServicoClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinServicoCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ServicoCliente', '\ImaTelecomBundle\Model\ServicoClienteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCliente $cliente Object to remove from the list of results
     *
     * @return $this|ChildClienteQuery The current query, for fluid interface
     */
    public function prune($cliente = null)
    {
        if ($cliente) {
            $this->addUsingAlias(ClienteTableMap::COL_IDCLIENTE, $cliente->getIdcliente(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the cliente table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClienteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ClienteTableMap::clearInstancePool();
            ClienteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClienteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ClienteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ClienteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ClienteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ClienteQuery
