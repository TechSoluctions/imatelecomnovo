<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BoletoBaixaHistorico as ChildBoletoBaixaHistorico;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery as ChildBoletoBaixaHistoricoQuery;
use ImaTelecomBundle\Model\Competencia as ChildCompetencia;
use ImaTelecomBundle\Model\CompetenciaQuery as ChildCompetenciaQuery;
use ImaTelecomBundle\Model\Convenio as ChildConvenio;
use ImaTelecomBundle\Model\ConvenioQuery as ChildConvenioQuery;
use ImaTelecomBundle\Model\Lancamentos as ChildLancamentos;
use ImaTelecomBundle\Model\LancamentosBoletos as ChildLancamentosBoletos;
use ImaTelecomBundle\Model\LancamentosBoletosQuery as ChildLancamentosBoletosQuery;
use ImaTelecomBundle\Model\LancamentosQuery as ChildLancamentosQuery;
use ImaTelecomBundle\Model\Map\BoletoBaixaHistoricoTableMap;
use ImaTelecomBundle\Model\Map\LancamentosBoletosTableMap;
use ImaTelecomBundle\Model\Map\LancamentosTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'lancamentos' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Lancamentos implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\LancamentosTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idlancamento field.
     *
     * @var        int
     */
    protected $idlancamento;

    /**
     * The value for the conteudo_arquivo_saida field.
     *
     * @var        string
     */
    protected $conteudo_arquivo_saida;

    /**
     * The value for the competencia_id field.
     *
     * @var        int
     */
    protected $competencia_id;

    /**
     * The value for the data_geracao field.
     *
     * @var        string
     */
    protected $data_geracao;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * The value for the convenio_id field.
     *
     * @var        int
     */
    protected $convenio_id;

    /**
     * The value for the sequencial_arquivo_cobranca field.
     *
     * @var        int
     */
    protected $sequencial_arquivo_cobranca;

    /**
     * @var        ChildCompetencia
     */
    protected $aCompetencia;

    /**
     * @var        ChildConvenio
     */
    protected $aConvenio;

    /**
     * @var        ObjectCollection|ChildBoletoBaixaHistorico[] Collection to store aggregation of ChildBoletoBaixaHistorico objects.
     */
    protected $collBoletoBaixaHistoricos;
    protected $collBoletoBaixaHistoricosPartial;

    /**
     * @var        ObjectCollection|ChildLancamentosBoletos[] Collection to store aggregation of ChildLancamentosBoletos objects.
     */
    protected $collLancamentosBoletoss;
    protected $collLancamentosBoletossPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBoletoBaixaHistorico[]
     */
    protected $boletoBaixaHistoricosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLancamentosBoletos[]
     */
    protected $lancamentosBoletossScheduledForDeletion = null;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Lancamentos object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Lancamentos</code> instance.  If
     * <code>obj</code> is an instance of <code>Lancamentos</code>, delegates to
     * <code>equals(Lancamentos)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Lancamentos The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idlancamento] column value.
     *
     * @return int
     */
    public function getIdlancamento()
    {
        return $this->idlancamento;
    }

    /**
     * Get the [conteudo_arquivo_saida] column value.
     *
     * @return string
     */
    public function getConteudoArquivoSaida()
    {
        return $this->conteudo_arquivo_saida;
    }

    /**
     * Get the [competencia_id] column value.
     *
     * @return int
     */
    public function getCompetenciaId()
    {
        return $this->competencia_id;
    }

    /**
     * Get the [data_geracao] column value.
     *
     * @return string
     */
    public function getDataGeracao()
    {
        return $this->data_geracao;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Get the [convenio_id] column value.
     *
     * @return int
     */
    public function getConvenioId()
    {
        return $this->convenio_id;
    }

    /**
     * Get the [sequencial_arquivo_cobranca] column value.
     *
     * @return int
     */
    public function getSequencialArquivoCobranca()
    {
        return $this->sequencial_arquivo_cobranca;
    }

    /**
     * Set the value of [idlancamento] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function setIdlancamento($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idlancamento !== $v) {
            $this->idlancamento = $v;
            $this->modifiedColumns[LancamentosTableMap::COL_IDLANCAMENTO] = true;
        }

        return $this;
    } // setIdlancamento()

    /**
     * Set the value of [conteudo_arquivo_saida] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function setConteudoArquivoSaida($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->conteudo_arquivo_saida !== $v) {
            $this->conteudo_arquivo_saida = $v;
            $this->modifiedColumns[LancamentosTableMap::COL_CONTEUDO_ARQUIVO_SAIDA] = true;
        }

        return $this;
    } // setConteudoArquivoSaida()

    /**
     * Set the value of [competencia_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function setCompetenciaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->competencia_id !== $v) {
            $this->competencia_id = $v;
            $this->modifiedColumns[LancamentosTableMap::COL_COMPETENCIA_ID] = true;
        }

        if ($this->aCompetencia !== null && $this->aCompetencia->getId() !== $v) {
            $this->aCompetencia = null;
        }

        return $this;
    } // setCompetenciaId()

    /**
     * Set the value of [data_geracao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function setDataGeracao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->data_geracao !== $v) {
            $this->data_geracao = $v;
            $this->modifiedColumns[LancamentosTableMap::COL_DATA_GERACAO] = true;
        }

        return $this;
    } // setDataGeracao()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[LancamentosTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[LancamentosTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[LancamentosTableMap::COL_USUARIO_ALTERADO] = true;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Set the value of [convenio_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function setConvenioId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->convenio_id !== $v) {
            $this->convenio_id = $v;
            $this->modifiedColumns[LancamentosTableMap::COL_CONVENIO_ID] = true;
        }

        if ($this->aConvenio !== null && $this->aConvenio->getIdconvenio() !== $v) {
            $this->aConvenio = null;
        }

        return $this;
    } // setConvenioId()

    /**
     * Set the value of [sequencial_arquivo_cobranca] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function setSequencialArquivoCobranca($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sequencial_arquivo_cobranca !== $v) {
            $this->sequencial_arquivo_cobranca = $v;
            $this->modifiedColumns[LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA] = true;
        }

        return $this;
    } // setSequencialArquivoCobranca()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : LancamentosTableMap::translateFieldName('Idlancamento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idlancamento = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : LancamentosTableMap::translateFieldName('ConteudoArquivoSaida', TableMap::TYPE_PHPNAME, $indexType)];
            $this->conteudo_arquivo_saida = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : LancamentosTableMap::translateFieldName('CompetenciaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->competencia_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : LancamentosTableMap::translateFieldName('DataGeracao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->data_geracao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : LancamentosTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : LancamentosTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : LancamentosTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : LancamentosTableMap::translateFieldName('ConvenioId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->convenio_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : LancamentosTableMap::translateFieldName('SequencialArquivoCobranca', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sequencial_arquivo_cobranca = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = LancamentosTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Lancamentos'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCompetencia !== null && $this->competencia_id !== $this->aCompetencia->getId()) {
            $this->aCompetencia = null;
        }
        if ($this->aConvenio !== null && $this->convenio_id !== $this->aConvenio->getIdconvenio()) {
            $this->aConvenio = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LancamentosTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildLancamentosQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCompetencia = null;
            $this->aConvenio = null;
            $this->collBoletoBaixaHistoricos = null;

            $this->collLancamentosBoletoss = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Lancamentos::setDeleted()
     * @see Lancamentos::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildLancamentosQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                LancamentosTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCompetencia !== null) {
                if ($this->aCompetencia->isModified() || $this->aCompetencia->isNew()) {
                    $affectedRows += $this->aCompetencia->save($con);
                }
                $this->setCompetencia($this->aCompetencia);
            }

            if ($this->aConvenio !== null) {
                if ($this->aConvenio->isModified() || $this->aConvenio->isNew()) {
                    $affectedRows += $this->aConvenio->save($con);
                }
                $this->setConvenio($this->aConvenio);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->boletoBaixaHistoricosScheduledForDeletion !== null) {
                if (!$this->boletoBaixaHistoricosScheduledForDeletion->isEmpty()) {
                    foreach ($this->boletoBaixaHistoricosScheduledForDeletion as $boletoBaixaHistorico) {
                        // need to save related object because we set the relation to null
                        $boletoBaixaHistorico->save($con);
                    }
                    $this->boletoBaixaHistoricosScheduledForDeletion = null;
                }
            }

            if ($this->collBoletoBaixaHistoricos !== null) {
                foreach ($this->collBoletoBaixaHistoricos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->lancamentosBoletossScheduledForDeletion !== null) {
                if (!$this->lancamentosBoletossScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\LancamentosBoletosQuery::create()
                        ->filterByPrimaryKeys($this->lancamentosBoletossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->lancamentosBoletossScheduledForDeletion = null;
                }
            }

            if ($this->collLancamentosBoletoss !== null) {
                foreach ($this->collLancamentosBoletoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[LancamentosTableMap::COL_IDLANCAMENTO] = true;
        if (null !== $this->idlancamento) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . LancamentosTableMap::COL_IDLANCAMENTO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(LancamentosTableMap::COL_IDLANCAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'idlancamento';
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_CONTEUDO_ARQUIVO_SAIDA)) {
            $modifiedColumns[':p' . $index++]  = 'conteudo_arquivo_saida';
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_COMPETENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'competencia_id';
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_DATA_GERACAO)) {
            $modifiedColumns[':p' . $index++]  = 'data_geracao';
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_CONVENIO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'convenio_id';
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA)) {
            $modifiedColumns[':p' . $index++]  = 'sequencial_arquivo_cobranca';
        }

        $sql = sprintf(
            'INSERT INTO lancamentos (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idlancamento':
                        $stmt->bindValue($identifier, $this->idlancamento, PDO::PARAM_INT);
                        break;
                    case 'conteudo_arquivo_saida':
                        $stmt->bindValue($identifier, $this->conteudo_arquivo_saida, PDO::PARAM_STR);
                        break;
                    case 'competencia_id':
                        $stmt->bindValue($identifier, $this->competencia_id, PDO::PARAM_INT);
                        break;
                    case 'data_geracao':
                        $stmt->bindValue($identifier, $this->data_geracao, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                    case 'convenio_id':
                        $stmt->bindValue($identifier, $this->convenio_id, PDO::PARAM_INT);
                        break;
                    case 'sequencial_arquivo_cobranca':
                        $stmt->bindValue($identifier, $this->sequencial_arquivo_cobranca, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdlancamento($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = LancamentosTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdlancamento();
                break;
            case 1:
                return $this->getConteudoArquivoSaida();
                break;
            case 2:
                return $this->getCompetenciaId();
                break;
            case 3:
                return $this->getDataGeracao();
                break;
            case 4:
                return $this->getDataCadastro();
                break;
            case 5:
                return $this->getDataAlterado();
                break;
            case 6:
                return $this->getUsuarioAlterado();
                break;
            case 7:
                return $this->getConvenioId();
                break;
            case 8:
                return $this->getSequencialArquivoCobranca();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Lancamentos'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Lancamentos'][$this->hashCode()] = true;
        $keys = LancamentosTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdlancamento(),
            $keys[1] => $this->getConteudoArquivoSaida(),
            $keys[2] => $this->getCompetenciaId(),
            $keys[3] => $this->getDataGeracao(),
            $keys[4] => $this->getDataCadastro(),
            $keys[5] => $this->getDataAlterado(),
            $keys[6] => $this->getUsuarioAlterado(),
            $keys[7] => $this->getConvenioId(),
            $keys[8] => $this->getSequencialArquivoCobranca(),
        );
        if ($result[$keys[4]] instanceof \DateTime) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTime) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCompetencia) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'competencia';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'competencia';
                        break;
                    default:
                        $key = 'Competencia';
                }

                $result[$key] = $this->aCompetencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aConvenio) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'convenio';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'convenio';
                        break;
                    default:
                        $key = 'Convenio';
                }

                $result[$key] = $this->aConvenio->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBoletoBaixaHistoricos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boletoBaixaHistoricos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto_baixa_historicos';
                        break;
                    default:
                        $key = 'BoletoBaixaHistoricos';
                }

                $result[$key] = $this->collBoletoBaixaHistoricos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLancamentosBoletoss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lancamentosBoletoss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lancamentos_boletoss';
                        break;
                    default:
                        $key = 'LancamentosBoletoss';
                }

                $result[$key] = $this->collLancamentosBoletoss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Lancamentos
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = LancamentosTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Lancamentos
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdlancamento($value);
                break;
            case 1:
                $this->setConteudoArquivoSaida($value);
                break;
            case 2:
                $this->setCompetenciaId($value);
                break;
            case 3:
                $this->setDataGeracao($value);
                break;
            case 4:
                $this->setDataCadastro($value);
                break;
            case 5:
                $this->setDataAlterado($value);
                break;
            case 6:
                $this->setUsuarioAlterado($value);
                break;
            case 7:
                $this->setConvenioId($value);
                break;
            case 8:
                $this->setSequencialArquivoCobranca($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = LancamentosTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdlancamento($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setConteudoArquivoSaida($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCompetenciaId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDataGeracao($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDataCadastro($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDataAlterado($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setConvenioId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setSequencialArquivoCobranca($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(LancamentosTableMap::DATABASE_NAME);

        if ($this->isColumnModified(LancamentosTableMap::COL_IDLANCAMENTO)) {
            $criteria->add(LancamentosTableMap::COL_IDLANCAMENTO, $this->idlancamento);
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_CONTEUDO_ARQUIVO_SAIDA)) {
            $criteria->add(LancamentosTableMap::COL_CONTEUDO_ARQUIVO_SAIDA, $this->conteudo_arquivo_saida);
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_COMPETENCIA_ID)) {
            $criteria->add(LancamentosTableMap::COL_COMPETENCIA_ID, $this->competencia_id);
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_DATA_GERACAO)) {
            $criteria->add(LancamentosTableMap::COL_DATA_GERACAO, $this->data_geracao);
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(LancamentosTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(LancamentosTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(LancamentosTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_CONVENIO_ID)) {
            $criteria->add(LancamentosTableMap::COL_CONVENIO_ID, $this->convenio_id);
        }
        if ($this->isColumnModified(LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA)) {
            $criteria->add(LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA, $this->sequencial_arquivo_cobranca);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildLancamentosQuery::create();
        $criteria->add(LancamentosTableMap::COL_IDLANCAMENTO, $this->idlancamento);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdlancamento();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdlancamento();
    }

    /**
     * Generic method to set the primary key (idlancamento column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdlancamento($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdlancamento();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Lancamentos (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setConteudoArquivoSaida($this->getConteudoArquivoSaida());
        $copyObj->setCompetenciaId($this->getCompetenciaId());
        $copyObj->setDataGeracao($this->getDataGeracao());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        $copyObj->setConvenioId($this->getConvenioId());
        $copyObj->setSequencialArquivoCobranca($this->getSequencialArquivoCobranca());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBoletoBaixaHistoricos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBoletoBaixaHistorico($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLancamentosBoletoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLancamentosBoletos($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdlancamento(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Lancamentos Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCompetencia object.
     *
     * @param  ChildCompetencia $v
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCompetencia(ChildCompetencia $v = null)
    {
        if ($v === null) {
            $this->setCompetenciaId(NULL);
        } else {
            $this->setCompetenciaId($v->getId());
        }

        $this->aCompetencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCompetencia object, it will not be re-added.
        if ($v !== null) {
            $v->addLancamentos($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCompetencia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCompetencia The associated ChildCompetencia object.
     * @throws PropelException
     */
    public function getCompetencia(ConnectionInterface $con = null)
    {
        if ($this->aCompetencia === null && ($this->competencia_id !== null)) {
            $this->aCompetencia = ChildCompetenciaQuery::create()->findPk($this->competencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCompetencia->addLancamentoss($this);
             */
        }

        return $this->aCompetencia;
    }

    /**
     * Declares an association between this object and a ChildConvenio object.
     *
     * @param  ChildConvenio $v
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     * @throws PropelException
     */
    public function setConvenio(ChildConvenio $v = null)
    {
        if ($v === null) {
            $this->setConvenioId(NULL);
        } else {
            $this->setConvenioId($v->getIdconvenio());
        }

        $this->aConvenio = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildConvenio object, it will not be re-added.
        if ($v !== null) {
            $v->addLancamentos($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildConvenio object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildConvenio The associated ChildConvenio object.
     * @throws PropelException
     */
    public function getConvenio(ConnectionInterface $con = null)
    {
        if ($this->aConvenio === null && ($this->convenio_id !== null)) {
            $this->aConvenio = ChildConvenioQuery::create()->findPk($this->convenio_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aConvenio->addLancamentoss($this);
             */
        }

        return $this->aConvenio;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('BoletoBaixaHistorico' == $relationName) {
            return $this->initBoletoBaixaHistoricos();
        }
        if ('LancamentosBoletos' == $relationName) {
            return $this->initLancamentosBoletoss();
        }
    }

    /**
     * Clears out the collBoletoBaixaHistoricos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBoletoBaixaHistoricos()
     */
    public function clearBoletoBaixaHistoricos()
    {
        $this->collBoletoBaixaHistoricos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBoletoBaixaHistoricos collection loaded partially.
     */
    public function resetPartialBoletoBaixaHistoricos($v = true)
    {
        $this->collBoletoBaixaHistoricosPartial = $v;
    }

    /**
     * Initializes the collBoletoBaixaHistoricos collection.
     *
     * By default this just sets the collBoletoBaixaHistoricos collection to an empty array (like clearcollBoletoBaixaHistoricos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBoletoBaixaHistoricos($overrideExisting = true)
    {
        if (null !== $this->collBoletoBaixaHistoricos && !$overrideExisting) {
            return;
        }

        $collectionClassName = BoletoBaixaHistoricoTableMap::getTableMap()->getCollectionClassName();

        $this->collBoletoBaixaHistoricos = new $collectionClassName;
        $this->collBoletoBaixaHistoricos->setModel('\ImaTelecomBundle\Model\BoletoBaixaHistorico');
    }

    /**
     * Gets an array of ChildBoletoBaixaHistorico objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLancamentos is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     * @throws PropelException
     */
    public function getBoletoBaixaHistoricos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                // return empty collection
                $this->initBoletoBaixaHistoricos();
            } else {
                $collBoletoBaixaHistoricos = ChildBoletoBaixaHistoricoQuery::create(null, $criteria)
                    ->filterByLancamentos($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBoletoBaixaHistoricosPartial && count($collBoletoBaixaHistoricos)) {
                        $this->initBoletoBaixaHistoricos(false);

                        foreach ($collBoletoBaixaHistoricos as $obj) {
                            if (false == $this->collBoletoBaixaHistoricos->contains($obj)) {
                                $this->collBoletoBaixaHistoricos->append($obj);
                            }
                        }

                        $this->collBoletoBaixaHistoricosPartial = true;
                    }

                    return $collBoletoBaixaHistoricos;
                }

                if ($partial && $this->collBoletoBaixaHistoricos) {
                    foreach ($this->collBoletoBaixaHistoricos as $obj) {
                        if ($obj->isNew()) {
                            $collBoletoBaixaHistoricos[] = $obj;
                        }
                    }
                }

                $this->collBoletoBaixaHistoricos = $collBoletoBaixaHistoricos;
                $this->collBoletoBaixaHistoricosPartial = false;
            }
        }

        return $this->collBoletoBaixaHistoricos;
    }

    /**
     * Sets a collection of ChildBoletoBaixaHistorico objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $boletoBaixaHistoricos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLancamentos The current object (for fluent API support)
     */
    public function setBoletoBaixaHistoricos(Collection $boletoBaixaHistoricos, ConnectionInterface $con = null)
    {
        /** @var ChildBoletoBaixaHistorico[] $boletoBaixaHistoricosToDelete */
        $boletoBaixaHistoricosToDelete = $this->getBoletoBaixaHistoricos(new Criteria(), $con)->diff($boletoBaixaHistoricos);


        $this->boletoBaixaHistoricosScheduledForDeletion = $boletoBaixaHistoricosToDelete;

        foreach ($boletoBaixaHistoricosToDelete as $boletoBaixaHistoricoRemoved) {
            $boletoBaixaHistoricoRemoved->setLancamentos(null);
        }

        $this->collBoletoBaixaHistoricos = null;
        foreach ($boletoBaixaHistoricos as $boletoBaixaHistorico) {
            $this->addBoletoBaixaHistorico($boletoBaixaHistorico);
        }

        $this->collBoletoBaixaHistoricos = $boletoBaixaHistoricos;
        $this->collBoletoBaixaHistoricosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BoletoBaixaHistorico objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BoletoBaixaHistorico objects.
     * @throws PropelException
     */
    public function countBoletoBaixaHistoricos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBoletoBaixaHistoricosPartial && !$this->isNew();
        if (null === $this->collBoletoBaixaHistoricos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBoletoBaixaHistoricos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBoletoBaixaHistoricos());
            }

            $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLancamentos($this)
                ->count($con);
        }

        return count($this->collBoletoBaixaHistoricos);
    }

    /**
     * Method called to associate a ChildBoletoBaixaHistorico object to this object
     * through the ChildBoletoBaixaHistorico foreign key attribute.
     *
     * @param  ChildBoletoBaixaHistorico $l ChildBoletoBaixaHistorico
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function addBoletoBaixaHistorico(ChildBoletoBaixaHistorico $l)
    {
        if ($this->collBoletoBaixaHistoricos === null) {
            $this->initBoletoBaixaHistoricos();
            $this->collBoletoBaixaHistoricosPartial = true;
        }

        if (!$this->collBoletoBaixaHistoricos->contains($l)) {
            $this->doAddBoletoBaixaHistorico($l);

            if ($this->boletoBaixaHistoricosScheduledForDeletion and $this->boletoBaixaHistoricosScheduledForDeletion->contains($l)) {
                $this->boletoBaixaHistoricosScheduledForDeletion->remove($this->boletoBaixaHistoricosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to add.
     */
    protected function doAddBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        $this->collBoletoBaixaHistoricos[]= $boletoBaixaHistorico;
        $boletoBaixaHistorico->setLancamentos($this);
    }

    /**
     * @param  ChildBoletoBaixaHistorico $boletoBaixaHistorico The ChildBoletoBaixaHistorico object to remove.
     * @return $this|ChildLancamentos The current object (for fluent API support)
     */
    public function removeBoletoBaixaHistorico(ChildBoletoBaixaHistorico $boletoBaixaHistorico)
    {
        if ($this->getBoletoBaixaHistoricos()->contains($boletoBaixaHistorico)) {
            $pos = $this->collBoletoBaixaHistoricos->search($boletoBaixaHistorico);
            $this->collBoletoBaixaHistoricos->remove($pos);
            if (null === $this->boletoBaixaHistoricosScheduledForDeletion) {
                $this->boletoBaixaHistoricosScheduledForDeletion = clone $this->collBoletoBaixaHistoricos;
                $this->boletoBaixaHistoricosScheduledForDeletion->clear();
            }
            $this->boletoBaixaHistoricosScheduledForDeletion[]= $boletoBaixaHistorico;
            $boletoBaixaHistorico->setLancamentos(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Lancamentos is new, it will return
     * an empty collection; or if this Lancamentos has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Lancamentos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixaEstorno(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('BaixaEstorno', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Lancamentos is new, it will return
     * an empty collection; or if this Lancamentos has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Lancamentos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBaixa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Baixa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Lancamentos is new, it will return
     * an empty collection; or if this Lancamentos has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Lancamentos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Lancamentos is new, it will return
     * an empty collection; or if this Lancamentos has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Lancamentos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Lancamentos is new, it will return
     * an empty collection; or if this Lancamentos has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Lancamentos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinPessoa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Pessoa', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Lancamentos is new, it will return
     * an empty collection; or if this Lancamentos has previously
     * been saved, it will retrieve related BoletoBaixaHistoricos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Lancamentos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBoletoBaixaHistorico[] List of ChildBoletoBaixaHistorico objects
     */
    public function getBoletoBaixaHistoricosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBoletoBaixaHistoricoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBoletoBaixaHistoricos($query, $con);
    }

    /**
     * Clears out the collLancamentosBoletoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLancamentosBoletoss()
     */
    public function clearLancamentosBoletoss()
    {
        $this->collLancamentosBoletoss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLancamentosBoletoss collection loaded partially.
     */
    public function resetPartialLancamentosBoletoss($v = true)
    {
        $this->collLancamentosBoletossPartial = $v;
    }

    /**
     * Initializes the collLancamentosBoletoss collection.
     *
     * By default this just sets the collLancamentosBoletoss collection to an empty array (like clearcollLancamentosBoletoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLancamentosBoletoss($overrideExisting = true)
    {
        if (null !== $this->collLancamentosBoletoss && !$overrideExisting) {
            return;
        }

        $collectionClassName = LancamentosBoletosTableMap::getTableMap()->getCollectionClassName();

        $this->collLancamentosBoletoss = new $collectionClassName;
        $this->collLancamentosBoletoss->setModel('\ImaTelecomBundle\Model\LancamentosBoletos');
    }

    /**
     * Gets an array of ChildLancamentosBoletos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLancamentos is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     * @throws PropelException
     */
    public function getLancamentosBoletoss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentosBoletossPartial && !$this->isNew();
        if (null === $this->collLancamentosBoletoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collLancamentosBoletoss) {
                // return empty collection
                $this->initLancamentosBoletoss();
            } else {
                $collLancamentosBoletoss = ChildLancamentosBoletosQuery::create(null, $criteria)
                    ->filterByLancamentos($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLancamentosBoletossPartial && count($collLancamentosBoletoss)) {
                        $this->initLancamentosBoletoss(false);

                        foreach ($collLancamentosBoletoss as $obj) {
                            if (false == $this->collLancamentosBoletoss->contains($obj)) {
                                $this->collLancamentosBoletoss->append($obj);
                            }
                        }

                        $this->collLancamentosBoletossPartial = true;
                    }

                    return $collLancamentosBoletoss;
                }

                if ($partial && $this->collLancamentosBoletoss) {
                    foreach ($this->collLancamentosBoletoss as $obj) {
                        if ($obj->isNew()) {
                            $collLancamentosBoletoss[] = $obj;
                        }
                    }
                }

                $this->collLancamentosBoletoss = $collLancamentosBoletoss;
                $this->collLancamentosBoletossPartial = false;
            }
        }

        return $this->collLancamentosBoletoss;
    }

    /**
     * Sets a collection of ChildLancamentosBoletos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $lancamentosBoletoss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLancamentos The current object (for fluent API support)
     */
    public function setLancamentosBoletoss(Collection $lancamentosBoletoss, ConnectionInterface $con = null)
    {
        /** @var ChildLancamentosBoletos[] $lancamentosBoletossToDelete */
        $lancamentosBoletossToDelete = $this->getLancamentosBoletoss(new Criteria(), $con)->diff($lancamentosBoletoss);


        $this->lancamentosBoletossScheduledForDeletion = $lancamentosBoletossToDelete;

        foreach ($lancamentosBoletossToDelete as $lancamentosBoletosRemoved) {
            $lancamentosBoletosRemoved->setLancamentos(null);
        }

        $this->collLancamentosBoletoss = null;
        foreach ($lancamentosBoletoss as $lancamentosBoletos) {
            $this->addLancamentosBoletos($lancamentosBoletos);
        }

        $this->collLancamentosBoletoss = $lancamentosBoletoss;
        $this->collLancamentosBoletossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LancamentosBoletos objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related LancamentosBoletos objects.
     * @throws PropelException
     */
    public function countLancamentosBoletoss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLancamentosBoletossPartial && !$this->isNew();
        if (null === $this->collLancamentosBoletoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLancamentosBoletoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLancamentosBoletoss());
            }

            $query = ChildLancamentosBoletosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLancamentos($this)
                ->count($con);
        }

        return count($this->collLancamentosBoletoss);
    }

    /**
     * Method called to associate a ChildLancamentosBoletos object to this object
     * through the ChildLancamentosBoletos foreign key attribute.
     *
     * @param  ChildLancamentosBoletos $l ChildLancamentosBoletos
     * @return $this|\ImaTelecomBundle\Model\Lancamentos The current object (for fluent API support)
     */
    public function addLancamentosBoletos(ChildLancamentosBoletos $l)
    {
        if ($this->collLancamentosBoletoss === null) {
            $this->initLancamentosBoletoss();
            $this->collLancamentosBoletossPartial = true;
        }

        if (!$this->collLancamentosBoletoss->contains($l)) {
            $this->doAddLancamentosBoletos($l);

            if ($this->lancamentosBoletossScheduledForDeletion and $this->lancamentosBoletossScheduledForDeletion->contains($l)) {
                $this->lancamentosBoletossScheduledForDeletion->remove($this->lancamentosBoletossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLancamentosBoletos $lancamentosBoletos The ChildLancamentosBoletos object to add.
     */
    protected function doAddLancamentosBoletos(ChildLancamentosBoletos $lancamentosBoletos)
    {
        $this->collLancamentosBoletoss[]= $lancamentosBoletos;
        $lancamentosBoletos->setLancamentos($this);
    }

    /**
     * @param  ChildLancamentosBoletos $lancamentosBoletos The ChildLancamentosBoletos object to remove.
     * @return $this|ChildLancamentos The current object (for fluent API support)
     */
    public function removeLancamentosBoletos(ChildLancamentosBoletos $lancamentosBoletos)
    {
        if ($this->getLancamentosBoletoss()->contains($lancamentosBoletos)) {
            $pos = $this->collLancamentosBoletoss->search($lancamentosBoletos);
            $this->collLancamentosBoletoss->remove($pos);
            if (null === $this->lancamentosBoletossScheduledForDeletion) {
                $this->lancamentosBoletossScheduledForDeletion = clone $this->collLancamentosBoletoss;
                $this->lancamentosBoletossScheduledForDeletion->clear();
            }
            $this->lancamentosBoletossScheduledForDeletion[]= clone $lancamentosBoletos;
            $lancamentosBoletos->setLancamentos(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Lancamentos is new, it will return
     * an empty collection; or if this Lancamentos has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Lancamentos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinBoleto(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Boleto', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Lancamentos is new, it will return
     * an empty collection; or if this Lancamentos has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Lancamentos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinCompetencia(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Competencia', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Lancamentos is new, it will return
     * an empty collection; or if this Lancamentos has previously
     * been saved, it will retrieve related LancamentosBoletoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Lancamentos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLancamentosBoletos[] List of ChildLancamentosBoletos objects
     */
    public function getLancamentosBoletossJoinConvenio(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLancamentosBoletosQuery::create(null, $criteria);
        $query->joinWith('Convenio', $joinBehavior);

        return $this->getLancamentosBoletoss($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCompetencia) {
            $this->aCompetencia->removeLancamentos($this);
        }
        if (null !== $this->aConvenio) {
            $this->aConvenio->removeLancamentos($this);
        }
        $this->idlancamento = null;
        $this->conteudo_arquivo_saida = null;
        $this->competencia_id = null;
        $this->data_geracao = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->convenio_id = null;
        $this->sequencial_arquivo_cobranca = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBoletoBaixaHistoricos) {
                foreach ($this->collBoletoBaixaHistoricos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLancamentosBoletoss) {
                foreach ($this->collLancamentosBoletoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBoletoBaixaHistoricos = null;
        $this->collLancamentosBoletoss = null;
        $this->aCompetencia = null;
        $this->aConvenio = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(LancamentosTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
