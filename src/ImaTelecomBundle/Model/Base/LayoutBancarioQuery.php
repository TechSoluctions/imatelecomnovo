<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\LayoutBancario as ChildLayoutBancario;
use ImaTelecomBundle\Model\LayoutBancarioQuery as ChildLayoutBancarioQuery;
use ImaTelecomBundle\Model\Map\LayoutBancarioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'layout_bancario' table.
 *
 *
 *
 * @method     ChildLayoutBancarioQuery orderByIdlayoutBancario($order = Criteria::ASC) Order by the idlayout_bancario column
 * @method     ChildLayoutBancarioQuery orderByTamanhoNossoNumero($order = Criteria::ASC) Order by the tamanho_nosso_numero column
 * @method     ChildLayoutBancarioQuery orderByPosicaoNossoNumeroInicial($order = Criteria::ASC) Order by the posicao_nosso_numero_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoNossoNumeroFinal($order = Criteria::ASC) Order by the posicao_nosso_numero_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoChaveLancamentoInicial($order = Criteria::ASC) Order by the posicao_chave_lancamento_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoChaveLancamentoFinal($order = Criteria::ASC) Order by the posicao_chave_lancamento_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoChaveLancamento2Inicial($order = Criteria::ASC) Order by the posicao_chave_lancamento2_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoChaveLancamento2Final($order = Criteria::ASC) Order by the posicao_chave_lancamento2_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorOriginalInicial($order = Criteria::ASC) Order by the posicao_valor_original_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorOriginalFinal($order = Criteria::ASC) Order by the posicao_valor_original_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorBaixaInicial($order = Criteria::ASC) Order by the posicao_valor_baixa_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorBaixaFinal($order = Criteria::ASC) Order by the posicao_valor_baixa_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorJurosInicial($order = Criteria::ASC) Order by the posicao_valor_juros_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorJurosFinal($order = Criteria::ASC) Order by the posicao_valor_juros_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorMultaInicial($order = Criteria::ASC) Order by the posicao_valor_multa_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorMultaFinal($order = Criteria::ASC) Order by the posicao_valor_multa_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorDescontoInicial($order = Criteria::ASC) Order by the posicao_valor_desconto_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoValorDescontoFinal($order = Criteria::ASC) Order by the posicao_valor_desconto_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoCodigoRetornoInicial($order = Criteria::ASC) Order by the posicao_codigo_retorno_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoCodigoRetornoFinal($order = Criteria::ASC) Order by the posicao_codigo_retorno_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoQuantidadeErrosInicial($order = Criteria::ASC) Order by the posicao_quantidade_erros_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoQuantidadeErrosFinal($order = Criteria::ASC) Order by the posicao_quantidade_erros_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoNumeroDocumentoInicial($order = Criteria::ASC) Order by the posicao_numero_documento_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoNumeroDocumentoFinal($order = Criteria::ASC) Order by the posicao_numero_documento_final column
 * @method     ChildLayoutBancarioQuery orderByPosicaoDataBaixaInicial($order = Criteria::ASC) Order by the posicao_data_baixa_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoDataBaixaFinal($order = Criteria::ASC) Order by the posicao_data_baixa_final column
 * @method     ChildLayoutBancarioQuery orderByFormatoDataBaixa($order = Criteria::ASC) Order by the formato_data_baixa column
 * @method     ChildLayoutBancarioQuery orderByPosicaoDataCompensacaoInicial($order = Criteria::ASC) Order by the posicao_data_compensacao_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoDataCompensacaoFinal($order = Criteria::ASC) Order by the posicao_data_compensacao_final column
 * @method     ChildLayoutBancarioQuery orderByFormatoDataCompensacao($order = Criteria::ASC) Order by the formato_data_compensacao column
 * @method     ChildLayoutBancarioQuery orderByPosicaoCampoComplementarInicial($order = Criteria::ASC) Order by the posicao_campo_complementar_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoCampoComplementarFinal($order = Criteria::ASC) Order by the posicao_campo_complementar_final column
 * @method     ChildLayoutBancarioQuery orderByFormatoComplementar($order = Criteria::ASC) Order by the formato_complementar column
 * @method     ChildLayoutBancarioQuery orderByPosicaoDataComplementarInicial($order = Criteria::ASC) Order by the posicao_data_complementar_inicial column
 * @method     ChildLayoutBancarioQuery orderByPosicaoDataComplementarFinal($order = Criteria::ASC) Order by the posicao_data_complementar_final column
 * @method     ChildLayoutBancarioQuery orderByFormatoDataComplementar($order = Criteria::ASC) Order by the formato_data_complementar column
 * @method     ChildLayoutBancarioQuery orderByCodigoRetornoBaixa($order = Criteria::ASC) Order by the codigo_retorno_baixa column
 * @method     ChildLayoutBancarioQuery orderByCodigoRetornoResgistrado($order = Criteria::ASC) Order by the codigo_retorno_resgistrado column
 * @method     ChildLayoutBancarioQuery orderByCodigoRetornoRecusado($order = Criteria::ASC) Order by the codigo_retorno_recusado column
 * @method     ChildLayoutBancarioQuery orderByDataCadastrado($order = Criteria::ASC) Order by the data_cadastrado column
 * @method     ChildLayoutBancarioQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 *
 * @method     ChildLayoutBancarioQuery groupByIdlayoutBancario() Group by the idlayout_bancario column
 * @method     ChildLayoutBancarioQuery groupByTamanhoNossoNumero() Group by the tamanho_nosso_numero column
 * @method     ChildLayoutBancarioQuery groupByPosicaoNossoNumeroInicial() Group by the posicao_nosso_numero_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoNossoNumeroFinal() Group by the posicao_nosso_numero_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoChaveLancamentoInicial() Group by the posicao_chave_lancamento_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoChaveLancamentoFinal() Group by the posicao_chave_lancamento_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoChaveLancamento2Inicial() Group by the posicao_chave_lancamento2_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoChaveLancamento2Final() Group by the posicao_chave_lancamento2_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorOriginalInicial() Group by the posicao_valor_original_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorOriginalFinal() Group by the posicao_valor_original_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorBaixaInicial() Group by the posicao_valor_baixa_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorBaixaFinal() Group by the posicao_valor_baixa_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorJurosInicial() Group by the posicao_valor_juros_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorJurosFinal() Group by the posicao_valor_juros_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorMultaInicial() Group by the posicao_valor_multa_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorMultaFinal() Group by the posicao_valor_multa_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorDescontoInicial() Group by the posicao_valor_desconto_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoValorDescontoFinal() Group by the posicao_valor_desconto_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoCodigoRetornoInicial() Group by the posicao_codigo_retorno_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoCodigoRetornoFinal() Group by the posicao_codigo_retorno_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoQuantidadeErrosInicial() Group by the posicao_quantidade_erros_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoQuantidadeErrosFinal() Group by the posicao_quantidade_erros_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoNumeroDocumentoInicial() Group by the posicao_numero_documento_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoNumeroDocumentoFinal() Group by the posicao_numero_documento_final column
 * @method     ChildLayoutBancarioQuery groupByPosicaoDataBaixaInicial() Group by the posicao_data_baixa_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoDataBaixaFinal() Group by the posicao_data_baixa_final column
 * @method     ChildLayoutBancarioQuery groupByFormatoDataBaixa() Group by the formato_data_baixa column
 * @method     ChildLayoutBancarioQuery groupByPosicaoDataCompensacaoInicial() Group by the posicao_data_compensacao_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoDataCompensacaoFinal() Group by the posicao_data_compensacao_final column
 * @method     ChildLayoutBancarioQuery groupByFormatoDataCompensacao() Group by the formato_data_compensacao column
 * @method     ChildLayoutBancarioQuery groupByPosicaoCampoComplementarInicial() Group by the posicao_campo_complementar_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoCampoComplementarFinal() Group by the posicao_campo_complementar_final column
 * @method     ChildLayoutBancarioQuery groupByFormatoComplementar() Group by the formato_complementar column
 * @method     ChildLayoutBancarioQuery groupByPosicaoDataComplementarInicial() Group by the posicao_data_complementar_inicial column
 * @method     ChildLayoutBancarioQuery groupByPosicaoDataComplementarFinal() Group by the posicao_data_complementar_final column
 * @method     ChildLayoutBancarioQuery groupByFormatoDataComplementar() Group by the formato_data_complementar column
 * @method     ChildLayoutBancarioQuery groupByCodigoRetornoBaixa() Group by the codigo_retorno_baixa column
 * @method     ChildLayoutBancarioQuery groupByCodigoRetornoResgistrado() Group by the codigo_retorno_resgistrado column
 * @method     ChildLayoutBancarioQuery groupByCodigoRetornoRecusado() Group by the codigo_retorno_recusado column
 * @method     ChildLayoutBancarioQuery groupByDataCadastrado() Group by the data_cadastrado column
 * @method     ChildLayoutBancarioQuery groupByDataAlterado() Group by the data_alterado column
 *
 * @method     ChildLayoutBancarioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLayoutBancarioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLayoutBancarioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLayoutBancarioQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLayoutBancarioQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLayoutBancarioQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLayoutBancarioQuery leftJoinBanco($relationAlias = null) Adds a LEFT JOIN clause to the query using the Banco relation
 * @method     ChildLayoutBancarioQuery rightJoinBanco($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Banco relation
 * @method     ChildLayoutBancarioQuery innerJoinBanco($relationAlias = null) Adds a INNER JOIN clause to the query using the Banco relation
 *
 * @method     ChildLayoutBancarioQuery joinWithBanco($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Banco relation
 *
 * @method     ChildLayoutBancarioQuery leftJoinWithBanco() Adds a LEFT JOIN clause and with to the query using the Banco relation
 * @method     ChildLayoutBancarioQuery rightJoinWithBanco() Adds a RIGHT JOIN clause and with to the query using the Banco relation
 * @method     ChildLayoutBancarioQuery innerJoinWithBanco() Adds a INNER JOIN clause and with to the query using the Banco relation
 *
 * @method     \ImaTelecomBundle\Model\BancoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLayoutBancario findOne(ConnectionInterface $con = null) Return the first ChildLayoutBancario matching the query
 * @method     ChildLayoutBancario findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLayoutBancario matching the query, or a new ChildLayoutBancario object populated from the query conditions when no match is found
 *
 * @method     ChildLayoutBancario findOneByIdlayoutBancario(int $idlayout_bancario) Return the first ChildLayoutBancario filtered by the idlayout_bancario column
 * @method     ChildLayoutBancario findOneByTamanhoNossoNumero(int $tamanho_nosso_numero) Return the first ChildLayoutBancario filtered by the tamanho_nosso_numero column
 * @method     ChildLayoutBancario findOneByPosicaoNossoNumeroInicial(int $posicao_nosso_numero_inicial) Return the first ChildLayoutBancario filtered by the posicao_nosso_numero_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoNossoNumeroFinal(int $posicao_nosso_numero_final) Return the first ChildLayoutBancario filtered by the posicao_nosso_numero_final column
 * @method     ChildLayoutBancario findOneByPosicaoChaveLancamentoInicial(int $posicao_chave_lancamento_inicial) Return the first ChildLayoutBancario filtered by the posicao_chave_lancamento_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoChaveLancamentoFinal(int $posicao_chave_lancamento_final) Return the first ChildLayoutBancario filtered by the posicao_chave_lancamento_final column
 * @method     ChildLayoutBancario findOneByPosicaoChaveLancamento2Inicial(int $posicao_chave_lancamento2_inicial) Return the first ChildLayoutBancario filtered by the posicao_chave_lancamento2_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoChaveLancamento2Final(int $posicao_chave_lancamento2_final) Return the first ChildLayoutBancario filtered by the posicao_chave_lancamento2_final column
 * @method     ChildLayoutBancario findOneByPosicaoValorOriginalInicial(int $posicao_valor_original_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_original_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoValorOriginalFinal(int $posicao_valor_original_final) Return the first ChildLayoutBancario filtered by the posicao_valor_original_final column
 * @method     ChildLayoutBancario findOneByPosicaoValorBaixaInicial(int $posicao_valor_baixa_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_baixa_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoValorBaixaFinal(int $posicao_valor_baixa_final) Return the first ChildLayoutBancario filtered by the posicao_valor_baixa_final column
 * @method     ChildLayoutBancario findOneByPosicaoValorJurosInicial(int $posicao_valor_juros_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_juros_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoValorJurosFinal(int $posicao_valor_juros_final) Return the first ChildLayoutBancario filtered by the posicao_valor_juros_final column
 * @method     ChildLayoutBancario findOneByPosicaoValorMultaInicial(int $posicao_valor_multa_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_multa_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoValorMultaFinal(int $posicao_valor_multa_final) Return the first ChildLayoutBancario filtered by the posicao_valor_multa_final column
 * @method     ChildLayoutBancario findOneByPosicaoValorDescontoInicial(int $posicao_valor_desconto_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_desconto_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoValorDescontoFinal(int $posicao_valor_desconto_final) Return the first ChildLayoutBancario filtered by the posicao_valor_desconto_final column
 * @method     ChildLayoutBancario findOneByPosicaoCodigoRetornoInicial(int $posicao_codigo_retorno_inicial) Return the first ChildLayoutBancario filtered by the posicao_codigo_retorno_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoCodigoRetornoFinal(int $posicao_codigo_retorno_final) Return the first ChildLayoutBancario filtered by the posicao_codigo_retorno_final column
 * @method     ChildLayoutBancario findOneByPosicaoQuantidadeErrosInicial(int $posicao_quantidade_erros_inicial) Return the first ChildLayoutBancario filtered by the posicao_quantidade_erros_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoQuantidadeErrosFinal(int $posicao_quantidade_erros_final) Return the first ChildLayoutBancario filtered by the posicao_quantidade_erros_final column
 * @method     ChildLayoutBancario findOneByPosicaoNumeroDocumentoInicial(int $posicao_numero_documento_inicial) Return the first ChildLayoutBancario filtered by the posicao_numero_documento_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoNumeroDocumentoFinal(int $posicao_numero_documento_final) Return the first ChildLayoutBancario filtered by the posicao_numero_documento_final column
 * @method     ChildLayoutBancario findOneByPosicaoDataBaixaInicial(int $posicao_data_baixa_inicial) Return the first ChildLayoutBancario filtered by the posicao_data_baixa_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoDataBaixaFinal(int $posicao_data_baixa_final) Return the first ChildLayoutBancario filtered by the posicao_data_baixa_final column
 * @method     ChildLayoutBancario findOneByFormatoDataBaixa(string $formato_data_baixa) Return the first ChildLayoutBancario filtered by the formato_data_baixa column
 * @method     ChildLayoutBancario findOneByPosicaoDataCompensacaoInicial(int $posicao_data_compensacao_inicial) Return the first ChildLayoutBancario filtered by the posicao_data_compensacao_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoDataCompensacaoFinal(int $posicao_data_compensacao_final) Return the first ChildLayoutBancario filtered by the posicao_data_compensacao_final column
 * @method     ChildLayoutBancario findOneByFormatoDataCompensacao(string $formato_data_compensacao) Return the first ChildLayoutBancario filtered by the formato_data_compensacao column
 * @method     ChildLayoutBancario findOneByPosicaoCampoComplementarInicial(int $posicao_campo_complementar_inicial) Return the first ChildLayoutBancario filtered by the posicao_campo_complementar_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoCampoComplementarFinal(int $posicao_campo_complementar_final) Return the first ChildLayoutBancario filtered by the posicao_campo_complementar_final column
 * @method     ChildLayoutBancario findOneByFormatoComplementar(string $formato_complementar) Return the first ChildLayoutBancario filtered by the formato_complementar column
 * @method     ChildLayoutBancario findOneByPosicaoDataComplementarInicial(int $posicao_data_complementar_inicial) Return the first ChildLayoutBancario filtered by the posicao_data_complementar_inicial column
 * @method     ChildLayoutBancario findOneByPosicaoDataComplementarFinal(int $posicao_data_complementar_final) Return the first ChildLayoutBancario filtered by the posicao_data_complementar_final column
 * @method     ChildLayoutBancario findOneByFormatoDataComplementar(string $formato_data_complementar) Return the first ChildLayoutBancario filtered by the formato_data_complementar column
 * @method     ChildLayoutBancario findOneByCodigoRetornoBaixa(string $codigo_retorno_baixa) Return the first ChildLayoutBancario filtered by the codigo_retorno_baixa column
 * @method     ChildLayoutBancario findOneByCodigoRetornoResgistrado(string $codigo_retorno_resgistrado) Return the first ChildLayoutBancario filtered by the codigo_retorno_resgistrado column
 * @method     ChildLayoutBancario findOneByCodigoRetornoRecusado(string $codigo_retorno_recusado) Return the first ChildLayoutBancario filtered by the codigo_retorno_recusado column
 * @method     ChildLayoutBancario findOneByDataCadastrado(string $data_cadastrado) Return the first ChildLayoutBancario filtered by the data_cadastrado column
 * @method     ChildLayoutBancario findOneByDataAlterado(string $data_alterado) Return the first ChildLayoutBancario filtered by the data_alterado column *

 * @method     ChildLayoutBancario requirePk($key, ConnectionInterface $con = null) Return the ChildLayoutBancario by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOne(ConnectionInterface $con = null) Return the first ChildLayoutBancario matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLayoutBancario requireOneByIdlayoutBancario(int $idlayout_bancario) Return the first ChildLayoutBancario filtered by the idlayout_bancario column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByTamanhoNossoNumero(int $tamanho_nosso_numero) Return the first ChildLayoutBancario filtered by the tamanho_nosso_numero column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoNossoNumeroInicial(int $posicao_nosso_numero_inicial) Return the first ChildLayoutBancario filtered by the posicao_nosso_numero_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoNossoNumeroFinal(int $posicao_nosso_numero_final) Return the first ChildLayoutBancario filtered by the posicao_nosso_numero_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoChaveLancamentoInicial(int $posicao_chave_lancamento_inicial) Return the first ChildLayoutBancario filtered by the posicao_chave_lancamento_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoChaveLancamentoFinal(int $posicao_chave_lancamento_final) Return the first ChildLayoutBancario filtered by the posicao_chave_lancamento_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoChaveLancamento2Inicial(int $posicao_chave_lancamento2_inicial) Return the first ChildLayoutBancario filtered by the posicao_chave_lancamento2_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoChaveLancamento2Final(int $posicao_chave_lancamento2_final) Return the first ChildLayoutBancario filtered by the posicao_chave_lancamento2_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorOriginalInicial(int $posicao_valor_original_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_original_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorOriginalFinal(int $posicao_valor_original_final) Return the first ChildLayoutBancario filtered by the posicao_valor_original_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorBaixaInicial(int $posicao_valor_baixa_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_baixa_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorBaixaFinal(int $posicao_valor_baixa_final) Return the first ChildLayoutBancario filtered by the posicao_valor_baixa_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorJurosInicial(int $posicao_valor_juros_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_juros_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorJurosFinal(int $posicao_valor_juros_final) Return the first ChildLayoutBancario filtered by the posicao_valor_juros_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorMultaInicial(int $posicao_valor_multa_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_multa_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorMultaFinal(int $posicao_valor_multa_final) Return the first ChildLayoutBancario filtered by the posicao_valor_multa_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorDescontoInicial(int $posicao_valor_desconto_inicial) Return the first ChildLayoutBancario filtered by the posicao_valor_desconto_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoValorDescontoFinal(int $posicao_valor_desconto_final) Return the first ChildLayoutBancario filtered by the posicao_valor_desconto_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoCodigoRetornoInicial(int $posicao_codigo_retorno_inicial) Return the first ChildLayoutBancario filtered by the posicao_codigo_retorno_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoCodigoRetornoFinal(int $posicao_codigo_retorno_final) Return the first ChildLayoutBancario filtered by the posicao_codigo_retorno_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoQuantidadeErrosInicial(int $posicao_quantidade_erros_inicial) Return the first ChildLayoutBancario filtered by the posicao_quantidade_erros_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoQuantidadeErrosFinal(int $posicao_quantidade_erros_final) Return the first ChildLayoutBancario filtered by the posicao_quantidade_erros_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoNumeroDocumentoInicial(int $posicao_numero_documento_inicial) Return the first ChildLayoutBancario filtered by the posicao_numero_documento_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoNumeroDocumentoFinal(int $posicao_numero_documento_final) Return the first ChildLayoutBancario filtered by the posicao_numero_documento_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoDataBaixaInicial(int $posicao_data_baixa_inicial) Return the first ChildLayoutBancario filtered by the posicao_data_baixa_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoDataBaixaFinal(int $posicao_data_baixa_final) Return the first ChildLayoutBancario filtered by the posicao_data_baixa_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByFormatoDataBaixa(string $formato_data_baixa) Return the first ChildLayoutBancario filtered by the formato_data_baixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoDataCompensacaoInicial(int $posicao_data_compensacao_inicial) Return the first ChildLayoutBancario filtered by the posicao_data_compensacao_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoDataCompensacaoFinal(int $posicao_data_compensacao_final) Return the first ChildLayoutBancario filtered by the posicao_data_compensacao_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByFormatoDataCompensacao(string $formato_data_compensacao) Return the first ChildLayoutBancario filtered by the formato_data_compensacao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoCampoComplementarInicial(int $posicao_campo_complementar_inicial) Return the first ChildLayoutBancario filtered by the posicao_campo_complementar_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoCampoComplementarFinal(int $posicao_campo_complementar_final) Return the first ChildLayoutBancario filtered by the posicao_campo_complementar_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByFormatoComplementar(string $formato_complementar) Return the first ChildLayoutBancario filtered by the formato_complementar column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoDataComplementarInicial(int $posicao_data_complementar_inicial) Return the first ChildLayoutBancario filtered by the posicao_data_complementar_inicial column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByPosicaoDataComplementarFinal(int $posicao_data_complementar_final) Return the first ChildLayoutBancario filtered by the posicao_data_complementar_final column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByFormatoDataComplementar(string $formato_data_complementar) Return the first ChildLayoutBancario filtered by the formato_data_complementar column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByCodigoRetornoBaixa(string $codigo_retorno_baixa) Return the first ChildLayoutBancario filtered by the codigo_retorno_baixa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByCodigoRetornoResgistrado(string $codigo_retorno_resgistrado) Return the first ChildLayoutBancario filtered by the codigo_retorno_resgistrado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByCodigoRetornoRecusado(string $codigo_retorno_recusado) Return the first ChildLayoutBancario filtered by the codigo_retorno_recusado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByDataCadastrado(string $data_cadastrado) Return the first ChildLayoutBancario filtered by the data_cadastrado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLayoutBancario requireOneByDataAlterado(string $data_alterado) Return the first ChildLayoutBancario filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLayoutBancario[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLayoutBancario objects based on current ModelCriteria
 * @method     ChildLayoutBancario[]|ObjectCollection findByIdlayoutBancario(int $idlayout_bancario) Return ChildLayoutBancario objects filtered by the idlayout_bancario column
 * @method     ChildLayoutBancario[]|ObjectCollection findByTamanhoNossoNumero(int $tamanho_nosso_numero) Return ChildLayoutBancario objects filtered by the tamanho_nosso_numero column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoNossoNumeroInicial(int $posicao_nosso_numero_inicial) Return ChildLayoutBancario objects filtered by the posicao_nosso_numero_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoNossoNumeroFinal(int $posicao_nosso_numero_final) Return ChildLayoutBancario objects filtered by the posicao_nosso_numero_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoChaveLancamentoInicial(int $posicao_chave_lancamento_inicial) Return ChildLayoutBancario objects filtered by the posicao_chave_lancamento_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoChaveLancamentoFinal(int $posicao_chave_lancamento_final) Return ChildLayoutBancario objects filtered by the posicao_chave_lancamento_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoChaveLancamento2Inicial(int $posicao_chave_lancamento2_inicial) Return ChildLayoutBancario objects filtered by the posicao_chave_lancamento2_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoChaveLancamento2Final(int $posicao_chave_lancamento2_final) Return ChildLayoutBancario objects filtered by the posicao_chave_lancamento2_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorOriginalInicial(int $posicao_valor_original_inicial) Return ChildLayoutBancario objects filtered by the posicao_valor_original_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorOriginalFinal(int $posicao_valor_original_final) Return ChildLayoutBancario objects filtered by the posicao_valor_original_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorBaixaInicial(int $posicao_valor_baixa_inicial) Return ChildLayoutBancario objects filtered by the posicao_valor_baixa_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorBaixaFinal(int $posicao_valor_baixa_final) Return ChildLayoutBancario objects filtered by the posicao_valor_baixa_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorJurosInicial(int $posicao_valor_juros_inicial) Return ChildLayoutBancario objects filtered by the posicao_valor_juros_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorJurosFinal(int $posicao_valor_juros_final) Return ChildLayoutBancario objects filtered by the posicao_valor_juros_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorMultaInicial(int $posicao_valor_multa_inicial) Return ChildLayoutBancario objects filtered by the posicao_valor_multa_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorMultaFinal(int $posicao_valor_multa_final) Return ChildLayoutBancario objects filtered by the posicao_valor_multa_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorDescontoInicial(int $posicao_valor_desconto_inicial) Return ChildLayoutBancario objects filtered by the posicao_valor_desconto_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoValorDescontoFinal(int $posicao_valor_desconto_final) Return ChildLayoutBancario objects filtered by the posicao_valor_desconto_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoCodigoRetornoInicial(int $posicao_codigo_retorno_inicial) Return ChildLayoutBancario objects filtered by the posicao_codigo_retorno_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoCodigoRetornoFinal(int $posicao_codigo_retorno_final) Return ChildLayoutBancario objects filtered by the posicao_codigo_retorno_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoQuantidadeErrosInicial(int $posicao_quantidade_erros_inicial) Return ChildLayoutBancario objects filtered by the posicao_quantidade_erros_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoQuantidadeErrosFinal(int $posicao_quantidade_erros_final) Return ChildLayoutBancario objects filtered by the posicao_quantidade_erros_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoNumeroDocumentoInicial(int $posicao_numero_documento_inicial) Return ChildLayoutBancario objects filtered by the posicao_numero_documento_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoNumeroDocumentoFinal(int $posicao_numero_documento_final) Return ChildLayoutBancario objects filtered by the posicao_numero_documento_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoDataBaixaInicial(int $posicao_data_baixa_inicial) Return ChildLayoutBancario objects filtered by the posicao_data_baixa_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoDataBaixaFinal(int $posicao_data_baixa_final) Return ChildLayoutBancario objects filtered by the posicao_data_baixa_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByFormatoDataBaixa(string $formato_data_baixa) Return ChildLayoutBancario objects filtered by the formato_data_baixa column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoDataCompensacaoInicial(int $posicao_data_compensacao_inicial) Return ChildLayoutBancario objects filtered by the posicao_data_compensacao_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoDataCompensacaoFinal(int $posicao_data_compensacao_final) Return ChildLayoutBancario objects filtered by the posicao_data_compensacao_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByFormatoDataCompensacao(string $formato_data_compensacao) Return ChildLayoutBancario objects filtered by the formato_data_compensacao column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoCampoComplementarInicial(int $posicao_campo_complementar_inicial) Return ChildLayoutBancario objects filtered by the posicao_campo_complementar_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoCampoComplementarFinal(int $posicao_campo_complementar_final) Return ChildLayoutBancario objects filtered by the posicao_campo_complementar_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByFormatoComplementar(string $formato_complementar) Return ChildLayoutBancario objects filtered by the formato_complementar column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoDataComplementarInicial(int $posicao_data_complementar_inicial) Return ChildLayoutBancario objects filtered by the posicao_data_complementar_inicial column
 * @method     ChildLayoutBancario[]|ObjectCollection findByPosicaoDataComplementarFinal(int $posicao_data_complementar_final) Return ChildLayoutBancario objects filtered by the posicao_data_complementar_final column
 * @method     ChildLayoutBancario[]|ObjectCollection findByFormatoDataComplementar(string $formato_data_complementar) Return ChildLayoutBancario objects filtered by the formato_data_complementar column
 * @method     ChildLayoutBancario[]|ObjectCollection findByCodigoRetornoBaixa(string $codigo_retorno_baixa) Return ChildLayoutBancario objects filtered by the codigo_retorno_baixa column
 * @method     ChildLayoutBancario[]|ObjectCollection findByCodigoRetornoResgistrado(string $codigo_retorno_resgistrado) Return ChildLayoutBancario objects filtered by the codigo_retorno_resgistrado column
 * @method     ChildLayoutBancario[]|ObjectCollection findByCodigoRetornoRecusado(string $codigo_retorno_recusado) Return ChildLayoutBancario objects filtered by the codigo_retorno_recusado column
 * @method     ChildLayoutBancario[]|ObjectCollection findByDataCadastrado(string $data_cadastrado) Return ChildLayoutBancario objects filtered by the data_cadastrado column
 * @method     ChildLayoutBancario[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildLayoutBancario objects filtered by the data_alterado column
 * @method     ChildLayoutBancario[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LayoutBancarioQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\LayoutBancarioQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\LayoutBancario', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLayoutBancarioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLayoutBancarioQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLayoutBancarioQuery) {
            return $criteria;
        }
        $query = new ChildLayoutBancarioQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLayoutBancario|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LayoutBancarioTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LayoutBancarioTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLayoutBancario A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idlayout_bancario, tamanho_nosso_numero, posicao_nosso_numero_inicial, posicao_nosso_numero_final, posicao_chave_lancamento_inicial, posicao_chave_lancamento_final, posicao_chave_lancamento2_inicial, posicao_chave_lancamento2_final, posicao_valor_original_inicial, posicao_valor_original_final, posicao_valor_baixa_inicial, posicao_valor_baixa_final, posicao_valor_juros_inicial, posicao_valor_juros_final, posicao_valor_multa_inicial, posicao_valor_multa_final, posicao_valor_desconto_inicial, posicao_valor_desconto_final, posicao_codigo_retorno_inicial, posicao_codigo_retorno_final, posicao_quantidade_erros_inicial, posicao_quantidade_erros_final, posicao_numero_documento_inicial, posicao_numero_documento_final, posicao_data_baixa_inicial, posicao_data_baixa_final, formato_data_baixa, posicao_data_compensacao_inicial, posicao_data_compensacao_final, formato_data_compensacao, posicao_campo_complementar_inicial, posicao_campo_complementar_final, formato_complementar, posicao_data_complementar_inicial, posicao_data_complementar_final, formato_data_complementar, codigo_retorno_baixa, codigo_retorno_resgistrado, codigo_retorno_recusado, data_cadastrado, data_alterado FROM layout_bancario WHERE idlayout_bancario = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLayoutBancario $obj */
            $obj = new ChildLayoutBancario();
            $obj->hydrate($row);
            LayoutBancarioTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLayoutBancario|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idlayout_bancario column
     *
     * Example usage:
     * <code>
     * $query->filterByIdlayoutBancario(1234); // WHERE idlayout_bancario = 1234
     * $query->filterByIdlayoutBancario(array(12, 34)); // WHERE idlayout_bancario IN (12, 34)
     * $query->filterByIdlayoutBancario(array('min' => 12)); // WHERE idlayout_bancario > 12
     * </code>
     *
     * @param     mixed $idlayoutBancario The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByIdlayoutBancario($idlayoutBancario = null, $comparison = null)
    {
        if (is_array($idlayoutBancario)) {
            $useMinMax = false;
            if (isset($idlayoutBancario['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, $idlayoutBancario['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idlayoutBancario['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, $idlayoutBancario['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, $idlayoutBancario, $comparison);
    }

    /**
     * Filter the query on the tamanho_nosso_numero column
     *
     * Example usage:
     * <code>
     * $query->filterByTamanhoNossoNumero(1234); // WHERE tamanho_nosso_numero = 1234
     * $query->filterByTamanhoNossoNumero(array(12, 34)); // WHERE tamanho_nosso_numero IN (12, 34)
     * $query->filterByTamanhoNossoNumero(array('min' => 12)); // WHERE tamanho_nosso_numero > 12
     * </code>
     *
     * @param     mixed $tamanhoNossoNumero The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByTamanhoNossoNumero($tamanhoNossoNumero = null, $comparison = null)
    {
        if (is_array($tamanhoNossoNumero)) {
            $useMinMax = false;
            if (isset($tamanhoNossoNumero['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO, $tamanhoNossoNumero['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tamanhoNossoNumero['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO, $tamanhoNossoNumero['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO, $tamanhoNossoNumero, $comparison);
    }

    /**
     * Filter the query on the posicao_nosso_numero_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoNossoNumeroInicial(1234); // WHERE posicao_nosso_numero_inicial = 1234
     * $query->filterByPosicaoNossoNumeroInicial(array(12, 34)); // WHERE posicao_nosso_numero_inicial IN (12, 34)
     * $query->filterByPosicaoNossoNumeroInicial(array('min' => 12)); // WHERE posicao_nosso_numero_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoNossoNumeroInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoNossoNumeroInicial($posicaoNossoNumeroInicial = null, $comparison = null)
    {
        if (is_array($posicaoNossoNumeroInicial)) {
            $useMinMax = false;
            if (isset($posicaoNossoNumeroInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL, $posicaoNossoNumeroInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoNossoNumeroInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL, $posicaoNossoNumeroInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL, $posicaoNossoNumeroInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_nosso_numero_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoNossoNumeroFinal(1234); // WHERE posicao_nosso_numero_final = 1234
     * $query->filterByPosicaoNossoNumeroFinal(array(12, 34)); // WHERE posicao_nosso_numero_final IN (12, 34)
     * $query->filterByPosicaoNossoNumeroFinal(array('min' => 12)); // WHERE posicao_nosso_numero_final > 12
     * </code>
     *
     * @param     mixed $posicaoNossoNumeroFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoNossoNumeroFinal($posicaoNossoNumeroFinal = null, $comparison = null)
    {
        if (is_array($posicaoNossoNumeroFinal)) {
            $useMinMax = false;
            if (isset($posicaoNossoNumeroFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL, $posicaoNossoNumeroFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoNossoNumeroFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL, $posicaoNossoNumeroFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL, $posicaoNossoNumeroFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_chave_lancamento_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoChaveLancamentoInicial(1234); // WHERE posicao_chave_lancamento_inicial = 1234
     * $query->filterByPosicaoChaveLancamentoInicial(array(12, 34)); // WHERE posicao_chave_lancamento_inicial IN (12, 34)
     * $query->filterByPosicaoChaveLancamentoInicial(array('min' => 12)); // WHERE posicao_chave_lancamento_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoChaveLancamentoInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoChaveLancamentoInicial($posicaoChaveLancamentoInicial = null, $comparison = null)
    {
        if (is_array($posicaoChaveLancamentoInicial)) {
            $useMinMax = false;
            if (isset($posicaoChaveLancamentoInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL, $posicaoChaveLancamentoInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoChaveLancamentoInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL, $posicaoChaveLancamentoInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL, $posicaoChaveLancamentoInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_chave_lancamento_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoChaveLancamentoFinal(1234); // WHERE posicao_chave_lancamento_final = 1234
     * $query->filterByPosicaoChaveLancamentoFinal(array(12, 34)); // WHERE posicao_chave_lancamento_final IN (12, 34)
     * $query->filterByPosicaoChaveLancamentoFinal(array('min' => 12)); // WHERE posicao_chave_lancamento_final > 12
     * </code>
     *
     * @param     mixed $posicaoChaveLancamentoFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoChaveLancamentoFinal($posicaoChaveLancamentoFinal = null, $comparison = null)
    {
        if (is_array($posicaoChaveLancamentoFinal)) {
            $useMinMax = false;
            if (isset($posicaoChaveLancamentoFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL, $posicaoChaveLancamentoFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoChaveLancamentoFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL, $posicaoChaveLancamentoFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL, $posicaoChaveLancamentoFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_chave_lancamento2_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoChaveLancamento2Inicial(1234); // WHERE posicao_chave_lancamento2_inicial = 1234
     * $query->filterByPosicaoChaveLancamento2Inicial(array(12, 34)); // WHERE posicao_chave_lancamento2_inicial IN (12, 34)
     * $query->filterByPosicaoChaveLancamento2Inicial(array('min' => 12)); // WHERE posicao_chave_lancamento2_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoChaveLancamento2Inicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoChaveLancamento2Inicial($posicaoChaveLancamento2Inicial = null, $comparison = null)
    {
        if (is_array($posicaoChaveLancamento2Inicial)) {
            $useMinMax = false;
            if (isset($posicaoChaveLancamento2Inicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL, $posicaoChaveLancamento2Inicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoChaveLancamento2Inicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL, $posicaoChaveLancamento2Inicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL, $posicaoChaveLancamento2Inicial, $comparison);
    }

    /**
     * Filter the query on the posicao_chave_lancamento2_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoChaveLancamento2Final(1234); // WHERE posicao_chave_lancamento2_final = 1234
     * $query->filterByPosicaoChaveLancamento2Final(array(12, 34)); // WHERE posicao_chave_lancamento2_final IN (12, 34)
     * $query->filterByPosicaoChaveLancamento2Final(array('min' => 12)); // WHERE posicao_chave_lancamento2_final > 12
     * </code>
     *
     * @param     mixed $posicaoChaveLancamento2Final The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoChaveLancamento2Final($posicaoChaveLancamento2Final = null, $comparison = null)
    {
        if (is_array($posicaoChaveLancamento2Final)) {
            $useMinMax = false;
            if (isset($posicaoChaveLancamento2Final['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL, $posicaoChaveLancamento2Final['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoChaveLancamento2Final['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL, $posicaoChaveLancamento2Final['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL, $posicaoChaveLancamento2Final, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_original_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorOriginalInicial(1234); // WHERE posicao_valor_original_inicial = 1234
     * $query->filterByPosicaoValorOriginalInicial(array(12, 34)); // WHERE posicao_valor_original_inicial IN (12, 34)
     * $query->filterByPosicaoValorOriginalInicial(array('min' => 12)); // WHERE posicao_valor_original_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoValorOriginalInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorOriginalInicial($posicaoValorOriginalInicial = null, $comparison = null)
    {
        if (is_array($posicaoValorOriginalInicial)) {
            $useMinMax = false;
            if (isset($posicaoValorOriginalInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL, $posicaoValorOriginalInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorOriginalInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL, $posicaoValorOriginalInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL, $posicaoValorOriginalInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_original_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorOriginalFinal(1234); // WHERE posicao_valor_original_final = 1234
     * $query->filterByPosicaoValorOriginalFinal(array(12, 34)); // WHERE posicao_valor_original_final IN (12, 34)
     * $query->filterByPosicaoValorOriginalFinal(array('min' => 12)); // WHERE posicao_valor_original_final > 12
     * </code>
     *
     * @param     mixed $posicaoValorOriginalFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorOriginalFinal($posicaoValorOriginalFinal = null, $comparison = null)
    {
        if (is_array($posicaoValorOriginalFinal)) {
            $useMinMax = false;
            if (isset($posicaoValorOriginalFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL, $posicaoValorOriginalFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorOriginalFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL, $posicaoValorOriginalFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL, $posicaoValorOriginalFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_baixa_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorBaixaInicial(1234); // WHERE posicao_valor_baixa_inicial = 1234
     * $query->filterByPosicaoValorBaixaInicial(array(12, 34)); // WHERE posicao_valor_baixa_inicial IN (12, 34)
     * $query->filterByPosicaoValorBaixaInicial(array('min' => 12)); // WHERE posicao_valor_baixa_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoValorBaixaInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorBaixaInicial($posicaoValorBaixaInicial = null, $comparison = null)
    {
        if (is_array($posicaoValorBaixaInicial)) {
            $useMinMax = false;
            if (isset($posicaoValorBaixaInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL, $posicaoValorBaixaInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorBaixaInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL, $posicaoValorBaixaInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL, $posicaoValorBaixaInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_baixa_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorBaixaFinal(1234); // WHERE posicao_valor_baixa_final = 1234
     * $query->filterByPosicaoValorBaixaFinal(array(12, 34)); // WHERE posicao_valor_baixa_final IN (12, 34)
     * $query->filterByPosicaoValorBaixaFinal(array('min' => 12)); // WHERE posicao_valor_baixa_final > 12
     * </code>
     *
     * @param     mixed $posicaoValorBaixaFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorBaixaFinal($posicaoValorBaixaFinal = null, $comparison = null)
    {
        if (is_array($posicaoValorBaixaFinal)) {
            $useMinMax = false;
            if (isset($posicaoValorBaixaFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL, $posicaoValorBaixaFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorBaixaFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL, $posicaoValorBaixaFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL, $posicaoValorBaixaFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_juros_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorJurosInicial(1234); // WHERE posicao_valor_juros_inicial = 1234
     * $query->filterByPosicaoValorJurosInicial(array(12, 34)); // WHERE posicao_valor_juros_inicial IN (12, 34)
     * $query->filterByPosicaoValorJurosInicial(array('min' => 12)); // WHERE posicao_valor_juros_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoValorJurosInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorJurosInicial($posicaoValorJurosInicial = null, $comparison = null)
    {
        if (is_array($posicaoValorJurosInicial)) {
            $useMinMax = false;
            if (isset($posicaoValorJurosInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL, $posicaoValorJurosInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorJurosInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL, $posicaoValorJurosInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL, $posicaoValorJurosInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_juros_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorJurosFinal(1234); // WHERE posicao_valor_juros_final = 1234
     * $query->filterByPosicaoValorJurosFinal(array(12, 34)); // WHERE posicao_valor_juros_final IN (12, 34)
     * $query->filterByPosicaoValorJurosFinal(array('min' => 12)); // WHERE posicao_valor_juros_final > 12
     * </code>
     *
     * @param     mixed $posicaoValorJurosFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorJurosFinal($posicaoValorJurosFinal = null, $comparison = null)
    {
        if (is_array($posicaoValorJurosFinal)) {
            $useMinMax = false;
            if (isset($posicaoValorJurosFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL, $posicaoValorJurosFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorJurosFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL, $posicaoValorJurosFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL, $posicaoValorJurosFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_multa_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorMultaInicial(1234); // WHERE posicao_valor_multa_inicial = 1234
     * $query->filterByPosicaoValorMultaInicial(array(12, 34)); // WHERE posicao_valor_multa_inicial IN (12, 34)
     * $query->filterByPosicaoValorMultaInicial(array('min' => 12)); // WHERE posicao_valor_multa_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoValorMultaInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorMultaInicial($posicaoValorMultaInicial = null, $comparison = null)
    {
        if (is_array($posicaoValorMultaInicial)) {
            $useMinMax = false;
            if (isset($posicaoValorMultaInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL, $posicaoValorMultaInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorMultaInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL, $posicaoValorMultaInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL, $posicaoValorMultaInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_multa_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorMultaFinal(1234); // WHERE posicao_valor_multa_final = 1234
     * $query->filterByPosicaoValorMultaFinal(array(12, 34)); // WHERE posicao_valor_multa_final IN (12, 34)
     * $query->filterByPosicaoValorMultaFinal(array('min' => 12)); // WHERE posicao_valor_multa_final > 12
     * </code>
     *
     * @param     mixed $posicaoValorMultaFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorMultaFinal($posicaoValorMultaFinal = null, $comparison = null)
    {
        if (is_array($posicaoValorMultaFinal)) {
            $useMinMax = false;
            if (isset($posicaoValorMultaFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL, $posicaoValorMultaFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorMultaFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL, $posicaoValorMultaFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL, $posicaoValorMultaFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_desconto_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorDescontoInicial(1234); // WHERE posicao_valor_desconto_inicial = 1234
     * $query->filterByPosicaoValorDescontoInicial(array(12, 34)); // WHERE posicao_valor_desconto_inicial IN (12, 34)
     * $query->filterByPosicaoValorDescontoInicial(array('min' => 12)); // WHERE posicao_valor_desconto_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoValorDescontoInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorDescontoInicial($posicaoValorDescontoInicial = null, $comparison = null)
    {
        if (is_array($posicaoValorDescontoInicial)) {
            $useMinMax = false;
            if (isset($posicaoValorDescontoInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL, $posicaoValorDescontoInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorDescontoInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL, $posicaoValorDescontoInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL, $posicaoValorDescontoInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_valor_desconto_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoValorDescontoFinal(1234); // WHERE posicao_valor_desconto_final = 1234
     * $query->filterByPosicaoValorDescontoFinal(array(12, 34)); // WHERE posicao_valor_desconto_final IN (12, 34)
     * $query->filterByPosicaoValorDescontoFinal(array('min' => 12)); // WHERE posicao_valor_desconto_final > 12
     * </code>
     *
     * @param     mixed $posicaoValorDescontoFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoValorDescontoFinal($posicaoValorDescontoFinal = null, $comparison = null)
    {
        if (is_array($posicaoValorDescontoFinal)) {
            $useMinMax = false;
            if (isset($posicaoValorDescontoFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL, $posicaoValorDescontoFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoValorDescontoFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL, $posicaoValorDescontoFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL, $posicaoValorDescontoFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_codigo_retorno_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoCodigoRetornoInicial(1234); // WHERE posicao_codigo_retorno_inicial = 1234
     * $query->filterByPosicaoCodigoRetornoInicial(array(12, 34)); // WHERE posicao_codigo_retorno_inicial IN (12, 34)
     * $query->filterByPosicaoCodigoRetornoInicial(array('min' => 12)); // WHERE posicao_codigo_retorno_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoCodigoRetornoInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoCodigoRetornoInicial($posicaoCodigoRetornoInicial = null, $comparison = null)
    {
        if (is_array($posicaoCodigoRetornoInicial)) {
            $useMinMax = false;
            if (isset($posicaoCodigoRetornoInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL, $posicaoCodigoRetornoInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoCodigoRetornoInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL, $posicaoCodigoRetornoInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL, $posicaoCodigoRetornoInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_codigo_retorno_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoCodigoRetornoFinal(1234); // WHERE posicao_codigo_retorno_final = 1234
     * $query->filterByPosicaoCodigoRetornoFinal(array(12, 34)); // WHERE posicao_codigo_retorno_final IN (12, 34)
     * $query->filterByPosicaoCodigoRetornoFinal(array('min' => 12)); // WHERE posicao_codigo_retorno_final > 12
     * </code>
     *
     * @param     mixed $posicaoCodigoRetornoFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoCodigoRetornoFinal($posicaoCodigoRetornoFinal = null, $comparison = null)
    {
        if (is_array($posicaoCodigoRetornoFinal)) {
            $useMinMax = false;
            if (isset($posicaoCodigoRetornoFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL, $posicaoCodigoRetornoFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoCodigoRetornoFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL, $posicaoCodigoRetornoFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL, $posicaoCodigoRetornoFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_quantidade_erros_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoQuantidadeErrosInicial(1234); // WHERE posicao_quantidade_erros_inicial = 1234
     * $query->filterByPosicaoQuantidadeErrosInicial(array(12, 34)); // WHERE posicao_quantidade_erros_inicial IN (12, 34)
     * $query->filterByPosicaoQuantidadeErrosInicial(array('min' => 12)); // WHERE posicao_quantidade_erros_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoQuantidadeErrosInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoQuantidadeErrosInicial($posicaoQuantidadeErrosInicial = null, $comparison = null)
    {
        if (is_array($posicaoQuantidadeErrosInicial)) {
            $useMinMax = false;
            if (isset($posicaoQuantidadeErrosInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL, $posicaoQuantidadeErrosInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoQuantidadeErrosInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL, $posicaoQuantidadeErrosInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL, $posicaoQuantidadeErrosInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_quantidade_erros_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoQuantidadeErrosFinal(1234); // WHERE posicao_quantidade_erros_final = 1234
     * $query->filterByPosicaoQuantidadeErrosFinal(array(12, 34)); // WHERE posicao_quantidade_erros_final IN (12, 34)
     * $query->filterByPosicaoQuantidadeErrosFinal(array('min' => 12)); // WHERE posicao_quantidade_erros_final > 12
     * </code>
     *
     * @param     mixed $posicaoQuantidadeErrosFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoQuantidadeErrosFinal($posicaoQuantidadeErrosFinal = null, $comparison = null)
    {
        if (is_array($posicaoQuantidadeErrosFinal)) {
            $useMinMax = false;
            if (isset($posicaoQuantidadeErrosFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL, $posicaoQuantidadeErrosFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoQuantidadeErrosFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL, $posicaoQuantidadeErrosFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL, $posicaoQuantidadeErrosFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_numero_documento_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoNumeroDocumentoInicial(1234); // WHERE posicao_numero_documento_inicial = 1234
     * $query->filterByPosicaoNumeroDocumentoInicial(array(12, 34)); // WHERE posicao_numero_documento_inicial IN (12, 34)
     * $query->filterByPosicaoNumeroDocumentoInicial(array('min' => 12)); // WHERE posicao_numero_documento_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoNumeroDocumentoInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoNumeroDocumentoInicial($posicaoNumeroDocumentoInicial = null, $comparison = null)
    {
        if (is_array($posicaoNumeroDocumentoInicial)) {
            $useMinMax = false;
            if (isset($posicaoNumeroDocumentoInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL, $posicaoNumeroDocumentoInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoNumeroDocumentoInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL, $posicaoNumeroDocumentoInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL, $posicaoNumeroDocumentoInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_numero_documento_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoNumeroDocumentoFinal(1234); // WHERE posicao_numero_documento_final = 1234
     * $query->filterByPosicaoNumeroDocumentoFinal(array(12, 34)); // WHERE posicao_numero_documento_final IN (12, 34)
     * $query->filterByPosicaoNumeroDocumentoFinal(array('min' => 12)); // WHERE posicao_numero_documento_final > 12
     * </code>
     *
     * @param     mixed $posicaoNumeroDocumentoFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoNumeroDocumentoFinal($posicaoNumeroDocumentoFinal = null, $comparison = null)
    {
        if (is_array($posicaoNumeroDocumentoFinal)) {
            $useMinMax = false;
            if (isset($posicaoNumeroDocumentoFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL, $posicaoNumeroDocumentoFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoNumeroDocumentoFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL, $posicaoNumeroDocumentoFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL, $posicaoNumeroDocumentoFinal, $comparison);
    }

    /**
     * Filter the query on the posicao_data_baixa_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoDataBaixaInicial(1234); // WHERE posicao_data_baixa_inicial = 1234
     * $query->filterByPosicaoDataBaixaInicial(array(12, 34)); // WHERE posicao_data_baixa_inicial IN (12, 34)
     * $query->filterByPosicaoDataBaixaInicial(array('min' => 12)); // WHERE posicao_data_baixa_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoDataBaixaInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoDataBaixaInicial($posicaoDataBaixaInicial = null, $comparison = null)
    {
        if (is_array($posicaoDataBaixaInicial)) {
            $useMinMax = false;
            if (isset($posicaoDataBaixaInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL, $posicaoDataBaixaInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoDataBaixaInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL, $posicaoDataBaixaInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL, $posicaoDataBaixaInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_data_baixa_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoDataBaixaFinal(1234); // WHERE posicao_data_baixa_final = 1234
     * $query->filterByPosicaoDataBaixaFinal(array(12, 34)); // WHERE posicao_data_baixa_final IN (12, 34)
     * $query->filterByPosicaoDataBaixaFinal(array('min' => 12)); // WHERE posicao_data_baixa_final > 12
     * </code>
     *
     * @param     mixed $posicaoDataBaixaFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoDataBaixaFinal($posicaoDataBaixaFinal = null, $comparison = null)
    {
        if (is_array($posicaoDataBaixaFinal)) {
            $useMinMax = false;
            if (isset($posicaoDataBaixaFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL, $posicaoDataBaixaFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoDataBaixaFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL, $posicaoDataBaixaFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL, $posicaoDataBaixaFinal, $comparison);
    }

    /**
     * Filter the query on the formato_data_baixa column
     *
     * Example usage:
     * <code>
     * $query->filterByFormatoDataBaixa('fooValue');   // WHERE formato_data_baixa = 'fooValue'
     * $query->filterByFormatoDataBaixa('%fooValue%', Criteria::LIKE); // WHERE formato_data_baixa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $formatoDataBaixa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByFormatoDataBaixa($formatoDataBaixa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($formatoDataBaixa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_FORMATO_DATA_BAIXA, $formatoDataBaixa, $comparison);
    }

    /**
     * Filter the query on the posicao_data_compensacao_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoDataCompensacaoInicial(1234); // WHERE posicao_data_compensacao_inicial = 1234
     * $query->filterByPosicaoDataCompensacaoInicial(array(12, 34)); // WHERE posicao_data_compensacao_inicial IN (12, 34)
     * $query->filterByPosicaoDataCompensacaoInicial(array('min' => 12)); // WHERE posicao_data_compensacao_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoDataCompensacaoInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoDataCompensacaoInicial($posicaoDataCompensacaoInicial = null, $comparison = null)
    {
        if (is_array($posicaoDataCompensacaoInicial)) {
            $useMinMax = false;
            if (isset($posicaoDataCompensacaoInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL, $posicaoDataCompensacaoInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoDataCompensacaoInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL, $posicaoDataCompensacaoInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL, $posicaoDataCompensacaoInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_data_compensacao_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoDataCompensacaoFinal(1234); // WHERE posicao_data_compensacao_final = 1234
     * $query->filterByPosicaoDataCompensacaoFinal(array(12, 34)); // WHERE posicao_data_compensacao_final IN (12, 34)
     * $query->filterByPosicaoDataCompensacaoFinal(array('min' => 12)); // WHERE posicao_data_compensacao_final > 12
     * </code>
     *
     * @param     mixed $posicaoDataCompensacaoFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoDataCompensacaoFinal($posicaoDataCompensacaoFinal = null, $comparison = null)
    {
        if (is_array($posicaoDataCompensacaoFinal)) {
            $useMinMax = false;
            if (isset($posicaoDataCompensacaoFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL, $posicaoDataCompensacaoFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoDataCompensacaoFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL, $posicaoDataCompensacaoFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL, $posicaoDataCompensacaoFinal, $comparison);
    }

    /**
     * Filter the query on the formato_data_compensacao column
     *
     * Example usage:
     * <code>
     * $query->filterByFormatoDataCompensacao('fooValue');   // WHERE formato_data_compensacao = 'fooValue'
     * $query->filterByFormatoDataCompensacao('%fooValue%', Criteria::LIKE); // WHERE formato_data_compensacao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $formatoDataCompensacao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByFormatoDataCompensacao($formatoDataCompensacao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($formatoDataCompensacao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPENSACAO, $formatoDataCompensacao, $comparison);
    }

    /**
     * Filter the query on the posicao_campo_complementar_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoCampoComplementarInicial(1234); // WHERE posicao_campo_complementar_inicial = 1234
     * $query->filterByPosicaoCampoComplementarInicial(array(12, 34)); // WHERE posicao_campo_complementar_inicial IN (12, 34)
     * $query->filterByPosicaoCampoComplementarInicial(array('min' => 12)); // WHERE posicao_campo_complementar_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoCampoComplementarInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoCampoComplementarInicial($posicaoCampoComplementarInicial = null, $comparison = null)
    {
        if (is_array($posicaoCampoComplementarInicial)) {
            $useMinMax = false;
            if (isset($posicaoCampoComplementarInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL, $posicaoCampoComplementarInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoCampoComplementarInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL, $posicaoCampoComplementarInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL, $posicaoCampoComplementarInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_campo_complementar_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoCampoComplementarFinal(1234); // WHERE posicao_campo_complementar_final = 1234
     * $query->filterByPosicaoCampoComplementarFinal(array(12, 34)); // WHERE posicao_campo_complementar_final IN (12, 34)
     * $query->filterByPosicaoCampoComplementarFinal(array('min' => 12)); // WHERE posicao_campo_complementar_final > 12
     * </code>
     *
     * @param     mixed $posicaoCampoComplementarFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoCampoComplementarFinal($posicaoCampoComplementarFinal = null, $comparison = null)
    {
        if (is_array($posicaoCampoComplementarFinal)) {
            $useMinMax = false;
            if (isset($posicaoCampoComplementarFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL, $posicaoCampoComplementarFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoCampoComplementarFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL, $posicaoCampoComplementarFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL, $posicaoCampoComplementarFinal, $comparison);
    }

    /**
     * Filter the query on the formato_complementar column
     *
     * Example usage:
     * <code>
     * $query->filterByFormatoComplementar('fooValue');   // WHERE formato_complementar = 'fooValue'
     * $query->filterByFormatoComplementar('%fooValue%', Criteria::LIKE); // WHERE formato_complementar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $formatoComplementar The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByFormatoComplementar($formatoComplementar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($formatoComplementar)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_FORMATO_COMPLEMENTAR, $formatoComplementar, $comparison);
    }

    /**
     * Filter the query on the posicao_data_complementar_inicial column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoDataComplementarInicial(1234); // WHERE posicao_data_complementar_inicial = 1234
     * $query->filterByPosicaoDataComplementarInicial(array(12, 34)); // WHERE posicao_data_complementar_inicial IN (12, 34)
     * $query->filterByPosicaoDataComplementarInicial(array('min' => 12)); // WHERE posicao_data_complementar_inicial > 12
     * </code>
     *
     * @param     mixed $posicaoDataComplementarInicial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoDataComplementarInicial($posicaoDataComplementarInicial = null, $comparison = null)
    {
        if (is_array($posicaoDataComplementarInicial)) {
            $useMinMax = false;
            if (isset($posicaoDataComplementarInicial['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL, $posicaoDataComplementarInicial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoDataComplementarInicial['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL, $posicaoDataComplementarInicial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL, $posicaoDataComplementarInicial, $comparison);
    }

    /**
     * Filter the query on the posicao_data_complementar_final column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoDataComplementarFinal(1234); // WHERE posicao_data_complementar_final = 1234
     * $query->filterByPosicaoDataComplementarFinal(array(12, 34)); // WHERE posicao_data_complementar_final IN (12, 34)
     * $query->filterByPosicaoDataComplementarFinal(array('min' => 12)); // WHERE posicao_data_complementar_final > 12
     * </code>
     *
     * @param     mixed $posicaoDataComplementarFinal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByPosicaoDataComplementarFinal($posicaoDataComplementarFinal = null, $comparison = null)
    {
        if (is_array($posicaoDataComplementarFinal)) {
            $useMinMax = false;
            if (isset($posicaoDataComplementarFinal['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL, $posicaoDataComplementarFinal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoDataComplementarFinal['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL, $posicaoDataComplementarFinal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL, $posicaoDataComplementarFinal, $comparison);
    }

    /**
     * Filter the query on the formato_data_complementar column
     *
     * Example usage:
     * <code>
     * $query->filterByFormatoDataComplementar('fooValue');   // WHERE formato_data_complementar = 'fooValue'
     * $query->filterByFormatoDataComplementar('%fooValue%', Criteria::LIKE); // WHERE formato_data_complementar LIKE '%fooValue%'
     * </code>
     *
     * @param     string $formatoDataComplementar The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByFormatoDataComplementar($formatoDataComplementar = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($formatoDataComplementar)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPLEMENTAR, $formatoDataComplementar, $comparison);
    }

    /**
     * Filter the query on the codigo_retorno_baixa column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigoRetornoBaixa('fooValue');   // WHERE codigo_retorno_baixa = 'fooValue'
     * $query->filterByCodigoRetornoBaixa('%fooValue%', Criteria::LIKE); // WHERE codigo_retorno_baixa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigoRetornoBaixa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByCodigoRetornoBaixa($codigoRetornoBaixa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigoRetornoBaixa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_CODIGO_RETORNO_BAIXA, $codigoRetornoBaixa, $comparison);
    }

    /**
     * Filter the query on the codigo_retorno_resgistrado column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigoRetornoResgistrado('fooValue');   // WHERE codigo_retorno_resgistrado = 'fooValue'
     * $query->filterByCodigoRetornoResgistrado('%fooValue%', Criteria::LIKE); // WHERE codigo_retorno_resgistrado LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigoRetornoResgistrado The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByCodigoRetornoResgistrado($codigoRetornoResgistrado = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigoRetornoResgistrado)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RESGISTRADO, $codigoRetornoResgistrado, $comparison);
    }

    /**
     * Filter the query on the codigo_retorno_recusado column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigoRetornoRecusado('fooValue');   // WHERE codigo_retorno_recusado = 'fooValue'
     * $query->filterByCodigoRetornoRecusado('%fooValue%', Criteria::LIKE); // WHERE codigo_retorno_recusado LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigoRetornoRecusado The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByCodigoRetornoRecusado($codigoRetornoRecusado = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigoRetornoRecusado)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RECUSADO, $codigoRetornoRecusado, $comparison);
    }

    /**
     * Filter the query on the data_cadastrado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastrado('2011-03-14'); // WHERE data_cadastrado = '2011-03-14'
     * $query->filterByDataCadastrado('now'); // WHERE data_cadastrado = '2011-03-14'
     * $query->filterByDataCadastrado(array('max' => 'yesterday')); // WHERE data_cadastrado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastrado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByDataCadastrado($dataCadastrado = null, $comparison = null)
    {
        if (is_array($dataCadastrado)) {
            $useMinMax = false;
            if (isset($dataCadastrado['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_DATA_CADASTRADO, $dataCadastrado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastrado['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_DATA_CADASTRADO, $dataCadastrado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_DATA_CADASTRADO, $dataCadastrado, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(LayoutBancarioTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LayoutBancarioTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Banco object
     *
     * @param \ImaTelecomBundle\Model\Banco|ObjectCollection $banco the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function filterByBanco($banco, $comparison = null)
    {
        if ($banco instanceof \ImaTelecomBundle\Model\Banco) {
            return $this
                ->addUsingAlias(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, $banco->getLayoutBancarioId(), $comparison);
        } elseif ($banco instanceof ObjectCollection) {
            return $this
                ->useBancoQuery()
                ->filterByPrimaryKeys($banco->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBanco() only accepts arguments of type \ImaTelecomBundle\Model\Banco or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Banco relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function joinBanco($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Banco');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Banco');
        }

        return $this;
    }

    /**
     * Use the Banco relation Banco object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoQuery A secondary query class using the current class as primary query
     */
    public function useBancoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBanco($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Banco', '\ImaTelecomBundle\Model\BancoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLayoutBancario $layoutBancario Object to remove from the list of results
     *
     * @return $this|ChildLayoutBancarioQuery The current query, for fluid interface
     */
    public function prune($layoutBancario = null)
    {
        if ($layoutBancario) {
            $this->addUsingAlias(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, $layoutBancario->getIdlayoutBancario(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the layout_bancario table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LayoutBancarioTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LayoutBancarioTableMap::clearInstancePool();
            LayoutBancarioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LayoutBancarioTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LayoutBancarioTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LayoutBancarioTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LayoutBancarioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LayoutBancarioQuery
