<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\DadosFiscaisGeracao as ChildDadosFiscaisGeracao;
use ImaTelecomBundle\Model\DadosFiscaisGeracaoQuery as ChildDadosFiscaisGeracaoQuery;
use ImaTelecomBundle\Model\Map\DadosFiscaisGeracaoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'dados_fiscais_geracao' table.
 *
 *
 *
 * @method     ChildDadosFiscaisGeracaoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildDadosFiscaisGeracaoQuery orderByBoletoItemId($order = Criteria::ASC) Order by the boleto_item_id column
 * @method     ChildDadosFiscaisGeracaoQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildDadosFiscaisGeracaoQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildDadosFiscaisGeracaoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildDadosFiscaisGeracaoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildDadosFiscaisGeracaoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildDadosFiscaisGeracaoQuery groupById() Group by the id column
 * @method     ChildDadosFiscaisGeracaoQuery groupByBoletoItemId() Group by the boleto_item_id column
 * @method     ChildDadosFiscaisGeracaoQuery groupByStatus() Group by the status column
 * @method     ChildDadosFiscaisGeracaoQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildDadosFiscaisGeracaoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildDadosFiscaisGeracaoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildDadosFiscaisGeracaoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildDadosFiscaisGeracaoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDadosFiscaisGeracaoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDadosFiscaisGeracaoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDadosFiscaisGeracaoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDadosFiscaisGeracaoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDadosFiscaisGeracaoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDadosFiscaisGeracaoQuery leftJoinBoletoItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the BoletoItem relation
 * @method     ChildDadosFiscaisGeracaoQuery rightJoinBoletoItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BoletoItem relation
 * @method     ChildDadosFiscaisGeracaoQuery innerJoinBoletoItem($relationAlias = null) Adds a INNER JOIN clause to the query using the BoletoItem relation
 *
 * @method     ChildDadosFiscaisGeracaoQuery joinWithBoletoItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BoletoItem relation
 *
 * @method     ChildDadosFiscaisGeracaoQuery leftJoinWithBoletoItem() Adds a LEFT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildDadosFiscaisGeracaoQuery rightJoinWithBoletoItem() Adds a RIGHT JOIN clause and with to the query using the BoletoItem relation
 * @method     ChildDadosFiscaisGeracaoQuery innerJoinWithBoletoItem() Adds a INNER JOIN clause and with to the query using the BoletoItem relation
 *
 * @method     \ImaTelecomBundle\Model\BoletoItemQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildDadosFiscaisGeracao findOne(ConnectionInterface $con = null) Return the first ChildDadosFiscaisGeracao matching the query
 * @method     ChildDadosFiscaisGeracao findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDadosFiscaisGeracao matching the query, or a new ChildDadosFiscaisGeracao object populated from the query conditions when no match is found
 *
 * @method     ChildDadosFiscaisGeracao findOneById(int $id) Return the first ChildDadosFiscaisGeracao filtered by the id column
 * @method     ChildDadosFiscaisGeracao findOneByBoletoItemId(int $boleto_item_id) Return the first ChildDadosFiscaisGeracao filtered by the boleto_item_id column
 * @method     ChildDadosFiscaisGeracao findOneByStatus(string $status) Return the first ChildDadosFiscaisGeracao filtered by the status column
 * @method     ChildDadosFiscaisGeracao findOneByCompetenciaId(int $competencia_id) Return the first ChildDadosFiscaisGeracao filtered by the competencia_id column
 * @method     ChildDadosFiscaisGeracao findOneByDataCadastro(string $data_cadastro) Return the first ChildDadosFiscaisGeracao filtered by the data_cadastro column
 * @method     ChildDadosFiscaisGeracao findOneByDataAlterado(string $data_alterado) Return the first ChildDadosFiscaisGeracao filtered by the data_alterado column
 * @method     ChildDadosFiscaisGeracao findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildDadosFiscaisGeracao filtered by the usuario_alterado column *

 * @method     ChildDadosFiscaisGeracao requirePk($key, ConnectionInterface $con = null) Return the ChildDadosFiscaisGeracao by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscaisGeracao requireOne(ConnectionInterface $con = null) Return the first ChildDadosFiscaisGeracao matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDadosFiscaisGeracao requireOneById(int $id) Return the first ChildDadosFiscaisGeracao filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscaisGeracao requireOneByBoletoItemId(int $boleto_item_id) Return the first ChildDadosFiscaisGeracao filtered by the boleto_item_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscaisGeracao requireOneByStatus(string $status) Return the first ChildDadosFiscaisGeracao filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscaisGeracao requireOneByCompetenciaId(int $competencia_id) Return the first ChildDadosFiscaisGeracao filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscaisGeracao requireOneByDataCadastro(string $data_cadastro) Return the first ChildDadosFiscaisGeracao filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscaisGeracao requireOneByDataAlterado(string $data_alterado) Return the first ChildDadosFiscaisGeracao filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscaisGeracao requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildDadosFiscaisGeracao filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDadosFiscaisGeracao[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDadosFiscaisGeracao objects based on current ModelCriteria
 * @method     ChildDadosFiscaisGeracao[]|ObjectCollection findById(int $id) Return ChildDadosFiscaisGeracao objects filtered by the id column
 * @method     ChildDadosFiscaisGeracao[]|ObjectCollection findByBoletoItemId(int $boleto_item_id) Return ChildDadosFiscaisGeracao objects filtered by the boleto_item_id column
 * @method     ChildDadosFiscaisGeracao[]|ObjectCollection findByStatus(string $status) Return ChildDadosFiscaisGeracao objects filtered by the status column
 * @method     ChildDadosFiscaisGeracao[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildDadosFiscaisGeracao objects filtered by the competencia_id column
 * @method     ChildDadosFiscaisGeracao[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildDadosFiscaisGeracao objects filtered by the data_cadastro column
 * @method     ChildDadosFiscaisGeracao[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildDadosFiscaisGeracao objects filtered by the data_alterado column
 * @method     ChildDadosFiscaisGeracao[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildDadosFiscaisGeracao objects filtered by the usuario_alterado column
 * @method     ChildDadosFiscaisGeracao[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DadosFiscaisGeracaoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\DadosFiscaisGeracaoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\DadosFiscaisGeracao', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDadosFiscaisGeracaoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDadosFiscaisGeracaoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDadosFiscaisGeracaoQuery) {
            return $criteria;
        }
        $query = new ChildDadosFiscaisGeracaoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDadosFiscaisGeracao|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DadosFiscaisGeracaoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = DadosFiscaisGeracaoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscaisGeracao A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, boleto_item_id, status, competencia_id, data_cadastro, data_alterado, usuario_alterado FROM dados_fiscais_geracao WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDadosFiscaisGeracao $obj */
            $obj = new ChildDadosFiscaisGeracao();
            $obj->hydrate($row);
            DadosFiscaisGeracaoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDadosFiscaisGeracao|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the boleto_item_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBoletoItemId(1234); // WHERE boleto_item_id = 1234
     * $query->filterByBoletoItemId(array(12, 34)); // WHERE boleto_item_id IN (12, 34)
     * $query->filterByBoletoItemId(array('min' => 12)); // WHERE boleto_item_id > 12
     * </code>
     *
     * @see       filterByBoletoItem()
     *
     * @param     mixed $boletoItemId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterByBoletoItemId($boletoItemId = null, $comparison = null)
    {
        if (is_array($boletoItemId)) {
            $useMinMax = false;
            if (isset($boletoItemId['min'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_BOLETO_ITEM_ID, $boletoItemId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($boletoItemId['max'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_BOLETO_ITEM_ID, $boletoItemId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_BOLETO_ITEM_ID, $boletoItemId, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BoletoItem object
     *
     * @param \ImaTelecomBundle\Model\BoletoItem|ObjectCollection $boletoItem The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function filterByBoletoItem($boletoItem, $comparison = null)
    {
        if ($boletoItem instanceof \ImaTelecomBundle\Model\BoletoItem) {
            return $this
                ->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_BOLETO_ITEM_ID, $boletoItem->getIdboletoItem(), $comparison);
        } elseif ($boletoItem instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_BOLETO_ITEM_ID, $boletoItem->toKeyValue('PrimaryKey', 'IdboletoItem'), $comparison);
        } else {
            throw new PropelException('filterByBoletoItem() only accepts arguments of type \ImaTelecomBundle\Model\BoletoItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BoletoItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function joinBoletoItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BoletoItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BoletoItem');
        }

        return $this;
    }

    /**
     * Use the BoletoItem relation BoletoItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoItemQuery A secondary query class using the current class as primary query
     */
    public function useBoletoItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoletoItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BoletoItem', '\ImaTelecomBundle\Model\BoletoItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDadosFiscaisGeracao $dadosFiscaisGeracao Object to remove from the list of results
     *
     * @return $this|ChildDadosFiscaisGeracaoQuery The current query, for fluid interface
     */
    public function prune($dadosFiscaisGeracao = null)
    {
        if ($dadosFiscaisGeracao) {
            $this->addUsingAlias(DadosFiscaisGeracaoTableMap::COL_ID, $dadosFiscaisGeracao->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the dados_fiscais_geracao table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscaisGeracaoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DadosFiscaisGeracaoTableMap::clearInstancePool();
            DadosFiscaisGeracaoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscaisGeracaoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DadosFiscaisGeracaoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DadosFiscaisGeracaoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DadosFiscaisGeracaoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // DadosFiscaisGeracaoQuery
