<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Caixa as ChildCaixa;
use ImaTelecomBundle\Model\CaixaMovimento as ChildCaixaMovimento;
use ImaTelecomBundle\Model\CaixaMovimentoQuery as ChildCaixaMovimentoQuery;
use ImaTelecomBundle\Model\CaixaQuery as ChildCaixaQuery;
use ImaTelecomBundle\Model\ContasPagarParcelas as ChildContasPagarParcelas;
use ImaTelecomBundle\Model\ContasPagarParcelasQuery as ChildContasPagarParcelasQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\CaixaMovimentoTableMap;
use ImaTelecomBundle\Model\Map\CaixaTableMap;
use ImaTelecomBundle\Model\Map\ContasPagarParcelasTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'caixa' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class Caixa implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\CaixaTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idcaixa field.
     *
     * @var        int
     */
    protected $idcaixa;

    /**
     * The value for the tipo field.
     *
     * @var        string
     */
    protected $tipo;

    /**
     * The value for the data_pagamento field.
     *
     * @var        DateTime
     */
    protected $data_pagamento;

    /**
     * The value for the data_baixa field.
     *
     * @var        DateTime
     */
    protected $data_baixa;

    /**
     * The value for the usuario_baixa field.
     *
     * @var        int
     */
    protected $usuario_baixa;

    /**
     * The value for the valor_pago field.
     *
     * @var        string
     */
    protected $valor_pago;

    /**
     * The value for the valor_juros field.
     *
     * @var        string
     */
    protected $valor_juros;

    /**
     * The value for the valor_desconto field.
     *
     * @var        string
     */
    protected $valor_desconto;

    /**
     * The value for the observacao field.
     *
     * @var        string
     */
    protected $observacao;

    /**
     * The value for the forma_pagamento_id field.
     *
     * @var        int
     */
    protected $forma_pagamento_id;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildCaixaMovimento[] Collection to store aggregation of ChildCaixaMovimento objects.
     */
    protected $collCaixaMovimentos;
    protected $collCaixaMovimentosPartial;

    /**
     * @var        ObjectCollection|ChildContasPagarParcelas[] Collection to store aggregation of ChildContasPagarParcelas objects.
     */
    protected $collContasPagarParcelass;
    protected $collContasPagarParcelassPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCaixaMovimento[]
     */
    protected $caixaMovimentosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContasPagarParcelas[]
     */
    protected $contasPagarParcelassScheduledForDeletion = null;

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\Caixa object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Caixa</code> instance.  If
     * <code>obj</code> is an instance of <code>Caixa</code>, delegates to
     * <code>equals(Caixa)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Caixa The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idcaixa] column value.
     *
     * @return int
     */
    public function getIdcaixa()
    {
        return $this->idcaixa;
    }

    /**
     * Get the [tipo] column value.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Get the [optionally formatted] temporal [data_pagamento] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataPagamento($format = NULL)
    {
        if ($format === null) {
            return $this->data_pagamento;
        } else {
            return $this->data_pagamento instanceof \DateTimeInterface ? $this->data_pagamento->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_baixa] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataBaixa($format = NULL)
    {
        if ($format === null) {
            return $this->data_baixa;
        } else {
            return $this->data_baixa instanceof \DateTimeInterface ? $this->data_baixa->format($format) : null;
        }
    }

    /**
     * Get the [usuario_baixa] column value.
     *
     * @return int
     */
    public function getUsuarioBaixa()
    {
        return $this->usuario_baixa;
    }

    /**
     * Get the [valor_pago] column value.
     *
     * @return string
     */
    public function getValorPago()
    {
        return $this->valor_pago;
    }

    /**
     * Get the [valor_juros] column value.
     *
     * @return string
     */
    public function getValorJuros()
    {
        return $this->valor_juros;
    }

    /**
     * Get the [valor_desconto] column value.
     *
     * @return string
     */
    public function getValorDesconto()
    {
        return $this->valor_desconto;
    }

    /**
     * Get the [observacao] column value.
     *
     * @return string
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * Get the [forma_pagamento_id] column value.
     *
     * @return int
     */
    public function getFormaPagamentoId()
    {
        return $this->forma_pagamento_id;
    }

    /**
     * Set the value of [idcaixa] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setIdcaixa($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idcaixa !== $v) {
            $this->idcaixa = $v;
            $this->modifiedColumns[CaixaTableMap::COL_IDCAIXA] = true;
        }

        return $this;
    } // setIdcaixa()

    /**
     * Set the value of [tipo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setTipo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo !== $v) {
            $this->tipo = $v;
            $this->modifiedColumns[CaixaTableMap::COL_TIPO] = true;
        }

        return $this;
    } // setTipo()

    /**
     * Sets the value of [data_pagamento] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setDataPagamento($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_pagamento !== null || $dt !== null) {
            if ($this->data_pagamento === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_pagamento->format("Y-m-d H:i:s.u")) {
                $this->data_pagamento = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CaixaTableMap::COL_DATA_PAGAMENTO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataPagamento()

    /**
     * Sets the value of [data_baixa] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setDataBaixa($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_baixa !== null || $dt !== null) {
            if ($this->data_baixa === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_baixa->format("Y-m-d H:i:s.u")) {
                $this->data_baixa = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CaixaTableMap::COL_DATA_BAIXA] = true;
            }
        } // if either are not null

        return $this;
    } // setDataBaixa()

    /**
     * Set the value of [usuario_baixa] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setUsuarioBaixa($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_baixa !== $v) {
            $this->usuario_baixa = $v;
            $this->modifiedColumns[CaixaTableMap::COL_USUARIO_BAIXA] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioBaixa()

    /**
     * Set the value of [valor_pago] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setValorPago($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_pago !== $v) {
            $this->valor_pago = $v;
            $this->modifiedColumns[CaixaTableMap::COL_VALOR_PAGO] = true;
        }

        return $this;
    } // setValorPago()

    /**
     * Set the value of [valor_juros] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setValorJuros($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_juros !== $v) {
            $this->valor_juros = $v;
            $this->modifiedColumns[CaixaTableMap::COL_VALOR_JUROS] = true;
        }

        return $this;
    } // setValorJuros()

    /**
     * Set the value of [valor_desconto] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setValorDesconto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_desconto !== $v) {
            $this->valor_desconto = $v;
            $this->modifiedColumns[CaixaTableMap::COL_VALOR_DESCONTO] = true;
        }

        return $this;
    } // setValorDesconto()

    /**
     * Set the value of [observacao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setObservacao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->observacao !== $v) {
            $this->observacao = $v;
            $this->modifiedColumns[CaixaTableMap::COL_OBSERVACAO] = true;
        }

        return $this;
    } // setObservacao()

    /**
     * Set the value of [forma_pagamento_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function setFormaPagamentoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->forma_pagamento_id !== $v) {
            $this->forma_pagamento_id = $v;
            $this->modifiedColumns[CaixaTableMap::COL_FORMA_PAGAMENTO_ID] = true;
        }

        return $this;
    } // setFormaPagamentoId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CaixaTableMap::translateFieldName('Idcaixa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idcaixa = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CaixaTableMap::translateFieldName('Tipo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CaixaTableMap::translateFieldName('DataPagamento', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_pagamento = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CaixaTableMap::translateFieldName('DataBaixa', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_baixa = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CaixaTableMap::translateFieldName('UsuarioBaixa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_baixa = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CaixaTableMap::translateFieldName('ValorPago', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_pago = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CaixaTableMap::translateFieldName('ValorJuros', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_juros = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CaixaTableMap::translateFieldName('ValorDesconto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_desconto = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CaixaTableMap::translateFieldName('Observacao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->observacao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CaixaTableMap::translateFieldName('FormaPagamentoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->forma_pagamento_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = CaixaTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\Caixa'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUsuario !== null && $this->usuario_baixa !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CaixaTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCaixaQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aUsuario = null;
            $this->collCaixaMovimentos = null;

            $this->collContasPagarParcelass = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Caixa::setDeleted()
     * @see Caixa::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CaixaTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCaixaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CaixaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CaixaTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->caixaMovimentosScheduledForDeletion !== null) {
                if (!$this->caixaMovimentosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\CaixaMovimentoQuery::create()
                        ->filterByPrimaryKeys($this->caixaMovimentosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->caixaMovimentosScheduledForDeletion = null;
                }
            }

            if ($this->collCaixaMovimentos !== null) {
                foreach ($this->collCaixaMovimentos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->contasPagarParcelassScheduledForDeletion !== null) {
                if (!$this->contasPagarParcelassScheduledForDeletion->isEmpty()) {
                    foreach ($this->contasPagarParcelassScheduledForDeletion as $contasPagarParcelas) {
                        // need to save related object because we set the relation to null
                        $contasPagarParcelas->save($con);
                    }
                    $this->contasPagarParcelassScheduledForDeletion = null;
                }
            }

            if ($this->collContasPagarParcelass !== null) {
                foreach ($this->collContasPagarParcelass as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CaixaTableMap::COL_IDCAIXA] = true;
        if (null !== $this->idcaixa) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CaixaTableMap::COL_IDCAIXA . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CaixaTableMap::COL_IDCAIXA)) {
            $modifiedColumns[':p' . $index++]  = 'idcaixa';
        }
        if ($this->isColumnModified(CaixaTableMap::COL_TIPO)) {
            $modifiedColumns[':p' . $index++]  = 'tipo';
        }
        if ($this->isColumnModified(CaixaTableMap::COL_DATA_PAGAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'data_pagamento';
        }
        if ($this->isColumnModified(CaixaTableMap::COL_DATA_BAIXA)) {
            $modifiedColumns[':p' . $index++]  = 'data_baixa';
        }
        if ($this->isColumnModified(CaixaTableMap::COL_USUARIO_BAIXA)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_baixa';
        }
        if ($this->isColumnModified(CaixaTableMap::COL_VALOR_PAGO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_pago';
        }
        if ($this->isColumnModified(CaixaTableMap::COL_VALOR_JUROS)) {
            $modifiedColumns[':p' . $index++]  = 'valor_juros';
        }
        if ($this->isColumnModified(CaixaTableMap::COL_VALOR_DESCONTO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_desconto';
        }
        if ($this->isColumnModified(CaixaTableMap::COL_OBSERVACAO)) {
            $modifiedColumns[':p' . $index++]  = 'observacao';
        }
        if ($this->isColumnModified(CaixaTableMap::COL_FORMA_PAGAMENTO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'forma_pagamento_id';
        }

        $sql = sprintf(
            'INSERT INTO caixa (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idcaixa':
                        $stmt->bindValue($identifier, $this->idcaixa, PDO::PARAM_INT);
                        break;
                    case 'tipo':
                        $stmt->bindValue($identifier, $this->tipo, PDO::PARAM_STR);
                        break;
                    case 'data_pagamento':
                        $stmt->bindValue($identifier, $this->data_pagamento ? $this->data_pagamento->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_baixa':
                        $stmt->bindValue($identifier, $this->data_baixa ? $this->data_baixa->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_baixa':
                        $stmt->bindValue($identifier, $this->usuario_baixa, PDO::PARAM_INT);
                        break;
                    case 'valor_pago':
                        $stmt->bindValue($identifier, $this->valor_pago, PDO::PARAM_STR);
                        break;
                    case 'valor_juros':
                        $stmt->bindValue($identifier, $this->valor_juros, PDO::PARAM_STR);
                        break;
                    case 'valor_desconto':
                        $stmt->bindValue($identifier, $this->valor_desconto, PDO::PARAM_STR);
                        break;
                    case 'observacao':
                        $stmt->bindValue($identifier, $this->observacao, PDO::PARAM_STR);
                        break;
                    case 'forma_pagamento_id':
                        $stmt->bindValue($identifier, $this->forma_pagamento_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdcaixa($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CaixaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdcaixa();
                break;
            case 1:
                return $this->getTipo();
                break;
            case 2:
                return $this->getDataPagamento();
                break;
            case 3:
                return $this->getDataBaixa();
                break;
            case 4:
                return $this->getUsuarioBaixa();
                break;
            case 5:
                return $this->getValorPago();
                break;
            case 6:
                return $this->getValorJuros();
                break;
            case 7:
                return $this->getValorDesconto();
                break;
            case 8:
                return $this->getObservacao();
                break;
            case 9:
                return $this->getFormaPagamentoId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Caixa'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Caixa'][$this->hashCode()] = true;
        $keys = CaixaTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdcaixa(),
            $keys[1] => $this->getTipo(),
            $keys[2] => $this->getDataPagamento(),
            $keys[3] => $this->getDataBaixa(),
            $keys[4] => $this->getUsuarioBaixa(),
            $keys[5] => $this->getValorPago(),
            $keys[6] => $this->getValorJuros(),
            $keys[7] => $this->getValorDesconto(),
            $keys[8] => $this->getObservacao(),
            $keys[9] => $this->getFormaPagamentoId(),
        );
        if ($result[$keys[2]] instanceof \DateTime) {
            $result[$keys[2]] = $result[$keys[2]]->format('c');
        }

        if ($result[$keys[3]] instanceof \DateTime) {
            $result[$keys[3]] = $result[$keys[3]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCaixaMovimentos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'caixaMovimentos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'caixa_movimentos';
                        break;
                    default:
                        $key = 'CaixaMovimentos';
                }

                $result[$key] = $this->collCaixaMovimentos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collContasPagarParcelass) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contasPagarParcelass';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contas_pagar_parcelass';
                        break;
                    default:
                        $key = 'ContasPagarParcelass';
                }

                $result[$key] = $this->collContasPagarParcelass->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\Caixa
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CaixaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\Caixa
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdcaixa($value);
                break;
            case 1:
                $this->setTipo($value);
                break;
            case 2:
                $this->setDataPagamento($value);
                break;
            case 3:
                $this->setDataBaixa($value);
                break;
            case 4:
                $this->setUsuarioBaixa($value);
                break;
            case 5:
                $this->setValorPago($value);
                break;
            case 6:
                $this->setValorJuros($value);
                break;
            case 7:
                $this->setValorDesconto($value);
                break;
            case 8:
                $this->setObservacao($value);
                break;
            case 9:
                $this->setFormaPagamentoId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CaixaTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdcaixa($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTipo($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDataPagamento($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDataBaixa($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUsuarioBaixa($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setValorPago($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setValorJuros($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setValorDesconto($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setObservacao($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setFormaPagamentoId($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CaixaTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CaixaTableMap::COL_IDCAIXA)) {
            $criteria->add(CaixaTableMap::COL_IDCAIXA, $this->idcaixa);
        }
        if ($this->isColumnModified(CaixaTableMap::COL_TIPO)) {
            $criteria->add(CaixaTableMap::COL_TIPO, $this->tipo);
        }
        if ($this->isColumnModified(CaixaTableMap::COL_DATA_PAGAMENTO)) {
            $criteria->add(CaixaTableMap::COL_DATA_PAGAMENTO, $this->data_pagamento);
        }
        if ($this->isColumnModified(CaixaTableMap::COL_DATA_BAIXA)) {
            $criteria->add(CaixaTableMap::COL_DATA_BAIXA, $this->data_baixa);
        }
        if ($this->isColumnModified(CaixaTableMap::COL_USUARIO_BAIXA)) {
            $criteria->add(CaixaTableMap::COL_USUARIO_BAIXA, $this->usuario_baixa);
        }
        if ($this->isColumnModified(CaixaTableMap::COL_VALOR_PAGO)) {
            $criteria->add(CaixaTableMap::COL_VALOR_PAGO, $this->valor_pago);
        }
        if ($this->isColumnModified(CaixaTableMap::COL_VALOR_JUROS)) {
            $criteria->add(CaixaTableMap::COL_VALOR_JUROS, $this->valor_juros);
        }
        if ($this->isColumnModified(CaixaTableMap::COL_VALOR_DESCONTO)) {
            $criteria->add(CaixaTableMap::COL_VALOR_DESCONTO, $this->valor_desconto);
        }
        if ($this->isColumnModified(CaixaTableMap::COL_OBSERVACAO)) {
            $criteria->add(CaixaTableMap::COL_OBSERVACAO, $this->observacao);
        }
        if ($this->isColumnModified(CaixaTableMap::COL_FORMA_PAGAMENTO_ID)) {
            $criteria->add(CaixaTableMap::COL_FORMA_PAGAMENTO_ID, $this->forma_pagamento_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCaixaQuery::create();
        $criteria->add(CaixaTableMap::COL_IDCAIXA, $this->idcaixa);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdcaixa();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdcaixa();
    }

    /**
     * Generic method to set the primary key (idcaixa column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdcaixa($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdcaixa();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\Caixa (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTipo($this->getTipo());
        $copyObj->setDataPagamento($this->getDataPagamento());
        $copyObj->setDataBaixa($this->getDataBaixa());
        $copyObj->setUsuarioBaixa($this->getUsuarioBaixa());
        $copyObj->setValorPago($this->getValorPago());
        $copyObj->setValorJuros($this->getValorJuros());
        $copyObj->setValorDesconto($this->getValorDesconto());
        $copyObj->setObservacao($this->getObservacao());
        $copyObj->setFormaPagamentoId($this->getFormaPagamentoId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCaixaMovimentos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCaixaMovimento($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getContasPagarParcelass() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContasPagarParcelas($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdcaixa(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\Caixa Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioBaixa(NULL);
        } else {
            $this->setUsuarioBaixa($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addCaixa($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_baixa !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_baixa, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addCaixas($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CaixaMovimento' == $relationName) {
            return $this->initCaixaMovimentos();
        }
        if ('ContasPagarParcelas' == $relationName) {
            return $this->initContasPagarParcelass();
        }
    }

    /**
     * Clears out the collCaixaMovimentos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCaixaMovimentos()
     */
    public function clearCaixaMovimentos()
    {
        $this->collCaixaMovimentos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCaixaMovimentos collection loaded partially.
     */
    public function resetPartialCaixaMovimentos($v = true)
    {
        $this->collCaixaMovimentosPartial = $v;
    }

    /**
     * Initializes the collCaixaMovimentos collection.
     *
     * By default this just sets the collCaixaMovimentos collection to an empty array (like clearcollCaixaMovimentos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCaixaMovimentos($overrideExisting = true)
    {
        if (null !== $this->collCaixaMovimentos && !$overrideExisting) {
            return;
        }

        $collectionClassName = CaixaMovimentoTableMap::getTableMap()->getCollectionClassName();

        $this->collCaixaMovimentos = new $collectionClassName;
        $this->collCaixaMovimentos->setModel('\ImaTelecomBundle\Model\CaixaMovimento');
    }

    /**
     * Gets an array of ChildCaixaMovimento objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCaixa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCaixaMovimento[] List of ChildCaixaMovimento objects
     * @throws PropelException
     */
    public function getCaixaMovimentos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCaixaMovimentosPartial && !$this->isNew();
        if (null === $this->collCaixaMovimentos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCaixaMovimentos) {
                // return empty collection
                $this->initCaixaMovimentos();
            } else {
                $collCaixaMovimentos = ChildCaixaMovimentoQuery::create(null, $criteria)
                    ->filterByCaixa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCaixaMovimentosPartial && count($collCaixaMovimentos)) {
                        $this->initCaixaMovimentos(false);

                        foreach ($collCaixaMovimentos as $obj) {
                            if (false == $this->collCaixaMovimentos->contains($obj)) {
                                $this->collCaixaMovimentos->append($obj);
                            }
                        }

                        $this->collCaixaMovimentosPartial = true;
                    }

                    return $collCaixaMovimentos;
                }

                if ($partial && $this->collCaixaMovimentos) {
                    foreach ($this->collCaixaMovimentos as $obj) {
                        if ($obj->isNew()) {
                            $collCaixaMovimentos[] = $obj;
                        }
                    }
                }

                $this->collCaixaMovimentos = $collCaixaMovimentos;
                $this->collCaixaMovimentosPartial = false;
            }
        }

        return $this->collCaixaMovimentos;
    }

    /**
     * Sets a collection of ChildCaixaMovimento objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $caixaMovimentos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCaixa The current object (for fluent API support)
     */
    public function setCaixaMovimentos(Collection $caixaMovimentos, ConnectionInterface $con = null)
    {
        /** @var ChildCaixaMovimento[] $caixaMovimentosToDelete */
        $caixaMovimentosToDelete = $this->getCaixaMovimentos(new Criteria(), $con)->diff($caixaMovimentos);


        $this->caixaMovimentosScheduledForDeletion = $caixaMovimentosToDelete;

        foreach ($caixaMovimentosToDelete as $caixaMovimentoRemoved) {
            $caixaMovimentoRemoved->setCaixa(null);
        }

        $this->collCaixaMovimentos = null;
        foreach ($caixaMovimentos as $caixaMovimento) {
            $this->addCaixaMovimento($caixaMovimento);
        }

        $this->collCaixaMovimentos = $caixaMovimentos;
        $this->collCaixaMovimentosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CaixaMovimento objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CaixaMovimento objects.
     * @throws PropelException
     */
    public function countCaixaMovimentos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCaixaMovimentosPartial && !$this->isNew();
        if (null === $this->collCaixaMovimentos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCaixaMovimentos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCaixaMovimentos());
            }

            $query = ChildCaixaMovimentoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCaixa($this)
                ->count($con);
        }

        return count($this->collCaixaMovimentos);
    }

    /**
     * Method called to associate a ChildCaixaMovimento object to this object
     * through the ChildCaixaMovimento foreign key attribute.
     *
     * @param  ChildCaixaMovimento $l ChildCaixaMovimento
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function addCaixaMovimento(ChildCaixaMovimento $l)
    {
        if ($this->collCaixaMovimentos === null) {
            $this->initCaixaMovimentos();
            $this->collCaixaMovimentosPartial = true;
        }

        if (!$this->collCaixaMovimentos->contains($l)) {
            $this->doAddCaixaMovimento($l);

            if ($this->caixaMovimentosScheduledForDeletion and $this->caixaMovimentosScheduledForDeletion->contains($l)) {
                $this->caixaMovimentosScheduledForDeletion->remove($this->caixaMovimentosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCaixaMovimento $caixaMovimento The ChildCaixaMovimento object to add.
     */
    protected function doAddCaixaMovimento(ChildCaixaMovimento $caixaMovimento)
    {
        $this->collCaixaMovimentos[]= $caixaMovimento;
        $caixaMovimento->setCaixa($this);
    }

    /**
     * @param  ChildCaixaMovimento $caixaMovimento The ChildCaixaMovimento object to remove.
     * @return $this|ChildCaixa The current object (for fluent API support)
     */
    public function removeCaixaMovimento(ChildCaixaMovimento $caixaMovimento)
    {
        if ($this->getCaixaMovimentos()->contains($caixaMovimento)) {
            $pos = $this->collCaixaMovimentos->search($caixaMovimento);
            $this->collCaixaMovimentos->remove($pos);
            if (null === $this->caixaMovimentosScheduledForDeletion) {
                $this->caixaMovimentosScheduledForDeletion = clone $this->collCaixaMovimentos;
                $this->caixaMovimentosScheduledForDeletion->clear();
            }
            $this->caixaMovimentosScheduledForDeletion[]= clone $caixaMovimento;
            $caixaMovimento->setCaixa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Caixa is new, it will return
     * an empty collection; or if this Caixa has previously
     * been saved, it will retrieve related CaixaMovimentos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Caixa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCaixaMovimento[] List of ChildCaixaMovimento objects
     */
    public function getCaixaMovimentosJoinFormaPagamento(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCaixaMovimentoQuery::create(null, $criteria);
        $query->joinWith('FormaPagamento', $joinBehavior);

        return $this->getCaixaMovimentos($query, $con);
    }

    /**
     * Clears out the collContasPagarParcelass collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContasPagarParcelass()
     */
    public function clearContasPagarParcelass()
    {
        $this->collContasPagarParcelass = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContasPagarParcelass collection loaded partially.
     */
    public function resetPartialContasPagarParcelass($v = true)
    {
        $this->collContasPagarParcelassPartial = $v;
    }

    /**
     * Initializes the collContasPagarParcelass collection.
     *
     * By default this just sets the collContasPagarParcelass collection to an empty array (like clearcollContasPagarParcelass());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContasPagarParcelass($overrideExisting = true)
    {
        if (null !== $this->collContasPagarParcelass && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContasPagarParcelasTableMap::getTableMap()->getCollectionClassName();

        $this->collContasPagarParcelass = new $collectionClassName;
        $this->collContasPagarParcelass->setModel('\ImaTelecomBundle\Model\ContasPagarParcelas');
    }

    /**
     * Gets an array of ChildContasPagarParcelas objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCaixa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContasPagarParcelas[] List of ChildContasPagarParcelas objects
     * @throws PropelException
     */
    public function getContasPagarParcelass(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarParcelassPartial && !$this->isNew();
        if (null === $this->collContasPagarParcelass || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContasPagarParcelass) {
                // return empty collection
                $this->initContasPagarParcelass();
            } else {
                $collContasPagarParcelass = ChildContasPagarParcelasQuery::create(null, $criteria)
                    ->filterByCaixa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContasPagarParcelassPartial && count($collContasPagarParcelass)) {
                        $this->initContasPagarParcelass(false);

                        foreach ($collContasPagarParcelass as $obj) {
                            if (false == $this->collContasPagarParcelass->contains($obj)) {
                                $this->collContasPagarParcelass->append($obj);
                            }
                        }

                        $this->collContasPagarParcelassPartial = true;
                    }

                    return $collContasPagarParcelass;
                }

                if ($partial && $this->collContasPagarParcelass) {
                    foreach ($this->collContasPagarParcelass as $obj) {
                        if ($obj->isNew()) {
                            $collContasPagarParcelass[] = $obj;
                        }
                    }
                }

                $this->collContasPagarParcelass = $collContasPagarParcelass;
                $this->collContasPagarParcelassPartial = false;
            }
        }

        return $this->collContasPagarParcelass;
    }

    /**
     * Sets a collection of ChildContasPagarParcelas objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contasPagarParcelass A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCaixa The current object (for fluent API support)
     */
    public function setContasPagarParcelass(Collection $contasPagarParcelass, ConnectionInterface $con = null)
    {
        /** @var ChildContasPagarParcelas[] $contasPagarParcelassToDelete */
        $contasPagarParcelassToDelete = $this->getContasPagarParcelass(new Criteria(), $con)->diff($contasPagarParcelass);


        $this->contasPagarParcelassScheduledForDeletion = $contasPagarParcelassToDelete;

        foreach ($contasPagarParcelassToDelete as $contasPagarParcelasRemoved) {
            $contasPagarParcelasRemoved->setCaixa(null);
        }

        $this->collContasPagarParcelass = null;
        foreach ($contasPagarParcelass as $contasPagarParcelas) {
            $this->addContasPagarParcelas($contasPagarParcelas);
        }

        $this->collContasPagarParcelass = $contasPagarParcelass;
        $this->collContasPagarParcelassPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ContasPagarParcelas objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ContasPagarParcelas objects.
     * @throws PropelException
     */
    public function countContasPagarParcelass(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContasPagarParcelassPartial && !$this->isNew();
        if (null === $this->collContasPagarParcelass || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContasPagarParcelass) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContasPagarParcelass());
            }

            $query = ChildContasPagarParcelasQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCaixa($this)
                ->count($con);
        }

        return count($this->collContasPagarParcelass);
    }

    /**
     * Method called to associate a ChildContasPagarParcelas object to this object
     * through the ChildContasPagarParcelas foreign key attribute.
     *
     * @param  ChildContasPagarParcelas $l ChildContasPagarParcelas
     * @return $this|\ImaTelecomBundle\Model\Caixa The current object (for fluent API support)
     */
    public function addContasPagarParcelas(ChildContasPagarParcelas $l)
    {
        if ($this->collContasPagarParcelass === null) {
            $this->initContasPagarParcelass();
            $this->collContasPagarParcelassPartial = true;
        }

        if (!$this->collContasPagarParcelass->contains($l)) {
            $this->doAddContasPagarParcelas($l);

            if ($this->contasPagarParcelassScheduledForDeletion and $this->contasPagarParcelassScheduledForDeletion->contains($l)) {
                $this->contasPagarParcelassScheduledForDeletion->remove($this->contasPagarParcelassScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContasPagarParcelas $contasPagarParcelas The ChildContasPagarParcelas object to add.
     */
    protected function doAddContasPagarParcelas(ChildContasPagarParcelas $contasPagarParcelas)
    {
        $this->collContasPagarParcelass[]= $contasPagarParcelas;
        $contasPagarParcelas->setCaixa($this);
    }

    /**
     * @param  ChildContasPagarParcelas $contasPagarParcelas The ChildContasPagarParcelas object to remove.
     * @return $this|ChildCaixa The current object (for fluent API support)
     */
    public function removeContasPagarParcelas(ChildContasPagarParcelas $contasPagarParcelas)
    {
        if ($this->getContasPagarParcelass()->contains($contasPagarParcelas)) {
            $pos = $this->collContasPagarParcelass->search($contasPagarParcelas);
            $this->collContasPagarParcelass->remove($pos);
            if (null === $this->contasPagarParcelassScheduledForDeletion) {
                $this->contasPagarParcelassScheduledForDeletion = clone $this->collContasPagarParcelass;
                $this->contasPagarParcelassScheduledForDeletion->clear();
            }
            $this->contasPagarParcelassScheduledForDeletion[]= $contasPagarParcelas;
            $contasPagarParcelas->setCaixa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Caixa is new, it will return
     * an empty collection; or if this Caixa has previously
     * been saved, it will retrieve related ContasPagarParcelass from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Caixa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContasPagarParcelas[] List of ChildContasPagarParcelas objects
     */
    public function getContasPagarParcelassJoinContasPagar(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContasPagarParcelasQuery::create(null, $criteria);
        $query->joinWith('ContasPagar', $joinBehavior);

        return $this->getContasPagarParcelass($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeCaixa($this);
        }
        $this->idcaixa = null;
        $this->tipo = null;
        $this->data_pagamento = null;
        $this->data_baixa = null;
        $this->usuario_baixa = null;
        $this->valor_pago = null;
        $this->valor_juros = null;
        $this->valor_desconto = null;
        $this->observacao = null;
        $this->forma_pagamento_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCaixaMovimentos) {
                foreach ($this->collCaixaMovimentos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collContasPagarParcelass) {
                foreach ($this->collContasPagarParcelass as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCaixaMovimentos = null;
        $this->collContasPagarParcelass = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CaixaTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
