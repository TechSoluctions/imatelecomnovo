<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Fornecedor as ChildFornecedor;
use ImaTelecomBundle\Model\FornecedorQuery as ChildFornecedorQuery;
use ImaTelecomBundle\Model\Map\FornecedorTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'fornecedor' table.
 *
 *
 *
 * @method     ChildFornecedorQuery orderByIdfornecedor($order = Criteria::ASC) Order by the idfornecedor column
 * @method     ChildFornecedorQuery orderByClienteId($order = Criteria::ASC) Order by the cliente_id column
 * @method     ChildFornecedorQuery orderByPessoaId($order = Criteria::ASC) Order by the pessoa_id column
 * @method     ChildFornecedorQuery orderByBancoAgenciaContaId($order = Criteria::ASC) Order by the banco_agencia_conta_id column
 * @method     ChildFornecedorQuery orderByObservacao($order = Criteria::ASC) Order by the observacao column
 * @method     ChildFornecedorQuery orderByAtivo($order = Criteria::ASC) Order by the ativo column
 * @method     ChildFornecedorQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildFornecedorQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildFornecedorQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildFornecedorQuery groupByIdfornecedor() Group by the idfornecedor column
 * @method     ChildFornecedorQuery groupByClienteId() Group by the cliente_id column
 * @method     ChildFornecedorQuery groupByPessoaId() Group by the pessoa_id column
 * @method     ChildFornecedorQuery groupByBancoAgenciaContaId() Group by the banco_agencia_conta_id column
 * @method     ChildFornecedorQuery groupByObservacao() Group by the observacao column
 * @method     ChildFornecedorQuery groupByAtivo() Group by the ativo column
 * @method     ChildFornecedorQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildFornecedorQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildFornecedorQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildFornecedorQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFornecedorQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFornecedorQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFornecedorQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFornecedorQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFornecedorQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFornecedorQuery leftJoinBancoAgenciaConta($relationAlias = null) Adds a LEFT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildFornecedorQuery rightJoinBancoAgenciaConta($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BancoAgenciaConta relation
 * @method     ChildFornecedorQuery innerJoinBancoAgenciaConta($relationAlias = null) Adds a INNER JOIN clause to the query using the BancoAgenciaConta relation
 *
 * @method     ChildFornecedorQuery joinWithBancoAgenciaConta($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildFornecedorQuery leftJoinWithBancoAgenciaConta() Adds a LEFT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildFornecedorQuery rightJoinWithBancoAgenciaConta() Adds a RIGHT JOIN clause and with to the query using the BancoAgenciaConta relation
 * @method     ChildFornecedorQuery innerJoinWithBancoAgenciaConta() Adds a INNER JOIN clause and with to the query using the BancoAgenciaConta relation
 *
 * @method     ChildFornecedorQuery leftJoinCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cliente relation
 * @method     ChildFornecedorQuery rightJoinCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cliente relation
 * @method     ChildFornecedorQuery innerJoinCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the Cliente relation
 *
 * @method     ChildFornecedorQuery joinWithCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cliente relation
 *
 * @method     ChildFornecedorQuery leftJoinWithCliente() Adds a LEFT JOIN clause and with to the query using the Cliente relation
 * @method     ChildFornecedorQuery rightJoinWithCliente() Adds a RIGHT JOIN clause and with to the query using the Cliente relation
 * @method     ChildFornecedorQuery innerJoinWithCliente() Adds a INNER JOIN clause and with to the query using the Cliente relation
 *
 * @method     ChildFornecedorQuery leftJoinPessoa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pessoa relation
 * @method     ChildFornecedorQuery rightJoinPessoa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pessoa relation
 * @method     ChildFornecedorQuery innerJoinPessoa($relationAlias = null) Adds a INNER JOIN clause to the query using the Pessoa relation
 *
 * @method     ChildFornecedorQuery joinWithPessoa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pessoa relation
 *
 * @method     ChildFornecedorQuery leftJoinWithPessoa() Adds a LEFT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildFornecedorQuery rightJoinWithPessoa() Adds a RIGHT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildFornecedorQuery innerJoinWithPessoa() Adds a INNER JOIN clause and with to the query using the Pessoa relation
 *
 * @method     ChildFornecedorQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildFornecedorQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildFornecedorQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildFornecedorQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildFornecedorQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildFornecedorQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildFornecedorQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildFornecedorQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildFornecedorQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildFornecedorQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildFornecedorQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildFornecedorQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildFornecedorQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildFornecedorQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildFornecedorQuery leftJoinContasPagar($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContasPagar relation
 * @method     ChildFornecedorQuery rightJoinContasPagar($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContasPagar relation
 * @method     ChildFornecedorQuery innerJoinContasPagar($relationAlias = null) Adds a INNER JOIN clause to the query using the ContasPagar relation
 *
 * @method     ChildFornecedorQuery joinWithContasPagar($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContasPagar relation
 *
 * @method     ChildFornecedorQuery leftJoinWithContasPagar() Adds a LEFT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildFornecedorQuery rightJoinWithContasPagar() Adds a RIGHT JOIN clause and with to the query using the ContasPagar relation
 * @method     ChildFornecedorQuery innerJoinWithContasPagar() Adds a INNER JOIN clause and with to the query using the ContasPagar relation
 *
 * @method     \ImaTelecomBundle\Model\BancoAgenciaContaQuery|\ImaTelecomBundle\Model\ClienteQuery|\ImaTelecomBundle\Model\PessoaQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\ContasPagarQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFornecedor findOne(ConnectionInterface $con = null) Return the first ChildFornecedor matching the query
 * @method     ChildFornecedor findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFornecedor matching the query, or a new ChildFornecedor object populated from the query conditions when no match is found
 *
 * @method     ChildFornecedor findOneByIdfornecedor(int $idfornecedor) Return the first ChildFornecedor filtered by the idfornecedor column
 * @method     ChildFornecedor findOneByClienteId(int $cliente_id) Return the first ChildFornecedor filtered by the cliente_id column
 * @method     ChildFornecedor findOneByPessoaId(int $pessoa_id) Return the first ChildFornecedor filtered by the pessoa_id column
 * @method     ChildFornecedor findOneByBancoAgenciaContaId(int $banco_agencia_conta_id) Return the first ChildFornecedor filtered by the banco_agencia_conta_id column
 * @method     ChildFornecedor findOneByObservacao(string $observacao) Return the first ChildFornecedor filtered by the observacao column
 * @method     ChildFornecedor findOneByAtivo(boolean $ativo) Return the first ChildFornecedor filtered by the ativo column
 * @method     ChildFornecedor findOneByDataCadastro(string $data_cadastro) Return the first ChildFornecedor filtered by the data_cadastro column
 * @method     ChildFornecedor findOneByDataAlterado(string $data_alterado) Return the first ChildFornecedor filtered by the data_alterado column
 * @method     ChildFornecedor findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildFornecedor filtered by the usuario_alterado column *

 * @method     ChildFornecedor requirePk($key, ConnectionInterface $con = null) Return the ChildFornecedor by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFornecedor requireOne(ConnectionInterface $con = null) Return the first ChildFornecedor matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFornecedor requireOneByIdfornecedor(int $idfornecedor) Return the first ChildFornecedor filtered by the idfornecedor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFornecedor requireOneByClienteId(int $cliente_id) Return the first ChildFornecedor filtered by the cliente_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFornecedor requireOneByPessoaId(int $pessoa_id) Return the first ChildFornecedor filtered by the pessoa_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFornecedor requireOneByBancoAgenciaContaId(int $banco_agencia_conta_id) Return the first ChildFornecedor filtered by the banco_agencia_conta_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFornecedor requireOneByObservacao(string $observacao) Return the first ChildFornecedor filtered by the observacao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFornecedor requireOneByAtivo(boolean $ativo) Return the first ChildFornecedor filtered by the ativo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFornecedor requireOneByDataCadastro(string $data_cadastro) Return the first ChildFornecedor filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFornecedor requireOneByDataAlterado(string $data_alterado) Return the first ChildFornecedor filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFornecedor requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildFornecedor filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFornecedor[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFornecedor objects based on current ModelCriteria
 * @method     ChildFornecedor[]|ObjectCollection findByIdfornecedor(int $idfornecedor) Return ChildFornecedor objects filtered by the idfornecedor column
 * @method     ChildFornecedor[]|ObjectCollection findByClienteId(int $cliente_id) Return ChildFornecedor objects filtered by the cliente_id column
 * @method     ChildFornecedor[]|ObjectCollection findByPessoaId(int $pessoa_id) Return ChildFornecedor objects filtered by the pessoa_id column
 * @method     ChildFornecedor[]|ObjectCollection findByBancoAgenciaContaId(int $banco_agencia_conta_id) Return ChildFornecedor objects filtered by the banco_agencia_conta_id column
 * @method     ChildFornecedor[]|ObjectCollection findByObservacao(string $observacao) Return ChildFornecedor objects filtered by the observacao column
 * @method     ChildFornecedor[]|ObjectCollection findByAtivo(boolean $ativo) Return ChildFornecedor objects filtered by the ativo column
 * @method     ChildFornecedor[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildFornecedor objects filtered by the data_cadastro column
 * @method     ChildFornecedor[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildFornecedor objects filtered by the data_alterado column
 * @method     ChildFornecedor[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildFornecedor objects filtered by the usuario_alterado column
 * @method     ChildFornecedor[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FornecedorQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\FornecedorQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\Fornecedor', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFornecedorQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFornecedorQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFornecedorQuery) {
            return $criteria;
        }
        $query = new ChildFornecedorQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFornecedor|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FornecedorTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FornecedorTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFornecedor A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idfornecedor, cliente_id, pessoa_id, banco_agencia_conta_id, observacao, ativo, data_cadastro, data_alterado, usuario_alterado FROM fornecedor WHERE idfornecedor = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFornecedor $obj */
            $obj = new ChildFornecedor();
            $obj->hydrate($row);
            FornecedorTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFornecedor|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FornecedorTableMap::COL_IDFORNECEDOR, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FornecedorTableMap::COL_IDFORNECEDOR, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idfornecedor column
     *
     * Example usage:
     * <code>
     * $query->filterByIdfornecedor(1234); // WHERE idfornecedor = 1234
     * $query->filterByIdfornecedor(array(12, 34)); // WHERE idfornecedor IN (12, 34)
     * $query->filterByIdfornecedor(array('min' => 12)); // WHERE idfornecedor > 12
     * </code>
     *
     * @param     mixed $idfornecedor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByIdfornecedor($idfornecedor = null, $comparison = null)
    {
        if (is_array($idfornecedor)) {
            $useMinMax = false;
            if (isset($idfornecedor['min'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_IDFORNECEDOR, $idfornecedor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idfornecedor['max'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_IDFORNECEDOR, $idfornecedor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FornecedorTableMap::COL_IDFORNECEDOR, $idfornecedor, $comparison);
    }

    /**
     * Filter the query on the cliente_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClienteId(1234); // WHERE cliente_id = 1234
     * $query->filterByClienteId(array(12, 34)); // WHERE cliente_id IN (12, 34)
     * $query->filterByClienteId(array('min' => 12)); // WHERE cliente_id > 12
     * </code>
     *
     * @see       filterByCliente()
     *
     * @param     mixed $clienteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByClienteId($clienteId = null, $comparison = null)
    {
        if (is_array($clienteId)) {
            $useMinMax = false;
            if (isset($clienteId['min'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_CLIENTE_ID, $clienteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clienteId['max'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_CLIENTE_ID, $clienteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FornecedorTableMap::COL_CLIENTE_ID, $clienteId, $comparison);
    }

    /**
     * Filter the query on the pessoa_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPessoaId(1234); // WHERE pessoa_id = 1234
     * $query->filterByPessoaId(array(12, 34)); // WHERE pessoa_id IN (12, 34)
     * $query->filterByPessoaId(array('min' => 12)); // WHERE pessoa_id > 12
     * </code>
     *
     * @see       filterByPessoa()
     *
     * @param     mixed $pessoaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByPessoaId($pessoaId = null, $comparison = null)
    {
        if (is_array($pessoaId)) {
            $useMinMax = false;
            if (isset($pessoaId['min'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_PESSOA_ID, $pessoaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pessoaId['max'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_PESSOA_ID, $pessoaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FornecedorTableMap::COL_PESSOA_ID, $pessoaId, $comparison);
    }

    /**
     * Filter the query on the banco_agencia_conta_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBancoAgenciaContaId(1234); // WHERE banco_agencia_conta_id = 1234
     * $query->filterByBancoAgenciaContaId(array(12, 34)); // WHERE banco_agencia_conta_id IN (12, 34)
     * $query->filterByBancoAgenciaContaId(array('min' => 12)); // WHERE banco_agencia_conta_id > 12
     * </code>
     *
     * @see       filterByBancoAgenciaConta()
     *
     * @param     mixed $bancoAgenciaContaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByBancoAgenciaContaId($bancoAgenciaContaId = null, $comparison = null)
    {
        if (is_array($bancoAgenciaContaId)) {
            $useMinMax = false;
            if (isset($bancoAgenciaContaId['min'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaContaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bancoAgenciaContaId['max'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaContaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaContaId, $comparison);
    }

    /**
     * Filter the query on the observacao column
     *
     * Example usage:
     * <code>
     * $query->filterByObservacao('fooValue');   // WHERE observacao = 'fooValue'
     * $query->filterByObservacao('%fooValue%', Criteria::LIKE); // WHERE observacao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $observacao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByObservacao($observacao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($observacao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FornecedorTableMap::COL_OBSERVACAO, $observacao, $comparison);
    }

    /**
     * Filter the query on the ativo column
     *
     * Example usage:
     * <code>
     * $query->filterByAtivo(true); // WHERE ativo = true
     * $query->filterByAtivo('yes'); // WHERE ativo = true
     * </code>
     *
     * @param     boolean|string $ativo The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByAtivo($ativo = null, $comparison = null)
    {
        if (is_string($ativo)) {
            $ativo = in_array(strtolower($ativo), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(FornecedorTableMap::COL_ATIVO, $ativo, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FornecedorTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FornecedorTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(FornecedorTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FornecedorTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BancoAgenciaConta object
     *
     * @param \ImaTelecomBundle\Model\BancoAgenciaConta|ObjectCollection $bancoAgenciaConta The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByBancoAgenciaConta($bancoAgenciaConta, $comparison = null)
    {
        if ($bancoAgenciaConta instanceof \ImaTelecomBundle\Model\BancoAgenciaConta) {
            return $this
                ->addUsingAlias(FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaConta->getIdbancoAgenciaConta(), $comparison);
        } elseif ($bancoAgenciaConta instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID, $bancoAgenciaConta->toKeyValue('PrimaryKey', 'IdbancoAgenciaConta'), $comparison);
        } else {
            throw new PropelException('filterByBancoAgenciaConta() only accepts arguments of type \ImaTelecomBundle\Model\BancoAgenciaConta or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BancoAgenciaConta relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function joinBancoAgenciaConta($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BancoAgenciaConta');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BancoAgenciaConta');
        }

        return $this;
    }

    /**
     * Use the BancoAgenciaConta relation BancoAgenciaConta object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoAgenciaContaQuery A secondary query class using the current class as primary query
     */
    public function useBancoAgenciaContaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBancoAgenciaConta($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BancoAgenciaConta', '\ImaTelecomBundle\Model\BancoAgenciaContaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cliente object
     *
     * @param \ImaTelecomBundle\Model\Cliente|ObjectCollection $cliente The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByCliente($cliente, $comparison = null)
    {
        if ($cliente instanceof \ImaTelecomBundle\Model\Cliente) {
            return $this
                ->addUsingAlias(FornecedorTableMap::COL_CLIENTE_ID, $cliente->getIdcliente(), $comparison);
        } elseif ($cliente instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FornecedorTableMap::COL_CLIENTE_ID, $cliente->toKeyValue('PrimaryKey', 'Idcliente'), $comparison);
        } else {
            throw new PropelException('filterByCliente() only accepts arguments of type \ImaTelecomBundle\Model\Cliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function joinCliente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cliente');
        }

        return $this;
    }

    /**
     * Use the Cliente relation Cliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteQuery A secondary query class using the current class as primary query
     */
    public function useClienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cliente', '\ImaTelecomBundle\Model\ClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Pessoa object
     *
     * @param \ImaTelecomBundle\Model\Pessoa|ObjectCollection $pessoa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByPessoa($pessoa, $comparison = null)
    {
        if ($pessoa instanceof \ImaTelecomBundle\Model\Pessoa) {
            return $this
                ->addUsingAlias(FornecedorTableMap::COL_PESSOA_ID, $pessoa->getId(), $comparison);
        } elseif ($pessoa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FornecedorTableMap::COL_PESSOA_ID, $pessoa->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPessoa() only accepts arguments of type \ImaTelecomBundle\Model\Pessoa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pessoa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function joinPessoa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pessoa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pessoa');
        }

        return $this;
    }

    /**
     * Use the Pessoa relation Pessoa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\PessoaQuery A secondary query class using the current class as primary query
     */
    public function usePessoaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPessoa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pessoa', '\ImaTelecomBundle\Model\PessoaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(FornecedorTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FornecedorTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(FornecedorTableMap::COL_IDFORNECEDOR, $boleto->getFornecedorId(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            return $this
                ->useBoletoQuery()
                ->filterByPrimaryKeys($boleto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContasPagar object
     *
     * @param \ImaTelecomBundle\Model\ContasPagar|ObjectCollection $contasPagar the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFornecedorQuery The current query, for fluid interface
     */
    public function filterByContasPagar($contasPagar, $comparison = null)
    {
        if ($contasPagar instanceof \ImaTelecomBundle\Model\ContasPagar) {
            return $this
                ->addUsingAlias(FornecedorTableMap::COL_IDFORNECEDOR, $contasPagar->getFornecedorId(), $comparison);
        } elseif ($contasPagar instanceof ObjectCollection) {
            return $this
                ->useContasPagarQuery()
                ->filterByPrimaryKeys($contasPagar->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContasPagar() only accepts arguments of type \ImaTelecomBundle\Model\ContasPagar or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContasPagar relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function joinContasPagar($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContasPagar');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContasPagar');
        }

        return $this;
    }

    /**
     * Use the ContasPagar relation ContasPagar object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContasPagarQuery A secondary query class using the current class as primary query
     */
    public function useContasPagarQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContasPagar($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContasPagar', '\ImaTelecomBundle\Model\ContasPagarQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFornecedor $fornecedor Object to remove from the list of results
     *
     * @return $this|ChildFornecedorQuery The current query, for fluid interface
     */
    public function prune($fornecedor = null)
    {
        if ($fornecedor) {
            $this->addUsingAlias(FornecedorTableMap::COL_IDFORNECEDOR, $fornecedor->getIdfornecedor(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the fornecedor table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FornecedorTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FornecedorTableMap::clearInstancePool();
            FornecedorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FornecedorTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FornecedorTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FornecedorTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FornecedorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FornecedorQuery
