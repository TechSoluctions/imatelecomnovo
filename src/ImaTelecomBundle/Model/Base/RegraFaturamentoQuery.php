<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\RegraFaturamento as ChildRegraFaturamento;
use ImaTelecomBundle\Model\RegraFaturamentoQuery as ChildRegraFaturamentoQuery;
use ImaTelecomBundle\Model\Map\RegraFaturamentoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'regra_faturamento' table.
 *
 *
 *
 * @method     ChildRegraFaturamentoQuery orderByIdregraFaturamento($order = Criteria::ASC) Order by the idregra_faturamento column
 * @method     ChildRegraFaturamentoQuery orderByDescricao($order = Criteria::ASC) Order by the descricao column
 * @method     ChildRegraFaturamentoQuery orderByPeriodo1DiaVencimentoCobranca($order = Criteria::ASC) Order by the periodo1_dia_vencimento_cobranca column
 * @method     ChildRegraFaturamentoQuery orderByPeriodo1DiaInicialCobranca($order = Criteria::ASC) Order by the periodo1_dia_inicial_cobranca column
 * @method     ChildRegraFaturamentoQuery orderByPeriodo1DiaFinalCobranca($order = Criteria::ASC) Order by the periodo1_dia_final_cobranca column
 * @method     ChildRegraFaturamentoQuery orderByMesSubsequente($order = Criteria::ASC) Order by the mes_subsequente column
 * @method     ChildRegraFaturamentoQuery orderByPeriodo2DiaVencimentoCobranca($order = Criteria::ASC) Order by the periodo2_dia_vencimento_cobranca column
 * @method     ChildRegraFaturamentoQuery orderByPeriodo2DiaInicialCobranca($order = Criteria::ASC) Order by the periodo2_dia_inicial_cobranca column
 * @method     ChildRegraFaturamentoQuery orderByPeriodo2DiaFinalCobranca($order = Criteria::ASC) Order by the periodo2_dia_final_cobranca column
 * @method     ChildRegraFaturamentoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildRegraFaturamentoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildRegraFaturamentoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildRegraFaturamentoQuery groupByIdregraFaturamento() Group by the idregra_faturamento column
 * @method     ChildRegraFaturamentoQuery groupByDescricao() Group by the descricao column
 * @method     ChildRegraFaturamentoQuery groupByPeriodo1DiaVencimentoCobranca() Group by the periodo1_dia_vencimento_cobranca column
 * @method     ChildRegraFaturamentoQuery groupByPeriodo1DiaInicialCobranca() Group by the periodo1_dia_inicial_cobranca column
 * @method     ChildRegraFaturamentoQuery groupByPeriodo1DiaFinalCobranca() Group by the periodo1_dia_final_cobranca column
 * @method     ChildRegraFaturamentoQuery groupByMesSubsequente() Group by the mes_subsequente column
 * @method     ChildRegraFaturamentoQuery groupByPeriodo2DiaVencimentoCobranca() Group by the periodo2_dia_vencimento_cobranca column
 * @method     ChildRegraFaturamentoQuery groupByPeriodo2DiaInicialCobranca() Group by the periodo2_dia_inicial_cobranca column
 * @method     ChildRegraFaturamentoQuery groupByPeriodo2DiaFinalCobranca() Group by the periodo2_dia_final_cobranca column
 * @method     ChildRegraFaturamentoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildRegraFaturamentoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildRegraFaturamentoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildRegraFaturamentoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRegraFaturamentoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRegraFaturamentoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRegraFaturamentoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRegraFaturamentoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRegraFaturamentoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRegraFaturamentoQuery leftJoinContrato($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contrato relation
 * @method     ChildRegraFaturamentoQuery rightJoinContrato($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contrato relation
 * @method     ChildRegraFaturamentoQuery innerJoinContrato($relationAlias = null) Adds a INNER JOIN clause to the query using the Contrato relation
 *
 * @method     ChildRegraFaturamentoQuery joinWithContrato($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Contrato relation
 *
 * @method     ChildRegraFaturamentoQuery leftJoinWithContrato() Adds a LEFT JOIN clause and with to the query using the Contrato relation
 * @method     ChildRegraFaturamentoQuery rightJoinWithContrato() Adds a RIGHT JOIN clause and with to the query using the Contrato relation
 * @method     ChildRegraFaturamentoQuery innerJoinWithContrato() Adds a INNER JOIN clause and with to the query using the Contrato relation
 *
 * @method     \ImaTelecomBundle\Model\ContratoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRegraFaturamento findOne(ConnectionInterface $con = null) Return the first ChildRegraFaturamento matching the query
 * @method     ChildRegraFaturamento findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRegraFaturamento matching the query, or a new ChildRegraFaturamento object populated from the query conditions when no match is found
 *
 * @method     ChildRegraFaturamento findOneByIdregraFaturamento(int $idregra_faturamento) Return the first ChildRegraFaturamento filtered by the idregra_faturamento column
 * @method     ChildRegraFaturamento findOneByDescricao(string $descricao) Return the first ChildRegraFaturamento filtered by the descricao column
 * @method     ChildRegraFaturamento findOneByPeriodo1DiaVencimentoCobranca(int $periodo1_dia_vencimento_cobranca) Return the first ChildRegraFaturamento filtered by the periodo1_dia_vencimento_cobranca column
 * @method     ChildRegraFaturamento findOneByPeriodo1DiaInicialCobranca(int $periodo1_dia_inicial_cobranca) Return the first ChildRegraFaturamento filtered by the periodo1_dia_inicial_cobranca column
 * @method     ChildRegraFaturamento findOneByPeriodo1DiaFinalCobranca(int $periodo1_dia_final_cobranca) Return the first ChildRegraFaturamento filtered by the periodo1_dia_final_cobranca column
 * @method     ChildRegraFaturamento findOneByMesSubsequente(string $mes_subsequente) Return the first ChildRegraFaturamento filtered by the mes_subsequente column
 * @method     ChildRegraFaturamento findOneByPeriodo2DiaVencimentoCobranca(int $periodo2_dia_vencimento_cobranca) Return the first ChildRegraFaturamento filtered by the periodo2_dia_vencimento_cobranca column
 * @method     ChildRegraFaturamento findOneByPeriodo2DiaInicialCobranca(int $periodo2_dia_inicial_cobranca) Return the first ChildRegraFaturamento filtered by the periodo2_dia_inicial_cobranca column
 * @method     ChildRegraFaturamento findOneByPeriodo2DiaFinalCobranca(int $periodo2_dia_final_cobranca) Return the first ChildRegraFaturamento filtered by the periodo2_dia_final_cobranca column
 * @method     ChildRegraFaturamento findOneByDataCadastro(string $data_cadastro) Return the first ChildRegraFaturamento filtered by the data_cadastro column
 * @method     ChildRegraFaturamento findOneByDataAlterado(string $data_alterado) Return the first ChildRegraFaturamento filtered by the data_alterado column
 * @method     ChildRegraFaturamento findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildRegraFaturamento filtered by the usuario_alterado column *

 * @method     ChildRegraFaturamento requirePk($key, ConnectionInterface $con = null) Return the ChildRegraFaturamento by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOne(ConnectionInterface $con = null) Return the first ChildRegraFaturamento matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRegraFaturamento requireOneByIdregraFaturamento(int $idregra_faturamento) Return the first ChildRegraFaturamento filtered by the idregra_faturamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByDescricao(string $descricao) Return the first ChildRegraFaturamento filtered by the descricao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByPeriodo1DiaVencimentoCobranca(int $periodo1_dia_vencimento_cobranca) Return the first ChildRegraFaturamento filtered by the periodo1_dia_vencimento_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByPeriodo1DiaInicialCobranca(int $periodo1_dia_inicial_cobranca) Return the first ChildRegraFaturamento filtered by the periodo1_dia_inicial_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByPeriodo1DiaFinalCobranca(int $periodo1_dia_final_cobranca) Return the first ChildRegraFaturamento filtered by the periodo1_dia_final_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByMesSubsequente(string $mes_subsequente) Return the first ChildRegraFaturamento filtered by the mes_subsequente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByPeriodo2DiaVencimentoCobranca(int $periodo2_dia_vencimento_cobranca) Return the first ChildRegraFaturamento filtered by the periodo2_dia_vencimento_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByPeriodo2DiaInicialCobranca(int $periodo2_dia_inicial_cobranca) Return the first ChildRegraFaturamento filtered by the periodo2_dia_inicial_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByPeriodo2DiaFinalCobranca(int $periodo2_dia_final_cobranca) Return the first ChildRegraFaturamento filtered by the periodo2_dia_final_cobranca column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByDataCadastro(string $data_cadastro) Return the first ChildRegraFaturamento filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByDataAlterado(string $data_alterado) Return the first ChildRegraFaturamento filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRegraFaturamento requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildRegraFaturamento filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRegraFaturamento[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRegraFaturamento objects based on current ModelCriteria
 * @method     ChildRegraFaturamento[]|ObjectCollection findByIdregraFaturamento(int $idregra_faturamento) Return ChildRegraFaturamento objects filtered by the idregra_faturamento column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByDescricao(string $descricao) Return ChildRegraFaturamento objects filtered by the descricao column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByPeriodo1DiaVencimentoCobranca(int $periodo1_dia_vencimento_cobranca) Return ChildRegraFaturamento objects filtered by the periodo1_dia_vencimento_cobranca column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByPeriodo1DiaInicialCobranca(int $periodo1_dia_inicial_cobranca) Return ChildRegraFaturamento objects filtered by the periodo1_dia_inicial_cobranca column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByPeriodo1DiaFinalCobranca(int $periodo1_dia_final_cobranca) Return ChildRegraFaturamento objects filtered by the periodo1_dia_final_cobranca column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByMesSubsequente(string $mes_subsequente) Return ChildRegraFaturamento objects filtered by the mes_subsequente column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByPeriodo2DiaVencimentoCobranca(int $periodo2_dia_vencimento_cobranca) Return ChildRegraFaturamento objects filtered by the periodo2_dia_vencimento_cobranca column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByPeriodo2DiaInicialCobranca(int $periodo2_dia_inicial_cobranca) Return ChildRegraFaturamento objects filtered by the periodo2_dia_inicial_cobranca column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByPeriodo2DiaFinalCobranca(int $periodo2_dia_final_cobranca) Return ChildRegraFaturamento objects filtered by the periodo2_dia_final_cobranca column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildRegraFaturamento objects filtered by the data_cadastro column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildRegraFaturamento objects filtered by the data_alterado column
 * @method     ChildRegraFaturamento[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildRegraFaturamento objects filtered by the usuario_alterado column
 * @method     ChildRegraFaturamento[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RegraFaturamentoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\RegraFaturamentoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\RegraFaturamento', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRegraFaturamentoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRegraFaturamentoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRegraFaturamentoQuery) {
            return $criteria;
        }
        $query = new ChildRegraFaturamentoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRegraFaturamento|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RegraFaturamentoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RegraFaturamentoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRegraFaturamento A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idregra_faturamento, descricao, periodo1_dia_vencimento_cobranca, periodo1_dia_inicial_cobranca, periodo1_dia_final_cobranca, mes_subsequente, periodo2_dia_vencimento_cobranca, periodo2_dia_inicial_cobranca, periodo2_dia_final_cobranca, data_cadastro, data_alterado, usuario_alterado FROM regra_faturamento WHERE idregra_faturamento = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRegraFaturamento $obj */
            $obj = new ChildRegraFaturamento();
            $obj->hydrate($row);
            RegraFaturamentoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRegraFaturamento|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idregra_faturamento column
     *
     * Example usage:
     * <code>
     * $query->filterByIdregraFaturamento(1234); // WHERE idregra_faturamento = 1234
     * $query->filterByIdregraFaturamento(array(12, 34)); // WHERE idregra_faturamento IN (12, 34)
     * $query->filterByIdregraFaturamento(array('min' => 12)); // WHERE idregra_faturamento > 12
     * </code>
     *
     * @param     mixed $idregraFaturamento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByIdregraFaturamento($idregraFaturamento = null, $comparison = null)
    {
        if (is_array($idregraFaturamento)) {
            $useMinMax = false;
            if (isset($idregraFaturamento['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, $idregraFaturamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idregraFaturamento['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, $idregraFaturamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, $idregraFaturamento, $comparison);
    }

    /**
     * Filter the query on the descricao column
     *
     * Example usage:
     * <code>
     * $query->filterByDescricao('fooValue');   // WHERE descricao = 'fooValue'
     * $query->filterByDescricao('%fooValue%', Criteria::LIKE); // WHERE descricao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descricao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByDescricao($descricao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descricao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_DESCRICAO, $descricao, $comparison);
    }

    /**
     * Filter the query on the periodo1_dia_vencimento_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodo1DiaVencimentoCobranca(1234); // WHERE periodo1_dia_vencimento_cobranca = 1234
     * $query->filterByPeriodo1DiaVencimentoCobranca(array(12, 34)); // WHERE periodo1_dia_vencimento_cobranca IN (12, 34)
     * $query->filterByPeriodo1DiaVencimentoCobranca(array('min' => 12)); // WHERE periodo1_dia_vencimento_cobranca > 12
     * </code>
     *
     * @param     mixed $periodo1DiaVencimentoCobranca The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPeriodo1DiaVencimentoCobranca($periodo1DiaVencimentoCobranca = null, $comparison = null)
    {
        if (is_array($periodo1DiaVencimentoCobranca)) {
            $useMinMax = false;
            if (isset($periodo1DiaVencimentoCobranca['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA, $periodo1DiaVencimentoCobranca['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodo1DiaVencimentoCobranca['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA, $periodo1DiaVencimentoCobranca['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA, $periodo1DiaVencimentoCobranca, $comparison);
    }

    /**
     * Filter the query on the periodo1_dia_inicial_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodo1DiaInicialCobranca(1234); // WHERE periodo1_dia_inicial_cobranca = 1234
     * $query->filterByPeriodo1DiaInicialCobranca(array(12, 34)); // WHERE periodo1_dia_inicial_cobranca IN (12, 34)
     * $query->filterByPeriodo1DiaInicialCobranca(array('min' => 12)); // WHERE periodo1_dia_inicial_cobranca > 12
     * </code>
     *
     * @param     mixed $periodo1DiaInicialCobranca The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPeriodo1DiaInicialCobranca($periodo1DiaInicialCobranca = null, $comparison = null)
    {
        if (is_array($periodo1DiaInicialCobranca)) {
            $useMinMax = false;
            if (isset($periodo1DiaInicialCobranca['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA, $periodo1DiaInicialCobranca['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodo1DiaInicialCobranca['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA, $periodo1DiaInicialCobranca['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA, $periodo1DiaInicialCobranca, $comparison);
    }

    /**
     * Filter the query on the periodo1_dia_final_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodo1DiaFinalCobranca(1234); // WHERE periodo1_dia_final_cobranca = 1234
     * $query->filterByPeriodo1DiaFinalCobranca(array(12, 34)); // WHERE periodo1_dia_final_cobranca IN (12, 34)
     * $query->filterByPeriodo1DiaFinalCobranca(array('min' => 12)); // WHERE periodo1_dia_final_cobranca > 12
     * </code>
     *
     * @param     mixed $periodo1DiaFinalCobranca The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPeriodo1DiaFinalCobranca($periodo1DiaFinalCobranca = null, $comparison = null)
    {
        if (is_array($periodo1DiaFinalCobranca)) {
            $useMinMax = false;
            if (isset($periodo1DiaFinalCobranca['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA, $periodo1DiaFinalCobranca['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodo1DiaFinalCobranca['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA, $periodo1DiaFinalCobranca['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA, $periodo1DiaFinalCobranca, $comparison);
    }

    /**
     * Filter the query on the mes_subsequente column
     *
     * Example usage:
     * <code>
     * $query->filterByMesSubsequente('fooValue');   // WHERE mes_subsequente = 'fooValue'
     * $query->filterByMesSubsequente('%fooValue%', Criteria::LIKE); // WHERE mes_subsequente LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mesSubsequente The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByMesSubsequente($mesSubsequente = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mesSubsequente)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_MES_SUBSEQUENTE, $mesSubsequente, $comparison);
    }

    /**
     * Filter the query on the periodo2_dia_vencimento_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodo2DiaVencimentoCobranca(1234); // WHERE periodo2_dia_vencimento_cobranca = 1234
     * $query->filterByPeriodo2DiaVencimentoCobranca(array(12, 34)); // WHERE periodo2_dia_vencimento_cobranca IN (12, 34)
     * $query->filterByPeriodo2DiaVencimentoCobranca(array('min' => 12)); // WHERE periodo2_dia_vencimento_cobranca > 12
     * </code>
     *
     * @param     mixed $periodo2DiaVencimentoCobranca The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPeriodo2DiaVencimentoCobranca($periodo2DiaVencimentoCobranca = null, $comparison = null)
    {
        if (is_array($periodo2DiaVencimentoCobranca)) {
            $useMinMax = false;
            if (isset($periodo2DiaVencimentoCobranca['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA, $periodo2DiaVencimentoCobranca['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodo2DiaVencimentoCobranca['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA, $periodo2DiaVencimentoCobranca['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA, $periodo2DiaVencimentoCobranca, $comparison);
    }

    /**
     * Filter the query on the periodo2_dia_inicial_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodo2DiaInicialCobranca(1234); // WHERE periodo2_dia_inicial_cobranca = 1234
     * $query->filterByPeriodo2DiaInicialCobranca(array(12, 34)); // WHERE periodo2_dia_inicial_cobranca IN (12, 34)
     * $query->filterByPeriodo2DiaInicialCobranca(array('min' => 12)); // WHERE periodo2_dia_inicial_cobranca > 12
     * </code>
     *
     * @param     mixed $periodo2DiaInicialCobranca The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPeriodo2DiaInicialCobranca($periodo2DiaInicialCobranca = null, $comparison = null)
    {
        if (is_array($periodo2DiaInicialCobranca)) {
            $useMinMax = false;
            if (isset($periodo2DiaInicialCobranca['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA, $periodo2DiaInicialCobranca['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodo2DiaInicialCobranca['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA, $periodo2DiaInicialCobranca['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA, $periodo2DiaInicialCobranca, $comparison);
    }

    /**
     * Filter the query on the periodo2_dia_final_cobranca column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodo2DiaFinalCobranca(1234); // WHERE periodo2_dia_final_cobranca = 1234
     * $query->filterByPeriodo2DiaFinalCobranca(array(12, 34)); // WHERE periodo2_dia_final_cobranca IN (12, 34)
     * $query->filterByPeriodo2DiaFinalCobranca(array('min' => 12)); // WHERE periodo2_dia_final_cobranca > 12
     * </code>
     *
     * @param     mixed $periodo2DiaFinalCobranca The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByPeriodo2DiaFinalCobranca($periodo2DiaFinalCobranca = null, $comparison = null)
    {
        if (is_array($periodo2DiaFinalCobranca)) {
            $useMinMax = false;
            if (isset($periodo2DiaFinalCobranca['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA, $periodo2DiaFinalCobranca['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periodo2DiaFinalCobranca['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA, $periodo2DiaFinalCobranca['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA, $periodo2DiaFinalCobranca, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(RegraFaturamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RegraFaturamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Contrato object
     *
     * @param \ImaTelecomBundle\Model\Contrato|ObjectCollection $contrato the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function filterByContrato($contrato, $comparison = null)
    {
        if ($contrato instanceof \ImaTelecomBundle\Model\Contrato) {
            return $this
                ->addUsingAlias(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, $contrato->getRegraFaturamentoId(), $comparison);
        } elseif ($contrato instanceof ObjectCollection) {
            return $this
                ->useContratoQuery()
                ->filterByPrimaryKeys($contrato->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContrato() only accepts arguments of type \ImaTelecomBundle\Model\Contrato or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contrato relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function joinContrato($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contrato');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contrato');
        }

        return $this;
    }

    /**
     * Use the Contrato relation Contrato object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContratoQuery A secondary query class using the current class as primary query
     */
    public function useContratoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContrato($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contrato', '\ImaTelecomBundle\Model\ContratoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRegraFaturamento $regraFaturamento Object to remove from the list of results
     *
     * @return $this|ChildRegraFaturamentoQuery The current query, for fluid interface
     */
    public function prune($regraFaturamento = null)
    {
        if ($regraFaturamento) {
            $this->addUsingAlias(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, $regraFaturamento->getIdregraFaturamento(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the regra_faturamento table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RegraFaturamentoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RegraFaturamentoTableMap::clearInstancePool();
            RegraFaturamentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RegraFaturamentoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RegraFaturamentoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RegraFaturamentoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RegraFaturamentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RegraFaturamentoQuery
