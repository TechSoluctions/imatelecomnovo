<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ServicoPrestado as ChildServicoPrestado;
use ImaTelecomBundle\Model\ServicoPrestadoQuery as ChildServicoPrestadoQuery;
use ImaTelecomBundle\Model\TipoServicoPrestado as ChildTipoServicoPrestado;
use ImaTelecomBundle\Model\TipoServicoPrestadoQuery as ChildTipoServicoPrestadoQuery;
use ImaTelecomBundle\Model\Map\ServicoPrestadoTableMap;
use ImaTelecomBundle\Model\Map\TipoServicoPrestadoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'tipo_servico_prestado' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class TipoServicoPrestado implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\TipoServicoPrestadoTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idtipo_servico_prestado field.
     *
     * @var        int
     */
    protected $idtipo_servico_prestado;

    /**
     * The value for the tipo field.
     *
     * @var        string
     */
    protected $tipo;

    /**
     * The value for the pre_pago field.
     *
     * @var        boolean
     */
    protected $pre_pago;

    /**
     * The value for the natureza_operacao field.
     *
     * Note: this column has a database default value of: '4'
     * @var        string
     */
    protected $natureza_operacao;

    /**
     * The value for the item_lista_servico field.
     *
     * Note: this column has a database default value of: '1.07'
     * @var        string
     */
    protected $item_lista_servico;

    /**
     * The value for the codigo_tributacao field.
     *
     * Note: this column has a database default value of: '619060100'
     * @var        string
     */
    protected $codigo_tributacao;

    /**
     * The value for the aliquota field.
     *
     * Note: this column has a database default value of: 2.0
     * @var        double
     */
    protected $aliquota;

    /**
     * The value for the unidade_negocio field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $unidade_negocio;

    /**
     * @var        ObjectCollection|ChildServicoPrestado[] Collection to store aggregation of ChildServicoPrestado objects.
     */
    protected $collServicoPrestados;
    protected $collServicoPrestadosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildServicoPrestado[]
     */
    protected $servicoPrestadosScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->natureza_operacao = '4';
        $this->item_lista_servico = '1.07';
        $this->codigo_tributacao = '619060100';
        $this->aliquota = 2.0;
        $this->unidade_negocio = 1;
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\TipoServicoPrestado object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>TipoServicoPrestado</code> instance.  If
     * <code>obj</code> is an instance of <code>TipoServicoPrestado</code>, delegates to
     * <code>equals(TipoServicoPrestado)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|TipoServicoPrestado The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idtipo_servico_prestado] column value.
     *
     * @return int
     */
    public function getIdtipoServicoPrestado()
    {
        return $this->idtipo_servico_prestado;
    }

    /**
     * Get the [tipo] column value.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Get the [pre_pago] column value.
     *
     * @return boolean
     */
    public function getPrePago()
    {
        return $this->pre_pago;
    }

    /**
     * Get the [pre_pago] column value.
     *
     * @return boolean
     */
    public function isPrePago()
    {
        return $this->getPrePago();
    }

    /**
     * Get the [natureza_operacao] column value.
     *
     * @return string
     */
    public function getNaturezaOperacao()
    {
        return $this->natureza_operacao;
    }

    /**
     * Get the [item_lista_servico] column value.
     *
     * @return string
     */
    public function getItemListaServico()
    {
        return $this->item_lista_servico;
    }

    /**
     * Get the [codigo_tributacao] column value.
     *
     * @return string
     */
    public function getCodigoTributacao()
    {
        return $this->codigo_tributacao;
    }

    /**
     * Get the [aliquota] column value.
     *
     * @return double
     */
    public function getAliquota()
    {
        return $this->aliquota;
    }

    /**
     * Get the [unidade_negocio] column value.
     *
     * @return int
     */
    public function getUnidadeNegocio()
    {
        return $this->unidade_negocio;
    }

    /**
     * Set the value of [idtipo_servico_prestado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object (for fluent API support)
     */
    public function setIdtipoServicoPrestado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idtipo_servico_prestado !== $v) {
            $this->idtipo_servico_prestado = $v;
            $this->modifiedColumns[TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO] = true;
        }

        return $this;
    } // setIdtipoServicoPrestado()

    /**
     * Set the value of [tipo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object (for fluent API support)
     */
    public function setTipo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tipo !== $v) {
            $this->tipo = $v;
            $this->modifiedColumns[TipoServicoPrestadoTableMap::COL_TIPO] = true;
        }

        return $this;
    } // setTipo()

    /**
     * Sets the value of the [pre_pago] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object (for fluent API support)
     */
    public function setPrePago($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->pre_pago !== $v) {
            $this->pre_pago = $v;
            $this->modifiedColumns[TipoServicoPrestadoTableMap::COL_PRE_PAGO] = true;
        }

        return $this;
    } // setPrePago()

    /**
     * Set the value of [natureza_operacao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object (for fluent API support)
     */
    public function setNaturezaOperacao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->natureza_operacao !== $v) {
            $this->natureza_operacao = $v;
            $this->modifiedColumns[TipoServicoPrestadoTableMap::COL_NATUREZA_OPERACAO] = true;
        }

        return $this;
    } // setNaturezaOperacao()

    /**
     * Set the value of [item_lista_servico] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object (for fluent API support)
     */
    public function setItemListaServico($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->item_lista_servico !== $v) {
            $this->item_lista_servico = $v;
            $this->modifiedColumns[TipoServicoPrestadoTableMap::COL_ITEM_LISTA_SERVICO] = true;
        }

        return $this;
    } // setItemListaServico()

    /**
     * Set the value of [codigo_tributacao] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object (for fluent API support)
     */
    public function setCodigoTributacao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo_tributacao !== $v) {
            $this->codigo_tributacao = $v;
            $this->modifiedColumns[TipoServicoPrestadoTableMap::COL_CODIGO_TRIBUTACAO] = true;
        }

        return $this;
    } // setCodigoTributacao()

    /**
     * Set the value of [aliquota] column.
     *
     * @param double $v new value
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object (for fluent API support)
     */
    public function setAliquota($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->aliquota !== $v) {
            $this->aliquota = $v;
            $this->modifiedColumns[TipoServicoPrestadoTableMap::COL_ALIQUOTA] = true;
        }

        return $this;
    } // setAliquota()

    /**
     * Set the value of [unidade_negocio] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object (for fluent API support)
     */
    public function setUnidadeNegocio($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->unidade_negocio !== $v) {
            $this->unidade_negocio = $v;
            $this->modifiedColumns[TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO] = true;
        }

        return $this;
    } // setUnidadeNegocio()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->natureza_operacao !== '4') {
                return false;
            }

            if ($this->item_lista_servico !== '1.07') {
                return false;
            }

            if ($this->codigo_tributacao !== '619060100') {
                return false;
            }

            if ($this->aliquota !== 2.0) {
                return false;
            }

            if ($this->unidade_negocio !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : TipoServicoPrestadoTableMap::translateFieldName('IdtipoServicoPrestado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idtipo_servico_prestado = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : TipoServicoPrestadoTableMap::translateFieldName('Tipo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tipo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : TipoServicoPrestadoTableMap::translateFieldName('PrePago', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pre_pago = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : TipoServicoPrestadoTableMap::translateFieldName('NaturezaOperacao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->natureza_operacao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : TipoServicoPrestadoTableMap::translateFieldName('ItemListaServico', TableMap::TYPE_PHPNAME, $indexType)];
            $this->item_lista_servico = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : TipoServicoPrestadoTableMap::translateFieldName('CodigoTributacao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codigo_tributacao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : TipoServicoPrestadoTableMap::translateFieldName('Aliquota', TableMap::TYPE_PHPNAME, $indexType)];
            $this->aliquota = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : TipoServicoPrestadoTableMap::translateFieldName('UnidadeNegocio', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unidade_negocio = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 8; // 8 = TipoServicoPrestadoTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\TipoServicoPrestado'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TipoServicoPrestadoTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildTipoServicoPrestadoQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collServicoPrestados = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see TipoServicoPrestado::setDeleted()
     * @see TipoServicoPrestado::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoServicoPrestadoTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildTipoServicoPrestadoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoServicoPrestadoTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                TipoServicoPrestadoTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->servicoPrestadosScheduledForDeletion !== null) {
                if (!$this->servicoPrestadosScheduledForDeletion->isEmpty()) {
                    \ImaTelecomBundle\Model\ServicoPrestadoQuery::create()
                        ->filterByPrimaryKeys($this->servicoPrestadosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->servicoPrestadosScheduledForDeletion = null;
                }
            }

            if ($this->collServicoPrestados !== null) {
                foreach ($this->collServicoPrestados as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO] = true;
        if (null !== $this->idtipo_servico_prestado) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO)) {
            $modifiedColumns[':p' . $index++]  = 'idtipo_servico_prestado';
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_TIPO)) {
            $modifiedColumns[':p' . $index++]  = 'tipo';
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_PRE_PAGO)) {
            $modifiedColumns[':p' . $index++]  = 'pre_pago';
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_NATUREZA_OPERACAO)) {
            $modifiedColumns[':p' . $index++]  = 'natureza_operacao';
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_ITEM_LISTA_SERVICO)) {
            $modifiedColumns[':p' . $index++]  = 'item_lista_servico';
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_CODIGO_TRIBUTACAO)) {
            $modifiedColumns[':p' . $index++]  = 'codigo_tributacao';
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_ALIQUOTA)) {
            $modifiedColumns[':p' . $index++]  = 'aliquota';
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO)) {
            $modifiedColumns[':p' . $index++]  = 'unidade_negocio';
        }

        $sql = sprintf(
            'INSERT INTO tipo_servico_prestado (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idtipo_servico_prestado':
                        $stmt->bindValue($identifier, $this->idtipo_servico_prestado, PDO::PARAM_INT);
                        break;
                    case 'tipo':
                        $stmt->bindValue($identifier, $this->tipo, PDO::PARAM_STR);
                        break;
                    case 'pre_pago':
                        $stmt->bindValue($identifier, (int) $this->pre_pago, PDO::PARAM_INT);
                        break;
                    case 'natureza_operacao':
                        $stmt->bindValue($identifier, $this->natureza_operacao, PDO::PARAM_STR);
                        break;
                    case 'item_lista_servico':
                        $stmt->bindValue($identifier, $this->item_lista_servico, PDO::PARAM_STR);
                        break;
                    case 'codigo_tributacao':
                        $stmt->bindValue($identifier, $this->codigo_tributacao, PDO::PARAM_STR);
                        break;
                    case 'aliquota':
                        $stmt->bindValue($identifier, $this->aliquota, PDO::PARAM_STR);
                        break;
                    case 'unidade_negocio':
                        $stmt->bindValue($identifier, $this->unidade_negocio, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdtipoServicoPrestado($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TipoServicoPrestadoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdtipoServicoPrestado();
                break;
            case 1:
                return $this->getTipo();
                break;
            case 2:
                return $this->getPrePago();
                break;
            case 3:
                return $this->getNaturezaOperacao();
                break;
            case 4:
                return $this->getItemListaServico();
                break;
            case 5:
                return $this->getCodigoTributacao();
                break;
            case 6:
                return $this->getAliquota();
                break;
            case 7:
                return $this->getUnidadeNegocio();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['TipoServicoPrestado'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['TipoServicoPrestado'][$this->hashCode()] = true;
        $keys = TipoServicoPrestadoTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdtipoServicoPrestado(),
            $keys[1] => $this->getTipo(),
            $keys[2] => $this->getPrePago(),
            $keys[3] => $this->getNaturezaOperacao(),
            $keys[4] => $this->getItemListaServico(),
            $keys[5] => $this->getCodigoTributacao(),
            $keys[6] => $this->getAliquota(),
            $keys[7] => $this->getUnidadeNegocio(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collServicoPrestados) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'servicoPrestados';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'servico_prestados';
                        break;
                    default:
                        $key = 'ServicoPrestados';
                }

                $result[$key] = $this->collServicoPrestados->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = TipoServicoPrestadoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdtipoServicoPrestado($value);
                break;
            case 1:
                $this->setTipo($value);
                break;
            case 2:
                $this->setPrePago($value);
                break;
            case 3:
                $this->setNaturezaOperacao($value);
                break;
            case 4:
                $this->setItemListaServico($value);
                break;
            case 5:
                $this->setCodigoTributacao($value);
                break;
            case 6:
                $this->setAliquota($value);
                break;
            case 7:
                $this->setUnidadeNegocio($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = TipoServicoPrestadoTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdtipoServicoPrestado($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTipo($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPrePago($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNaturezaOperacao($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setItemListaServico($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCodigoTributacao($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setAliquota($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setUnidadeNegocio($arr[$keys[7]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(TipoServicoPrestadoTableMap::DATABASE_NAME);

        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO)) {
            $criteria->add(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, $this->idtipo_servico_prestado);
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_TIPO)) {
            $criteria->add(TipoServicoPrestadoTableMap::COL_TIPO, $this->tipo);
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_PRE_PAGO)) {
            $criteria->add(TipoServicoPrestadoTableMap::COL_PRE_PAGO, $this->pre_pago);
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_NATUREZA_OPERACAO)) {
            $criteria->add(TipoServicoPrestadoTableMap::COL_NATUREZA_OPERACAO, $this->natureza_operacao);
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_ITEM_LISTA_SERVICO)) {
            $criteria->add(TipoServicoPrestadoTableMap::COL_ITEM_LISTA_SERVICO, $this->item_lista_servico);
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_CODIGO_TRIBUTACAO)) {
            $criteria->add(TipoServicoPrestadoTableMap::COL_CODIGO_TRIBUTACAO, $this->codigo_tributacao);
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_ALIQUOTA)) {
            $criteria->add(TipoServicoPrestadoTableMap::COL_ALIQUOTA, $this->aliquota);
        }
        if ($this->isColumnModified(TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO)) {
            $criteria->add(TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO, $this->unidade_negocio);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildTipoServicoPrestadoQuery::create();
        $criteria->add(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, $this->idtipo_servico_prestado);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdtipoServicoPrestado();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdtipoServicoPrestado();
    }

    /**
     * Generic method to set the primary key (idtipo_servico_prestado column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdtipoServicoPrestado($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdtipoServicoPrestado();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\TipoServicoPrestado (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTipo($this->getTipo());
        $copyObj->setPrePago($this->getPrePago());
        $copyObj->setNaturezaOperacao($this->getNaturezaOperacao());
        $copyObj->setItemListaServico($this->getItemListaServico());
        $copyObj->setCodigoTributacao($this->getCodigoTributacao());
        $copyObj->setAliquota($this->getAliquota());
        $copyObj->setUnidadeNegocio($this->getUnidadeNegocio());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getServicoPrestados() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addServicoPrestado($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdtipoServicoPrestado(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\TipoServicoPrestado Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ServicoPrestado' == $relationName) {
            return $this->initServicoPrestados();
        }
    }

    /**
     * Clears out the collServicoPrestados collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addServicoPrestados()
     */
    public function clearServicoPrestados()
    {
        $this->collServicoPrestados = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collServicoPrestados collection loaded partially.
     */
    public function resetPartialServicoPrestados($v = true)
    {
        $this->collServicoPrestadosPartial = $v;
    }

    /**
     * Initializes the collServicoPrestados collection.
     *
     * By default this just sets the collServicoPrestados collection to an empty array (like clearcollServicoPrestados());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initServicoPrestados($overrideExisting = true)
    {
        if (null !== $this->collServicoPrestados && !$overrideExisting) {
            return;
        }

        $collectionClassName = ServicoPrestadoTableMap::getTableMap()->getCollectionClassName();

        $this->collServicoPrestados = new $collectionClassName;
        $this->collServicoPrestados->setModel('\ImaTelecomBundle\Model\ServicoPrestado');
    }

    /**
     * Gets an array of ChildServicoPrestado objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildTipoServicoPrestado is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildServicoPrestado[] List of ChildServicoPrestado objects
     * @throws PropelException
     */
    public function getServicoPrestados(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoPrestadosPartial && !$this->isNew();
        if (null === $this->collServicoPrestados || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collServicoPrestados) {
                // return empty collection
                $this->initServicoPrestados();
            } else {
                $collServicoPrestados = ChildServicoPrestadoQuery::create(null, $criteria)
                    ->filterByTipoServicoPrestado($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collServicoPrestadosPartial && count($collServicoPrestados)) {
                        $this->initServicoPrestados(false);

                        foreach ($collServicoPrestados as $obj) {
                            if (false == $this->collServicoPrestados->contains($obj)) {
                                $this->collServicoPrestados->append($obj);
                            }
                        }

                        $this->collServicoPrestadosPartial = true;
                    }

                    return $collServicoPrestados;
                }

                if ($partial && $this->collServicoPrestados) {
                    foreach ($this->collServicoPrestados as $obj) {
                        if ($obj->isNew()) {
                            $collServicoPrestados[] = $obj;
                        }
                    }
                }

                $this->collServicoPrestados = $collServicoPrestados;
                $this->collServicoPrestadosPartial = false;
            }
        }

        return $this->collServicoPrestados;
    }

    /**
     * Sets a collection of ChildServicoPrestado objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $servicoPrestados A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildTipoServicoPrestado The current object (for fluent API support)
     */
    public function setServicoPrestados(Collection $servicoPrestados, ConnectionInterface $con = null)
    {
        /** @var ChildServicoPrestado[] $servicoPrestadosToDelete */
        $servicoPrestadosToDelete = $this->getServicoPrestados(new Criteria(), $con)->diff($servicoPrestados);


        $this->servicoPrestadosScheduledForDeletion = $servicoPrestadosToDelete;

        foreach ($servicoPrestadosToDelete as $servicoPrestadoRemoved) {
            $servicoPrestadoRemoved->setTipoServicoPrestado(null);
        }

        $this->collServicoPrestados = null;
        foreach ($servicoPrestados as $servicoPrestado) {
            $this->addServicoPrestado($servicoPrestado);
        }

        $this->collServicoPrestados = $servicoPrestados;
        $this->collServicoPrestadosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ServicoPrestado objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ServicoPrestado objects.
     * @throws PropelException
     */
    public function countServicoPrestados(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collServicoPrestadosPartial && !$this->isNew();
        if (null === $this->collServicoPrestados || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collServicoPrestados) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getServicoPrestados());
            }

            $query = ChildServicoPrestadoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByTipoServicoPrestado($this)
                ->count($con);
        }

        return count($this->collServicoPrestados);
    }

    /**
     * Method called to associate a ChildServicoPrestado object to this object
     * through the ChildServicoPrestado foreign key attribute.
     *
     * @param  ChildServicoPrestado $l ChildServicoPrestado
     * @return $this|\ImaTelecomBundle\Model\TipoServicoPrestado The current object (for fluent API support)
     */
    public function addServicoPrestado(ChildServicoPrestado $l)
    {
        if ($this->collServicoPrestados === null) {
            $this->initServicoPrestados();
            $this->collServicoPrestadosPartial = true;
        }

        if (!$this->collServicoPrestados->contains($l)) {
            $this->doAddServicoPrestado($l);

            if ($this->servicoPrestadosScheduledForDeletion and $this->servicoPrestadosScheduledForDeletion->contains($l)) {
                $this->servicoPrestadosScheduledForDeletion->remove($this->servicoPrestadosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildServicoPrestado $servicoPrestado The ChildServicoPrestado object to add.
     */
    protected function doAddServicoPrestado(ChildServicoPrestado $servicoPrestado)
    {
        $this->collServicoPrestados[]= $servicoPrestado;
        $servicoPrestado->setTipoServicoPrestado($this);
    }

    /**
     * @param  ChildServicoPrestado $servicoPrestado The ChildServicoPrestado object to remove.
     * @return $this|ChildTipoServicoPrestado The current object (for fluent API support)
     */
    public function removeServicoPrestado(ChildServicoPrestado $servicoPrestado)
    {
        if ($this->getServicoPrestados()->contains($servicoPrestado)) {
            $pos = $this->collServicoPrestados->search($servicoPrestado);
            $this->collServicoPrestados->remove($pos);
            if (null === $this->servicoPrestadosScheduledForDeletion) {
                $this->servicoPrestadosScheduledForDeletion = clone $this->collServicoPrestados;
                $this->servicoPrestadosScheduledForDeletion->clear();
            }
            $this->servicoPrestadosScheduledForDeletion[]= clone $servicoPrestado;
            $servicoPrestado->setTipoServicoPrestado(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TipoServicoPrestado is new, it will return
     * an empty collection; or if this TipoServicoPrestado has previously
     * been saved, it will retrieve related ServicoPrestados from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TipoServicoPrestado.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoPrestado[] List of ChildServicoPrestado objects
     */
    public function getServicoPrestadosJoinPlanos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoPrestadoQuery::create(null, $criteria);
        $query->joinWith('Planos', $joinBehavior);

        return $this->getServicoPrestados($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this TipoServicoPrestado is new, it will return
     * an empty collection; or if this TipoServicoPrestado has previously
     * been saved, it will retrieve related ServicoPrestados from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in TipoServicoPrestado.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildServicoPrestado[] List of ChildServicoPrestado objects
     */
    public function getServicoPrestadosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildServicoPrestadoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getServicoPrestados($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->idtipo_servico_prestado = null;
        $this->tipo = null;
        $this->pre_pago = null;
        $this->natureza_operacao = null;
        $this->item_lista_servico = null;
        $this->codigo_tributacao = null;
        $this->aliquota = null;
        $this->unidade_negocio = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collServicoPrestados) {
                foreach ($this->collServicoPrestados as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collServicoPrestados = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(TipoServicoPrestadoTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
