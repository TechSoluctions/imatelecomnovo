<?php

namespace ImaTelecomBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use ImaTelecomBundle\Model\Baixa as ChildBaixa;
use ImaTelecomBundle\Model\BaixaEstorno as ChildBaixaEstorno;
use ImaTelecomBundle\Model\BaixaEstornoQuery as ChildBaixaEstornoQuery;
use ImaTelecomBundle\Model\BaixaQuery as ChildBaixaQuery;
use ImaTelecomBundle\Model\Boleto as ChildBoleto;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery as ChildBoletoBaixaHistoricoQuery;
use ImaTelecomBundle\Model\BoletoQuery as ChildBoletoQuery;
use ImaTelecomBundle\Model\Competencia as ChildCompetencia;
use ImaTelecomBundle\Model\CompetenciaQuery as ChildCompetenciaQuery;
use ImaTelecomBundle\Model\Lancamentos as ChildLancamentos;
use ImaTelecomBundle\Model\LancamentosQuery as ChildLancamentosQuery;
use ImaTelecomBundle\Model\Pessoa as ChildPessoa;
use ImaTelecomBundle\Model\PessoaQuery as ChildPessoaQuery;
use ImaTelecomBundle\Model\Usuario as ChildUsuario;
use ImaTelecomBundle\Model\UsuarioQuery as ChildUsuarioQuery;
use ImaTelecomBundle\Model\Map\BoletoBaixaHistoricoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'boleto_baixa_historico' table.
 *
 *
 *
 * @package    propel.generator.src\ImaTelecomBundle.Model.Base
 */
abstract class BoletoBaixaHistorico implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\ImaTelecomBundle\\Model\\Map\\BoletoBaixaHistoricoTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idboleto_baixa_historico field.
     *
     * @var        int
     */
    protected $idboleto_baixa_historico;

    /**
     * The value for the boleto_id field.
     *
     * @var        int
     */
    protected $boleto_id;

    /**
     * The value for the lancamento_id field.
     *
     * @var        int
     */
    protected $lancamento_id;

    /**
     * The value for the pessoa_id field.
     *
     * @var        int
     */
    protected $pessoa_id;

    /**
     * The value for the competencia_id field.
     *
     * @var        int
     */
    protected $competencia_id;

    /**
     * The value for the baixa_id field.
     *
     * @var        int
     */
    protected $baixa_id;

    /**
     * The value for the baixa_estorno_id field.
     *
     * @var        int
     */
    protected $baixa_estorno_id;

    /**
     * The value for the status field.
     *
     * @var        string
     */
    protected $status;

    /**
     * The value for the mensagem_retorno field.
     *
     * @var        string
     */
    protected $mensagem_retorno;

    /**
     * The value for the data_pagamento field.
     *
     * @var        string
     */
    protected $data_pagamento;

    /**
     * The value for the data_recusa field.
     *
     * @var        string
     */
    protected $data_recusa;

    /**
     * The value for the valor_tarifa_custo field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_tarifa_custo;

    /**
     * The value for the valor_original field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_original;

    /**
     * The value for the valor_multa field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_multa;

    /**
     * The value for the valor_juros field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_juros;

    /**
     * The value for the valor_desconto field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_desconto;

    /**
     * The value for the valor_total field.
     *
     * Note: this column has a database default value of: '0.00'
     * @var        string
     */
    protected $valor_total;

    /**
     * The value for the data_vencimento field.
     *
     * @var        DateTime
     */
    protected $data_vencimento;

    /**
     * The value for the numero_documento field.
     *
     * @var        string
     */
    protected $numero_documento;

    /**
     * The value for the codigo_movimento field.
     *
     * @var        string
     */
    protected $codigo_movimento;

    /**
     * The value for the descricao_movimento field.
     *
     * @var        string
     */
    protected $descricao_movimento;

    /**
     * The value for the data_cadastro field.
     *
     * @var        DateTime
     */
    protected $data_cadastro;

    /**
     * The value for the data_alterado field.
     *
     * @var        DateTime
     */
    protected $data_alterado;

    /**
     * The value for the usuario_alterado field.
     *
     * @var        int
     */
    protected $usuario_alterado;

    /**
     * @var        ChildBaixaEstorno
     */
    protected $aBaixaEstorno;

    /**
     * @var        ChildBaixa
     */
    protected $aBaixa;

    /**
     * @var        ChildBoleto
     */
    protected $aBoleto;

    /**
     * @var        ChildCompetencia
     */
    protected $aCompetencia;

    /**
     * @var        ChildLancamentos
     */
    protected $aLancamentos;

    /**
     * @var        ChildPessoa
     */
    protected $aPessoa;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->valor_tarifa_custo = '0.00';
        $this->valor_original = '0.00';
        $this->valor_multa = '0.00';
        $this->valor_juros = '0.00';
        $this->valor_desconto = '0.00';
        $this->valor_total = '0.00';
    }

    /**
     * Initializes internal state of ImaTelecomBundle\Model\Base\BoletoBaixaHistorico object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>BoletoBaixaHistorico</code> instance.  If
     * <code>obj</code> is an instance of <code>BoletoBaixaHistorico</code>, delegates to
     * <code>equals(BoletoBaixaHistorico)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|BoletoBaixaHistorico The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idboleto_baixa_historico] column value.
     *
     * @return int
     */
    public function getIdboletoBaixaHistorico()
    {
        return $this->idboleto_baixa_historico;
    }

    /**
     * Get the [boleto_id] column value.
     *
     * @return int
     */
    public function getBoletoId()
    {
        return $this->boleto_id;
    }

    /**
     * Get the [lancamento_id] column value.
     *
     * @return int
     */
    public function getLancamentoId()
    {
        return $this->lancamento_id;
    }

    /**
     * Get the [pessoa_id] column value.
     *
     * @return int
     */
    public function getPessoaId()
    {
        return $this->pessoa_id;
    }

    /**
     * Get the [competencia_id] column value.
     *
     * @return int
     */
    public function getCompetenciaId()
    {
        return $this->competencia_id;
    }

    /**
     * Get the [baixa_id] column value.
     *
     * @return int
     */
    public function getBaixaId()
    {
        return $this->baixa_id;
    }

    /**
     * Get the [baixa_estorno_id] column value.
     *
     * @return int
     */
    public function getBaixaEstornoId()
    {
        return $this->baixa_estorno_id;
    }

    /**
     * Get the [status] column value.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [mensagem_retorno] column value.
     *
     * @return string
     */
    public function getMensagemRetorno()
    {
        return $this->mensagem_retorno;
    }

    /**
     * Get the [data_pagamento] column value.
     *
     * @return string
     */
    public function getDataPagamento()
    {
        return $this->data_pagamento;
    }

    /**
     * Get the [data_recusa] column value.
     *
     * @return string
     */
    public function getDataRecusa()
    {
        return $this->data_recusa;
    }

    /**
     * Get the [valor_tarifa_custo] column value.
     *
     * @return string
     */
    public function getValorTarifaCusto()
    {
        return $this->valor_tarifa_custo;
    }

    /**
     * Get the [valor_original] column value.
     *
     * @return string
     */
    public function getValorOriginal()
    {
        return $this->valor_original;
    }

    /**
     * Get the [valor_multa] column value.
     *
     * @return string
     */
    public function getValorMulta()
    {
        return $this->valor_multa;
    }

    /**
     * Get the [valor_juros] column value.
     *
     * @return string
     */
    public function getValorJuros()
    {
        return $this->valor_juros;
    }

    /**
     * Get the [valor_desconto] column value.
     *
     * @return string
     */
    public function getValorDesconto()
    {
        return $this->valor_desconto;
    }

    /**
     * Get the [valor_total] column value.
     *
     * @return string
     */
    public function getValorTotal()
    {
        return $this->valor_total;
    }

    /**
     * Get the [optionally formatted] temporal [data_vencimento] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataVencimento($format = NULL)
    {
        if ($format === null) {
            return $this->data_vencimento;
        } else {
            return $this->data_vencimento instanceof \DateTimeInterface ? $this->data_vencimento->format($format) : null;
        }
    }

    /**
     * Get the [numero_documento] column value.
     *
     * @return string
     */
    public function getNumeroDocumento()
    {
        return $this->numero_documento;
    }

    /**
     * Get the [codigo_movimento] column value.
     *
     * @return string
     */
    public function getCodigoMovimento()
    {
        return $this->codigo_movimento;
    }

    /**
     * Get the [descricao_movimento] column value.
     *
     * @return string
     */
    public function getDescricaoMovimento()
    {
        return $this->descricao_movimento;
    }

    /**
     * Get the [optionally formatted] temporal [data_cadastro] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataCadastro($format = NULL)
    {
        if ($format === null) {
            return $this->data_cadastro;
        } else {
            return $this->data_cadastro instanceof \DateTimeInterface ? $this->data_cadastro->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [data_alterado] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDataAlterado($format = NULL)
    {
        if ($format === null) {
            return $this->data_alterado;
        } else {
            return $this->data_alterado instanceof \DateTimeInterface ? $this->data_alterado->format($format) : null;
        }
    }

    /**
     * Get the [usuario_alterado] column value.
     *
     * @return int
     */
    public function getUsuarioAlterado()
    {
        return $this->usuario_alterado;
    }

    /**
     * Set the value of [idboleto_baixa_historico] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setIdboletoBaixaHistorico($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idboleto_baixa_historico !== $v) {
            $this->idboleto_baixa_historico = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO] = true;
        }

        return $this;
    } // setIdboletoBaixaHistorico()

    /**
     * Set the value of [boleto_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setBoletoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->boleto_id !== $v) {
            $this->boleto_id = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_BOLETO_ID] = true;
        }

        if ($this->aBoleto !== null && $this->aBoleto->getIdboleto() !== $v) {
            $this->aBoleto = null;
        }

        return $this;
    } // setBoletoId()

    /**
     * Set the value of [lancamento_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setLancamentoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->lancamento_id !== $v) {
            $this->lancamento_id = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID] = true;
        }

        if ($this->aLancamentos !== null && $this->aLancamentos->getIdlancamento() !== $v) {
            $this->aLancamentos = null;
        }

        return $this;
    } // setLancamentoId()

    /**
     * Set the value of [pessoa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setPessoaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pessoa_id !== $v) {
            $this->pessoa_id = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_PESSOA_ID] = true;
        }

        if ($this->aPessoa !== null && $this->aPessoa->getId() !== $v) {
            $this->aPessoa = null;
        }

        return $this;
    } // setPessoaId()

    /**
     * Set the value of [competencia_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setCompetenciaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->competencia_id !== $v) {
            $this->competencia_id = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID] = true;
        }

        if ($this->aCompetencia !== null && $this->aCompetencia->getId() !== $v) {
            $this->aCompetencia = null;
        }

        return $this;
    } // setCompetenciaId()

    /**
     * Set the value of [baixa_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setBaixaId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->baixa_id !== $v) {
            $this->baixa_id = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_BAIXA_ID] = true;
        }

        if ($this->aBaixa !== null && $this->aBaixa->getIdbaixa() !== $v) {
            $this->aBaixa = null;
        }

        return $this;
    } // setBaixaId()

    /**
     * Set the value of [baixa_estorno_id] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setBaixaEstornoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->baixa_estorno_id !== $v) {
            $this->baixa_estorno_id = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID] = true;
        }

        if ($this->aBaixaEstorno !== null && $this->aBaixaEstorno->getIdbaixaEstorno() !== $v) {
            $this->aBaixaEstorno = null;
        }

        return $this;
    } // setBaixaEstornoId()

    /**
     * Set the value of [status] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Set the value of [mensagem_retorno] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setMensagemRetorno($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mensagem_retorno !== $v) {
            $this->mensagem_retorno = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_MENSAGEM_RETORNO] = true;
        }

        return $this;
    } // setMensagemRetorno()

    /**
     * Set the value of [data_pagamento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setDataPagamento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->data_pagamento !== $v) {
            $this->data_pagamento = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_DATA_PAGAMENTO] = true;
        }

        return $this;
    } // setDataPagamento()

    /**
     * Set the value of [data_recusa] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setDataRecusa($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->data_recusa !== $v) {
            $this->data_recusa = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_DATA_RECUSA] = true;
        }

        return $this;
    } // setDataRecusa()

    /**
     * Set the value of [valor_tarifa_custo] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setValorTarifaCusto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_tarifa_custo !== $v) {
            $this->valor_tarifa_custo = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO] = true;
        }

        return $this;
    } // setValorTarifaCusto()

    /**
     * Set the value of [valor_original] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setValorOriginal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_original !== $v) {
            $this->valor_original = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL] = true;
        }

        return $this;
    } // setValorOriginal()

    /**
     * Set the value of [valor_multa] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setValorMulta($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_multa !== $v) {
            $this->valor_multa = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA] = true;
        }

        return $this;
    } // setValorMulta()

    /**
     * Set the value of [valor_juros] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setValorJuros($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_juros !== $v) {
            $this->valor_juros = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS] = true;
        }

        return $this;
    } // setValorJuros()

    /**
     * Set the value of [valor_desconto] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setValorDesconto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_desconto !== $v) {
            $this->valor_desconto = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO] = true;
        }

        return $this;
    } // setValorDesconto()

    /**
     * Set the value of [valor_total] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setValorTotal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->valor_total !== $v) {
            $this->valor_total = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL] = true;
        }

        return $this;
    } // setValorTotal()

    /**
     * Sets the value of [data_vencimento] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setDataVencimento($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_vencimento !== null || $dt !== null) {
            if ($this->data_vencimento === null || $dt === null || $dt->format("Y-m-d") !== $this->data_vencimento->format("Y-m-d")) {
                $this->data_vencimento = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataVencimento()

    /**
     * Set the value of [numero_documento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setNumeroDocumento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->numero_documento !== $v) {
            $this->numero_documento = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_NUMERO_DOCUMENTO] = true;
        }

        return $this;
    } // setNumeroDocumento()

    /**
     * Set the value of [codigo_movimento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setCodigoMovimento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo_movimento !== $v) {
            $this->codigo_movimento = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_CODIGO_MOVIMENTO] = true;
        }

        return $this;
    } // setCodigoMovimento()

    /**
     * Set the value of [descricao_movimento] column.
     *
     * @param string $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setDescricaoMovimento($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descricao_movimento !== $v) {
            $this->descricao_movimento = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_DESCRICAO_MOVIMENTO] = true;
        }

        return $this;
    } // setDescricaoMovimento()

    /**
     * Sets the value of [data_cadastro] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setDataCadastro($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_cadastro !== null || $dt !== null) {
            if ($this->data_cadastro === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_cadastro->format("Y-m-d H:i:s.u")) {
                $this->data_cadastro = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataCadastro()

    /**
     * Sets the value of [data_alterado] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setDataAlterado($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->data_alterado !== null || $dt !== null) {
            if ($this->data_alterado === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->data_alterado->format("Y-m-d H:i:s.u")) {
                $this->data_alterado = $dt === null ? null : clone $dt;
                $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO] = true;
            }
        } // if either are not null

        return $this;
    } // setDataAlterado()

    /**
     * Set the value of [usuario_alterado] column.
     *
     * @param int $v new value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     */
    public function setUsuarioAlterado($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->usuario_alterado !== $v) {
            $this->usuario_alterado = $v;
            $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getIdusuario() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setUsuarioAlterado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->valor_tarifa_custo !== '0.00') {
                return false;
            }

            if ($this->valor_original !== '0.00') {
                return false;
            }

            if ($this->valor_multa !== '0.00') {
                return false;
            }

            if ($this->valor_juros !== '0.00') {
                return false;
            }

            if ($this->valor_desconto !== '0.00') {
                return false;
            }

            if ($this->valor_total !== '0.00') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('IdboletoBaixaHistorico', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idboleto_baixa_historico = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('BoletoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->boleto_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('LancamentoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->lancamento_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('PessoaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pessoa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('CompetenciaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->competencia_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('BaixaId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->baixa_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('BaixaEstornoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->baixa_estorno_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('MensagemRetorno', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mensagem_retorno = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('DataPagamento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->data_pagamento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('DataRecusa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->data_recusa = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('ValorTarifaCusto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_tarifa_custo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('ValorOriginal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_original = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('ValorMulta', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_multa = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('ValorJuros', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_juros = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('ValorDesconto', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_desconto = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('ValorTotal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->valor_total = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('DataVencimento', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->data_vencimento = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('NumeroDocumento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->numero_documento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('CodigoMovimento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->codigo_movimento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('DescricaoMovimento', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descricao_movimento = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('DataCadastro', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_cadastro = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('DataAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->data_alterado = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : BoletoBaixaHistoricoTableMap::translateFieldName('UsuarioAlterado', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario_alterado = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 24; // 24 = BoletoBaixaHistoricoTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aBoleto !== null && $this->boleto_id !== $this->aBoleto->getIdboleto()) {
            $this->aBoleto = null;
        }
        if ($this->aLancamentos !== null && $this->lancamento_id !== $this->aLancamentos->getIdlancamento()) {
            $this->aLancamentos = null;
        }
        if ($this->aPessoa !== null && $this->pessoa_id !== $this->aPessoa->getId()) {
            $this->aPessoa = null;
        }
        if ($this->aCompetencia !== null && $this->competencia_id !== $this->aCompetencia->getId()) {
            $this->aCompetencia = null;
        }
        if ($this->aBaixa !== null && $this->baixa_id !== $this->aBaixa->getIdbaixa()) {
            $this->aBaixa = null;
        }
        if ($this->aBaixaEstorno !== null && $this->baixa_estorno_id !== $this->aBaixaEstorno->getIdbaixaEstorno()) {
            $this->aBaixaEstorno = null;
        }
        if ($this->aUsuario !== null && $this->usuario_alterado !== $this->aUsuario->getIdusuario()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildBoletoBaixaHistoricoQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBaixaEstorno = null;
            $this->aBaixa = null;
            $this->aBoleto = null;
            $this->aCompetencia = null;
            $this->aLancamentos = null;
            $this->aPessoa = null;
            $this->aUsuario = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see BoletoBaixaHistorico::setDeleted()
     * @see BoletoBaixaHistorico::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildBoletoBaixaHistoricoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                BoletoBaixaHistoricoTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBaixaEstorno !== null) {
                if ($this->aBaixaEstorno->isModified() || $this->aBaixaEstorno->isNew()) {
                    $affectedRows += $this->aBaixaEstorno->save($con);
                }
                $this->setBaixaEstorno($this->aBaixaEstorno);
            }

            if ($this->aBaixa !== null) {
                if ($this->aBaixa->isModified() || $this->aBaixa->isNew()) {
                    $affectedRows += $this->aBaixa->save($con);
                }
                $this->setBaixa($this->aBaixa);
            }

            if ($this->aBoleto !== null) {
                if ($this->aBoleto->isModified() || $this->aBoleto->isNew()) {
                    $affectedRows += $this->aBoleto->save($con);
                }
                $this->setBoleto($this->aBoleto);
            }

            if ($this->aCompetencia !== null) {
                if ($this->aCompetencia->isModified() || $this->aCompetencia->isNew()) {
                    $affectedRows += $this->aCompetencia->save($con);
                }
                $this->setCompetencia($this->aCompetencia);
            }

            if ($this->aLancamentos !== null) {
                if ($this->aLancamentos->isModified() || $this->aLancamentos->isNew()) {
                    $affectedRows += $this->aLancamentos->save($con);
                }
                $this->setLancamentos($this->aLancamentos);
            }

            if ($this->aPessoa !== null) {
                if ($this->aPessoa->isModified() || $this->aPessoa->isNew()) {
                    $affectedRows += $this->aPessoa->save($con);
                }
                $this->setPessoa($this->aPessoa);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO] = true;
        if (null !== $this->idboleto_baixa_historico) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO)) {
            $modifiedColumns[':p' . $index++]  = 'idboleto_baixa_historico';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_BOLETO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'boleto_id';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'lancamento_id';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_PESSOA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'pessoa_id';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'competencia_id';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_BAIXA_ID)) {
            $modifiedColumns[':p' . $index++]  = 'baixa_id';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'baixa_estorno_id';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_MENSAGEM_RETORNO)) {
            $modifiedColumns[':p' . $index++]  = 'mensagem_retorno';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_PAGAMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'data_pagamento';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_RECUSA)) {
            $modifiedColumns[':p' . $index++]  = 'data_recusa';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_tarifa_custo';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL)) {
            $modifiedColumns[':p' . $index++]  = 'valor_original';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA)) {
            $modifiedColumns[':p' . $index++]  = 'valor_multa';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS)) {
            $modifiedColumns[':p' . $index++]  = 'valor_juros';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO)) {
            $modifiedColumns[':p' . $index++]  = 'valor_desconto';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL)) {
            $modifiedColumns[':p' . $index++]  = 'valor_total';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'data_vencimento';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_NUMERO_DOCUMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'numero_documento';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_CODIGO_MOVIMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'codigo_movimento';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DESCRICAO_MOVIMENTO)) {
            $modifiedColumns[':p' . $index++]  = 'descricao_movimento';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO)) {
            $modifiedColumns[':p' . $index++]  = 'data_cadastro';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'data_alterado';
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario_alterado';
        }

        $sql = sprintf(
            'INSERT INTO boleto_baixa_historico (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idboleto_baixa_historico':
                        $stmt->bindValue($identifier, $this->idboleto_baixa_historico, PDO::PARAM_INT);
                        break;
                    case 'boleto_id':
                        $stmt->bindValue($identifier, $this->boleto_id, PDO::PARAM_INT);
                        break;
                    case 'lancamento_id':
                        $stmt->bindValue($identifier, $this->lancamento_id, PDO::PARAM_INT);
                        break;
                    case 'pessoa_id':
                        $stmt->bindValue($identifier, $this->pessoa_id, PDO::PARAM_INT);
                        break;
                    case 'competencia_id':
                        $stmt->bindValue($identifier, $this->competencia_id, PDO::PARAM_INT);
                        break;
                    case 'baixa_id':
                        $stmt->bindValue($identifier, $this->baixa_id, PDO::PARAM_INT);
                        break;
                    case 'baixa_estorno_id':
                        $stmt->bindValue($identifier, $this->baixa_estorno_id, PDO::PARAM_INT);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case 'mensagem_retorno':
                        $stmt->bindValue($identifier, $this->mensagem_retorno, PDO::PARAM_STR);
                        break;
                    case 'data_pagamento':
                        $stmt->bindValue($identifier, $this->data_pagamento, PDO::PARAM_STR);
                        break;
                    case 'data_recusa':
                        $stmt->bindValue($identifier, $this->data_recusa, PDO::PARAM_STR);
                        break;
                    case 'valor_tarifa_custo':
                        $stmt->bindValue($identifier, $this->valor_tarifa_custo, PDO::PARAM_STR);
                        break;
                    case 'valor_original':
                        $stmt->bindValue($identifier, $this->valor_original, PDO::PARAM_STR);
                        break;
                    case 'valor_multa':
                        $stmt->bindValue($identifier, $this->valor_multa, PDO::PARAM_STR);
                        break;
                    case 'valor_juros':
                        $stmt->bindValue($identifier, $this->valor_juros, PDO::PARAM_STR);
                        break;
                    case 'valor_desconto':
                        $stmt->bindValue($identifier, $this->valor_desconto, PDO::PARAM_STR);
                        break;
                    case 'valor_total':
                        $stmt->bindValue($identifier, $this->valor_total, PDO::PARAM_STR);
                        break;
                    case 'data_vencimento':
                        $stmt->bindValue($identifier, $this->data_vencimento ? $this->data_vencimento->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'numero_documento':
                        $stmt->bindValue($identifier, $this->numero_documento, PDO::PARAM_STR);
                        break;
                    case 'codigo_movimento':
                        $stmt->bindValue($identifier, $this->codigo_movimento, PDO::PARAM_STR);
                        break;
                    case 'descricao_movimento':
                        $stmt->bindValue($identifier, $this->descricao_movimento, PDO::PARAM_STR);
                        break;
                    case 'data_cadastro':
                        $stmt->bindValue($identifier, $this->data_cadastro ? $this->data_cadastro->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'data_alterado':
                        $stmt->bindValue($identifier, $this->data_alterado ? $this->data_alterado->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'usuario_alterado':
                        $stmt->bindValue($identifier, $this->usuario_alterado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdboletoBaixaHistorico($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BoletoBaixaHistoricoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdboletoBaixaHistorico();
                break;
            case 1:
                return $this->getBoletoId();
                break;
            case 2:
                return $this->getLancamentoId();
                break;
            case 3:
                return $this->getPessoaId();
                break;
            case 4:
                return $this->getCompetenciaId();
                break;
            case 5:
                return $this->getBaixaId();
                break;
            case 6:
                return $this->getBaixaEstornoId();
                break;
            case 7:
                return $this->getStatus();
                break;
            case 8:
                return $this->getMensagemRetorno();
                break;
            case 9:
                return $this->getDataPagamento();
                break;
            case 10:
                return $this->getDataRecusa();
                break;
            case 11:
                return $this->getValorTarifaCusto();
                break;
            case 12:
                return $this->getValorOriginal();
                break;
            case 13:
                return $this->getValorMulta();
                break;
            case 14:
                return $this->getValorJuros();
                break;
            case 15:
                return $this->getValorDesconto();
                break;
            case 16:
                return $this->getValorTotal();
                break;
            case 17:
                return $this->getDataVencimento();
                break;
            case 18:
                return $this->getNumeroDocumento();
                break;
            case 19:
                return $this->getCodigoMovimento();
                break;
            case 20:
                return $this->getDescricaoMovimento();
                break;
            case 21:
                return $this->getDataCadastro();
                break;
            case 22:
                return $this->getDataAlterado();
                break;
            case 23:
                return $this->getUsuarioAlterado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['BoletoBaixaHistorico'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['BoletoBaixaHistorico'][$this->hashCode()] = true;
        $keys = BoletoBaixaHistoricoTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdboletoBaixaHistorico(),
            $keys[1] => $this->getBoletoId(),
            $keys[2] => $this->getLancamentoId(),
            $keys[3] => $this->getPessoaId(),
            $keys[4] => $this->getCompetenciaId(),
            $keys[5] => $this->getBaixaId(),
            $keys[6] => $this->getBaixaEstornoId(),
            $keys[7] => $this->getStatus(),
            $keys[8] => $this->getMensagemRetorno(),
            $keys[9] => $this->getDataPagamento(),
            $keys[10] => $this->getDataRecusa(),
            $keys[11] => $this->getValorTarifaCusto(),
            $keys[12] => $this->getValorOriginal(),
            $keys[13] => $this->getValorMulta(),
            $keys[14] => $this->getValorJuros(),
            $keys[15] => $this->getValorDesconto(),
            $keys[16] => $this->getValorTotal(),
            $keys[17] => $this->getDataVencimento(),
            $keys[18] => $this->getNumeroDocumento(),
            $keys[19] => $this->getCodigoMovimento(),
            $keys[20] => $this->getDescricaoMovimento(),
            $keys[21] => $this->getDataCadastro(),
            $keys[22] => $this->getDataAlterado(),
            $keys[23] => $this->getUsuarioAlterado(),
        );
        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[21]] instanceof \DateTime) {
            $result[$keys[21]] = $result[$keys[21]]->format('c');
        }

        if ($result[$keys[22]] instanceof \DateTime) {
            $result[$keys[22]] = $result[$keys[22]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBaixaEstorno) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixaEstorno';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixa_estorno';
                        break;
                    default:
                        $key = 'BaixaEstorno';
                }

                $result[$key] = $this->aBaixaEstorno->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBaixa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'baixa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'baixa';
                        break;
                    default:
                        $key = 'Baixa';
                }

                $result[$key] = $this->aBaixa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBoleto) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'boleto';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'boleto';
                        break;
                    default:
                        $key = 'Boleto';
                }

                $result[$key] = $this->aBoleto->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCompetencia) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'competencia';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'competencia';
                        break;
                    default:
                        $key = 'Competencia';
                }

                $result[$key] = $this->aCompetencia->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLancamentos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'lancamentos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'lancamentos';
                        break;
                    default:
                        $key = 'Lancamentos';
                }

                $result[$key] = $this->aLancamentos->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPessoa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pessoa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'pessoa';
                        break;
                    default:
                        $key = 'Pessoa';
                }

                $result[$key] = $this->aPessoa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = BoletoBaixaHistoricoTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdboletoBaixaHistorico($value);
                break;
            case 1:
                $this->setBoletoId($value);
                break;
            case 2:
                $this->setLancamentoId($value);
                break;
            case 3:
                $this->setPessoaId($value);
                break;
            case 4:
                $this->setCompetenciaId($value);
                break;
            case 5:
                $this->setBaixaId($value);
                break;
            case 6:
                $this->setBaixaEstornoId($value);
                break;
            case 7:
                $this->setStatus($value);
                break;
            case 8:
                $this->setMensagemRetorno($value);
                break;
            case 9:
                $this->setDataPagamento($value);
                break;
            case 10:
                $this->setDataRecusa($value);
                break;
            case 11:
                $this->setValorTarifaCusto($value);
                break;
            case 12:
                $this->setValorOriginal($value);
                break;
            case 13:
                $this->setValorMulta($value);
                break;
            case 14:
                $this->setValorJuros($value);
                break;
            case 15:
                $this->setValorDesconto($value);
                break;
            case 16:
                $this->setValorTotal($value);
                break;
            case 17:
                $this->setDataVencimento($value);
                break;
            case 18:
                $this->setNumeroDocumento($value);
                break;
            case 19:
                $this->setCodigoMovimento($value);
                break;
            case 20:
                $this->setDescricaoMovimento($value);
                break;
            case 21:
                $this->setDataCadastro($value);
                break;
            case 22:
                $this->setDataAlterado($value);
                break;
            case 23:
                $this->setUsuarioAlterado($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = BoletoBaixaHistoricoTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdboletoBaixaHistorico($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setBoletoId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setLancamentoId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPessoaId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCompetenciaId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setBaixaId($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setBaixaEstornoId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setStatus($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setMensagemRetorno($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDataPagamento($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDataRecusa($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setValorTarifaCusto($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setValorOriginal($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setValorMulta($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setValorJuros($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setValorDesconto($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setValorTotal($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setDataVencimento($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setNumeroDocumento($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setCodigoMovimento($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setDescricaoMovimento($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setDataCadastro($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setDataAlterado($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setUsuarioAlterado($arr[$keys[23]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(BoletoBaixaHistoricoTableMap::DATABASE_NAME);

        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, $this->idboleto_baixa_historico);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_BOLETO_ID)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_BOLETO_ID, $this->boleto_id);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID, $this->lancamento_id);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_PESSOA_ID)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_PESSOA_ID, $this->pessoa_id);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID, $this->competencia_id);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_BAIXA_ID)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_BAIXA_ID, $this->baixa_id);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID, $this->baixa_estorno_id);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_STATUS)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_MENSAGEM_RETORNO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_MENSAGEM_RETORNO, $this->mensagem_retorno);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_PAGAMENTO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_DATA_PAGAMENTO, $this->data_pagamento);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_RECUSA)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_DATA_RECUSA, $this->data_recusa);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO, $this->valor_tarifa_custo);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL, $this->valor_original);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA, $this->valor_multa);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS, $this->valor_juros);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO, $this->valor_desconto);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL, $this->valor_total);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO, $this->data_vencimento);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_NUMERO_DOCUMENTO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_NUMERO_DOCUMENTO, $this->numero_documento);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_CODIGO_MOVIMENTO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_CODIGO_MOVIMENTO, $this->codigo_movimento);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DESCRICAO_MOVIMENTO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_DESCRICAO_MOVIMENTO, $this->descricao_movimento);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO, $this->data_cadastro);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO, $this->data_alterado);
        }
        if ($this->isColumnModified(BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO)) {
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO, $this->usuario_alterado);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildBoletoBaixaHistoricoQuery::create();
        $criteria->add(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, $this->idboleto_baixa_historico);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdboletoBaixaHistorico();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdboletoBaixaHistorico();
    }

    /**
     * Generic method to set the primary key (idboleto_baixa_historico column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdboletoBaixaHistorico($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdboletoBaixaHistorico();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \ImaTelecomBundle\Model\BoletoBaixaHistorico (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setBoletoId($this->getBoletoId());
        $copyObj->setLancamentoId($this->getLancamentoId());
        $copyObj->setPessoaId($this->getPessoaId());
        $copyObj->setCompetenciaId($this->getCompetenciaId());
        $copyObj->setBaixaId($this->getBaixaId());
        $copyObj->setBaixaEstornoId($this->getBaixaEstornoId());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setMensagemRetorno($this->getMensagemRetorno());
        $copyObj->setDataPagamento($this->getDataPagamento());
        $copyObj->setDataRecusa($this->getDataRecusa());
        $copyObj->setValorTarifaCusto($this->getValorTarifaCusto());
        $copyObj->setValorOriginal($this->getValorOriginal());
        $copyObj->setValorMulta($this->getValorMulta());
        $copyObj->setValorJuros($this->getValorJuros());
        $copyObj->setValorDesconto($this->getValorDesconto());
        $copyObj->setValorTotal($this->getValorTotal());
        $copyObj->setDataVencimento($this->getDataVencimento());
        $copyObj->setNumeroDocumento($this->getNumeroDocumento());
        $copyObj->setCodigoMovimento($this->getCodigoMovimento());
        $copyObj->setDescricaoMovimento($this->getDescricaoMovimento());
        $copyObj->setDataCadastro($this->getDataCadastro());
        $copyObj->setDataAlterado($this->getDataAlterado());
        $copyObj->setUsuarioAlterado($this->getUsuarioAlterado());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdboletoBaixaHistorico(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \ImaTelecomBundle\Model\BoletoBaixaHistorico Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildBaixaEstorno object.
     *
     * @param  ChildBaixaEstorno $v
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBaixaEstorno(ChildBaixaEstorno $v = null)
    {
        if ($v === null) {
            $this->setBaixaEstornoId(NULL);
        } else {
            $this->setBaixaEstornoId($v->getIdbaixaEstorno());
        }

        $this->aBaixaEstorno = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBaixaEstorno object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoBaixaHistorico($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBaixaEstorno object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBaixaEstorno The associated ChildBaixaEstorno object.
     * @throws PropelException
     */
    public function getBaixaEstorno(ConnectionInterface $con = null)
    {
        if ($this->aBaixaEstorno === null && ($this->baixa_estorno_id !== null)) {
            $this->aBaixaEstorno = ChildBaixaEstornoQuery::create()->findPk($this->baixa_estorno_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBaixaEstorno->addBoletoBaixaHistoricos($this);
             */
        }

        return $this->aBaixaEstorno;
    }

    /**
     * Declares an association between this object and a ChildBaixa object.
     *
     * @param  ChildBaixa $v
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBaixa(ChildBaixa $v = null)
    {
        if ($v === null) {
            $this->setBaixaId(NULL);
        } else {
            $this->setBaixaId($v->getIdbaixa());
        }

        $this->aBaixa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBaixa object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoBaixaHistorico($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBaixa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBaixa The associated ChildBaixa object.
     * @throws PropelException
     */
    public function getBaixa(ConnectionInterface $con = null)
    {
        if ($this->aBaixa === null && ($this->baixa_id !== null)) {
            $this->aBaixa = ChildBaixaQuery::create()->findPk($this->baixa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBaixa->addBoletoBaixaHistoricos($this);
             */
        }

        return $this->aBaixa;
    }

    /**
     * Declares an association between this object and a ChildBoleto object.
     *
     * @param  ChildBoleto $v
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBoleto(ChildBoleto $v = null)
    {
        if ($v === null) {
            $this->setBoletoId(NULL);
        } else {
            $this->setBoletoId($v->getIdboleto());
        }

        $this->aBoleto = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildBoleto object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoBaixaHistorico($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildBoleto object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildBoleto The associated ChildBoleto object.
     * @throws PropelException
     */
    public function getBoleto(ConnectionInterface $con = null)
    {
        if ($this->aBoleto === null && ($this->boleto_id !== null)) {
            $this->aBoleto = ChildBoletoQuery::create()->findPk($this->boleto_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBoleto->addBoletoBaixaHistoricos($this);
             */
        }

        return $this->aBoleto;
    }

    /**
     * Declares an association between this object and a ChildCompetencia object.
     *
     * @param  ChildCompetencia $v
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCompetencia(ChildCompetencia $v = null)
    {
        if ($v === null) {
            $this->setCompetenciaId(NULL);
        } else {
            $this->setCompetenciaId($v->getId());
        }

        $this->aCompetencia = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCompetencia object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoBaixaHistorico($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCompetencia object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCompetencia The associated ChildCompetencia object.
     * @throws PropelException
     */
    public function getCompetencia(ConnectionInterface $con = null)
    {
        if ($this->aCompetencia === null && ($this->competencia_id !== null)) {
            $this->aCompetencia = ChildCompetenciaQuery::create()->findPk($this->competencia_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCompetencia->addBoletoBaixaHistoricos($this);
             */
        }

        return $this->aCompetencia;
    }

    /**
     * Declares an association between this object and a ChildLancamentos object.
     *
     * @param  ChildLancamentos $v
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLancamentos(ChildLancamentos $v = null)
    {
        if ($v === null) {
            $this->setLancamentoId(NULL);
        } else {
            $this->setLancamentoId($v->getIdlancamento());
        }

        $this->aLancamentos = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildLancamentos object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoBaixaHistorico($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildLancamentos object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildLancamentos The associated ChildLancamentos object.
     * @throws PropelException
     */
    public function getLancamentos(ConnectionInterface $con = null)
    {
        if ($this->aLancamentos === null && ($this->lancamento_id !== null)) {
            $this->aLancamentos = ChildLancamentosQuery::create()->findPk($this->lancamento_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLancamentos->addBoletoBaixaHistoricos($this);
             */
        }

        return $this->aLancamentos;
    }

    /**
     * Declares an association between this object and a ChildPessoa object.
     *
     * @param  ChildPessoa $v
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPessoa(ChildPessoa $v = null)
    {
        if ($v === null) {
            $this->setPessoaId(NULL);
        } else {
            $this->setPessoaId($v->getId());
        }

        $this->aPessoa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPessoa object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoBaixaHistorico($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPessoa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPessoa The associated ChildPessoa object.
     * @throws PropelException
     */
    public function getPessoa(ConnectionInterface $con = null)
    {
        if ($this->aPessoa === null && ($this->pessoa_id !== null)) {
            $this->aPessoa = ChildPessoaQuery::create()->findPk($this->pessoa_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPessoa->addBoletoBaixaHistoricos($this);
             */
        }

        return $this->aPessoa;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\ImaTelecomBundle\Model\BoletoBaixaHistorico The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setUsuarioAlterado(NULL);
        } else {
            $this->setUsuarioAlterado($v->getIdusuario());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addBoletoBaixaHistorico($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->usuario_alterado !== null)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->usuario_alterado, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addBoletoBaixaHistoricos($this);
             */
        }

        return $this->aUsuario;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBaixaEstorno) {
            $this->aBaixaEstorno->removeBoletoBaixaHistorico($this);
        }
        if (null !== $this->aBaixa) {
            $this->aBaixa->removeBoletoBaixaHistorico($this);
        }
        if (null !== $this->aBoleto) {
            $this->aBoleto->removeBoletoBaixaHistorico($this);
        }
        if (null !== $this->aCompetencia) {
            $this->aCompetencia->removeBoletoBaixaHistorico($this);
        }
        if (null !== $this->aLancamentos) {
            $this->aLancamentos->removeBoletoBaixaHistorico($this);
        }
        if (null !== $this->aPessoa) {
            $this->aPessoa->removeBoletoBaixaHistorico($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeBoletoBaixaHistorico($this);
        }
        $this->idboleto_baixa_historico = null;
        $this->boleto_id = null;
        $this->lancamento_id = null;
        $this->pessoa_id = null;
        $this->competencia_id = null;
        $this->baixa_id = null;
        $this->baixa_estorno_id = null;
        $this->status = null;
        $this->mensagem_retorno = null;
        $this->data_pagamento = null;
        $this->data_recusa = null;
        $this->valor_tarifa_custo = null;
        $this->valor_original = null;
        $this->valor_multa = null;
        $this->valor_juros = null;
        $this->valor_desconto = null;
        $this->valor_total = null;
        $this->data_vencimento = null;
        $this->numero_documento = null;
        $this->codigo_movimento = null;
        $this->descricao_movimento = null;
        $this->data_cadastro = null;
        $this->data_alterado = null;
        $this->usuario_alterado = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aBaixaEstorno = null;
        $this->aBaixa = null;
        $this->aBoleto = null;
        $this->aCompetencia = null;
        $this->aLancamentos = null;
        $this->aPessoa = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(BoletoBaixaHistoricoTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
