<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\EstoqueLancamento as ChildEstoqueLancamento;
use ImaTelecomBundle\Model\EstoqueLancamentoQuery as ChildEstoqueLancamentoQuery;
use ImaTelecomBundle\Model\Map\EstoqueLancamentoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'estoque_lancamento' table.
 *
 *
 *
 * @method     ChildEstoqueLancamentoQuery orderByIdestoqueLancamento($order = Criteria::ASC) Order by the idestoque_lancamento column
 * @method     ChildEstoqueLancamentoQuery orderByData($order = Criteria::ASC) Order by the data column
 * @method     ChildEstoqueLancamentoQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 * @method     ChildEstoqueLancamentoQuery orderByMovimento($order = Criteria::ASC) Order by the movimento column
 * @method     ChildEstoqueLancamentoQuery orderByOsId($order = Criteria::ASC) Order by the os_id column
 * @method     ChildEstoqueLancamentoQuery orderByClienteId($order = Criteria::ASC) Order by the cliente_id column
 * @method     ChildEstoqueLancamentoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildEstoqueLancamentoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildEstoqueLancamentoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildEstoqueLancamentoQuery groupByIdestoqueLancamento() Group by the idestoque_lancamento column
 * @method     ChildEstoqueLancamentoQuery groupByData() Group by the data column
 * @method     ChildEstoqueLancamentoQuery groupByTipo() Group by the tipo column
 * @method     ChildEstoqueLancamentoQuery groupByMovimento() Group by the movimento column
 * @method     ChildEstoqueLancamentoQuery groupByOsId() Group by the os_id column
 * @method     ChildEstoqueLancamentoQuery groupByClienteId() Group by the cliente_id column
 * @method     ChildEstoqueLancamentoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildEstoqueLancamentoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildEstoqueLancamentoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildEstoqueLancamentoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEstoqueLancamentoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEstoqueLancamentoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEstoqueLancamentoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEstoqueLancamentoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEstoqueLancamentoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEstoqueLancamentoQuery leftJoinCliente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Cliente relation
 * @method     ChildEstoqueLancamentoQuery rightJoinCliente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Cliente relation
 * @method     ChildEstoqueLancamentoQuery innerJoinCliente($relationAlias = null) Adds a INNER JOIN clause to the query using the Cliente relation
 *
 * @method     ChildEstoqueLancamentoQuery joinWithCliente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Cliente relation
 *
 * @method     ChildEstoqueLancamentoQuery leftJoinWithCliente() Adds a LEFT JOIN clause and with to the query using the Cliente relation
 * @method     ChildEstoqueLancamentoQuery rightJoinWithCliente() Adds a RIGHT JOIN clause and with to the query using the Cliente relation
 * @method     ChildEstoqueLancamentoQuery innerJoinWithCliente() Adds a INNER JOIN clause and with to the query using the Cliente relation
 *
 * @method     ChildEstoqueLancamentoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildEstoqueLancamentoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildEstoqueLancamentoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildEstoqueLancamentoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildEstoqueLancamentoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEstoqueLancamentoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEstoqueLancamentoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildEstoqueLancamentoQuery leftJoinEstoqueLancamentoItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the EstoqueLancamentoItem relation
 * @method     ChildEstoqueLancamentoQuery rightJoinEstoqueLancamentoItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EstoqueLancamentoItem relation
 * @method     ChildEstoqueLancamentoQuery innerJoinEstoqueLancamentoItem($relationAlias = null) Adds a INNER JOIN clause to the query using the EstoqueLancamentoItem relation
 *
 * @method     ChildEstoqueLancamentoQuery joinWithEstoqueLancamentoItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EstoqueLancamentoItem relation
 *
 * @method     ChildEstoqueLancamentoQuery leftJoinWithEstoqueLancamentoItem() Adds a LEFT JOIN clause and with to the query using the EstoqueLancamentoItem relation
 * @method     ChildEstoqueLancamentoQuery rightJoinWithEstoqueLancamentoItem() Adds a RIGHT JOIN clause and with to the query using the EstoqueLancamentoItem relation
 * @method     ChildEstoqueLancamentoQuery innerJoinWithEstoqueLancamentoItem() Adds a INNER JOIN clause and with to the query using the EstoqueLancamentoItem relation
 *
 * @method     \ImaTelecomBundle\Model\ClienteQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\EstoqueLancamentoItemQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEstoqueLancamento findOne(ConnectionInterface $con = null) Return the first ChildEstoqueLancamento matching the query
 * @method     ChildEstoqueLancamento findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEstoqueLancamento matching the query, or a new ChildEstoqueLancamento object populated from the query conditions when no match is found
 *
 * @method     ChildEstoqueLancamento findOneByIdestoqueLancamento(int $idestoque_lancamento) Return the first ChildEstoqueLancamento filtered by the idestoque_lancamento column
 * @method     ChildEstoqueLancamento findOneByData(string $data) Return the first ChildEstoqueLancamento filtered by the data column
 * @method     ChildEstoqueLancamento findOneByTipo(string $tipo) Return the first ChildEstoqueLancamento filtered by the tipo column
 * @method     ChildEstoqueLancamento findOneByMovimento(string $movimento) Return the first ChildEstoqueLancamento filtered by the movimento column
 * @method     ChildEstoqueLancamento findOneByOsId(int $os_id) Return the first ChildEstoqueLancamento filtered by the os_id column
 * @method     ChildEstoqueLancamento findOneByClienteId(int $cliente_id) Return the first ChildEstoqueLancamento filtered by the cliente_id column
 * @method     ChildEstoqueLancamento findOneByDataCadastro(string $data_cadastro) Return the first ChildEstoqueLancamento filtered by the data_cadastro column
 * @method     ChildEstoqueLancamento findOneByDataAlterado(string $data_alterado) Return the first ChildEstoqueLancamento filtered by the data_alterado column
 * @method     ChildEstoqueLancamento findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEstoqueLancamento filtered by the usuario_alterado column *

 * @method     ChildEstoqueLancamento requirePk($key, ConnectionInterface $con = null) Return the ChildEstoqueLancamento by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamento requireOne(ConnectionInterface $con = null) Return the first ChildEstoqueLancamento matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEstoqueLancamento requireOneByIdestoqueLancamento(int $idestoque_lancamento) Return the first ChildEstoqueLancamento filtered by the idestoque_lancamento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamento requireOneByData(string $data) Return the first ChildEstoqueLancamento filtered by the data column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamento requireOneByTipo(string $tipo) Return the first ChildEstoqueLancamento filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamento requireOneByMovimento(string $movimento) Return the first ChildEstoqueLancamento filtered by the movimento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamento requireOneByOsId(int $os_id) Return the first ChildEstoqueLancamento filtered by the os_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamento requireOneByClienteId(int $cliente_id) Return the first ChildEstoqueLancamento filtered by the cliente_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamento requireOneByDataCadastro(string $data_cadastro) Return the first ChildEstoqueLancamento filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamento requireOneByDataAlterado(string $data_alterado) Return the first ChildEstoqueLancamento filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEstoqueLancamento requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildEstoqueLancamento filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEstoqueLancamento[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEstoqueLancamento objects based on current ModelCriteria
 * @method     ChildEstoqueLancamento[]|ObjectCollection findByIdestoqueLancamento(int $idestoque_lancamento) Return ChildEstoqueLancamento objects filtered by the idestoque_lancamento column
 * @method     ChildEstoqueLancamento[]|ObjectCollection findByData(string $data) Return ChildEstoqueLancamento objects filtered by the data column
 * @method     ChildEstoqueLancamento[]|ObjectCollection findByTipo(string $tipo) Return ChildEstoqueLancamento objects filtered by the tipo column
 * @method     ChildEstoqueLancamento[]|ObjectCollection findByMovimento(string $movimento) Return ChildEstoqueLancamento objects filtered by the movimento column
 * @method     ChildEstoqueLancamento[]|ObjectCollection findByOsId(int $os_id) Return ChildEstoqueLancamento objects filtered by the os_id column
 * @method     ChildEstoqueLancamento[]|ObjectCollection findByClienteId(int $cliente_id) Return ChildEstoqueLancamento objects filtered by the cliente_id column
 * @method     ChildEstoqueLancamento[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildEstoqueLancamento objects filtered by the data_cadastro column
 * @method     ChildEstoqueLancamento[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildEstoqueLancamento objects filtered by the data_alterado column
 * @method     ChildEstoqueLancamento[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildEstoqueLancamento objects filtered by the usuario_alterado column
 * @method     ChildEstoqueLancamento[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EstoqueLancamentoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\EstoqueLancamentoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\EstoqueLancamento', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEstoqueLancamentoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEstoqueLancamentoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEstoqueLancamentoQuery) {
            return $criteria;
        }
        $query = new ChildEstoqueLancamentoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEstoqueLancamento|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EstoqueLancamentoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EstoqueLancamentoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoqueLancamento A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idestoque_lancamento, data, tipo, movimento, os_id, cliente_id, data_cadastro, data_alterado, usuario_alterado FROM estoque_lancamento WHERE idestoque_lancamento = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEstoqueLancamento $obj */
            $obj = new ChildEstoqueLancamento();
            $obj->hydrate($row);
            EstoqueLancamentoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEstoqueLancamento|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_IDESTOQUE_LANCAMENTO, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_IDESTOQUE_LANCAMENTO, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idestoque_lancamento column
     *
     * Example usage:
     * <code>
     * $query->filterByIdestoqueLancamento(1234); // WHERE idestoque_lancamento = 1234
     * $query->filterByIdestoqueLancamento(array(12, 34)); // WHERE idestoque_lancamento IN (12, 34)
     * $query->filterByIdestoqueLancamento(array('min' => 12)); // WHERE idestoque_lancamento > 12
     * </code>
     *
     * @param     mixed $idestoqueLancamento The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByIdestoqueLancamento($idestoqueLancamento = null, $comparison = null)
    {
        if (is_array($idestoqueLancamento)) {
            $useMinMax = false;
            if (isset($idestoqueLancamento['min'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_IDESTOQUE_LANCAMENTO, $idestoqueLancamento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idestoqueLancamento['max'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_IDESTOQUE_LANCAMENTO, $idestoqueLancamento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_IDESTOQUE_LANCAMENTO, $idestoqueLancamento, $comparison);
    }

    /**
     * Filter the query on the data column
     *
     * Example usage:
     * <code>
     * $query->filterByData('2011-03-14'); // WHERE data = '2011-03-14'
     * $query->filterByData('now'); // WHERE data = '2011-03-14'
     * $query->filterByData(array('max' => 'yesterday')); // WHERE data > '2011-03-13'
     * </code>
     *
     * @param     mixed $data The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByData($data = null, $comparison = null)
    {
        if (is_array($data)) {
            $useMinMax = false;
            if (isset($data['min'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_DATA, $data['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($data['max'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_DATA, $data['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_DATA, $data, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Filter the query on the movimento column
     *
     * Example usage:
     * <code>
     * $query->filterByMovimento('fooValue');   // WHERE movimento = 'fooValue'
     * $query->filterByMovimento('%fooValue%', Criteria::LIKE); // WHERE movimento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $movimento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByMovimento($movimento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($movimento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_MOVIMENTO, $movimento, $comparison);
    }

    /**
     * Filter the query on the os_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOsId(1234); // WHERE os_id = 1234
     * $query->filterByOsId(array(12, 34)); // WHERE os_id IN (12, 34)
     * $query->filterByOsId(array('min' => 12)); // WHERE os_id > 12
     * </code>
     *
     * @param     mixed $osId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByOsId($osId = null, $comparison = null)
    {
        if (is_array($osId)) {
            $useMinMax = false;
            if (isset($osId['min'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_OS_ID, $osId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($osId['max'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_OS_ID, $osId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_OS_ID, $osId, $comparison);
    }

    /**
     * Filter the query on the cliente_id column
     *
     * Example usage:
     * <code>
     * $query->filterByClienteId(1234); // WHERE cliente_id = 1234
     * $query->filterByClienteId(array(12, 34)); // WHERE cliente_id IN (12, 34)
     * $query->filterByClienteId(array('min' => 12)); // WHERE cliente_id > 12
     * </code>
     *
     * @see       filterByCliente()
     *
     * @param     mixed $clienteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByClienteId($clienteId = null, $comparison = null)
    {
        if (is_array($clienteId)) {
            $useMinMax = false;
            if (isset($clienteId['min'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_CLIENTE_ID, $clienteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clienteId['max'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_CLIENTE_ID, $clienteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_CLIENTE_ID, $clienteId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(EstoqueLancamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EstoqueLancamentoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Cliente object
     *
     * @param \ImaTelecomBundle\Model\Cliente|ObjectCollection $cliente The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByCliente($cliente, $comparison = null)
    {
        if ($cliente instanceof \ImaTelecomBundle\Model\Cliente) {
            return $this
                ->addUsingAlias(EstoqueLancamentoTableMap::COL_CLIENTE_ID, $cliente->getIdcliente(), $comparison);
        } elseif ($cliente instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EstoqueLancamentoTableMap::COL_CLIENTE_ID, $cliente->toKeyValue('PrimaryKey', 'Idcliente'), $comparison);
        } else {
            throw new PropelException('filterByCliente() only accepts arguments of type \ImaTelecomBundle\Model\Cliente or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Cliente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function joinCliente($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Cliente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Cliente');
        }

        return $this;
    }

    /**
     * Use the Cliente relation Cliente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ClienteQuery A secondary query class using the current class as primary query
     */
    public function useClienteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCliente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Cliente', '\ImaTelecomBundle\Model\ClienteQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(EstoqueLancamentoTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EstoqueLancamentoTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\EstoqueLancamentoItem object
     *
     * @param \ImaTelecomBundle\Model\EstoqueLancamentoItem|ObjectCollection $estoqueLancamentoItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function filterByEstoqueLancamentoItem($estoqueLancamentoItem, $comparison = null)
    {
        if ($estoqueLancamentoItem instanceof \ImaTelecomBundle\Model\EstoqueLancamentoItem) {
            return $this
                ->addUsingAlias(EstoqueLancamentoTableMap::COL_IDESTOQUE_LANCAMENTO, $estoqueLancamentoItem->getLancamentoId(), $comparison);
        } elseif ($estoqueLancamentoItem instanceof ObjectCollection) {
            return $this
                ->useEstoqueLancamentoItemQuery()
                ->filterByPrimaryKeys($estoqueLancamentoItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEstoqueLancamentoItem() only accepts arguments of type \ImaTelecomBundle\Model\EstoqueLancamentoItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EstoqueLancamentoItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function joinEstoqueLancamentoItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EstoqueLancamentoItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EstoqueLancamentoItem');
        }

        return $this;
    }

    /**
     * Use the EstoqueLancamentoItem relation EstoqueLancamentoItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\EstoqueLancamentoItemQuery A secondary query class using the current class as primary query
     */
    public function useEstoqueLancamentoItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstoqueLancamentoItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EstoqueLancamentoItem', '\ImaTelecomBundle\Model\EstoqueLancamentoItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEstoqueLancamento $estoqueLancamento Object to remove from the list of results
     *
     * @return $this|ChildEstoqueLancamentoQuery The current query, for fluid interface
     */
    public function prune($estoqueLancamento = null)
    {
        if ($estoqueLancamento) {
            $this->addUsingAlias(EstoqueLancamentoTableMap::COL_IDESTOQUE_LANCAMENTO, $estoqueLancamento->getIdestoqueLancamento(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the estoque_lancamento table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EstoqueLancamentoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EstoqueLancamentoTableMap::clearInstancePool();
            EstoqueLancamentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EstoqueLancamentoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EstoqueLancamentoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EstoqueLancamentoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EstoqueLancamentoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EstoqueLancamentoQuery
