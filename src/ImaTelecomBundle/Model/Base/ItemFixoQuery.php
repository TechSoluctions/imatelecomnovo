<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\ItemFixo as ChildItemFixo;
use ImaTelecomBundle\Model\ItemFixoQuery as ChildItemFixoQuery;
use ImaTelecomBundle\Model\Map\ItemFixoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'item_fixo' table.
 *
 *
 *
 * @method     ChildItemFixoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildItemFixoQuery orderByValorMinimo($order = Criteria::ASC) Order by the valor_minimo column
 * @method     ChildItemFixoQuery orderByValorMaximo($order = Criteria::ASC) Order by the valor_maximo column
 * @method     ChildItemFixoQuery orderByValorRetorno($order = Criteria::ASC) Order by the valor_retorno column
 * @method     ChildItemFixoQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildItemFixoQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildItemFixoQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildItemFixoQuery orderByTipo($order = Criteria::ASC) Order by the tipo column
 *
 * @method     ChildItemFixoQuery groupById() Group by the id column
 * @method     ChildItemFixoQuery groupByValorMinimo() Group by the valor_minimo column
 * @method     ChildItemFixoQuery groupByValorMaximo() Group by the valor_maximo column
 * @method     ChildItemFixoQuery groupByValorRetorno() Group by the valor_retorno column
 * @method     ChildItemFixoQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildItemFixoQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildItemFixoQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildItemFixoQuery groupByTipo() Group by the tipo column
 *
 * @method     ChildItemFixoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildItemFixoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildItemFixoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildItemFixoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildItemFixoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildItemFixoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildItemFixo findOne(ConnectionInterface $con = null) Return the first ChildItemFixo matching the query
 * @method     ChildItemFixo findOneOrCreate(ConnectionInterface $con = null) Return the first ChildItemFixo matching the query, or a new ChildItemFixo object populated from the query conditions when no match is found
 *
 * @method     ChildItemFixo findOneById(int $id) Return the first ChildItemFixo filtered by the id column
 * @method     ChildItemFixo findOneByValorMinimo(string $valor_minimo) Return the first ChildItemFixo filtered by the valor_minimo column
 * @method     ChildItemFixo findOneByValorMaximo(string $valor_maximo) Return the first ChildItemFixo filtered by the valor_maximo column
 * @method     ChildItemFixo findOneByValorRetorno(string $valor_retorno) Return the first ChildItemFixo filtered by the valor_retorno column
 * @method     ChildItemFixo findOneByDataCadastro(string $data_cadastro) Return the first ChildItemFixo filtered by the data_cadastro column
 * @method     ChildItemFixo findOneByDataAlterado(string $data_alterado) Return the first ChildItemFixo filtered by the data_alterado column
 * @method     ChildItemFixo findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildItemFixo filtered by the usuario_alterado column
 * @method     ChildItemFixo findOneByTipo(string $tipo) Return the first ChildItemFixo filtered by the tipo column *

 * @method     ChildItemFixo requirePk($key, ConnectionInterface $con = null) Return the ChildItemFixo by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemFixo requireOne(ConnectionInterface $con = null) Return the first ChildItemFixo matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildItemFixo requireOneById(int $id) Return the first ChildItemFixo filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemFixo requireOneByValorMinimo(string $valor_minimo) Return the first ChildItemFixo filtered by the valor_minimo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemFixo requireOneByValorMaximo(string $valor_maximo) Return the first ChildItemFixo filtered by the valor_maximo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemFixo requireOneByValorRetorno(string $valor_retorno) Return the first ChildItemFixo filtered by the valor_retorno column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemFixo requireOneByDataCadastro(string $data_cadastro) Return the first ChildItemFixo filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemFixo requireOneByDataAlterado(string $data_alterado) Return the first ChildItemFixo filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemFixo requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildItemFixo filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildItemFixo requireOneByTipo(string $tipo) Return the first ChildItemFixo filtered by the tipo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildItemFixo[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildItemFixo objects based on current ModelCriteria
 * @method     ChildItemFixo[]|ObjectCollection findById(int $id) Return ChildItemFixo objects filtered by the id column
 * @method     ChildItemFixo[]|ObjectCollection findByValorMinimo(string $valor_minimo) Return ChildItemFixo objects filtered by the valor_minimo column
 * @method     ChildItemFixo[]|ObjectCollection findByValorMaximo(string $valor_maximo) Return ChildItemFixo objects filtered by the valor_maximo column
 * @method     ChildItemFixo[]|ObjectCollection findByValorRetorno(string $valor_retorno) Return ChildItemFixo objects filtered by the valor_retorno column
 * @method     ChildItemFixo[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildItemFixo objects filtered by the data_cadastro column
 * @method     ChildItemFixo[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildItemFixo objects filtered by the data_alterado column
 * @method     ChildItemFixo[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildItemFixo objects filtered by the usuario_alterado column
 * @method     ChildItemFixo[]|ObjectCollection findByTipo(string $tipo) Return ChildItemFixo objects filtered by the tipo column
 * @method     ChildItemFixo[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ItemFixoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\ItemFixoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\ItemFixo', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildItemFixoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildItemFixoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildItemFixoQuery) {
            return $criteria;
        }
        $query = new ChildItemFixoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildItemFixo|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ItemFixoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ItemFixoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildItemFixo A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, valor_minimo, valor_maximo, valor_retorno, data_cadastro, data_alterado, usuario_alterado, tipo FROM item_fixo WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildItemFixo $obj */
            $obj = new ChildItemFixo();
            $obj->hydrate($row);
            ItemFixoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildItemFixo|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ItemFixoTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ItemFixoTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemFixoTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the valor_minimo column
     *
     * Example usage:
     * <code>
     * $query->filterByValorMinimo(1234); // WHERE valor_minimo = 1234
     * $query->filterByValorMinimo(array(12, 34)); // WHERE valor_minimo IN (12, 34)
     * $query->filterByValorMinimo(array('min' => 12)); // WHERE valor_minimo > 12
     * </code>
     *
     * @param     mixed $valorMinimo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterByValorMinimo($valorMinimo = null, $comparison = null)
    {
        if (is_array($valorMinimo)) {
            $useMinMax = false;
            if (isset($valorMinimo['min'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_VALOR_MINIMO, $valorMinimo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorMinimo['max'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_VALOR_MINIMO, $valorMinimo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemFixoTableMap::COL_VALOR_MINIMO, $valorMinimo, $comparison);
    }

    /**
     * Filter the query on the valor_maximo column
     *
     * Example usage:
     * <code>
     * $query->filterByValorMaximo(1234); // WHERE valor_maximo = 1234
     * $query->filterByValorMaximo(array(12, 34)); // WHERE valor_maximo IN (12, 34)
     * $query->filterByValorMaximo(array('min' => 12)); // WHERE valor_maximo > 12
     * </code>
     *
     * @param     mixed $valorMaximo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterByValorMaximo($valorMaximo = null, $comparison = null)
    {
        if (is_array($valorMaximo)) {
            $useMinMax = false;
            if (isset($valorMaximo['min'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_VALOR_MAXIMO, $valorMaximo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorMaximo['max'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_VALOR_MAXIMO, $valorMaximo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemFixoTableMap::COL_VALOR_MAXIMO, $valorMaximo, $comparison);
    }

    /**
     * Filter the query on the valor_retorno column
     *
     * Example usage:
     * <code>
     * $query->filterByValorRetorno(1234); // WHERE valor_retorno = 1234
     * $query->filterByValorRetorno(array(12, 34)); // WHERE valor_retorno IN (12, 34)
     * $query->filterByValorRetorno(array('min' => 12)); // WHERE valor_retorno > 12
     * </code>
     *
     * @param     mixed $valorRetorno The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterByValorRetorno($valorRetorno = null, $comparison = null)
    {
        if (is_array($valorRetorno)) {
            $useMinMax = false;
            if (isset($valorRetorno['min'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_VALOR_RETORNO, $valorRetorno['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valorRetorno['max'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_VALOR_RETORNO, $valorRetorno['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemFixoTableMap::COL_VALOR_RETORNO, $valorRetorno, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemFixoTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemFixoTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(ItemFixoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemFixoTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the tipo column
     *
     * Example usage:
     * <code>
     * $query->filterByTipo('fooValue');   // WHERE tipo = 'fooValue'
     * $query->filterByTipo('%fooValue%', Criteria::LIKE); // WHERE tipo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function filterByTipo($tipo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ItemFixoTableMap::COL_TIPO, $tipo, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildItemFixo $itemFixo Object to remove from the list of results
     *
     * @return $this|ChildItemFixoQuery The current query, for fluid interface
     */
    public function prune($itemFixo = null)
    {
        if ($itemFixo) {
            $this->addUsingAlias(ItemFixoTableMap::COL_ID, $itemFixo->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the item_fixo table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ItemFixoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ItemFixoTableMap::clearInstancePool();
            ItemFixoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ItemFixoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ItemFixoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ItemFixoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ItemFixoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ItemFixoQuery
