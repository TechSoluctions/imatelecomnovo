<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\DadosFiscal as ChildDadosFiscal;
use ImaTelecomBundle\Model\DadosFiscalQuery as ChildDadosFiscalQuery;
use ImaTelecomBundle\Model\Map\DadosFiscalTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'dados_fiscal' table.
 *
 *
 *
 * @method     ChildDadosFiscalQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildDadosFiscalQuery orderByBoletoItemId($order = Criteria::ASC) Order by the boleto_item_id column
 * @method     ChildDadosFiscalQuery orderByDocumento($order = Criteria::ASC) Order by the documento column
 * @method     ChildDadosFiscalQuery orderByItem($order = Criteria::ASC) Order by the item column
 * @method     ChildDadosFiscalQuery orderByMestre($order = Criteria::ASC) Order by the mestre column
 * @method     ChildDadosFiscalQuery orderByNumeroNotaFiscal($order = Criteria::ASC) Order by the numero_nota_fiscal column
 * @method     ChildDadosFiscalQuery orderByCompetenciaId($order = Criteria::ASC) Order by the competencia_id column
 * @method     ChildDadosFiscalQuery orderByDataEmissao($order = Criteria::ASC) Order by the data_emissao column
 * @method     ChildDadosFiscalQuery orderByMestreChaveDigitalMeio($order = Criteria::ASC) Order by the mestre_chave_digital_meio column
 * @method     ChildDadosFiscalQuery orderByCfop($order = Criteria::ASC) Order by the cfop column
 * @method     ChildDadosFiscalQuery orderByBaseCalculo($order = Criteria::ASC) Order by the base_calculo column
 * @method     ChildDadosFiscalQuery orderByValorIcms($order = Criteria::ASC) Order by the valor_icms column
 * @method     ChildDadosFiscalQuery orderByNTributo($order = Criteria::ASC) Order by the n_tributo column
 * @method     ChildDadosFiscalQuery orderByValorNota($order = Criteria::ASC) Order by the valor_nota column
 * @method     ChildDadosFiscalQuery orderBySituacao($order = Criteria::ASC) Order by the situacao column
 * @method     ChildDadosFiscalQuery orderByServicoClienteImportId($order = Criteria::ASC) Order by the servico_cliente_import_id column
 * @method     ChildDadosFiscalQuery orderByCodNotaFiscalLotePrevio($order = Criteria::ASC) Order by the cod_nota_fiscal_lote_previo column
 * @method     ChildDadosFiscalQuery orderByPessoaImportId($order = Criteria::ASC) Order by the pessoa_import_id column
 * @method     ChildDadosFiscalQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildDadosFiscalQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildDadosFiscalQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 * @method     ChildDadosFiscalQuery orderByBoletoId($order = Criteria::ASC) Order by the boleto_id column
 * @method     ChildDadosFiscalQuery orderByPosicaoPrimeiroItem($order = Criteria::ASC) Order by the posicao_primeiro_item column
 * @method     ChildDadosFiscalQuery orderByPosicaoUltimoItem($order = Criteria::ASC) Order by the posicao_ultimo_item column
 * @method     ChildDadosFiscalQuery orderByUltimaNotaGerada($order = Criteria::ASC) Order by the ultima_nota_gerada column
 * @method     ChildDadosFiscalQuery orderByTipoNota($order = Criteria::ASC) Order by the tipo_nota column
 * @method     ChildDadosFiscalQuery orderByProtocolo($order = Criteria::ASC) Order by the protocolo column
 * @method     ChildDadosFiscalQuery orderByDataEmitido($order = Criteria::ASC) Order by the data_emitido column
 * @method     ChildDadosFiscalQuery orderByNumeroRps($order = Criteria::ASC) Order by the numero_rps column
 *
 * @method     ChildDadosFiscalQuery groupById() Group by the id column
 * @method     ChildDadosFiscalQuery groupByBoletoItemId() Group by the boleto_item_id column
 * @method     ChildDadosFiscalQuery groupByDocumento() Group by the documento column
 * @method     ChildDadosFiscalQuery groupByItem() Group by the item column
 * @method     ChildDadosFiscalQuery groupByMestre() Group by the mestre column
 * @method     ChildDadosFiscalQuery groupByNumeroNotaFiscal() Group by the numero_nota_fiscal column
 * @method     ChildDadosFiscalQuery groupByCompetenciaId() Group by the competencia_id column
 * @method     ChildDadosFiscalQuery groupByDataEmissao() Group by the data_emissao column
 * @method     ChildDadosFiscalQuery groupByMestreChaveDigitalMeio() Group by the mestre_chave_digital_meio column
 * @method     ChildDadosFiscalQuery groupByCfop() Group by the cfop column
 * @method     ChildDadosFiscalQuery groupByBaseCalculo() Group by the base_calculo column
 * @method     ChildDadosFiscalQuery groupByValorIcms() Group by the valor_icms column
 * @method     ChildDadosFiscalQuery groupByNTributo() Group by the n_tributo column
 * @method     ChildDadosFiscalQuery groupByValorNota() Group by the valor_nota column
 * @method     ChildDadosFiscalQuery groupBySituacao() Group by the situacao column
 * @method     ChildDadosFiscalQuery groupByServicoClienteImportId() Group by the servico_cliente_import_id column
 * @method     ChildDadosFiscalQuery groupByCodNotaFiscalLotePrevio() Group by the cod_nota_fiscal_lote_previo column
 * @method     ChildDadosFiscalQuery groupByPessoaImportId() Group by the pessoa_import_id column
 * @method     ChildDadosFiscalQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildDadosFiscalQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildDadosFiscalQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 * @method     ChildDadosFiscalQuery groupByBoletoId() Group by the boleto_id column
 * @method     ChildDadosFiscalQuery groupByPosicaoPrimeiroItem() Group by the posicao_primeiro_item column
 * @method     ChildDadosFiscalQuery groupByPosicaoUltimoItem() Group by the posicao_ultimo_item column
 * @method     ChildDadosFiscalQuery groupByUltimaNotaGerada() Group by the ultima_nota_gerada column
 * @method     ChildDadosFiscalQuery groupByTipoNota() Group by the tipo_nota column
 * @method     ChildDadosFiscalQuery groupByProtocolo() Group by the protocolo column
 * @method     ChildDadosFiscalQuery groupByDataEmitido() Group by the data_emitido column
 * @method     ChildDadosFiscalQuery groupByNumeroRps() Group by the numero_rps column
 *
 * @method     ChildDadosFiscalQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDadosFiscalQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDadosFiscalQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDadosFiscalQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDadosFiscalQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDadosFiscalQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDadosFiscalQuery leftJoinBoleto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Boleto relation
 * @method     ChildDadosFiscalQuery rightJoinBoleto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Boleto relation
 * @method     ChildDadosFiscalQuery innerJoinBoleto($relationAlias = null) Adds a INNER JOIN clause to the query using the Boleto relation
 *
 * @method     ChildDadosFiscalQuery joinWithBoleto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Boleto relation
 *
 * @method     ChildDadosFiscalQuery leftJoinWithBoleto() Adds a LEFT JOIN clause and with to the query using the Boleto relation
 * @method     ChildDadosFiscalQuery rightJoinWithBoleto() Adds a RIGHT JOIN clause and with to the query using the Boleto relation
 * @method     ChildDadosFiscalQuery innerJoinWithBoleto() Adds a INNER JOIN clause and with to the query using the Boleto relation
 *
 * @method     ChildDadosFiscalQuery leftJoinCompetencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the Competencia relation
 * @method     ChildDadosFiscalQuery rightJoinCompetencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Competencia relation
 * @method     ChildDadosFiscalQuery innerJoinCompetencia($relationAlias = null) Adds a INNER JOIN clause to the query using the Competencia relation
 *
 * @method     ChildDadosFiscalQuery joinWithCompetencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Competencia relation
 *
 * @method     ChildDadosFiscalQuery leftJoinWithCompetencia() Adds a LEFT JOIN clause and with to the query using the Competencia relation
 * @method     ChildDadosFiscalQuery rightJoinWithCompetencia() Adds a RIGHT JOIN clause and with to the query using the Competencia relation
 * @method     ChildDadosFiscalQuery innerJoinWithCompetencia() Adds a INNER JOIN clause and with to the query using the Competencia relation
 *
 * @method     ChildDadosFiscalQuery leftJoinPessoa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pessoa relation
 * @method     ChildDadosFiscalQuery rightJoinPessoa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pessoa relation
 * @method     ChildDadosFiscalQuery innerJoinPessoa($relationAlias = null) Adds a INNER JOIN clause to the query using the Pessoa relation
 *
 * @method     ChildDadosFiscalQuery joinWithPessoa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pessoa relation
 *
 * @method     ChildDadosFiscalQuery leftJoinWithPessoa() Adds a LEFT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildDadosFiscalQuery rightJoinWithPessoa() Adds a RIGHT JOIN clause and with to the query using the Pessoa relation
 * @method     ChildDadosFiscalQuery innerJoinWithPessoa() Adds a INNER JOIN clause and with to the query using the Pessoa relation
 *
 * @method     ChildDadosFiscalQuery leftJoinDadosFiscalItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the DadosFiscalItem relation
 * @method     ChildDadosFiscalQuery rightJoinDadosFiscalItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DadosFiscalItem relation
 * @method     ChildDadosFiscalQuery innerJoinDadosFiscalItem($relationAlias = null) Adds a INNER JOIN clause to the query using the DadosFiscalItem relation
 *
 * @method     ChildDadosFiscalQuery joinWithDadosFiscalItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DadosFiscalItem relation
 *
 * @method     ChildDadosFiscalQuery leftJoinWithDadosFiscalItem() Adds a LEFT JOIN clause and with to the query using the DadosFiscalItem relation
 * @method     ChildDadosFiscalQuery rightJoinWithDadosFiscalItem() Adds a RIGHT JOIN clause and with to the query using the DadosFiscalItem relation
 * @method     ChildDadosFiscalQuery innerJoinWithDadosFiscalItem() Adds a INNER JOIN clause and with to the query using the DadosFiscalItem relation
 *
 * @method     \ImaTelecomBundle\Model\BoletoQuery|\ImaTelecomBundle\Model\CompetenciaQuery|\ImaTelecomBundle\Model\PessoaQuery|\ImaTelecomBundle\Model\DadosFiscalItemQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildDadosFiscal findOne(ConnectionInterface $con = null) Return the first ChildDadosFiscal matching the query
 * @method     ChildDadosFiscal findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDadosFiscal matching the query, or a new ChildDadosFiscal object populated from the query conditions when no match is found
 *
 * @method     ChildDadosFiscal findOneById(int $id) Return the first ChildDadosFiscal filtered by the id column
 * @method     ChildDadosFiscal findOneByBoletoItemId(int $boleto_item_id) Return the first ChildDadosFiscal filtered by the boleto_item_id column
 * @method     ChildDadosFiscal findOneByDocumento(string $documento) Return the first ChildDadosFiscal filtered by the documento column
 * @method     ChildDadosFiscal findOneByItem(string $item) Return the first ChildDadosFiscal filtered by the item column
 * @method     ChildDadosFiscal findOneByMestre(string $mestre) Return the first ChildDadosFiscal filtered by the mestre column
 * @method     ChildDadosFiscal findOneByNumeroNotaFiscal(int $numero_nota_fiscal) Return the first ChildDadosFiscal filtered by the numero_nota_fiscal column
 * @method     ChildDadosFiscal findOneByCompetenciaId(int $competencia_id) Return the first ChildDadosFiscal filtered by the competencia_id column
 * @method     ChildDadosFiscal findOneByDataEmissao(string $data_emissao) Return the first ChildDadosFiscal filtered by the data_emissao column
 * @method     ChildDadosFiscal findOneByMestreChaveDigitalMeio(string $mestre_chave_digital_meio) Return the first ChildDadosFiscal filtered by the mestre_chave_digital_meio column
 * @method     ChildDadosFiscal findOneByCfop(string $cfop) Return the first ChildDadosFiscal filtered by the cfop column
 * @method     ChildDadosFiscal findOneByBaseCalculo(string $base_calculo) Return the first ChildDadosFiscal filtered by the base_calculo column
 * @method     ChildDadosFiscal findOneByValorIcms(string $valor_icms) Return the first ChildDadosFiscal filtered by the valor_icms column
 * @method     ChildDadosFiscal findOneByNTributo(string $n_tributo) Return the first ChildDadosFiscal filtered by the n_tributo column
 * @method     ChildDadosFiscal findOneByValorNota(string $valor_nota) Return the first ChildDadosFiscal filtered by the valor_nota column
 * @method     ChildDadosFiscal findOneBySituacao(string $situacao) Return the first ChildDadosFiscal filtered by the situacao column
 * @method     ChildDadosFiscal findOneByServicoClienteImportId(int $servico_cliente_import_id) Return the first ChildDadosFiscal filtered by the servico_cliente_import_id column
 * @method     ChildDadosFiscal findOneByCodNotaFiscalLotePrevio(int $cod_nota_fiscal_lote_previo) Return the first ChildDadosFiscal filtered by the cod_nota_fiscal_lote_previo column
 * @method     ChildDadosFiscal findOneByPessoaImportId(int $pessoa_import_id) Return the first ChildDadosFiscal filtered by the pessoa_import_id column
 * @method     ChildDadosFiscal findOneByDataCadastro(string $data_cadastro) Return the first ChildDadosFiscal filtered by the data_cadastro column
 * @method     ChildDadosFiscal findOneByDataAlterado(string $data_alterado) Return the first ChildDadosFiscal filtered by the data_alterado column
 * @method     ChildDadosFiscal findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildDadosFiscal filtered by the usuario_alterado column
 * @method     ChildDadosFiscal findOneByBoletoId(int $boleto_id) Return the first ChildDadosFiscal filtered by the boleto_id column
 * @method     ChildDadosFiscal findOneByPosicaoPrimeiroItem(int $posicao_primeiro_item) Return the first ChildDadosFiscal filtered by the posicao_primeiro_item column
 * @method     ChildDadosFiscal findOneByPosicaoUltimoItem(int $posicao_ultimo_item) Return the first ChildDadosFiscal filtered by the posicao_ultimo_item column
 * @method     ChildDadosFiscal findOneByUltimaNotaGerada(boolean $ultima_nota_gerada) Return the first ChildDadosFiscal filtered by the ultima_nota_gerada column
 * @method     ChildDadosFiscal findOneByTipoNota(string $tipo_nota) Return the first ChildDadosFiscal filtered by the tipo_nota column
 * @method     ChildDadosFiscal findOneByProtocolo(int $protocolo) Return the first ChildDadosFiscal filtered by the protocolo column
 * @method     ChildDadosFiscal findOneByDataEmitido(string $data_emitido) Return the first ChildDadosFiscal filtered by the data_emitido column
 * @method     ChildDadosFiscal findOneByNumeroRps(int $numero_rps) Return the first ChildDadosFiscal filtered by the numero_rps column *

 * @method     ChildDadosFiscal requirePk($key, ConnectionInterface $con = null) Return the ChildDadosFiscal by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOne(ConnectionInterface $con = null) Return the first ChildDadosFiscal matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDadosFiscal requireOneById(int $id) Return the first ChildDadosFiscal filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByBoletoItemId(int $boleto_item_id) Return the first ChildDadosFiscal filtered by the boleto_item_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByDocumento(string $documento) Return the first ChildDadosFiscal filtered by the documento column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByItem(string $item) Return the first ChildDadosFiscal filtered by the item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByMestre(string $mestre) Return the first ChildDadosFiscal filtered by the mestre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByNumeroNotaFiscal(int $numero_nota_fiscal) Return the first ChildDadosFiscal filtered by the numero_nota_fiscal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByCompetenciaId(int $competencia_id) Return the first ChildDadosFiscal filtered by the competencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByDataEmissao(string $data_emissao) Return the first ChildDadosFiscal filtered by the data_emissao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByMestreChaveDigitalMeio(string $mestre_chave_digital_meio) Return the first ChildDadosFiscal filtered by the mestre_chave_digital_meio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByCfop(string $cfop) Return the first ChildDadosFiscal filtered by the cfop column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByBaseCalculo(string $base_calculo) Return the first ChildDadosFiscal filtered by the base_calculo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByValorIcms(string $valor_icms) Return the first ChildDadosFiscal filtered by the valor_icms column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByNTributo(string $n_tributo) Return the first ChildDadosFiscal filtered by the n_tributo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByValorNota(string $valor_nota) Return the first ChildDadosFiscal filtered by the valor_nota column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneBySituacao(string $situacao) Return the first ChildDadosFiscal filtered by the situacao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByServicoClienteImportId(int $servico_cliente_import_id) Return the first ChildDadosFiscal filtered by the servico_cliente_import_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByCodNotaFiscalLotePrevio(int $cod_nota_fiscal_lote_previo) Return the first ChildDadosFiscal filtered by the cod_nota_fiscal_lote_previo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByPessoaImportId(int $pessoa_import_id) Return the first ChildDadosFiscal filtered by the pessoa_import_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByDataCadastro(string $data_cadastro) Return the first ChildDadosFiscal filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByDataAlterado(string $data_alterado) Return the first ChildDadosFiscal filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildDadosFiscal filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByBoletoId(int $boleto_id) Return the first ChildDadosFiscal filtered by the boleto_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByPosicaoPrimeiroItem(int $posicao_primeiro_item) Return the first ChildDadosFiscal filtered by the posicao_primeiro_item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByPosicaoUltimoItem(int $posicao_ultimo_item) Return the first ChildDadosFiscal filtered by the posicao_ultimo_item column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByUltimaNotaGerada(boolean $ultima_nota_gerada) Return the first ChildDadosFiscal filtered by the ultima_nota_gerada column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByTipoNota(string $tipo_nota) Return the first ChildDadosFiscal filtered by the tipo_nota column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByProtocolo(int $protocolo) Return the first ChildDadosFiscal filtered by the protocolo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByDataEmitido(string $data_emitido) Return the first ChildDadosFiscal filtered by the data_emitido column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDadosFiscal requireOneByNumeroRps(int $numero_rps) Return the first ChildDadosFiscal filtered by the numero_rps column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDadosFiscal[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDadosFiscal objects based on current ModelCriteria
 * @method     ChildDadosFiscal[]|ObjectCollection findById(int $id) Return ChildDadosFiscal objects filtered by the id column
 * @method     ChildDadosFiscal[]|ObjectCollection findByBoletoItemId(int $boleto_item_id) Return ChildDadosFiscal objects filtered by the boleto_item_id column
 * @method     ChildDadosFiscal[]|ObjectCollection findByDocumento(string $documento) Return ChildDadosFiscal objects filtered by the documento column
 * @method     ChildDadosFiscal[]|ObjectCollection findByItem(string $item) Return ChildDadosFiscal objects filtered by the item column
 * @method     ChildDadosFiscal[]|ObjectCollection findByMestre(string $mestre) Return ChildDadosFiscal objects filtered by the mestre column
 * @method     ChildDadosFiscal[]|ObjectCollection findByNumeroNotaFiscal(int $numero_nota_fiscal) Return ChildDadosFiscal objects filtered by the numero_nota_fiscal column
 * @method     ChildDadosFiscal[]|ObjectCollection findByCompetenciaId(int $competencia_id) Return ChildDadosFiscal objects filtered by the competencia_id column
 * @method     ChildDadosFiscal[]|ObjectCollection findByDataEmissao(string $data_emissao) Return ChildDadosFiscal objects filtered by the data_emissao column
 * @method     ChildDadosFiscal[]|ObjectCollection findByMestreChaveDigitalMeio(string $mestre_chave_digital_meio) Return ChildDadosFiscal objects filtered by the mestre_chave_digital_meio column
 * @method     ChildDadosFiscal[]|ObjectCollection findByCfop(string $cfop) Return ChildDadosFiscal objects filtered by the cfop column
 * @method     ChildDadosFiscal[]|ObjectCollection findByBaseCalculo(string $base_calculo) Return ChildDadosFiscal objects filtered by the base_calculo column
 * @method     ChildDadosFiscal[]|ObjectCollection findByValorIcms(string $valor_icms) Return ChildDadosFiscal objects filtered by the valor_icms column
 * @method     ChildDadosFiscal[]|ObjectCollection findByNTributo(string $n_tributo) Return ChildDadosFiscal objects filtered by the n_tributo column
 * @method     ChildDadosFiscal[]|ObjectCollection findByValorNota(string $valor_nota) Return ChildDadosFiscal objects filtered by the valor_nota column
 * @method     ChildDadosFiscal[]|ObjectCollection findBySituacao(string $situacao) Return ChildDadosFiscal objects filtered by the situacao column
 * @method     ChildDadosFiscal[]|ObjectCollection findByServicoClienteImportId(int $servico_cliente_import_id) Return ChildDadosFiscal objects filtered by the servico_cliente_import_id column
 * @method     ChildDadosFiscal[]|ObjectCollection findByCodNotaFiscalLotePrevio(int $cod_nota_fiscal_lote_previo) Return ChildDadosFiscal objects filtered by the cod_nota_fiscal_lote_previo column
 * @method     ChildDadosFiscal[]|ObjectCollection findByPessoaImportId(int $pessoa_import_id) Return ChildDadosFiscal objects filtered by the pessoa_import_id column
 * @method     ChildDadosFiscal[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildDadosFiscal objects filtered by the data_cadastro column
 * @method     ChildDadosFiscal[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildDadosFiscal objects filtered by the data_alterado column
 * @method     ChildDadosFiscal[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildDadosFiscal objects filtered by the usuario_alterado column
 * @method     ChildDadosFiscal[]|ObjectCollection findByBoletoId(int $boleto_id) Return ChildDadosFiscal objects filtered by the boleto_id column
 * @method     ChildDadosFiscal[]|ObjectCollection findByPosicaoPrimeiroItem(int $posicao_primeiro_item) Return ChildDadosFiscal objects filtered by the posicao_primeiro_item column
 * @method     ChildDadosFiscal[]|ObjectCollection findByPosicaoUltimoItem(int $posicao_ultimo_item) Return ChildDadosFiscal objects filtered by the posicao_ultimo_item column
 * @method     ChildDadosFiscal[]|ObjectCollection findByUltimaNotaGerada(boolean $ultima_nota_gerada) Return ChildDadosFiscal objects filtered by the ultima_nota_gerada column
 * @method     ChildDadosFiscal[]|ObjectCollection findByTipoNota(string $tipo_nota) Return ChildDadosFiscal objects filtered by the tipo_nota column
 * @method     ChildDadosFiscal[]|ObjectCollection findByProtocolo(int $protocolo) Return ChildDadosFiscal objects filtered by the protocolo column
 * @method     ChildDadosFiscal[]|ObjectCollection findByDataEmitido(string $data_emitido) Return ChildDadosFiscal objects filtered by the data_emitido column
 * @method     ChildDadosFiscal[]|ObjectCollection findByNumeroRps(int $numero_rps) Return ChildDadosFiscal objects filtered by the numero_rps column
 * @method     ChildDadosFiscal[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DadosFiscalQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\DadosFiscalQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\DadosFiscal', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDadosFiscalQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDadosFiscalQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDadosFiscalQuery) {
            return $criteria;
        }
        $query = new ChildDadosFiscalQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDadosFiscal|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DadosFiscalTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = DadosFiscalTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscal A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, boleto_item_id, documento, item, mestre, numero_nota_fiscal, competencia_id, data_emissao, mestre_chave_digital_meio, cfop, base_calculo, valor_icms, n_tributo, valor_nota, situacao, servico_cliente_import_id, cod_nota_fiscal_lote_previo, pessoa_import_id, data_cadastro, data_alterado, usuario_alterado, boleto_id, posicao_primeiro_item, posicao_ultimo_item, ultima_nota_gerada, tipo_nota, protocolo, data_emitido, numero_rps FROM dados_fiscal WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDadosFiscal $obj */
            $obj = new ChildDadosFiscal();
            $obj->hydrate($row);
            DadosFiscalTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDadosFiscal|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DadosFiscalTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DadosFiscalTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the boleto_item_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBoletoItemId(1234); // WHERE boleto_item_id = 1234
     * $query->filterByBoletoItemId(array(12, 34)); // WHERE boleto_item_id IN (12, 34)
     * $query->filterByBoletoItemId(array('min' => 12)); // WHERE boleto_item_id > 12
     * </code>
     *
     * @param     mixed $boletoItemId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByBoletoItemId($boletoItemId = null, $comparison = null)
    {
        if (is_array($boletoItemId)) {
            $useMinMax = false;
            if (isset($boletoItemId['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_BOLETO_ITEM_ID, $boletoItemId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($boletoItemId['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_BOLETO_ITEM_ID, $boletoItemId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_BOLETO_ITEM_ID, $boletoItemId, $comparison);
    }

    /**
     * Filter the query on the documento column
     *
     * Example usage:
     * <code>
     * $query->filterByDocumento('fooValue');   // WHERE documento = 'fooValue'
     * $query->filterByDocumento('%fooValue%', Criteria::LIKE); // WHERE documento LIKE '%fooValue%'
     * </code>
     *
     * @param     string $documento The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByDocumento($documento = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($documento)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_DOCUMENTO, $documento, $comparison);
    }

    /**
     * Filter the query on the item column
     *
     * Example usage:
     * <code>
     * $query->filterByItem('fooValue');   // WHERE item = 'fooValue'
     * $query->filterByItem('%fooValue%', Criteria::LIKE); // WHERE item LIKE '%fooValue%'
     * </code>
     *
     * @param     string $item The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByItem($item = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($item)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_ITEM, $item, $comparison);
    }

    /**
     * Filter the query on the mestre column
     *
     * Example usage:
     * <code>
     * $query->filterByMestre('fooValue');   // WHERE mestre = 'fooValue'
     * $query->filterByMestre('%fooValue%', Criteria::LIKE); // WHERE mestre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mestre The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByMestre($mestre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mestre)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_MESTRE, $mestre, $comparison);
    }

    /**
     * Filter the query on the numero_nota_fiscal column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroNotaFiscal(1234); // WHERE numero_nota_fiscal = 1234
     * $query->filterByNumeroNotaFiscal(array(12, 34)); // WHERE numero_nota_fiscal IN (12, 34)
     * $query->filterByNumeroNotaFiscal(array('min' => 12)); // WHERE numero_nota_fiscal > 12
     * </code>
     *
     * @param     mixed $numeroNotaFiscal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByNumeroNotaFiscal($numeroNotaFiscal = null, $comparison = null)
    {
        if (is_array($numeroNotaFiscal)) {
            $useMinMax = false;
            if (isset($numeroNotaFiscal['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL, $numeroNotaFiscal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numeroNotaFiscal['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL, $numeroNotaFiscal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL, $numeroNotaFiscal, $comparison);
    }

    /**
     * Filter the query on the competencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompetenciaId(1234); // WHERE competencia_id = 1234
     * $query->filterByCompetenciaId(array(12, 34)); // WHERE competencia_id IN (12, 34)
     * $query->filterByCompetenciaId(array('min' => 12)); // WHERE competencia_id > 12
     * </code>
     *
     * @see       filterByCompetencia()
     *
     * @param     mixed $competenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByCompetenciaId($competenciaId = null, $comparison = null)
    {
        if (is_array($competenciaId)) {
            $useMinMax = false;
            if (isset($competenciaId['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_COMPETENCIA_ID, $competenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($competenciaId['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_COMPETENCIA_ID, $competenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_COMPETENCIA_ID, $competenciaId, $comparison);
    }

    /**
     * Filter the query on the data_emissao column
     *
     * Example usage:
     * <code>
     * $query->filterByDataEmissao('2011-03-14'); // WHERE data_emissao = '2011-03-14'
     * $query->filterByDataEmissao('now'); // WHERE data_emissao = '2011-03-14'
     * $query->filterByDataEmissao(array('max' => 'yesterday')); // WHERE data_emissao > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataEmissao The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByDataEmissao($dataEmissao = null, $comparison = null)
    {
        if (is_array($dataEmissao)) {
            $useMinMax = false;
            if (isset($dataEmissao['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_EMISSAO, $dataEmissao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataEmissao['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_EMISSAO, $dataEmissao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_EMISSAO, $dataEmissao, $comparison);
    }

    /**
     * Filter the query on the mestre_chave_digital_meio column
     *
     * Example usage:
     * <code>
     * $query->filterByMestreChaveDigitalMeio('fooValue');   // WHERE mestre_chave_digital_meio = 'fooValue'
     * $query->filterByMestreChaveDigitalMeio('%fooValue%', Criteria::LIKE); // WHERE mestre_chave_digital_meio LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mestreChaveDigitalMeio The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByMestreChaveDigitalMeio($mestreChaveDigitalMeio = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mestreChaveDigitalMeio)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_MESTRE_CHAVE_DIGITAL_MEIO, $mestreChaveDigitalMeio, $comparison);
    }

    /**
     * Filter the query on the cfop column
     *
     * Example usage:
     * <code>
     * $query->filterByCfop('fooValue');   // WHERE cfop = 'fooValue'
     * $query->filterByCfop('%fooValue%', Criteria::LIKE); // WHERE cfop LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cfop The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByCfop($cfop = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cfop)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_CFOP, $cfop, $comparison);
    }

    /**
     * Filter the query on the base_calculo column
     *
     * Example usage:
     * <code>
     * $query->filterByBaseCalculo('fooValue');   // WHERE base_calculo = 'fooValue'
     * $query->filterByBaseCalculo('%fooValue%', Criteria::LIKE); // WHERE base_calculo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $baseCalculo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByBaseCalculo($baseCalculo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($baseCalculo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_BASE_CALCULO, $baseCalculo, $comparison);
    }

    /**
     * Filter the query on the valor_icms column
     *
     * Example usage:
     * <code>
     * $query->filterByValorIcms('fooValue');   // WHERE valor_icms = 'fooValue'
     * $query->filterByValorIcms('%fooValue%', Criteria::LIKE); // WHERE valor_icms LIKE '%fooValue%'
     * </code>
     *
     * @param     string $valorIcms The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByValorIcms($valorIcms = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($valorIcms)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_VALOR_ICMS, $valorIcms, $comparison);
    }

    /**
     * Filter the query on the n_tributo column
     *
     * Example usage:
     * <code>
     * $query->filterByNTributo('fooValue');   // WHERE n_tributo = 'fooValue'
     * $query->filterByNTributo('%fooValue%', Criteria::LIKE); // WHERE n_tributo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nTributo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByNTributo($nTributo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nTributo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_N_TRIBUTO, $nTributo, $comparison);
    }

    /**
     * Filter the query on the valor_nota column
     *
     * Example usage:
     * <code>
     * $query->filterByValorNota('fooValue');   // WHERE valor_nota = 'fooValue'
     * $query->filterByValorNota('%fooValue%', Criteria::LIKE); // WHERE valor_nota LIKE '%fooValue%'
     * </code>
     *
     * @param     string $valorNota The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByValorNota($valorNota = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($valorNota)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_VALOR_NOTA, $valorNota, $comparison);
    }

    /**
     * Filter the query on the situacao column
     *
     * Example usage:
     * <code>
     * $query->filterBySituacao('fooValue');   // WHERE situacao = 'fooValue'
     * $query->filterBySituacao('%fooValue%', Criteria::LIKE); // WHERE situacao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $situacao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterBySituacao($situacao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($situacao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_SITUACAO, $situacao, $comparison);
    }

    /**
     * Filter the query on the servico_cliente_import_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServicoClienteImportId(1234); // WHERE servico_cliente_import_id = 1234
     * $query->filterByServicoClienteImportId(array(12, 34)); // WHERE servico_cliente_import_id IN (12, 34)
     * $query->filterByServicoClienteImportId(array('min' => 12)); // WHERE servico_cliente_import_id > 12
     * </code>
     *
     * @param     mixed $servicoClienteImportId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByServicoClienteImportId($servicoClienteImportId = null, $comparison = null)
    {
        if (is_array($servicoClienteImportId)) {
            $useMinMax = false;
            if (isset($servicoClienteImportId['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID, $servicoClienteImportId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($servicoClienteImportId['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID, $servicoClienteImportId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID, $servicoClienteImportId, $comparison);
    }

    /**
     * Filter the query on the cod_nota_fiscal_lote_previo column
     *
     * Example usage:
     * <code>
     * $query->filterByCodNotaFiscalLotePrevio(1234); // WHERE cod_nota_fiscal_lote_previo = 1234
     * $query->filterByCodNotaFiscalLotePrevio(array(12, 34)); // WHERE cod_nota_fiscal_lote_previo IN (12, 34)
     * $query->filterByCodNotaFiscalLotePrevio(array('min' => 12)); // WHERE cod_nota_fiscal_lote_previo > 12
     * </code>
     *
     * @param     mixed $codNotaFiscalLotePrevio The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByCodNotaFiscalLotePrevio($codNotaFiscalLotePrevio = null, $comparison = null)
    {
        if (is_array($codNotaFiscalLotePrevio)) {
            $useMinMax = false;
            if (isset($codNotaFiscalLotePrevio['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO, $codNotaFiscalLotePrevio['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($codNotaFiscalLotePrevio['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO, $codNotaFiscalLotePrevio['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO, $codNotaFiscalLotePrevio, $comparison);
    }

    /**
     * Filter the query on the pessoa_import_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPessoaImportId(1234); // WHERE pessoa_import_id = 1234
     * $query->filterByPessoaImportId(array(12, 34)); // WHERE pessoa_import_id IN (12, 34)
     * $query->filterByPessoaImportId(array('min' => 12)); // WHERE pessoa_import_id > 12
     * </code>
     *
     * @see       filterByPessoa()
     *
     * @param     mixed $pessoaImportId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByPessoaImportId($pessoaImportId = null, $comparison = null)
    {
        if (is_array($pessoaImportId)) {
            $useMinMax = false;
            if (isset($pessoaImportId['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_PESSOA_IMPORT_ID, $pessoaImportId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pessoaImportId['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_PESSOA_IMPORT_ID, $pessoaImportId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_PESSOA_IMPORT_ID, $pessoaImportId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query on the boleto_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBoletoId(1234); // WHERE boleto_id = 1234
     * $query->filterByBoletoId(array(12, 34)); // WHERE boleto_id IN (12, 34)
     * $query->filterByBoletoId(array('min' => 12)); // WHERE boleto_id > 12
     * </code>
     *
     * @see       filterByBoleto()
     *
     * @param     mixed $boletoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByBoletoId($boletoId = null, $comparison = null)
    {
        if (is_array($boletoId)) {
            $useMinMax = false;
            if (isset($boletoId['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_BOLETO_ID, $boletoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($boletoId['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_BOLETO_ID, $boletoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_BOLETO_ID, $boletoId, $comparison);
    }

    /**
     * Filter the query on the posicao_primeiro_item column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoPrimeiroItem(1234); // WHERE posicao_primeiro_item = 1234
     * $query->filterByPosicaoPrimeiroItem(array(12, 34)); // WHERE posicao_primeiro_item IN (12, 34)
     * $query->filterByPosicaoPrimeiroItem(array('min' => 12)); // WHERE posicao_primeiro_item > 12
     * </code>
     *
     * @param     mixed $posicaoPrimeiroItem The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByPosicaoPrimeiroItem($posicaoPrimeiroItem = null, $comparison = null)
    {
        if (is_array($posicaoPrimeiroItem)) {
            $useMinMax = false;
            if (isset($posicaoPrimeiroItem['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM, $posicaoPrimeiroItem['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoPrimeiroItem['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM, $posicaoPrimeiroItem['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM, $posicaoPrimeiroItem, $comparison);
    }

    /**
     * Filter the query on the posicao_ultimo_item column
     *
     * Example usage:
     * <code>
     * $query->filterByPosicaoUltimoItem(1234); // WHERE posicao_ultimo_item = 1234
     * $query->filterByPosicaoUltimoItem(array(12, 34)); // WHERE posicao_ultimo_item IN (12, 34)
     * $query->filterByPosicaoUltimoItem(array('min' => 12)); // WHERE posicao_ultimo_item > 12
     * </code>
     *
     * @param     mixed $posicaoUltimoItem The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByPosicaoUltimoItem($posicaoUltimoItem = null, $comparison = null)
    {
        if (is_array($posicaoUltimoItem)) {
            $useMinMax = false;
            if (isset($posicaoUltimoItem['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM, $posicaoUltimoItem['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($posicaoUltimoItem['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM, $posicaoUltimoItem['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM, $posicaoUltimoItem, $comparison);
    }

    /**
     * Filter the query on the ultima_nota_gerada column
     *
     * Example usage:
     * <code>
     * $query->filterByUltimaNotaGerada(true); // WHERE ultima_nota_gerada = true
     * $query->filterByUltimaNotaGerada('yes'); // WHERE ultima_nota_gerada = true
     * </code>
     *
     * @param     boolean|string $ultimaNotaGerada The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByUltimaNotaGerada($ultimaNotaGerada = null, $comparison = null)
    {
        if (is_string($ultimaNotaGerada)) {
            $ultimaNotaGerada = in_array(strtolower($ultimaNotaGerada), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_ULTIMA_NOTA_GERADA, $ultimaNotaGerada, $comparison);
    }

    /**
     * Filter the query on the tipo_nota column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoNota('fooValue');   // WHERE tipo_nota = 'fooValue'
     * $query->filterByTipoNota('%fooValue%', Criteria::LIKE); // WHERE tipo_nota LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoNota The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByTipoNota($tipoNota = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoNota)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_TIPO_NOTA, $tipoNota, $comparison);
    }

    /**
     * Filter the query on the protocolo column
     *
     * Example usage:
     * <code>
     * $query->filterByProtocolo(1234); // WHERE protocolo = 1234
     * $query->filterByProtocolo(array(12, 34)); // WHERE protocolo IN (12, 34)
     * $query->filterByProtocolo(array('min' => 12)); // WHERE protocolo > 12
     * </code>
     *
     * @param     mixed $protocolo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByProtocolo($protocolo = null, $comparison = null)
    {
        if (is_array($protocolo)) {
            $useMinMax = false;
            if (isset($protocolo['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_PROTOCOLO, $protocolo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($protocolo['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_PROTOCOLO, $protocolo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_PROTOCOLO, $protocolo, $comparison);
    }

    /**
     * Filter the query on the data_emitido column
     *
     * Example usage:
     * <code>
     * $query->filterByDataEmitido('2011-03-14'); // WHERE data_emitido = '2011-03-14'
     * $query->filterByDataEmitido('now'); // WHERE data_emitido = '2011-03-14'
     * $query->filterByDataEmitido(array('max' => 'yesterday')); // WHERE data_emitido > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataEmitido The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByDataEmitido($dataEmitido = null, $comparison = null)
    {
        if (is_array($dataEmitido)) {
            $useMinMax = false;
            if (isset($dataEmitido['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_EMITIDO, $dataEmitido['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataEmitido['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_EMITIDO, $dataEmitido['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_DATA_EMITIDO, $dataEmitido, $comparison);
    }

    /**
     * Filter the query on the numero_rps column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroRps(1234); // WHERE numero_rps = 1234
     * $query->filterByNumeroRps(array(12, 34)); // WHERE numero_rps IN (12, 34)
     * $query->filterByNumeroRps(array('min' => 12)); // WHERE numero_rps > 12
     * </code>
     *
     * @param     mixed $numeroRps The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByNumeroRps($numeroRps = null, $comparison = null)
    {
        if (is_array($numeroRps)) {
            $useMinMax = false;
            if (isset($numeroRps['min'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_NUMERO_RPS, $numeroRps['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numeroRps['max'])) {
                $this->addUsingAlias(DadosFiscalTableMap::COL_NUMERO_RPS, $numeroRps['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DadosFiscalTableMap::COL_NUMERO_RPS, $numeroRps, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Boleto object
     *
     * @param \ImaTelecomBundle\Model\Boleto|ObjectCollection $boleto The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByBoleto($boleto, $comparison = null)
    {
        if ($boleto instanceof \ImaTelecomBundle\Model\Boleto) {
            return $this
                ->addUsingAlias(DadosFiscalTableMap::COL_BOLETO_ID, $boleto->getIdboleto(), $comparison);
        } elseif ($boleto instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DadosFiscalTableMap::COL_BOLETO_ID, $boleto->toKeyValue('PrimaryKey', 'Idboleto'), $comparison);
        } else {
            throw new PropelException('filterByBoleto() only accepts arguments of type \ImaTelecomBundle\Model\Boleto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Boleto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function joinBoleto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Boleto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Boleto');
        }

        return $this;
    }

    /**
     * Use the Boleto relation Boleto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BoletoQuery A secondary query class using the current class as primary query
     */
    public function useBoletoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBoleto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Boleto', '\ImaTelecomBundle\Model\BoletoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Competencia object
     *
     * @param \ImaTelecomBundle\Model\Competencia|ObjectCollection $competencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByCompetencia($competencia, $comparison = null)
    {
        if ($competencia instanceof \ImaTelecomBundle\Model\Competencia) {
            return $this
                ->addUsingAlias(DadosFiscalTableMap::COL_COMPETENCIA_ID, $competencia->getId(), $comparison);
        } elseif ($competencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DadosFiscalTableMap::COL_COMPETENCIA_ID, $competencia->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompetencia() only accepts arguments of type \ImaTelecomBundle\Model\Competencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Competencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function joinCompetencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Competencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Competencia');
        }

        return $this;
    }

    /**
     * Use the Competencia relation Competencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\CompetenciaQuery A secondary query class using the current class as primary query
     */
    public function useCompetenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompetencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Competencia', '\ImaTelecomBundle\Model\CompetenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Pessoa object
     *
     * @param \ImaTelecomBundle\Model\Pessoa|ObjectCollection $pessoa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByPessoa($pessoa, $comparison = null)
    {
        if ($pessoa instanceof \ImaTelecomBundle\Model\Pessoa) {
            return $this
                ->addUsingAlias(DadosFiscalTableMap::COL_PESSOA_IMPORT_ID, $pessoa->getImportId(), $comparison);
        } elseif ($pessoa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DadosFiscalTableMap::COL_PESSOA_IMPORT_ID, $pessoa->toKeyValue('PrimaryKey', 'ImportId'), $comparison);
        } else {
            throw new PropelException('filterByPessoa() only accepts arguments of type \ImaTelecomBundle\Model\Pessoa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pessoa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function joinPessoa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pessoa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pessoa');
        }

        return $this;
    }

    /**
     * Use the Pessoa relation Pessoa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\PessoaQuery A secondary query class using the current class as primary query
     */
    public function usePessoaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPessoa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pessoa', '\ImaTelecomBundle\Model\PessoaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\DadosFiscalItem object
     *
     * @param \ImaTelecomBundle\Model\DadosFiscalItem|ObjectCollection $dadosFiscalItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function filterByDadosFiscalItem($dadosFiscalItem, $comparison = null)
    {
        if ($dadosFiscalItem instanceof \ImaTelecomBundle\Model\DadosFiscalItem) {
            return $this
                ->addUsingAlias(DadosFiscalTableMap::COL_ID, $dadosFiscalItem->getDadosFiscalId(), $comparison);
        } elseif ($dadosFiscalItem instanceof ObjectCollection) {
            return $this
                ->useDadosFiscalItemQuery()
                ->filterByPrimaryKeys($dadosFiscalItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDadosFiscalItem() only accepts arguments of type \ImaTelecomBundle\Model\DadosFiscalItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DadosFiscalItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function joinDadosFiscalItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DadosFiscalItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DadosFiscalItem');
        }

        return $this;
    }

    /**
     * Use the DadosFiscalItem relation DadosFiscalItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\DadosFiscalItemQuery A secondary query class using the current class as primary query
     */
    public function useDadosFiscalItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDadosFiscalItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DadosFiscalItem', '\ImaTelecomBundle\Model\DadosFiscalItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDadosFiscal $dadosFiscal Object to remove from the list of results
     *
     * @return $this|ChildDadosFiscalQuery The current query, for fluid interface
     */
    public function prune($dadosFiscal = null)
    {
        if ($dadosFiscal) {
            $this->addUsingAlias(DadosFiscalTableMap::COL_ID, $dadosFiscal->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the dados_fiscal table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DadosFiscalTableMap::clearInstancePool();
            DadosFiscalTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DadosFiscalTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DadosFiscalTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DadosFiscalTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // DadosFiscalQuery
