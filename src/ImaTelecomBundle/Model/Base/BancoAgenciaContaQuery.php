<?php

namespace ImaTelecomBundle\Model\Base;

use \Exception;
use \PDO;
use ImaTelecomBundle\Model\BancoAgenciaConta as ChildBancoAgenciaConta;
use ImaTelecomBundle\Model\BancoAgenciaContaQuery as ChildBancoAgenciaContaQuery;
use ImaTelecomBundle\Model\Map\BancoAgenciaContaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'banco_agencia_conta' table.
 *
 *
 *
 * @method     ChildBancoAgenciaContaQuery orderByIdbancoAgenciaConta($order = Criteria::ASC) Order by the idbanco_agencia_conta column
 * @method     ChildBancoAgenciaContaQuery orderByBancoId($order = Criteria::ASC) Order by the banco_id column
 * @method     ChildBancoAgenciaContaQuery orderByBancoAgenciaId($order = Criteria::ASC) Order by the banco_agencia_id column
 * @method     ChildBancoAgenciaContaQuery orderByTipoConta($order = Criteria::ASC) Order by the tipo_conta column
 * @method     ChildBancoAgenciaContaQuery orderByNumeroConta($order = Criteria::ASC) Order by the numero_conta column
 * @method     ChildBancoAgenciaContaQuery orderByDigitoVerificador($order = Criteria::ASC) Order by the digito_verificador column
 * @method     ChildBancoAgenciaContaQuery orderByConvenioId($order = Criteria::ASC) Order by the convenio_id column
 * @method     ChildBancoAgenciaContaQuery orderByDataCadastro($order = Criteria::ASC) Order by the data_cadastro column
 * @method     ChildBancoAgenciaContaQuery orderByDataAlterado($order = Criteria::ASC) Order by the data_alterado column
 * @method     ChildBancoAgenciaContaQuery orderByUsuarioAlterado($order = Criteria::ASC) Order by the usuario_alterado column
 *
 * @method     ChildBancoAgenciaContaQuery groupByIdbancoAgenciaConta() Group by the idbanco_agencia_conta column
 * @method     ChildBancoAgenciaContaQuery groupByBancoId() Group by the banco_id column
 * @method     ChildBancoAgenciaContaQuery groupByBancoAgenciaId() Group by the banco_agencia_id column
 * @method     ChildBancoAgenciaContaQuery groupByTipoConta() Group by the tipo_conta column
 * @method     ChildBancoAgenciaContaQuery groupByNumeroConta() Group by the numero_conta column
 * @method     ChildBancoAgenciaContaQuery groupByDigitoVerificador() Group by the digito_verificador column
 * @method     ChildBancoAgenciaContaQuery groupByConvenioId() Group by the convenio_id column
 * @method     ChildBancoAgenciaContaQuery groupByDataCadastro() Group by the data_cadastro column
 * @method     ChildBancoAgenciaContaQuery groupByDataAlterado() Group by the data_alterado column
 * @method     ChildBancoAgenciaContaQuery groupByUsuarioAlterado() Group by the usuario_alterado column
 *
 * @method     ChildBancoAgenciaContaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBancoAgenciaContaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBancoAgenciaContaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBancoAgenciaContaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBancoAgenciaContaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinBancoAgencia($relationAlias = null) Adds a LEFT JOIN clause to the query using the BancoAgencia relation
 * @method     ChildBancoAgenciaContaQuery rightJoinBancoAgencia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the BancoAgencia relation
 * @method     ChildBancoAgenciaContaQuery innerJoinBancoAgencia($relationAlias = null) Adds a INNER JOIN clause to the query using the BancoAgencia relation
 *
 * @method     ChildBancoAgenciaContaQuery joinWithBancoAgencia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the BancoAgencia relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinWithBancoAgencia() Adds a LEFT JOIN clause and with to the query using the BancoAgencia relation
 * @method     ChildBancoAgenciaContaQuery rightJoinWithBancoAgencia() Adds a RIGHT JOIN clause and with to the query using the BancoAgencia relation
 * @method     ChildBancoAgenciaContaQuery innerJoinWithBancoAgencia() Adds a INNER JOIN clause and with to the query using the BancoAgencia relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinBanco($relationAlias = null) Adds a LEFT JOIN clause to the query using the Banco relation
 * @method     ChildBancoAgenciaContaQuery rightJoinBanco($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Banco relation
 * @method     ChildBancoAgenciaContaQuery innerJoinBanco($relationAlias = null) Adds a INNER JOIN clause to the query using the Banco relation
 *
 * @method     ChildBancoAgenciaContaQuery joinWithBanco($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Banco relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinWithBanco() Adds a LEFT JOIN clause and with to the query using the Banco relation
 * @method     ChildBancoAgenciaContaQuery rightJoinWithBanco() Adds a RIGHT JOIN clause and with to the query using the Banco relation
 * @method     ChildBancoAgenciaContaQuery innerJoinWithBanco() Adds a INNER JOIN clause and with to the query using the Banco relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinConvenio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Convenio relation
 * @method     ChildBancoAgenciaContaQuery rightJoinConvenio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Convenio relation
 * @method     ChildBancoAgenciaContaQuery innerJoinConvenio($relationAlias = null) Adds a INNER JOIN clause to the query using the Convenio relation
 *
 * @method     ChildBancoAgenciaContaQuery joinWithConvenio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Convenio relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinWithConvenio() Adds a LEFT JOIN clause and with to the query using the Convenio relation
 * @method     ChildBancoAgenciaContaQuery rightJoinWithConvenio() Adds a RIGHT JOIN clause and with to the query using the Convenio relation
 * @method     ChildBancoAgenciaContaQuery innerJoinWithConvenio() Adds a INNER JOIN clause and with to the query using the Convenio relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBancoAgenciaContaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBancoAgenciaContaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBancoAgenciaContaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBancoAgenciaContaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBancoAgenciaContaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinContaCaixa($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContaCaixa relation
 * @method     ChildBancoAgenciaContaQuery rightJoinContaCaixa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContaCaixa relation
 * @method     ChildBancoAgenciaContaQuery innerJoinContaCaixa($relationAlias = null) Adds a INNER JOIN clause to the query using the ContaCaixa relation
 *
 * @method     ChildBancoAgenciaContaQuery joinWithContaCaixa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContaCaixa relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinWithContaCaixa() Adds a LEFT JOIN clause and with to the query using the ContaCaixa relation
 * @method     ChildBancoAgenciaContaQuery rightJoinWithContaCaixa() Adds a RIGHT JOIN clause and with to the query using the ContaCaixa relation
 * @method     ChildBancoAgenciaContaQuery innerJoinWithContaCaixa() Adds a INNER JOIN clause and with to the query using the ContaCaixa relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinFornecedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fornecedor relation
 * @method     ChildBancoAgenciaContaQuery rightJoinFornecedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fornecedor relation
 * @method     ChildBancoAgenciaContaQuery innerJoinFornecedor($relationAlias = null) Adds a INNER JOIN clause to the query using the Fornecedor relation
 *
 * @method     ChildBancoAgenciaContaQuery joinWithFornecedor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fornecedor relation
 *
 * @method     ChildBancoAgenciaContaQuery leftJoinWithFornecedor() Adds a LEFT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildBancoAgenciaContaQuery rightJoinWithFornecedor() Adds a RIGHT JOIN clause and with to the query using the Fornecedor relation
 * @method     ChildBancoAgenciaContaQuery innerJoinWithFornecedor() Adds a INNER JOIN clause and with to the query using the Fornecedor relation
 *
 * @method     \ImaTelecomBundle\Model\BancoAgenciaQuery|\ImaTelecomBundle\Model\BancoQuery|\ImaTelecomBundle\Model\ConvenioQuery|\ImaTelecomBundle\Model\UsuarioQuery|\ImaTelecomBundle\Model\ContaCaixaQuery|\ImaTelecomBundle\Model\FornecedorQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBancoAgenciaConta findOne(ConnectionInterface $con = null) Return the first ChildBancoAgenciaConta matching the query
 * @method     ChildBancoAgenciaConta findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBancoAgenciaConta matching the query, or a new ChildBancoAgenciaConta object populated from the query conditions when no match is found
 *
 * @method     ChildBancoAgenciaConta findOneByIdbancoAgenciaConta(int $idbanco_agencia_conta) Return the first ChildBancoAgenciaConta filtered by the idbanco_agencia_conta column
 * @method     ChildBancoAgenciaConta findOneByBancoId(int $banco_id) Return the first ChildBancoAgenciaConta filtered by the banco_id column
 * @method     ChildBancoAgenciaConta findOneByBancoAgenciaId(int $banco_agencia_id) Return the first ChildBancoAgenciaConta filtered by the banco_agencia_id column
 * @method     ChildBancoAgenciaConta findOneByTipoConta(string $tipo_conta) Return the first ChildBancoAgenciaConta filtered by the tipo_conta column
 * @method     ChildBancoAgenciaConta findOneByNumeroConta(string $numero_conta) Return the first ChildBancoAgenciaConta filtered by the numero_conta column
 * @method     ChildBancoAgenciaConta findOneByDigitoVerificador(string $digito_verificador) Return the first ChildBancoAgenciaConta filtered by the digito_verificador column
 * @method     ChildBancoAgenciaConta findOneByConvenioId(int $convenio_id) Return the first ChildBancoAgenciaConta filtered by the convenio_id column
 * @method     ChildBancoAgenciaConta findOneByDataCadastro(string $data_cadastro) Return the first ChildBancoAgenciaConta filtered by the data_cadastro column
 * @method     ChildBancoAgenciaConta findOneByDataAlterado(string $data_alterado) Return the first ChildBancoAgenciaConta filtered by the data_alterado column
 * @method     ChildBancoAgenciaConta findOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBancoAgenciaConta filtered by the usuario_alterado column *

 * @method     ChildBancoAgenciaConta requirePk($key, ConnectionInterface $con = null) Return the ChildBancoAgenciaConta by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOne(ConnectionInterface $con = null) Return the first ChildBancoAgenciaConta matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBancoAgenciaConta requireOneByIdbancoAgenciaConta(int $idbanco_agencia_conta) Return the first ChildBancoAgenciaConta filtered by the idbanco_agencia_conta column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOneByBancoId(int $banco_id) Return the first ChildBancoAgenciaConta filtered by the banco_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOneByBancoAgenciaId(int $banco_agencia_id) Return the first ChildBancoAgenciaConta filtered by the banco_agencia_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOneByTipoConta(string $tipo_conta) Return the first ChildBancoAgenciaConta filtered by the tipo_conta column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOneByNumeroConta(string $numero_conta) Return the first ChildBancoAgenciaConta filtered by the numero_conta column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOneByDigitoVerificador(string $digito_verificador) Return the first ChildBancoAgenciaConta filtered by the digito_verificador column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOneByConvenioId(int $convenio_id) Return the first ChildBancoAgenciaConta filtered by the convenio_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOneByDataCadastro(string $data_cadastro) Return the first ChildBancoAgenciaConta filtered by the data_cadastro column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOneByDataAlterado(string $data_alterado) Return the first ChildBancoAgenciaConta filtered by the data_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBancoAgenciaConta requireOneByUsuarioAlterado(int $usuario_alterado) Return the first ChildBancoAgenciaConta filtered by the usuario_alterado column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBancoAgenciaConta[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBancoAgenciaConta objects based on current ModelCriteria
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByIdbancoAgenciaConta(int $idbanco_agencia_conta) Return ChildBancoAgenciaConta objects filtered by the idbanco_agencia_conta column
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByBancoId(int $banco_id) Return ChildBancoAgenciaConta objects filtered by the banco_id column
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByBancoAgenciaId(int $banco_agencia_id) Return ChildBancoAgenciaConta objects filtered by the banco_agencia_id column
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByTipoConta(string $tipo_conta) Return ChildBancoAgenciaConta objects filtered by the tipo_conta column
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByNumeroConta(string $numero_conta) Return ChildBancoAgenciaConta objects filtered by the numero_conta column
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByDigitoVerificador(string $digito_verificador) Return ChildBancoAgenciaConta objects filtered by the digito_verificador column
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByConvenioId(int $convenio_id) Return ChildBancoAgenciaConta objects filtered by the convenio_id column
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByDataCadastro(string $data_cadastro) Return ChildBancoAgenciaConta objects filtered by the data_cadastro column
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByDataAlterado(string $data_alterado) Return ChildBancoAgenciaConta objects filtered by the data_alterado column
 * @method     ChildBancoAgenciaConta[]|ObjectCollection findByUsuarioAlterado(int $usuario_alterado) Return ChildBancoAgenciaConta objects filtered by the usuario_alterado column
 * @method     ChildBancoAgenciaConta[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BancoAgenciaContaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \ImaTelecomBundle\Model\Base\BancoAgenciaContaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ima_telecom', $modelName = '\\ImaTelecomBundle\\Model\\BancoAgenciaConta', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBancoAgenciaContaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBancoAgenciaContaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBancoAgenciaContaQuery) {
            return $criteria;
        }
        $query = new ChildBancoAgenciaContaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBancoAgenciaConta|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BancoAgenciaContaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BancoAgenciaContaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoAgenciaConta A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idbanco_agencia_conta, banco_id, banco_agencia_id, tipo_conta, numero_conta, digito_verificador, convenio_id, data_cadastro, data_alterado, usuario_alterado FROM banco_agencia_conta WHERE idbanco_agencia_conta = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBancoAgenciaConta $obj */
            $obj = new ChildBancoAgenciaConta();
            $obj->hydrate($row);
            BancoAgenciaContaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBancoAgenciaConta|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idbanco_agencia_conta column
     *
     * Example usage:
     * <code>
     * $query->filterByIdbancoAgenciaConta(1234); // WHERE idbanco_agencia_conta = 1234
     * $query->filterByIdbancoAgenciaConta(array(12, 34)); // WHERE idbanco_agencia_conta IN (12, 34)
     * $query->filterByIdbancoAgenciaConta(array('min' => 12)); // WHERE idbanco_agencia_conta > 12
     * </code>
     *
     * @param     mixed $idbancoAgenciaConta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByIdbancoAgenciaConta($idbancoAgenciaConta = null, $comparison = null)
    {
        if (is_array($idbancoAgenciaConta)) {
            $useMinMax = false;
            if (isset($idbancoAgenciaConta['min'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $idbancoAgenciaConta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idbancoAgenciaConta['max'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $idbancoAgenciaConta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $idbancoAgenciaConta, $comparison);
    }

    /**
     * Filter the query on the banco_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBancoId(1234); // WHERE banco_id = 1234
     * $query->filterByBancoId(array(12, 34)); // WHERE banco_id IN (12, 34)
     * $query->filterByBancoId(array('min' => 12)); // WHERE banco_id > 12
     * </code>
     *
     * @see       filterByBanco()
     *
     * @param     mixed $bancoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByBancoId($bancoId = null, $comparison = null)
    {
        if (is_array($bancoId)) {
            $useMinMax = false;
            if (isset($bancoId['min'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_ID, $bancoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bancoId['max'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_ID, $bancoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_ID, $bancoId, $comparison);
    }

    /**
     * Filter the query on the banco_agencia_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBancoAgenciaId(1234); // WHERE banco_agencia_id = 1234
     * $query->filterByBancoAgenciaId(array(12, 34)); // WHERE banco_agencia_id IN (12, 34)
     * $query->filterByBancoAgenciaId(array('min' => 12)); // WHERE banco_agencia_id > 12
     * </code>
     *
     * @see       filterByBancoAgencia()
     *
     * @param     mixed $bancoAgenciaId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByBancoAgenciaId($bancoAgenciaId = null, $comparison = null)
    {
        if (is_array($bancoAgenciaId)) {
            $useMinMax = false;
            if (isset($bancoAgenciaId['min'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID, $bancoAgenciaId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bancoAgenciaId['max'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID, $bancoAgenciaId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID, $bancoAgenciaId, $comparison);
    }

    /**
     * Filter the query on the tipo_conta column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoConta('fooValue');   // WHERE tipo_conta = 'fooValue'
     * $query->filterByTipoConta('%fooValue%', Criteria::LIKE); // WHERE tipo_conta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tipoConta The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByTipoConta($tipoConta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tipoConta)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_TIPO_CONTA, $tipoConta, $comparison);
    }

    /**
     * Filter the query on the numero_conta column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroConta('fooValue');   // WHERE numero_conta = 'fooValue'
     * $query->filterByNumeroConta('%fooValue%', Criteria::LIKE); // WHERE numero_conta LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numeroConta The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByNumeroConta($numeroConta = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numeroConta)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_NUMERO_CONTA, $numeroConta, $comparison);
    }

    /**
     * Filter the query on the digito_verificador column
     *
     * Example usage:
     * <code>
     * $query->filterByDigitoVerificador('fooValue');   // WHERE digito_verificador = 'fooValue'
     * $query->filterByDigitoVerificador('%fooValue%', Criteria::LIKE); // WHERE digito_verificador LIKE '%fooValue%'
     * </code>
     *
     * @param     string $digitoVerificador The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByDigitoVerificador($digitoVerificador = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($digitoVerificador)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_DIGITO_VERIFICADOR, $digitoVerificador, $comparison);
    }

    /**
     * Filter the query on the convenio_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConvenioId(1234); // WHERE convenio_id = 1234
     * $query->filterByConvenioId(array(12, 34)); // WHERE convenio_id IN (12, 34)
     * $query->filterByConvenioId(array('min' => 12)); // WHERE convenio_id > 12
     * </code>
     *
     * @see       filterByConvenio()
     *
     * @param     mixed $convenioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByConvenioId($convenioId = null, $comparison = null)
    {
        if (is_array($convenioId)) {
            $useMinMax = false;
            if (isset($convenioId['min'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_CONVENIO_ID, $convenioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($convenioId['max'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_CONVENIO_ID, $convenioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_CONVENIO_ID, $convenioId, $comparison);
    }

    /**
     * Filter the query on the data_cadastro column
     *
     * Example usage:
     * <code>
     * $query->filterByDataCadastro('2011-03-14'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro('now'); // WHERE data_cadastro = '2011-03-14'
     * $query->filterByDataCadastro(array('max' => 'yesterday')); // WHERE data_cadastro > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataCadastro The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByDataCadastro($dataCadastro = null, $comparison = null)
    {
        if (is_array($dataCadastro)) {
            $useMinMax = false;
            if (isset($dataCadastro['min'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_DATA_CADASTRO, $dataCadastro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataCadastro['max'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_DATA_CADASTRO, $dataCadastro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_DATA_CADASTRO, $dataCadastro, $comparison);
    }

    /**
     * Filter the query on the data_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByDataAlterado('2011-03-14'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado('now'); // WHERE data_alterado = '2011-03-14'
     * $query->filterByDataAlterado(array('max' => 'yesterday')); // WHERE data_alterado > '2011-03-13'
     * </code>
     *
     * @param     mixed $dataAlterado The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByDataAlterado($dataAlterado = null, $comparison = null)
    {
        if (is_array($dataAlterado)) {
            $useMinMax = false;
            if (isset($dataAlterado['min'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_DATA_ALTERADO, $dataAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataAlterado['max'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_DATA_ALTERADO, $dataAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_DATA_ALTERADO, $dataAlterado, $comparison);
    }

    /**
     * Filter the query on the usuario_alterado column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioAlterado(1234); // WHERE usuario_alterado = 1234
     * $query->filterByUsuarioAlterado(array(12, 34)); // WHERE usuario_alterado IN (12, 34)
     * $query->filterByUsuarioAlterado(array('min' => 12)); // WHERE usuario_alterado > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioAlterado The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByUsuarioAlterado($usuarioAlterado = null, $comparison = null)
    {
        if (is_array($usuarioAlterado)) {
            $useMinMax = false;
            if (isset($usuarioAlterado['min'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioAlterado['max'])) {
                $this->addUsingAlias(BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO, $usuarioAlterado, $comparison);
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\BancoAgencia object
     *
     * @param \ImaTelecomBundle\Model\BancoAgencia|ObjectCollection $bancoAgencia The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByBancoAgencia($bancoAgencia, $comparison = null)
    {
        if ($bancoAgencia instanceof \ImaTelecomBundle\Model\BancoAgencia) {
            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID, $bancoAgencia->getIdbancoAgencia(), $comparison);
        } elseif ($bancoAgencia instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID, $bancoAgencia->toKeyValue('PrimaryKey', 'IdbancoAgencia'), $comparison);
        } else {
            throw new PropelException('filterByBancoAgencia() only accepts arguments of type \ImaTelecomBundle\Model\BancoAgencia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the BancoAgencia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function joinBancoAgencia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('BancoAgencia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'BancoAgencia');
        }

        return $this;
    }

    /**
     * Use the BancoAgencia relation BancoAgencia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoAgenciaQuery A secondary query class using the current class as primary query
     */
    public function useBancoAgenciaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBancoAgencia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'BancoAgencia', '\ImaTelecomBundle\Model\BancoAgenciaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Banco object
     *
     * @param \ImaTelecomBundle\Model\Banco|ObjectCollection $banco The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByBanco($banco, $comparison = null)
    {
        if ($banco instanceof \ImaTelecomBundle\Model\Banco) {
            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_ID, $banco->getIdbanco(), $comparison);
        } elseif ($banco instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_BANCO_ID, $banco->toKeyValue('PrimaryKey', 'Idbanco'), $comparison);
        } else {
            throw new PropelException('filterByBanco() only accepts arguments of type \ImaTelecomBundle\Model\Banco or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Banco relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function joinBanco($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Banco');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Banco');
        }

        return $this;
    }

    /**
     * Use the Banco relation Banco object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\BancoQuery A secondary query class using the current class as primary query
     */
    public function useBancoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBanco($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Banco', '\ImaTelecomBundle\Model\BancoQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Convenio object
     *
     * @param \ImaTelecomBundle\Model\Convenio|ObjectCollection $convenio The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByConvenio($convenio, $comparison = null)
    {
        if ($convenio instanceof \ImaTelecomBundle\Model\Convenio) {
            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_CONVENIO_ID, $convenio->getIdconvenio(), $comparison);
        } elseif ($convenio instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_CONVENIO_ID, $convenio->toKeyValue('PrimaryKey', 'Idconvenio'), $comparison);
        } else {
            throw new PropelException('filterByConvenio() only accepts arguments of type \ImaTelecomBundle\Model\Convenio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Convenio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function joinConvenio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Convenio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Convenio');
        }

        return $this;
    }

    /**
     * Use the Convenio relation Convenio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ConvenioQuery A secondary query class using the current class as primary query
     */
    public function useConvenioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinConvenio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Convenio', '\ImaTelecomBundle\Model\ConvenioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Usuario object
     *
     * @param \ImaTelecomBundle\Model\Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \ImaTelecomBundle\Model\Usuario) {
            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO, $usuario->getIdusuario(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO, $usuario->toKeyValue('PrimaryKey', 'Idusuario'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \ImaTelecomBundle\Model\Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\ImaTelecomBundle\Model\UsuarioQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\ContaCaixa object
     *
     * @param \ImaTelecomBundle\Model\ContaCaixa|ObjectCollection $contaCaixa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByContaCaixa($contaCaixa, $comparison = null)
    {
        if ($contaCaixa instanceof \ImaTelecomBundle\Model\ContaCaixa) {
            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $contaCaixa->getBancoAgenciaContaId(), $comparison);
        } elseif ($contaCaixa instanceof ObjectCollection) {
            return $this
                ->useContaCaixaQuery()
                ->filterByPrimaryKeys($contaCaixa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContaCaixa() only accepts arguments of type \ImaTelecomBundle\Model\ContaCaixa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContaCaixa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function joinContaCaixa($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContaCaixa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContaCaixa');
        }

        return $this;
    }

    /**
     * Use the ContaCaixa relation ContaCaixa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\ContaCaixaQuery A secondary query class using the current class as primary query
     */
    public function useContaCaixaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContaCaixa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContaCaixa', '\ImaTelecomBundle\Model\ContaCaixaQuery');
    }

    /**
     * Filter the query by a related \ImaTelecomBundle\Model\Fornecedor object
     *
     * @param \ImaTelecomBundle\Model\Fornecedor|ObjectCollection $fornecedor the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function filterByFornecedor($fornecedor, $comparison = null)
    {
        if ($fornecedor instanceof \ImaTelecomBundle\Model\Fornecedor) {
            return $this
                ->addUsingAlias(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $fornecedor->getBancoAgenciaContaId(), $comparison);
        } elseif ($fornecedor instanceof ObjectCollection) {
            return $this
                ->useFornecedorQuery()
                ->filterByPrimaryKeys($fornecedor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFornecedor() only accepts arguments of type \ImaTelecomBundle\Model\Fornecedor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fornecedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function joinFornecedor($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fornecedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fornecedor');
        }

        return $this;
    }

    /**
     * Use the Fornecedor relation Fornecedor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ImaTelecomBundle\Model\FornecedorQuery A secondary query class using the current class as primary query
     */
    public function useFornecedorQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFornecedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fornecedor', '\ImaTelecomBundle\Model\FornecedorQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBancoAgenciaConta $bancoAgenciaConta Object to remove from the list of results
     *
     * @return $this|ChildBancoAgenciaContaQuery The current query, for fluid interface
     */
    public function prune($bancoAgenciaConta = null)
    {
        if ($bancoAgenciaConta) {
            $this->addUsingAlias(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, $bancoAgenciaConta->getIdbancoAgenciaConta(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the banco_agencia_conta table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaContaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BancoAgenciaContaTableMap::clearInstancePool();
            BancoAgenciaContaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaContaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BancoAgenciaContaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BancoAgenciaContaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BancoAgenciaContaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BancoAgenciaContaQuery
