<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Baixa as BaseBaixa;

/**
 * Skeleton subclass for representing a row from the 'baixa' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Baixa extends BaseBaixa
{

}
