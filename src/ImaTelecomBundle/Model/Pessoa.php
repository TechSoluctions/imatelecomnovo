<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Pessoa as BasePessoa;

/**
 * Skeleton subclass for representing a row from the 'pessoa' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Pessoa extends BasePessoa
{
    public function __construct()
    {
        $this->applyDefaultValues();
        $this->tipo_pessoa = 'fisica';
    }
    
    public function cidades() {
        $cidades = CidadeQuery::create()->orderByMunicipio()->find();
        return $cidades;
    }
    
    public function cidade() {
        $cidade = CidadeQuery::create()->findPk($this->cidade_id);
        if ($cidade == null) {
            return new Cidade();
        }
        
        return $cidade;
    }
    
    public function usuario() {
        $usuario = UsuarioQuery::create()->findPk($this->getUsuarioAlterado());        
        if ($usuario == null)
            $usuario = new Usuario();
        return $usuario;
    }
    
    public function getCliente() {
        $cliente = ClienteQuery::create()->findOneByAtivo(true);
        
        if ($cliente == null) 
            $cliente = new Cliente();
        
        return $cliente;
    }
    
    public function getTelefone() {
        $telefone = TelefoneQuery::create()
                ->where("tipo in ('principal', 'celular', 'residencial') and (ddd is not null or trim(ddd) != '') and pessoa_id = ".$this->id)
                ->find()->getLast();
        
        if ($telefone == null)
            $telefone = new Telefone();
        
        return $telefone;
    }
}
