<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\ItemComprado;
use ImaTelecomBundle\Model\ItemCompradoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'item_comprado' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ItemCompradoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ItemCompradoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'item_comprado';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\ItemComprado';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.ItemComprado';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the iditem_comprado field
     */
    const COL_IDITEM_COMPRADO = 'item_comprado.iditem_comprado';

    /**
     * the column name for the item_de_compra_id field
     */
    const COL_ITEM_DE_COMPRA_ID = 'item_comprado.item_de_compra_id';

    /**
     * the column name for the contas_pagar_id field
     */
    const COL_CONTAS_PAGAR_ID = 'item_comprado.contas_pagar_id';

    /**
     * the column name for the qtde field
     */
    const COL_QTDE = 'item_comprado.qtde';

    /**
     * the column name for the valor_unitario field
     */
    const COL_VALOR_UNITARIO = 'item_comprado.valor_unitario';

    /**
     * the column name for the obs field
     */
    const COL_OBS = 'item_comprado.obs';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'item_comprado.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'item_comprado.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'item_comprado.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IditemComprado', 'ItemDeCompraId', 'ContasPagarId', 'Qtde', 'ValorUnitario', 'Obs', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('iditemComprado', 'itemDeCompraId', 'contasPagarId', 'qtde', 'valorUnitario', 'obs', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(ItemCompradoTableMap::COL_IDITEM_COMPRADO, ItemCompradoTableMap::COL_ITEM_DE_COMPRA_ID, ItemCompradoTableMap::COL_CONTAS_PAGAR_ID, ItemCompradoTableMap::COL_QTDE, ItemCompradoTableMap::COL_VALOR_UNITARIO, ItemCompradoTableMap::COL_OBS, ItemCompradoTableMap::COL_DATA_CADASTRO, ItemCompradoTableMap::COL_DATA_ALTERADO, ItemCompradoTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('iditem_comprado', 'item_de_compra_id', 'contas_pagar_id', 'qtde', 'valor_unitario', 'obs', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IditemComprado' => 0, 'ItemDeCompraId' => 1, 'ContasPagarId' => 2, 'Qtde' => 3, 'ValorUnitario' => 4, 'Obs' => 5, 'DataCadastro' => 6, 'DataAlterado' => 7, 'UsuarioAlterado' => 8, ),
        self::TYPE_CAMELNAME     => array('iditemComprado' => 0, 'itemDeCompraId' => 1, 'contasPagarId' => 2, 'qtde' => 3, 'valorUnitario' => 4, 'obs' => 5, 'dataCadastro' => 6, 'dataAlterado' => 7, 'usuarioAlterado' => 8, ),
        self::TYPE_COLNAME       => array(ItemCompradoTableMap::COL_IDITEM_COMPRADO => 0, ItemCompradoTableMap::COL_ITEM_DE_COMPRA_ID => 1, ItemCompradoTableMap::COL_CONTAS_PAGAR_ID => 2, ItemCompradoTableMap::COL_QTDE => 3, ItemCompradoTableMap::COL_VALOR_UNITARIO => 4, ItemCompradoTableMap::COL_OBS => 5, ItemCompradoTableMap::COL_DATA_CADASTRO => 6, ItemCompradoTableMap::COL_DATA_ALTERADO => 7, ItemCompradoTableMap::COL_USUARIO_ALTERADO => 8, ),
        self::TYPE_FIELDNAME     => array('iditem_comprado' => 0, 'item_de_compra_id' => 1, 'contas_pagar_id' => 2, 'qtde' => 3, 'valor_unitario' => 4, 'obs' => 5, 'data_cadastro' => 6, 'data_alterado' => 7, 'usuario_alterado' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('item_comprado');
        $this->setPhpName('ItemComprado');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\ItemComprado');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('iditem_comprado', 'IditemComprado', 'INTEGER', true, null, null);
        $this->addForeignKey('item_de_compra_id', 'ItemDeCompraId', 'INTEGER', 'item_de_compra', 'iditem_de_compra', true, null, null);
        $this->addForeignKey('contas_pagar_id', 'ContasPagarId', 'INTEGER', 'contas_pagar', 'idcontas_pagar', true, 10, null);
        $this->addColumn('qtde', 'Qtde', 'INTEGER', true, null, null);
        $this->addColumn('valor_unitario', 'ValorUnitario', 'DOUBLE', true, 15, null);
        $this->addColumn('obs', 'Obs', 'VARCHAR', false, 255, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ContasPagar', '\\ImaTelecomBundle\\Model\\ContasPagar', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':contas_pagar_id',
    1 => ':idcontas_pagar',
  ),
), null, null, null, false);
        $this->addRelation('ItemDeCompra', '\\ImaTelecomBundle\\Model\\ItemDeCompra', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':item_de_compra_id',
    1 => ':iditem_de_compra',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IditemComprado', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IditemComprado', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IditemComprado', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IditemComprado', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IditemComprado', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IditemComprado', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IditemComprado', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ItemCompradoTableMap::CLASS_DEFAULT : ItemCompradoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ItemComprado object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ItemCompradoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ItemCompradoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ItemCompradoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ItemCompradoTableMap::OM_CLASS;
            /** @var ItemComprado $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ItemCompradoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ItemCompradoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ItemCompradoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ItemComprado $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ItemCompradoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ItemCompradoTableMap::COL_IDITEM_COMPRADO);
            $criteria->addSelectColumn(ItemCompradoTableMap::COL_ITEM_DE_COMPRA_ID);
            $criteria->addSelectColumn(ItemCompradoTableMap::COL_CONTAS_PAGAR_ID);
            $criteria->addSelectColumn(ItemCompradoTableMap::COL_QTDE);
            $criteria->addSelectColumn(ItemCompradoTableMap::COL_VALOR_UNITARIO);
            $criteria->addSelectColumn(ItemCompradoTableMap::COL_OBS);
            $criteria->addSelectColumn(ItemCompradoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(ItemCompradoTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(ItemCompradoTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.iditem_comprado');
            $criteria->addSelectColumn($alias . '.item_de_compra_id');
            $criteria->addSelectColumn($alias . '.contas_pagar_id');
            $criteria->addSelectColumn($alias . '.qtde');
            $criteria->addSelectColumn($alias . '.valor_unitario');
            $criteria->addSelectColumn($alias . '.obs');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ItemCompradoTableMap::DATABASE_NAME)->getTable(ItemCompradoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ItemCompradoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ItemCompradoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ItemCompradoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ItemComprado or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ItemComprado object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ItemCompradoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\ItemComprado) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ItemCompradoTableMap::DATABASE_NAME);
            $criteria->add(ItemCompradoTableMap::COL_IDITEM_COMPRADO, (array) $values, Criteria::IN);
        }

        $query = ItemCompradoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ItemCompradoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ItemCompradoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the item_comprado table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ItemCompradoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ItemComprado or Criteria object.
     *
     * @param mixed               $criteria Criteria or ItemComprado object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ItemCompradoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ItemComprado object
        }

        if ($criteria->containsKey(ItemCompradoTableMap::COL_IDITEM_COMPRADO) && $criteria->keyContainsValue(ItemCompradoTableMap::COL_IDITEM_COMPRADO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ItemCompradoTableMap::COL_IDITEM_COMPRADO.')');
        }


        // Set the correct dbName
        $query = ItemCompradoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ItemCompradoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ItemCompradoTableMap::buildTableMap();
