<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\DadosFiscal;
use ImaTelecomBundle\Model\DadosFiscalQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'dados_fiscal' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class DadosFiscalTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.DadosFiscalTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'dados_fiscal';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\DadosFiscal';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.DadosFiscal';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 29;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 29;

    /**
     * the column name for the id field
     */
    const COL_ID = 'dados_fiscal.id';

    /**
     * the column name for the boleto_item_id field
     */
    const COL_BOLETO_ITEM_ID = 'dados_fiscal.boleto_item_id';

    /**
     * the column name for the documento field
     */
    const COL_DOCUMENTO = 'dados_fiscal.documento';

    /**
     * the column name for the item field
     */
    const COL_ITEM = 'dados_fiscal.item';

    /**
     * the column name for the mestre field
     */
    const COL_MESTRE = 'dados_fiscal.mestre';

    /**
     * the column name for the numero_nota_fiscal field
     */
    const COL_NUMERO_NOTA_FISCAL = 'dados_fiscal.numero_nota_fiscal';

    /**
     * the column name for the competencia_id field
     */
    const COL_COMPETENCIA_ID = 'dados_fiscal.competencia_id';

    /**
     * the column name for the data_emissao field
     */
    const COL_DATA_EMISSAO = 'dados_fiscal.data_emissao';

    /**
     * the column name for the mestre_chave_digital_meio field
     */
    const COL_MESTRE_CHAVE_DIGITAL_MEIO = 'dados_fiscal.mestre_chave_digital_meio';

    /**
     * the column name for the cfop field
     */
    const COL_CFOP = 'dados_fiscal.cfop';

    /**
     * the column name for the base_calculo field
     */
    const COL_BASE_CALCULO = 'dados_fiscal.base_calculo';

    /**
     * the column name for the valor_icms field
     */
    const COL_VALOR_ICMS = 'dados_fiscal.valor_icms';

    /**
     * the column name for the n_tributo field
     */
    const COL_N_TRIBUTO = 'dados_fiscal.n_tributo';

    /**
     * the column name for the valor_nota field
     */
    const COL_VALOR_NOTA = 'dados_fiscal.valor_nota';

    /**
     * the column name for the situacao field
     */
    const COL_SITUACAO = 'dados_fiscal.situacao';

    /**
     * the column name for the servico_cliente_import_id field
     */
    const COL_SERVICO_CLIENTE_IMPORT_ID = 'dados_fiscal.servico_cliente_import_id';

    /**
     * the column name for the cod_nota_fiscal_lote_previo field
     */
    const COL_COD_NOTA_FISCAL_LOTE_PREVIO = 'dados_fiscal.cod_nota_fiscal_lote_previo';

    /**
     * the column name for the pessoa_import_id field
     */
    const COL_PESSOA_IMPORT_ID = 'dados_fiscal.pessoa_import_id';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'dados_fiscal.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'dados_fiscal.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'dados_fiscal.usuario_alterado';

    /**
     * the column name for the boleto_id field
     */
    const COL_BOLETO_ID = 'dados_fiscal.boleto_id';

    /**
     * the column name for the posicao_primeiro_item field
     */
    const COL_POSICAO_PRIMEIRO_ITEM = 'dados_fiscal.posicao_primeiro_item';

    /**
     * the column name for the posicao_ultimo_item field
     */
    const COL_POSICAO_ULTIMO_ITEM = 'dados_fiscal.posicao_ultimo_item';

    /**
     * the column name for the ultima_nota_gerada field
     */
    const COL_ULTIMA_NOTA_GERADA = 'dados_fiscal.ultima_nota_gerada';

    /**
     * the column name for the tipo_nota field
     */
    const COL_TIPO_NOTA = 'dados_fiscal.tipo_nota';

    /**
     * the column name for the protocolo field
     */
    const COL_PROTOCOLO = 'dados_fiscal.protocolo';

    /**
     * the column name for the data_emitido field
     */
    const COL_DATA_EMITIDO = 'dados_fiscal.data_emitido';

    /**
     * the column name for the numero_rps field
     */
    const COL_NUMERO_RPS = 'dados_fiscal.numero_rps';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'BoletoItemId', 'Documento', 'Item', 'Mestre', 'NumeroNotaFiscal', 'CompetenciaId', 'DataEmissao', 'MestreChaveDigitalMeio', 'Cfop', 'BaseCalculo', 'ValorIcms', 'NTributo', 'ValorNota', 'Situacao', 'ServicoClienteImportId', 'CodNotaFiscalLotePrevio', 'PessoaImportId', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'BoletoId', 'PosicaoPrimeiroItem', 'PosicaoUltimoItem', 'UltimaNotaGerada', 'TipoNota', 'Protocolo', 'DataEmitido', 'NumeroRps', ),
        self::TYPE_CAMELNAME     => array('id', 'boletoItemId', 'documento', 'item', 'mestre', 'numeroNotaFiscal', 'competenciaId', 'dataEmissao', 'mestreChaveDigitalMeio', 'cfop', 'baseCalculo', 'valorIcms', 'nTributo', 'valorNota', 'situacao', 'servicoClienteImportId', 'codNotaFiscalLotePrevio', 'pessoaImportId', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'boletoId', 'posicaoPrimeiroItem', 'posicaoUltimoItem', 'ultimaNotaGerada', 'tipoNota', 'protocolo', 'dataEmitido', 'numeroRps', ),
        self::TYPE_COLNAME       => array(DadosFiscalTableMap::COL_ID, DadosFiscalTableMap::COL_BOLETO_ITEM_ID, DadosFiscalTableMap::COL_DOCUMENTO, DadosFiscalTableMap::COL_ITEM, DadosFiscalTableMap::COL_MESTRE, DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL, DadosFiscalTableMap::COL_COMPETENCIA_ID, DadosFiscalTableMap::COL_DATA_EMISSAO, DadosFiscalTableMap::COL_MESTRE_CHAVE_DIGITAL_MEIO, DadosFiscalTableMap::COL_CFOP, DadosFiscalTableMap::COL_BASE_CALCULO, DadosFiscalTableMap::COL_VALOR_ICMS, DadosFiscalTableMap::COL_N_TRIBUTO, DadosFiscalTableMap::COL_VALOR_NOTA, DadosFiscalTableMap::COL_SITUACAO, DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID, DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO, DadosFiscalTableMap::COL_PESSOA_IMPORT_ID, DadosFiscalTableMap::COL_DATA_CADASTRO, DadosFiscalTableMap::COL_DATA_ALTERADO, DadosFiscalTableMap::COL_USUARIO_ALTERADO, DadosFiscalTableMap::COL_BOLETO_ID, DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM, DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM, DadosFiscalTableMap::COL_ULTIMA_NOTA_GERADA, DadosFiscalTableMap::COL_TIPO_NOTA, DadosFiscalTableMap::COL_PROTOCOLO, DadosFiscalTableMap::COL_DATA_EMITIDO, DadosFiscalTableMap::COL_NUMERO_RPS, ),
        self::TYPE_FIELDNAME     => array('id', 'boleto_item_id', 'documento', 'item', 'mestre', 'numero_nota_fiscal', 'competencia_id', 'data_emissao', 'mestre_chave_digital_meio', 'cfop', 'base_calculo', 'valor_icms', 'n_tributo', 'valor_nota', 'situacao', 'servico_cliente_import_id', 'cod_nota_fiscal_lote_previo', 'pessoa_import_id', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'boleto_id', 'posicao_primeiro_item', 'posicao_ultimo_item', 'ultima_nota_gerada', 'tipo_nota', 'protocolo', 'data_emitido', 'numero_rps', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'BoletoItemId' => 1, 'Documento' => 2, 'Item' => 3, 'Mestre' => 4, 'NumeroNotaFiscal' => 5, 'CompetenciaId' => 6, 'DataEmissao' => 7, 'MestreChaveDigitalMeio' => 8, 'Cfop' => 9, 'BaseCalculo' => 10, 'ValorIcms' => 11, 'NTributo' => 12, 'ValorNota' => 13, 'Situacao' => 14, 'ServicoClienteImportId' => 15, 'CodNotaFiscalLotePrevio' => 16, 'PessoaImportId' => 17, 'DataCadastro' => 18, 'DataAlterado' => 19, 'UsuarioAlterado' => 20, 'BoletoId' => 21, 'PosicaoPrimeiroItem' => 22, 'PosicaoUltimoItem' => 23, 'UltimaNotaGerada' => 24, 'TipoNota' => 25, 'Protocolo' => 26, 'DataEmitido' => 27, 'NumeroRps' => 28, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'boletoItemId' => 1, 'documento' => 2, 'item' => 3, 'mestre' => 4, 'numeroNotaFiscal' => 5, 'competenciaId' => 6, 'dataEmissao' => 7, 'mestreChaveDigitalMeio' => 8, 'cfop' => 9, 'baseCalculo' => 10, 'valorIcms' => 11, 'nTributo' => 12, 'valorNota' => 13, 'situacao' => 14, 'servicoClienteImportId' => 15, 'codNotaFiscalLotePrevio' => 16, 'pessoaImportId' => 17, 'dataCadastro' => 18, 'dataAlterado' => 19, 'usuarioAlterado' => 20, 'boletoId' => 21, 'posicaoPrimeiroItem' => 22, 'posicaoUltimoItem' => 23, 'ultimaNotaGerada' => 24, 'tipoNota' => 25, 'protocolo' => 26, 'dataEmitido' => 27, 'numeroRps' => 28, ),
        self::TYPE_COLNAME       => array(DadosFiscalTableMap::COL_ID => 0, DadosFiscalTableMap::COL_BOLETO_ITEM_ID => 1, DadosFiscalTableMap::COL_DOCUMENTO => 2, DadosFiscalTableMap::COL_ITEM => 3, DadosFiscalTableMap::COL_MESTRE => 4, DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL => 5, DadosFiscalTableMap::COL_COMPETENCIA_ID => 6, DadosFiscalTableMap::COL_DATA_EMISSAO => 7, DadosFiscalTableMap::COL_MESTRE_CHAVE_DIGITAL_MEIO => 8, DadosFiscalTableMap::COL_CFOP => 9, DadosFiscalTableMap::COL_BASE_CALCULO => 10, DadosFiscalTableMap::COL_VALOR_ICMS => 11, DadosFiscalTableMap::COL_N_TRIBUTO => 12, DadosFiscalTableMap::COL_VALOR_NOTA => 13, DadosFiscalTableMap::COL_SITUACAO => 14, DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID => 15, DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO => 16, DadosFiscalTableMap::COL_PESSOA_IMPORT_ID => 17, DadosFiscalTableMap::COL_DATA_CADASTRO => 18, DadosFiscalTableMap::COL_DATA_ALTERADO => 19, DadosFiscalTableMap::COL_USUARIO_ALTERADO => 20, DadosFiscalTableMap::COL_BOLETO_ID => 21, DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM => 22, DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM => 23, DadosFiscalTableMap::COL_ULTIMA_NOTA_GERADA => 24, DadosFiscalTableMap::COL_TIPO_NOTA => 25, DadosFiscalTableMap::COL_PROTOCOLO => 26, DadosFiscalTableMap::COL_DATA_EMITIDO => 27, DadosFiscalTableMap::COL_NUMERO_RPS => 28, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'boleto_item_id' => 1, 'documento' => 2, 'item' => 3, 'mestre' => 4, 'numero_nota_fiscal' => 5, 'competencia_id' => 6, 'data_emissao' => 7, 'mestre_chave_digital_meio' => 8, 'cfop' => 9, 'base_calculo' => 10, 'valor_icms' => 11, 'n_tributo' => 12, 'valor_nota' => 13, 'situacao' => 14, 'servico_cliente_import_id' => 15, 'cod_nota_fiscal_lote_previo' => 16, 'pessoa_import_id' => 17, 'data_cadastro' => 18, 'data_alterado' => 19, 'usuario_alterado' => 20, 'boleto_id' => 21, 'posicao_primeiro_item' => 22, 'posicao_ultimo_item' => 23, 'ultima_nota_gerada' => 24, 'tipo_nota' => 25, 'protocolo' => 26, 'data_emitido' => 27, 'numero_rps' => 28, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('dados_fiscal');
        $this->setPhpName('DadosFiscal');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\DadosFiscal');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('boleto_item_id', 'BoletoItemId', 'INTEGER', false, 10, null);
        $this->addColumn('documento', 'Documento', 'LONGVARCHAR', false, null, null);
        $this->addColumn('item', 'Item', 'LONGVARCHAR', false, null, null);
        $this->addColumn('mestre', 'Mestre', 'LONGVARCHAR', false, null, null);
        $this->addColumn('numero_nota_fiscal', 'NumeroNotaFiscal', 'INTEGER', false, 10, null);
        $this->addForeignKey('competencia_id', 'CompetenciaId', 'INTEGER', 'competencia', 'id', true, 10, null);
        $this->addColumn('data_emissao', 'DataEmissao', 'DATE', true, null, null);
        $this->addColumn('mestre_chave_digital_meio', 'MestreChaveDigitalMeio', 'VARCHAR', false, 250, null);
        $this->addColumn('cfop', 'Cfop', 'CHAR', false, null, null);
        $this->addColumn('base_calculo', 'BaseCalculo', 'VARCHAR', true, 45, null);
        $this->addColumn('valor_icms', 'ValorIcms', 'VARCHAR', true, 45, null);
        $this->addColumn('n_tributo', 'NTributo', 'VARCHAR', true, 45, null);
        $this->addColumn('valor_nota', 'ValorNota', 'VARCHAR', true, 45, null);
        $this->addColumn('situacao', 'Situacao', 'CHAR', true, null, null);
        $this->addColumn('servico_cliente_import_id', 'ServicoClienteImportId', 'INTEGER', false, 10, null);
        $this->addColumn('cod_nota_fiscal_lote_previo', 'CodNotaFiscalLotePrevio', 'INTEGER', false, 10, null);
        $this->addForeignKey('pessoa_import_id', 'PessoaImportId', 'INTEGER', 'pessoa', 'import_id', true, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addColumn('usuario_alterado', 'UsuarioAlterado', 'INTEGER', true, 10, null);
        $this->addForeignKey('boleto_id', 'BoletoId', 'INTEGER', 'boleto', 'idboleto', true, 10, null);
        $this->addColumn('posicao_primeiro_item', 'PosicaoPrimeiroItem', 'INTEGER', false, 10, null);
        $this->addColumn('posicao_ultimo_item', 'PosicaoUltimoItem', 'INTEGER', false, 10, null);
        $this->addColumn('ultima_nota_gerada', 'UltimaNotaGerada', 'BOOLEAN', false, 1, true);
        $this->addColumn('tipo_nota', 'TipoNota', 'CHAR', false, null, 'scm');
        $this->addColumn('protocolo', 'Protocolo', 'INTEGER', false, 10, null);
        $this->addColumn('data_emitido', 'DataEmitido', 'TIMESTAMP', false, null, null);
        $this->addColumn('numero_rps', 'NumeroRps', 'INTEGER', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, null, false);
        $this->addRelation('Competencia', '\\ImaTelecomBundle\\Model\\Competencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Pessoa', '\\ImaTelecomBundle\\Model\\Pessoa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pessoa_import_id',
    1 => ':import_id',
  ),
), null, null, null, false);
        $this->addRelation('DadosFiscalItem', '\\ImaTelecomBundle\\Model\\DadosFiscalItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':dados_fiscal_id',
    1 => ':id',
  ),
), null, null, 'DadosFiscalItems', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? DadosFiscalTableMap::CLASS_DEFAULT : DadosFiscalTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (DadosFiscal object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = DadosFiscalTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = DadosFiscalTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + DadosFiscalTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = DadosFiscalTableMap::OM_CLASS;
            /** @var DadosFiscal $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            DadosFiscalTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = DadosFiscalTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = DadosFiscalTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var DadosFiscal $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                DadosFiscalTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_ID);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_BOLETO_ITEM_ID);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_DOCUMENTO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_ITEM);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_MESTRE);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_NUMERO_NOTA_FISCAL);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_COMPETENCIA_ID);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_DATA_EMISSAO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_MESTRE_CHAVE_DIGITAL_MEIO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_CFOP);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_BASE_CALCULO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_VALOR_ICMS);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_N_TRIBUTO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_VALOR_NOTA);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_SITUACAO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_SERVICO_CLIENTE_IMPORT_ID);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_COD_NOTA_FISCAL_LOTE_PREVIO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_PESSOA_IMPORT_ID);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_BOLETO_ID);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_POSICAO_PRIMEIRO_ITEM);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_POSICAO_ULTIMO_ITEM);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_ULTIMA_NOTA_GERADA);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_TIPO_NOTA);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_PROTOCOLO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_DATA_EMITIDO);
            $criteria->addSelectColumn(DadosFiscalTableMap::COL_NUMERO_RPS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.boleto_item_id');
            $criteria->addSelectColumn($alias . '.documento');
            $criteria->addSelectColumn($alias . '.item');
            $criteria->addSelectColumn($alias . '.mestre');
            $criteria->addSelectColumn($alias . '.numero_nota_fiscal');
            $criteria->addSelectColumn($alias . '.competencia_id');
            $criteria->addSelectColumn($alias . '.data_emissao');
            $criteria->addSelectColumn($alias . '.mestre_chave_digital_meio');
            $criteria->addSelectColumn($alias . '.cfop');
            $criteria->addSelectColumn($alias . '.base_calculo');
            $criteria->addSelectColumn($alias . '.valor_icms');
            $criteria->addSelectColumn($alias . '.n_tributo');
            $criteria->addSelectColumn($alias . '.valor_nota');
            $criteria->addSelectColumn($alias . '.situacao');
            $criteria->addSelectColumn($alias . '.servico_cliente_import_id');
            $criteria->addSelectColumn($alias . '.cod_nota_fiscal_lote_previo');
            $criteria->addSelectColumn($alias . '.pessoa_import_id');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.boleto_id');
            $criteria->addSelectColumn($alias . '.posicao_primeiro_item');
            $criteria->addSelectColumn($alias . '.posicao_ultimo_item');
            $criteria->addSelectColumn($alias . '.ultima_nota_gerada');
            $criteria->addSelectColumn($alias . '.tipo_nota');
            $criteria->addSelectColumn($alias . '.protocolo');
            $criteria->addSelectColumn($alias . '.data_emitido');
            $criteria->addSelectColumn($alias . '.numero_rps');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(DadosFiscalTableMap::DATABASE_NAME)->getTable(DadosFiscalTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(DadosFiscalTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(DadosFiscalTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new DadosFiscalTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a DadosFiscal or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or DadosFiscal object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\DadosFiscal) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(DadosFiscalTableMap::DATABASE_NAME);
            $criteria->add(DadosFiscalTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = DadosFiscalQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            DadosFiscalTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                DadosFiscalTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the dados_fiscal table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return DadosFiscalQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a DadosFiscal or Criteria object.
     *
     * @param mixed               $criteria Criteria or DadosFiscal object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from DadosFiscal object
        }

        if ($criteria->containsKey(DadosFiscalTableMap::COL_ID) && $criteria->keyContainsValue(DadosFiscalTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.DadosFiscalTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = DadosFiscalQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // DadosFiscalTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
DadosFiscalTableMap::buildTableMap();
