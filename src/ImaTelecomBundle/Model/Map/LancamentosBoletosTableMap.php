<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\LancamentosBoletos;
use ImaTelecomBundle\Model\LancamentosBoletosQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'lancamentos_boletos' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class LancamentosBoletosTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.LancamentosBoletosTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'lancamentos_boletos';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\LancamentosBoletos';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.LancamentosBoletos';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the idlancamentos_boletos field
     */
    const COL_IDLANCAMENTOS_BOLETOS = 'lancamentos_boletos.idlancamentos_boletos';

    /**
     * the column name for the lancamento_id field
     */
    const COL_LANCAMENTO_ID = 'lancamentos_boletos.lancamento_id';

    /**
     * the column name for the boleto_id field
     */
    const COL_BOLETO_ID = 'lancamentos_boletos.boleto_id';

    /**
     * the column name for the conteudo_linhas_lancamento_boleto field
     */
    const COL_CONTEUDO_LINHAS_LANCAMENTO_BOLETO = 'lancamentos_boletos.conteudo_linhas_lancamento_boleto';

    /**
     * the column name for the data_cadastrado field
     */
    const COL_DATA_CADASTRADO = 'lancamentos_boletos.data_cadastrado';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'lancamentos_boletos.data_alterado';

    /**
     * the column name for the data_geracao field
     */
    const COL_DATA_GERACAO = 'lancamentos_boletos.data_geracao';

    /**
     * the column name for the competencia_id field
     */
    const COL_COMPETENCIA_ID = 'lancamentos_boletos.competencia_id';

    /**
     * the column name for the convenio_id field
     */
    const COL_CONVENIO_ID = 'lancamentos_boletos.convenio_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdlancamentosBoletos', 'LancamentoId', 'BoletoId', 'ConteudoLinhasLancamentoBoleto', 'DataCadastrado', 'DataAlterado', 'DataGeracao', 'CompetenciaId', 'ConvenioId', ),
        self::TYPE_CAMELNAME     => array('idlancamentosBoletos', 'lancamentoId', 'boletoId', 'conteudoLinhasLancamentoBoleto', 'dataCadastrado', 'dataAlterado', 'dataGeracao', 'competenciaId', 'convenioId', ),
        self::TYPE_COLNAME       => array(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, LancamentosBoletosTableMap::COL_LANCAMENTO_ID, LancamentosBoletosTableMap::COL_BOLETO_ID, LancamentosBoletosTableMap::COL_CONTEUDO_LINHAS_LANCAMENTO_BOLETO, LancamentosBoletosTableMap::COL_DATA_CADASTRADO, LancamentosBoletosTableMap::COL_DATA_ALTERADO, LancamentosBoletosTableMap::COL_DATA_GERACAO, LancamentosBoletosTableMap::COL_COMPETENCIA_ID, LancamentosBoletosTableMap::COL_CONVENIO_ID, ),
        self::TYPE_FIELDNAME     => array('idlancamentos_boletos', 'lancamento_id', 'boleto_id', 'conteudo_linhas_lancamento_boleto', 'data_cadastrado', 'data_alterado', 'data_geracao', 'competencia_id', 'convenio_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdlancamentosBoletos' => 0, 'LancamentoId' => 1, 'BoletoId' => 2, 'ConteudoLinhasLancamentoBoleto' => 3, 'DataCadastrado' => 4, 'DataAlterado' => 5, 'DataGeracao' => 6, 'CompetenciaId' => 7, 'ConvenioId' => 8, ),
        self::TYPE_CAMELNAME     => array('idlancamentosBoletos' => 0, 'lancamentoId' => 1, 'boletoId' => 2, 'conteudoLinhasLancamentoBoleto' => 3, 'dataCadastrado' => 4, 'dataAlterado' => 5, 'dataGeracao' => 6, 'competenciaId' => 7, 'convenioId' => 8, ),
        self::TYPE_COLNAME       => array(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS => 0, LancamentosBoletosTableMap::COL_LANCAMENTO_ID => 1, LancamentosBoletosTableMap::COL_BOLETO_ID => 2, LancamentosBoletosTableMap::COL_CONTEUDO_LINHAS_LANCAMENTO_BOLETO => 3, LancamentosBoletosTableMap::COL_DATA_CADASTRADO => 4, LancamentosBoletosTableMap::COL_DATA_ALTERADO => 5, LancamentosBoletosTableMap::COL_DATA_GERACAO => 6, LancamentosBoletosTableMap::COL_COMPETENCIA_ID => 7, LancamentosBoletosTableMap::COL_CONVENIO_ID => 8, ),
        self::TYPE_FIELDNAME     => array('idlancamentos_boletos' => 0, 'lancamento_id' => 1, 'boleto_id' => 2, 'conteudo_linhas_lancamento_boleto' => 3, 'data_cadastrado' => 4, 'data_alterado' => 5, 'data_geracao' => 6, 'competencia_id' => 7, 'convenio_id' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('lancamentos_boletos');
        $this->setPhpName('LancamentosBoletos');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\LancamentosBoletos');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idlancamentos_boletos', 'IdlancamentosBoletos', 'INTEGER', true, 10, null);
        $this->addForeignKey('lancamento_id', 'LancamentoId', 'INTEGER', 'lancamentos', 'idlancamento', true, 10, null);
        $this->addForeignKey('boleto_id', 'BoletoId', 'INTEGER', 'boleto', 'idboleto', true, 10, null);
        $this->addColumn('conteudo_linhas_lancamento_boleto', 'ConteudoLinhasLancamentoBoleto', 'CLOB', true, null, null);
        $this->addColumn('data_cadastrado', 'DataCadastrado', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_geracao', 'DataGeracao', 'VARCHAR', true, 45, null);
        $this->addForeignKey('competencia_id', 'CompetenciaId', 'INTEGER', 'competencia', 'id', true, 10, null);
        $this->addForeignKey('convenio_id', 'ConvenioId', 'INTEGER', 'convenio', 'idconvenio', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, null, false);
        $this->addRelation('Competencia', '\\ImaTelecomBundle\\Model\\Competencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Convenio', '\\ImaTelecomBundle\\Model\\Convenio', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':convenio_id',
    1 => ':idconvenio',
  ),
), null, null, null, false);
        $this->addRelation('Lancamentos', '\\ImaTelecomBundle\\Model\\Lancamentos', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':lancamento_id',
    1 => ':idlancamento',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlancamentosBoletos', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlancamentosBoletos', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlancamentosBoletos', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlancamentosBoletos', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlancamentosBoletos', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlancamentosBoletos', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdlancamentosBoletos', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? LancamentosBoletosTableMap::CLASS_DEFAULT : LancamentosBoletosTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (LancamentosBoletos object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = LancamentosBoletosTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = LancamentosBoletosTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + LancamentosBoletosTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = LancamentosBoletosTableMap::OM_CLASS;
            /** @var LancamentosBoletos $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            LancamentosBoletosTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = LancamentosBoletosTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = LancamentosBoletosTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var LancamentosBoletos $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                LancamentosBoletosTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS);
            $criteria->addSelectColumn(LancamentosBoletosTableMap::COL_LANCAMENTO_ID);
            $criteria->addSelectColumn(LancamentosBoletosTableMap::COL_BOLETO_ID);
            $criteria->addSelectColumn(LancamentosBoletosTableMap::COL_CONTEUDO_LINHAS_LANCAMENTO_BOLETO);
            $criteria->addSelectColumn(LancamentosBoletosTableMap::COL_DATA_CADASTRADO);
            $criteria->addSelectColumn(LancamentosBoletosTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(LancamentosBoletosTableMap::COL_DATA_GERACAO);
            $criteria->addSelectColumn(LancamentosBoletosTableMap::COL_COMPETENCIA_ID);
            $criteria->addSelectColumn(LancamentosBoletosTableMap::COL_CONVENIO_ID);
        } else {
            $criteria->addSelectColumn($alias . '.idlancamentos_boletos');
            $criteria->addSelectColumn($alias . '.lancamento_id');
            $criteria->addSelectColumn($alias . '.boleto_id');
            $criteria->addSelectColumn($alias . '.conteudo_linhas_lancamento_boleto');
            $criteria->addSelectColumn($alias . '.data_cadastrado');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.data_geracao');
            $criteria->addSelectColumn($alias . '.competencia_id');
            $criteria->addSelectColumn($alias . '.convenio_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(LancamentosBoletosTableMap::DATABASE_NAME)->getTable(LancamentosBoletosTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(LancamentosBoletosTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(LancamentosBoletosTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new LancamentosBoletosTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a LancamentosBoletos or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or LancamentosBoletos object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosBoletosTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\LancamentosBoletos) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(LancamentosBoletosTableMap::DATABASE_NAME);
            $criteria->add(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS, (array) $values, Criteria::IN);
        }

        $query = LancamentosBoletosQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            LancamentosBoletosTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                LancamentosBoletosTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the lancamentos_boletos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return LancamentosBoletosQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a LancamentosBoletos or Criteria object.
     *
     * @param mixed               $criteria Criteria or LancamentosBoletos object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosBoletosTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from LancamentosBoletos object
        }

        if ($criteria->containsKey(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS) && $criteria->keyContainsValue(LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.LancamentosBoletosTableMap::COL_IDLANCAMENTOS_BOLETOS.')');
        }


        // Set the correct dbName
        $query = LancamentosBoletosQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // LancamentosBoletosTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
LancamentosBoletosTableMap::buildTableMap();
