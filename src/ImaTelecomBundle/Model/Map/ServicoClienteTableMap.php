<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\ServicoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'servico_cliente' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ServicoClienteTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ServicoClienteTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'servico_cliente';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\ServicoCliente';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.ServicoCliente';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the idservico_cliente field
     */
    const COL_IDSERVICO_CLIENTE = 'servico_cliente.idservico_cliente';

    /**
     * the column name for the servico_prestado_id field
     */
    const COL_SERVICO_PRESTADO_ID = 'servico_cliente.servico_prestado_id';

    /**
     * the column name for the cliente_id field
     */
    const COL_CLIENTE_ID = 'servico_cliente.cliente_id';

    /**
     * the column name for the cod_cesta field
     */
    const COL_COD_CESTA = 'servico_cliente.cod_cesta';

    /**
     * the column name for the data_contratado field
     */
    const COL_DATA_CONTRATADO = 'servico_cliente.data_contratado';

    /**
     * the column name for the data_cancelado field
     */
    const COL_DATA_CANCELADO = 'servico_cliente.data_cancelado';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'servico_cliente.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'servico_cliente.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'servico_cliente.usuario_alterado';

    /**
     * the column name for the import_id field
     */
    const COL_IMPORT_ID = 'servico_cliente.import_id';

    /**
     * the column name for the descricao_nota_fiscal field
     */
    const COL_DESCRICAO_NOTA_FISCAL = 'servico_cliente.descricao_nota_fiscal';

    /**
     * the column name for the contrato_id field
     */
    const COL_CONTRATO_ID = 'servico_cliente.contrato_id';

    /**
     * the column name for the tipo_periodo_referencia field
     */
    const COL_TIPO_PERIODO_REFERENCIA = 'servico_cliente.tipo_periodo_referencia';

    /**
     * the column name for the valor_desconto field
     */
    const COL_VALOR_DESCONTO = 'servico_cliente.valor_desconto';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'servico_cliente.valor';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdservicoCliente', 'ServicoPrestadoId', 'ClienteId', 'CodCesta', 'DataContratado', 'DataCancelado', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'ImportId', 'DescricaoNotaFiscal', 'ContratoId', 'TipoPeriodoReferencia', 'ValorDesconto', 'Valor', ),
        self::TYPE_CAMELNAME     => array('idservicoCliente', 'servicoPrestadoId', 'clienteId', 'codCesta', 'dataContratado', 'dataCancelado', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'importId', 'descricaoNotaFiscal', 'contratoId', 'tipoPeriodoReferencia', 'valorDesconto', 'valor', ),
        self::TYPE_COLNAME       => array(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID, ServicoClienteTableMap::COL_CLIENTE_ID, ServicoClienteTableMap::COL_COD_CESTA, ServicoClienteTableMap::COL_DATA_CONTRATADO, ServicoClienteTableMap::COL_DATA_CANCELADO, ServicoClienteTableMap::COL_DATA_CADASTRO, ServicoClienteTableMap::COL_DATA_ALTERADO, ServicoClienteTableMap::COL_USUARIO_ALTERADO, ServicoClienteTableMap::COL_IMPORT_ID, ServicoClienteTableMap::COL_DESCRICAO_NOTA_FISCAL, ServicoClienteTableMap::COL_CONTRATO_ID, ServicoClienteTableMap::COL_TIPO_PERIODO_REFERENCIA, ServicoClienteTableMap::COL_VALOR_DESCONTO, ServicoClienteTableMap::COL_VALOR, ),
        self::TYPE_FIELDNAME     => array('idservico_cliente', 'servico_prestado_id', 'cliente_id', 'cod_cesta', 'data_contratado', 'data_cancelado', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'import_id', 'descricao_nota_fiscal', 'contrato_id', 'tipo_periodo_referencia', 'valor_desconto', 'valor', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdservicoCliente' => 0, 'ServicoPrestadoId' => 1, 'ClienteId' => 2, 'CodCesta' => 3, 'DataContratado' => 4, 'DataCancelado' => 5, 'DataCadastro' => 6, 'DataAlterado' => 7, 'UsuarioAlterado' => 8, 'ImportId' => 9, 'DescricaoNotaFiscal' => 10, 'ContratoId' => 11, 'TipoPeriodoReferencia' => 12, 'ValorDesconto' => 13, 'Valor' => 14, ),
        self::TYPE_CAMELNAME     => array('idservicoCliente' => 0, 'servicoPrestadoId' => 1, 'clienteId' => 2, 'codCesta' => 3, 'dataContratado' => 4, 'dataCancelado' => 5, 'dataCadastro' => 6, 'dataAlterado' => 7, 'usuarioAlterado' => 8, 'importId' => 9, 'descricaoNotaFiscal' => 10, 'contratoId' => 11, 'tipoPeriodoReferencia' => 12, 'valorDesconto' => 13, 'valor' => 14, ),
        self::TYPE_COLNAME       => array(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE => 0, ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID => 1, ServicoClienteTableMap::COL_CLIENTE_ID => 2, ServicoClienteTableMap::COL_COD_CESTA => 3, ServicoClienteTableMap::COL_DATA_CONTRATADO => 4, ServicoClienteTableMap::COL_DATA_CANCELADO => 5, ServicoClienteTableMap::COL_DATA_CADASTRO => 6, ServicoClienteTableMap::COL_DATA_ALTERADO => 7, ServicoClienteTableMap::COL_USUARIO_ALTERADO => 8, ServicoClienteTableMap::COL_IMPORT_ID => 9, ServicoClienteTableMap::COL_DESCRICAO_NOTA_FISCAL => 10, ServicoClienteTableMap::COL_CONTRATO_ID => 11, ServicoClienteTableMap::COL_TIPO_PERIODO_REFERENCIA => 12, ServicoClienteTableMap::COL_VALOR_DESCONTO => 13, ServicoClienteTableMap::COL_VALOR => 14, ),
        self::TYPE_FIELDNAME     => array('idservico_cliente' => 0, 'servico_prestado_id' => 1, 'cliente_id' => 2, 'cod_cesta' => 3, 'data_contratado' => 4, 'data_cancelado' => 5, 'data_cadastro' => 6, 'data_alterado' => 7, 'usuario_alterado' => 8, 'import_id' => 9, 'descricao_nota_fiscal' => 10, 'contrato_id' => 11, 'tipo_periodo_referencia' => 12, 'valor_desconto' => 13, 'valor' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('servico_cliente');
        $this->setPhpName('ServicoCliente');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\ServicoCliente');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idservico_cliente', 'IdservicoCliente', 'INTEGER', true, 10, null);
        $this->addForeignKey('servico_prestado_id', 'ServicoPrestadoId', 'INTEGER', 'servico_prestado', 'idservico_prestado', true, 10, null);
        $this->addForeignKey('cliente_id', 'ClienteId', 'INTEGER', 'cliente', 'idcliente', true, 10, null);
        $this->addColumn('cod_cesta', 'CodCesta', 'INTEGER', false, 10, null);
        $this->addColumn('data_contratado', 'DataContratado', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_cancelado', 'DataCancelado', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
        $this->addColumn('import_id', 'ImportId', 'INTEGER', false, 10, null);
        $this->addColumn('descricao_nota_fiscal', 'DescricaoNotaFiscal', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('contrato_id', 'ContratoId', 'INTEGER', 'contrato', 'idcontrato', false, 10, null);
        $this->addColumn('tipo_periodo_referencia', 'TipoPeriodoReferencia', 'CHAR', true, null, 'corrente');
        $this->addColumn('valor_desconto', 'ValorDesconto', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor', 'Valor', 'DECIMAL', false, 10, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Cliente', '\\ImaTelecomBundle\\Model\\Cliente', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':cliente_id',
    1 => ':idcliente',
  ),
), null, null, null, false);
        $this->addRelation('Contrato', '\\ImaTelecomBundle\\Model\\Contrato', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':contrato_id',
    1 => ':idcontrato',
  ),
), null, null, null, false);
        $this->addRelation('ServicoPrestado', '\\ImaTelecomBundle\\Model\\ServicoPrestado', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':servico_prestado_id',
    1 => ':idservico_prestado',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('BoletoItem', '\\ImaTelecomBundle\\Model\\BoletoItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':servico_cliente_id',
    1 => ':idservico_cliente',
  ),
), null, null, 'BoletoItems', false);
        $this->addRelation('EnderecoServCliente', '\\ImaTelecomBundle\\Model\\EnderecoServCliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':servico_cliente_id',
    1 => ':idservico_cliente',
  ),
), null, null, 'EnderecoServClientes', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoCliente', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoCliente', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoCliente', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoCliente', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoCliente', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoCliente', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdservicoCliente', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ServicoClienteTableMap::CLASS_DEFAULT : ServicoClienteTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ServicoCliente object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ServicoClienteTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ServicoClienteTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ServicoClienteTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ServicoClienteTableMap::OM_CLASS;
            /** @var ServicoCliente $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ServicoClienteTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ServicoClienteTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ServicoClienteTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ServicoCliente $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ServicoClienteTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_SERVICO_PRESTADO_ID);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_CLIENTE_ID);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_COD_CESTA);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_DATA_CONTRATADO);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_DATA_CANCELADO);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_IMPORT_ID);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_DESCRICAO_NOTA_FISCAL);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_CONTRATO_ID);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_TIPO_PERIODO_REFERENCIA);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_VALOR_DESCONTO);
            $criteria->addSelectColumn(ServicoClienteTableMap::COL_VALOR);
        } else {
            $criteria->addSelectColumn($alias . '.idservico_cliente');
            $criteria->addSelectColumn($alias . '.servico_prestado_id');
            $criteria->addSelectColumn($alias . '.cliente_id');
            $criteria->addSelectColumn($alias . '.cod_cesta');
            $criteria->addSelectColumn($alias . '.data_contratado');
            $criteria->addSelectColumn($alias . '.data_cancelado');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.import_id');
            $criteria->addSelectColumn($alias . '.descricao_nota_fiscal');
            $criteria->addSelectColumn($alias . '.contrato_id');
            $criteria->addSelectColumn($alias . '.tipo_periodo_referencia');
            $criteria->addSelectColumn($alias . '.valor_desconto');
            $criteria->addSelectColumn($alias . '.valor');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ServicoClienteTableMap::DATABASE_NAME)->getTable(ServicoClienteTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ServicoClienteTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ServicoClienteTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ServicoClienteTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ServicoCliente or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ServicoCliente object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoClienteTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\ServicoCliente) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ServicoClienteTableMap::DATABASE_NAME);
            $criteria->add(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE, (array) $values, Criteria::IN);
        }

        $query = ServicoClienteQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ServicoClienteTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ServicoClienteTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the servico_cliente table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ServicoClienteQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ServicoCliente or Criteria object.
     *
     * @param mixed               $criteria Criteria or ServicoCliente object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoClienteTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ServicoCliente object
        }

        if ($criteria->containsKey(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE) && $criteria->keyContainsValue(ServicoClienteTableMap::COL_IDSERVICO_CLIENTE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ServicoClienteTableMap::COL_IDSERVICO_CLIENTE.')');
        }


        // Set the correct dbName
        $query = ServicoClienteQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ServicoClienteTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ServicoClienteTableMap::buildTableMap();
