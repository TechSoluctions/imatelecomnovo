<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Contrato;
use ImaTelecomBundle\Model\ContratoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'contrato' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ContratoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ContratoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'contrato';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Contrato';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Contrato';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 59;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 59;

    /**
     * the column name for the idcontrato field
     */
    const COL_IDCONTRATO = 'contrato.idcontrato';

    /**
     * the column name for the descricao field
     */
    const COL_DESCRICAO = 'contrato.descricao';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'contrato.valor';

    /**
     * the column name for the endereco_cliente_id field
     */
    const COL_ENDERECO_CLIENTE_ID = 'contrato.endereco_cliente_id';

    /**
     * the column name for the data_contratado field
     */
    const COL_DATA_CONTRATADO = 'contrato.data_contratado';

    /**
     * the column name for the data_cancelado field
     */
    const COL_DATA_CANCELADO = 'contrato.data_cancelado';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'contrato.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'contrato.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'contrato.usuario_alterado';

    /**
     * the column name for the cancelado field
     */
    const COL_CANCELADO = 'contrato.cancelado';

    /**
     * the column name for the cliente_id field
     */
    const COL_CLIENTE_ID = 'contrato.cliente_id';

    /**
     * the column name for the forma_pagamento_id field
     */
    const COL_FORMA_PAGAMENTO_ID = 'contrato.forma_pagamento_id';

    /**
     * the column name for the profile_pagamento_id field
     */
    const COL_PROFILE_PAGAMENTO_ID = 'contrato.profile_pagamento_id';

    /**
     * the column name for the vendedor_id field
     */
    const COL_VENDEDOR_ID = 'contrato.vendedor_id';

    /**
     * the column name for the renovado_ate field
     */
    const COL_RENOVADO_ATE = 'contrato.renovado_ate';

    /**
     * the column name for the primeiro_vencimento field
     */
    const COL_PRIMEIRO_VENCIMENTO = 'contrato.primeiro_vencimento';

    /**
     * the column name for the percentual_multa field
     */
    const COL_PERCENTUAL_MULTA = 'contrato.percentual_multa';

    /**
     * the column name for the percentual_acrescimo field
     */
    const COL_PERCENTUAL_ACRESCIMO = 'contrato.percentual_acrescimo';

    /**
     * the column name for the usuario_cancelamento field
     */
    const COL_USUARIO_CANCELAMENTO = 'contrato.usuario_cancelamento';

    /**
     * the column name for the motivo_cancelamento field
     */
    const COL_MOTIVO_CANCELAMENTO = 'contrato.motivo_cancelamento';

    /**
     * the column name for the agencia_debito field
     */
    const COL_AGENCIA_DEBITO = 'contrato.agencia_debito';

    /**
     * the column name for the id_cliente_banco field
     */
    const COL_ID_CLIENTE_BANCO = 'contrato.id_cliente_banco';

    /**
     * the column name for the emissao_nf_21 field
     */
    const COL_EMISSAO_NF_21 = 'contrato.emissao_nf_21';

    /**
     * the column name for the emissao_nf_pre field
     */
    const COL_EMISSAO_NF_PRE = 'contrato.emissao_nf_pre';

    /**
     * the column name for the ignora_renovacao_automatica field
     */
    const COL_IGNORA_RENOVACAO_AUTOMATICA = 'contrato.ignora_renovacao_automatica';

    /**
     * the column name for the comodato field
     */
    const COL_COMODATO = 'contrato.comodato';

    /**
     * the column name for the empresa_id field
     */
    const COL_EMPRESA_ID = 'contrato.empresa_id';

    /**
     * the column name for the descricao_comodato field
     */
    const COL_DESCRICAO_COMODATO = 'contrato.descricao_comodato';

    /**
     * the column name for the data_comodato field
     */
    const COL_DATA_COMODATO = 'contrato.data_comodato';

    /**
     * the column name for the obs_comodato field
     */
    const COL_OBS_COMODATO = 'contrato.obs_comodato';

    /**
     * the column name for the centrocusto field
     */
    const COL_CENTROCUSTO = 'contrato.centrocusto';

    /**
     * the column name for the unidade_negocio field
     */
    const COL_UNIDADE_NEGOCIO = 'contrato.unidade_negocio';

    /**
     * the column name for the valor_renovacao field
     */
    const COL_VALOR_RENOVACAO = 'contrato.valor_renovacao';

    /**
     * the column name for the modelo_nf field
     */
    const COL_MODELO_NF = 'contrato.modelo_nf';

    /**
     * the column name for the emissao_nfse field
     */
    const COL_EMISSAO_NFSE = 'contrato.emissao_nfse';

    /**
     * the column name for the suspenso field
     */
    const COL_SUSPENSO = 'contrato.suspenso';

    /**
     * the column name for the data_suspensao field
     */
    const COL_DATA_SUSPENSAO = 'contrato.data_suspensao';

    /**
     * the column name for the usuario_suspensao field
     */
    const COL_USUARIO_SUSPENSAO = 'contrato.usuario_suspensao';

    /**
     * the column name for the motivo_suspensao field
     */
    const COL_MOTIVO_SUSPENSAO = 'contrato.motivo_suspensao';

    /**
     * the column name for the versao field
     */
    const COL_VERSAO = 'contrato.versao';

    /**
     * the column name for the faturamento_automatico field
     */
    const COL_FATURAMENTO_AUTOMATICO = 'contrato.faturamento_automatico';

    /**
     * the column name for the regra_faturamento_id field
     */
    const COL_REGRA_FATURAMENTO_ID = 'contrato.regra_faturamento_id';

    /**
     * the column name for the endereco_nf_id field
     */
    const COL_ENDERECO_NF_ID = 'contrato.endereco_nf_id';

    /**
     * the column name for the endereco_nfse_id field
     */
    const COL_ENDERECO_NFSE_ID = 'contrato.endereco_nfse_id';

    /**
     * the column name for the obs_nf field
     */
    const COL_OBS_NF = 'contrato.obs_nf';

    /**
     * the column name for the tipo_faturamento_id field
     */
    const COL_TIPO_FATURAMENTO_ID = 'contrato.tipo_faturamento_id';

    /**
     * the column name for the tem_scm field
     */
    const COL_TEM_SCM = 'contrato.tem_scm';

    /**
     * the column name for the scm field
     */
    const COL_SCM = 'contrato.scm';

    /**
     * the column name for the tem_sva field
     */
    const COL_TEM_SVA = 'contrato.tem_sva';

    /**
     * the column name for the sva field
     */
    const COL_SVA = 'contrato.sva';

    /**
     * the column name for the forcar_valor field
     */
    const COL_FORCAR_VALOR = 'contrato.forcar_valor';

    /**
     * the column name for the descricao_scm field
     */
    const COL_DESCRICAO_SCM = 'contrato.descricao_scm';

    /**
     * the column name for the descricao_sva field
     */
    const COL_DESCRICAO_SVA = 'contrato.descricao_sva';

    /**
     * the column name for the tipo_scm field
     */
    const COL_TIPO_SCM = 'contrato.tipo_scm';

    /**
     * the column name for the tipo_sva field
     */
    const COL_TIPO_SVA = 'contrato.tipo_sva';

    /**
     * the column name for the gerar_cobranca field
     */
    const COL_GERAR_COBRANCA = 'contrato.gerar_cobranca';

    /**
     * the column name for the codigo_obra field
     */
    const COL_CODIGO_OBRA = 'contrato.codigo_obra';

    /**
     * the column name for the art field
     */
    const COL_ART = 'contrato.art';

    /**
     * the column name for the tem_reembolso field
     */
    const COL_TEM_REEMBOLSO = 'contrato.tem_reembolso';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idcontrato', 'Descricao', 'Valor', 'EnderecoClienteId', 'DataContratado', 'DataCancelado', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'Cancelado', 'ClienteId', 'FormaPagamentoId', 'ProfilePagamentoId', 'VendedorId', 'RenovadoAte', 'PrimeiroVencimento', 'PercentualMulta', 'PercentualAcrescimo', 'UsuarioCancelamento', 'MotivoCancelamento', 'AgenciaDebito', 'IdClienteBanco', 'EmissaoNf21', 'EmissaoNfPre', 'IgnoraRenovacaoAutomatica', 'Comodato', 'EmpresaId', 'DescricaoComodato', 'DataComodato', 'ObsComodato', 'Centrocusto', 'UnidadeNegocio', 'ValorRenovacao', 'ModeloNf', 'EmissaoNfse', 'Suspenso', 'DataSuspensao', 'UsuarioSuspensao', 'MotivoSuspensao', 'Versao', 'FaturamentoAutomatico', 'RegraFaturamentoId', 'EnderecoNfId', 'EnderecoNfseId', 'ObsNf', 'TipoFaturamentoId', 'TemScm', 'Scm', 'TemSva', 'Sva', 'ForcarValor', 'DescricaoScm', 'DescricaoSva', 'TipoScm', 'TipoSva', 'GerarCobranca', 'CodigoObra', 'Art', 'TemReembolso', ),
        self::TYPE_CAMELNAME     => array('idcontrato', 'descricao', 'valor', 'enderecoClienteId', 'dataContratado', 'dataCancelado', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'cancelado', 'clienteId', 'formaPagamentoId', 'profilePagamentoId', 'vendedorId', 'renovadoAte', 'primeiroVencimento', 'percentualMulta', 'percentualAcrescimo', 'usuarioCancelamento', 'motivoCancelamento', 'agenciaDebito', 'idClienteBanco', 'emissaoNf21', 'emissaoNfPre', 'ignoraRenovacaoAutomatica', 'comodato', 'empresaId', 'descricaoComodato', 'dataComodato', 'obsComodato', 'centrocusto', 'unidadeNegocio', 'valorRenovacao', 'modeloNf', 'emissaoNfse', 'suspenso', 'dataSuspensao', 'usuarioSuspensao', 'motivoSuspensao', 'versao', 'faturamentoAutomatico', 'regraFaturamentoId', 'enderecoNfId', 'enderecoNfseId', 'obsNf', 'tipoFaturamentoId', 'temScm', 'scm', 'temSva', 'sva', 'forcarValor', 'descricaoScm', 'descricaoSva', 'tipoScm', 'tipoSva', 'gerarCobranca', 'codigoObra', 'art', 'temReembolso', ),
        self::TYPE_COLNAME       => array(ContratoTableMap::COL_IDCONTRATO, ContratoTableMap::COL_DESCRICAO, ContratoTableMap::COL_VALOR, ContratoTableMap::COL_ENDERECO_CLIENTE_ID, ContratoTableMap::COL_DATA_CONTRATADO, ContratoTableMap::COL_DATA_CANCELADO, ContratoTableMap::COL_DATA_CADASTRO, ContratoTableMap::COL_DATA_ALTERADO, ContratoTableMap::COL_USUARIO_ALTERADO, ContratoTableMap::COL_CANCELADO, ContratoTableMap::COL_CLIENTE_ID, ContratoTableMap::COL_FORMA_PAGAMENTO_ID, ContratoTableMap::COL_PROFILE_PAGAMENTO_ID, ContratoTableMap::COL_VENDEDOR_ID, ContratoTableMap::COL_RENOVADO_ATE, ContratoTableMap::COL_PRIMEIRO_VENCIMENTO, ContratoTableMap::COL_PERCENTUAL_MULTA, ContratoTableMap::COL_PERCENTUAL_ACRESCIMO, ContratoTableMap::COL_USUARIO_CANCELAMENTO, ContratoTableMap::COL_MOTIVO_CANCELAMENTO, ContratoTableMap::COL_AGENCIA_DEBITO, ContratoTableMap::COL_ID_CLIENTE_BANCO, ContratoTableMap::COL_EMISSAO_NF_21, ContratoTableMap::COL_EMISSAO_NF_PRE, ContratoTableMap::COL_IGNORA_RENOVACAO_AUTOMATICA, ContratoTableMap::COL_COMODATO, ContratoTableMap::COL_EMPRESA_ID, ContratoTableMap::COL_DESCRICAO_COMODATO, ContratoTableMap::COL_DATA_COMODATO, ContratoTableMap::COL_OBS_COMODATO, ContratoTableMap::COL_CENTROCUSTO, ContratoTableMap::COL_UNIDADE_NEGOCIO, ContratoTableMap::COL_VALOR_RENOVACAO, ContratoTableMap::COL_MODELO_NF, ContratoTableMap::COL_EMISSAO_NFSE, ContratoTableMap::COL_SUSPENSO, ContratoTableMap::COL_DATA_SUSPENSAO, ContratoTableMap::COL_USUARIO_SUSPENSAO, ContratoTableMap::COL_MOTIVO_SUSPENSAO, ContratoTableMap::COL_VERSAO, ContratoTableMap::COL_FATURAMENTO_AUTOMATICO, ContratoTableMap::COL_REGRA_FATURAMENTO_ID, ContratoTableMap::COL_ENDERECO_NF_ID, ContratoTableMap::COL_ENDERECO_NFSE_ID, ContratoTableMap::COL_OBS_NF, ContratoTableMap::COL_TIPO_FATURAMENTO_ID, ContratoTableMap::COL_TEM_SCM, ContratoTableMap::COL_SCM, ContratoTableMap::COL_TEM_SVA, ContratoTableMap::COL_SVA, ContratoTableMap::COL_FORCAR_VALOR, ContratoTableMap::COL_DESCRICAO_SCM, ContratoTableMap::COL_DESCRICAO_SVA, ContratoTableMap::COL_TIPO_SCM, ContratoTableMap::COL_TIPO_SVA, ContratoTableMap::COL_GERAR_COBRANCA, ContratoTableMap::COL_CODIGO_OBRA, ContratoTableMap::COL_ART, ContratoTableMap::COL_TEM_REEMBOLSO, ),
        self::TYPE_FIELDNAME     => array('idcontrato', 'descricao', 'valor', 'endereco_cliente_id', 'data_contratado', 'data_cancelado', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'cancelado', 'cliente_id', 'forma_pagamento_id', 'profile_pagamento_id', 'vendedor_id', 'renovado_ate', 'primeiro_vencimento', 'percentual_multa', 'percentual_acrescimo', 'usuario_cancelamento', 'motivo_cancelamento', 'agencia_debito', 'id_cliente_banco', 'emissao_nf_21', 'emissao_nf_pre', 'ignora_renovacao_automatica', 'comodato', 'empresa_id', 'descricao_comodato', 'data_comodato', 'obs_comodato', 'centrocusto', 'unidade_negocio', 'valor_renovacao', 'modelo_nf', 'emissao_nfse', 'suspenso', 'data_suspensao', 'usuario_suspensao', 'motivo_suspensao', 'versao', 'faturamento_automatico', 'regra_faturamento_id', 'endereco_nf_id', 'endereco_nfse_id', 'obs_nf', 'tipo_faturamento_id', 'tem_scm', 'scm', 'tem_sva', 'sva', 'forcar_valor', 'descricao_scm', 'descricao_sva', 'tipo_scm', 'tipo_sva', 'gerar_cobranca', 'codigo_obra', 'art', 'tem_reembolso', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idcontrato' => 0, 'Descricao' => 1, 'Valor' => 2, 'EnderecoClienteId' => 3, 'DataContratado' => 4, 'DataCancelado' => 5, 'DataCadastro' => 6, 'DataAlterado' => 7, 'UsuarioAlterado' => 8, 'Cancelado' => 9, 'ClienteId' => 10, 'FormaPagamentoId' => 11, 'ProfilePagamentoId' => 12, 'VendedorId' => 13, 'RenovadoAte' => 14, 'PrimeiroVencimento' => 15, 'PercentualMulta' => 16, 'PercentualAcrescimo' => 17, 'UsuarioCancelamento' => 18, 'MotivoCancelamento' => 19, 'AgenciaDebito' => 20, 'IdClienteBanco' => 21, 'EmissaoNf21' => 22, 'EmissaoNfPre' => 23, 'IgnoraRenovacaoAutomatica' => 24, 'Comodato' => 25, 'EmpresaId' => 26, 'DescricaoComodato' => 27, 'DataComodato' => 28, 'ObsComodato' => 29, 'Centrocusto' => 30, 'UnidadeNegocio' => 31, 'ValorRenovacao' => 32, 'ModeloNf' => 33, 'EmissaoNfse' => 34, 'Suspenso' => 35, 'DataSuspensao' => 36, 'UsuarioSuspensao' => 37, 'MotivoSuspensao' => 38, 'Versao' => 39, 'FaturamentoAutomatico' => 40, 'RegraFaturamentoId' => 41, 'EnderecoNfId' => 42, 'EnderecoNfseId' => 43, 'ObsNf' => 44, 'TipoFaturamentoId' => 45, 'TemScm' => 46, 'Scm' => 47, 'TemSva' => 48, 'Sva' => 49, 'ForcarValor' => 50, 'DescricaoScm' => 51, 'DescricaoSva' => 52, 'TipoScm' => 53, 'TipoSva' => 54, 'GerarCobranca' => 55, 'CodigoObra' => 56, 'Art' => 57, 'TemReembolso' => 58, ),
        self::TYPE_CAMELNAME     => array('idcontrato' => 0, 'descricao' => 1, 'valor' => 2, 'enderecoClienteId' => 3, 'dataContratado' => 4, 'dataCancelado' => 5, 'dataCadastro' => 6, 'dataAlterado' => 7, 'usuarioAlterado' => 8, 'cancelado' => 9, 'clienteId' => 10, 'formaPagamentoId' => 11, 'profilePagamentoId' => 12, 'vendedorId' => 13, 'renovadoAte' => 14, 'primeiroVencimento' => 15, 'percentualMulta' => 16, 'percentualAcrescimo' => 17, 'usuarioCancelamento' => 18, 'motivoCancelamento' => 19, 'agenciaDebito' => 20, 'idClienteBanco' => 21, 'emissaoNf21' => 22, 'emissaoNfPre' => 23, 'ignoraRenovacaoAutomatica' => 24, 'comodato' => 25, 'empresaId' => 26, 'descricaoComodato' => 27, 'dataComodato' => 28, 'obsComodato' => 29, 'centrocusto' => 30, 'unidadeNegocio' => 31, 'valorRenovacao' => 32, 'modeloNf' => 33, 'emissaoNfse' => 34, 'suspenso' => 35, 'dataSuspensao' => 36, 'usuarioSuspensao' => 37, 'motivoSuspensao' => 38, 'versao' => 39, 'faturamentoAutomatico' => 40, 'regraFaturamentoId' => 41, 'enderecoNfId' => 42, 'enderecoNfseId' => 43, 'obsNf' => 44, 'tipoFaturamentoId' => 45, 'temScm' => 46, 'scm' => 47, 'temSva' => 48, 'sva' => 49, 'forcarValor' => 50, 'descricaoScm' => 51, 'descricaoSva' => 52, 'tipoScm' => 53, 'tipoSva' => 54, 'gerarCobranca' => 55, 'codigoObra' => 56, 'art' => 57, 'temReembolso' => 58, ),
        self::TYPE_COLNAME       => array(ContratoTableMap::COL_IDCONTRATO => 0, ContratoTableMap::COL_DESCRICAO => 1, ContratoTableMap::COL_VALOR => 2, ContratoTableMap::COL_ENDERECO_CLIENTE_ID => 3, ContratoTableMap::COL_DATA_CONTRATADO => 4, ContratoTableMap::COL_DATA_CANCELADO => 5, ContratoTableMap::COL_DATA_CADASTRO => 6, ContratoTableMap::COL_DATA_ALTERADO => 7, ContratoTableMap::COL_USUARIO_ALTERADO => 8, ContratoTableMap::COL_CANCELADO => 9, ContratoTableMap::COL_CLIENTE_ID => 10, ContratoTableMap::COL_FORMA_PAGAMENTO_ID => 11, ContratoTableMap::COL_PROFILE_PAGAMENTO_ID => 12, ContratoTableMap::COL_VENDEDOR_ID => 13, ContratoTableMap::COL_RENOVADO_ATE => 14, ContratoTableMap::COL_PRIMEIRO_VENCIMENTO => 15, ContratoTableMap::COL_PERCENTUAL_MULTA => 16, ContratoTableMap::COL_PERCENTUAL_ACRESCIMO => 17, ContratoTableMap::COL_USUARIO_CANCELAMENTO => 18, ContratoTableMap::COL_MOTIVO_CANCELAMENTO => 19, ContratoTableMap::COL_AGENCIA_DEBITO => 20, ContratoTableMap::COL_ID_CLIENTE_BANCO => 21, ContratoTableMap::COL_EMISSAO_NF_21 => 22, ContratoTableMap::COL_EMISSAO_NF_PRE => 23, ContratoTableMap::COL_IGNORA_RENOVACAO_AUTOMATICA => 24, ContratoTableMap::COL_COMODATO => 25, ContratoTableMap::COL_EMPRESA_ID => 26, ContratoTableMap::COL_DESCRICAO_COMODATO => 27, ContratoTableMap::COL_DATA_COMODATO => 28, ContratoTableMap::COL_OBS_COMODATO => 29, ContratoTableMap::COL_CENTROCUSTO => 30, ContratoTableMap::COL_UNIDADE_NEGOCIO => 31, ContratoTableMap::COL_VALOR_RENOVACAO => 32, ContratoTableMap::COL_MODELO_NF => 33, ContratoTableMap::COL_EMISSAO_NFSE => 34, ContratoTableMap::COL_SUSPENSO => 35, ContratoTableMap::COL_DATA_SUSPENSAO => 36, ContratoTableMap::COL_USUARIO_SUSPENSAO => 37, ContratoTableMap::COL_MOTIVO_SUSPENSAO => 38, ContratoTableMap::COL_VERSAO => 39, ContratoTableMap::COL_FATURAMENTO_AUTOMATICO => 40, ContratoTableMap::COL_REGRA_FATURAMENTO_ID => 41, ContratoTableMap::COL_ENDERECO_NF_ID => 42, ContratoTableMap::COL_ENDERECO_NFSE_ID => 43, ContratoTableMap::COL_OBS_NF => 44, ContratoTableMap::COL_TIPO_FATURAMENTO_ID => 45, ContratoTableMap::COL_TEM_SCM => 46, ContratoTableMap::COL_SCM => 47, ContratoTableMap::COL_TEM_SVA => 48, ContratoTableMap::COL_SVA => 49, ContratoTableMap::COL_FORCAR_VALOR => 50, ContratoTableMap::COL_DESCRICAO_SCM => 51, ContratoTableMap::COL_DESCRICAO_SVA => 52, ContratoTableMap::COL_TIPO_SCM => 53, ContratoTableMap::COL_TIPO_SVA => 54, ContratoTableMap::COL_GERAR_COBRANCA => 55, ContratoTableMap::COL_CODIGO_OBRA => 56, ContratoTableMap::COL_ART => 57, ContratoTableMap::COL_TEM_REEMBOLSO => 58, ),
        self::TYPE_FIELDNAME     => array('idcontrato' => 0, 'descricao' => 1, 'valor' => 2, 'endereco_cliente_id' => 3, 'data_contratado' => 4, 'data_cancelado' => 5, 'data_cadastro' => 6, 'data_alterado' => 7, 'usuario_alterado' => 8, 'cancelado' => 9, 'cliente_id' => 10, 'forma_pagamento_id' => 11, 'profile_pagamento_id' => 12, 'vendedor_id' => 13, 'renovado_ate' => 14, 'primeiro_vencimento' => 15, 'percentual_multa' => 16, 'percentual_acrescimo' => 17, 'usuario_cancelamento' => 18, 'motivo_cancelamento' => 19, 'agencia_debito' => 20, 'id_cliente_banco' => 21, 'emissao_nf_21' => 22, 'emissao_nf_pre' => 23, 'ignora_renovacao_automatica' => 24, 'comodato' => 25, 'empresa_id' => 26, 'descricao_comodato' => 27, 'data_comodato' => 28, 'obs_comodato' => 29, 'centrocusto' => 30, 'unidade_negocio' => 31, 'valor_renovacao' => 32, 'modelo_nf' => 33, 'emissao_nfse' => 34, 'suspenso' => 35, 'data_suspensao' => 36, 'usuario_suspensao' => 37, 'motivo_suspensao' => 38, 'versao' => 39, 'faturamento_automatico' => 40, 'regra_faturamento_id' => 41, 'endereco_nf_id' => 42, 'endereco_nfse_id' => 43, 'obs_nf' => 44, 'tipo_faturamento_id' => 45, 'tem_scm' => 46, 'scm' => 47, 'tem_sva' => 48, 'sva' => 49, 'forcar_valor' => 50, 'descricao_scm' => 51, 'descricao_sva' => 52, 'tipo_scm' => 53, 'tipo_sva' => 54, 'gerar_cobranca' => 55, 'codigo_obra' => 56, 'art' => 57, 'tem_reembolso' => 58, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('contrato');
        $this->setPhpName('Contrato');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Contrato');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idcontrato', 'Idcontrato', 'INTEGER', true, 10, null);
        $this->addColumn('descricao', 'Descricao', 'VARCHAR', false, 250, null);
        $this->addColumn('valor', 'Valor', 'DECIMAL', false, 10, null);
        $this->addForeignKey('endereco_cliente_id', 'EnderecoClienteId', 'INTEGER', 'endereco_cliente', 'idendereco_cliente', false, 10, null);
        $this->addColumn('data_contratado', 'DataContratado', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_cancelado', 'DataCancelado', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', false, null, null);
        $this->addColumn('usuario_alterado', 'UsuarioAlterado', 'INTEGER', false, 10, null);
        $this->addColumn('cancelado', 'Cancelado', 'BOOLEAN', false, 1, null);
        $this->addColumn('cliente_id', 'ClienteId', 'INTEGER', false, 10, null);
        $this->addColumn('forma_pagamento_id', 'FormaPagamentoId', 'INTEGER', false, 10, null);
        $this->addColumn('profile_pagamento_id', 'ProfilePagamentoId', 'INTEGER', false, 10, null);
        $this->addColumn('vendedor_id', 'VendedorId', 'INTEGER', false, 10, null);
        $this->addColumn('renovado_ate', 'RenovadoAte', 'DATE', false, null, null);
        $this->addColumn('primeiro_vencimento', 'PrimeiroVencimento', 'DATE', false, null, null);
        $this->addColumn('percentual_multa', 'PercentualMulta', 'INTEGER', false, 10, null);
        $this->addColumn('percentual_acrescimo', 'PercentualAcrescimo', 'INTEGER', false, 10, null);
        $this->addColumn('usuario_cancelamento', 'UsuarioCancelamento', 'INTEGER', false, 10, null);
        $this->addColumn('motivo_cancelamento', 'MotivoCancelamento', 'INTEGER', false, 10, null);
        $this->addColumn('agencia_debito', 'AgenciaDebito', 'VARCHAR', false, 45, null);
        $this->addColumn('id_cliente_banco', 'IdClienteBanco', 'VARCHAR', false, 45, null);
        $this->addColumn('emissao_nf_21', 'EmissaoNf21', 'BOOLEAN', false, 1, null);
        $this->addColumn('emissao_nf_pre', 'EmissaoNfPre', 'BOOLEAN', false, 1, null);
        $this->addColumn('ignora_renovacao_automatica', 'IgnoraRenovacaoAutomatica', 'BOOLEAN', false, 1, null);
        $this->addColumn('comodato', 'Comodato', 'INTEGER', false, 10, null);
        $this->addColumn('empresa_id', 'EmpresaId', 'INTEGER', false, 10, null);
        $this->addColumn('descricao_comodato', 'DescricaoComodato', 'LONGVARCHAR', false, null, null);
        $this->addColumn('data_comodato', 'DataComodato', 'DATE', false, null, null);
        $this->addColumn('obs_comodato', 'ObsComodato', 'LONGVARCHAR', false, null, null);
        $this->addColumn('centrocusto', 'Centrocusto', 'INTEGER', false, 10, null);
        $this->addColumn('unidade_negocio', 'UnidadeNegocio', 'INTEGER', false, 10, null);
        $this->addColumn('valor_renovacao', 'ValorRenovacao', 'DECIMAL', false, null, null);
        $this->addColumn('modelo_nf', 'ModeloNf', 'INTEGER', false, 10, null);
        $this->addColumn('emissao_nfse', 'EmissaoNfse', 'BOOLEAN', false, 1, null);
        $this->addColumn('suspenso', 'Suspenso', 'BOOLEAN', false, 1, null);
        $this->addColumn('data_suspensao', 'DataSuspensao', 'TIMESTAMP', false, null, null);
        $this->addColumn('usuario_suspensao', 'UsuarioSuspensao', 'INTEGER', false, 10, null);
        $this->addColumn('motivo_suspensao', 'MotivoSuspensao', 'INTEGER', false, 10, null);
        $this->addColumn('versao', 'Versao', 'INTEGER', false, 10, null);
        $this->addColumn('faturamento_automatico', 'FaturamentoAutomatico', 'BOOLEAN', false, 1, null);
        $this->addForeignKey('regra_faturamento_id', 'RegraFaturamentoId', 'INTEGER', 'regra_faturamento', 'idregra_faturamento', false, 10, null);
        $this->addColumn('endereco_nf_id', 'EnderecoNfId', 'INTEGER', false, 10, null);
        $this->addColumn('endereco_nfse_id', 'EnderecoNfseId', 'INTEGER', false, 10, null);
        $this->addColumn('obs_nf', 'ObsNf', 'LONGVARCHAR', false, null, null);
        $this->addColumn('tipo_faturamento_id', 'TipoFaturamentoId', 'INTEGER', false, 10, null);
        $this->addColumn('tem_scm', 'TemScm', 'BOOLEAN', false, 1, false);
        $this->addColumn('scm', 'Scm', 'VARCHAR', false, 15, null);
        $this->addColumn('tem_sva', 'TemSva', 'BOOLEAN', false, 1, false);
        $this->addColumn('sva', 'Sva', 'VARCHAR', false, 15, null);
        $this->addColumn('forcar_valor', 'ForcarValor', 'BOOLEAN', false, 1, false);
        $this->addColumn('descricao_scm', 'DescricaoScm', 'LONGVARCHAR', false, null, null);
        $this->addColumn('descricao_sva', 'DescricaoSva', 'LONGVARCHAR', false, null, null);
        $this->addColumn('tipo_scm', 'TipoScm', 'CHAR', false, null, null);
        $this->addColumn('tipo_sva', 'TipoSva', 'CHAR', false, null, null);
        $this->addColumn('gerar_cobranca', 'GerarCobranca', 'BOOLEAN', false, 1, false);
        $this->addColumn('codigo_obra', 'CodigoObra', 'VARCHAR', false, 45, null);
        $this->addColumn('art', 'Art', 'VARCHAR', false, 45, null);
        $this->addColumn('tem_reembolso', 'TemReembolso', 'BOOLEAN', false, 1, true);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('EnderecoCliente', '\\ImaTelecomBundle\\Model\\EnderecoCliente', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':endereco_cliente_id',
    1 => ':idendereco_cliente',
  ),
), null, null, null, false);
        $this->addRelation('RegraFaturamento', '\\ImaTelecomBundle\\Model\\RegraFaturamento', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':regra_faturamento_id',
    1 => ':idregra_faturamento',
  ),
), null, null, null, false);
        $this->addRelation('ServicoCliente', '\\ImaTelecomBundle\\Model\\ServicoCliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':contrato_id',
    1 => ':idcontrato',
  ),
), null, null, 'ServicoClientes', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrato', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrato', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrato', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrato', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrato', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrato', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idcontrato', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ContratoTableMap::CLASS_DEFAULT : ContratoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Contrato object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ContratoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ContratoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ContratoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ContratoTableMap::OM_CLASS;
            /** @var Contrato $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ContratoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ContratoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ContratoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Contrato $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ContratoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ContratoTableMap::COL_IDCONTRATO);
            $criteria->addSelectColumn(ContratoTableMap::COL_DESCRICAO);
            $criteria->addSelectColumn(ContratoTableMap::COL_VALOR);
            $criteria->addSelectColumn(ContratoTableMap::COL_ENDERECO_CLIENTE_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_DATA_CONTRATADO);
            $criteria->addSelectColumn(ContratoTableMap::COL_DATA_CANCELADO);
            $criteria->addSelectColumn(ContratoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(ContratoTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(ContratoTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(ContratoTableMap::COL_CANCELADO);
            $criteria->addSelectColumn(ContratoTableMap::COL_CLIENTE_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_FORMA_PAGAMENTO_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_PROFILE_PAGAMENTO_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_VENDEDOR_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_RENOVADO_ATE);
            $criteria->addSelectColumn(ContratoTableMap::COL_PRIMEIRO_VENCIMENTO);
            $criteria->addSelectColumn(ContratoTableMap::COL_PERCENTUAL_MULTA);
            $criteria->addSelectColumn(ContratoTableMap::COL_PERCENTUAL_ACRESCIMO);
            $criteria->addSelectColumn(ContratoTableMap::COL_USUARIO_CANCELAMENTO);
            $criteria->addSelectColumn(ContratoTableMap::COL_MOTIVO_CANCELAMENTO);
            $criteria->addSelectColumn(ContratoTableMap::COL_AGENCIA_DEBITO);
            $criteria->addSelectColumn(ContratoTableMap::COL_ID_CLIENTE_BANCO);
            $criteria->addSelectColumn(ContratoTableMap::COL_EMISSAO_NF_21);
            $criteria->addSelectColumn(ContratoTableMap::COL_EMISSAO_NF_PRE);
            $criteria->addSelectColumn(ContratoTableMap::COL_IGNORA_RENOVACAO_AUTOMATICA);
            $criteria->addSelectColumn(ContratoTableMap::COL_COMODATO);
            $criteria->addSelectColumn(ContratoTableMap::COL_EMPRESA_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_DESCRICAO_COMODATO);
            $criteria->addSelectColumn(ContratoTableMap::COL_DATA_COMODATO);
            $criteria->addSelectColumn(ContratoTableMap::COL_OBS_COMODATO);
            $criteria->addSelectColumn(ContratoTableMap::COL_CENTROCUSTO);
            $criteria->addSelectColumn(ContratoTableMap::COL_UNIDADE_NEGOCIO);
            $criteria->addSelectColumn(ContratoTableMap::COL_VALOR_RENOVACAO);
            $criteria->addSelectColumn(ContratoTableMap::COL_MODELO_NF);
            $criteria->addSelectColumn(ContratoTableMap::COL_EMISSAO_NFSE);
            $criteria->addSelectColumn(ContratoTableMap::COL_SUSPENSO);
            $criteria->addSelectColumn(ContratoTableMap::COL_DATA_SUSPENSAO);
            $criteria->addSelectColumn(ContratoTableMap::COL_USUARIO_SUSPENSAO);
            $criteria->addSelectColumn(ContratoTableMap::COL_MOTIVO_SUSPENSAO);
            $criteria->addSelectColumn(ContratoTableMap::COL_VERSAO);
            $criteria->addSelectColumn(ContratoTableMap::COL_FATURAMENTO_AUTOMATICO);
            $criteria->addSelectColumn(ContratoTableMap::COL_REGRA_FATURAMENTO_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_ENDERECO_NF_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_ENDERECO_NFSE_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_OBS_NF);
            $criteria->addSelectColumn(ContratoTableMap::COL_TIPO_FATURAMENTO_ID);
            $criteria->addSelectColumn(ContratoTableMap::COL_TEM_SCM);
            $criteria->addSelectColumn(ContratoTableMap::COL_SCM);
            $criteria->addSelectColumn(ContratoTableMap::COL_TEM_SVA);
            $criteria->addSelectColumn(ContratoTableMap::COL_SVA);
            $criteria->addSelectColumn(ContratoTableMap::COL_FORCAR_VALOR);
            $criteria->addSelectColumn(ContratoTableMap::COL_DESCRICAO_SCM);
            $criteria->addSelectColumn(ContratoTableMap::COL_DESCRICAO_SVA);
            $criteria->addSelectColumn(ContratoTableMap::COL_TIPO_SCM);
            $criteria->addSelectColumn(ContratoTableMap::COL_TIPO_SVA);
            $criteria->addSelectColumn(ContratoTableMap::COL_GERAR_COBRANCA);
            $criteria->addSelectColumn(ContratoTableMap::COL_CODIGO_OBRA);
            $criteria->addSelectColumn(ContratoTableMap::COL_ART);
            $criteria->addSelectColumn(ContratoTableMap::COL_TEM_REEMBOLSO);
        } else {
            $criteria->addSelectColumn($alias . '.idcontrato');
            $criteria->addSelectColumn($alias . '.descricao');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.endereco_cliente_id');
            $criteria->addSelectColumn($alias . '.data_contratado');
            $criteria->addSelectColumn($alias . '.data_cancelado');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.cancelado');
            $criteria->addSelectColumn($alias . '.cliente_id');
            $criteria->addSelectColumn($alias . '.forma_pagamento_id');
            $criteria->addSelectColumn($alias . '.profile_pagamento_id');
            $criteria->addSelectColumn($alias . '.vendedor_id');
            $criteria->addSelectColumn($alias . '.renovado_ate');
            $criteria->addSelectColumn($alias . '.primeiro_vencimento');
            $criteria->addSelectColumn($alias . '.percentual_multa');
            $criteria->addSelectColumn($alias . '.percentual_acrescimo');
            $criteria->addSelectColumn($alias . '.usuario_cancelamento');
            $criteria->addSelectColumn($alias . '.motivo_cancelamento');
            $criteria->addSelectColumn($alias . '.agencia_debito');
            $criteria->addSelectColumn($alias . '.id_cliente_banco');
            $criteria->addSelectColumn($alias . '.emissao_nf_21');
            $criteria->addSelectColumn($alias . '.emissao_nf_pre');
            $criteria->addSelectColumn($alias . '.ignora_renovacao_automatica');
            $criteria->addSelectColumn($alias . '.comodato');
            $criteria->addSelectColumn($alias . '.empresa_id');
            $criteria->addSelectColumn($alias . '.descricao_comodato');
            $criteria->addSelectColumn($alias . '.data_comodato');
            $criteria->addSelectColumn($alias . '.obs_comodato');
            $criteria->addSelectColumn($alias . '.centrocusto');
            $criteria->addSelectColumn($alias . '.unidade_negocio');
            $criteria->addSelectColumn($alias . '.valor_renovacao');
            $criteria->addSelectColumn($alias . '.modelo_nf');
            $criteria->addSelectColumn($alias . '.emissao_nfse');
            $criteria->addSelectColumn($alias . '.suspenso');
            $criteria->addSelectColumn($alias . '.data_suspensao');
            $criteria->addSelectColumn($alias . '.usuario_suspensao');
            $criteria->addSelectColumn($alias . '.motivo_suspensao');
            $criteria->addSelectColumn($alias . '.versao');
            $criteria->addSelectColumn($alias . '.faturamento_automatico');
            $criteria->addSelectColumn($alias . '.regra_faturamento_id');
            $criteria->addSelectColumn($alias . '.endereco_nf_id');
            $criteria->addSelectColumn($alias . '.endereco_nfse_id');
            $criteria->addSelectColumn($alias . '.obs_nf');
            $criteria->addSelectColumn($alias . '.tipo_faturamento_id');
            $criteria->addSelectColumn($alias . '.tem_scm');
            $criteria->addSelectColumn($alias . '.scm');
            $criteria->addSelectColumn($alias . '.tem_sva');
            $criteria->addSelectColumn($alias . '.sva');
            $criteria->addSelectColumn($alias . '.forcar_valor');
            $criteria->addSelectColumn($alias . '.descricao_scm');
            $criteria->addSelectColumn($alias . '.descricao_sva');
            $criteria->addSelectColumn($alias . '.tipo_scm');
            $criteria->addSelectColumn($alias . '.tipo_sva');
            $criteria->addSelectColumn($alias . '.gerar_cobranca');
            $criteria->addSelectColumn($alias . '.codigo_obra');
            $criteria->addSelectColumn($alias . '.art');
            $criteria->addSelectColumn($alias . '.tem_reembolso');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ContratoTableMap::DATABASE_NAME)->getTable(ContratoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ContratoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ContratoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ContratoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Contrato or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Contrato object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Contrato) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ContratoTableMap::DATABASE_NAME);
            $criteria->add(ContratoTableMap::COL_IDCONTRATO, (array) $values, Criteria::IN);
        }

        $query = ContratoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ContratoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ContratoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the contrato table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ContratoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Contrato or Criteria object.
     *
     * @param mixed               $criteria Criteria or Contrato object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Contrato object
        }

        if ($criteria->containsKey(ContratoTableMap::COL_IDCONTRATO) && $criteria->keyContainsValue(ContratoTableMap::COL_IDCONTRATO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ContratoTableMap::COL_IDCONTRATO.')');
        }


        // Set the correct dbName
        $query = ContratoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ContratoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ContratoTableMap::buildTableMap();
