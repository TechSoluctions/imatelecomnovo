<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Baixa;
use ImaTelecomBundle\Model\BaixaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'baixa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BaixaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.BaixaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'baixa';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Baixa';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Baixa';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the idbaixa field
     */
    const COL_IDBAIXA = 'baixa.idbaixa';

    /**
     * the column name for the conta_caixa_id field
     */
    const COL_CONTA_CAIXA_ID = 'baixa.conta_caixa_id';

    /**
     * the column name for the competencia_id field
     */
    const COL_COMPETENCIA_ID = 'baixa.competencia_id';

    /**
     * the column name for the usuario_baixa field
     */
    const COL_USUARIO_BAIXA = 'baixa.usuario_baixa';

    /**
     * the column name for the data_baixa field
     */
    const COL_DATA_BAIXA = 'baixa.data_baixa';

    /**
     * the column name for the data_pagamento field
     */
    const COL_DATA_PAGAMENTO = 'baixa.data_pagamento';

    /**
     * the column name for the descricao_movimento field
     */
    const COL_DESCRICAO_MOVIMENTO = 'baixa.descricao_movimento';

    /**
     * the column name for the valor_original field
     */
    const COL_VALOR_ORIGINAL = 'baixa.valor_original';

    /**
     * the column name for the valor_multa field
     */
    const COL_VALOR_MULTA = 'baixa.valor_multa';

    /**
     * the column name for the valor_juros field
     */
    const COL_VALOR_JUROS = 'baixa.valor_juros';

    /**
     * the column name for the valor_desconto field
     */
    const COL_VALOR_DESCONTO = 'baixa.valor_desconto';

    /**
     * the column name for the valor_total field
     */
    const COL_VALOR_TOTAL = 'baixa.valor_total';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'baixa.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'baixa.data_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idbaixa', 'ContaCaixaId', 'CompetenciaId', 'UsuarioBaixa', 'DataBaixa', 'DataPagamento', 'DescricaoMovimento', 'ValorOriginal', 'ValorMulta', 'ValorJuros', 'ValorDesconto', 'ValorTotal', 'DataCadastro', 'DataAlterado', ),
        self::TYPE_CAMELNAME     => array('idbaixa', 'contaCaixaId', 'competenciaId', 'usuarioBaixa', 'dataBaixa', 'dataPagamento', 'descricaoMovimento', 'valorOriginal', 'valorMulta', 'valorJuros', 'valorDesconto', 'valorTotal', 'dataCadastro', 'dataAlterado', ),
        self::TYPE_COLNAME       => array(BaixaTableMap::COL_IDBAIXA, BaixaTableMap::COL_CONTA_CAIXA_ID, BaixaTableMap::COL_COMPETENCIA_ID, BaixaTableMap::COL_USUARIO_BAIXA, BaixaTableMap::COL_DATA_BAIXA, BaixaTableMap::COL_DATA_PAGAMENTO, BaixaTableMap::COL_DESCRICAO_MOVIMENTO, BaixaTableMap::COL_VALOR_ORIGINAL, BaixaTableMap::COL_VALOR_MULTA, BaixaTableMap::COL_VALOR_JUROS, BaixaTableMap::COL_VALOR_DESCONTO, BaixaTableMap::COL_VALOR_TOTAL, BaixaTableMap::COL_DATA_CADASTRO, BaixaTableMap::COL_DATA_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idbaixa', 'conta_caixa_id', 'competencia_id', 'usuario_baixa', 'data_baixa', 'data_pagamento', 'descricao_movimento', 'valor_original', 'valor_multa', 'valor_juros', 'valor_desconto', 'valor_total', 'data_cadastro', 'data_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idbaixa' => 0, 'ContaCaixaId' => 1, 'CompetenciaId' => 2, 'UsuarioBaixa' => 3, 'DataBaixa' => 4, 'DataPagamento' => 5, 'DescricaoMovimento' => 6, 'ValorOriginal' => 7, 'ValorMulta' => 8, 'ValorJuros' => 9, 'ValorDesconto' => 10, 'ValorTotal' => 11, 'DataCadastro' => 12, 'DataAlterado' => 13, ),
        self::TYPE_CAMELNAME     => array('idbaixa' => 0, 'contaCaixaId' => 1, 'competenciaId' => 2, 'usuarioBaixa' => 3, 'dataBaixa' => 4, 'dataPagamento' => 5, 'descricaoMovimento' => 6, 'valorOriginal' => 7, 'valorMulta' => 8, 'valorJuros' => 9, 'valorDesconto' => 10, 'valorTotal' => 11, 'dataCadastro' => 12, 'dataAlterado' => 13, ),
        self::TYPE_COLNAME       => array(BaixaTableMap::COL_IDBAIXA => 0, BaixaTableMap::COL_CONTA_CAIXA_ID => 1, BaixaTableMap::COL_COMPETENCIA_ID => 2, BaixaTableMap::COL_USUARIO_BAIXA => 3, BaixaTableMap::COL_DATA_BAIXA => 4, BaixaTableMap::COL_DATA_PAGAMENTO => 5, BaixaTableMap::COL_DESCRICAO_MOVIMENTO => 6, BaixaTableMap::COL_VALOR_ORIGINAL => 7, BaixaTableMap::COL_VALOR_MULTA => 8, BaixaTableMap::COL_VALOR_JUROS => 9, BaixaTableMap::COL_VALOR_DESCONTO => 10, BaixaTableMap::COL_VALOR_TOTAL => 11, BaixaTableMap::COL_DATA_CADASTRO => 12, BaixaTableMap::COL_DATA_ALTERADO => 13, ),
        self::TYPE_FIELDNAME     => array('idbaixa' => 0, 'conta_caixa_id' => 1, 'competencia_id' => 2, 'usuario_baixa' => 3, 'data_baixa' => 4, 'data_pagamento' => 5, 'descricao_movimento' => 6, 'valor_original' => 7, 'valor_multa' => 8, 'valor_juros' => 9, 'valor_desconto' => 10, 'valor_total' => 11, 'data_cadastro' => 12, 'data_alterado' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('baixa');
        $this->setPhpName('Baixa');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Baixa');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idbaixa', 'Idbaixa', 'INTEGER', true, null, null);
        $this->addForeignKey('conta_caixa_id', 'ContaCaixaId', 'INTEGER', 'conta_caixa', 'idconta_caixa', true, null, null);
        $this->addForeignKey('competencia_id', 'CompetenciaId', 'INTEGER', 'competencia', 'id', true, null, null);
        $this->addForeignKey('usuario_baixa', 'UsuarioBaixa', 'INTEGER', 'usuario', 'idusuario', true, null, null);
        $this->addColumn('data_baixa', 'DataBaixa', 'DATE', true, null, null);
        $this->addColumn('data_pagamento', 'DataPagamento', 'TIMESTAMP', true, null, null);
        $this->addColumn('descricao_movimento', 'DescricaoMovimento', 'LONGVARCHAR', false, null, null);
        $this->addColumn('valor_original', 'ValorOriginal', 'DECIMAL', true, 10, 0);
        $this->addColumn('valor_multa', 'ValorMulta', 'DECIMAL', true, 10, 0);
        $this->addColumn('valor_juros', 'ValorJuros', 'DECIMAL', true, 10, 0);
        $this->addColumn('valor_desconto', 'ValorDesconto', 'DECIMAL', true, 10, 0);
        $this->addColumn('valor_total', 'ValorTotal', 'DECIMAL', true, 10, 0);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Competencia', '\\ImaTelecomBundle\\Model\\Competencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('ContaCaixa', '\\ImaTelecomBundle\\Model\\ContaCaixa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':conta_caixa_id',
    1 => ':idconta_caixa',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_baixa',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('BaixaEstorno', '\\ImaTelecomBundle\\Model\\BaixaEstorno', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':baixa_id',
    1 => ':idbaixa',
  ),
), null, null, 'BaixaEstornos', false);
        $this->addRelation('BaixaMovimento', '\\ImaTelecomBundle\\Model\\BaixaMovimento', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':baixa_id',
    1 => ':idbaixa',
  ),
), null, null, 'BaixaMovimentos', false);
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':baixa_id',
    1 => ':idbaixa',
  ),
), null, null, 'Boletos', false);
        $this->addRelation('BoletoBaixaHistorico', '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':baixa_id',
    1 => ':idbaixa',
  ),
), null, null, 'BoletoBaixaHistoricos', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbaixa', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbaixa', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbaixa', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbaixa', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbaixa', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbaixa', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idbaixa', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BaixaTableMap::CLASS_DEFAULT : BaixaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Baixa object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BaixaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BaixaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BaixaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BaixaTableMap::OM_CLASS;
            /** @var Baixa $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BaixaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BaixaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BaixaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Baixa $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BaixaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BaixaTableMap::COL_IDBAIXA);
            $criteria->addSelectColumn(BaixaTableMap::COL_CONTA_CAIXA_ID);
            $criteria->addSelectColumn(BaixaTableMap::COL_COMPETENCIA_ID);
            $criteria->addSelectColumn(BaixaTableMap::COL_USUARIO_BAIXA);
            $criteria->addSelectColumn(BaixaTableMap::COL_DATA_BAIXA);
            $criteria->addSelectColumn(BaixaTableMap::COL_DATA_PAGAMENTO);
            $criteria->addSelectColumn(BaixaTableMap::COL_DESCRICAO_MOVIMENTO);
            $criteria->addSelectColumn(BaixaTableMap::COL_VALOR_ORIGINAL);
            $criteria->addSelectColumn(BaixaTableMap::COL_VALOR_MULTA);
            $criteria->addSelectColumn(BaixaTableMap::COL_VALOR_JUROS);
            $criteria->addSelectColumn(BaixaTableMap::COL_VALOR_DESCONTO);
            $criteria->addSelectColumn(BaixaTableMap::COL_VALOR_TOTAL);
            $criteria->addSelectColumn(BaixaTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(BaixaTableMap::COL_DATA_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idbaixa');
            $criteria->addSelectColumn($alias . '.conta_caixa_id');
            $criteria->addSelectColumn($alias . '.competencia_id');
            $criteria->addSelectColumn($alias . '.usuario_baixa');
            $criteria->addSelectColumn($alias . '.data_baixa');
            $criteria->addSelectColumn($alias . '.data_pagamento');
            $criteria->addSelectColumn($alias . '.descricao_movimento');
            $criteria->addSelectColumn($alias . '.valor_original');
            $criteria->addSelectColumn($alias . '.valor_multa');
            $criteria->addSelectColumn($alias . '.valor_juros');
            $criteria->addSelectColumn($alias . '.valor_desconto');
            $criteria->addSelectColumn($alias . '.valor_total');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BaixaTableMap::DATABASE_NAME)->getTable(BaixaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BaixaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BaixaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BaixaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Baixa or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Baixa object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Baixa) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BaixaTableMap::DATABASE_NAME);
            $criteria->add(BaixaTableMap::COL_IDBAIXA, (array) $values, Criteria::IN);
        }

        $query = BaixaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BaixaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BaixaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the baixa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BaixaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Baixa or Criteria object.
     *
     * @param mixed               $criteria Criteria or Baixa object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Baixa object
        }

        if ($criteria->containsKey(BaixaTableMap::COL_IDBAIXA) && $criteria->keyContainsValue(BaixaTableMap::COL_IDBAIXA) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BaixaTableMap::COL_IDBAIXA.')');
        }


        // Set the correct dbName
        $query = BaixaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BaixaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaixaTableMap::buildTableMap();
