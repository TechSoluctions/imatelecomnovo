<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\BancoAgencia;
use ImaTelecomBundle\Model\BancoAgenciaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'banco_agencia' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BancoAgenciaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.BancoAgenciaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'banco_agencia';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\BancoAgencia';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.BancoAgencia';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the idbanco_agencia field
     */
    const COL_IDBANCO_AGENCIA = 'banco_agencia.idbanco_agencia';

    /**
     * the column name for the agencia field
     */
    const COL_AGENCIA = 'banco_agencia.agencia';

    /**
     * the column name for the digito_verificador field
     */
    const COL_DIGITO_VERIFICADOR = 'banco_agencia.digito_verificador';

    /**
     * the column name for the nome field
     */
    const COL_NOME = 'banco_agencia.nome';

    /**
     * the column name for the endereco_agencia_id field
     */
    const COL_ENDERECO_AGENCIA_ID = 'banco_agencia.endereco_agencia_id';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'banco_agencia.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'banco_agencia.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'banco_agencia.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdbancoAgencia', 'Agencia', 'DigitoVerificador', 'Nome', 'EnderecoAgenciaId', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idbancoAgencia', 'agencia', 'digitoVerificador', 'nome', 'enderecoAgenciaId', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA, BancoAgenciaTableMap::COL_AGENCIA, BancoAgenciaTableMap::COL_DIGITO_VERIFICADOR, BancoAgenciaTableMap::COL_NOME, BancoAgenciaTableMap::COL_ENDERECO_AGENCIA_ID, BancoAgenciaTableMap::COL_DATA_CADASTRO, BancoAgenciaTableMap::COL_DATA_ALTERADO, BancoAgenciaTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idbanco_agencia', 'agencia', 'digito_verificador', 'nome', 'endereco_agencia_id', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdbancoAgencia' => 0, 'Agencia' => 1, 'DigitoVerificador' => 2, 'Nome' => 3, 'EnderecoAgenciaId' => 4, 'DataCadastro' => 5, 'DataAlterado' => 6, 'UsuarioAlterado' => 7, ),
        self::TYPE_CAMELNAME     => array('idbancoAgencia' => 0, 'agencia' => 1, 'digitoVerificador' => 2, 'nome' => 3, 'enderecoAgenciaId' => 4, 'dataCadastro' => 5, 'dataAlterado' => 6, 'usuarioAlterado' => 7, ),
        self::TYPE_COLNAME       => array(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA => 0, BancoAgenciaTableMap::COL_AGENCIA => 1, BancoAgenciaTableMap::COL_DIGITO_VERIFICADOR => 2, BancoAgenciaTableMap::COL_NOME => 3, BancoAgenciaTableMap::COL_ENDERECO_AGENCIA_ID => 4, BancoAgenciaTableMap::COL_DATA_CADASTRO => 5, BancoAgenciaTableMap::COL_DATA_ALTERADO => 6, BancoAgenciaTableMap::COL_USUARIO_ALTERADO => 7, ),
        self::TYPE_FIELDNAME     => array('idbanco_agencia' => 0, 'agencia' => 1, 'digito_verificador' => 2, 'nome' => 3, 'endereco_agencia_id' => 4, 'data_cadastro' => 5, 'data_alterado' => 6, 'usuario_alterado' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('banco_agencia');
        $this->setPhpName('BancoAgencia');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\BancoAgencia');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idbanco_agencia', 'IdbancoAgencia', 'INTEGER', true, 10, null);
        $this->addColumn('agencia', 'Agencia', 'VARCHAR', true, 10, null);
        $this->addColumn('digito_verificador', 'DigitoVerificador', 'VARCHAR', true, 2, null);
        $this->addColumn('nome', 'Nome', 'VARCHAR', true, 45, null);
        $this->addForeignKey('endereco_agencia_id', 'EnderecoAgenciaId', 'INTEGER', 'endereco_agencia', 'idendereco_agencia', true, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('EnderecoAgencia', '\\ImaTelecomBundle\\Model\\EnderecoAgencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':endereco_agencia_id',
    1 => ':idendereco_agencia',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('BancoAgenciaConta', '\\ImaTelecomBundle\\Model\\BancoAgenciaConta', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':banco_agencia_id',
    1 => ':idbanco_agencia',
  ),
), null, null, 'BancoAgenciaContas', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgencia', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgencia', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgencia', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgencia', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgencia', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgencia', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdbancoAgencia', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BancoAgenciaTableMap::CLASS_DEFAULT : BancoAgenciaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BancoAgencia object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BancoAgenciaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BancoAgenciaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BancoAgenciaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BancoAgenciaTableMap::OM_CLASS;
            /** @var BancoAgencia $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BancoAgenciaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BancoAgenciaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BancoAgenciaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BancoAgencia $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BancoAgenciaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA);
            $criteria->addSelectColumn(BancoAgenciaTableMap::COL_AGENCIA);
            $criteria->addSelectColumn(BancoAgenciaTableMap::COL_DIGITO_VERIFICADOR);
            $criteria->addSelectColumn(BancoAgenciaTableMap::COL_NOME);
            $criteria->addSelectColumn(BancoAgenciaTableMap::COL_ENDERECO_AGENCIA_ID);
            $criteria->addSelectColumn(BancoAgenciaTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(BancoAgenciaTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(BancoAgenciaTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idbanco_agencia');
            $criteria->addSelectColumn($alias . '.agencia');
            $criteria->addSelectColumn($alias . '.digito_verificador');
            $criteria->addSelectColumn($alias . '.nome');
            $criteria->addSelectColumn($alias . '.endereco_agencia_id');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BancoAgenciaTableMap::DATABASE_NAME)->getTable(BancoAgenciaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BancoAgenciaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BancoAgenciaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BancoAgenciaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BancoAgencia or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BancoAgencia object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\BancoAgencia) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BancoAgenciaTableMap::DATABASE_NAME);
            $criteria->add(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA, (array) $values, Criteria::IN);
        }

        $query = BancoAgenciaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BancoAgenciaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BancoAgenciaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the banco_agencia table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BancoAgenciaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BancoAgencia or Criteria object.
     *
     * @param mixed               $criteria Criteria or BancoAgencia object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BancoAgencia object
        }

        if ($criteria->containsKey(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA) && $criteria->keyContainsValue(BancoAgenciaTableMap::COL_IDBANCO_AGENCIA) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BancoAgenciaTableMap::COL_IDBANCO_AGENCIA.')');
        }


        // Set the correct dbName
        $query = BancoAgenciaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BancoAgenciaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BancoAgenciaTableMap::buildTableMap();
