<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\ServicoPrestado;
use ImaTelecomBundle\Model\ServicoPrestadoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'servico_prestado' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ServicoPrestadoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ServicoPrestadoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'servico_prestado';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\ServicoPrestado';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.ServicoPrestado';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the idservico_prestado field
     */
    const COL_IDSERVICO_PRESTADO = 'servico_prestado.idservico_prestado';

    /**
     * the column name for the nome field
     */
    const COL_NOME = 'servico_prestado.nome';

    /**
     * the column name for the codigo field
     */
    const COL_CODIGO = 'servico_prestado.codigo';

    /**
     * the column name for the download field
     */
    const COL_DOWNLOAD = 'servico_prestado.download';

    /**
     * the column name for the upload field
     */
    const COL_UPLOAD = 'servico_prestado.upload';

    /**
     * the column name for the franquia field
     */
    const COL_FRANQUIA = 'servico_prestado.franquia';

    /**
     * the column name for the tecnologia field
     */
    const COL_TECNOLOGIA = 'servico_prestado.tecnologia';

    /**
     * the column name for the garantia_banda field
     */
    const COL_GARANTIA_BANDA = 'servico_prestado.garantia_banda';

    /**
     * the column name for the ativo field
     */
    const COL_ATIVO = 'servico_prestado.ativo';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'servico_prestado.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'servico_prestado.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'servico_prestado.usuario_alterado';

    /**
     * the column name for the pre_pago field
     */
    const COL_PRE_PAGO = 'servico_prestado.pre_pago';

    /**
     * the column name for the plano_id field
     */
    const COL_PLANO_ID = 'servico_prestado.plano_id';

    /**
     * the column name for the tipo field
     */
    const COL_TIPO = 'servico_prestado.tipo';

    /**
     * the column name for the tipo_servico_prestado_id field
     */
    const COL_TIPO_SERVICO_PRESTADO_ID = 'servico_prestado.tipo_servico_prestado_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdservicoPrestado', 'Nome', 'Codigo', 'Download', 'Upload', 'Franquia', 'Tecnologia', 'GarantiaBanda', 'Ativo', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'PrePago', 'PlanoId', 'Tipo', 'TipoServicoPrestadoId', ),
        self::TYPE_CAMELNAME     => array('idservicoPrestado', 'nome', 'codigo', 'download', 'upload', 'franquia', 'tecnologia', 'garantiaBanda', 'ativo', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'prePago', 'planoId', 'tipo', 'tipoServicoPrestadoId', ),
        self::TYPE_COLNAME       => array(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, ServicoPrestadoTableMap::COL_NOME, ServicoPrestadoTableMap::COL_CODIGO, ServicoPrestadoTableMap::COL_DOWNLOAD, ServicoPrestadoTableMap::COL_UPLOAD, ServicoPrestadoTableMap::COL_FRANQUIA, ServicoPrestadoTableMap::COL_TECNOLOGIA, ServicoPrestadoTableMap::COL_GARANTIA_BANDA, ServicoPrestadoTableMap::COL_ATIVO, ServicoPrestadoTableMap::COL_DATA_CADASTRO, ServicoPrestadoTableMap::COL_DATA_ALTERADO, ServicoPrestadoTableMap::COL_USUARIO_ALTERADO, ServicoPrestadoTableMap::COL_PRE_PAGO, ServicoPrestadoTableMap::COL_PLANO_ID, ServicoPrestadoTableMap::COL_TIPO, ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID, ),
        self::TYPE_FIELDNAME     => array('idservico_prestado', 'nome', 'codigo', 'download', 'upload', 'franquia', 'tecnologia', 'garantia_banda', 'ativo', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'pre_pago', 'plano_id', 'tipo', 'tipo_servico_prestado_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdservicoPrestado' => 0, 'Nome' => 1, 'Codigo' => 2, 'Download' => 3, 'Upload' => 4, 'Franquia' => 5, 'Tecnologia' => 6, 'GarantiaBanda' => 7, 'Ativo' => 8, 'DataCadastro' => 9, 'DataAlterado' => 10, 'UsuarioAlterado' => 11, 'PrePago' => 12, 'PlanoId' => 13, 'Tipo' => 14, 'TipoServicoPrestadoId' => 15, ),
        self::TYPE_CAMELNAME     => array('idservicoPrestado' => 0, 'nome' => 1, 'codigo' => 2, 'download' => 3, 'upload' => 4, 'franquia' => 5, 'tecnologia' => 6, 'garantiaBanda' => 7, 'ativo' => 8, 'dataCadastro' => 9, 'dataAlterado' => 10, 'usuarioAlterado' => 11, 'prePago' => 12, 'planoId' => 13, 'tipo' => 14, 'tipoServicoPrestadoId' => 15, ),
        self::TYPE_COLNAME       => array(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO => 0, ServicoPrestadoTableMap::COL_NOME => 1, ServicoPrestadoTableMap::COL_CODIGO => 2, ServicoPrestadoTableMap::COL_DOWNLOAD => 3, ServicoPrestadoTableMap::COL_UPLOAD => 4, ServicoPrestadoTableMap::COL_FRANQUIA => 5, ServicoPrestadoTableMap::COL_TECNOLOGIA => 6, ServicoPrestadoTableMap::COL_GARANTIA_BANDA => 7, ServicoPrestadoTableMap::COL_ATIVO => 8, ServicoPrestadoTableMap::COL_DATA_CADASTRO => 9, ServicoPrestadoTableMap::COL_DATA_ALTERADO => 10, ServicoPrestadoTableMap::COL_USUARIO_ALTERADO => 11, ServicoPrestadoTableMap::COL_PRE_PAGO => 12, ServicoPrestadoTableMap::COL_PLANO_ID => 13, ServicoPrestadoTableMap::COL_TIPO => 14, ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID => 15, ),
        self::TYPE_FIELDNAME     => array('idservico_prestado' => 0, 'nome' => 1, 'codigo' => 2, 'download' => 3, 'upload' => 4, 'franquia' => 5, 'tecnologia' => 6, 'garantia_banda' => 7, 'ativo' => 8, 'data_cadastro' => 9, 'data_alterado' => 10, 'usuario_alterado' => 11, 'pre_pago' => 12, 'plano_id' => 13, 'tipo' => 14, 'tipo_servico_prestado_id' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('servico_prestado');
        $this->setPhpName('ServicoPrestado');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\ServicoPrestado');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idservico_prestado', 'IdservicoPrestado', 'INTEGER', true, 10, null);
        $this->addColumn('nome', 'Nome', 'VARCHAR', true, 45, null);
        $this->addColumn('codigo', 'Codigo', 'VARCHAR', false, 6, null);
        $this->addColumn('download', 'Download', 'INTEGER', true, 10, 0);
        $this->addColumn('upload', 'Upload', 'INTEGER', true, 10, 0);
        $this->addColumn('franquia', 'Franquia', 'INTEGER', true, 10, 0);
        $this->addColumn('tecnologia', 'Tecnologia', 'CHAR', true, null, null);
        $this->addColumn('garantia_banda', 'GarantiaBanda', 'INTEGER', true, null, null);
        $this->addColumn('ativo', 'Ativo', 'BOOLEAN', true, 1, false);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
        $this->addColumn('pre_pago', 'PrePago', 'BOOLEAN', false, 1, false);
        $this->addForeignKey('plano_id', 'PlanoId', 'INTEGER', 'planos', 'idplano', false, null, null);
        $this->addColumn('tipo', 'Tipo', 'CHAR', false, null, 'internet');
        $this->addForeignKey('tipo_servico_prestado_id', 'TipoServicoPrestadoId', 'INTEGER', 'tipo_servico_prestado', 'idtipo_servico_prestado', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Planos', '\\ImaTelecomBundle\\Model\\Planos', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':plano_id',
    1 => ':idplano',
  ),
), null, null, null, false);
        $this->addRelation('TipoServicoPrestado', '\\ImaTelecomBundle\\Model\\TipoServicoPrestado', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':tipo_servico_prestado_id',
    1 => ':idtipo_servico_prestado',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('ServicoCliente', '\\ImaTelecomBundle\\Model\\ServicoCliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':servico_prestado_id',
    1 => ':idservico_prestado',
  ),
), null, null, 'ServicoClientes', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoPrestado', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoPrestado', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoPrestado', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoPrestado', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoPrestado', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdservicoPrestado', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdservicoPrestado', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ServicoPrestadoTableMap::CLASS_DEFAULT : ServicoPrestadoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ServicoPrestado object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ServicoPrestadoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ServicoPrestadoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ServicoPrestadoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ServicoPrestadoTableMap::OM_CLASS;
            /** @var ServicoPrestado $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ServicoPrestadoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ServicoPrestadoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ServicoPrestadoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ServicoPrestado $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ServicoPrestadoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_NOME);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_CODIGO);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_DOWNLOAD);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_UPLOAD);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_FRANQUIA);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_TECNOLOGIA);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_GARANTIA_BANDA);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_ATIVO);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_PRE_PAGO);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_PLANO_ID);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_TIPO);
            $criteria->addSelectColumn(ServicoPrestadoTableMap::COL_TIPO_SERVICO_PRESTADO_ID);
        } else {
            $criteria->addSelectColumn($alias . '.idservico_prestado');
            $criteria->addSelectColumn($alias . '.nome');
            $criteria->addSelectColumn($alias . '.codigo');
            $criteria->addSelectColumn($alias . '.download');
            $criteria->addSelectColumn($alias . '.upload');
            $criteria->addSelectColumn($alias . '.franquia');
            $criteria->addSelectColumn($alias . '.tecnologia');
            $criteria->addSelectColumn($alias . '.garantia_banda');
            $criteria->addSelectColumn($alias . '.ativo');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.pre_pago');
            $criteria->addSelectColumn($alias . '.plano_id');
            $criteria->addSelectColumn($alias . '.tipo');
            $criteria->addSelectColumn($alias . '.tipo_servico_prestado_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ServicoPrestadoTableMap::DATABASE_NAME)->getTable(ServicoPrestadoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ServicoPrestadoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ServicoPrestadoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ServicoPrestadoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ServicoPrestado or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ServicoPrestado object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoPrestadoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\ServicoPrestado) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ServicoPrestadoTableMap::DATABASE_NAME);
            $criteria->add(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO, (array) $values, Criteria::IN);
        }

        $query = ServicoPrestadoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ServicoPrestadoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ServicoPrestadoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the servico_prestado table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ServicoPrestadoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ServicoPrestado or Criteria object.
     *
     * @param mixed               $criteria Criteria or ServicoPrestado object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ServicoPrestadoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ServicoPrestado object
        }

        if ($criteria->containsKey(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO) && $criteria->keyContainsValue(ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ServicoPrestadoTableMap::COL_IDSERVICO_PRESTADO.')');
        }


        // Set the correct dbName
        $query = ServicoPrestadoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ServicoPrestadoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ServicoPrestadoTableMap::buildTableMap();
