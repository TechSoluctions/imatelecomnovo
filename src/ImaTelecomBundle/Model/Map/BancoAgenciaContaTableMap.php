<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\BancoAgenciaConta;
use ImaTelecomBundle\Model\BancoAgenciaContaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'banco_agencia_conta' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BancoAgenciaContaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.BancoAgenciaContaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'banco_agencia_conta';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\BancoAgenciaConta';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.BancoAgenciaConta';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the idbanco_agencia_conta field
     */
    const COL_IDBANCO_AGENCIA_CONTA = 'banco_agencia_conta.idbanco_agencia_conta';

    /**
     * the column name for the banco_id field
     */
    const COL_BANCO_ID = 'banco_agencia_conta.banco_id';

    /**
     * the column name for the banco_agencia_id field
     */
    const COL_BANCO_AGENCIA_ID = 'banco_agencia_conta.banco_agencia_id';

    /**
     * the column name for the tipo_conta field
     */
    const COL_TIPO_CONTA = 'banco_agencia_conta.tipo_conta';

    /**
     * the column name for the numero_conta field
     */
    const COL_NUMERO_CONTA = 'banco_agencia_conta.numero_conta';

    /**
     * the column name for the digito_verificador field
     */
    const COL_DIGITO_VERIFICADOR = 'banco_agencia_conta.digito_verificador';

    /**
     * the column name for the convenio_id field
     */
    const COL_CONVENIO_ID = 'banco_agencia_conta.convenio_id';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'banco_agencia_conta.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'banco_agencia_conta.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'banco_agencia_conta.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdbancoAgenciaConta', 'BancoId', 'BancoAgenciaId', 'TipoConta', 'NumeroConta', 'DigitoVerificador', 'ConvenioId', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idbancoAgenciaConta', 'bancoId', 'bancoAgenciaId', 'tipoConta', 'numeroConta', 'digitoVerificador', 'convenioId', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, BancoAgenciaContaTableMap::COL_BANCO_ID, BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID, BancoAgenciaContaTableMap::COL_TIPO_CONTA, BancoAgenciaContaTableMap::COL_NUMERO_CONTA, BancoAgenciaContaTableMap::COL_DIGITO_VERIFICADOR, BancoAgenciaContaTableMap::COL_CONVENIO_ID, BancoAgenciaContaTableMap::COL_DATA_CADASTRO, BancoAgenciaContaTableMap::COL_DATA_ALTERADO, BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idbanco_agencia_conta', 'banco_id', 'banco_agencia_id', 'tipo_conta', 'numero_conta', 'digito_verificador', 'convenio_id', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdbancoAgenciaConta' => 0, 'BancoId' => 1, 'BancoAgenciaId' => 2, 'TipoConta' => 3, 'NumeroConta' => 4, 'DigitoVerificador' => 5, 'ConvenioId' => 6, 'DataCadastro' => 7, 'DataAlterado' => 8, 'UsuarioAlterado' => 9, ),
        self::TYPE_CAMELNAME     => array('idbancoAgenciaConta' => 0, 'bancoId' => 1, 'bancoAgenciaId' => 2, 'tipoConta' => 3, 'numeroConta' => 4, 'digitoVerificador' => 5, 'convenioId' => 6, 'dataCadastro' => 7, 'dataAlterado' => 8, 'usuarioAlterado' => 9, ),
        self::TYPE_COLNAME       => array(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA => 0, BancoAgenciaContaTableMap::COL_BANCO_ID => 1, BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID => 2, BancoAgenciaContaTableMap::COL_TIPO_CONTA => 3, BancoAgenciaContaTableMap::COL_NUMERO_CONTA => 4, BancoAgenciaContaTableMap::COL_DIGITO_VERIFICADOR => 5, BancoAgenciaContaTableMap::COL_CONVENIO_ID => 6, BancoAgenciaContaTableMap::COL_DATA_CADASTRO => 7, BancoAgenciaContaTableMap::COL_DATA_ALTERADO => 8, BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO => 9, ),
        self::TYPE_FIELDNAME     => array('idbanco_agencia_conta' => 0, 'banco_id' => 1, 'banco_agencia_id' => 2, 'tipo_conta' => 3, 'numero_conta' => 4, 'digito_verificador' => 5, 'convenio_id' => 6, 'data_cadastro' => 7, 'data_alterado' => 8, 'usuario_alterado' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('banco_agencia_conta');
        $this->setPhpName('BancoAgenciaConta');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\BancoAgenciaConta');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idbanco_agencia_conta', 'IdbancoAgenciaConta', 'INTEGER', true, 10, null);
        $this->addForeignKey('banco_id', 'BancoId', 'INTEGER', 'banco', 'idbanco', true, 10, null);
        $this->addForeignKey('banco_agencia_id', 'BancoAgenciaId', 'INTEGER', 'banco_agencia', 'idbanco_agencia', true, 10, null);
        $this->addColumn('tipo_conta', 'TipoConta', 'CHAR', true, null, null);
        $this->addColumn('numero_conta', 'NumeroConta', 'VARCHAR', true, 15, null);
        $this->addColumn('digito_verificador', 'DigitoVerificador', 'VARCHAR', true, 2, null);
        $this->addForeignKey('convenio_id', 'ConvenioId', 'INTEGER', 'convenio', 'idconvenio', true, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BancoAgencia', '\\ImaTelecomBundle\\Model\\BancoAgencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':banco_agencia_id',
    1 => ':idbanco_agencia',
  ),
), null, null, null, false);
        $this->addRelation('Banco', '\\ImaTelecomBundle\\Model\\Banco', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':banco_id',
    1 => ':idbanco',
  ),
), null, null, null, false);
        $this->addRelation('Convenio', '\\ImaTelecomBundle\\Model\\Convenio', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':convenio_id',
    1 => ':idconvenio',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('ContaCaixa', '\\ImaTelecomBundle\\Model\\ContaCaixa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':banco_agencia_conta_id',
    1 => ':idbanco_agencia_conta',
  ),
), null, null, 'ContaCaixas', false);
        $this->addRelation('Fornecedor', '\\ImaTelecomBundle\\Model\\Fornecedor', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':banco_agencia_conta_id',
    1 => ':idbanco_agencia_conta',
  ),
), null, null, 'Fornecedors', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgenciaConta', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgenciaConta', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgenciaConta', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgenciaConta', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgenciaConta', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbancoAgenciaConta', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdbancoAgenciaConta', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BancoAgenciaContaTableMap::CLASS_DEFAULT : BancoAgenciaContaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BancoAgenciaConta object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BancoAgenciaContaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BancoAgenciaContaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BancoAgenciaContaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BancoAgenciaContaTableMap::OM_CLASS;
            /** @var BancoAgenciaConta $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BancoAgenciaContaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BancoAgenciaContaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BancoAgenciaContaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BancoAgenciaConta $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BancoAgenciaContaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA);
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_BANCO_ID);
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_BANCO_AGENCIA_ID);
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_TIPO_CONTA);
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_NUMERO_CONTA);
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_DIGITO_VERIFICADOR);
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_CONVENIO_ID);
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(BancoAgenciaContaTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idbanco_agencia_conta');
            $criteria->addSelectColumn($alias . '.banco_id');
            $criteria->addSelectColumn($alias . '.banco_agencia_id');
            $criteria->addSelectColumn($alias . '.tipo_conta');
            $criteria->addSelectColumn($alias . '.numero_conta');
            $criteria->addSelectColumn($alias . '.digito_verificador');
            $criteria->addSelectColumn($alias . '.convenio_id');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BancoAgenciaContaTableMap::DATABASE_NAME)->getTable(BancoAgenciaContaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BancoAgenciaContaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BancoAgenciaContaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BancoAgenciaContaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BancoAgenciaConta or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BancoAgenciaConta object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaContaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\BancoAgenciaConta) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BancoAgenciaContaTableMap::DATABASE_NAME);
            $criteria->add(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA, (array) $values, Criteria::IN);
        }

        $query = BancoAgenciaContaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BancoAgenciaContaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BancoAgenciaContaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the banco_agencia_conta table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BancoAgenciaContaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BancoAgenciaConta or Criteria object.
     *
     * @param mixed               $criteria Criteria or BancoAgenciaConta object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoAgenciaContaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BancoAgenciaConta object
        }

        if ($criteria->containsKey(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA) && $criteria->keyContainsValue(BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BancoAgenciaContaTableMap::COL_IDBANCO_AGENCIA_CONTA.')');
        }


        // Set the correct dbName
        $query = BancoAgenciaContaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BancoAgenciaContaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BancoAgenciaContaTableMap::buildTableMap();
