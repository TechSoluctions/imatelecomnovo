<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\ContasPagar;
use ImaTelecomBundle\Model\ContasPagarQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'contas_pagar' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ContasPagarTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ContasPagarTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'contas_pagar';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\ContasPagar';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.ContasPagar';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the idcontas_pagar field
     */
    const COL_IDCONTAS_PAGAR = 'contas_pagar.idcontas_pagar';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'contas_pagar.valor';

    /**
     * the column name for the valor_frete field
     */
    const COL_VALOR_FRETE = 'contas_pagar.valor_frete';

    /**
     * the column name for the fornecedor_id field
     */
    const COL_FORNECEDOR_ID = 'contas_pagar.fornecedor_id';

    /**
     * the column name for the num_documento field
     */
    const COL_NUM_DOCUMENTO = 'contas_pagar.num_documento';

    /**
     * the column name for the juros field
     */
    const COL_JUROS = 'contas_pagar.juros';

    /**
     * the column name for the multa field
     */
    const COL_MULTA = 'contas_pagar.multa';

    /**
     * the column name for the data_emissao field
     */
    const COL_DATA_EMISSAO = 'contas_pagar.data_emissao';

    /**
     * the column name for the tipo field
     */
    const COL_TIPO = 'contas_pagar.tipo';

    /**
     * the column name for the especie field
     */
    const COL_ESPECIE = 'contas_pagar.especie';

    /**
     * the column name for the arquivo field
     */
    const COL_ARQUIVO = 'contas_pagar.arquivo';

    /**
     * the column name for the obs field
     */
    const COL_OBS = 'contas_pagar.obs';

    /**
     * the column name for the ativo field
     */
    const COL_ATIVO = 'contas_pagar.ativo';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'contas_pagar.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'contas_pagar.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'contas_pagar.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdcontasPagar', 'Valor', 'ValorFrete', 'FornecedorId', 'NumDocumento', 'Juros', 'Multa', 'DataEmissao', 'Tipo', 'Especie', 'Arquivo', 'Obs', 'Ativo', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idcontasPagar', 'valor', 'valorFrete', 'fornecedorId', 'numDocumento', 'juros', 'multa', 'dataEmissao', 'tipo', 'especie', 'arquivo', 'obs', 'ativo', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(ContasPagarTableMap::COL_IDCONTAS_PAGAR, ContasPagarTableMap::COL_VALOR, ContasPagarTableMap::COL_VALOR_FRETE, ContasPagarTableMap::COL_FORNECEDOR_ID, ContasPagarTableMap::COL_NUM_DOCUMENTO, ContasPagarTableMap::COL_JUROS, ContasPagarTableMap::COL_MULTA, ContasPagarTableMap::COL_DATA_EMISSAO, ContasPagarTableMap::COL_TIPO, ContasPagarTableMap::COL_ESPECIE, ContasPagarTableMap::COL_ARQUIVO, ContasPagarTableMap::COL_OBS, ContasPagarTableMap::COL_ATIVO, ContasPagarTableMap::COL_DATA_CADASTRO, ContasPagarTableMap::COL_DATA_ALTERADO, ContasPagarTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idcontas_pagar', 'valor', 'valor_frete', 'fornecedor_id', 'num_documento', 'juros', 'multa', 'data_emissao', 'tipo', 'especie', 'arquivo', 'obs', 'ativo', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdcontasPagar' => 0, 'Valor' => 1, 'ValorFrete' => 2, 'FornecedorId' => 3, 'NumDocumento' => 4, 'Juros' => 5, 'Multa' => 6, 'DataEmissao' => 7, 'Tipo' => 8, 'Especie' => 9, 'Arquivo' => 10, 'Obs' => 11, 'Ativo' => 12, 'DataCadastro' => 13, 'DataAlterado' => 14, 'UsuarioAlterado' => 15, ),
        self::TYPE_CAMELNAME     => array('idcontasPagar' => 0, 'valor' => 1, 'valorFrete' => 2, 'fornecedorId' => 3, 'numDocumento' => 4, 'juros' => 5, 'multa' => 6, 'dataEmissao' => 7, 'tipo' => 8, 'especie' => 9, 'arquivo' => 10, 'obs' => 11, 'ativo' => 12, 'dataCadastro' => 13, 'dataAlterado' => 14, 'usuarioAlterado' => 15, ),
        self::TYPE_COLNAME       => array(ContasPagarTableMap::COL_IDCONTAS_PAGAR => 0, ContasPagarTableMap::COL_VALOR => 1, ContasPagarTableMap::COL_VALOR_FRETE => 2, ContasPagarTableMap::COL_FORNECEDOR_ID => 3, ContasPagarTableMap::COL_NUM_DOCUMENTO => 4, ContasPagarTableMap::COL_JUROS => 5, ContasPagarTableMap::COL_MULTA => 6, ContasPagarTableMap::COL_DATA_EMISSAO => 7, ContasPagarTableMap::COL_TIPO => 8, ContasPagarTableMap::COL_ESPECIE => 9, ContasPagarTableMap::COL_ARQUIVO => 10, ContasPagarTableMap::COL_OBS => 11, ContasPagarTableMap::COL_ATIVO => 12, ContasPagarTableMap::COL_DATA_CADASTRO => 13, ContasPagarTableMap::COL_DATA_ALTERADO => 14, ContasPagarTableMap::COL_USUARIO_ALTERADO => 15, ),
        self::TYPE_FIELDNAME     => array('idcontas_pagar' => 0, 'valor' => 1, 'valor_frete' => 2, 'fornecedor_id' => 3, 'num_documento' => 4, 'juros' => 5, 'multa' => 6, 'data_emissao' => 7, 'tipo' => 8, 'especie' => 9, 'arquivo' => 10, 'obs' => 11, 'ativo' => 12, 'data_cadastro' => 13, 'data_alterado' => 14, 'usuario_alterado' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('contas_pagar');
        $this->setPhpName('ContasPagar');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\ContasPagar');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idcontas_pagar', 'IdcontasPagar', 'INTEGER', true, 10, null);
        $this->addColumn('valor', 'Valor', 'DOUBLE', true, null, null);
        $this->addColumn('valor_frete', 'ValorFrete', 'DOUBLE', true, null, 0);
        $this->addForeignKey('fornecedor_id', 'FornecedorId', 'INTEGER', 'fornecedor', 'idfornecedor', true, 10, null);
        $this->addColumn('num_documento', 'NumDocumento', 'VARCHAR', false, 45, null);
        $this->addColumn('juros', 'Juros', 'DOUBLE', false, null, null);
        $this->addColumn('multa', 'Multa', 'DOUBLE', false, null, null);
        $this->addColumn('data_emissao', 'DataEmissao', 'DATE', true, null, null);
        $this->addColumn('tipo', 'Tipo', 'CHAR', true, null, null);
        $this->addColumn('especie', 'Especie', 'CHAR', true, null, null);
        $this->addColumn('arquivo', 'Arquivo', 'VARCHAR', false, 255, null);
        $this->addColumn('obs', 'Obs', 'LONGVARCHAR', true, null, null);
        $this->addColumn('ativo', 'Ativo', 'BOOLEAN', true, 1, true);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Fornecedor', '\\ImaTelecomBundle\\Model\\Fornecedor', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':fornecedor_id',
    1 => ':idfornecedor',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('ContasPagarParcelas', '\\ImaTelecomBundle\\Model\\ContasPagarParcelas', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':conta_pagar_id',
    1 => ':idcontas_pagar',
  ),
), null, null, 'ContasPagarParcelass', false);
        $this->addRelation('ContasPagarTributos', '\\ImaTelecomBundle\\Model\\ContasPagarTributos', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':conta_pagar_id',
    1 => ':idcontas_pagar',
  ),
), null, null, 'ContasPagarTributoss', false);
        $this->addRelation('ItemComprado', '\\ImaTelecomBundle\\Model\\ItemComprado', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':contas_pagar_id',
    1 => ':idcontas_pagar',
  ),
), null, null, 'ItemComprados', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagar', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagar', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagar', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagar', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagar', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagar', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdcontasPagar', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ContasPagarTableMap::CLASS_DEFAULT : ContasPagarTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ContasPagar object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ContasPagarTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ContasPagarTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ContasPagarTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ContasPagarTableMap::OM_CLASS;
            /** @var ContasPagar $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ContasPagarTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ContasPagarTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ContasPagarTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ContasPagar $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ContasPagarTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ContasPagarTableMap::COL_IDCONTAS_PAGAR);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_VALOR);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_VALOR_FRETE);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_FORNECEDOR_ID);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_NUM_DOCUMENTO);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_JUROS);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_MULTA);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_DATA_EMISSAO);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_TIPO);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_ESPECIE);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_ARQUIVO);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_OBS);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_ATIVO);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(ContasPagarTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idcontas_pagar');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.valor_frete');
            $criteria->addSelectColumn($alias . '.fornecedor_id');
            $criteria->addSelectColumn($alias . '.num_documento');
            $criteria->addSelectColumn($alias . '.juros');
            $criteria->addSelectColumn($alias . '.multa');
            $criteria->addSelectColumn($alias . '.data_emissao');
            $criteria->addSelectColumn($alias . '.tipo');
            $criteria->addSelectColumn($alias . '.especie');
            $criteria->addSelectColumn($alias . '.arquivo');
            $criteria->addSelectColumn($alias . '.obs');
            $criteria->addSelectColumn($alias . '.ativo');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ContasPagarTableMap::DATABASE_NAME)->getTable(ContasPagarTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ContasPagarTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ContasPagarTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ContasPagarTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ContasPagar or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ContasPagar object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\ContasPagar) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ContasPagarTableMap::DATABASE_NAME);
            $criteria->add(ContasPagarTableMap::COL_IDCONTAS_PAGAR, (array) $values, Criteria::IN);
        }

        $query = ContasPagarQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ContasPagarTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ContasPagarTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the contas_pagar table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ContasPagarQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ContasPagar or Criteria object.
     *
     * @param mixed               $criteria Criteria or ContasPagar object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ContasPagar object
        }

        if ($criteria->containsKey(ContasPagarTableMap::COL_IDCONTAS_PAGAR) && $criteria->keyContainsValue(ContasPagarTableMap::COL_IDCONTAS_PAGAR) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ContasPagarTableMap::COL_IDCONTAS_PAGAR.')');
        }


        // Set the correct dbName
        $query = ContasPagarQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ContasPagarTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ContasPagarTableMap::buildTableMap();
