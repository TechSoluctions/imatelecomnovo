<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\BoletoItem;
use ImaTelecomBundle\Model\BoletoItemQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'boleto_item' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BoletoItemTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.BoletoItemTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'boleto_item';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\BoletoItem';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.BoletoItem';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the idboleto_item field
     */
    const COL_IDBOLETO_ITEM = 'boleto_item.idboleto_item';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'boleto_item.valor';

    /**
     * the column name for the boleto_id field
     */
    const COL_BOLETO_ID = 'boleto_item.boleto_id';

    /**
     * the column name for the servico_cliente_id field
     */
    const COL_SERVICO_CLIENTE_ID = 'boleto_item.servico_cliente_id';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'boleto_item.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'boleto_item.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'boleto_item.usuario_alterado';

    /**
     * the column name for the import_id field
     */
    const COL_IMPORT_ID = 'boleto_item.import_id';

    /**
     * the column name for the base_calculo_scm field
     */
    const COL_BASE_CALCULO_SCM = 'boleto_item.base_calculo_scm';

    /**
     * the column name for the base_calculo_sva field
     */
    const COL_BASE_CALCULO_SVA = 'boleto_item.base_calculo_sva';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdboletoItem', 'Valor', 'BoletoId', 'ServicoClienteId', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'ImportId', 'BaseCalculoScm', 'BaseCalculoSva', ),
        self::TYPE_CAMELNAME     => array('idboletoItem', 'valor', 'boletoId', 'servicoClienteId', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'importId', 'baseCalculoScm', 'baseCalculoSva', ),
        self::TYPE_COLNAME       => array(BoletoItemTableMap::COL_IDBOLETO_ITEM, BoletoItemTableMap::COL_VALOR, BoletoItemTableMap::COL_BOLETO_ID, BoletoItemTableMap::COL_SERVICO_CLIENTE_ID, BoletoItemTableMap::COL_DATA_CADASTRO, BoletoItemTableMap::COL_DATA_ALTERADO, BoletoItemTableMap::COL_USUARIO_ALTERADO, BoletoItemTableMap::COL_IMPORT_ID, BoletoItemTableMap::COL_BASE_CALCULO_SCM, BoletoItemTableMap::COL_BASE_CALCULO_SVA, ),
        self::TYPE_FIELDNAME     => array('idboleto_item', 'valor', 'boleto_id', 'servico_cliente_id', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'import_id', 'base_calculo_scm', 'base_calculo_sva', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdboletoItem' => 0, 'Valor' => 1, 'BoletoId' => 2, 'ServicoClienteId' => 3, 'DataCadastro' => 4, 'DataAlterado' => 5, 'UsuarioAlterado' => 6, 'ImportId' => 7, 'BaseCalculoScm' => 8, 'BaseCalculoSva' => 9, ),
        self::TYPE_CAMELNAME     => array('idboletoItem' => 0, 'valor' => 1, 'boletoId' => 2, 'servicoClienteId' => 3, 'dataCadastro' => 4, 'dataAlterado' => 5, 'usuarioAlterado' => 6, 'importId' => 7, 'baseCalculoScm' => 8, 'baseCalculoSva' => 9, ),
        self::TYPE_COLNAME       => array(BoletoItemTableMap::COL_IDBOLETO_ITEM => 0, BoletoItemTableMap::COL_VALOR => 1, BoletoItemTableMap::COL_BOLETO_ID => 2, BoletoItemTableMap::COL_SERVICO_CLIENTE_ID => 3, BoletoItemTableMap::COL_DATA_CADASTRO => 4, BoletoItemTableMap::COL_DATA_ALTERADO => 5, BoletoItemTableMap::COL_USUARIO_ALTERADO => 6, BoletoItemTableMap::COL_IMPORT_ID => 7, BoletoItemTableMap::COL_BASE_CALCULO_SCM => 8, BoletoItemTableMap::COL_BASE_CALCULO_SVA => 9, ),
        self::TYPE_FIELDNAME     => array('idboleto_item' => 0, 'valor' => 1, 'boleto_id' => 2, 'servico_cliente_id' => 3, 'data_cadastro' => 4, 'data_alterado' => 5, 'usuario_alterado' => 6, 'import_id' => 7, 'base_calculo_scm' => 8, 'base_calculo_sva' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('boleto_item');
        $this->setPhpName('BoletoItem');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\BoletoItem');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idboleto_item', 'IdboletoItem', 'INTEGER', true, 10, null);
        $this->addColumn('valor', 'Valor', 'DECIMAL', true, 10, null);
        $this->addForeignKey('boleto_id', 'BoletoId', 'INTEGER', 'boleto', 'idboleto', true, 10, null);
        $this->addForeignKey('servico_cliente_id', 'ServicoClienteId', 'INTEGER', 'servico_cliente', 'idservico_cliente', true, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
        $this->addColumn('import_id', 'ImportId', 'INTEGER', false, 10, null);
        $this->addColumn('base_calculo_scm', 'BaseCalculoScm', 'DECIMAL', false, 10, 0);
        $this->addColumn('base_calculo_sva', 'BaseCalculoSva', 'DECIMAL', false, 10, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, null, false);
        $this->addRelation('ServicoCliente', '\\ImaTelecomBundle\\Model\\ServicoCliente', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':servico_cliente_id',
    1 => ':idservico_cliente',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('DadosFiscaisGeracao', '\\ImaTelecomBundle\\Model\\DadosFiscaisGeracao', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':boleto_item_id',
    1 => ':idboleto_item',
  ),
), null, null, 'DadosFiscaisGeracaos', false);
        $this->addRelation('DadosFiscalItem', '\\ImaTelecomBundle\\Model\\DadosFiscalItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':boleto_item_id',
    1 => ':idboleto_item',
  ),
), null, null, 'DadosFiscalItems', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoItem', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoItem', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoItem', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoItem', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoItem', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoItem', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdboletoItem', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BoletoItemTableMap::CLASS_DEFAULT : BoletoItemTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BoletoItem object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BoletoItemTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BoletoItemTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BoletoItemTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BoletoItemTableMap::OM_CLASS;
            /** @var BoletoItem $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BoletoItemTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BoletoItemTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BoletoItemTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BoletoItem $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BoletoItemTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BoletoItemTableMap::COL_IDBOLETO_ITEM);
            $criteria->addSelectColumn(BoletoItemTableMap::COL_VALOR);
            $criteria->addSelectColumn(BoletoItemTableMap::COL_BOLETO_ID);
            $criteria->addSelectColumn(BoletoItemTableMap::COL_SERVICO_CLIENTE_ID);
            $criteria->addSelectColumn(BoletoItemTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(BoletoItemTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(BoletoItemTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(BoletoItemTableMap::COL_IMPORT_ID);
            $criteria->addSelectColumn(BoletoItemTableMap::COL_BASE_CALCULO_SCM);
            $criteria->addSelectColumn(BoletoItemTableMap::COL_BASE_CALCULO_SVA);
        } else {
            $criteria->addSelectColumn($alias . '.idboleto_item');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.boleto_id');
            $criteria->addSelectColumn($alias . '.servico_cliente_id');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.import_id');
            $criteria->addSelectColumn($alias . '.base_calculo_scm');
            $criteria->addSelectColumn($alias . '.base_calculo_sva');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BoletoItemTableMap::DATABASE_NAME)->getTable(BoletoItemTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BoletoItemTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BoletoItemTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BoletoItemTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BoletoItem or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BoletoItem object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoItemTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\BoletoItem) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BoletoItemTableMap::DATABASE_NAME);
            $criteria->add(BoletoItemTableMap::COL_IDBOLETO_ITEM, (array) $values, Criteria::IN);
        }

        $query = BoletoItemQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BoletoItemTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BoletoItemTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the boleto_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BoletoItemQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BoletoItem or Criteria object.
     *
     * @param mixed               $criteria Criteria or BoletoItem object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoItemTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BoletoItem object
        }

        if ($criteria->containsKey(BoletoItemTableMap::COL_IDBOLETO_ITEM) && $criteria->keyContainsValue(BoletoItemTableMap::COL_IDBOLETO_ITEM) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BoletoItemTableMap::COL_IDBOLETO_ITEM.')');
        }


        // Set the correct dbName
        $query = BoletoItemQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BoletoItemTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BoletoItemTableMap::buildTableMap();
