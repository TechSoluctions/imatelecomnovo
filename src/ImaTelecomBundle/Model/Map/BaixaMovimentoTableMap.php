<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\BaixaMovimento;
use ImaTelecomBundle\Model\BaixaMovimentoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'baixa_movimento' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BaixaMovimentoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.BaixaMovimentoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'baixa_movimento';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\BaixaMovimento';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.BaixaMovimento';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the idbaixa_movimento field
     */
    const COL_IDBAIXA_MOVIMENTO = 'baixa_movimento.idbaixa_movimento';

    /**
     * the column name for the baixa_id field
     */
    const COL_BAIXA_ID = 'baixa_movimento.baixa_id';

    /**
     * the column name for the forma_pagamento_id field
     */
    const COL_FORMA_PAGAMENTO_ID = 'baixa_movimento.forma_pagamento_id';

    /**
     * the column name for the usuario_baixa field
     */
    const COL_USUARIO_BAIXA = 'baixa_movimento.usuario_baixa';

    /**
     * the column name for the data_baixa field
     */
    const COL_DATA_BAIXA = 'baixa_movimento.data_baixa';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'baixa_movimento.valor';

    /**
     * the column name for the numero_documento field
     */
    const COL_NUMERO_DOCUMENTO = 'baixa_movimento.numero_documento';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'baixa_movimento.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'baixa_movimento.data_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdbaixaMovimento', 'BaixaId', 'FormaPagamentoId', 'UsuarioBaixa', 'DataBaixa', 'Valor', 'NumeroDocumento', 'DataCadastro', 'DataAlterado', ),
        self::TYPE_CAMELNAME     => array('idbaixaMovimento', 'baixaId', 'formaPagamentoId', 'usuarioBaixa', 'dataBaixa', 'valor', 'numeroDocumento', 'dataCadastro', 'dataAlterado', ),
        self::TYPE_COLNAME       => array(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO, BaixaMovimentoTableMap::COL_BAIXA_ID, BaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID, BaixaMovimentoTableMap::COL_USUARIO_BAIXA, BaixaMovimentoTableMap::COL_DATA_BAIXA, BaixaMovimentoTableMap::COL_VALOR, BaixaMovimentoTableMap::COL_NUMERO_DOCUMENTO, BaixaMovimentoTableMap::COL_DATA_CADASTRO, BaixaMovimentoTableMap::COL_DATA_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idbaixa_movimento', 'baixa_id', 'forma_pagamento_id', 'usuario_baixa', 'data_baixa', 'valor', 'numero_documento', 'data_cadastro', 'data_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdbaixaMovimento' => 0, 'BaixaId' => 1, 'FormaPagamentoId' => 2, 'UsuarioBaixa' => 3, 'DataBaixa' => 4, 'Valor' => 5, 'NumeroDocumento' => 6, 'DataCadastro' => 7, 'DataAlterado' => 8, ),
        self::TYPE_CAMELNAME     => array('idbaixaMovimento' => 0, 'baixaId' => 1, 'formaPagamentoId' => 2, 'usuarioBaixa' => 3, 'dataBaixa' => 4, 'valor' => 5, 'numeroDocumento' => 6, 'dataCadastro' => 7, 'dataAlterado' => 8, ),
        self::TYPE_COLNAME       => array(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO => 0, BaixaMovimentoTableMap::COL_BAIXA_ID => 1, BaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID => 2, BaixaMovimentoTableMap::COL_USUARIO_BAIXA => 3, BaixaMovimentoTableMap::COL_DATA_BAIXA => 4, BaixaMovimentoTableMap::COL_VALOR => 5, BaixaMovimentoTableMap::COL_NUMERO_DOCUMENTO => 6, BaixaMovimentoTableMap::COL_DATA_CADASTRO => 7, BaixaMovimentoTableMap::COL_DATA_ALTERADO => 8, ),
        self::TYPE_FIELDNAME     => array('idbaixa_movimento' => 0, 'baixa_id' => 1, 'forma_pagamento_id' => 2, 'usuario_baixa' => 3, 'data_baixa' => 4, 'valor' => 5, 'numero_documento' => 6, 'data_cadastro' => 7, 'data_alterado' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('baixa_movimento');
        $this->setPhpName('BaixaMovimento');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\BaixaMovimento');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idbaixa_movimento', 'IdbaixaMovimento', 'INTEGER', true, null, null);
        $this->addForeignKey('baixa_id', 'BaixaId', 'INTEGER', 'baixa', 'idbaixa', true, null, null);
        $this->addForeignKey('forma_pagamento_id', 'FormaPagamentoId', 'INTEGER', 'forma_pagamento', 'idforma_pagamento', true, null, null);
        $this->addForeignKey('usuario_baixa', 'UsuarioBaixa', 'INTEGER', 'usuario', 'idusuario', true, null, null);
        $this->addColumn('data_baixa', 'DataBaixa', 'DATE', true, null, null);
        $this->addColumn('valor', 'Valor', 'DECIMAL', true, 10, 0);
        $this->addColumn('numero_documento', 'NumeroDocumento', 'VARCHAR', false, 45, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Baixa', '\\ImaTelecomBundle\\Model\\Baixa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':baixa_id',
    1 => ':idbaixa',
  ),
), null, null, null, false);
        $this->addRelation('FormaPagamento', '\\ImaTelecomBundle\\Model\\FormaPagamento', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':forma_pagamento_id',
    1 => ':idforma_pagamento',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_baixa',
    1 => ':idusuario',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaMovimento', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaMovimento', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaMovimento', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaMovimento', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaMovimento', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaMovimento', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdbaixaMovimento', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BaixaMovimentoTableMap::CLASS_DEFAULT : BaixaMovimentoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BaixaMovimento object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BaixaMovimentoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BaixaMovimentoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BaixaMovimentoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BaixaMovimentoTableMap::OM_CLASS;
            /** @var BaixaMovimento $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BaixaMovimentoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BaixaMovimentoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BaixaMovimentoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BaixaMovimento $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BaixaMovimentoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO);
            $criteria->addSelectColumn(BaixaMovimentoTableMap::COL_BAIXA_ID);
            $criteria->addSelectColumn(BaixaMovimentoTableMap::COL_FORMA_PAGAMENTO_ID);
            $criteria->addSelectColumn(BaixaMovimentoTableMap::COL_USUARIO_BAIXA);
            $criteria->addSelectColumn(BaixaMovimentoTableMap::COL_DATA_BAIXA);
            $criteria->addSelectColumn(BaixaMovimentoTableMap::COL_VALOR);
            $criteria->addSelectColumn(BaixaMovimentoTableMap::COL_NUMERO_DOCUMENTO);
            $criteria->addSelectColumn(BaixaMovimentoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(BaixaMovimentoTableMap::COL_DATA_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idbaixa_movimento');
            $criteria->addSelectColumn($alias . '.baixa_id');
            $criteria->addSelectColumn($alias . '.forma_pagamento_id');
            $criteria->addSelectColumn($alias . '.usuario_baixa');
            $criteria->addSelectColumn($alias . '.data_baixa');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.numero_documento');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BaixaMovimentoTableMap::DATABASE_NAME)->getTable(BaixaMovimentoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BaixaMovimentoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BaixaMovimentoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BaixaMovimentoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BaixaMovimento or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BaixaMovimento object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaMovimentoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\BaixaMovimento) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BaixaMovimentoTableMap::DATABASE_NAME);
            $criteria->add(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO, (array) $values, Criteria::IN);
        }

        $query = BaixaMovimentoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BaixaMovimentoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BaixaMovimentoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the baixa_movimento table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BaixaMovimentoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BaixaMovimento or Criteria object.
     *
     * @param mixed               $criteria Criteria or BaixaMovimento object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaMovimentoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BaixaMovimento object
        }

        if ($criteria->containsKey(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO) && $criteria->keyContainsValue(BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BaixaMovimentoTableMap::COL_IDBAIXA_MOVIMENTO.')');
        }


        // Set the correct dbName
        $query = BaixaMovimentoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BaixaMovimentoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaixaMovimentoTableMap::buildTableMap();
