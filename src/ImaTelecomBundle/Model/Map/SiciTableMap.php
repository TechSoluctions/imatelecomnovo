<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Sici;
use ImaTelecomBundle\Model\SiciQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'sici' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class SiciTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.SiciTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'sici';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Sici';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Sici';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the idsici field
     */
    const COL_IDSICI = 'sici.idsici';

    /**
     * the column name for the ano field
     */
    const COL_ANO = 'sici.ano';

    /**
     * the column name for the mes field
     */
    const COL_MES = 'sici.mes';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'sici.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'sici.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'sici.usuario_alterado';

    /**
     * the column name for the nome_arquivo field
     */
    const COL_NOME_ARQUIVO = 'sici.nome_arquivo';

    /**
     * the column name for the competencia_id field
     */
    const COL_COMPETENCIA_ID = 'sici.competencia_id';

    /**
     * the column name for the qtde_notas field
     */
    const COL_QTDE_NOTAS = 'sici.qtde_notas';

    /**
     * the column name for the qtde_pontos field
     */
    const COL_QTDE_PONTOS = 'sici.qtde_pontos';

    /**
     * the column name for the valor_total field
     */
    const COL_VALOR_TOTAL = 'sici.valor_total';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idsici', 'Ano', 'Mes', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'NomeArquivo', 'CompetenciaId', 'QtdeNotas', 'QtdePontos', 'ValorTotal', ),
        self::TYPE_CAMELNAME     => array('idsici', 'ano', 'mes', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'nomeArquivo', 'competenciaId', 'qtdeNotas', 'qtdePontos', 'valorTotal', ),
        self::TYPE_COLNAME       => array(SiciTableMap::COL_IDSICI, SiciTableMap::COL_ANO, SiciTableMap::COL_MES, SiciTableMap::COL_DATA_CADASTRO, SiciTableMap::COL_DATA_ALTERADO, SiciTableMap::COL_USUARIO_ALTERADO, SiciTableMap::COL_NOME_ARQUIVO, SiciTableMap::COL_COMPETENCIA_ID, SiciTableMap::COL_QTDE_NOTAS, SiciTableMap::COL_QTDE_PONTOS, SiciTableMap::COL_VALOR_TOTAL, ),
        self::TYPE_FIELDNAME     => array('idsici', 'ano', 'mes', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'nome_arquivo', 'competencia_id', 'qtde_notas', 'qtde_pontos', 'valor_total', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idsici' => 0, 'Ano' => 1, 'Mes' => 2, 'DataCadastro' => 3, 'DataAlterado' => 4, 'UsuarioAlterado' => 5, 'NomeArquivo' => 6, 'CompetenciaId' => 7, 'QtdeNotas' => 8, 'QtdePontos' => 9, 'ValorTotal' => 10, ),
        self::TYPE_CAMELNAME     => array('idsici' => 0, 'ano' => 1, 'mes' => 2, 'dataCadastro' => 3, 'dataAlterado' => 4, 'usuarioAlterado' => 5, 'nomeArquivo' => 6, 'competenciaId' => 7, 'qtdeNotas' => 8, 'qtdePontos' => 9, 'valorTotal' => 10, ),
        self::TYPE_COLNAME       => array(SiciTableMap::COL_IDSICI => 0, SiciTableMap::COL_ANO => 1, SiciTableMap::COL_MES => 2, SiciTableMap::COL_DATA_CADASTRO => 3, SiciTableMap::COL_DATA_ALTERADO => 4, SiciTableMap::COL_USUARIO_ALTERADO => 5, SiciTableMap::COL_NOME_ARQUIVO => 6, SiciTableMap::COL_COMPETENCIA_ID => 7, SiciTableMap::COL_QTDE_NOTAS => 8, SiciTableMap::COL_QTDE_PONTOS => 9, SiciTableMap::COL_VALOR_TOTAL => 10, ),
        self::TYPE_FIELDNAME     => array('idsici' => 0, 'ano' => 1, 'mes' => 2, 'data_cadastro' => 3, 'data_alterado' => 4, 'usuario_alterado' => 5, 'nome_arquivo' => 6, 'competencia_id' => 7, 'qtde_notas' => 8, 'qtde_pontos' => 9, 'valor_total' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sici');
        $this->setPhpName('Sici');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Sici');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idsici', 'Idsici', 'INTEGER', true, 10, null);
        $this->addColumn('ano', 'Ano', 'VARCHAR', true, 4, null);
        $this->addColumn('mes', 'Mes', 'VARCHAR', true, 2, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
        $this->addColumn('nome_arquivo', 'NomeArquivo', 'VARCHAR', true, 250, null);
        $this->addForeignKey('competencia_id', 'CompetenciaId', 'INTEGER', 'competencia', 'id', true, 10, null);
        $this->addColumn('qtde_notas', 'QtdeNotas', 'INTEGER', false, null, 0);
        $this->addColumn('qtde_pontos', 'QtdePontos', 'INTEGER', false, null, 0);
        $this->addColumn('valor_total', 'ValorTotal', 'DECIMAL', false, 10, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Competencia', '\\ImaTelecomBundle\\Model\\Competencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idsici', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idsici', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idsici', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idsici', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idsici', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idsici', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idsici', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiciTableMap::CLASS_DEFAULT : SiciTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Sici object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiciTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiciTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiciTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiciTableMap::OM_CLASS;
            /** @var Sici $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiciTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiciTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiciTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Sici $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiciTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiciTableMap::COL_IDSICI);
            $criteria->addSelectColumn(SiciTableMap::COL_ANO);
            $criteria->addSelectColumn(SiciTableMap::COL_MES);
            $criteria->addSelectColumn(SiciTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(SiciTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(SiciTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(SiciTableMap::COL_NOME_ARQUIVO);
            $criteria->addSelectColumn(SiciTableMap::COL_COMPETENCIA_ID);
            $criteria->addSelectColumn(SiciTableMap::COL_QTDE_NOTAS);
            $criteria->addSelectColumn(SiciTableMap::COL_QTDE_PONTOS);
            $criteria->addSelectColumn(SiciTableMap::COL_VALOR_TOTAL);
        } else {
            $criteria->addSelectColumn($alias . '.idsici');
            $criteria->addSelectColumn($alias . '.ano');
            $criteria->addSelectColumn($alias . '.mes');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.nome_arquivo');
            $criteria->addSelectColumn($alias . '.competencia_id');
            $criteria->addSelectColumn($alias . '.qtde_notas');
            $criteria->addSelectColumn($alias . '.qtde_pontos');
            $criteria->addSelectColumn($alias . '.valor_total');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiciTableMap::DATABASE_NAME)->getTable(SiciTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiciTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiciTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiciTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Sici or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Sici object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiciTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Sici) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiciTableMap::DATABASE_NAME);
            $criteria->add(SiciTableMap::COL_IDSICI, (array) $values, Criteria::IN);
        }

        $query = SiciQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiciTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiciTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the sici table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiciQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Sici or Criteria object.
     *
     * @param mixed               $criteria Criteria or Sici object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiciTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Sici object
        }

        if ($criteria->containsKey(SiciTableMap::COL_IDSICI) && $criteria->keyContainsValue(SiciTableMap::COL_IDSICI) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SiciTableMap::COL_IDSICI.')');
        }


        // Set the correct dbName
        $query = SiciQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiciTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiciTableMap::buildTableMap();
