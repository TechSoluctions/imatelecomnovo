<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Empresa;
use ImaTelecomBundle\Model\EmpresaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'empresa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class EmpresaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.EmpresaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'empresa';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Empresa';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Empresa';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the idempresa field
     */
    const COL_IDEMPRESA = 'empresa.idempresa';

    /**
     * the column name for the pessoa_id field
     */
    const COL_PESSOA_ID = 'empresa.pessoa_id';

    /**
     * the column name for the ativo field
     */
    const COL_ATIVO = 'empresa.ativo';

    /**
     * the column name for the tipo_geracao_cobranca field
     */
    const COL_TIPO_GERACAO_COBRANCA = 'empresa.tipo_geracao_cobranca';

    /**
     * the column name for the ultimo_lote_enviado field
     */
    const COL_ULTIMO_LOTE_ENVIADO = 'empresa.ultimo_lote_enviado';

    /**
     * the column name for the ultimo_rps_enviado field
     */
    const COL_ULTIMO_RPS_ENVIADO = 'empresa.ultimo_rps_enviado';

    /**
     * the column name for the certificado_digital field
     */
    const COL_CERTIFICADO_DIGITAL = 'empresa.certificado_digital';

    /**
     * the column name for the notas_por_lote field
     */
    const COL_NOTAS_POR_LOTE = 'empresa.notas_por_lote';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idempresa', 'PessoaId', 'Ativo', 'TipoGeracaoCobranca', 'UltimoLoteEnviado', 'UltimoRpsEnviado', 'CertificadoDigital', 'NotasPorLote', ),
        self::TYPE_CAMELNAME     => array('idempresa', 'pessoaId', 'ativo', 'tipoGeracaoCobranca', 'ultimoLoteEnviado', 'ultimoRpsEnviado', 'certificadoDigital', 'notasPorLote', ),
        self::TYPE_COLNAME       => array(EmpresaTableMap::COL_IDEMPRESA, EmpresaTableMap::COL_PESSOA_ID, EmpresaTableMap::COL_ATIVO, EmpresaTableMap::COL_TIPO_GERACAO_COBRANCA, EmpresaTableMap::COL_ULTIMO_LOTE_ENVIADO, EmpresaTableMap::COL_ULTIMO_RPS_ENVIADO, EmpresaTableMap::COL_CERTIFICADO_DIGITAL, EmpresaTableMap::COL_NOTAS_POR_LOTE, ),
        self::TYPE_FIELDNAME     => array('idempresa', 'pessoa_id', 'ativo', 'tipo_geracao_cobranca', 'ultimo_lote_enviado', 'ultimo_rps_enviado', 'certificado_digital', 'notas_por_lote', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idempresa' => 0, 'PessoaId' => 1, 'Ativo' => 2, 'TipoGeracaoCobranca' => 3, 'UltimoLoteEnviado' => 4, 'UltimoRpsEnviado' => 5, 'CertificadoDigital' => 6, 'NotasPorLote' => 7, ),
        self::TYPE_CAMELNAME     => array('idempresa' => 0, 'pessoaId' => 1, 'ativo' => 2, 'tipoGeracaoCobranca' => 3, 'ultimoLoteEnviado' => 4, 'ultimoRpsEnviado' => 5, 'certificadoDigital' => 6, 'notasPorLote' => 7, ),
        self::TYPE_COLNAME       => array(EmpresaTableMap::COL_IDEMPRESA => 0, EmpresaTableMap::COL_PESSOA_ID => 1, EmpresaTableMap::COL_ATIVO => 2, EmpresaTableMap::COL_TIPO_GERACAO_COBRANCA => 3, EmpresaTableMap::COL_ULTIMO_LOTE_ENVIADO => 4, EmpresaTableMap::COL_ULTIMO_RPS_ENVIADO => 5, EmpresaTableMap::COL_CERTIFICADO_DIGITAL => 6, EmpresaTableMap::COL_NOTAS_POR_LOTE => 7, ),
        self::TYPE_FIELDNAME     => array('idempresa' => 0, 'pessoa_id' => 1, 'ativo' => 2, 'tipo_geracao_cobranca' => 3, 'ultimo_lote_enviado' => 4, 'ultimo_rps_enviado' => 5, 'certificado_digital' => 6, 'notas_por_lote' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('empresa');
        $this->setPhpName('Empresa');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Empresa');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idempresa', 'Idempresa', 'INTEGER', true, 10, null);
        $this->addForeignKey('pessoa_id', 'PessoaId', 'INTEGER', 'pessoa', 'id', true, 10, null);
        $this->addColumn('ativo', 'Ativo', 'BOOLEAN', true, 1, null);
        $this->addColumn('tipo_geracao_cobranca', 'TipoGeracaoCobranca', 'CHAR', true, null, null);
        $this->addColumn('ultimo_lote_enviado', 'UltimoLoteEnviado', 'INTEGER', false, null, 0);
        $this->addColumn('ultimo_rps_enviado', 'UltimoRpsEnviado', 'INTEGER', false, null, 0);
        $this->addColumn('certificado_digital', 'CertificadoDigital', 'VARCHAR', false, 250, null);
        $this->addColumn('notas_por_lote', 'NotasPorLote', 'INTEGER', false, null, 1);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Pessoa', '\\ImaTelecomBundle\\Model\\Pessoa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Convenio', '\\ImaTelecomBundle\\Model\\Convenio', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':empresa_id',
    1 => ':idempresa',
  ),
), null, null, 'Convenios', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idempresa', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idempresa', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idempresa', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idempresa', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idempresa', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idempresa', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idempresa', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? EmpresaTableMap::CLASS_DEFAULT : EmpresaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Empresa object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = EmpresaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = EmpresaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + EmpresaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EmpresaTableMap::OM_CLASS;
            /** @var Empresa $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            EmpresaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = EmpresaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = EmpresaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Empresa $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EmpresaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EmpresaTableMap::COL_IDEMPRESA);
            $criteria->addSelectColumn(EmpresaTableMap::COL_PESSOA_ID);
            $criteria->addSelectColumn(EmpresaTableMap::COL_ATIVO);
            $criteria->addSelectColumn(EmpresaTableMap::COL_TIPO_GERACAO_COBRANCA);
            $criteria->addSelectColumn(EmpresaTableMap::COL_ULTIMO_LOTE_ENVIADO);
            $criteria->addSelectColumn(EmpresaTableMap::COL_ULTIMO_RPS_ENVIADO);
            $criteria->addSelectColumn(EmpresaTableMap::COL_CERTIFICADO_DIGITAL);
            $criteria->addSelectColumn(EmpresaTableMap::COL_NOTAS_POR_LOTE);
        } else {
            $criteria->addSelectColumn($alias . '.idempresa');
            $criteria->addSelectColumn($alias . '.pessoa_id');
            $criteria->addSelectColumn($alias . '.ativo');
            $criteria->addSelectColumn($alias . '.tipo_geracao_cobranca');
            $criteria->addSelectColumn($alias . '.ultimo_lote_enviado');
            $criteria->addSelectColumn($alias . '.ultimo_rps_enviado');
            $criteria->addSelectColumn($alias . '.certificado_digital');
            $criteria->addSelectColumn($alias . '.notas_por_lote');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(EmpresaTableMap::DATABASE_NAME)->getTable(EmpresaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(EmpresaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(EmpresaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new EmpresaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Empresa or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Empresa object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EmpresaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Empresa) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EmpresaTableMap::DATABASE_NAME);
            $criteria->add(EmpresaTableMap::COL_IDEMPRESA, (array) $values, Criteria::IN);
        }

        $query = EmpresaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            EmpresaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                EmpresaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the empresa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return EmpresaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Empresa or Criteria object.
     *
     * @param mixed               $criteria Criteria or Empresa object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EmpresaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Empresa object
        }

        if ($criteria->containsKey(EmpresaTableMap::COL_IDEMPRESA) && $criteria->keyContainsValue(EmpresaTableMap::COL_IDEMPRESA) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.EmpresaTableMap::COL_IDEMPRESA.')');
        }


        // Set the correct dbName
        $query = EmpresaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // EmpresaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
EmpresaTableMap::buildTableMap();
