<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Convenio;
use ImaTelecomBundle\Model\ConvenioQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'convenio' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ConvenioTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ConvenioTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'convenio';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Convenio';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Convenio';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 20;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 20;

    /**
     * the column name for the idconvenio field
     */
    const COL_IDCONVENIO = 'convenio.idconvenio';

    /**
     * the column name for the codigo_convenio field
     */
    const COL_CODIGO_CONVENIO = 'convenio.codigo_convenio';

    /**
     * the column name for the digito_verificador field
     */
    const COL_DIGITO_VERIFICADOR = 'convenio.digito_verificador';

    /**
     * the column name for the nome field
     */
    const COL_NOME = 'convenio.nome';

    /**
     * the column name for the ativo field
     */
    const COL_ATIVO = 'convenio.ativo';

    /**
     * the column name for the carteira field
     */
    const COL_CARTEIRA = 'convenio.carteira';

    /**
     * the column name for the tipo_carteira field
     */
    const COL_TIPO_CARTEIRA = 'convenio.tipo_carteira';

    /**
     * the column name for the cnpj field
     */
    const COL_CNPJ = 'convenio.cnpj';

    /**
     * the column name for the tipo_cobranca field
     */
    const COL_TIPO_COBRANCA = 'convenio.tipo_cobranca';

    /**
     * the column name for the tipo_nosso_numero field
     */
    const COL_TIPO_NOSSO_NUMERO = 'convenio.tipo_nosso_numero';

    /**
     * the column name for the limite_inferior field
     */
    const COL_LIMITE_INFERIOR = 'convenio.limite_inferior';

    /**
     * the column name for the limite_superior field
     */
    const COL_LIMITE_SUPERIOR = 'convenio.limite_superior';

    /**
     * the column name for the ultimo_sequencial field
     */
    const COL_ULTIMO_SEQUENCIAL = 'convenio.ultimo_sequencial';

    /**
     * the column name for the valor_codigo_barra field
     */
    const COL_VALOR_CODIGO_BARRA = 'convenio.valor_codigo_barra';

    /**
     * the column name for the ultimo_sequencial_arquivo_cobranca field
     */
    const COL_ULTIMO_SEQUENCIAL_ARQUIVO_COBRANCA = 'convenio.ultimo_sequencial_arquivo_cobranca';

    /**
     * the column name for the ultimo_sequencial_debito_automatico field
     */
    const COL_ULTIMO_SEQUENCIAL_DEBITO_AUTOMATICO = 'convenio.ultimo_sequencial_debito_automatico';

    /**
     * the column name for the empresa_id field
     */
    const COL_EMPRESA_ID = 'convenio.empresa_id';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'convenio.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'convenio.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'convenio.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idconvenio', 'CodigoConvenio', 'DigitoVerificador', 'Nome', 'Ativo', 'Carteira', 'TipoCarteira', 'Cnpj', 'TipoCobranca', 'TipoNossoNumero', 'LimiteInferior', 'LimiteSuperior', 'UltimoSequencial', 'ValorCodigoBarra', 'UltimoSequencialArquivoCobranca', 'UltimoSequencialDebitoAutomatico', 'EmpresaId', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idconvenio', 'codigoConvenio', 'digitoVerificador', 'nome', 'ativo', 'carteira', 'tipoCarteira', 'cnpj', 'tipoCobranca', 'tipoNossoNumero', 'limiteInferior', 'limiteSuperior', 'ultimoSequencial', 'valorCodigoBarra', 'ultimoSequencialArquivoCobranca', 'ultimoSequencialDebitoAutomatico', 'empresaId', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(ConvenioTableMap::COL_IDCONVENIO, ConvenioTableMap::COL_CODIGO_CONVENIO, ConvenioTableMap::COL_DIGITO_VERIFICADOR, ConvenioTableMap::COL_NOME, ConvenioTableMap::COL_ATIVO, ConvenioTableMap::COL_CARTEIRA, ConvenioTableMap::COL_TIPO_CARTEIRA, ConvenioTableMap::COL_CNPJ, ConvenioTableMap::COL_TIPO_COBRANCA, ConvenioTableMap::COL_TIPO_NOSSO_NUMERO, ConvenioTableMap::COL_LIMITE_INFERIOR, ConvenioTableMap::COL_LIMITE_SUPERIOR, ConvenioTableMap::COL_ULTIMO_SEQUENCIAL, ConvenioTableMap::COL_VALOR_CODIGO_BARRA, ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_ARQUIVO_COBRANCA, ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_DEBITO_AUTOMATICO, ConvenioTableMap::COL_EMPRESA_ID, ConvenioTableMap::COL_DATA_CADASTRO, ConvenioTableMap::COL_DATA_ALTERADO, ConvenioTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idconvenio', 'codigo_convenio', 'digito_verificador', 'nome', 'ativo', 'carteira', 'tipo_carteira', 'cnpj', 'tipo_cobranca', 'tipo_nosso_numero', 'limite_inferior', 'limite_superior', 'ultimo_sequencial', 'valor_codigo_barra', 'ultimo_sequencial_arquivo_cobranca', 'ultimo_sequencial_debito_automatico', 'empresa_id', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idconvenio' => 0, 'CodigoConvenio' => 1, 'DigitoVerificador' => 2, 'Nome' => 3, 'Ativo' => 4, 'Carteira' => 5, 'TipoCarteira' => 6, 'Cnpj' => 7, 'TipoCobranca' => 8, 'TipoNossoNumero' => 9, 'LimiteInferior' => 10, 'LimiteSuperior' => 11, 'UltimoSequencial' => 12, 'ValorCodigoBarra' => 13, 'UltimoSequencialArquivoCobranca' => 14, 'UltimoSequencialDebitoAutomatico' => 15, 'EmpresaId' => 16, 'DataCadastro' => 17, 'DataAlterado' => 18, 'UsuarioAlterado' => 19, ),
        self::TYPE_CAMELNAME     => array('idconvenio' => 0, 'codigoConvenio' => 1, 'digitoVerificador' => 2, 'nome' => 3, 'ativo' => 4, 'carteira' => 5, 'tipoCarteira' => 6, 'cnpj' => 7, 'tipoCobranca' => 8, 'tipoNossoNumero' => 9, 'limiteInferior' => 10, 'limiteSuperior' => 11, 'ultimoSequencial' => 12, 'valorCodigoBarra' => 13, 'ultimoSequencialArquivoCobranca' => 14, 'ultimoSequencialDebitoAutomatico' => 15, 'empresaId' => 16, 'dataCadastro' => 17, 'dataAlterado' => 18, 'usuarioAlterado' => 19, ),
        self::TYPE_COLNAME       => array(ConvenioTableMap::COL_IDCONVENIO => 0, ConvenioTableMap::COL_CODIGO_CONVENIO => 1, ConvenioTableMap::COL_DIGITO_VERIFICADOR => 2, ConvenioTableMap::COL_NOME => 3, ConvenioTableMap::COL_ATIVO => 4, ConvenioTableMap::COL_CARTEIRA => 5, ConvenioTableMap::COL_TIPO_CARTEIRA => 6, ConvenioTableMap::COL_CNPJ => 7, ConvenioTableMap::COL_TIPO_COBRANCA => 8, ConvenioTableMap::COL_TIPO_NOSSO_NUMERO => 9, ConvenioTableMap::COL_LIMITE_INFERIOR => 10, ConvenioTableMap::COL_LIMITE_SUPERIOR => 11, ConvenioTableMap::COL_ULTIMO_SEQUENCIAL => 12, ConvenioTableMap::COL_VALOR_CODIGO_BARRA => 13, ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_ARQUIVO_COBRANCA => 14, ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_DEBITO_AUTOMATICO => 15, ConvenioTableMap::COL_EMPRESA_ID => 16, ConvenioTableMap::COL_DATA_CADASTRO => 17, ConvenioTableMap::COL_DATA_ALTERADO => 18, ConvenioTableMap::COL_USUARIO_ALTERADO => 19, ),
        self::TYPE_FIELDNAME     => array('idconvenio' => 0, 'codigo_convenio' => 1, 'digito_verificador' => 2, 'nome' => 3, 'ativo' => 4, 'carteira' => 5, 'tipo_carteira' => 6, 'cnpj' => 7, 'tipo_cobranca' => 8, 'tipo_nosso_numero' => 9, 'limite_inferior' => 10, 'limite_superior' => 11, 'ultimo_sequencial' => 12, 'valor_codigo_barra' => 13, 'ultimo_sequencial_arquivo_cobranca' => 14, 'ultimo_sequencial_debito_automatico' => 15, 'empresa_id' => 16, 'data_cadastro' => 17, 'data_alterado' => 18, 'usuario_alterado' => 19, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('convenio');
        $this->setPhpName('Convenio');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Convenio');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idconvenio', 'Idconvenio', 'INTEGER', true, 10, null);
        $this->addColumn('codigo_convenio', 'CodigoConvenio', 'VARCHAR', true, 15, null);
        $this->addColumn('digito_verificador', 'DigitoVerificador', 'VARCHAR', true, 2, null);
        $this->addColumn('nome', 'Nome', 'VARCHAR', true, 250, null);
        $this->addColumn('ativo', 'Ativo', 'BOOLEAN', true, 1, null);
        $this->addColumn('carteira', 'Carteira', 'VARCHAR', true, 15, null);
        $this->addColumn('tipo_carteira', 'TipoCarteira', 'VARCHAR', true, 250, null);
        $this->addColumn('cnpj', 'Cnpj', 'VARCHAR', true, 15, null);
        $this->addColumn('tipo_cobranca', 'TipoCobranca', 'CHAR', true, null, null);
        $this->addColumn('tipo_nosso_numero', 'TipoNossoNumero', 'CHAR', true, null, null);
        $this->addColumn('limite_inferior', 'LimiteInferior', 'VARCHAR', true, 15, null);
        $this->addColumn('limite_superior', 'LimiteSuperior', 'VARCHAR', true, 15, null);
        $this->addColumn('ultimo_sequencial', 'UltimoSequencial', 'VARCHAR', true, 15, null);
        $this->addColumn('valor_codigo_barra', 'ValorCodigoBarra', 'CHAR', true, null, null);
        $this->addColumn('ultimo_sequencial_arquivo_cobranca', 'UltimoSequencialArquivoCobranca', 'VARCHAR', true, 15, null);
        $this->addColumn('ultimo_sequencial_debito_automatico', 'UltimoSequencialDebitoAutomatico', 'VARCHAR', true, 15, null);
        $this->addForeignKey('empresa_id', 'EmpresaId', 'INTEGER', 'empresa', 'idempresa', true, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Empresa', '\\ImaTelecomBundle\\Model\\Empresa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':empresa_id',
    1 => ':idempresa',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('BancoAgenciaConta', '\\ImaTelecomBundle\\Model\\BancoAgenciaConta', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':convenio_id',
    1 => ':idconvenio',
  ),
), null, null, 'BancoAgenciaContas', false);
        $this->addRelation('Lancamentos', '\\ImaTelecomBundle\\Model\\Lancamentos', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':convenio_id',
    1 => ':idconvenio',
  ),
), null, null, 'Lancamentoss', false);
        $this->addRelation('LancamentosBoletos', '\\ImaTelecomBundle\\Model\\LancamentosBoletos', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':convenio_id',
    1 => ':idconvenio',
  ),
), null, null, 'LancamentosBoletoss', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idconvenio', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idconvenio', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idconvenio', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idconvenio', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idconvenio', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idconvenio', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idconvenio', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ConvenioTableMap::CLASS_DEFAULT : ConvenioTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Convenio object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ConvenioTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ConvenioTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ConvenioTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ConvenioTableMap::OM_CLASS;
            /** @var Convenio $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ConvenioTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ConvenioTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ConvenioTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Convenio $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ConvenioTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ConvenioTableMap::COL_IDCONVENIO);
            $criteria->addSelectColumn(ConvenioTableMap::COL_CODIGO_CONVENIO);
            $criteria->addSelectColumn(ConvenioTableMap::COL_DIGITO_VERIFICADOR);
            $criteria->addSelectColumn(ConvenioTableMap::COL_NOME);
            $criteria->addSelectColumn(ConvenioTableMap::COL_ATIVO);
            $criteria->addSelectColumn(ConvenioTableMap::COL_CARTEIRA);
            $criteria->addSelectColumn(ConvenioTableMap::COL_TIPO_CARTEIRA);
            $criteria->addSelectColumn(ConvenioTableMap::COL_CNPJ);
            $criteria->addSelectColumn(ConvenioTableMap::COL_TIPO_COBRANCA);
            $criteria->addSelectColumn(ConvenioTableMap::COL_TIPO_NOSSO_NUMERO);
            $criteria->addSelectColumn(ConvenioTableMap::COL_LIMITE_INFERIOR);
            $criteria->addSelectColumn(ConvenioTableMap::COL_LIMITE_SUPERIOR);
            $criteria->addSelectColumn(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL);
            $criteria->addSelectColumn(ConvenioTableMap::COL_VALOR_CODIGO_BARRA);
            $criteria->addSelectColumn(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_ARQUIVO_COBRANCA);
            $criteria->addSelectColumn(ConvenioTableMap::COL_ULTIMO_SEQUENCIAL_DEBITO_AUTOMATICO);
            $criteria->addSelectColumn(ConvenioTableMap::COL_EMPRESA_ID);
            $criteria->addSelectColumn(ConvenioTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(ConvenioTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(ConvenioTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idconvenio');
            $criteria->addSelectColumn($alias . '.codigo_convenio');
            $criteria->addSelectColumn($alias . '.digito_verificador');
            $criteria->addSelectColumn($alias . '.nome');
            $criteria->addSelectColumn($alias . '.ativo');
            $criteria->addSelectColumn($alias . '.carteira');
            $criteria->addSelectColumn($alias . '.tipo_carteira');
            $criteria->addSelectColumn($alias . '.cnpj');
            $criteria->addSelectColumn($alias . '.tipo_cobranca');
            $criteria->addSelectColumn($alias . '.tipo_nosso_numero');
            $criteria->addSelectColumn($alias . '.limite_inferior');
            $criteria->addSelectColumn($alias . '.limite_superior');
            $criteria->addSelectColumn($alias . '.ultimo_sequencial');
            $criteria->addSelectColumn($alias . '.valor_codigo_barra');
            $criteria->addSelectColumn($alias . '.ultimo_sequencial_arquivo_cobranca');
            $criteria->addSelectColumn($alias . '.ultimo_sequencial_debito_automatico');
            $criteria->addSelectColumn($alias . '.empresa_id');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ConvenioTableMap::DATABASE_NAME)->getTable(ConvenioTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ConvenioTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ConvenioTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ConvenioTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Convenio or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Convenio object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConvenioTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Convenio) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ConvenioTableMap::DATABASE_NAME);
            $criteria->add(ConvenioTableMap::COL_IDCONVENIO, (array) $values, Criteria::IN);
        }

        $query = ConvenioQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ConvenioTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ConvenioTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the convenio table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ConvenioQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Convenio or Criteria object.
     *
     * @param mixed               $criteria Criteria or Convenio object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ConvenioTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Convenio object
        }

        if ($criteria->containsKey(ConvenioTableMap::COL_IDCONVENIO) && $criteria->keyContainsValue(ConvenioTableMap::COL_IDCONVENIO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ConvenioTableMap::COL_IDCONVENIO.')');
        }


        // Set the correct dbName
        $query = ConvenioQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ConvenioTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ConvenioTableMap::buildTableMap();
