<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Fornecedor;
use ImaTelecomBundle\Model\FornecedorQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'fornecedor' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class FornecedorTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.FornecedorTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'fornecedor';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Fornecedor';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Fornecedor';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the idfornecedor field
     */
    const COL_IDFORNECEDOR = 'fornecedor.idfornecedor';

    /**
     * the column name for the cliente_id field
     */
    const COL_CLIENTE_ID = 'fornecedor.cliente_id';

    /**
     * the column name for the pessoa_id field
     */
    const COL_PESSOA_ID = 'fornecedor.pessoa_id';

    /**
     * the column name for the banco_agencia_conta_id field
     */
    const COL_BANCO_AGENCIA_CONTA_ID = 'fornecedor.banco_agencia_conta_id';

    /**
     * the column name for the observacao field
     */
    const COL_OBSERVACAO = 'fornecedor.observacao';

    /**
     * the column name for the ativo field
     */
    const COL_ATIVO = 'fornecedor.ativo';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'fornecedor.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'fornecedor.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'fornecedor.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idfornecedor', 'ClienteId', 'PessoaId', 'BancoAgenciaContaId', 'Observacao', 'Ativo', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idfornecedor', 'clienteId', 'pessoaId', 'bancoAgenciaContaId', 'observacao', 'ativo', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(FornecedorTableMap::COL_IDFORNECEDOR, FornecedorTableMap::COL_CLIENTE_ID, FornecedorTableMap::COL_PESSOA_ID, FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID, FornecedorTableMap::COL_OBSERVACAO, FornecedorTableMap::COL_ATIVO, FornecedorTableMap::COL_DATA_CADASTRO, FornecedorTableMap::COL_DATA_ALTERADO, FornecedorTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idfornecedor', 'cliente_id', 'pessoa_id', 'banco_agencia_conta_id', 'observacao', 'ativo', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idfornecedor' => 0, 'ClienteId' => 1, 'PessoaId' => 2, 'BancoAgenciaContaId' => 3, 'Observacao' => 4, 'Ativo' => 5, 'DataCadastro' => 6, 'DataAlterado' => 7, 'UsuarioAlterado' => 8, ),
        self::TYPE_CAMELNAME     => array('idfornecedor' => 0, 'clienteId' => 1, 'pessoaId' => 2, 'bancoAgenciaContaId' => 3, 'observacao' => 4, 'ativo' => 5, 'dataCadastro' => 6, 'dataAlterado' => 7, 'usuarioAlterado' => 8, ),
        self::TYPE_COLNAME       => array(FornecedorTableMap::COL_IDFORNECEDOR => 0, FornecedorTableMap::COL_CLIENTE_ID => 1, FornecedorTableMap::COL_PESSOA_ID => 2, FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID => 3, FornecedorTableMap::COL_OBSERVACAO => 4, FornecedorTableMap::COL_ATIVO => 5, FornecedorTableMap::COL_DATA_CADASTRO => 6, FornecedorTableMap::COL_DATA_ALTERADO => 7, FornecedorTableMap::COL_USUARIO_ALTERADO => 8, ),
        self::TYPE_FIELDNAME     => array('idfornecedor' => 0, 'cliente_id' => 1, 'pessoa_id' => 2, 'banco_agencia_conta_id' => 3, 'observacao' => 4, 'ativo' => 5, 'data_cadastro' => 6, 'data_alterado' => 7, 'usuario_alterado' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('fornecedor');
        $this->setPhpName('Fornecedor');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Fornecedor');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idfornecedor', 'Idfornecedor', 'INTEGER', true, 10, null);
        $this->addForeignKey('cliente_id', 'ClienteId', 'INTEGER', 'cliente', 'idcliente', true, 10, null);
        $this->addForeignKey('pessoa_id', 'PessoaId', 'INTEGER', 'pessoa', 'id', true, 10, null);
        $this->addForeignKey('banco_agencia_conta_id', 'BancoAgenciaContaId', 'INTEGER', 'banco_agencia_conta', 'idbanco_agencia_conta', false, 10, null);
        $this->addColumn('observacao', 'Observacao', 'LONGVARCHAR', false, null, null);
        $this->addColumn('ativo', 'Ativo', 'BOOLEAN', true, 1, true);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BancoAgenciaConta', '\\ImaTelecomBundle\\Model\\BancoAgenciaConta', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':banco_agencia_conta_id',
    1 => ':idbanco_agencia_conta',
  ),
), null, null, null, false);
        $this->addRelation('Cliente', '\\ImaTelecomBundle\\Model\\Cliente', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':cliente_id',
    1 => ':idcliente',
  ),
), null, null, null, false);
        $this->addRelation('Pessoa', '\\ImaTelecomBundle\\Model\\Pessoa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':fornecedor_id',
    1 => ':idfornecedor',
  ),
), null, null, 'Boletos', false);
        $this->addRelation('ContasPagar', '\\ImaTelecomBundle\\Model\\ContasPagar', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':fornecedor_id',
    1 => ':idfornecedor',
  ),
), null, null, 'ContasPagars', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfornecedor', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfornecedor', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfornecedor', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfornecedor', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfornecedor', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfornecedor', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idfornecedor', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? FornecedorTableMap::CLASS_DEFAULT : FornecedorTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Fornecedor object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = FornecedorTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = FornecedorTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + FornecedorTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = FornecedorTableMap::OM_CLASS;
            /** @var Fornecedor $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            FornecedorTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = FornecedorTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = FornecedorTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Fornecedor $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                FornecedorTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(FornecedorTableMap::COL_IDFORNECEDOR);
            $criteria->addSelectColumn(FornecedorTableMap::COL_CLIENTE_ID);
            $criteria->addSelectColumn(FornecedorTableMap::COL_PESSOA_ID);
            $criteria->addSelectColumn(FornecedorTableMap::COL_BANCO_AGENCIA_CONTA_ID);
            $criteria->addSelectColumn(FornecedorTableMap::COL_OBSERVACAO);
            $criteria->addSelectColumn(FornecedorTableMap::COL_ATIVO);
            $criteria->addSelectColumn(FornecedorTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(FornecedorTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(FornecedorTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idfornecedor');
            $criteria->addSelectColumn($alias . '.cliente_id');
            $criteria->addSelectColumn($alias . '.pessoa_id');
            $criteria->addSelectColumn($alias . '.banco_agencia_conta_id');
            $criteria->addSelectColumn($alias . '.observacao');
            $criteria->addSelectColumn($alias . '.ativo');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(FornecedorTableMap::DATABASE_NAME)->getTable(FornecedorTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(FornecedorTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(FornecedorTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new FornecedorTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Fornecedor or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Fornecedor object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FornecedorTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Fornecedor) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(FornecedorTableMap::DATABASE_NAME);
            $criteria->add(FornecedorTableMap::COL_IDFORNECEDOR, (array) $values, Criteria::IN);
        }

        $query = FornecedorQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            FornecedorTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                FornecedorTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the fornecedor table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return FornecedorQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Fornecedor or Criteria object.
     *
     * @param mixed               $criteria Criteria or Fornecedor object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FornecedorTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Fornecedor object
        }

        if ($criteria->containsKey(FornecedorTableMap::COL_IDFORNECEDOR) && $criteria->keyContainsValue(FornecedorTableMap::COL_IDFORNECEDOR) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.FornecedorTableMap::COL_IDFORNECEDOR.')');
        }


        // Set the correct dbName
        $query = FornecedorQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // FornecedorTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
FornecedorTableMap::buildTableMap();
