<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Banco;
use ImaTelecomBundle\Model\BancoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'banco' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BancoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.BancoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'banco';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Banco';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Banco';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the idbanco field
     */
    const COL_IDBANCO = 'banco.idbanco';

    /**
     * the column name for the nome field
     */
    const COL_NOME = 'banco.nome';

    /**
     * the column name for the codigo_febraban field
     */
    const COL_CODIGO_FEBRABAN = 'banco.codigo_febraban';

    /**
     * the column name for the ativo field
     */
    const COL_ATIVO = 'banco.ativo';

    /**
     * the column name for the layout_bancario_id field
     */
    const COL_LAYOUT_BANCARIO_ID = 'banco.layout_bancario_id';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'banco.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'banco.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'banco.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idbanco', 'Nome', 'CodigoFebraban', 'Ativo', 'LayoutBancarioId', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idbanco', 'nome', 'codigoFebraban', 'ativo', 'layoutBancarioId', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(BancoTableMap::COL_IDBANCO, BancoTableMap::COL_NOME, BancoTableMap::COL_CODIGO_FEBRABAN, BancoTableMap::COL_ATIVO, BancoTableMap::COL_LAYOUT_BANCARIO_ID, BancoTableMap::COL_DATA_CADASTRO, BancoTableMap::COL_DATA_ALTERADO, BancoTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idbanco', 'nome', 'codigo_febraban', 'ativo', 'layout_bancario_id', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idbanco' => 0, 'Nome' => 1, 'CodigoFebraban' => 2, 'Ativo' => 3, 'LayoutBancarioId' => 4, 'DataCadastro' => 5, 'DataAlterado' => 6, 'UsuarioAlterado' => 7, ),
        self::TYPE_CAMELNAME     => array('idbanco' => 0, 'nome' => 1, 'codigoFebraban' => 2, 'ativo' => 3, 'layoutBancarioId' => 4, 'dataCadastro' => 5, 'dataAlterado' => 6, 'usuarioAlterado' => 7, ),
        self::TYPE_COLNAME       => array(BancoTableMap::COL_IDBANCO => 0, BancoTableMap::COL_NOME => 1, BancoTableMap::COL_CODIGO_FEBRABAN => 2, BancoTableMap::COL_ATIVO => 3, BancoTableMap::COL_LAYOUT_BANCARIO_ID => 4, BancoTableMap::COL_DATA_CADASTRO => 5, BancoTableMap::COL_DATA_ALTERADO => 6, BancoTableMap::COL_USUARIO_ALTERADO => 7, ),
        self::TYPE_FIELDNAME     => array('idbanco' => 0, 'nome' => 1, 'codigo_febraban' => 2, 'ativo' => 3, 'layout_bancario_id' => 4, 'data_cadastro' => 5, 'data_alterado' => 6, 'usuario_alterado' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('banco');
        $this->setPhpName('Banco');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Banco');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idbanco', 'Idbanco', 'INTEGER', true, null, null);
        $this->addColumn('nome', 'Nome', 'VARCHAR', true, 60, null);
        $this->addColumn('codigo_febraban', 'CodigoFebraban', 'VARCHAR', true, 5, null);
        $this->addColumn('ativo', 'Ativo', 'TINYINT', true, 3, 1);
        $this->addForeignKey('layout_bancario_id', 'LayoutBancarioId', 'INTEGER', 'layout_bancario', 'idlayout_bancario', false, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('LayoutBancario', '\\ImaTelecomBundle\\Model\\LayoutBancario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':layout_bancario_id',
    1 => ':idlayout_bancario',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('BancoAgenciaConta', '\\ImaTelecomBundle\\Model\\BancoAgenciaConta', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':banco_id',
    1 => ':idbanco',
  ),
), null, null, 'BancoAgenciaContas', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbanco', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbanco', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbanco', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbanco', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbanco', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idbanco', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idbanco', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BancoTableMap::CLASS_DEFAULT : BancoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Banco object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BancoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BancoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BancoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BancoTableMap::OM_CLASS;
            /** @var Banco $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BancoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BancoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BancoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Banco $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BancoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BancoTableMap::COL_IDBANCO);
            $criteria->addSelectColumn(BancoTableMap::COL_NOME);
            $criteria->addSelectColumn(BancoTableMap::COL_CODIGO_FEBRABAN);
            $criteria->addSelectColumn(BancoTableMap::COL_ATIVO);
            $criteria->addSelectColumn(BancoTableMap::COL_LAYOUT_BANCARIO_ID);
            $criteria->addSelectColumn(BancoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(BancoTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(BancoTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idbanco');
            $criteria->addSelectColumn($alias . '.nome');
            $criteria->addSelectColumn($alias . '.codigo_febraban');
            $criteria->addSelectColumn($alias . '.ativo');
            $criteria->addSelectColumn($alias . '.layout_bancario_id');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BancoTableMap::DATABASE_NAME)->getTable(BancoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BancoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BancoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BancoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Banco or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Banco object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Banco) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BancoTableMap::DATABASE_NAME);
            $criteria->add(BancoTableMap::COL_IDBANCO, (array) $values, Criteria::IN);
        }

        $query = BancoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BancoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BancoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the banco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BancoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Banco or Criteria object.
     *
     * @param mixed               $criteria Criteria or Banco object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BancoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Banco object
        }

        if ($criteria->containsKey(BancoTableMap::COL_IDBANCO) && $criteria->keyContainsValue(BancoTableMap::COL_IDBANCO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BancoTableMap::COL_IDBANCO.')');
        }


        // Set the correct dbName
        $query = BancoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BancoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BancoTableMap::buildTableMap();
