<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\EnderecoAgencia;
use ImaTelecomBundle\Model\EnderecoAgenciaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'endereco_agencia' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class EnderecoAgenciaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.EnderecoAgenciaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'endereco_agencia';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\EnderecoAgencia';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.EnderecoAgencia';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the idendereco_agencia field
     */
    const COL_IDENDERECO_AGENCIA = 'endereco_agencia.idendereco_agencia';

    /**
     * the column name for the rua field
     */
    const COL_RUA = 'endereco_agencia.rua';

    /**
     * the column name for the bairro field
     */
    const COL_BAIRRO = 'endereco_agencia.bairro';

    /**
     * the column name for the num field
     */
    const COL_NUM = 'endereco_agencia.num';

    /**
     * the column name for the complemento field
     */
    const COL_COMPLEMENTO = 'endereco_agencia.complemento';

    /**
     * the column name for the cidade_id field
     */
    const COL_CIDADE_ID = 'endereco_agencia.cidade_id';

    /**
     * the column name for the cep field
     */
    const COL_CEP = 'endereco_agencia.cep';

    /**
     * the column name for the uf field
     */
    const COL_UF = 'endereco_agencia.uf';

    /**
     * the column name for the ponto_referencia field
     */
    const COL_PONTO_REFERENCIA = 'endereco_agencia.ponto_referencia';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'endereco_agencia.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'endereco_agencia.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'endereco_agencia.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdenderecoAgencia', 'Rua', 'Bairro', 'Num', 'Complemento', 'CidadeId', 'Cep', 'Uf', 'PontoReferencia', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idenderecoAgencia', 'rua', 'bairro', 'num', 'complemento', 'cidadeId', 'cep', 'uf', 'pontoReferencia', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA, EnderecoAgenciaTableMap::COL_RUA, EnderecoAgenciaTableMap::COL_BAIRRO, EnderecoAgenciaTableMap::COL_NUM, EnderecoAgenciaTableMap::COL_COMPLEMENTO, EnderecoAgenciaTableMap::COL_CIDADE_ID, EnderecoAgenciaTableMap::COL_CEP, EnderecoAgenciaTableMap::COL_UF, EnderecoAgenciaTableMap::COL_PONTO_REFERENCIA, EnderecoAgenciaTableMap::COL_DATA_CADASTRO, EnderecoAgenciaTableMap::COL_DATA_ALTERADO, EnderecoAgenciaTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idendereco_agencia', 'rua', 'bairro', 'num', 'complemento', 'cidade_id', 'cep', 'uf', 'ponto_referencia', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdenderecoAgencia' => 0, 'Rua' => 1, 'Bairro' => 2, 'Num' => 3, 'Complemento' => 4, 'CidadeId' => 5, 'Cep' => 6, 'Uf' => 7, 'PontoReferencia' => 8, 'DataCadastro' => 9, 'DataAlterado' => 10, 'UsuarioAlterado' => 11, ),
        self::TYPE_CAMELNAME     => array('idenderecoAgencia' => 0, 'rua' => 1, 'bairro' => 2, 'num' => 3, 'complemento' => 4, 'cidadeId' => 5, 'cep' => 6, 'uf' => 7, 'pontoReferencia' => 8, 'dataCadastro' => 9, 'dataAlterado' => 10, 'usuarioAlterado' => 11, ),
        self::TYPE_COLNAME       => array(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA => 0, EnderecoAgenciaTableMap::COL_RUA => 1, EnderecoAgenciaTableMap::COL_BAIRRO => 2, EnderecoAgenciaTableMap::COL_NUM => 3, EnderecoAgenciaTableMap::COL_COMPLEMENTO => 4, EnderecoAgenciaTableMap::COL_CIDADE_ID => 5, EnderecoAgenciaTableMap::COL_CEP => 6, EnderecoAgenciaTableMap::COL_UF => 7, EnderecoAgenciaTableMap::COL_PONTO_REFERENCIA => 8, EnderecoAgenciaTableMap::COL_DATA_CADASTRO => 9, EnderecoAgenciaTableMap::COL_DATA_ALTERADO => 10, EnderecoAgenciaTableMap::COL_USUARIO_ALTERADO => 11, ),
        self::TYPE_FIELDNAME     => array('idendereco_agencia' => 0, 'rua' => 1, 'bairro' => 2, 'num' => 3, 'complemento' => 4, 'cidade_id' => 5, 'cep' => 6, 'uf' => 7, 'ponto_referencia' => 8, 'data_cadastro' => 9, 'data_alterado' => 10, 'usuario_alterado' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('endereco_agencia');
        $this->setPhpName('EnderecoAgencia');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\EnderecoAgencia');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idendereco_agencia', 'IdenderecoAgencia', 'INTEGER', true, 10, null);
        $this->addColumn('rua', 'Rua', 'VARCHAR', false, 255, null);
        $this->addColumn('bairro', 'Bairro', 'VARCHAR', false, 45, null);
        $this->addColumn('num', 'Num', 'INTEGER', false, 10, null);
        $this->addColumn('complemento', 'Complemento', 'VARCHAR', false, 45, null);
        $this->addForeignKey('cidade_id', 'CidadeId', 'INTEGER', 'cidade', 'idcidade', true, 10, 1);
        $this->addColumn('cep', 'Cep', 'VARCHAR', false, 10, null);
        $this->addColumn('uf', 'Uf', 'CHAR', false, 2, null);
        $this->addColumn('ponto_referencia', 'PontoReferencia', 'VARCHAR', false, 256, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Cidade', '\\ImaTelecomBundle\\Model\\Cidade', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':cidade_id',
    1 => ':idcidade',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('BancoAgencia', '\\ImaTelecomBundle\\Model\\BancoAgencia', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':endereco_agencia_id',
    1 => ':idendereco_agencia',
  ),
), null, null, 'BancoAgencias', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoAgencia', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoAgencia', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoAgencia', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoAgencia', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoAgencia', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoAgencia', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdenderecoAgencia', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? EnderecoAgenciaTableMap::CLASS_DEFAULT : EnderecoAgenciaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (EnderecoAgencia object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = EnderecoAgenciaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = EnderecoAgenciaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + EnderecoAgenciaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EnderecoAgenciaTableMap::OM_CLASS;
            /** @var EnderecoAgencia $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            EnderecoAgenciaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = EnderecoAgenciaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = EnderecoAgenciaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var EnderecoAgencia $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EnderecoAgenciaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_RUA);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_BAIRRO);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_NUM);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_COMPLEMENTO);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_CIDADE_ID);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_CEP);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_UF);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_PONTO_REFERENCIA);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(EnderecoAgenciaTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idendereco_agencia');
            $criteria->addSelectColumn($alias . '.rua');
            $criteria->addSelectColumn($alias . '.bairro');
            $criteria->addSelectColumn($alias . '.num');
            $criteria->addSelectColumn($alias . '.complemento');
            $criteria->addSelectColumn($alias . '.cidade_id');
            $criteria->addSelectColumn($alias . '.cep');
            $criteria->addSelectColumn($alias . '.uf');
            $criteria->addSelectColumn($alias . '.ponto_referencia');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(EnderecoAgenciaTableMap::DATABASE_NAME)->getTable(EnderecoAgenciaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(EnderecoAgenciaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(EnderecoAgenciaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new EnderecoAgenciaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a EnderecoAgencia or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or EnderecoAgencia object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EnderecoAgenciaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\EnderecoAgencia) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EnderecoAgenciaTableMap::DATABASE_NAME);
            $criteria->add(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA, (array) $values, Criteria::IN);
        }

        $query = EnderecoAgenciaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            EnderecoAgenciaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                EnderecoAgenciaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the endereco_agencia table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return EnderecoAgenciaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a EnderecoAgencia or Criteria object.
     *
     * @param mixed               $criteria Criteria or EnderecoAgencia object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EnderecoAgenciaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from EnderecoAgencia object
        }

        if ($criteria->containsKey(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA) && $criteria->keyContainsValue(EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.EnderecoAgenciaTableMap::COL_IDENDERECO_AGENCIA.')');
        }


        // Set the correct dbName
        $query = EnderecoAgenciaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // EnderecoAgenciaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
EnderecoAgenciaTableMap::buildTableMap();
