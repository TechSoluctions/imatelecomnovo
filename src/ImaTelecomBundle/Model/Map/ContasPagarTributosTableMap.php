<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\ContasPagarTributos;
use ImaTelecomBundle\Model\ContasPagarTributosQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'contas_pagar_tributos' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ContasPagarTributosTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ContasPagarTributosTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'contas_pagar_tributos';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\ContasPagarTributos';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.ContasPagarTributos';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the idcontas_pagar_tributos field
     */
    const COL_IDCONTAS_PAGAR_TRIBUTOS = 'contas_pagar_tributos.idcontas_pagar_tributos';

    /**
     * the column name for the tributo_id field
     */
    const COL_TRIBUTO_ID = 'contas_pagar_tributos.tributo_id';

    /**
     * the column name for the percentual field
     */
    const COL_PERCENTUAL = 'contas_pagar_tributos.percentual';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'contas_pagar_tributos.valor';

    /**
     * the column name for the conta_pagar_id field
     */
    const COL_CONTA_PAGAR_ID = 'contas_pagar_tributos.conta_pagar_id';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'contas_pagar_tributos.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'contas_pagar_tributos.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'contas_pagar_tributos.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdcontasPagarTributos', 'TributoId', 'Percentual', 'Valor', 'ContaPagarId', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idcontasPagarTributos', 'tributoId', 'percentual', 'valor', 'contaPagarId', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS, ContasPagarTributosTableMap::COL_TRIBUTO_ID, ContasPagarTributosTableMap::COL_PERCENTUAL, ContasPagarTributosTableMap::COL_VALOR, ContasPagarTributosTableMap::COL_CONTA_PAGAR_ID, ContasPagarTributosTableMap::COL_DATA_CADASTRO, ContasPagarTributosTableMap::COL_DATA_ALTERADO, ContasPagarTributosTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idcontas_pagar_tributos', 'tributo_id', 'percentual', 'valor', 'conta_pagar_id', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdcontasPagarTributos' => 0, 'TributoId' => 1, 'Percentual' => 2, 'Valor' => 3, 'ContaPagarId' => 4, 'DataCadastro' => 5, 'DataAlterado' => 6, 'UsuarioAlterado' => 7, ),
        self::TYPE_CAMELNAME     => array('idcontasPagarTributos' => 0, 'tributoId' => 1, 'percentual' => 2, 'valor' => 3, 'contaPagarId' => 4, 'dataCadastro' => 5, 'dataAlterado' => 6, 'usuarioAlterado' => 7, ),
        self::TYPE_COLNAME       => array(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS => 0, ContasPagarTributosTableMap::COL_TRIBUTO_ID => 1, ContasPagarTributosTableMap::COL_PERCENTUAL => 2, ContasPagarTributosTableMap::COL_VALOR => 3, ContasPagarTributosTableMap::COL_CONTA_PAGAR_ID => 4, ContasPagarTributosTableMap::COL_DATA_CADASTRO => 5, ContasPagarTributosTableMap::COL_DATA_ALTERADO => 6, ContasPagarTributosTableMap::COL_USUARIO_ALTERADO => 7, ),
        self::TYPE_FIELDNAME     => array('idcontas_pagar_tributos' => 0, 'tributo_id' => 1, 'percentual' => 2, 'valor' => 3, 'conta_pagar_id' => 4, 'data_cadastro' => 5, 'data_alterado' => 6, 'usuario_alterado' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('contas_pagar_tributos');
        $this->setPhpName('ContasPagarTributos');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\ContasPagarTributos');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idcontas_pagar_tributos', 'IdcontasPagarTributos', 'INTEGER', true, 10, null);
        $this->addForeignKey('tributo_id', 'TributoId', 'INTEGER', 'tributo', 'idtributo', true, 10, null);
        $this->addColumn('percentual', 'Percentual', 'DECIMAL', true, 6, null);
        $this->addColumn('valor', 'Valor', 'DECIMAL', true, 6, null);
        $this->addForeignKey('conta_pagar_id', 'ContaPagarId', 'INTEGER', 'contas_pagar', 'idcontas_pagar', true, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ContasPagar', '\\ImaTelecomBundle\\Model\\ContasPagar', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':conta_pagar_id',
    1 => ':idcontas_pagar',
  ),
), null, null, null, false);
        $this->addRelation('Tributo', '\\ImaTelecomBundle\\Model\\Tributo', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':tributo_id',
    1 => ':idtributo',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarTributos', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarTributos', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarTributos', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarTributos', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarTributos', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarTributos', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdcontasPagarTributos', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ContasPagarTributosTableMap::CLASS_DEFAULT : ContasPagarTributosTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ContasPagarTributos object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ContasPagarTributosTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ContasPagarTributosTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ContasPagarTributosTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ContasPagarTributosTableMap::OM_CLASS;
            /** @var ContasPagarTributos $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ContasPagarTributosTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ContasPagarTributosTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ContasPagarTributosTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ContasPagarTributos $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ContasPagarTributosTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS);
            $criteria->addSelectColumn(ContasPagarTributosTableMap::COL_TRIBUTO_ID);
            $criteria->addSelectColumn(ContasPagarTributosTableMap::COL_PERCENTUAL);
            $criteria->addSelectColumn(ContasPagarTributosTableMap::COL_VALOR);
            $criteria->addSelectColumn(ContasPagarTributosTableMap::COL_CONTA_PAGAR_ID);
            $criteria->addSelectColumn(ContasPagarTributosTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(ContasPagarTributosTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(ContasPagarTributosTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idcontas_pagar_tributos');
            $criteria->addSelectColumn($alias . '.tributo_id');
            $criteria->addSelectColumn($alias . '.percentual');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.conta_pagar_id');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ContasPagarTributosTableMap::DATABASE_NAME)->getTable(ContasPagarTributosTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ContasPagarTributosTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ContasPagarTributosTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ContasPagarTributosTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ContasPagarTributos or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ContasPagarTributos object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTributosTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\ContasPagarTributos) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ContasPagarTributosTableMap::DATABASE_NAME);
            $criteria->add(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS, (array) $values, Criteria::IN);
        }

        $query = ContasPagarTributosQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ContasPagarTributosTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ContasPagarTributosTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the contas_pagar_tributos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ContasPagarTributosQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ContasPagarTributos or Criteria object.
     *
     * @param mixed               $criteria Criteria or ContasPagarTributos object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarTributosTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ContasPagarTributos object
        }

        if ($criteria->containsKey(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS) && $criteria->keyContainsValue(ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ContasPagarTributosTableMap::COL_IDCONTAS_PAGAR_TRIBUTOS.')');
        }


        // Set the correct dbName
        $query = ContasPagarTributosQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ContasPagarTributosTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ContasPagarTributosTableMap::buildTableMap();
