<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\EnderecoServCliente;
use ImaTelecomBundle\Model\EnderecoServClienteQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'endereco_serv_cliente' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class EnderecoServClienteTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.EnderecoServClienteTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'endereco_serv_cliente';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\EnderecoServCliente';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.EnderecoServCliente';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 15;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 15;

    /**
     * the column name for the idendereco_serv_cliente field
     */
    const COL_IDENDERECO_SERV_CLIENTE = 'endereco_serv_cliente.idendereco_serv_cliente';

    /**
     * the column name for the servico_cliente_id field
     */
    const COL_SERVICO_CLIENTE_ID = 'endereco_serv_cliente.servico_cliente_id';

    /**
     * the column name for the rua field
     */
    const COL_RUA = 'endereco_serv_cliente.rua';

    /**
     * the column name for the bairro field
     */
    const COL_BAIRRO = 'endereco_serv_cliente.bairro';

    /**
     * the column name for the num field
     */
    const COL_NUM = 'endereco_serv_cliente.num';

    /**
     * the column name for the complemento field
     */
    const COL_COMPLEMENTO = 'endereco_serv_cliente.complemento';

    /**
     * the column name for the cidade_id field
     */
    const COL_CIDADE_ID = 'endereco_serv_cliente.cidade_id';

    /**
     * the column name for the cep field
     */
    const COL_CEP = 'endereco_serv_cliente.cep';

    /**
     * the column name for the uf field
     */
    const COL_UF = 'endereco_serv_cliente.uf';

    /**
     * the column name for the ponto_referencia field
     */
    const COL_PONTO_REFERENCIA = 'endereco_serv_cliente.ponto_referencia';

    /**
     * the column name for the contato field
     */
    const COL_CONTATO = 'endereco_serv_cliente.contato';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'endereco_serv_cliente.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'endereco_serv_cliente.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'endereco_serv_cliente.usuario_alterado';

    /**
     * the column name for the import_id field
     */
    const COL_IMPORT_ID = 'endereco_serv_cliente.import_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdenderecoServCliente', 'ServicoClienteId', 'Rua', 'Bairro', 'Num', 'Complemento', 'CidadeId', 'Cep', 'Uf', 'PontoReferencia', 'Contato', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'ImportId', ),
        self::TYPE_CAMELNAME     => array('idenderecoServCliente', 'servicoClienteId', 'rua', 'bairro', 'num', 'complemento', 'cidadeId', 'cep', 'uf', 'pontoReferencia', 'contato', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'importId', ),
        self::TYPE_COLNAME       => array(EnderecoServClienteTableMap::COL_IDENDERECO_SERV_CLIENTE, EnderecoServClienteTableMap::COL_SERVICO_CLIENTE_ID, EnderecoServClienteTableMap::COL_RUA, EnderecoServClienteTableMap::COL_BAIRRO, EnderecoServClienteTableMap::COL_NUM, EnderecoServClienteTableMap::COL_COMPLEMENTO, EnderecoServClienteTableMap::COL_CIDADE_ID, EnderecoServClienteTableMap::COL_CEP, EnderecoServClienteTableMap::COL_UF, EnderecoServClienteTableMap::COL_PONTO_REFERENCIA, EnderecoServClienteTableMap::COL_CONTATO, EnderecoServClienteTableMap::COL_DATA_CADASTRO, EnderecoServClienteTableMap::COL_DATA_ALTERADO, EnderecoServClienteTableMap::COL_USUARIO_ALTERADO, EnderecoServClienteTableMap::COL_IMPORT_ID, ),
        self::TYPE_FIELDNAME     => array('idendereco_serv_cliente', 'servico_cliente_id', 'rua', 'bairro', 'num', 'complemento', 'cidade_id', 'cep', 'uf', 'ponto_referencia', 'contato', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'import_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdenderecoServCliente' => 0, 'ServicoClienteId' => 1, 'Rua' => 2, 'Bairro' => 3, 'Num' => 4, 'Complemento' => 5, 'CidadeId' => 6, 'Cep' => 7, 'Uf' => 8, 'PontoReferencia' => 9, 'Contato' => 10, 'DataCadastro' => 11, 'DataAlterado' => 12, 'UsuarioAlterado' => 13, 'ImportId' => 14, ),
        self::TYPE_CAMELNAME     => array('idenderecoServCliente' => 0, 'servicoClienteId' => 1, 'rua' => 2, 'bairro' => 3, 'num' => 4, 'complemento' => 5, 'cidadeId' => 6, 'cep' => 7, 'uf' => 8, 'pontoReferencia' => 9, 'contato' => 10, 'dataCadastro' => 11, 'dataAlterado' => 12, 'usuarioAlterado' => 13, 'importId' => 14, ),
        self::TYPE_COLNAME       => array(EnderecoServClienteTableMap::COL_IDENDERECO_SERV_CLIENTE => 0, EnderecoServClienteTableMap::COL_SERVICO_CLIENTE_ID => 1, EnderecoServClienteTableMap::COL_RUA => 2, EnderecoServClienteTableMap::COL_BAIRRO => 3, EnderecoServClienteTableMap::COL_NUM => 4, EnderecoServClienteTableMap::COL_COMPLEMENTO => 5, EnderecoServClienteTableMap::COL_CIDADE_ID => 6, EnderecoServClienteTableMap::COL_CEP => 7, EnderecoServClienteTableMap::COL_UF => 8, EnderecoServClienteTableMap::COL_PONTO_REFERENCIA => 9, EnderecoServClienteTableMap::COL_CONTATO => 10, EnderecoServClienteTableMap::COL_DATA_CADASTRO => 11, EnderecoServClienteTableMap::COL_DATA_ALTERADO => 12, EnderecoServClienteTableMap::COL_USUARIO_ALTERADO => 13, EnderecoServClienteTableMap::COL_IMPORT_ID => 14, ),
        self::TYPE_FIELDNAME     => array('idendereco_serv_cliente' => 0, 'servico_cliente_id' => 1, 'rua' => 2, 'bairro' => 3, 'num' => 4, 'complemento' => 5, 'cidade_id' => 6, 'cep' => 7, 'uf' => 8, 'ponto_referencia' => 9, 'contato' => 10, 'data_cadastro' => 11, 'data_alterado' => 12, 'usuario_alterado' => 13, 'import_id' => 14, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('endereco_serv_cliente');
        $this->setPhpName('EnderecoServCliente');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\EnderecoServCliente');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idendereco_serv_cliente', 'IdenderecoServCliente', 'INTEGER', true, 10, null);
        $this->addForeignKey('servico_cliente_id', 'ServicoClienteId', 'INTEGER', 'servico_cliente', 'idservico_cliente', true, 10, null);
        $this->addColumn('rua', 'Rua', 'VARCHAR', false, 255, null);
        $this->addColumn('bairro', 'Bairro', 'VARCHAR', false, 45, null);
        $this->addColumn('num', 'Num', 'INTEGER', false, 10, null);
        $this->addColumn('complemento', 'Complemento', 'VARCHAR', true, 45, null);
        $this->addForeignKey('cidade_id', 'CidadeId', 'INTEGER', 'cidade', 'idcidade', true, 10, 1);
        $this->addColumn('cep', 'Cep', 'VARCHAR', false, 10, null);
        $this->addColumn('uf', 'Uf', 'CHAR', false, 2, null);
        $this->addColumn('ponto_referencia', 'PontoReferencia', 'VARCHAR', false, 256, null);
        $this->addColumn('contato', 'Contato', 'VARCHAR', false, 127, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
        $this->addColumn('import_id', 'ImportId', 'INTEGER', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Cidade', '\\ImaTelecomBundle\\Model\\Cidade', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':cidade_id',
    1 => ':idcidade',
  ),
), null, null, null, false);
        $this->addRelation('ServicoCliente', '\\ImaTelecomBundle\\Model\\ServicoCliente', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':servico_cliente_id',
    1 => ':idservico_cliente',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoServCliente', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoServCliente', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoServCliente', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoServCliente', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoServCliente', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdenderecoServCliente', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdenderecoServCliente', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? EnderecoServClienteTableMap::CLASS_DEFAULT : EnderecoServClienteTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (EnderecoServCliente object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = EnderecoServClienteTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = EnderecoServClienteTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + EnderecoServClienteTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EnderecoServClienteTableMap::OM_CLASS;
            /** @var EnderecoServCliente $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            EnderecoServClienteTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = EnderecoServClienteTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = EnderecoServClienteTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var EnderecoServCliente $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EnderecoServClienteTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_IDENDERECO_SERV_CLIENTE);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_SERVICO_CLIENTE_ID);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_RUA);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_BAIRRO);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_NUM);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_COMPLEMENTO);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_CIDADE_ID);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_CEP);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_UF);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_PONTO_REFERENCIA);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_CONTATO);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(EnderecoServClienteTableMap::COL_IMPORT_ID);
        } else {
            $criteria->addSelectColumn($alias . '.idendereco_serv_cliente');
            $criteria->addSelectColumn($alias . '.servico_cliente_id');
            $criteria->addSelectColumn($alias . '.rua');
            $criteria->addSelectColumn($alias . '.bairro');
            $criteria->addSelectColumn($alias . '.num');
            $criteria->addSelectColumn($alias . '.complemento');
            $criteria->addSelectColumn($alias . '.cidade_id');
            $criteria->addSelectColumn($alias . '.cep');
            $criteria->addSelectColumn($alias . '.uf');
            $criteria->addSelectColumn($alias . '.ponto_referencia');
            $criteria->addSelectColumn($alias . '.contato');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.import_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(EnderecoServClienteTableMap::DATABASE_NAME)->getTable(EnderecoServClienteTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(EnderecoServClienteTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(EnderecoServClienteTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new EnderecoServClienteTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a EnderecoServCliente or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or EnderecoServCliente object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EnderecoServClienteTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\EnderecoServCliente) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EnderecoServClienteTableMap::DATABASE_NAME);
            $criteria->add(EnderecoServClienteTableMap::COL_IDENDERECO_SERV_CLIENTE, (array) $values, Criteria::IN);
        }

        $query = EnderecoServClienteQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            EnderecoServClienteTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                EnderecoServClienteTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the endereco_serv_cliente table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return EnderecoServClienteQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a EnderecoServCliente or Criteria object.
     *
     * @param mixed               $criteria Criteria or EnderecoServCliente object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EnderecoServClienteTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from EnderecoServCliente object
        }

        if ($criteria->containsKey(EnderecoServClienteTableMap::COL_IDENDERECO_SERV_CLIENTE) && $criteria->keyContainsValue(EnderecoServClienteTableMap::COL_IDENDERECO_SERV_CLIENTE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.EnderecoServClienteTableMap::COL_IDENDERECO_SERV_CLIENTE.')');
        }


        // Set the correct dbName
        $query = EnderecoServClienteQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // EnderecoServClienteTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
EnderecoServClienteTableMap::buildTableMap();
