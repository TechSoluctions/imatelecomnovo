<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Boleto;
use ImaTelecomBundle\Model\BoletoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'boleto' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BoletoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.BoletoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'boleto';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Boleto';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Boleto';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 35;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 35;

    /**
     * the column name for the idboleto field
     */
    const COL_IDBOLETO = 'boleto.idboleto';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'boleto.valor';

    /**
     * the column name for the vencimento field
     */
    const COL_VENCIMENTO = 'boleto.vencimento';

    /**
     * the column name for the cliente_id field
     */
    const COL_CLIENTE_ID = 'boleto.cliente_id';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'boleto.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'boleto.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'boleto.usuario_alterado';

    /**
     * the column name for the import_id field
     */
    const COL_IMPORT_ID = 'boleto.import_id';

    /**
     * the column name for the competencia_id field
     */
    const COL_COMPETENCIA_ID = 'boleto.competencia_id';

    /**
     * the column name for the nosso_numero field
     */
    const COL_NOSSO_NUMERO = 'boleto.nosso_numero';

    /**
     * the column name for the numero_documento field
     */
    const COL_NUMERO_DOCUMENTO = 'boleto.numero_documento';

    /**
     * the column name for the registrado field
     */
    const COL_REGISTRADO = 'boleto.registrado';

    /**
     * the column name for the valor_original field
     */
    const COL_VALOR_ORIGINAL = 'boleto.valor_original';

    /**
     * the column name for the valor_multa field
     */
    const COL_VALOR_MULTA = 'boleto.valor_multa';

    /**
     * the column name for the valor_juros field
     */
    const COL_VALOR_JUROS = 'boleto.valor_juros';

    /**
     * the column name for the valor_desconto field
     */
    const COL_VALOR_DESCONTO = 'boleto.valor_desconto';

    /**
     * the column name for the valor_pago field
     */
    const COL_VALOR_PAGO = 'boleto.valor_pago';

    /**
     * the column name for the linha_digitavel field
     */
    const COL_LINHA_DIGITAVEL = 'boleto.linha_digitavel';

    /**
     * the column name for the data_pagamento field
     */
    const COL_DATA_PAGAMENTO = 'boleto.data_pagamento';

    /**
     * the column name for the esta_pago field
     */
    const COL_ESTA_PAGO = 'boleto.esta_pago';

    /**
     * the column name for the valor_abatimento field
     */
    const COL_VALOR_ABATIMENTO = 'boleto.valor_abatimento';

    /**
     * the column name for the valor_iof field
     */
    const COL_VALOR_IOF = 'boleto.valor_iof';

    /**
     * the column name for the valor_liquido field
     */
    const COL_VALOR_LIQUIDO = 'boleto.valor_liquido';

    /**
     * the column name for the valor_outras_despesas field
     */
    const COL_VALOR_OUTRAS_DESPESAS = 'boleto.valor_outras_despesas';

    /**
     * the column name for the valor_outros_creditos field
     */
    const COL_VALOR_OUTROS_CREDITOS = 'boleto.valor_outros_creditos';

    /**
     * the column name for the nota_scm_gerada field
     */
    const COL_NOTA_SCM_GERADA = 'boleto.nota_scm_gerada';

    /**
     * the column name for the nota_scm_cancelada field
     */
    const COL_NOTA_SCM_CANCELADA = 'boleto.nota_scm_cancelada';

    /**
     * the column name for the base_calculo_scm field
     */
    const COL_BASE_CALCULO_SCM = 'boleto.base_calculo_scm';

    /**
     * the column name for the base_calculo_sva field
     */
    const COL_BASE_CALCULO_SVA = 'boleto.base_calculo_sva';

    /**
     * the column name for the nota_sva_gerada field
     */
    const COL_NOTA_SVA_GERADA = 'boleto.nota_sva_gerada';

    /**
     * the column name for the nota_sva_cancelada field
     */
    const COL_NOTA_SVA_CANCELADA = 'boleto.nota_sva_cancelada';

    /**
     * the column name for the tipo_conta field
     */
    const COL_TIPO_CONTA = 'boleto.tipo_conta';

    /**
     * the column name for the baixa_id field
     */
    const COL_BAIXA_ID = 'boleto.baixa_id';

    /**
     * the column name for the baixa_estorno_id field
     */
    const COL_BAIXA_ESTORNO_ID = 'boleto.baixa_estorno_id';

    /**
     * the column name for the fornecedor_id field
     */
    const COL_FORNECEDOR_ID = 'boleto.fornecedor_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idboleto', 'Valor', 'Vencimento', 'ClienteId', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'ImportId', 'CompetenciaId', 'NossoNumero', 'NumeroDocumento', 'Registrado', 'ValorOriginal', 'ValorMulta', 'ValorJuros', 'ValorDesconto', 'ValorPago', 'LinhaDigitavel', 'DataPagamento', 'EstaPago', 'ValorAbatimento', 'ValorIof', 'ValorLiquido', 'ValorOutrasDespesas', 'ValorOutrosCreditos', 'NotaScmGerada', 'NotaScmCancelada', 'BaseCalculoScm', 'BaseCalculoSva', 'NotaSvaGerada', 'NotaSvaCancelada', 'TipoConta', 'BaixaId', 'BaixaEstornoId', 'FornecedorId', ),
        self::TYPE_CAMELNAME     => array('idboleto', 'valor', 'vencimento', 'clienteId', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'importId', 'competenciaId', 'nossoNumero', 'numeroDocumento', 'registrado', 'valorOriginal', 'valorMulta', 'valorJuros', 'valorDesconto', 'valorPago', 'linhaDigitavel', 'dataPagamento', 'estaPago', 'valorAbatimento', 'valorIof', 'valorLiquido', 'valorOutrasDespesas', 'valorOutrosCreditos', 'notaScmGerada', 'notaScmCancelada', 'baseCalculoScm', 'baseCalculoSva', 'notaSvaGerada', 'notaSvaCancelada', 'tipoConta', 'baixaId', 'baixaEstornoId', 'fornecedorId', ),
        self::TYPE_COLNAME       => array(BoletoTableMap::COL_IDBOLETO, BoletoTableMap::COL_VALOR, BoletoTableMap::COL_VENCIMENTO, BoletoTableMap::COL_CLIENTE_ID, BoletoTableMap::COL_DATA_CADASTRO, BoletoTableMap::COL_DATA_ALTERADO, BoletoTableMap::COL_USUARIO_ALTERADO, BoletoTableMap::COL_IMPORT_ID, BoletoTableMap::COL_COMPETENCIA_ID, BoletoTableMap::COL_NOSSO_NUMERO, BoletoTableMap::COL_NUMERO_DOCUMENTO, BoletoTableMap::COL_REGISTRADO, BoletoTableMap::COL_VALOR_ORIGINAL, BoletoTableMap::COL_VALOR_MULTA, BoletoTableMap::COL_VALOR_JUROS, BoletoTableMap::COL_VALOR_DESCONTO, BoletoTableMap::COL_VALOR_PAGO, BoletoTableMap::COL_LINHA_DIGITAVEL, BoletoTableMap::COL_DATA_PAGAMENTO, BoletoTableMap::COL_ESTA_PAGO, BoletoTableMap::COL_VALOR_ABATIMENTO, BoletoTableMap::COL_VALOR_IOF, BoletoTableMap::COL_VALOR_LIQUIDO, BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS, BoletoTableMap::COL_VALOR_OUTROS_CREDITOS, BoletoTableMap::COL_NOTA_SCM_GERADA, BoletoTableMap::COL_NOTA_SCM_CANCELADA, BoletoTableMap::COL_BASE_CALCULO_SCM, BoletoTableMap::COL_BASE_CALCULO_SVA, BoletoTableMap::COL_NOTA_SVA_GERADA, BoletoTableMap::COL_NOTA_SVA_CANCELADA, BoletoTableMap::COL_TIPO_CONTA, BoletoTableMap::COL_BAIXA_ID, BoletoTableMap::COL_BAIXA_ESTORNO_ID, BoletoTableMap::COL_FORNECEDOR_ID, ),
        self::TYPE_FIELDNAME     => array('idboleto', 'valor', 'vencimento', 'cliente_id', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'import_id', 'competencia_id', 'nosso_numero', 'numero_documento', 'registrado', 'valor_original', 'valor_multa', 'valor_juros', 'valor_desconto', 'valor_pago', 'linha_digitavel', 'data_pagamento', 'esta_pago', 'valor_abatimento', 'valor_iof', 'valor_liquido', 'valor_outras_despesas', 'valor_outros_creditos', 'nota_scm_gerada', 'nota_scm_cancelada', 'base_calculo_scm', 'base_calculo_sva', 'nota_sva_gerada', 'nota_sva_cancelada', 'tipo_conta', 'baixa_id', 'baixa_estorno_id', 'fornecedor_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idboleto' => 0, 'Valor' => 1, 'Vencimento' => 2, 'ClienteId' => 3, 'DataCadastro' => 4, 'DataAlterado' => 5, 'UsuarioAlterado' => 6, 'ImportId' => 7, 'CompetenciaId' => 8, 'NossoNumero' => 9, 'NumeroDocumento' => 10, 'Registrado' => 11, 'ValorOriginal' => 12, 'ValorMulta' => 13, 'ValorJuros' => 14, 'ValorDesconto' => 15, 'ValorPago' => 16, 'LinhaDigitavel' => 17, 'DataPagamento' => 18, 'EstaPago' => 19, 'ValorAbatimento' => 20, 'ValorIof' => 21, 'ValorLiquido' => 22, 'ValorOutrasDespesas' => 23, 'ValorOutrosCreditos' => 24, 'NotaScmGerada' => 25, 'NotaScmCancelada' => 26, 'BaseCalculoScm' => 27, 'BaseCalculoSva' => 28, 'NotaSvaGerada' => 29, 'NotaSvaCancelada' => 30, 'TipoConta' => 31, 'BaixaId' => 32, 'BaixaEstornoId' => 33, 'FornecedorId' => 34, ),
        self::TYPE_CAMELNAME     => array('idboleto' => 0, 'valor' => 1, 'vencimento' => 2, 'clienteId' => 3, 'dataCadastro' => 4, 'dataAlterado' => 5, 'usuarioAlterado' => 6, 'importId' => 7, 'competenciaId' => 8, 'nossoNumero' => 9, 'numeroDocumento' => 10, 'registrado' => 11, 'valorOriginal' => 12, 'valorMulta' => 13, 'valorJuros' => 14, 'valorDesconto' => 15, 'valorPago' => 16, 'linhaDigitavel' => 17, 'dataPagamento' => 18, 'estaPago' => 19, 'valorAbatimento' => 20, 'valorIof' => 21, 'valorLiquido' => 22, 'valorOutrasDespesas' => 23, 'valorOutrosCreditos' => 24, 'notaScmGerada' => 25, 'notaScmCancelada' => 26, 'baseCalculoScm' => 27, 'baseCalculoSva' => 28, 'notaSvaGerada' => 29, 'notaSvaCancelada' => 30, 'tipoConta' => 31, 'baixaId' => 32, 'baixaEstornoId' => 33, 'fornecedorId' => 34, ),
        self::TYPE_COLNAME       => array(BoletoTableMap::COL_IDBOLETO => 0, BoletoTableMap::COL_VALOR => 1, BoletoTableMap::COL_VENCIMENTO => 2, BoletoTableMap::COL_CLIENTE_ID => 3, BoletoTableMap::COL_DATA_CADASTRO => 4, BoletoTableMap::COL_DATA_ALTERADO => 5, BoletoTableMap::COL_USUARIO_ALTERADO => 6, BoletoTableMap::COL_IMPORT_ID => 7, BoletoTableMap::COL_COMPETENCIA_ID => 8, BoletoTableMap::COL_NOSSO_NUMERO => 9, BoletoTableMap::COL_NUMERO_DOCUMENTO => 10, BoletoTableMap::COL_REGISTRADO => 11, BoletoTableMap::COL_VALOR_ORIGINAL => 12, BoletoTableMap::COL_VALOR_MULTA => 13, BoletoTableMap::COL_VALOR_JUROS => 14, BoletoTableMap::COL_VALOR_DESCONTO => 15, BoletoTableMap::COL_VALOR_PAGO => 16, BoletoTableMap::COL_LINHA_DIGITAVEL => 17, BoletoTableMap::COL_DATA_PAGAMENTO => 18, BoletoTableMap::COL_ESTA_PAGO => 19, BoletoTableMap::COL_VALOR_ABATIMENTO => 20, BoletoTableMap::COL_VALOR_IOF => 21, BoletoTableMap::COL_VALOR_LIQUIDO => 22, BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS => 23, BoletoTableMap::COL_VALOR_OUTROS_CREDITOS => 24, BoletoTableMap::COL_NOTA_SCM_GERADA => 25, BoletoTableMap::COL_NOTA_SCM_CANCELADA => 26, BoletoTableMap::COL_BASE_CALCULO_SCM => 27, BoletoTableMap::COL_BASE_CALCULO_SVA => 28, BoletoTableMap::COL_NOTA_SVA_GERADA => 29, BoletoTableMap::COL_NOTA_SVA_CANCELADA => 30, BoletoTableMap::COL_TIPO_CONTA => 31, BoletoTableMap::COL_BAIXA_ID => 32, BoletoTableMap::COL_BAIXA_ESTORNO_ID => 33, BoletoTableMap::COL_FORNECEDOR_ID => 34, ),
        self::TYPE_FIELDNAME     => array('idboleto' => 0, 'valor' => 1, 'vencimento' => 2, 'cliente_id' => 3, 'data_cadastro' => 4, 'data_alterado' => 5, 'usuario_alterado' => 6, 'import_id' => 7, 'competencia_id' => 8, 'nosso_numero' => 9, 'numero_documento' => 10, 'registrado' => 11, 'valor_original' => 12, 'valor_multa' => 13, 'valor_juros' => 14, 'valor_desconto' => 15, 'valor_pago' => 16, 'linha_digitavel' => 17, 'data_pagamento' => 18, 'esta_pago' => 19, 'valor_abatimento' => 20, 'valor_iof' => 21, 'valor_liquido' => 22, 'valor_outras_despesas' => 23, 'valor_outros_creditos' => 24, 'nota_scm_gerada' => 25, 'nota_scm_cancelada' => 26, 'base_calculo_scm' => 27, 'base_calculo_sva' => 28, 'nota_sva_gerada' => 29, 'nota_sva_cancelada' => 30, 'tipo_conta' => 31, 'baixa_id' => 32, 'baixa_estorno_id' => 33, 'fornecedor_id' => 34, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('boleto');
        $this->setPhpName('Boleto');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Boleto');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idboleto', 'Idboleto', 'INTEGER', true, 10, null);
        $this->addColumn('valor', 'Valor', 'DOUBLE', true, null, null);
        $this->addColumn('vencimento', 'Vencimento', 'DATE', true, null, null);
        $this->addForeignKey('cliente_id', 'ClienteId', 'INTEGER', 'cliente', 'idcliente', true, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
        $this->addColumn('import_id', 'ImportId', 'INTEGER', false, 10, null);
        $this->addForeignKey('competencia_id', 'CompetenciaId', 'INTEGER', 'competencia', 'id', true, 10, null);
        $this->addColumn('nosso_numero', 'NossoNumero', 'VARCHAR', false, 13, null);
        $this->addColumn('numero_documento', 'NumeroDocumento', 'VARCHAR', false, 13, null);
        $this->addColumn('registrado', 'Registrado', 'BOOLEAN', true, 1, false);
        $this->addColumn('valor_original', 'ValorOriginal', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_multa', 'ValorMulta', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_juros', 'ValorJuros', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_desconto', 'ValorDesconto', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_pago', 'ValorPago', 'DECIMAL', false, 10, 0);
        $this->addColumn('linha_digitavel', 'LinhaDigitavel', 'VARCHAR', false, 70, null);
        $this->addColumn('data_pagamento', 'DataPagamento', 'TIMESTAMP', false, null, null);
        $this->addColumn('esta_pago', 'EstaPago', 'BOOLEAN', true, 1, false);
        $this->addColumn('valor_abatimento', 'ValorAbatimento', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_iof', 'ValorIof', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_liquido', 'ValorLiquido', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_outras_despesas', 'ValorOutrasDespesas', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_outros_creditos', 'ValorOutrosCreditos', 'DECIMAL', false, 10, 0);
        $this->addColumn('nota_scm_gerada', 'NotaScmGerada', 'BOOLEAN', false, 1, false);
        $this->addColumn('nota_scm_cancelada', 'NotaScmCancelada', 'BOOLEAN', false, 1, false);
        $this->addColumn('base_calculo_scm', 'BaseCalculoScm', 'DECIMAL', false, 10, 0);
        $this->addColumn('base_calculo_sva', 'BaseCalculoSva', 'DECIMAL', false, 10, 0);
        $this->addColumn('nota_sva_gerada', 'NotaSvaGerada', 'BOOLEAN', false, 1, false);
        $this->addColumn('nota_sva_cancelada', 'NotaSvaCancelada', 'BOOLEAN', false, 1, false);
        $this->addColumn('tipo_conta', 'TipoConta', 'CHAR', false, null, 'a_receber');
        $this->addForeignKey('baixa_id', 'BaixaId', 'INTEGER', 'baixa', 'idbaixa', false, 10, null);
        $this->addForeignKey('baixa_estorno_id', 'BaixaEstornoId', 'INTEGER', 'baixa_estorno', 'idbaixa_estorno', false, 10, null);
        $this->addForeignKey('fornecedor_id', 'FornecedorId', 'INTEGER', 'fornecedor', 'idfornecedor', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BaixaEstorno', '\\ImaTelecomBundle\\Model\\BaixaEstorno', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':baixa_estorno_id',
    1 => ':idbaixa_estorno',
  ),
), null, null, null, false);
        $this->addRelation('Baixa', '\\ImaTelecomBundle\\Model\\Baixa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':baixa_id',
    1 => ':idbaixa',
  ),
), null, null, null, false);
        $this->addRelation('Cliente', '\\ImaTelecomBundle\\Model\\Cliente', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':cliente_id',
    1 => ':idcliente',
  ),
), null, null, null, false);
        $this->addRelation('Competencia', '\\ImaTelecomBundle\\Model\\Competencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Fornecedor', '\\ImaTelecomBundle\\Model\\Fornecedor', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':fornecedor_id',
    1 => ':idfornecedor',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('BoletoBaixaHistorico', '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, 'BoletoBaixaHistoricos', false);
        $this->addRelation('BoletoItem', '\\ImaTelecomBundle\\Model\\BoletoItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, 'BoletoItems', false);
        $this->addRelation('DadosFiscal', '\\ImaTelecomBundle\\Model\\DadosFiscal', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, 'DadosFiscals', false);
        $this->addRelation('DadosFiscalItem', '\\ImaTelecomBundle\\Model\\DadosFiscalItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, 'DadosFiscalItems', false);
        $this->addRelation('LancamentosBoletos', '\\ImaTelecomBundle\\Model\\LancamentosBoletos', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, 'LancamentosBoletoss', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idboleto', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idboleto', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idboleto', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idboleto', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idboleto', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idboleto', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idboleto', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BoletoTableMap::CLASS_DEFAULT : BoletoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Boleto object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BoletoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BoletoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BoletoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BoletoTableMap::OM_CLASS;
            /** @var Boleto $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BoletoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BoletoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BoletoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Boleto $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BoletoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BoletoTableMap::COL_IDBOLETO);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR);
            $criteria->addSelectColumn(BoletoTableMap::COL_VENCIMENTO);
            $criteria->addSelectColumn(BoletoTableMap::COL_CLIENTE_ID);
            $criteria->addSelectColumn(BoletoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(BoletoTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(BoletoTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(BoletoTableMap::COL_IMPORT_ID);
            $criteria->addSelectColumn(BoletoTableMap::COL_COMPETENCIA_ID);
            $criteria->addSelectColumn(BoletoTableMap::COL_NOSSO_NUMERO);
            $criteria->addSelectColumn(BoletoTableMap::COL_NUMERO_DOCUMENTO);
            $criteria->addSelectColumn(BoletoTableMap::COL_REGISTRADO);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_ORIGINAL);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_MULTA);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_JUROS);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_DESCONTO);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_PAGO);
            $criteria->addSelectColumn(BoletoTableMap::COL_LINHA_DIGITAVEL);
            $criteria->addSelectColumn(BoletoTableMap::COL_DATA_PAGAMENTO);
            $criteria->addSelectColumn(BoletoTableMap::COL_ESTA_PAGO);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_ABATIMENTO);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_IOF);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_LIQUIDO);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_OUTRAS_DESPESAS);
            $criteria->addSelectColumn(BoletoTableMap::COL_VALOR_OUTROS_CREDITOS);
            $criteria->addSelectColumn(BoletoTableMap::COL_NOTA_SCM_GERADA);
            $criteria->addSelectColumn(BoletoTableMap::COL_NOTA_SCM_CANCELADA);
            $criteria->addSelectColumn(BoletoTableMap::COL_BASE_CALCULO_SCM);
            $criteria->addSelectColumn(BoletoTableMap::COL_BASE_CALCULO_SVA);
            $criteria->addSelectColumn(BoletoTableMap::COL_NOTA_SVA_GERADA);
            $criteria->addSelectColumn(BoletoTableMap::COL_NOTA_SVA_CANCELADA);
            $criteria->addSelectColumn(BoletoTableMap::COL_TIPO_CONTA);
            $criteria->addSelectColumn(BoletoTableMap::COL_BAIXA_ID);
            $criteria->addSelectColumn(BoletoTableMap::COL_BAIXA_ESTORNO_ID);
            $criteria->addSelectColumn(BoletoTableMap::COL_FORNECEDOR_ID);
        } else {
            $criteria->addSelectColumn($alias . '.idboleto');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.vencimento');
            $criteria->addSelectColumn($alias . '.cliente_id');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.import_id');
            $criteria->addSelectColumn($alias . '.competencia_id');
            $criteria->addSelectColumn($alias . '.nosso_numero');
            $criteria->addSelectColumn($alias . '.numero_documento');
            $criteria->addSelectColumn($alias . '.registrado');
            $criteria->addSelectColumn($alias . '.valor_original');
            $criteria->addSelectColumn($alias . '.valor_multa');
            $criteria->addSelectColumn($alias . '.valor_juros');
            $criteria->addSelectColumn($alias . '.valor_desconto');
            $criteria->addSelectColumn($alias . '.valor_pago');
            $criteria->addSelectColumn($alias . '.linha_digitavel');
            $criteria->addSelectColumn($alias . '.data_pagamento');
            $criteria->addSelectColumn($alias . '.esta_pago');
            $criteria->addSelectColumn($alias . '.valor_abatimento');
            $criteria->addSelectColumn($alias . '.valor_iof');
            $criteria->addSelectColumn($alias . '.valor_liquido');
            $criteria->addSelectColumn($alias . '.valor_outras_despesas');
            $criteria->addSelectColumn($alias . '.valor_outros_creditos');
            $criteria->addSelectColumn($alias . '.nota_scm_gerada');
            $criteria->addSelectColumn($alias . '.nota_scm_cancelada');
            $criteria->addSelectColumn($alias . '.base_calculo_scm');
            $criteria->addSelectColumn($alias . '.base_calculo_sva');
            $criteria->addSelectColumn($alias . '.nota_sva_gerada');
            $criteria->addSelectColumn($alias . '.nota_sva_cancelada');
            $criteria->addSelectColumn($alias . '.tipo_conta');
            $criteria->addSelectColumn($alias . '.baixa_id');
            $criteria->addSelectColumn($alias . '.baixa_estorno_id');
            $criteria->addSelectColumn($alias . '.fornecedor_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BoletoTableMap::DATABASE_NAME)->getTable(BoletoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BoletoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BoletoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BoletoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Boleto or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Boleto object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Boleto) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BoletoTableMap::DATABASE_NAME);
            $criteria->add(BoletoTableMap::COL_IDBOLETO, (array) $values, Criteria::IN);
        }

        $query = BoletoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BoletoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BoletoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the boleto table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BoletoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Boleto or Criteria object.
     *
     * @param mixed               $criteria Criteria or Boleto object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Boleto object
        }

        if ($criteria->containsKey(BoletoTableMap::COL_IDBOLETO) && $criteria->keyContainsValue(BoletoTableMap::COL_IDBOLETO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BoletoTableMap::COL_IDBOLETO.')');
        }


        // Set the correct dbName
        $query = BoletoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BoletoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BoletoTableMap::buildTableMap();
