<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\CronTaskArquivos;
use ImaTelecomBundle\Model\CronTaskArquivosQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'cron_task_arquivos' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CronTaskArquivosTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.CronTaskArquivosTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'cron_task_arquivos';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\CronTaskArquivos';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.CronTaskArquivos';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'cron_task_arquivos.id';

    /**
     * the column name for the arquivo field
     */
    const COL_ARQUIVO = 'cron_task_arquivos.arquivo';

    /**
     * the column name for the cron_task_id field
     */
    const COL_CRON_TASK_ID = 'cron_task_arquivos.cron_task_id';

    /**
     * the column name for the executar field
     */
    const COL_EXECUTAR = 'cron_task_arquivos.executar';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'cron_task_arquivos.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'cron_task_arquivos.data_alterado';

    /**
     * the column name for the ordem_execucao field
     */
    const COL_ORDEM_EXECUCAO = 'cron_task_arquivos.ordem_execucao';

    /**
     * the column name for the diretorio_filho field
     */
    const COL_DIRETORIO_FILHO = 'cron_task_arquivos.diretorio_filho';

    /**
     * the column name for the conteudo_script field
     */
    const COL_CONTEUDO_SCRIPT = 'cron_task_arquivos.conteudo_script';

    /**
     * the column name for the tipo_arquivo field
     */
    const COL_TIPO_ARQUIVO = 'cron_task_arquivos.tipo_arquivo';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Arquivo', 'CronTaskId', 'Executar', 'DataCadastro', 'DataAlterado', 'OrdemExecucao', 'DiretorioFilho', 'ConteudoScript', 'TipoArquivo', ),
        self::TYPE_CAMELNAME     => array('id', 'arquivo', 'cronTaskId', 'executar', 'dataCadastro', 'dataAlterado', 'ordemExecucao', 'diretorioFilho', 'conteudoScript', 'tipoArquivo', ),
        self::TYPE_COLNAME       => array(CronTaskArquivosTableMap::COL_ID, CronTaskArquivosTableMap::COL_ARQUIVO, CronTaskArquivosTableMap::COL_CRON_TASK_ID, CronTaskArquivosTableMap::COL_EXECUTAR, CronTaskArquivosTableMap::COL_DATA_CADASTRO, CronTaskArquivosTableMap::COL_DATA_ALTERADO, CronTaskArquivosTableMap::COL_ORDEM_EXECUCAO, CronTaskArquivosTableMap::COL_DIRETORIO_FILHO, CronTaskArquivosTableMap::COL_CONTEUDO_SCRIPT, CronTaskArquivosTableMap::COL_TIPO_ARQUIVO, ),
        self::TYPE_FIELDNAME     => array('id', 'arquivo', 'cron_task_id', 'executar', 'data_cadastro', 'data_alterado', 'ordem_execucao', 'diretorio_filho', 'conteudo_script', 'tipo_arquivo', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Arquivo' => 1, 'CronTaskId' => 2, 'Executar' => 3, 'DataCadastro' => 4, 'DataAlterado' => 5, 'OrdemExecucao' => 6, 'DiretorioFilho' => 7, 'ConteudoScript' => 8, 'TipoArquivo' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'arquivo' => 1, 'cronTaskId' => 2, 'executar' => 3, 'dataCadastro' => 4, 'dataAlterado' => 5, 'ordemExecucao' => 6, 'diretorioFilho' => 7, 'conteudoScript' => 8, 'tipoArquivo' => 9, ),
        self::TYPE_COLNAME       => array(CronTaskArquivosTableMap::COL_ID => 0, CronTaskArquivosTableMap::COL_ARQUIVO => 1, CronTaskArquivosTableMap::COL_CRON_TASK_ID => 2, CronTaskArquivosTableMap::COL_EXECUTAR => 3, CronTaskArquivosTableMap::COL_DATA_CADASTRO => 4, CronTaskArquivosTableMap::COL_DATA_ALTERADO => 5, CronTaskArquivosTableMap::COL_ORDEM_EXECUCAO => 6, CronTaskArquivosTableMap::COL_DIRETORIO_FILHO => 7, CronTaskArquivosTableMap::COL_CONTEUDO_SCRIPT => 8, CronTaskArquivosTableMap::COL_TIPO_ARQUIVO => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'arquivo' => 1, 'cron_task_id' => 2, 'executar' => 3, 'data_cadastro' => 4, 'data_alterado' => 5, 'ordem_execucao' => 6, 'diretorio_filho' => 7, 'conteudo_script' => 8, 'tipo_arquivo' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cron_task_arquivos');
        $this->setPhpName('CronTaskArquivos');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\CronTaskArquivos');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('arquivo', 'Arquivo', 'VARCHAR', true, 250, null);
        $this->addForeignKey('cron_task_id', 'CronTaskId', 'INTEGER', 'cron_task', 'id', true, 10, null);
        $this->addColumn('executar', 'Executar', 'BOOLEAN', true, 1, false);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', false, null, null);
        $this->addColumn('ordem_execucao', 'OrdemExecucao', 'INTEGER', true, 10, 0);
        $this->addColumn('diretorio_filho', 'DiretorioFilho', 'VARCHAR', true, 250, null);
        $this->addColumn('conteudo_script', 'ConteudoScript', 'CLOB', true, null, null);
        $this->addColumn('tipo_arquivo', 'TipoArquivo', 'CHAR', true, null, 'unico');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CronTask', '\\ImaTelecomBundle\\Model\\CronTask', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':cron_task_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CronTaskArquivosTableMap::CLASS_DEFAULT : CronTaskArquivosTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CronTaskArquivos object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CronTaskArquivosTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CronTaskArquivosTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CronTaskArquivosTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CronTaskArquivosTableMap::OM_CLASS;
            /** @var CronTaskArquivos $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CronTaskArquivosTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CronTaskArquivosTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CronTaskArquivosTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CronTaskArquivos $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CronTaskArquivosTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_ID);
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_ARQUIVO);
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_CRON_TASK_ID);
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_EXECUTAR);
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_ORDEM_EXECUCAO);
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_DIRETORIO_FILHO);
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_CONTEUDO_SCRIPT);
            $criteria->addSelectColumn(CronTaskArquivosTableMap::COL_TIPO_ARQUIVO);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.arquivo');
            $criteria->addSelectColumn($alias . '.cron_task_id');
            $criteria->addSelectColumn($alias . '.executar');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.ordem_execucao');
            $criteria->addSelectColumn($alias . '.diretorio_filho');
            $criteria->addSelectColumn($alias . '.conteudo_script');
            $criteria->addSelectColumn($alias . '.tipo_arquivo');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CronTaskArquivosTableMap::DATABASE_NAME)->getTable(CronTaskArquivosTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CronTaskArquivosTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CronTaskArquivosTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CronTaskArquivosTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CronTaskArquivos or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CronTaskArquivos object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskArquivosTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\CronTaskArquivos) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CronTaskArquivosTableMap::DATABASE_NAME);
            $criteria->add(CronTaskArquivosTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CronTaskArquivosQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CronTaskArquivosTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CronTaskArquivosTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the cron_task_arquivos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CronTaskArquivosQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CronTaskArquivos or Criteria object.
     *
     * @param mixed               $criteria Criteria or CronTaskArquivos object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CronTaskArquivosTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CronTaskArquivos object
        }

        if ($criteria->containsKey(CronTaskArquivosTableMap::COL_ID) && $criteria->keyContainsValue(CronTaskArquivosTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CronTaskArquivosTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CronTaskArquivosQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CronTaskArquivosTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CronTaskArquivosTableMap::buildTableMap();
