<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Faturamento;
use ImaTelecomBundle\Model\FaturamentoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'faturamento' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class FaturamentoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.FaturamentoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'faturamento';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Faturamento';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Faturamento';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the idfaturamento field
     */
    const COL_IDFATURAMENTO = 'faturamento.idfaturamento';

    /**
     * the column name for the descricao field
     */
    const COL_DESCRICAO = 'faturamento.descricao';

    /**
     * the column name for the competencia_id field
     */
    const COL_COMPETENCIA_ID = 'faturamento.competencia_id';

    /**
     * the column name for the data_faturamento field
     */
    const COL_DATA_FATURAMENTO = 'faturamento.data_faturamento';

    /**
     * the column name for the qtde_total_contratos field
     */
    const COL_QTDE_TOTAL_CONTRATOS = 'faturamento.qtde_total_contratos';

    /**
     * the column name for the valor_total_contratos field
     */
    const COL_VALOR_TOTAL_CONTRATOS = 'faturamento.valor_total_contratos';

    /**
     * the column name for the qtde_total_contratos_pj field
     */
    const COL_QTDE_TOTAL_CONTRATOS_PJ = 'faturamento.qtde_total_contratos_pj';

    /**
     * the column name for the valor_total_contratos_pj field
     */
    const COL_VALOR_TOTAL_CONTRATOS_PJ = 'faturamento.valor_total_contratos_pj';

    /**
     * the column name for the qtde_total_contratos_pf field
     */
    const COL_QTDE_TOTAL_CONTRATOS_PF = 'faturamento.qtde_total_contratos_pf';

    /**
     * the column name for the valor_total_contratos_pf field
     */
    const COL_VALOR_TOTAL_CONTRATOS_PF = 'faturamento.valor_total_contratos_pf';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'faturamento.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'faturamento.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'faturamento.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idfaturamento', 'Descricao', 'CompetenciaId', 'DataFaturamento', 'QtdeTotalContratos', 'ValorTotalContratos', 'QtdeTotalContratosPj', 'ValorTotalContratosPj', 'QtdeTotalContratosPf', 'ValorTotalContratosPf', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idfaturamento', 'descricao', 'competenciaId', 'dataFaturamento', 'qtdeTotalContratos', 'valorTotalContratos', 'qtdeTotalContratosPj', 'valorTotalContratosPj', 'qtdeTotalContratosPf', 'valorTotalContratosPf', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(FaturamentoTableMap::COL_IDFATURAMENTO, FaturamentoTableMap::COL_DESCRICAO, FaturamentoTableMap::COL_COMPETENCIA_ID, FaturamentoTableMap::COL_DATA_FATURAMENTO, FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS, FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS, FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ, FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ, FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF, FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF, FaturamentoTableMap::COL_DATA_CADASTRO, FaturamentoTableMap::COL_DATA_ALTERADO, FaturamentoTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idfaturamento', 'descricao', 'competencia_id', 'data_faturamento', 'qtde_total_contratos', 'valor_total_contratos', 'qtde_total_contratos_pj', 'valor_total_contratos_pj', 'qtde_total_contratos_pf', 'valor_total_contratos_pf', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idfaturamento' => 0, 'Descricao' => 1, 'CompetenciaId' => 2, 'DataFaturamento' => 3, 'QtdeTotalContratos' => 4, 'ValorTotalContratos' => 5, 'QtdeTotalContratosPj' => 6, 'ValorTotalContratosPj' => 7, 'QtdeTotalContratosPf' => 8, 'ValorTotalContratosPf' => 9, 'DataCadastro' => 10, 'DataAlterado' => 11, 'UsuarioAlterado' => 12, ),
        self::TYPE_CAMELNAME     => array('idfaturamento' => 0, 'descricao' => 1, 'competenciaId' => 2, 'dataFaturamento' => 3, 'qtdeTotalContratos' => 4, 'valorTotalContratos' => 5, 'qtdeTotalContratosPj' => 6, 'valorTotalContratosPj' => 7, 'qtdeTotalContratosPf' => 8, 'valorTotalContratosPf' => 9, 'dataCadastro' => 10, 'dataAlterado' => 11, 'usuarioAlterado' => 12, ),
        self::TYPE_COLNAME       => array(FaturamentoTableMap::COL_IDFATURAMENTO => 0, FaturamentoTableMap::COL_DESCRICAO => 1, FaturamentoTableMap::COL_COMPETENCIA_ID => 2, FaturamentoTableMap::COL_DATA_FATURAMENTO => 3, FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS => 4, FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS => 5, FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ => 6, FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ => 7, FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF => 8, FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF => 9, FaturamentoTableMap::COL_DATA_CADASTRO => 10, FaturamentoTableMap::COL_DATA_ALTERADO => 11, FaturamentoTableMap::COL_USUARIO_ALTERADO => 12, ),
        self::TYPE_FIELDNAME     => array('idfaturamento' => 0, 'descricao' => 1, 'competencia_id' => 2, 'data_faturamento' => 3, 'qtde_total_contratos' => 4, 'valor_total_contratos' => 5, 'qtde_total_contratos_pj' => 6, 'valor_total_contratos_pj' => 7, 'qtde_total_contratos_pf' => 8, 'valor_total_contratos_pf' => 9, 'data_cadastro' => 10, 'data_alterado' => 11, 'usuario_alterado' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('faturamento');
        $this->setPhpName('Faturamento');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Faturamento');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idfaturamento', 'Idfaturamento', 'INTEGER', true, 10, null);
        $this->addColumn('descricao', 'Descricao', 'LONGVARCHAR', true, null, null);
        $this->addForeignKey('competencia_id', 'CompetenciaId', 'INTEGER', 'competencia', 'id', true, 10, null);
        $this->addColumn('data_faturamento', 'DataFaturamento', 'DATE', true, null, null);
        $this->addColumn('qtde_total_contratos', 'QtdeTotalContratos', 'INTEGER', true, 10, null);
        $this->addColumn('valor_total_contratos', 'ValorTotalContratos', 'DECIMAL', true, 10, null);
        $this->addColumn('qtde_total_contratos_pj', 'QtdeTotalContratosPj', 'INTEGER', true, 10, null);
        $this->addColumn('valor_total_contratos_pj', 'ValorTotalContratosPj', 'DECIMAL', true, 10, null);
        $this->addColumn('qtde_total_contratos_pf', 'QtdeTotalContratosPf', 'INTEGER', true, 10, null);
        $this->addColumn('valor_total_contratos_pf', 'ValorTotalContratosPf', 'DECIMAL', true, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addColumn('usuario_alterado', 'UsuarioAlterado', 'INTEGER', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Competencia', '\\ImaTelecomBundle\\Model\\Competencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfaturamento', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfaturamento', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfaturamento', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfaturamento', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfaturamento', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfaturamento', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idfaturamento', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? FaturamentoTableMap::CLASS_DEFAULT : FaturamentoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Faturamento object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = FaturamentoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = FaturamentoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + FaturamentoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = FaturamentoTableMap::OM_CLASS;
            /** @var Faturamento $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            FaturamentoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = FaturamentoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = FaturamentoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Faturamento $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                FaturamentoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(FaturamentoTableMap::COL_IDFATURAMENTO);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_DESCRICAO);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_COMPETENCIA_ID);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_DATA_FATURAMENTO);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PJ);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PJ);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_QTDE_TOTAL_CONTRATOS_PF);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_VALOR_TOTAL_CONTRATOS_PF);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(FaturamentoTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idfaturamento');
            $criteria->addSelectColumn($alias . '.descricao');
            $criteria->addSelectColumn($alias . '.competencia_id');
            $criteria->addSelectColumn($alias . '.data_faturamento');
            $criteria->addSelectColumn($alias . '.qtde_total_contratos');
            $criteria->addSelectColumn($alias . '.valor_total_contratos');
            $criteria->addSelectColumn($alias . '.qtde_total_contratos_pj');
            $criteria->addSelectColumn($alias . '.valor_total_contratos_pj');
            $criteria->addSelectColumn($alias . '.qtde_total_contratos_pf');
            $criteria->addSelectColumn($alias . '.valor_total_contratos_pf');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(FaturamentoTableMap::DATABASE_NAME)->getTable(FaturamentoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(FaturamentoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(FaturamentoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new FaturamentoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Faturamento or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Faturamento object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FaturamentoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Faturamento) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(FaturamentoTableMap::DATABASE_NAME);
            $criteria->add(FaturamentoTableMap::COL_IDFATURAMENTO, (array) $values, Criteria::IN);
        }

        $query = FaturamentoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            FaturamentoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                FaturamentoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the faturamento table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return FaturamentoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Faturamento or Criteria object.
     *
     * @param mixed               $criteria Criteria or Faturamento object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FaturamentoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Faturamento object
        }

        if ($criteria->containsKey(FaturamentoTableMap::COL_IDFATURAMENTO) && $criteria->keyContainsValue(FaturamentoTableMap::COL_IDFATURAMENTO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.FaturamentoTableMap::COL_IDFATURAMENTO.')');
        }


        // Set the correct dbName
        $query = FaturamentoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // FaturamentoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
FaturamentoTableMap::buildTableMap();
