<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Cliente;
use ImaTelecomBundle\Model\ClienteQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'cliente' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ClienteTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ClienteTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'cliente';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Cliente';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Cliente';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the idcliente field
     */
    const COL_IDCLIENTE = 'cliente.idcliente';

    /**
     * the column name for the pessoa_id field
     */
    const COL_PESSOA_ID = 'cliente.pessoa_id';

    /**
     * the column name for the ativo field
     */
    const COL_ATIVO = 'cliente.ativo';

    /**
     * the column name for the data_contrato field
     */
    const COL_DATA_CONTRATO = 'cliente.data_contrato';

    /**
     * the column name for the data_cancelado field
     */
    const COL_DATA_CANCELADO = 'cliente.data_cancelado';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'cliente.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'cliente.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'cliente.usuario_alterado';

    /**
     * the column name for the import_id field
     */
    const COL_IMPORT_ID = 'cliente.import_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idcliente', 'PessoaId', 'Ativo', 'DataContrato', 'DataCancelado', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'ImportId', ),
        self::TYPE_CAMELNAME     => array('idcliente', 'pessoaId', 'ativo', 'dataContrato', 'dataCancelado', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'importId', ),
        self::TYPE_COLNAME       => array(ClienteTableMap::COL_IDCLIENTE, ClienteTableMap::COL_PESSOA_ID, ClienteTableMap::COL_ATIVO, ClienteTableMap::COL_DATA_CONTRATO, ClienteTableMap::COL_DATA_CANCELADO, ClienteTableMap::COL_DATA_CADASTRO, ClienteTableMap::COL_DATA_ALTERADO, ClienteTableMap::COL_USUARIO_ALTERADO, ClienteTableMap::COL_IMPORT_ID, ),
        self::TYPE_FIELDNAME     => array('idcliente', 'pessoa_id', 'ativo', 'data_contrato', 'data_cancelado', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'import_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idcliente' => 0, 'PessoaId' => 1, 'Ativo' => 2, 'DataContrato' => 3, 'DataCancelado' => 4, 'DataCadastro' => 5, 'DataAlterado' => 6, 'UsuarioAlterado' => 7, 'ImportId' => 8, ),
        self::TYPE_CAMELNAME     => array('idcliente' => 0, 'pessoaId' => 1, 'ativo' => 2, 'dataContrato' => 3, 'dataCancelado' => 4, 'dataCadastro' => 5, 'dataAlterado' => 6, 'usuarioAlterado' => 7, 'importId' => 8, ),
        self::TYPE_COLNAME       => array(ClienteTableMap::COL_IDCLIENTE => 0, ClienteTableMap::COL_PESSOA_ID => 1, ClienteTableMap::COL_ATIVO => 2, ClienteTableMap::COL_DATA_CONTRATO => 3, ClienteTableMap::COL_DATA_CANCELADO => 4, ClienteTableMap::COL_DATA_CADASTRO => 5, ClienteTableMap::COL_DATA_ALTERADO => 6, ClienteTableMap::COL_USUARIO_ALTERADO => 7, ClienteTableMap::COL_IMPORT_ID => 8, ),
        self::TYPE_FIELDNAME     => array('idcliente' => 0, 'pessoa_id' => 1, 'ativo' => 2, 'data_contrato' => 3, 'data_cancelado' => 4, 'data_cadastro' => 5, 'data_alterado' => 6, 'usuario_alterado' => 7, 'import_id' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('cliente');
        $this->setPhpName('Cliente');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Cliente');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idcliente', 'Idcliente', 'INTEGER', true, 10, null);
        $this->addForeignKey('pessoa_id', 'PessoaId', 'INTEGER', 'pessoa', 'id', true, 10, null);
        $this->addColumn('ativo', 'Ativo', 'BOOLEAN', true, 1, null);
        $this->addColumn('data_contrato', 'DataContrato', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_cancelado', 'DataCancelado', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
        $this->addColumn('import_id', 'ImportId', 'INTEGER', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Pessoa', '\\ImaTelecomBundle\\Model\\Pessoa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':cliente_id',
    1 => ':idcliente',
  ),
), null, null, 'Boletos', false);
        $this->addRelation('ClienteEstoqueItem', '\\ImaTelecomBundle\\Model\\ClienteEstoqueItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':cliente_id',
    1 => ':idcliente',
  ),
), null, null, 'ClienteEstoqueItems', false);
        $this->addRelation('EnderecoCliente', '\\ImaTelecomBundle\\Model\\EnderecoCliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':cliente_id',
    1 => ':idcliente',
  ),
), null, null, 'EnderecoClientes', false);
        $this->addRelation('EstoqueLancamento', '\\ImaTelecomBundle\\Model\\EstoqueLancamento', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':cliente_id',
    1 => ':idcliente',
  ),
), null, null, 'EstoqueLancamentos', false);
        $this->addRelation('Fornecedor', '\\ImaTelecomBundle\\Model\\Fornecedor', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':cliente_id',
    1 => ':idcliente',
  ),
), null, null, 'Fornecedors', false);
        $this->addRelation('ServicoCliente', '\\ImaTelecomBundle\\Model\\ServicoCliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':cliente_id',
    1 => ':idcliente',
  ),
), null, null, 'ServicoClientes', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcliente', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcliente', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcliente', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcliente', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcliente', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcliente', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idcliente', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ClienteTableMap::CLASS_DEFAULT : ClienteTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Cliente object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ClienteTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ClienteTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ClienteTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ClienteTableMap::OM_CLASS;
            /** @var Cliente $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ClienteTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ClienteTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ClienteTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Cliente $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ClienteTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ClienteTableMap::COL_IDCLIENTE);
            $criteria->addSelectColumn(ClienteTableMap::COL_PESSOA_ID);
            $criteria->addSelectColumn(ClienteTableMap::COL_ATIVO);
            $criteria->addSelectColumn(ClienteTableMap::COL_DATA_CONTRATO);
            $criteria->addSelectColumn(ClienteTableMap::COL_DATA_CANCELADO);
            $criteria->addSelectColumn(ClienteTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(ClienteTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(ClienteTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(ClienteTableMap::COL_IMPORT_ID);
        } else {
            $criteria->addSelectColumn($alias . '.idcliente');
            $criteria->addSelectColumn($alias . '.pessoa_id');
            $criteria->addSelectColumn($alias . '.ativo');
            $criteria->addSelectColumn($alias . '.data_contrato');
            $criteria->addSelectColumn($alias . '.data_cancelado');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.import_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ClienteTableMap::DATABASE_NAME)->getTable(ClienteTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ClienteTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ClienteTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ClienteTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Cliente or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Cliente object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClienteTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Cliente) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ClienteTableMap::DATABASE_NAME);
            $criteria->add(ClienteTableMap::COL_IDCLIENTE, (array) $values, Criteria::IN);
        }

        $query = ClienteQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ClienteTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ClienteTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the cliente table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ClienteQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Cliente or Criteria object.
     *
     * @param mixed               $criteria Criteria or Cliente object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClienteTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Cliente object
        }

        if ($criteria->containsKey(ClienteTableMap::COL_IDCLIENTE) && $criteria->keyContainsValue(ClienteTableMap::COL_IDCLIENTE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ClienteTableMap::COL_IDCLIENTE.')');
        }


        // Set the correct dbName
        $query = ClienteQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ClienteTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ClienteTableMap::buildTableMap();
