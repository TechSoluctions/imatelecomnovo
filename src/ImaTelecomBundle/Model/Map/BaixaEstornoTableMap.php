<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\BaixaEstorno;
use ImaTelecomBundle\Model\BaixaEstornoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'baixa_estorno' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BaixaEstornoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.BaixaEstornoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'baixa_estorno';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\BaixaEstorno';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.BaixaEstorno';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the idbaixa_estorno field
     */
    const COL_IDBAIXA_ESTORNO = 'baixa_estorno.idbaixa_estorno';

    /**
     * the column name for the baixa_id field
     */
    const COL_BAIXA_ID = 'baixa_estorno.baixa_id';

    /**
     * the column name for the conta_caixa_id field
     */
    const COL_CONTA_CAIXA_ID = 'baixa_estorno.conta_caixa_id';

    /**
     * the column name for the competencia_id field
     */
    const COL_COMPETENCIA_ID = 'baixa_estorno.competencia_id';

    /**
     * the column name for the usuario_estorno field
     */
    const COL_USUARIO_ESTORNO = 'baixa_estorno.usuario_estorno';

    /**
     * the column name for the data_estorno field
     */
    const COL_DATA_ESTORNO = 'baixa_estorno.data_estorno';

    /**
     * the column name for the descricao_movimento field
     */
    const COL_DESCRICAO_MOVIMENTO = 'baixa_estorno.descricao_movimento';

    /**
     * the column name for the valor_original field
     */
    const COL_VALOR_ORIGINAL = 'baixa_estorno.valor_original';

    /**
     * the column name for the valor_multa field
     */
    const COL_VALOR_MULTA = 'baixa_estorno.valor_multa';

    /**
     * the column name for the valor_juros field
     */
    const COL_VALOR_JUROS = 'baixa_estorno.valor_juros';

    /**
     * the column name for the valor_desconto field
     */
    const COL_VALOR_DESCONTO = 'baixa_estorno.valor_desconto';

    /**
     * the column name for the valor_total field
     */
    const COL_VALOR_TOTAL = 'baixa_estorno.valor_total';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'baixa_estorno.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'baixa_estorno.data_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdbaixaEstorno', 'BaixaId', 'ContaCaixaId', 'CompetenciaId', 'UsuarioEstorno', 'DataEstorno', 'DescricaoMovimento', 'ValorOriginal', 'ValorMulta', 'ValorJuros', 'ValorDesconto', 'ValorTotal', 'DataCadastro', 'DataAlterado', ),
        self::TYPE_CAMELNAME     => array('idbaixaEstorno', 'baixaId', 'contaCaixaId', 'competenciaId', 'usuarioEstorno', 'dataEstorno', 'descricaoMovimento', 'valorOriginal', 'valorMulta', 'valorJuros', 'valorDesconto', 'valorTotal', 'dataCadastro', 'dataAlterado', ),
        self::TYPE_COLNAME       => array(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, BaixaEstornoTableMap::COL_BAIXA_ID, BaixaEstornoTableMap::COL_CONTA_CAIXA_ID, BaixaEstornoTableMap::COL_COMPETENCIA_ID, BaixaEstornoTableMap::COL_USUARIO_ESTORNO, BaixaEstornoTableMap::COL_DATA_ESTORNO, BaixaEstornoTableMap::COL_DESCRICAO_MOVIMENTO, BaixaEstornoTableMap::COL_VALOR_ORIGINAL, BaixaEstornoTableMap::COL_VALOR_MULTA, BaixaEstornoTableMap::COL_VALOR_JUROS, BaixaEstornoTableMap::COL_VALOR_DESCONTO, BaixaEstornoTableMap::COL_VALOR_TOTAL, BaixaEstornoTableMap::COL_DATA_CADASTRO, BaixaEstornoTableMap::COL_DATA_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idbaixa_estorno', 'baixa_id', 'conta_caixa_id', 'competencia_id', 'usuario_estorno', 'data_estorno', 'descricao_movimento', 'valor_original', 'valor_multa', 'valor_juros', 'valor_desconto', 'valor_total', 'data_cadastro', 'data_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdbaixaEstorno' => 0, 'BaixaId' => 1, 'ContaCaixaId' => 2, 'CompetenciaId' => 3, 'UsuarioEstorno' => 4, 'DataEstorno' => 5, 'DescricaoMovimento' => 6, 'ValorOriginal' => 7, 'ValorMulta' => 8, 'ValorJuros' => 9, 'ValorDesconto' => 10, 'ValorTotal' => 11, 'DataCadastro' => 12, 'DataAlterado' => 13, ),
        self::TYPE_CAMELNAME     => array('idbaixaEstorno' => 0, 'baixaId' => 1, 'contaCaixaId' => 2, 'competenciaId' => 3, 'usuarioEstorno' => 4, 'dataEstorno' => 5, 'descricaoMovimento' => 6, 'valorOriginal' => 7, 'valorMulta' => 8, 'valorJuros' => 9, 'valorDesconto' => 10, 'valorTotal' => 11, 'dataCadastro' => 12, 'dataAlterado' => 13, ),
        self::TYPE_COLNAME       => array(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO => 0, BaixaEstornoTableMap::COL_BAIXA_ID => 1, BaixaEstornoTableMap::COL_CONTA_CAIXA_ID => 2, BaixaEstornoTableMap::COL_COMPETENCIA_ID => 3, BaixaEstornoTableMap::COL_USUARIO_ESTORNO => 4, BaixaEstornoTableMap::COL_DATA_ESTORNO => 5, BaixaEstornoTableMap::COL_DESCRICAO_MOVIMENTO => 6, BaixaEstornoTableMap::COL_VALOR_ORIGINAL => 7, BaixaEstornoTableMap::COL_VALOR_MULTA => 8, BaixaEstornoTableMap::COL_VALOR_JUROS => 9, BaixaEstornoTableMap::COL_VALOR_DESCONTO => 10, BaixaEstornoTableMap::COL_VALOR_TOTAL => 11, BaixaEstornoTableMap::COL_DATA_CADASTRO => 12, BaixaEstornoTableMap::COL_DATA_ALTERADO => 13, ),
        self::TYPE_FIELDNAME     => array('idbaixa_estorno' => 0, 'baixa_id' => 1, 'conta_caixa_id' => 2, 'competencia_id' => 3, 'usuario_estorno' => 4, 'data_estorno' => 5, 'descricao_movimento' => 6, 'valor_original' => 7, 'valor_multa' => 8, 'valor_juros' => 9, 'valor_desconto' => 10, 'valor_total' => 11, 'data_cadastro' => 12, 'data_alterado' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('baixa_estorno');
        $this->setPhpName('BaixaEstorno');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\BaixaEstorno');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idbaixa_estorno', 'IdbaixaEstorno', 'INTEGER', true, null, null);
        $this->addForeignKey('baixa_id', 'BaixaId', 'INTEGER', 'baixa', 'idbaixa', true, null, null);
        $this->addForeignKey('conta_caixa_id', 'ContaCaixaId', 'INTEGER', 'conta_caixa', 'idconta_caixa', true, null, null);
        $this->addForeignKey('competencia_id', 'CompetenciaId', 'INTEGER', 'competencia', 'id', true, null, null);
        $this->addForeignKey('usuario_estorno', 'UsuarioEstorno', 'INTEGER', 'usuario', 'idusuario', true, null, null);
        $this->addColumn('data_estorno', 'DataEstorno', 'TIMESTAMP', true, null, null);
        $this->addColumn('descricao_movimento', 'DescricaoMovimento', 'LONGVARCHAR', false, null, null);
        $this->addColumn('valor_original', 'ValorOriginal', 'DECIMAL', true, 10, 0);
        $this->addColumn('valor_multa', 'ValorMulta', 'DECIMAL', true, 10, 0);
        $this->addColumn('valor_juros', 'ValorJuros', 'DECIMAL', true, 10, 0);
        $this->addColumn('valor_desconto', 'ValorDesconto', 'DECIMAL', true, 10, 0);
        $this->addColumn('valor_total', 'ValorTotal', 'DECIMAL', true, 10, 0);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Baixa', '\\ImaTelecomBundle\\Model\\Baixa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':baixa_id',
    1 => ':idbaixa',
  ),
), null, null, null, false);
        $this->addRelation('Competencia', '\\ImaTelecomBundle\\Model\\Competencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('ContaCaixa', '\\ImaTelecomBundle\\Model\\ContaCaixa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':conta_caixa_id',
    1 => ':idconta_caixa',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_estorno',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':baixa_estorno_id',
    1 => ':idbaixa_estorno',
  ),
), null, null, 'Boletos', false);
        $this->addRelation('BoletoBaixaHistorico', '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':baixa_estorno_id',
    1 => ':idbaixa_estorno',
  ),
), null, null, 'BoletoBaixaHistoricos', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaEstorno', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaEstorno', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaEstorno', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaEstorno', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaEstorno', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdbaixaEstorno', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdbaixaEstorno', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BaixaEstornoTableMap::CLASS_DEFAULT : BaixaEstornoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BaixaEstorno object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BaixaEstornoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BaixaEstornoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BaixaEstornoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BaixaEstornoTableMap::OM_CLASS;
            /** @var BaixaEstorno $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BaixaEstornoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BaixaEstornoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BaixaEstornoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BaixaEstorno $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BaixaEstornoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_BAIXA_ID);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_CONTA_CAIXA_ID);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_COMPETENCIA_ID);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_USUARIO_ESTORNO);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_DATA_ESTORNO);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_DESCRICAO_MOVIMENTO);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_VALOR_ORIGINAL);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_VALOR_MULTA);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_VALOR_JUROS);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_VALOR_DESCONTO);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_VALOR_TOTAL);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(BaixaEstornoTableMap::COL_DATA_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idbaixa_estorno');
            $criteria->addSelectColumn($alias . '.baixa_id');
            $criteria->addSelectColumn($alias . '.conta_caixa_id');
            $criteria->addSelectColumn($alias . '.competencia_id');
            $criteria->addSelectColumn($alias . '.usuario_estorno');
            $criteria->addSelectColumn($alias . '.data_estorno');
            $criteria->addSelectColumn($alias . '.descricao_movimento');
            $criteria->addSelectColumn($alias . '.valor_original');
            $criteria->addSelectColumn($alias . '.valor_multa');
            $criteria->addSelectColumn($alias . '.valor_juros');
            $criteria->addSelectColumn($alias . '.valor_desconto');
            $criteria->addSelectColumn($alias . '.valor_total');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BaixaEstornoTableMap::DATABASE_NAME)->getTable(BaixaEstornoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BaixaEstornoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BaixaEstornoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BaixaEstornoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BaixaEstorno or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BaixaEstorno object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaEstornoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\BaixaEstorno) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BaixaEstornoTableMap::DATABASE_NAME);
            $criteria->add(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO, (array) $values, Criteria::IN);
        }

        $query = BaixaEstornoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BaixaEstornoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BaixaEstornoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the baixa_estorno table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BaixaEstornoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BaixaEstorno or Criteria object.
     *
     * @param mixed               $criteria Criteria or BaixaEstorno object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BaixaEstornoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BaixaEstorno object
        }

        if ($criteria->containsKey(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO) && $criteria->keyContainsValue(BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BaixaEstornoTableMap::COL_IDBAIXA_ESTORNO.')');
        }


        // Set the correct dbName
        $query = BaixaEstornoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BaixaEstornoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaixaEstornoTableMap::buildTableMap();
