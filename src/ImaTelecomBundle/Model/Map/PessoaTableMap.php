<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Pessoa;
use ImaTelecomBundle\Model\PessoaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'pessoa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PessoaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.PessoaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'pessoa';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Pessoa';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Pessoa';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 25;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 25;

    /**
     * the column name for the id field
     */
    const COL_ID = 'pessoa.id';

    /**
     * the column name for the nome field
     */
    const COL_NOME = 'pessoa.nome';

    /**
     * the column name for the razao_social field
     */
    const COL_RAZAO_SOCIAL = 'pessoa.razao_social';

    /**
     * the column name for the rua field
     */
    const COL_RUA = 'pessoa.rua';

    /**
     * the column name for the bairro field
     */
    const COL_BAIRRO = 'pessoa.bairro';

    /**
     * the column name for the cep field
     */
    const COL_CEP = 'pessoa.cep';

    /**
     * the column name for the num field
     */
    const COL_NUM = 'pessoa.num';

    /**
     * the column name for the uf field
     */
    const COL_UF = 'pessoa.uf';

    /**
     * the column name for the cpf_cnpj field
     */
    const COL_CPF_CNPJ = 'pessoa.cpf_cnpj';

    /**
     * the column name for the rg_inscrest field
     */
    const COL_RG_INSCREST = 'pessoa.rg_inscrest';

    /**
     * the column name for the ponto_referencia field
     */
    const COL_PONTO_REFERENCIA = 'pessoa.ponto_referencia';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'pessoa.email';

    /**
     * the column name for the complemento field
     */
    const COL_COMPLEMENTO = 'pessoa.complemento';

    /**
     * the column name for the tipo_pessoa field
     */
    const COL_TIPO_PESSOA = 'pessoa.tipo_pessoa';

    /**
     * the column name for the data_nascimento field
     */
    const COL_DATA_NASCIMENTO = 'pessoa.data_nascimento';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'pessoa.data_cadastro';

    /**
     * the column name for the obs field
     */
    const COL_OBS = 'pessoa.obs';

    /**
     * the column name for the cidade_id field
     */
    const COL_CIDADE_ID = 'pessoa.cidade_id';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'pessoa.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'pessoa.usuario_alterado';

    /**
     * the column name for the responsavel field
     */
    const COL_RESPONSAVEL = 'pessoa.responsavel';

    /**
     * the column name for the contato field
     */
    const COL_CONTATO = 'pessoa.contato';

    /**
     * the column name for the sexo field
     */
    const COL_SEXO = 'pessoa.sexo';

    /**
     * the column name for the import_id field
     */
    const COL_IMPORT_ID = 'pessoa.import_id';

    /**
     * the column name for the e_fornecedor field
     */
    const COL_E_FORNECEDOR = 'pessoa.e_fornecedor';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Nome', 'RazaoSocial', 'Rua', 'Bairro', 'Cep', 'Num', 'Uf', 'CpfCnpj', 'RgInscrest', 'PontoReferencia', 'Email', 'Complemento', 'TipoPessoa', 'DataNascimento', 'DataCadastro', 'Obs', 'CidadeId', 'DataAlterado', 'UsuarioAlterado', 'Responsavel', 'Contato', 'Sexo', 'ImportId', 'EFornecedor', ),
        self::TYPE_CAMELNAME     => array('id', 'nome', 'razaoSocial', 'rua', 'bairro', 'cep', 'num', 'uf', 'cpfCnpj', 'rgInscrest', 'pontoReferencia', 'email', 'complemento', 'tipoPessoa', 'dataNascimento', 'dataCadastro', 'obs', 'cidadeId', 'dataAlterado', 'usuarioAlterado', 'responsavel', 'contato', 'sexo', 'importId', 'eFornecedor', ),
        self::TYPE_COLNAME       => array(PessoaTableMap::COL_ID, PessoaTableMap::COL_NOME, PessoaTableMap::COL_RAZAO_SOCIAL, PessoaTableMap::COL_RUA, PessoaTableMap::COL_BAIRRO, PessoaTableMap::COL_CEP, PessoaTableMap::COL_NUM, PessoaTableMap::COL_UF, PessoaTableMap::COL_CPF_CNPJ, PessoaTableMap::COL_RG_INSCREST, PessoaTableMap::COL_PONTO_REFERENCIA, PessoaTableMap::COL_EMAIL, PessoaTableMap::COL_COMPLEMENTO, PessoaTableMap::COL_TIPO_PESSOA, PessoaTableMap::COL_DATA_NASCIMENTO, PessoaTableMap::COL_DATA_CADASTRO, PessoaTableMap::COL_OBS, PessoaTableMap::COL_CIDADE_ID, PessoaTableMap::COL_DATA_ALTERADO, PessoaTableMap::COL_USUARIO_ALTERADO, PessoaTableMap::COL_RESPONSAVEL, PessoaTableMap::COL_CONTATO, PessoaTableMap::COL_SEXO, PessoaTableMap::COL_IMPORT_ID, PessoaTableMap::COL_E_FORNECEDOR, ),
        self::TYPE_FIELDNAME     => array('id', 'nome', 'razao_social', 'rua', 'bairro', 'cep', 'num', 'uf', 'cpf_cnpj', 'rg_inscrest', 'ponto_referencia', 'email', 'complemento', 'tipo_pessoa', 'data_nascimento', 'data_cadastro', 'obs', 'cidade_id', 'data_alterado', 'usuario_alterado', 'responsavel', 'contato', 'sexo', 'import_id', 'e_fornecedor', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Nome' => 1, 'RazaoSocial' => 2, 'Rua' => 3, 'Bairro' => 4, 'Cep' => 5, 'Num' => 6, 'Uf' => 7, 'CpfCnpj' => 8, 'RgInscrest' => 9, 'PontoReferencia' => 10, 'Email' => 11, 'Complemento' => 12, 'TipoPessoa' => 13, 'DataNascimento' => 14, 'DataCadastro' => 15, 'Obs' => 16, 'CidadeId' => 17, 'DataAlterado' => 18, 'UsuarioAlterado' => 19, 'Responsavel' => 20, 'Contato' => 21, 'Sexo' => 22, 'ImportId' => 23, 'EFornecedor' => 24, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'nome' => 1, 'razaoSocial' => 2, 'rua' => 3, 'bairro' => 4, 'cep' => 5, 'num' => 6, 'uf' => 7, 'cpfCnpj' => 8, 'rgInscrest' => 9, 'pontoReferencia' => 10, 'email' => 11, 'complemento' => 12, 'tipoPessoa' => 13, 'dataNascimento' => 14, 'dataCadastro' => 15, 'obs' => 16, 'cidadeId' => 17, 'dataAlterado' => 18, 'usuarioAlterado' => 19, 'responsavel' => 20, 'contato' => 21, 'sexo' => 22, 'importId' => 23, 'eFornecedor' => 24, ),
        self::TYPE_COLNAME       => array(PessoaTableMap::COL_ID => 0, PessoaTableMap::COL_NOME => 1, PessoaTableMap::COL_RAZAO_SOCIAL => 2, PessoaTableMap::COL_RUA => 3, PessoaTableMap::COL_BAIRRO => 4, PessoaTableMap::COL_CEP => 5, PessoaTableMap::COL_NUM => 6, PessoaTableMap::COL_UF => 7, PessoaTableMap::COL_CPF_CNPJ => 8, PessoaTableMap::COL_RG_INSCREST => 9, PessoaTableMap::COL_PONTO_REFERENCIA => 10, PessoaTableMap::COL_EMAIL => 11, PessoaTableMap::COL_COMPLEMENTO => 12, PessoaTableMap::COL_TIPO_PESSOA => 13, PessoaTableMap::COL_DATA_NASCIMENTO => 14, PessoaTableMap::COL_DATA_CADASTRO => 15, PessoaTableMap::COL_OBS => 16, PessoaTableMap::COL_CIDADE_ID => 17, PessoaTableMap::COL_DATA_ALTERADO => 18, PessoaTableMap::COL_USUARIO_ALTERADO => 19, PessoaTableMap::COL_RESPONSAVEL => 20, PessoaTableMap::COL_CONTATO => 21, PessoaTableMap::COL_SEXO => 22, PessoaTableMap::COL_IMPORT_ID => 23, PessoaTableMap::COL_E_FORNECEDOR => 24, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'nome' => 1, 'razao_social' => 2, 'rua' => 3, 'bairro' => 4, 'cep' => 5, 'num' => 6, 'uf' => 7, 'cpf_cnpj' => 8, 'rg_inscrest' => 9, 'ponto_referencia' => 10, 'email' => 11, 'complemento' => 12, 'tipo_pessoa' => 13, 'data_nascimento' => 14, 'data_cadastro' => 15, 'obs' => 16, 'cidade_id' => 17, 'data_alterado' => 18, 'usuario_alterado' => 19, 'responsavel' => 20, 'contato' => 21, 'sexo' => 22, 'import_id' => 23, 'e_fornecedor' => 24, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('pessoa');
        $this->setPhpName('Pessoa');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Pessoa');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('nome', 'Nome', 'VARCHAR', true, 127, null);
        $this->addColumn('razao_social', 'RazaoSocial', 'VARCHAR', false, 127, null);
        $this->addColumn('rua', 'Rua', 'VARCHAR', false, 255, null);
        $this->addColumn('bairro', 'Bairro', 'VARCHAR', false, 45, null);
        $this->addColumn('cep', 'Cep', 'VARCHAR', false, 10, null);
        $this->addColumn('num', 'Num', 'INTEGER', true, 10, null);
        $this->addColumn('uf', 'Uf', 'CHAR', false, 2, null);
        $this->addColumn('cpf_cnpj', 'CpfCnpj', 'VARCHAR', false, 14, null);
        $this->addColumn('rg_inscrest', 'RgInscrest', 'VARCHAR', false, 45, null);
        $this->addColumn('ponto_referencia', 'PontoReferencia', 'VARCHAR', false, 256, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 45, null);
        $this->addColumn('complemento', 'Complemento', 'VARCHAR', true, 45, null);
        $this->addColumn('tipo_pessoa', 'TipoPessoa', 'CHAR', true, null, null);
        $this->addColumn('data_nascimento', 'DataNascimento', 'DATE', false, null, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('obs', 'Obs', 'LONGVARCHAR', true, null, null);
        $this->addColumn('cidade_id', 'CidadeId', 'INTEGER', true, 10, 1);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('usuario_alterado', 'UsuarioAlterado', 'INTEGER', true, 10, 1);
        $this->addColumn('responsavel', 'Responsavel', 'VARCHAR', false, 127, null);
        $this->addColumn('contato', 'Contato', 'VARCHAR', false, 127, null);
        $this->addColumn('sexo', 'Sexo', 'CHAR', false, null, 'm');
        $this->addColumn('import_id', 'ImportId', 'INTEGER', false, 10, null);
        $this->addColumn('e_fornecedor', 'EFornecedor', 'BOOLEAN', true, 1, false);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BoletoBaixaHistorico', '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, 'BoletoBaixaHistoricos', false);
        $this->addRelation('Cliente', '\\ImaTelecomBundle\\Model\\Cliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, 'Clientes', false);
        $this->addRelation('DadosFiscal', '\\ImaTelecomBundle\\Model\\DadosFiscal', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pessoa_import_id',
    1 => ':import_id',
  ),
), null, null, 'DadosFiscals', false);
        $this->addRelation('Empresa', '\\ImaTelecomBundle\\Model\\Empresa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, 'Empresas', false);
        $this->addRelation('Fornecedor', '\\ImaTelecomBundle\\Model\\Fornecedor', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, 'Fornecedors', false);
        $this->addRelation('Telefone', '\\ImaTelecomBundle\\Model\\Telefone', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, 'Telefones', false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, 'Usuarios', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PessoaTableMap::CLASS_DEFAULT : PessoaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Pessoa object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PessoaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PessoaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PessoaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PessoaTableMap::OM_CLASS;
            /** @var Pessoa $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PessoaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PessoaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PessoaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Pessoa $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PessoaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PessoaTableMap::COL_ID);
            $criteria->addSelectColumn(PessoaTableMap::COL_NOME);
            $criteria->addSelectColumn(PessoaTableMap::COL_RAZAO_SOCIAL);
            $criteria->addSelectColumn(PessoaTableMap::COL_RUA);
            $criteria->addSelectColumn(PessoaTableMap::COL_BAIRRO);
            $criteria->addSelectColumn(PessoaTableMap::COL_CEP);
            $criteria->addSelectColumn(PessoaTableMap::COL_NUM);
            $criteria->addSelectColumn(PessoaTableMap::COL_UF);
            $criteria->addSelectColumn(PessoaTableMap::COL_CPF_CNPJ);
            $criteria->addSelectColumn(PessoaTableMap::COL_RG_INSCREST);
            $criteria->addSelectColumn(PessoaTableMap::COL_PONTO_REFERENCIA);
            $criteria->addSelectColumn(PessoaTableMap::COL_EMAIL);
            $criteria->addSelectColumn(PessoaTableMap::COL_COMPLEMENTO);
            $criteria->addSelectColumn(PessoaTableMap::COL_TIPO_PESSOA);
            $criteria->addSelectColumn(PessoaTableMap::COL_DATA_NASCIMENTO);
            $criteria->addSelectColumn(PessoaTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(PessoaTableMap::COL_OBS);
            $criteria->addSelectColumn(PessoaTableMap::COL_CIDADE_ID);
            $criteria->addSelectColumn(PessoaTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(PessoaTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(PessoaTableMap::COL_RESPONSAVEL);
            $criteria->addSelectColumn(PessoaTableMap::COL_CONTATO);
            $criteria->addSelectColumn(PessoaTableMap::COL_SEXO);
            $criteria->addSelectColumn(PessoaTableMap::COL_IMPORT_ID);
            $criteria->addSelectColumn(PessoaTableMap::COL_E_FORNECEDOR);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.nome');
            $criteria->addSelectColumn($alias . '.razao_social');
            $criteria->addSelectColumn($alias . '.rua');
            $criteria->addSelectColumn($alias . '.bairro');
            $criteria->addSelectColumn($alias . '.cep');
            $criteria->addSelectColumn($alias . '.num');
            $criteria->addSelectColumn($alias . '.uf');
            $criteria->addSelectColumn($alias . '.cpf_cnpj');
            $criteria->addSelectColumn($alias . '.rg_inscrest');
            $criteria->addSelectColumn($alias . '.ponto_referencia');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.complemento');
            $criteria->addSelectColumn($alias . '.tipo_pessoa');
            $criteria->addSelectColumn($alias . '.data_nascimento');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.obs');
            $criteria->addSelectColumn($alias . '.cidade_id');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.responsavel');
            $criteria->addSelectColumn($alias . '.contato');
            $criteria->addSelectColumn($alias . '.sexo');
            $criteria->addSelectColumn($alias . '.import_id');
            $criteria->addSelectColumn($alias . '.e_fornecedor');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PessoaTableMap::DATABASE_NAME)->getTable(PessoaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PessoaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PessoaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PessoaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Pessoa or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Pessoa object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PessoaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Pessoa) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PessoaTableMap::DATABASE_NAME);
            $criteria->add(PessoaTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PessoaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PessoaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PessoaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the pessoa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PessoaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Pessoa or Criteria object.
     *
     * @param mixed               $criteria Criteria or Pessoa object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PessoaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Pessoa object
        }

        if ($criteria->containsKey(PessoaTableMap::COL_ID) && $criteria->keyContainsValue(PessoaTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PessoaTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PessoaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PessoaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PessoaTableMap::buildTableMap();
