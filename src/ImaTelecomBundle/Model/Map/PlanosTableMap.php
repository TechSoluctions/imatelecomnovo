<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Planos;
use ImaTelecomBundle\Model\PlanosQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'planos' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PlanosTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.PlanosTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'planos';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Planos';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Planos';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the idplano field
     */
    const COL_IDPLANO = 'planos.idplano';

    /**
     * the column name for the nome field
     */
    const COL_NOME = 'planos.nome';

    /**
     * the column name for the tipo field
     */
    const COL_TIPO = 'planos.tipo';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'planos.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'planos.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'planos.usuario_alterado';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'planos.valor';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idplano', 'Nome', 'Tipo', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'Valor', ),
        self::TYPE_CAMELNAME     => array('idplano', 'nome', 'tipo', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'valor', ),
        self::TYPE_COLNAME       => array(PlanosTableMap::COL_IDPLANO, PlanosTableMap::COL_NOME, PlanosTableMap::COL_TIPO, PlanosTableMap::COL_DATA_CADASTRO, PlanosTableMap::COL_DATA_ALTERADO, PlanosTableMap::COL_USUARIO_ALTERADO, PlanosTableMap::COL_VALOR, ),
        self::TYPE_FIELDNAME     => array('idplano', 'nome', 'tipo', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'valor', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idplano' => 0, 'Nome' => 1, 'Tipo' => 2, 'DataCadastro' => 3, 'DataAlterado' => 4, 'UsuarioAlterado' => 5, 'Valor' => 6, ),
        self::TYPE_CAMELNAME     => array('idplano' => 0, 'nome' => 1, 'tipo' => 2, 'dataCadastro' => 3, 'dataAlterado' => 4, 'usuarioAlterado' => 5, 'valor' => 6, ),
        self::TYPE_COLNAME       => array(PlanosTableMap::COL_IDPLANO => 0, PlanosTableMap::COL_NOME => 1, PlanosTableMap::COL_TIPO => 2, PlanosTableMap::COL_DATA_CADASTRO => 3, PlanosTableMap::COL_DATA_ALTERADO => 4, PlanosTableMap::COL_USUARIO_ALTERADO => 5, PlanosTableMap::COL_VALOR => 6, ),
        self::TYPE_FIELDNAME     => array('idplano' => 0, 'nome' => 1, 'tipo' => 2, 'data_cadastro' => 3, 'data_alterado' => 4, 'usuario_alterado' => 5, 'valor' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('planos');
        $this->setPhpName('Planos');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Planos');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idplano', 'Idplano', 'INTEGER', true, null, null);
        $this->addColumn('nome', 'Nome', 'VARCHAR', false, 250, null);
        $this->addColumn('tipo', 'Tipo', 'CHAR', false, null, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', false, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', false, null, null);
        $this->addColumn('usuario_alterado', 'UsuarioAlterado', 'INTEGER', false, null, null);
        $this->addColumn('valor', 'Valor', 'DECIMAL', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ServicoPrestado', '\\ImaTelecomBundle\\Model\\ServicoPrestado', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':plano_id',
    1 => ':idplano',
  ),
), null, null, 'ServicoPrestados', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idplano', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idplano', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idplano', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idplano', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idplano', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idplano', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idplano', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PlanosTableMap::CLASS_DEFAULT : PlanosTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Planos object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PlanosTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PlanosTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PlanosTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PlanosTableMap::OM_CLASS;
            /** @var Planos $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PlanosTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PlanosTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PlanosTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Planos $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PlanosTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PlanosTableMap::COL_IDPLANO);
            $criteria->addSelectColumn(PlanosTableMap::COL_NOME);
            $criteria->addSelectColumn(PlanosTableMap::COL_TIPO);
            $criteria->addSelectColumn(PlanosTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(PlanosTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(PlanosTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(PlanosTableMap::COL_VALOR);
        } else {
            $criteria->addSelectColumn($alias . '.idplano');
            $criteria->addSelectColumn($alias . '.nome');
            $criteria->addSelectColumn($alias . '.tipo');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.valor');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PlanosTableMap::DATABASE_NAME)->getTable(PlanosTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PlanosTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PlanosTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PlanosTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Planos or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Planos object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlanosTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Planos) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PlanosTableMap::DATABASE_NAME);
            $criteria->add(PlanosTableMap::COL_IDPLANO, (array) $values, Criteria::IN);
        }

        $query = PlanosQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PlanosTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PlanosTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the planos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PlanosQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Planos or Criteria object.
     *
     * @param mixed               $criteria Criteria or Planos object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PlanosTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Planos object
        }

        if ($criteria->containsKey(PlanosTableMap::COL_IDPLANO) && $criteria->keyContainsValue(PlanosTableMap::COL_IDPLANO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PlanosTableMap::COL_IDPLANO.')');
        }


        // Set the correct dbName
        $query = PlanosQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PlanosTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PlanosTableMap::buildTableMap();
