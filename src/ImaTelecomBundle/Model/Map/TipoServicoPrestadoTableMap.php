<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\TipoServicoPrestado;
use ImaTelecomBundle\Model\TipoServicoPrestadoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'tipo_servico_prestado' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class TipoServicoPrestadoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.TipoServicoPrestadoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'tipo_servico_prestado';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\TipoServicoPrestado';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.TipoServicoPrestado';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the idtipo_servico_prestado field
     */
    const COL_IDTIPO_SERVICO_PRESTADO = 'tipo_servico_prestado.idtipo_servico_prestado';

    /**
     * the column name for the tipo field
     */
    const COL_TIPO = 'tipo_servico_prestado.tipo';

    /**
     * the column name for the pre_pago field
     */
    const COL_PRE_PAGO = 'tipo_servico_prestado.pre_pago';

    /**
     * the column name for the natureza_operacao field
     */
    const COL_NATUREZA_OPERACAO = 'tipo_servico_prestado.natureza_operacao';

    /**
     * the column name for the item_lista_servico field
     */
    const COL_ITEM_LISTA_SERVICO = 'tipo_servico_prestado.item_lista_servico';

    /**
     * the column name for the codigo_tributacao field
     */
    const COL_CODIGO_TRIBUTACAO = 'tipo_servico_prestado.codigo_tributacao';

    /**
     * the column name for the aliquota field
     */
    const COL_ALIQUOTA = 'tipo_servico_prestado.aliquota';

    /**
     * the column name for the unidade_negocio field
     */
    const COL_UNIDADE_NEGOCIO = 'tipo_servico_prestado.unidade_negocio';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdtipoServicoPrestado', 'Tipo', 'PrePago', 'NaturezaOperacao', 'ItemListaServico', 'CodigoTributacao', 'Aliquota', 'UnidadeNegocio', ),
        self::TYPE_CAMELNAME     => array('idtipoServicoPrestado', 'tipo', 'prePago', 'naturezaOperacao', 'itemListaServico', 'codigoTributacao', 'aliquota', 'unidadeNegocio', ),
        self::TYPE_COLNAME       => array(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, TipoServicoPrestadoTableMap::COL_TIPO, TipoServicoPrestadoTableMap::COL_PRE_PAGO, TipoServicoPrestadoTableMap::COL_NATUREZA_OPERACAO, TipoServicoPrestadoTableMap::COL_ITEM_LISTA_SERVICO, TipoServicoPrestadoTableMap::COL_CODIGO_TRIBUTACAO, TipoServicoPrestadoTableMap::COL_ALIQUOTA, TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO, ),
        self::TYPE_FIELDNAME     => array('idtipo_servico_prestado', 'tipo', 'pre_pago', 'natureza_operacao', 'item_lista_servico', 'codigo_tributacao', 'aliquota', 'unidade_negocio', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdtipoServicoPrestado' => 0, 'Tipo' => 1, 'PrePago' => 2, 'NaturezaOperacao' => 3, 'ItemListaServico' => 4, 'CodigoTributacao' => 5, 'Aliquota' => 6, 'UnidadeNegocio' => 7, ),
        self::TYPE_CAMELNAME     => array('idtipoServicoPrestado' => 0, 'tipo' => 1, 'prePago' => 2, 'naturezaOperacao' => 3, 'itemListaServico' => 4, 'codigoTributacao' => 5, 'aliquota' => 6, 'unidadeNegocio' => 7, ),
        self::TYPE_COLNAME       => array(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO => 0, TipoServicoPrestadoTableMap::COL_TIPO => 1, TipoServicoPrestadoTableMap::COL_PRE_PAGO => 2, TipoServicoPrestadoTableMap::COL_NATUREZA_OPERACAO => 3, TipoServicoPrestadoTableMap::COL_ITEM_LISTA_SERVICO => 4, TipoServicoPrestadoTableMap::COL_CODIGO_TRIBUTACAO => 5, TipoServicoPrestadoTableMap::COL_ALIQUOTA => 6, TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO => 7, ),
        self::TYPE_FIELDNAME     => array('idtipo_servico_prestado' => 0, 'tipo' => 1, 'pre_pago' => 2, 'natureza_operacao' => 3, 'item_lista_servico' => 4, 'codigo_tributacao' => 5, 'aliquota' => 6, 'unidade_negocio' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tipo_servico_prestado');
        $this->setPhpName('TipoServicoPrestado');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\TipoServicoPrestado');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idtipo_servico_prestado', 'IdtipoServicoPrestado', 'INTEGER', true, 10, null);
        $this->addColumn('tipo', 'Tipo', 'VARCHAR', true, 45, null);
        $this->addColumn('pre_pago', 'PrePago', 'BOOLEAN', true, 1, null);
        $this->addColumn('natureza_operacao', 'NaturezaOperacao', 'VARCHAR', true, 2, '4');
        $this->addColumn('item_lista_servico', 'ItemListaServico', 'VARCHAR', true, 4, '1.07');
        $this->addColumn('codigo_tributacao', 'CodigoTributacao', 'VARCHAR', true, 9, '619060100');
        $this->addColumn('aliquota', 'Aliquota', 'DOUBLE', true, null, 2);
        $this->addColumn('unidade_negocio', 'UnidadeNegocio', 'INTEGER', true, 10, 1);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ServicoPrestado', '\\ImaTelecomBundle\\Model\\ServicoPrestado', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':tipo_servico_prestado_id',
    1 => ':idtipo_servico_prestado',
  ),
), null, null, 'ServicoPrestados', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdtipoServicoPrestado', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdtipoServicoPrestado', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdtipoServicoPrestado', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdtipoServicoPrestado', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdtipoServicoPrestado', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdtipoServicoPrestado', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdtipoServicoPrestado', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? TipoServicoPrestadoTableMap::CLASS_DEFAULT : TipoServicoPrestadoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (TipoServicoPrestado object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = TipoServicoPrestadoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = TipoServicoPrestadoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + TipoServicoPrestadoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = TipoServicoPrestadoTableMap::OM_CLASS;
            /** @var TipoServicoPrestado $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            TipoServicoPrestadoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = TipoServicoPrestadoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = TipoServicoPrestadoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var TipoServicoPrestado $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                TipoServicoPrestadoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO);
            $criteria->addSelectColumn(TipoServicoPrestadoTableMap::COL_TIPO);
            $criteria->addSelectColumn(TipoServicoPrestadoTableMap::COL_PRE_PAGO);
            $criteria->addSelectColumn(TipoServicoPrestadoTableMap::COL_NATUREZA_OPERACAO);
            $criteria->addSelectColumn(TipoServicoPrestadoTableMap::COL_ITEM_LISTA_SERVICO);
            $criteria->addSelectColumn(TipoServicoPrestadoTableMap::COL_CODIGO_TRIBUTACAO);
            $criteria->addSelectColumn(TipoServicoPrestadoTableMap::COL_ALIQUOTA);
            $criteria->addSelectColumn(TipoServicoPrestadoTableMap::COL_UNIDADE_NEGOCIO);
        } else {
            $criteria->addSelectColumn($alias . '.idtipo_servico_prestado');
            $criteria->addSelectColumn($alias . '.tipo');
            $criteria->addSelectColumn($alias . '.pre_pago');
            $criteria->addSelectColumn($alias . '.natureza_operacao');
            $criteria->addSelectColumn($alias . '.item_lista_servico');
            $criteria->addSelectColumn($alias . '.codigo_tributacao');
            $criteria->addSelectColumn($alias . '.aliquota');
            $criteria->addSelectColumn($alias . '.unidade_negocio');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(TipoServicoPrestadoTableMap::DATABASE_NAME)->getTable(TipoServicoPrestadoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(TipoServicoPrestadoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(TipoServicoPrestadoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new TipoServicoPrestadoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a TipoServicoPrestado or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or TipoServicoPrestado object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoServicoPrestadoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\TipoServicoPrestado) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(TipoServicoPrestadoTableMap::DATABASE_NAME);
            $criteria->add(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO, (array) $values, Criteria::IN);
        }

        $query = TipoServicoPrestadoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            TipoServicoPrestadoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                TipoServicoPrestadoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the tipo_servico_prestado table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return TipoServicoPrestadoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a TipoServicoPrestado or Criteria object.
     *
     * @param mixed               $criteria Criteria or TipoServicoPrestado object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoServicoPrestadoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from TipoServicoPrestado object
        }

        if ($criteria->containsKey(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO) && $criteria->keyContainsValue(TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.TipoServicoPrestadoTableMap::COL_IDTIPO_SERVICO_PRESTADO.')');
        }


        // Set the correct dbName
        $query = TipoServicoPrestadoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // TipoServicoPrestadoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
TipoServicoPrestadoTableMap::buildTableMap();
