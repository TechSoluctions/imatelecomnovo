<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\ClassificacaoFinanceira;
use ImaTelecomBundle\Model\ClassificacaoFinanceiraQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'classificacao_financeira' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ClassificacaoFinanceiraTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ClassificacaoFinanceiraTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'classificacao_financeira';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\ClassificacaoFinanceira';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.ClassificacaoFinanceira';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 4;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 4;

    /**
     * the column name for the idclassificacao_financeira field
     */
    const COL_IDCLASSIFICACAO_FINANCEIRA = 'classificacao_financeira.idclassificacao_financeira';

    /**
     * the column name for the codigo field
     */
    const COL_CODIGO = 'classificacao_financeira.codigo';

    /**
     * the column name for the descricao field
     */
    const COL_DESCRICAO = 'classificacao_financeira.descricao';

    /**
     * the column name for the pai field
     */
    const COL_PAI = 'classificacao_financeira.pai';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdclassificacaoFinanceira', 'Codigo', 'Descricao', 'Pai', ),
        self::TYPE_CAMELNAME     => array('idclassificacaoFinanceira', 'codigo', 'descricao', 'pai', ),
        self::TYPE_COLNAME       => array(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA, ClassificacaoFinanceiraTableMap::COL_CODIGO, ClassificacaoFinanceiraTableMap::COL_DESCRICAO, ClassificacaoFinanceiraTableMap::COL_PAI, ),
        self::TYPE_FIELDNAME     => array('idclassificacao_financeira', 'codigo', 'descricao', 'pai', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdclassificacaoFinanceira' => 0, 'Codigo' => 1, 'Descricao' => 2, 'Pai' => 3, ),
        self::TYPE_CAMELNAME     => array('idclassificacaoFinanceira' => 0, 'codigo' => 1, 'descricao' => 2, 'pai' => 3, ),
        self::TYPE_COLNAME       => array(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA => 0, ClassificacaoFinanceiraTableMap::COL_CODIGO => 1, ClassificacaoFinanceiraTableMap::COL_DESCRICAO => 2, ClassificacaoFinanceiraTableMap::COL_PAI => 3, ),
        self::TYPE_FIELDNAME     => array('idclassificacao_financeira' => 0, 'codigo' => 1, 'descricao' => 2, 'pai' => 3, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('classificacao_financeira');
        $this->setPhpName('ClassificacaoFinanceira');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\ClassificacaoFinanceira');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idclassificacao_financeira', 'IdclassificacaoFinanceira', 'INTEGER', true, 10, null);
        $this->addColumn('codigo', 'Codigo', 'VARCHAR', true, 15, null);
        $this->addColumn('descricao', 'Descricao', 'VARCHAR', true, 80, null);
        $this->addColumn('pai', 'Pai', 'INTEGER', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ItemDeCompra', '\\ImaTelecomBundle\\Model\\ItemDeCompra', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':classificacao_financeira_id',
    1 => ':idclassificacao_financeira',
  ),
), null, null, 'ItemDeCompras', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdclassificacaoFinanceira', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdclassificacaoFinanceira', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdclassificacaoFinanceira', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdclassificacaoFinanceira', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdclassificacaoFinanceira', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdclassificacaoFinanceira', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdclassificacaoFinanceira', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ClassificacaoFinanceiraTableMap::CLASS_DEFAULT : ClassificacaoFinanceiraTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ClassificacaoFinanceira object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ClassificacaoFinanceiraTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ClassificacaoFinanceiraTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ClassificacaoFinanceiraTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ClassificacaoFinanceiraTableMap::OM_CLASS;
            /** @var ClassificacaoFinanceira $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ClassificacaoFinanceiraTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ClassificacaoFinanceiraTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ClassificacaoFinanceiraTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ClassificacaoFinanceira $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ClassificacaoFinanceiraTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA);
            $criteria->addSelectColumn(ClassificacaoFinanceiraTableMap::COL_CODIGO);
            $criteria->addSelectColumn(ClassificacaoFinanceiraTableMap::COL_DESCRICAO);
            $criteria->addSelectColumn(ClassificacaoFinanceiraTableMap::COL_PAI);
        } else {
            $criteria->addSelectColumn($alias . '.idclassificacao_financeira');
            $criteria->addSelectColumn($alias . '.codigo');
            $criteria->addSelectColumn($alias . '.descricao');
            $criteria->addSelectColumn($alias . '.pai');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ClassificacaoFinanceiraTableMap::DATABASE_NAME)->getTable(ClassificacaoFinanceiraTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ClassificacaoFinanceiraTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ClassificacaoFinanceiraTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ClassificacaoFinanceiraTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ClassificacaoFinanceira or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ClassificacaoFinanceira object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassificacaoFinanceiraTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\ClassificacaoFinanceira) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ClassificacaoFinanceiraTableMap::DATABASE_NAME);
            $criteria->add(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA, (array) $values, Criteria::IN);
        }

        $query = ClassificacaoFinanceiraQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ClassificacaoFinanceiraTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ClassificacaoFinanceiraTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the classificacao_financeira table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ClassificacaoFinanceiraQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ClassificacaoFinanceira or Criteria object.
     *
     * @param mixed               $criteria Criteria or ClassificacaoFinanceira object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ClassificacaoFinanceiraTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ClassificacaoFinanceira object
        }

        if ($criteria->containsKey(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA) && $criteria->keyContainsValue(ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ClassificacaoFinanceiraTableMap::COL_IDCLASSIFICACAO_FINANCEIRA.')');
        }


        // Set the correct dbName
        $query = ClassificacaoFinanceiraQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ClassificacaoFinanceiraTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ClassificacaoFinanceiraTableMap::buildTableMap();
