<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\ViewFaturamentosMensalSuc;
use ImaTelecomBundle\Model\ViewFaturamentosMensalSucQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'view_faturamentos_mensal_suc' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ViewFaturamentosMensalSucTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ViewFaturamentosMensalSucTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'view_faturamentos_mensal_suc';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\ViewFaturamentosMensalSuc';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.ViewFaturamentosMensalSuc';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the idview_faturamentos_mensal field
     */
    const COL_IDVIEW_FATURAMENTOS_MENSAL = 'view_faturamentos_mensal_suc.idview_faturamentos_mensal';

    /**
     * the column name for the boleto field
     */
    const COL_BOLETO = 'view_faturamentos_mensal_suc.boleto';

    /**
     * the column name for the pagamento_parcelas field
     */
    const COL_PAGAMENTO_PARCELAS = 'view_faturamentos_mensal_suc.pagamento_parcelas';

    /**
     * the column name for the vencimento field
     */
    const COL_VENCIMENTO = 'view_faturamentos_mensal_suc.vencimento';

    /**
     * the column name for the servico_contratado field
     */
    const COL_SERVICO_CONTRATADO = 'view_faturamentos_mensal_suc.servico_contratado';

    /**
     * the column name for the valor_parcela field
     */
    const COL_VALOR_PARCELA = 'view_faturamentos_mensal_suc.valor_parcela';

    /**
     * the column name for the cliente field
     */
    const COL_CLIENTE = 'view_faturamentos_mensal_suc.cliente';

    /**
     * the column name for the nome field
     */
    const COL_NOME = 'view_faturamentos_mensal_suc.nome';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdviewFaturamentosMensal', 'Boleto', 'PagamentoParcelas', 'Vencimento', 'ServicoContratado', 'ValorParcela', 'Cliente', 'Nome', ),
        self::TYPE_CAMELNAME     => array('idviewFaturamentosMensal', 'boleto', 'pagamentoParcelas', 'vencimento', 'servicoContratado', 'valorParcela', 'cliente', 'nome', ),
        self::TYPE_COLNAME       => array(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL, ViewFaturamentosMensalSucTableMap::COL_BOLETO, ViewFaturamentosMensalSucTableMap::COL_PAGAMENTO_PARCELAS, ViewFaturamentosMensalSucTableMap::COL_VENCIMENTO, ViewFaturamentosMensalSucTableMap::COL_SERVICO_CONTRATADO, ViewFaturamentosMensalSucTableMap::COL_VALOR_PARCELA, ViewFaturamentosMensalSucTableMap::COL_CLIENTE, ViewFaturamentosMensalSucTableMap::COL_NOME, ),
        self::TYPE_FIELDNAME     => array('idview_faturamentos_mensal', 'boleto', 'pagamento_parcelas', 'vencimento', 'servico_contratado', 'valor_parcela', 'cliente', 'nome', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdviewFaturamentosMensal' => 0, 'Boleto' => 1, 'PagamentoParcelas' => 2, 'Vencimento' => 3, 'ServicoContratado' => 4, 'ValorParcela' => 5, 'Cliente' => 6, 'Nome' => 7, ),
        self::TYPE_CAMELNAME     => array('idviewFaturamentosMensal' => 0, 'boleto' => 1, 'pagamentoParcelas' => 2, 'vencimento' => 3, 'servicoContratado' => 4, 'valorParcela' => 5, 'cliente' => 6, 'nome' => 7, ),
        self::TYPE_COLNAME       => array(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL => 0, ViewFaturamentosMensalSucTableMap::COL_BOLETO => 1, ViewFaturamentosMensalSucTableMap::COL_PAGAMENTO_PARCELAS => 2, ViewFaturamentosMensalSucTableMap::COL_VENCIMENTO => 3, ViewFaturamentosMensalSucTableMap::COL_SERVICO_CONTRATADO => 4, ViewFaturamentosMensalSucTableMap::COL_VALOR_PARCELA => 5, ViewFaturamentosMensalSucTableMap::COL_CLIENTE => 6, ViewFaturamentosMensalSucTableMap::COL_NOME => 7, ),
        self::TYPE_FIELDNAME     => array('idview_faturamentos_mensal' => 0, 'boleto' => 1, 'pagamento_parcelas' => 2, 'vencimento' => 3, 'servico_contratado' => 4, 'valor_parcela' => 5, 'cliente' => 6, 'nome' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('view_faturamentos_mensal_suc');
        $this->setPhpName('ViewFaturamentosMensalSuc');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\ViewFaturamentosMensalSuc');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idview_faturamentos_mensal', 'IdviewFaturamentosMensal', 'INTEGER', true, 10, null);
        $this->addColumn('boleto', 'Boleto', 'INTEGER', true, 10, null);
        $this->addColumn('pagamento_parcelas', 'PagamentoParcelas', 'INTEGER', true, 10, null);
        $this->addColumn('vencimento', 'Vencimento', 'VARCHAR', true, 45, null);
        $this->addColumn('servico_contratado', 'ServicoContratado', 'INTEGER', true, 10, null);
        $this->addColumn('valor_parcela', 'ValorParcela', 'DECIMAL', true, 10, null);
        $this->addColumn('cliente', 'Cliente', 'INTEGER', true, 10, null);
        $this->addColumn('nome', 'Nome', 'VARCHAR', true, 250, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdviewFaturamentosMensal', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdviewFaturamentosMensal', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdviewFaturamentosMensal', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdviewFaturamentosMensal', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdviewFaturamentosMensal', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdviewFaturamentosMensal', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdviewFaturamentosMensal', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ViewFaturamentosMensalSucTableMap::CLASS_DEFAULT : ViewFaturamentosMensalSucTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ViewFaturamentosMensalSuc object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ViewFaturamentosMensalSucTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ViewFaturamentosMensalSucTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ViewFaturamentosMensalSucTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ViewFaturamentosMensalSucTableMap::OM_CLASS;
            /** @var ViewFaturamentosMensalSuc $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ViewFaturamentosMensalSucTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ViewFaturamentosMensalSucTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ViewFaturamentosMensalSucTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ViewFaturamentosMensalSuc $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ViewFaturamentosMensalSucTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL);
            $criteria->addSelectColumn(ViewFaturamentosMensalSucTableMap::COL_BOLETO);
            $criteria->addSelectColumn(ViewFaturamentosMensalSucTableMap::COL_PAGAMENTO_PARCELAS);
            $criteria->addSelectColumn(ViewFaturamentosMensalSucTableMap::COL_VENCIMENTO);
            $criteria->addSelectColumn(ViewFaturamentosMensalSucTableMap::COL_SERVICO_CONTRATADO);
            $criteria->addSelectColumn(ViewFaturamentosMensalSucTableMap::COL_VALOR_PARCELA);
            $criteria->addSelectColumn(ViewFaturamentosMensalSucTableMap::COL_CLIENTE);
            $criteria->addSelectColumn(ViewFaturamentosMensalSucTableMap::COL_NOME);
        } else {
            $criteria->addSelectColumn($alias . '.idview_faturamentos_mensal');
            $criteria->addSelectColumn($alias . '.boleto');
            $criteria->addSelectColumn($alias . '.pagamento_parcelas');
            $criteria->addSelectColumn($alias . '.vencimento');
            $criteria->addSelectColumn($alias . '.servico_contratado');
            $criteria->addSelectColumn($alias . '.valor_parcela');
            $criteria->addSelectColumn($alias . '.cliente');
            $criteria->addSelectColumn($alias . '.nome');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ViewFaturamentosMensalSucTableMap::DATABASE_NAME)->getTable(ViewFaturamentosMensalSucTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ViewFaturamentosMensalSucTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ViewFaturamentosMensalSucTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ViewFaturamentosMensalSucTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ViewFaturamentosMensalSuc or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ViewFaturamentosMensalSuc object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ViewFaturamentosMensalSucTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\ViewFaturamentosMensalSuc) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ViewFaturamentosMensalSucTableMap::DATABASE_NAME);
            $criteria->add(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL, (array) $values, Criteria::IN);
        }

        $query = ViewFaturamentosMensalSucQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ViewFaturamentosMensalSucTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ViewFaturamentosMensalSucTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the view_faturamentos_mensal_suc table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ViewFaturamentosMensalSucQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ViewFaturamentosMensalSuc or Criteria object.
     *
     * @param mixed               $criteria Criteria or ViewFaturamentosMensalSuc object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ViewFaturamentosMensalSucTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ViewFaturamentosMensalSuc object
        }

        if ($criteria->containsKey(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL) && $criteria->keyContainsValue(ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ViewFaturamentosMensalSucTableMap::COL_IDVIEW_FATURAMENTOS_MENSAL.')');
        }


        // Set the correct dbName
        $query = ViewFaturamentosMensalSucQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ViewFaturamentosMensalSucTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ViewFaturamentosMensalSucTableMap::buildTableMap();
