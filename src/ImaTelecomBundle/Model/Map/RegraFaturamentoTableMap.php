<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\RegraFaturamento;
use ImaTelecomBundle\Model\RegraFaturamentoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'regra_faturamento' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class RegraFaturamentoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.RegraFaturamentoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'regra_faturamento';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\RegraFaturamento';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.RegraFaturamento';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the idregra_faturamento field
     */
    const COL_IDREGRA_FATURAMENTO = 'regra_faturamento.idregra_faturamento';

    /**
     * the column name for the descricao field
     */
    const COL_DESCRICAO = 'regra_faturamento.descricao';

    /**
     * the column name for the periodo1_dia_vencimento_cobranca field
     */
    const COL_PERIODO1_DIA_VENCIMENTO_COBRANCA = 'regra_faturamento.periodo1_dia_vencimento_cobranca';

    /**
     * the column name for the periodo1_dia_inicial_cobranca field
     */
    const COL_PERIODO1_DIA_INICIAL_COBRANCA = 'regra_faturamento.periodo1_dia_inicial_cobranca';

    /**
     * the column name for the periodo1_dia_final_cobranca field
     */
    const COL_PERIODO1_DIA_FINAL_COBRANCA = 'regra_faturamento.periodo1_dia_final_cobranca';

    /**
     * the column name for the mes_subsequente field
     */
    const COL_MES_SUBSEQUENTE = 'regra_faturamento.mes_subsequente';

    /**
     * the column name for the periodo2_dia_vencimento_cobranca field
     */
    const COL_PERIODO2_DIA_VENCIMENTO_COBRANCA = 'regra_faturamento.periodo2_dia_vencimento_cobranca';

    /**
     * the column name for the periodo2_dia_inicial_cobranca field
     */
    const COL_PERIODO2_DIA_INICIAL_COBRANCA = 'regra_faturamento.periodo2_dia_inicial_cobranca';

    /**
     * the column name for the periodo2_dia_final_cobranca field
     */
    const COL_PERIODO2_DIA_FINAL_COBRANCA = 'regra_faturamento.periodo2_dia_final_cobranca';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'regra_faturamento.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'regra_faturamento.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'regra_faturamento.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdregraFaturamento', 'Descricao', 'Periodo1DiaVencimentoCobranca', 'Periodo1DiaInicialCobranca', 'Periodo1DiaFinalCobranca', 'MesSubsequente', 'Periodo2DiaVencimentoCobranca', 'Periodo2DiaInicialCobranca', 'Periodo2DiaFinalCobranca', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idregraFaturamento', 'descricao', 'periodo1DiaVencimentoCobranca', 'periodo1DiaInicialCobranca', 'periodo1DiaFinalCobranca', 'mesSubsequente', 'periodo2DiaVencimentoCobranca', 'periodo2DiaInicialCobranca', 'periodo2DiaFinalCobranca', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, RegraFaturamentoTableMap::COL_DESCRICAO, RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA, RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA, RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA, RegraFaturamentoTableMap::COL_MES_SUBSEQUENTE, RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA, RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA, RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA, RegraFaturamentoTableMap::COL_DATA_CADASTRO, RegraFaturamentoTableMap::COL_DATA_ALTERADO, RegraFaturamentoTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idregra_faturamento', 'descricao', 'periodo1_dia_vencimento_cobranca', 'periodo1_dia_inicial_cobranca', 'periodo1_dia_final_cobranca', 'mes_subsequente', 'periodo2_dia_vencimento_cobranca', 'periodo2_dia_inicial_cobranca', 'periodo2_dia_final_cobranca', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdregraFaturamento' => 0, 'Descricao' => 1, 'Periodo1DiaVencimentoCobranca' => 2, 'Periodo1DiaInicialCobranca' => 3, 'Periodo1DiaFinalCobranca' => 4, 'MesSubsequente' => 5, 'Periodo2DiaVencimentoCobranca' => 6, 'Periodo2DiaInicialCobranca' => 7, 'Periodo2DiaFinalCobranca' => 8, 'DataCadastro' => 9, 'DataAlterado' => 10, 'UsuarioAlterado' => 11, ),
        self::TYPE_CAMELNAME     => array('idregraFaturamento' => 0, 'descricao' => 1, 'periodo1DiaVencimentoCobranca' => 2, 'periodo1DiaInicialCobranca' => 3, 'periodo1DiaFinalCobranca' => 4, 'mesSubsequente' => 5, 'periodo2DiaVencimentoCobranca' => 6, 'periodo2DiaInicialCobranca' => 7, 'periodo2DiaFinalCobranca' => 8, 'dataCadastro' => 9, 'dataAlterado' => 10, 'usuarioAlterado' => 11, ),
        self::TYPE_COLNAME       => array(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO => 0, RegraFaturamentoTableMap::COL_DESCRICAO => 1, RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA => 2, RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA => 3, RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA => 4, RegraFaturamentoTableMap::COL_MES_SUBSEQUENTE => 5, RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA => 6, RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA => 7, RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA => 8, RegraFaturamentoTableMap::COL_DATA_CADASTRO => 9, RegraFaturamentoTableMap::COL_DATA_ALTERADO => 10, RegraFaturamentoTableMap::COL_USUARIO_ALTERADO => 11, ),
        self::TYPE_FIELDNAME     => array('idregra_faturamento' => 0, 'descricao' => 1, 'periodo1_dia_vencimento_cobranca' => 2, 'periodo1_dia_inicial_cobranca' => 3, 'periodo1_dia_final_cobranca' => 4, 'mes_subsequente' => 5, 'periodo2_dia_vencimento_cobranca' => 6, 'periodo2_dia_inicial_cobranca' => 7, 'periodo2_dia_final_cobranca' => 8, 'data_cadastro' => 9, 'data_alterado' => 10, 'usuario_alterado' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('regra_faturamento');
        $this->setPhpName('RegraFaturamento');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\RegraFaturamento');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idregra_faturamento', 'IdregraFaturamento', 'INTEGER', true, 10, null);
        $this->addColumn('descricao', 'Descricao', 'VARCHAR', true, 250, null);
        $this->addColumn('periodo1_dia_vencimento_cobranca', 'Periodo1DiaVencimentoCobranca', 'INTEGER', true, 10, null);
        $this->addColumn('periodo1_dia_inicial_cobranca', 'Periodo1DiaInicialCobranca', 'INTEGER', true, 10, null);
        $this->addColumn('periodo1_dia_final_cobranca', 'Periodo1DiaFinalCobranca', 'INTEGER', true, 10, null);
        $this->addColumn('mes_subsequente', 'MesSubsequente', 'CHAR', true, null, null);
        $this->addColumn('periodo2_dia_vencimento_cobranca', 'Periodo2DiaVencimentoCobranca', 'INTEGER', false, 10, null);
        $this->addColumn('periodo2_dia_inicial_cobranca', 'Periodo2DiaInicialCobranca', 'INTEGER', false, 10, null);
        $this->addColumn('periodo2_dia_final_cobranca', 'Periodo2DiaFinalCobranca', 'INTEGER', false, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addColumn('usuario_alterado', 'UsuarioAlterado', 'INTEGER', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Contrato', '\\ImaTelecomBundle\\Model\\Contrato', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':regra_faturamento_id',
    1 => ':idregra_faturamento',
  ),
), null, null, 'Contratos', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdregraFaturamento', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdregraFaturamento', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdregraFaturamento', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdregraFaturamento', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdregraFaturamento', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdregraFaturamento', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdregraFaturamento', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RegraFaturamentoTableMap::CLASS_DEFAULT : RegraFaturamentoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (RegraFaturamento object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RegraFaturamentoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RegraFaturamentoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RegraFaturamentoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RegraFaturamentoTableMap::OM_CLASS;
            /** @var RegraFaturamento $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RegraFaturamentoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RegraFaturamentoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RegraFaturamentoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var RegraFaturamento $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RegraFaturamentoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_DESCRICAO);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_PERIODO1_DIA_VENCIMENTO_COBRANCA);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_PERIODO1_DIA_INICIAL_COBRANCA);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_PERIODO1_DIA_FINAL_COBRANCA);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_MES_SUBSEQUENTE);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_PERIODO2_DIA_VENCIMENTO_COBRANCA);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_PERIODO2_DIA_INICIAL_COBRANCA);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_PERIODO2_DIA_FINAL_COBRANCA);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(RegraFaturamentoTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idregra_faturamento');
            $criteria->addSelectColumn($alias . '.descricao');
            $criteria->addSelectColumn($alias . '.periodo1_dia_vencimento_cobranca');
            $criteria->addSelectColumn($alias . '.periodo1_dia_inicial_cobranca');
            $criteria->addSelectColumn($alias . '.periodo1_dia_final_cobranca');
            $criteria->addSelectColumn($alias . '.mes_subsequente');
            $criteria->addSelectColumn($alias . '.periodo2_dia_vencimento_cobranca');
            $criteria->addSelectColumn($alias . '.periodo2_dia_inicial_cobranca');
            $criteria->addSelectColumn($alias . '.periodo2_dia_final_cobranca');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RegraFaturamentoTableMap::DATABASE_NAME)->getTable(RegraFaturamentoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RegraFaturamentoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RegraFaturamentoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RegraFaturamentoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a RegraFaturamento or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or RegraFaturamento object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RegraFaturamentoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\RegraFaturamento) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RegraFaturamentoTableMap::DATABASE_NAME);
            $criteria->add(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO, (array) $values, Criteria::IN);
        }

        $query = RegraFaturamentoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RegraFaturamentoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RegraFaturamentoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the regra_faturamento table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RegraFaturamentoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a RegraFaturamento or Criteria object.
     *
     * @param mixed               $criteria Criteria or RegraFaturamento object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RegraFaturamentoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from RegraFaturamento object
        }

        if ($criteria->containsKey(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO) && $criteria->keyContainsValue(RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RegraFaturamentoTableMap::COL_IDREGRA_FATURAMENTO.')');
        }


        // Set the correct dbName
        $query = RegraFaturamentoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RegraFaturamentoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RegraFaturamentoTableMap::buildTableMap();
