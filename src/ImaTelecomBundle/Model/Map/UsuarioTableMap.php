<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Usuario;
use ImaTelecomBundle\Model\UsuarioQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'usuario' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UsuarioTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.UsuarioTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'usuario';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Usuario';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Usuario';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the idusuario field
     */
    const COL_IDUSUARIO = 'usuario.idusuario';

    /**
     * the column name for the pessoa_id field
     */
    const COL_PESSOA_ID = 'usuario.pessoa_id';

    /**
     * the column name for the login field
     */
    const COL_LOGIN = 'usuario.login';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'usuario.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'usuario.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'usuario.usuario_alterado';

    /**
     * the column name for the ativo field
     */
    const COL_ATIVO = 'usuario.ativo';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idusuario', 'PessoaId', 'Login', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'Ativo', ),
        self::TYPE_CAMELNAME     => array('idusuario', 'pessoaId', 'login', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'ativo', ),
        self::TYPE_COLNAME       => array(UsuarioTableMap::COL_IDUSUARIO, UsuarioTableMap::COL_PESSOA_ID, UsuarioTableMap::COL_LOGIN, UsuarioTableMap::COL_DATA_CADASTRO, UsuarioTableMap::COL_DATA_ALTERADO, UsuarioTableMap::COL_USUARIO_ALTERADO, UsuarioTableMap::COL_ATIVO, ),
        self::TYPE_FIELDNAME     => array('idusuario', 'pessoa_id', 'login', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'ativo', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idusuario' => 0, 'PessoaId' => 1, 'Login' => 2, 'DataCadastro' => 3, 'DataAlterado' => 4, 'UsuarioAlterado' => 5, 'Ativo' => 6, ),
        self::TYPE_CAMELNAME     => array('idusuario' => 0, 'pessoaId' => 1, 'login' => 2, 'dataCadastro' => 3, 'dataAlterado' => 4, 'usuarioAlterado' => 5, 'ativo' => 6, ),
        self::TYPE_COLNAME       => array(UsuarioTableMap::COL_IDUSUARIO => 0, UsuarioTableMap::COL_PESSOA_ID => 1, UsuarioTableMap::COL_LOGIN => 2, UsuarioTableMap::COL_DATA_CADASTRO => 3, UsuarioTableMap::COL_DATA_ALTERADO => 4, UsuarioTableMap::COL_USUARIO_ALTERADO => 5, UsuarioTableMap::COL_ATIVO => 6, ),
        self::TYPE_FIELDNAME     => array('idusuario' => 0, 'pessoa_id' => 1, 'login' => 2, 'data_cadastro' => 3, 'data_alterado' => 4, 'usuario_alterado' => 5, 'ativo' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('usuario');
        $this->setPhpName('Usuario');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Usuario');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idusuario', 'Idusuario', 'INTEGER', true, 10, null);
        $this->addForeignKey('pessoa_id', 'PessoaId', 'INTEGER', 'pessoa', 'id', true, 10, null);
        $this->addColumn('login', 'Login', 'VARCHAR', true, 45, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addColumn('usuario_alterado', 'UsuarioAlterado', 'INTEGER', true, 10, null);
        $this->addColumn('ativo', 'Ativo', 'BOOLEAN', true, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Pessoa', '\\ImaTelecomBundle\\Model\\Pessoa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Baixa', '\\ImaTelecomBundle\\Model\\Baixa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_baixa',
    1 => ':idusuario',
  ),
), null, null, 'Baixas', false);
        $this->addRelation('BaixaEstorno', '\\ImaTelecomBundle\\Model\\BaixaEstorno', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_estorno',
    1 => ':idusuario',
  ),
), null, null, 'BaixaEstornos', false);
        $this->addRelation('BaixaMovimento', '\\ImaTelecomBundle\\Model\\BaixaMovimento', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_baixa',
    1 => ':idusuario',
  ),
), null, null, 'BaixaMovimentos', false);
        $this->addRelation('Banco', '\\ImaTelecomBundle\\Model\\Banco', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'Bancos', false);
        $this->addRelation('BancoAgencia', '\\ImaTelecomBundle\\Model\\BancoAgencia', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'BancoAgencias', false);
        $this->addRelation('BancoAgenciaConta', '\\ImaTelecomBundle\\Model\\BancoAgenciaConta', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'BancoAgenciaContas', false);
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'Boletos', false);
        $this->addRelation('BoletoBaixaHistorico', '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'BoletoBaixaHistoricos', false);
        $this->addRelation('BoletoItem', '\\ImaTelecomBundle\\Model\\BoletoItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'BoletoItems', false);
        $this->addRelation('Caixa', '\\ImaTelecomBundle\\Model\\Caixa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_baixa',
    1 => ':idusuario',
  ),
), null, null, 'Caixas', false);
        $this->addRelation('Cliente', '\\ImaTelecomBundle\\Model\\Cliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'Clientes', false);
        $this->addRelation('ClienteEstoqueItem', '\\ImaTelecomBundle\\Model\\ClienteEstoqueItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'ClienteEstoqueItems', false);
        $this->addRelation('ContaCaixa', '\\ImaTelecomBundle\\Model\\ContaCaixa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'ContaCaixas', false);
        $this->addRelation('ContasPagar', '\\ImaTelecomBundle\\Model\\ContasPagar', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'ContasPagars', false);
        $this->addRelation('ContasPagarTributos', '\\ImaTelecomBundle\\Model\\ContasPagarTributos', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'ContasPagarTributoss', false);
        $this->addRelation('Convenio', '\\ImaTelecomBundle\\Model\\Convenio', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'Convenios', false);
        $this->addRelation('CronTask', '\\ImaTelecomBundle\\Model\\CronTask', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'CronTasks', false);
        $this->addRelation('CronTaskGrupo', '\\ImaTelecomBundle\\Model\\CronTaskGrupo', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'CronTaskGrupos', false);
        $this->addRelation('EnderecoAgencia', '\\ImaTelecomBundle\\Model\\EnderecoAgencia', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'EnderecoAgencias', false);
        $this->addRelation('EnderecoCliente', '\\ImaTelecomBundle\\Model\\EnderecoCliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'EnderecoClientes', false);
        $this->addRelation('EnderecoServCliente', '\\ImaTelecomBundle\\Model\\EnderecoServCliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'EnderecoServClientes', false);
        $this->addRelation('Estoque', '\\ImaTelecomBundle\\Model\\Estoque', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'Estoques', false);
        $this->addRelation('EstoqueLancamento', '\\ImaTelecomBundle\\Model\\EstoqueLancamento', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'EstoqueLancamentos', false);
        $this->addRelation('EstoqueLancamentoItem', '\\ImaTelecomBundle\\Model\\EstoqueLancamentoItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'EstoqueLancamentoItems', false);
        $this->addRelation('FormaPagamento', '\\ImaTelecomBundle\\Model\\FormaPagamento', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'FormaPagamentos', false);
        $this->addRelation('Fornecedor', '\\ImaTelecomBundle\\Model\\Fornecedor', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'Fornecedors', false);
        $this->addRelation('ItemComprado', '\\ImaTelecomBundle\\Model\\ItemComprado', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'ItemComprados', false);
        $this->addRelation('ItemDeCompra', '\\ImaTelecomBundle\\Model\\ItemDeCompra', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'ItemDeCompras', false);
        $this->addRelation('ServicoCliente', '\\ImaTelecomBundle\\Model\\ServicoCliente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'ServicoClientes', false);
        $this->addRelation('ServicoPrestado', '\\ImaTelecomBundle\\Model\\ServicoPrestado', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'ServicoPrestados', false);
        $this->addRelation('Sici', '\\ImaTelecomBundle\\Model\\Sici', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'Sicis', false);
        $this->addRelation('SysPerfil', '\\ImaTelecomBundle\\Model\\SysPerfil', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'SysPerfils', false);
        $this->addRelation('SysProcessos', '\\ImaTelecomBundle\\Model\\SysProcessos', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'SysProcessoss', false);
        $this->addRelation('SysUsuarioPerfil', '\\ImaTelecomBundle\\Model\\SysUsuarioPerfil', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_id',
    1 => ':idusuario',
  ),
), null, null, 'SysUsuarioPerfils', false);
        $this->addRelation('Telefone', '\\ImaTelecomBundle\\Model\\Telefone', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'Telefones', false);
        $this->addRelation('Tributo', '\\ImaTelecomBundle\\Model\\Tributo', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, 'Tributos', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idusuario', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idusuario', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idusuario', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idusuario', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idusuario', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idusuario', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idusuario', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UsuarioTableMap::CLASS_DEFAULT : UsuarioTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Usuario object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UsuarioTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UsuarioTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UsuarioTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UsuarioTableMap::OM_CLASS;
            /** @var Usuario $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UsuarioTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UsuarioTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UsuarioTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Usuario $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UsuarioTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UsuarioTableMap::COL_IDUSUARIO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_PESSOA_ID);
            $criteria->addSelectColumn(UsuarioTableMap::COL_LOGIN);
            $criteria->addSelectColumn(UsuarioTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_ATIVO);
        } else {
            $criteria->addSelectColumn($alias . '.idusuario');
            $criteria->addSelectColumn($alias . '.pessoa_id');
            $criteria->addSelectColumn($alias . '.login');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.ativo');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UsuarioTableMap::DATABASE_NAME)->getTable(UsuarioTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UsuarioTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UsuarioTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UsuarioTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Usuario or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Usuario object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Usuario) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UsuarioTableMap::DATABASE_NAME);
            $criteria->add(UsuarioTableMap::COL_IDUSUARIO, (array) $values, Criteria::IN);
        }

        $query = UsuarioQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UsuarioTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UsuarioTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the usuario table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UsuarioQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Usuario or Criteria object.
     *
     * @param mixed               $criteria Criteria or Usuario object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Usuario object
        }

        if ($criteria->containsKey(UsuarioTableMap::COL_IDUSUARIO) && $criteria->keyContainsValue(UsuarioTableMap::COL_IDUSUARIO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UsuarioTableMap::COL_IDUSUARIO.')');
        }


        // Set the correct dbName
        $query = UsuarioQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UsuarioTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UsuarioTableMap::buildTableMap();
