<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Competencia;
use ImaTelecomBundle\Model\CompetenciaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'competencia' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CompetenciaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.CompetenciaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'competencia';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Competencia';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Competencia';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id field
     */
    const COL_ID = 'competencia.id';

    /**
     * the column name for the ano field
     */
    const COL_ANO = 'competencia.ano';

    /**
     * the column name for the mes field
     */
    const COL_MES = 'competencia.mes';

    /**
     * the column name for the numero_nf field
     */
    const COL_NUMERO_NF = 'competencia.numero_nf';

    /**
     * the column name for the periodo_inicial field
     */
    const COL_PERIODO_INICIAL = 'competencia.periodo_inicial';

    /**
     * the column name for the periodo_final field
     */
    const COL_PERIODO_FINAL = 'competencia.periodo_final';

    /**
     * the column name for the ativo field
     */
    const COL_ATIVO = 'competencia.ativo';

    /**
     * the column name for the descricao field
     */
    const COL_DESCRICAO = 'competencia.descricao';

    /**
     * the column name for the periodo_futuro field
     */
    const COL_PERIODO_FUTURO = 'competencia.periodo_futuro';

    /**
     * the column name for the proxima_competencia field
     */
    const COL_PROXIMA_COMPETENCIA = 'competencia.proxima_competencia';

    /**
     * the column name for the encerramento field
     */
    const COL_ENCERRAMENTO = 'competencia.encerramento';

    /**
     * the column name for the trabalhar_periodo_futuro field
     */
    const COL_TRABALHAR_PERIODO_FUTURO = 'competencia.trabalhar_periodo_futuro';

    /**
     * the column name for the ultima_data_emissao field
     */
    const COL_ULTIMA_DATA_EMISSAO = 'competencia.ultima_data_emissao';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Ano', 'Mes', 'NumeroNf', 'PeriodoInicial', 'PeriodoFinal', 'Ativo', 'Descricao', 'PeriodoFuturo', 'ProximaCompetencia', 'Encerramento', 'TrabalharPeriodoFuturo', 'UltimaDataEmissao', ),
        self::TYPE_CAMELNAME     => array('id', 'ano', 'mes', 'numeroNf', 'periodoInicial', 'periodoFinal', 'ativo', 'descricao', 'periodoFuturo', 'proximaCompetencia', 'encerramento', 'trabalharPeriodoFuturo', 'ultimaDataEmissao', ),
        self::TYPE_COLNAME       => array(CompetenciaTableMap::COL_ID, CompetenciaTableMap::COL_ANO, CompetenciaTableMap::COL_MES, CompetenciaTableMap::COL_NUMERO_NF, CompetenciaTableMap::COL_PERIODO_INICIAL, CompetenciaTableMap::COL_PERIODO_FINAL, CompetenciaTableMap::COL_ATIVO, CompetenciaTableMap::COL_DESCRICAO, CompetenciaTableMap::COL_PERIODO_FUTURO, CompetenciaTableMap::COL_PROXIMA_COMPETENCIA, CompetenciaTableMap::COL_ENCERRAMENTO, CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO, CompetenciaTableMap::COL_ULTIMA_DATA_EMISSAO, ),
        self::TYPE_FIELDNAME     => array('id', 'ano', 'mes', 'numero_nf', 'periodo_inicial', 'periodo_final', 'ativo', 'descricao', 'periodo_futuro', 'proxima_competencia', 'encerramento', 'trabalhar_periodo_futuro', 'ultima_data_emissao', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Ano' => 1, 'Mes' => 2, 'NumeroNf' => 3, 'PeriodoInicial' => 4, 'PeriodoFinal' => 5, 'Ativo' => 6, 'Descricao' => 7, 'PeriodoFuturo' => 8, 'ProximaCompetencia' => 9, 'Encerramento' => 10, 'TrabalharPeriodoFuturo' => 11, 'UltimaDataEmissao' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'ano' => 1, 'mes' => 2, 'numeroNf' => 3, 'periodoInicial' => 4, 'periodoFinal' => 5, 'ativo' => 6, 'descricao' => 7, 'periodoFuturo' => 8, 'proximaCompetencia' => 9, 'encerramento' => 10, 'trabalharPeriodoFuturo' => 11, 'ultimaDataEmissao' => 12, ),
        self::TYPE_COLNAME       => array(CompetenciaTableMap::COL_ID => 0, CompetenciaTableMap::COL_ANO => 1, CompetenciaTableMap::COL_MES => 2, CompetenciaTableMap::COL_NUMERO_NF => 3, CompetenciaTableMap::COL_PERIODO_INICIAL => 4, CompetenciaTableMap::COL_PERIODO_FINAL => 5, CompetenciaTableMap::COL_ATIVO => 6, CompetenciaTableMap::COL_DESCRICAO => 7, CompetenciaTableMap::COL_PERIODO_FUTURO => 8, CompetenciaTableMap::COL_PROXIMA_COMPETENCIA => 9, CompetenciaTableMap::COL_ENCERRAMENTO => 10, CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO => 11, CompetenciaTableMap::COL_ULTIMA_DATA_EMISSAO => 12, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'ano' => 1, 'mes' => 2, 'numero_nf' => 3, 'periodo_inicial' => 4, 'periodo_final' => 5, 'ativo' => 6, 'descricao' => 7, 'periodo_futuro' => 8, 'proxima_competencia' => 9, 'encerramento' => 10, 'trabalhar_periodo_futuro' => 11, 'ultima_data_emissao' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('competencia');
        $this->setPhpName('Competencia');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Competencia');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('ano', 'Ano', 'VARCHAR', true, 45, null);
        $this->addColumn('mes', 'Mes', 'VARCHAR', true, 45, null);
        $this->addColumn('numero_nf', 'NumeroNf', 'INTEGER', true, null, null);
        $this->addColumn('periodo_inicial', 'PeriodoInicial', 'DATE', false, null, null);
        $this->addColumn('periodo_final', 'PeriodoFinal', 'DATE', false, null, null);
        $this->addColumn('ativo', 'Ativo', 'INTEGER', false, null, 0);
        $this->addColumn('descricao', 'Descricao', 'VARCHAR', false, 255, null);
        $this->addColumn('periodo_futuro', 'PeriodoFuturo', 'INTEGER', false, null, null);
        $this->addColumn('proxima_competencia', 'ProximaCompetencia', 'INTEGER', true, 10, null);
        $this->addColumn('encerramento', 'Encerramento', 'TINYINT', true, 2, null);
        $this->addColumn('trabalhar_periodo_futuro', 'TrabalharPeriodoFuturo', 'TINYINT', true, 2, null);
        $this->addColumn('ultima_data_emissao', 'UltimaDataEmissao', 'VARCHAR', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Baixa', '\\ImaTelecomBundle\\Model\\Baixa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'Baixas', false);
        $this->addRelation('BaixaEstorno', '\\ImaTelecomBundle\\Model\\BaixaEstorno', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'BaixaEstornos', false);
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'Boletos', false);
        $this->addRelation('BoletoBaixaHistorico', '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'BoletoBaixaHistoricos', false);
        $this->addRelation('DadosFiscal', '\\ImaTelecomBundle\\Model\\DadosFiscal', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'DadosFiscals', false);
        $this->addRelation('Faturamento', '\\ImaTelecomBundle\\Model\\Faturamento', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'Faturamentos', false);
        $this->addRelation('Lancamentos', '\\ImaTelecomBundle\\Model\\Lancamentos', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'Lancamentoss', false);
        $this->addRelation('LancamentosBoletos', '\\ImaTelecomBundle\\Model\\LancamentosBoletos', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'LancamentosBoletoss', false);
        $this->addRelation('Sici', '\\ImaTelecomBundle\\Model\\Sici', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'Sicis', false);
        $this->addRelation('Sintegra', '\\ImaTelecomBundle\\Model\\Sintegra', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, 'Sintegras', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CompetenciaTableMap::CLASS_DEFAULT : CompetenciaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Competencia object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CompetenciaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CompetenciaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CompetenciaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CompetenciaTableMap::OM_CLASS;
            /** @var Competencia $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CompetenciaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CompetenciaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CompetenciaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Competencia $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CompetenciaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CompetenciaTableMap::COL_ID);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_ANO);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_MES);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_NUMERO_NF);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_PERIODO_INICIAL);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_PERIODO_FINAL);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_ATIVO);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_DESCRICAO);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_PERIODO_FUTURO);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_PROXIMA_COMPETENCIA);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_ENCERRAMENTO);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_TRABALHAR_PERIODO_FUTURO);
            $criteria->addSelectColumn(CompetenciaTableMap::COL_ULTIMA_DATA_EMISSAO);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.ano');
            $criteria->addSelectColumn($alias . '.mes');
            $criteria->addSelectColumn($alias . '.numero_nf');
            $criteria->addSelectColumn($alias . '.periodo_inicial');
            $criteria->addSelectColumn($alias . '.periodo_final');
            $criteria->addSelectColumn($alias . '.ativo');
            $criteria->addSelectColumn($alias . '.descricao');
            $criteria->addSelectColumn($alias . '.periodo_futuro');
            $criteria->addSelectColumn($alias . '.proxima_competencia');
            $criteria->addSelectColumn($alias . '.encerramento');
            $criteria->addSelectColumn($alias . '.trabalhar_periodo_futuro');
            $criteria->addSelectColumn($alias . '.ultima_data_emissao');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CompetenciaTableMap::DATABASE_NAME)->getTable(CompetenciaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CompetenciaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CompetenciaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CompetenciaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Competencia or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Competencia object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompetenciaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Competencia) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CompetenciaTableMap::DATABASE_NAME);
            $criteria->add(CompetenciaTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CompetenciaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CompetenciaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CompetenciaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the competencia table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CompetenciaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Competencia or Criteria object.
     *
     * @param mixed               $criteria Criteria or Competencia object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompetenciaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Competencia object
        }

        if ($criteria->containsKey(CompetenciaTableMap::COL_ID) && $criteria->keyContainsValue(CompetenciaTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CompetenciaTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CompetenciaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CompetenciaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CompetenciaTableMap::buildTableMap();
