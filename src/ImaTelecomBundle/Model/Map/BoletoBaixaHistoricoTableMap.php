<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\BoletoBaixaHistorico;
use ImaTelecomBundle\Model\BoletoBaixaHistoricoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'boleto_baixa_historico' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class BoletoBaixaHistoricoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.BoletoBaixaHistoricoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'boleto_baixa_historico';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.BoletoBaixaHistorico';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 24;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 24;

    /**
     * the column name for the idboleto_baixa_historico field
     */
    const COL_IDBOLETO_BAIXA_HISTORICO = 'boleto_baixa_historico.idboleto_baixa_historico';

    /**
     * the column name for the boleto_id field
     */
    const COL_BOLETO_ID = 'boleto_baixa_historico.boleto_id';

    /**
     * the column name for the lancamento_id field
     */
    const COL_LANCAMENTO_ID = 'boleto_baixa_historico.lancamento_id';

    /**
     * the column name for the pessoa_id field
     */
    const COL_PESSOA_ID = 'boleto_baixa_historico.pessoa_id';

    /**
     * the column name for the competencia_id field
     */
    const COL_COMPETENCIA_ID = 'boleto_baixa_historico.competencia_id';

    /**
     * the column name for the baixa_id field
     */
    const COL_BAIXA_ID = 'boleto_baixa_historico.baixa_id';

    /**
     * the column name for the baixa_estorno_id field
     */
    const COL_BAIXA_ESTORNO_ID = 'boleto_baixa_historico.baixa_estorno_id';

    /**
     * the column name for the status field
     */
    const COL_STATUS = 'boleto_baixa_historico.status';

    /**
     * the column name for the mensagem_retorno field
     */
    const COL_MENSAGEM_RETORNO = 'boleto_baixa_historico.mensagem_retorno';

    /**
     * the column name for the data_pagamento field
     */
    const COL_DATA_PAGAMENTO = 'boleto_baixa_historico.data_pagamento';

    /**
     * the column name for the data_recusa field
     */
    const COL_DATA_RECUSA = 'boleto_baixa_historico.data_recusa';

    /**
     * the column name for the valor_tarifa_custo field
     */
    const COL_VALOR_TARIFA_CUSTO = 'boleto_baixa_historico.valor_tarifa_custo';

    /**
     * the column name for the valor_original field
     */
    const COL_VALOR_ORIGINAL = 'boleto_baixa_historico.valor_original';

    /**
     * the column name for the valor_multa field
     */
    const COL_VALOR_MULTA = 'boleto_baixa_historico.valor_multa';

    /**
     * the column name for the valor_juros field
     */
    const COL_VALOR_JUROS = 'boleto_baixa_historico.valor_juros';

    /**
     * the column name for the valor_desconto field
     */
    const COL_VALOR_DESCONTO = 'boleto_baixa_historico.valor_desconto';

    /**
     * the column name for the valor_total field
     */
    const COL_VALOR_TOTAL = 'boleto_baixa_historico.valor_total';

    /**
     * the column name for the data_vencimento field
     */
    const COL_DATA_VENCIMENTO = 'boleto_baixa_historico.data_vencimento';

    /**
     * the column name for the numero_documento field
     */
    const COL_NUMERO_DOCUMENTO = 'boleto_baixa_historico.numero_documento';

    /**
     * the column name for the codigo_movimento field
     */
    const COL_CODIGO_MOVIMENTO = 'boleto_baixa_historico.codigo_movimento';

    /**
     * the column name for the descricao_movimento field
     */
    const COL_DESCRICAO_MOVIMENTO = 'boleto_baixa_historico.descricao_movimento';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'boleto_baixa_historico.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'boleto_baixa_historico.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'boleto_baixa_historico.usuario_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdboletoBaixaHistorico', 'BoletoId', 'LancamentoId', 'PessoaId', 'CompetenciaId', 'BaixaId', 'BaixaEstornoId', 'Status', 'MensagemRetorno', 'DataPagamento', 'DataRecusa', 'ValorTarifaCusto', 'ValorOriginal', 'ValorMulta', 'ValorJuros', 'ValorDesconto', 'ValorTotal', 'DataVencimento', 'NumeroDocumento', 'CodigoMovimento', 'DescricaoMovimento', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', ),
        self::TYPE_CAMELNAME     => array('idboletoBaixaHistorico', 'boletoId', 'lancamentoId', 'pessoaId', 'competenciaId', 'baixaId', 'baixaEstornoId', 'status', 'mensagemRetorno', 'dataPagamento', 'dataRecusa', 'valorTarifaCusto', 'valorOriginal', 'valorMulta', 'valorJuros', 'valorDesconto', 'valorTotal', 'dataVencimento', 'numeroDocumento', 'codigoMovimento', 'descricaoMovimento', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', ),
        self::TYPE_COLNAME       => array(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, BoletoBaixaHistoricoTableMap::COL_BOLETO_ID, BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID, BoletoBaixaHistoricoTableMap::COL_PESSOA_ID, BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID, BoletoBaixaHistoricoTableMap::COL_BAIXA_ID, BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID, BoletoBaixaHistoricoTableMap::COL_STATUS, BoletoBaixaHistoricoTableMap::COL_MENSAGEM_RETORNO, BoletoBaixaHistoricoTableMap::COL_DATA_PAGAMENTO, BoletoBaixaHistoricoTableMap::COL_DATA_RECUSA, BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO, BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL, BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA, BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS, BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO, BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL, BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO, BoletoBaixaHistoricoTableMap::COL_NUMERO_DOCUMENTO, BoletoBaixaHistoricoTableMap::COL_CODIGO_MOVIMENTO, BoletoBaixaHistoricoTableMap::COL_DESCRICAO_MOVIMENTO, BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO, BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO, BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idboleto_baixa_historico', 'boleto_id', 'lancamento_id', 'pessoa_id', 'competencia_id', 'baixa_id', 'baixa_estorno_id', 'status', 'mensagem_retorno', 'data_pagamento', 'data_recusa', 'valor_tarifa_custo', 'valor_original', 'valor_multa', 'valor_juros', 'valor_desconto', 'valor_total', 'data_vencimento', 'numero_documento', 'codigo_movimento', 'descricao_movimento', 'data_cadastro', 'data_alterado', 'usuario_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdboletoBaixaHistorico' => 0, 'BoletoId' => 1, 'LancamentoId' => 2, 'PessoaId' => 3, 'CompetenciaId' => 4, 'BaixaId' => 5, 'BaixaEstornoId' => 6, 'Status' => 7, 'MensagemRetorno' => 8, 'DataPagamento' => 9, 'DataRecusa' => 10, 'ValorTarifaCusto' => 11, 'ValorOriginal' => 12, 'ValorMulta' => 13, 'ValorJuros' => 14, 'ValorDesconto' => 15, 'ValorTotal' => 16, 'DataVencimento' => 17, 'NumeroDocumento' => 18, 'CodigoMovimento' => 19, 'DescricaoMovimento' => 20, 'DataCadastro' => 21, 'DataAlterado' => 22, 'UsuarioAlterado' => 23, ),
        self::TYPE_CAMELNAME     => array('idboletoBaixaHistorico' => 0, 'boletoId' => 1, 'lancamentoId' => 2, 'pessoaId' => 3, 'competenciaId' => 4, 'baixaId' => 5, 'baixaEstornoId' => 6, 'status' => 7, 'mensagemRetorno' => 8, 'dataPagamento' => 9, 'dataRecusa' => 10, 'valorTarifaCusto' => 11, 'valorOriginal' => 12, 'valorMulta' => 13, 'valorJuros' => 14, 'valorDesconto' => 15, 'valorTotal' => 16, 'dataVencimento' => 17, 'numeroDocumento' => 18, 'codigoMovimento' => 19, 'descricaoMovimento' => 20, 'dataCadastro' => 21, 'dataAlterado' => 22, 'usuarioAlterado' => 23, ),
        self::TYPE_COLNAME       => array(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO => 0, BoletoBaixaHistoricoTableMap::COL_BOLETO_ID => 1, BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID => 2, BoletoBaixaHistoricoTableMap::COL_PESSOA_ID => 3, BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID => 4, BoletoBaixaHistoricoTableMap::COL_BAIXA_ID => 5, BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID => 6, BoletoBaixaHistoricoTableMap::COL_STATUS => 7, BoletoBaixaHistoricoTableMap::COL_MENSAGEM_RETORNO => 8, BoletoBaixaHistoricoTableMap::COL_DATA_PAGAMENTO => 9, BoletoBaixaHistoricoTableMap::COL_DATA_RECUSA => 10, BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO => 11, BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL => 12, BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA => 13, BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS => 14, BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO => 15, BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL => 16, BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO => 17, BoletoBaixaHistoricoTableMap::COL_NUMERO_DOCUMENTO => 18, BoletoBaixaHistoricoTableMap::COL_CODIGO_MOVIMENTO => 19, BoletoBaixaHistoricoTableMap::COL_DESCRICAO_MOVIMENTO => 20, BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO => 21, BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO => 22, BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO => 23, ),
        self::TYPE_FIELDNAME     => array('idboleto_baixa_historico' => 0, 'boleto_id' => 1, 'lancamento_id' => 2, 'pessoa_id' => 3, 'competencia_id' => 4, 'baixa_id' => 5, 'baixa_estorno_id' => 6, 'status' => 7, 'mensagem_retorno' => 8, 'data_pagamento' => 9, 'data_recusa' => 10, 'valor_tarifa_custo' => 11, 'valor_original' => 12, 'valor_multa' => 13, 'valor_juros' => 14, 'valor_desconto' => 15, 'valor_total' => 16, 'data_vencimento' => 17, 'numero_documento' => 18, 'codigo_movimento' => 19, 'descricao_movimento' => 20, 'data_cadastro' => 21, 'data_alterado' => 22, 'usuario_alterado' => 23, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('boleto_baixa_historico');
        $this->setPhpName('BoletoBaixaHistorico');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idboleto_baixa_historico', 'IdboletoBaixaHistorico', 'INTEGER', true, 10, null);
        $this->addForeignKey('boleto_id', 'BoletoId', 'INTEGER', 'boleto', 'idboleto', true, 10, null);
        $this->addForeignKey('lancamento_id', 'LancamentoId', 'INTEGER', 'lancamentos', 'idlancamento', false, 10, null);
        $this->addForeignKey('pessoa_id', 'PessoaId', 'INTEGER', 'pessoa', 'id', false, 10, null);
        $this->addForeignKey('competencia_id', 'CompetenciaId', 'INTEGER', 'competencia', 'id', true, 10, null);
        $this->addForeignKey('baixa_id', 'BaixaId', 'INTEGER', 'baixa', 'idbaixa', false, 10, null);
        $this->addForeignKey('baixa_estorno_id', 'BaixaEstornoId', 'INTEGER', 'baixa_estorno', 'idbaixa_estorno', false, 10, null);
        $this->addColumn('status', 'Status', 'CHAR', true, null, null);
        $this->addColumn('mensagem_retorno', 'MensagemRetorno', 'LONGVARCHAR', false, null, null);
        $this->addColumn('data_pagamento', 'DataPagamento', 'VARCHAR', false, 10, null);
        $this->addColumn('data_recusa', 'DataRecusa', 'VARCHAR', false, 10, null);
        $this->addColumn('valor_tarifa_custo', 'ValorTarifaCusto', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_original', 'ValorOriginal', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_multa', 'ValorMulta', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_juros', 'ValorJuros', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_desconto', 'ValorDesconto', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_total', 'ValorTotal', 'DECIMAL', false, 10, 0);
        $this->addColumn('data_vencimento', 'DataVencimento', 'DATE', false, null, null);
        $this->addColumn('numero_documento', 'NumeroDocumento', 'VARCHAR', false, 50, null);
        $this->addColumn('codigo_movimento', 'CodigoMovimento', 'VARCHAR', false, 2, null);
        $this->addColumn('descricao_movimento', 'DescricaoMovimento', 'VARCHAR', false, 250, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_alterado', 'UsuarioAlterado', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BaixaEstorno', '\\ImaTelecomBundle\\Model\\BaixaEstorno', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':baixa_estorno_id',
    1 => ':idbaixa_estorno',
  ),
), null, null, null, false);
        $this->addRelation('Baixa', '\\ImaTelecomBundle\\Model\\Baixa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':baixa_id',
    1 => ':idbaixa',
  ),
), null, null, null, false);
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, null, false);
        $this->addRelation('Competencia', '\\ImaTelecomBundle\\Model\\Competencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Lancamentos', '\\ImaTelecomBundle\\Model\\Lancamentos', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':lancamento_id',
    1 => ':idlancamento',
  ),
), null, null, null, false);
        $this->addRelation('Pessoa', '\\ImaTelecomBundle\\Model\\Pessoa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':pessoa_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_alterado',
    1 => ':idusuario',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoBaixaHistorico', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoBaixaHistorico', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoBaixaHistorico', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoBaixaHistorico', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoBaixaHistorico', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdboletoBaixaHistorico', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdboletoBaixaHistorico', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BoletoBaixaHistoricoTableMap::CLASS_DEFAULT : BoletoBaixaHistoricoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (BoletoBaixaHistorico object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BoletoBaixaHistoricoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BoletoBaixaHistoricoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BoletoBaixaHistoricoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BoletoBaixaHistoricoTableMap::OM_CLASS;
            /** @var BoletoBaixaHistorico $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BoletoBaixaHistoricoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BoletoBaixaHistoricoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BoletoBaixaHistoricoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var BoletoBaixaHistorico $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BoletoBaixaHistoricoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_BOLETO_ID);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_LANCAMENTO_ID);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_PESSOA_ID);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_COMPETENCIA_ID);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_BAIXA_ID);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_BAIXA_ESTORNO_ID);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_STATUS);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_MENSAGEM_RETORNO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_DATA_PAGAMENTO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_DATA_RECUSA);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_VALOR_TARIFA_CUSTO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_VALOR_ORIGINAL);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_VALOR_MULTA);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_VALOR_JUROS);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_VALOR_DESCONTO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_VALOR_TOTAL);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_DATA_VENCIMENTO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_NUMERO_DOCUMENTO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_CODIGO_MOVIMENTO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_DESCRICAO_MOVIMENTO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(BoletoBaixaHistoricoTableMap::COL_USUARIO_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idboleto_baixa_historico');
            $criteria->addSelectColumn($alias . '.boleto_id');
            $criteria->addSelectColumn($alias . '.lancamento_id');
            $criteria->addSelectColumn($alias . '.pessoa_id');
            $criteria->addSelectColumn($alias . '.competencia_id');
            $criteria->addSelectColumn($alias . '.baixa_id');
            $criteria->addSelectColumn($alias . '.baixa_estorno_id');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.mensagem_retorno');
            $criteria->addSelectColumn($alias . '.data_pagamento');
            $criteria->addSelectColumn($alias . '.data_recusa');
            $criteria->addSelectColumn($alias . '.valor_tarifa_custo');
            $criteria->addSelectColumn($alias . '.valor_original');
            $criteria->addSelectColumn($alias . '.valor_multa');
            $criteria->addSelectColumn($alias . '.valor_juros');
            $criteria->addSelectColumn($alias . '.valor_desconto');
            $criteria->addSelectColumn($alias . '.valor_total');
            $criteria->addSelectColumn($alias . '.data_vencimento');
            $criteria->addSelectColumn($alias . '.numero_documento');
            $criteria->addSelectColumn($alias . '.codigo_movimento');
            $criteria->addSelectColumn($alias . '.descricao_movimento');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BoletoBaixaHistoricoTableMap::DATABASE_NAME)->getTable(BoletoBaixaHistoricoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BoletoBaixaHistoricoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BoletoBaixaHistoricoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a BoletoBaixaHistorico or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or BoletoBaixaHistorico object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\BoletoBaixaHistorico) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
            $criteria->add(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO, (array) $values, Criteria::IN);
        }

        $query = BoletoBaixaHistoricoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BoletoBaixaHistoricoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BoletoBaixaHistoricoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the boleto_baixa_historico table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BoletoBaixaHistoricoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a BoletoBaixaHistorico or Criteria object.
     *
     * @param mixed               $criteria Criteria or BoletoBaixaHistorico object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BoletoBaixaHistoricoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from BoletoBaixaHistorico object
        }

        if ($criteria->containsKey(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO) && $criteria->keyContainsValue(BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BoletoBaixaHistoricoTableMap::COL_IDBOLETO_BAIXA_HISTORICO.')');
        }


        // Set the correct dbName
        $query = BoletoBaixaHistoricoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BoletoBaixaHistoricoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BoletoBaixaHistoricoTableMap::buildTableMap();
