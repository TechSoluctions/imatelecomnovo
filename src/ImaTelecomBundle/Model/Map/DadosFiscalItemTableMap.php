<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\DadosFiscalItem;
use ImaTelecomBundle\Model\DadosFiscalItemQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'dados_fiscal_item' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class DadosFiscalItemTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.DadosFiscalItemTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'dados_fiscal_item';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\DadosFiscalItem';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.DadosFiscalItem';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the iddados_fiscal_item field
     */
    const COL_IDDADOS_FISCAL_ITEM = 'dados_fiscal_item.iddados_fiscal_item';

    /**
     * the column name for the boleto_id field
     */
    const COL_BOLETO_ID = 'dados_fiscal_item.boleto_id';

    /**
     * the column name for the boleto_item_id field
     */
    const COL_BOLETO_ITEM_ID = 'dados_fiscal_item.boleto_item_id';

    /**
     * the column name for the item field
     */
    const COL_ITEM = 'dados_fiscal_item.item';

    /**
     * the column name for the numero_item_nota_fiscal field
     */
    const COL_NUMERO_ITEM_NOTA_FISCAL = 'dados_fiscal_item.numero_item_nota_fiscal';

    /**
     * the column name for the valor_item field
     */
    const COL_VALOR_ITEM = 'dados_fiscal_item.valor_item';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'dados_fiscal_item.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'dados_fiscal_item.data_alterado';

    /**
     * the column name for the dados_fiscal_id field
     */
    const COL_DADOS_FISCAL_ID = 'dados_fiscal_item.dados_fiscal_id';

    /**
     * the column name for the posicao_item field
     */
    const COL_POSICAO_ITEM = 'dados_fiscal_item.posicao_item';

    /**
     * the column name for the valor_scm field
     */
    const COL_VALOR_SCM = 'dados_fiscal_item.valor_scm';

    /**
     * the column name for the valor_sva field
     */
    const COL_VALOR_SVA = 'dados_fiscal_item.valor_sva';

    /**
     * the column name for the tem_scm field
     */
    const COL_TEM_SCM = 'dados_fiscal_item.tem_scm';

    /**
     * the column name for the tem_sva field
     */
    const COL_TEM_SVA = 'dados_fiscal_item.tem_sva';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IddadosFiscalItem', 'BoletoId', 'BoletoItemId', 'Item', 'NumeroItemNotaFiscal', 'ValorItem', 'DataCadastro', 'DataAlterado', 'DadosFiscalId', 'PosicaoItem', 'ValorScm', 'ValorSva', 'TemScm', 'TemSva', ),
        self::TYPE_CAMELNAME     => array('iddadosFiscalItem', 'boletoId', 'boletoItemId', 'item', 'numeroItemNotaFiscal', 'valorItem', 'dataCadastro', 'dataAlterado', 'dadosFiscalId', 'posicaoItem', 'valorScm', 'valorSva', 'temScm', 'temSva', ),
        self::TYPE_COLNAME       => array(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, DadosFiscalItemTableMap::COL_BOLETO_ID, DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID, DadosFiscalItemTableMap::COL_ITEM, DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL, DadosFiscalItemTableMap::COL_VALOR_ITEM, DadosFiscalItemTableMap::COL_DATA_CADASTRO, DadosFiscalItemTableMap::COL_DATA_ALTERADO, DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID, DadosFiscalItemTableMap::COL_POSICAO_ITEM, DadosFiscalItemTableMap::COL_VALOR_SCM, DadosFiscalItemTableMap::COL_VALOR_SVA, DadosFiscalItemTableMap::COL_TEM_SCM, DadosFiscalItemTableMap::COL_TEM_SVA, ),
        self::TYPE_FIELDNAME     => array('iddados_fiscal_item', 'boleto_id', 'boleto_item_id', 'item', 'numero_item_nota_fiscal', 'valor_item', 'data_cadastro', 'data_alterado', 'dados_fiscal_id', 'posicao_item', 'valor_scm', 'valor_sva', 'tem_scm', 'tem_sva', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IddadosFiscalItem' => 0, 'BoletoId' => 1, 'BoletoItemId' => 2, 'Item' => 3, 'NumeroItemNotaFiscal' => 4, 'ValorItem' => 5, 'DataCadastro' => 6, 'DataAlterado' => 7, 'DadosFiscalId' => 8, 'PosicaoItem' => 9, 'ValorScm' => 10, 'ValorSva' => 11, 'TemScm' => 12, 'TemSva' => 13, ),
        self::TYPE_CAMELNAME     => array('iddadosFiscalItem' => 0, 'boletoId' => 1, 'boletoItemId' => 2, 'item' => 3, 'numeroItemNotaFiscal' => 4, 'valorItem' => 5, 'dataCadastro' => 6, 'dataAlterado' => 7, 'dadosFiscalId' => 8, 'posicaoItem' => 9, 'valorScm' => 10, 'valorSva' => 11, 'temScm' => 12, 'temSva' => 13, ),
        self::TYPE_COLNAME       => array(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM => 0, DadosFiscalItemTableMap::COL_BOLETO_ID => 1, DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID => 2, DadosFiscalItemTableMap::COL_ITEM => 3, DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL => 4, DadosFiscalItemTableMap::COL_VALOR_ITEM => 5, DadosFiscalItemTableMap::COL_DATA_CADASTRO => 6, DadosFiscalItemTableMap::COL_DATA_ALTERADO => 7, DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID => 8, DadosFiscalItemTableMap::COL_POSICAO_ITEM => 9, DadosFiscalItemTableMap::COL_VALOR_SCM => 10, DadosFiscalItemTableMap::COL_VALOR_SVA => 11, DadosFiscalItemTableMap::COL_TEM_SCM => 12, DadosFiscalItemTableMap::COL_TEM_SVA => 13, ),
        self::TYPE_FIELDNAME     => array('iddados_fiscal_item' => 0, 'boleto_id' => 1, 'boleto_item_id' => 2, 'item' => 3, 'numero_item_nota_fiscal' => 4, 'valor_item' => 5, 'data_cadastro' => 6, 'data_alterado' => 7, 'dados_fiscal_id' => 8, 'posicao_item' => 9, 'valor_scm' => 10, 'valor_sva' => 11, 'tem_scm' => 12, 'tem_sva' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('dados_fiscal_item');
        $this->setPhpName('DadosFiscalItem');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\DadosFiscalItem');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('iddados_fiscal_item', 'IddadosFiscalItem', 'INTEGER', true, 10, null);
        $this->addForeignKey('boleto_id', 'BoletoId', 'INTEGER', 'boleto', 'idboleto', true, 10, null);
        $this->addForeignKey('boleto_item_id', 'BoletoItemId', 'INTEGER', 'boleto_item', 'idboleto_item', true, 10, null);
        $this->addColumn('item', 'Item', 'CLOB', false, null, null);
        $this->addColumn('numero_item_nota_fiscal', 'NumeroItemNotaFiscal', 'INTEGER', true, 10, null);
        $this->addColumn('valor_item', 'ValorItem', 'DECIMAL', true, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('dados_fiscal_id', 'DadosFiscalId', 'INTEGER', 'dados_fiscal', 'id', true, 10, null);
        $this->addColumn('posicao_item', 'PosicaoItem', 'INTEGER', true, 10, null);
        $this->addColumn('valor_scm', 'ValorScm', 'DECIMAL', false, 10, 0);
        $this->addColumn('valor_sva', 'ValorSva', 'DECIMAL', false, 10, 0);
        $this->addColumn('tem_scm', 'TemScm', 'BOOLEAN', false, 1, false);
        $this->addColumn('tem_sva', 'TemSva', 'BOOLEAN', false, 1, false);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Boleto', '\\ImaTelecomBundle\\Model\\Boleto', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':boleto_id',
    1 => ':idboleto',
  ),
), null, null, null, false);
        $this->addRelation('BoletoItem', '\\ImaTelecomBundle\\Model\\BoletoItem', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':boleto_item_id',
    1 => ':idboleto_item',
  ),
), null, null, null, false);
        $this->addRelation('DadosFiscal', '\\ImaTelecomBundle\\Model\\DadosFiscal', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':dados_fiscal_id',
    1 => ':id',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IddadosFiscalItem', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IddadosFiscalItem', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IddadosFiscalItem', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IddadosFiscalItem', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IddadosFiscalItem', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IddadosFiscalItem', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IddadosFiscalItem', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? DadosFiscalItemTableMap::CLASS_DEFAULT : DadosFiscalItemTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (DadosFiscalItem object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = DadosFiscalItemTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = DadosFiscalItemTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + DadosFiscalItemTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = DadosFiscalItemTableMap::OM_CLASS;
            /** @var DadosFiscalItem $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            DadosFiscalItemTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = DadosFiscalItemTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = DadosFiscalItemTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var DadosFiscalItem $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                DadosFiscalItemTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_BOLETO_ID);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_BOLETO_ITEM_ID);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_ITEM);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_NUMERO_ITEM_NOTA_FISCAL);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_VALOR_ITEM);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_DADOS_FISCAL_ID);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_POSICAO_ITEM);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_VALOR_SCM);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_VALOR_SVA);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_TEM_SCM);
            $criteria->addSelectColumn(DadosFiscalItemTableMap::COL_TEM_SVA);
        } else {
            $criteria->addSelectColumn($alias . '.iddados_fiscal_item');
            $criteria->addSelectColumn($alias . '.boleto_id');
            $criteria->addSelectColumn($alias . '.boleto_item_id');
            $criteria->addSelectColumn($alias . '.item');
            $criteria->addSelectColumn($alias . '.numero_item_nota_fiscal');
            $criteria->addSelectColumn($alias . '.valor_item');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.dados_fiscal_id');
            $criteria->addSelectColumn($alias . '.posicao_item');
            $criteria->addSelectColumn($alias . '.valor_scm');
            $criteria->addSelectColumn($alias . '.valor_sva');
            $criteria->addSelectColumn($alias . '.tem_scm');
            $criteria->addSelectColumn($alias . '.tem_sva');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(DadosFiscalItemTableMap::DATABASE_NAME)->getTable(DadosFiscalItemTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(DadosFiscalItemTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(DadosFiscalItemTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new DadosFiscalItemTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a DadosFiscalItem or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or DadosFiscalItem object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalItemTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\DadosFiscalItem) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(DadosFiscalItemTableMap::DATABASE_NAME);
            $criteria->add(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM, (array) $values, Criteria::IN);
        }

        $query = DadosFiscalItemQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            DadosFiscalItemTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                DadosFiscalItemTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the dados_fiscal_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return DadosFiscalItemQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a DadosFiscalItem or Criteria object.
     *
     * @param mixed               $criteria Criteria or DadosFiscalItem object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DadosFiscalItemTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from DadosFiscalItem object
        }

        if ($criteria->containsKey(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM) && $criteria->keyContainsValue(DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.DadosFiscalItemTableMap::COL_IDDADOS_FISCAL_ITEM.')');
        }


        // Set the correct dbName
        $query = DadosFiscalItemQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // DadosFiscalItemTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
DadosFiscalItemTableMap::buildTableMap();
