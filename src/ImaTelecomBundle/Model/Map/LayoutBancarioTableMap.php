<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\LayoutBancario;
use ImaTelecomBundle\Model\LayoutBancarioQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'layout_bancario' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class LayoutBancarioTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.LayoutBancarioTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'layout_bancario';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\LayoutBancario';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.LayoutBancario';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 41;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 41;

    /**
     * the column name for the idlayout_bancario field
     */
    const COL_IDLAYOUT_BANCARIO = 'layout_bancario.idlayout_bancario';

    /**
     * the column name for the tamanho_nosso_numero field
     */
    const COL_TAMANHO_NOSSO_NUMERO = 'layout_bancario.tamanho_nosso_numero';

    /**
     * the column name for the posicao_nosso_numero_inicial field
     */
    const COL_POSICAO_NOSSO_NUMERO_INICIAL = 'layout_bancario.posicao_nosso_numero_inicial';

    /**
     * the column name for the posicao_nosso_numero_final field
     */
    const COL_POSICAO_NOSSO_NUMERO_FINAL = 'layout_bancario.posicao_nosso_numero_final';

    /**
     * the column name for the posicao_chave_lancamento_inicial field
     */
    const COL_POSICAO_CHAVE_LANCAMENTO_INICIAL = 'layout_bancario.posicao_chave_lancamento_inicial';

    /**
     * the column name for the posicao_chave_lancamento_final field
     */
    const COL_POSICAO_CHAVE_LANCAMENTO_FINAL = 'layout_bancario.posicao_chave_lancamento_final';

    /**
     * the column name for the posicao_chave_lancamento2_inicial field
     */
    const COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL = 'layout_bancario.posicao_chave_lancamento2_inicial';

    /**
     * the column name for the posicao_chave_lancamento2_final field
     */
    const COL_POSICAO_CHAVE_LANCAMENTO2_FINAL = 'layout_bancario.posicao_chave_lancamento2_final';

    /**
     * the column name for the posicao_valor_original_inicial field
     */
    const COL_POSICAO_VALOR_ORIGINAL_INICIAL = 'layout_bancario.posicao_valor_original_inicial';

    /**
     * the column name for the posicao_valor_original_final field
     */
    const COL_POSICAO_VALOR_ORIGINAL_FINAL = 'layout_bancario.posicao_valor_original_final';

    /**
     * the column name for the posicao_valor_baixa_inicial field
     */
    const COL_POSICAO_VALOR_BAIXA_INICIAL = 'layout_bancario.posicao_valor_baixa_inicial';

    /**
     * the column name for the posicao_valor_baixa_final field
     */
    const COL_POSICAO_VALOR_BAIXA_FINAL = 'layout_bancario.posicao_valor_baixa_final';

    /**
     * the column name for the posicao_valor_juros_inicial field
     */
    const COL_POSICAO_VALOR_JUROS_INICIAL = 'layout_bancario.posicao_valor_juros_inicial';

    /**
     * the column name for the posicao_valor_juros_final field
     */
    const COL_POSICAO_VALOR_JUROS_FINAL = 'layout_bancario.posicao_valor_juros_final';

    /**
     * the column name for the posicao_valor_multa_inicial field
     */
    const COL_POSICAO_VALOR_MULTA_INICIAL = 'layout_bancario.posicao_valor_multa_inicial';

    /**
     * the column name for the posicao_valor_multa_final field
     */
    const COL_POSICAO_VALOR_MULTA_FINAL = 'layout_bancario.posicao_valor_multa_final';

    /**
     * the column name for the posicao_valor_desconto_inicial field
     */
    const COL_POSICAO_VALOR_DESCONTO_INICIAL = 'layout_bancario.posicao_valor_desconto_inicial';

    /**
     * the column name for the posicao_valor_desconto_final field
     */
    const COL_POSICAO_VALOR_DESCONTO_FINAL = 'layout_bancario.posicao_valor_desconto_final';

    /**
     * the column name for the posicao_codigo_retorno_inicial field
     */
    const COL_POSICAO_CODIGO_RETORNO_INICIAL = 'layout_bancario.posicao_codigo_retorno_inicial';

    /**
     * the column name for the posicao_codigo_retorno_final field
     */
    const COL_POSICAO_CODIGO_RETORNO_FINAL = 'layout_bancario.posicao_codigo_retorno_final';

    /**
     * the column name for the posicao_quantidade_erros_inicial field
     */
    const COL_POSICAO_QUANTIDADE_ERROS_INICIAL = 'layout_bancario.posicao_quantidade_erros_inicial';

    /**
     * the column name for the posicao_quantidade_erros_final field
     */
    const COL_POSICAO_QUANTIDADE_ERROS_FINAL = 'layout_bancario.posicao_quantidade_erros_final';

    /**
     * the column name for the posicao_numero_documento_inicial field
     */
    const COL_POSICAO_NUMERO_DOCUMENTO_INICIAL = 'layout_bancario.posicao_numero_documento_inicial';

    /**
     * the column name for the posicao_numero_documento_final field
     */
    const COL_POSICAO_NUMERO_DOCUMENTO_FINAL = 'layout_bancario.posicao_numero_documento_final';

    /**
     * the column name for the posicao_data_baixa_inicial field
     */
    const COL_POSICAO_DATA_BAIXA_INICIAL = 'layout_bancario.posicao_data_baixa_inicial';

    /**
     * the column name for the posicao_data_baixa_final field
     */
    const COL_POSICAO_DATA_BAIXA_FINAL = 'layout_bancario.posicao_data_baixa_final';

    /**
     * the column name for the formato_data_baixa field
     */
    const COL_FORMATO_DATA_BAIXA = 'layout_bancario.formato_data_baixa';

    /**
     * the column name for the posicao_data_compensacao_inicial field
     */
    const COL_POSICAO_DATA_COMPENSACAO_INICIAL = 'layout_bancario.posicao_data_compensacao_inicial';

    /**
     * the column name for the posicao_data_compensacao_final field
     */
    const COL_POSICAO_DATA_COMPENSACAO_FINAL = 'layout_bancario.posicao_data_compensacao_final';

    /**
     * the column name for the formato_data_compensacao field
     */
    const COL_FORMATO_DATA_COMPENSACAO = 'layout_bancario.formato_data_compensacao';

    /**
     * the column name for the posicao_campo_complementar_inicial field
     */
    const COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL = 'layout_bancario.posicao_campo_complementar_inicial';

    /**
     * the column name for the posicao_campo_complementar_final field
     */
    const COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL = 'layout_bancario.posicao_campo_complementar_final';

    /**
     * the column name for the formato_complementar field
     */
    const COL_FORMATO_COMPLEMENTAR = 'layout_bancario.formato_complementar';

    /**
     * the column name for the posicao_data_complementar_inicial field
     */
    const COL_POSICAO_DATA_COMPLEMENTAR_INICIAL = 'layout_bancario.posicao_data_complementar_inicial';

    /**
     * the column name for the posicao_data_complementar_final field
     */
    const COL_POSICAO_DATA_COMPLEMENTAR_FINAL = 'layout_bancario.posicao_data_complementar_final';

    /**
     * the column name for the formato_data_complementar field
     */
    const COL_FORMATO_DATA_COMPLEMENTAR = 'layout_bancario.formato_data_complementar';

    /**
     * the column name for the codigo_retorno_baixa field
     */
    const COL_CODIGO_RETORNO_BAIXA = 'layout_bancario.codigo_retorno_baixa';

    /**
     * the column name for the codigo_retorno_resgistrado field
     */
    const COL_CODIGO_RETORNO_RESGISTRADO = 'layout_bancario.codigo_retorno_resgistrado';

    /**
     * the column name for the codigo_retorno_recusado field
     */
    const COL_CODIGO_RETORNO_RECUSADO = 'layout_bancario.codigo_retorno_recusado';

    /**
     * the column name for the data_cadastrado field
     */
    const COL_DATA_CADASTRADO = 'layout_bancario.data_cadastrado';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'layout_bancario.data_alterado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdlayoutBancario', 'TamanhoNossoNumero', 'PosicaoNossoNumeroInicial', 'PosicaoNossoNumeroFinal', 'PosicaoChaveLancamentoInicial', 'PosicaoChaveLancamentoFinal', 'PosicaoChaveLancamento2Inicial', 'PosicaoChaveLancamento2Final', 'PosicaoValorOriginalInicial', 'PosicaoValorOriginalFinal', 'PosicaoValorBaixaInicial', 'PosicaoValorBaixaFinal', 'PosicaoValorJurosInicial', 'PosicaoValorJurosFinal', 'PosicaoValorMultaInicial', 'PosicaoValorMultaFinal', 'PosicaoValorDescontoInicial', 'PosicaoValorDescontoFinal', 'PosicaoCodigoRetornoInicial', 'PosicaoCodigoRetornoFinal', 'PosicaoQuantidadeErrosInicial', 'PosicaoQuantidadeErrosFinal', 'PosicaoNumeroDocumentoInicial', 'PosicaoNumeroDocumentoFinal', 'PosicaoDataBaixaInicial', 'PosicaoDataBaixaFinal', 'FormatoDataBaixa', 'PosicaoDataCompensacaoInicial', 'PosicaoDataCompensacaoFinal', 'FormatoDataCompensacao', 'PosicaoCampoComplementarInicial', 'PosicaoCampoComplementarFinal', 'FormatoComplementar', 'PosicaoDataComplementarInicial', 'PosicaoDataComplementarFinal', 'FormatoDataComplementar', 'CodigoRetornoBaixa', 'CodigoRetornoResgistrado', 'CodigoRetornoRecusado', 'DataCadastrado', 'DataAlterado', ),
        self::TYPE_CAMELNAME     => array('idlayoutBancario', 'tamanhoNossoNumero', 'posicaoNossoNumeroInicial', 'posicaoNossoNumeroFinal', 'posicaoChaveLancamentoInicial', 'posicaoChaveLancamentoFinal', 'posicaoChaveLancamento2Inicial', 'posicaoChaveLancamento2Final', 'posicaoValorOriginalInicial', 'posicaoValorOriginalFinal', 'posicaoValorBaixaInicial', 'posicaoValorBaixaFinal', 'posicaoValorJurosInicial', 'posicaoValorJurosFinal', 'posicaoValorMultaInicial', 'posicaoValorMultaFinal', 'posicaoValorDescontoInicial', 'posicaoValorDescontoFinal', 'posicaoCodigoRetornoInicial', 'posicaoCodigoRetornoFinal', 'posicaoQuantidadeErrosInicial', 'posicaoQuantidadeErrosFinal', 'posicaoNumeroDocumentoInicial', 'posicaoNumeroDocumentoFinal', 'posicaoDataBaixaInicial', 'posicaoDataBaixaFinal', 'formatoDataBaixa', 'posicaoDataCompensacaoInicial', 'posicaoDataCompensacaoFinal', 'formatoDataCompensacao', 'posicaoCampoComplementarInicial', 'posicaoCampoComplementarFinal', 'formatoComplementar', 'posicaoDataComplementarInicial', 'posicaoDataComplementarFinal', 'formatoDataComplementar', 'codigoRetornoBaixa', 'codigoRetornoResgistrado', 'codigoRetornoRecusado', 'dataCadastrado', 'dataAlterado', ),
        self::TYPE_COLNAME       => array(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO, LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL, LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL, LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL, LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL, LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL, LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL, LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL, LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL, LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL, LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL, LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL, LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL, LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL, LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL, LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL, LayoutBancarioTableMap::COL_FORMATO_DATA_BAIXA, LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL, LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL, LayoutBancarioTableMap::COL_FORMATO_DATA_COMPENSACAO, LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL, LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL, LayoutBancarioTableMap::COL_FORMATO_COMPLEMENTAR, LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL, LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL, LayoutBancarioTableMap::COL_FORMATO_DATA_COMPLEMENTAR, LayoutBancarioTableMap::COL_CODIGO_RETORNO_BAIXA, LayoutBancarioTableMap::COL_CODIGO_RETORNO_RESGISTRADO, LayoutBancarioTableMap::COL_CODIGO_RETORNO_RECUSADO, LayoutBancarioTableMap::COL_DATA_CADASTRADO, LayoutBancarioTableMap::COL_DATA_ALTERADO, ),
        self::TYPE_FIELDNAME     => array('idlayout_bancario', 'tamanho_nosso_numero', 'posicao_nosso_numero_inicial', 'posicao_nosso_numero_final', 'posicao_chave_lancamento_inicial', 'posicao_chave_lancamento_final', 'posicao_chave_lancamento2_inicial', 'posicao_chave_lancamento2_final', 'posicao_valor_original_inicial', 'posicao_valor_original_final', 'posicao_valor_baixa_inicial', 'posicao_valor_baixa_final', 'posicao_valor_juros_inicial', 'posicao_valor_juros_final', 'posicao_valor_multa_inicial', 'posicao_valor_multa_final', 'posicao_valor_desconto_inicial', 'posicao_valor_desconto_final', 'posicao_codigo_retorno_inicial', 'posicao_codigo_retorno_final', 'posicao_quantidade_erros_inicial', 'posicao_quantidade_erros_final', 'posicao_numero_documento_inicial', 'posicao_numero_documento_final', 'posicao_data_baixa_inicial', 'posicao_data_baixa_final', 'formato_data_baixa', 'posicao_data_compensacao_inicial', 'posicao_data_compensacao_final', 'formato_data_compensacao', 'posicao_campo_complementar_inicial', 'posicao_campo_complementar_final', 'formato_complementar', 'posicao_data_complementar_inicial', 'posicao_data_complementar_final', 'formato_data_complementar', 'codigo_retorno_baixa', 'codigo_retorno_resgistrado', 'codigo_retorno_recusado', 'data_cadastrado', 'data_alterado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdlayoutBancario' => 0, 'TamanhoNossoNumero' => 1, 'PosicaoNossoNumeroInicial' => 2, 'PosicaoNossoNumeroFinal' => 3, 'PosicaoChaveLancamentoInicial' => 4, 'PosicaoChaveLancamentoFinal' => 5, 'PosicaoChaveLancamento2Inicial' => 6, 'PosicaoChaveLancamento2Final' => 7, 'PosicaoValorOriginalInicial' => 8, 'PosicaoValorOriginalFinal' => 9, 'PosicaoValorBaixaInicial' => 10, 'PosicaoValorBaixaFinal' => 11, 'PosicaoValorJurosInicial' => 12, 'PosicaoValorJurosFinal' => 13, 'PosicaoValorMultaInicial' => 14, 'PosicaoValorMultaFinal' => 15, 'PosicaoValorDescontoInicial' => 16, 'PosicaoValorDescontoFinal' => 17, 'PosicaoCodigoRetornoInicial' => 18, 'PosicaoCodigoRetornoFinal' => 19, 'PosicaoQuantidadeErrosInicial' => 20, 'PosicaoQuantidadeErrosFinal' => 21, 'PosicaoNumeroDocumentoInicial' => 22, 'PosicaoNumeroDocumentoFinal' => 23, 'PosicaoDataBaixaInicial' => 24, 'PosicaoDataBaixaFinal' => 25, 'FormatoDataBaixa' => 26, 'PosicaoDataCompensacaoInicial' => 27, 'PosicaoDataCompensacaoFinal' => 28, 'FormatoDataCompensacao' => 29, 'PosicaoCampoComplementarInicial' => 30, 'PosicaoCampoComplementarFinal' => 31, 'FormatoComplementar' => 32, 'PosicaoDataComplementarInicial' => 33, 'PosicaoDataComplementarFinal' => 34, 'FormatoDataComplementar' => 35, 'CodigoRetornoBaixa' => 36, 'CodigoRetornoResgistrado' => 37, 'CodigoRetornoRecusado' => 38, 'DataCadastrado' => 39, 'DataAlterado' => 40, ),
        self::TYPE_CAMELNAME     => array('idlayoutBancario' => 0, 'tamanhoNossoNumero' => 1, 'posicaoNossoNumeroInicial' => 2, 'posicaoNossoNumeroFinal' => 3, 'posicaoChaveLancamentoInicial' => 4, 'posicaoChaveLancamentoFinal' => 5, 'posicaoChaveLancamento2Inicial' => 6, 'posicaoChaveLancamento2Final' => 7, 'posicaoValorOriginalInicial' => 8, 'posicaoValorOriginalFinal' => 9, 'posicaoValorBaixaInicial' => 10, 'posicaoValorBaixaFinal' => 11, 'posicaoValorJurosInicial' => 12, 'posicaoValorJurosFinal' => 13, 'posicaoValorMultaInicial' => 14, 'posicaoValorMultaFinal' => 15, 'posicaoValorDescontoInicial' => 16, 'posicaoValorDescontoFinal' => 17, 'posicaoCodigoRetornoInicial' => 18, 'posicaoCodigoRetornoFinal' => 19, 'posicaoQuantidadeErrosInicial' => 20, 'posicaoQuantidadeErrosFinal' => 21, 'posicaoNumeroDocumentoInicial' => 22, 'posicaoNumeroDocumentoFinal' => 23, 'posicaoDataBaixaInicial' => 24, 'posicaoDataBaixaFinal' => 25, 'formatoDataBaixa' => 26, 'posicaoDataCompensacaoInicial' => 27, 'posicaoDataCompensacaoFinal' => 28, 'formatoDataCompensacao' => 29, 'posicaoCampoComplementarInicial' => 30, 'posicaoCampoComplementarFinal' => 31, 'formatoComplementar' => 32, 'posicaoDataComplementarInicial' => 33, 'posicaoDataComplementarFinal' => 34, 'formatoDataComplementar' => 35, 'codigoRetornoBaixa' => 36, 'codigoRetornoResgistrado' => 37, 'codigoRetornoRecusado' => 38, 'dataCadastrado' => 39, 'dataAlterado' => 40, ),
        self::TYPE_COLNAME       => array(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO => 0, LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO => 1, LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL => 2, LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL => 3, LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL => 4, LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL => 5, LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL => 6, LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL => 7, LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL => 8, LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL => 9, LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL => 10, LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL => 11, LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL => 12, LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL => 13, LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL => 14, LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL => 15, LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL => 16, LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL => 17, LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL => 18, LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL => 19, LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL => 20, LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL => 21, LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL => 22, LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL => 23, LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL => 24, LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL => 25, LayoutBancarioTableMap::COL_FORMATO_DATA_BAIXA => 26, LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL => 27, LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL => 28, LayoutBancarioTableMap::COL_FORMATO_DATA_COMPENSACAO => 29, LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL => 30, LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL => 31, LayoutBancarioTableMap::COL_FORMATO_COMPLEMENTAR => 32, LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL => 33, LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL => 34, LayoutBancarioTableMap::COL_FORMATO_DATA_COMPLEMENTAR => 35, LayoutBancarioTableMap::COL_CODIGO_RETORNO_BAIXA => 36, LayoutBancarioTableMap::COL_CODIGO_RETORNO_RESGISTRADO => 37, LayoutBancarioTableMap::COL_CODIGO_RETORNO_RECUSADO => 38, LayoutBancarioTableMap::COL_DATA_CADASTRADO => 39, LayoutBancarioTableMap::COL_DATA_ALTERADO => 40, ),
        self::TYPE_FIELDNAME     => array('idlayout_bancario' => 0, 'tamanho_nosso_numero' => 1, 'posicao_nosso_numero_inicial' => 2, 'posicao_nosso_numero_final' => 3, 'posicao_chave_lancamento_inicial' => 4, 'posicao_chave_lancamento_final' => 5, 'posicao_chave_lancamento2_inicial' => 6, 'posicao_chave_lancamento2_final' => 7, 'posicao_valor_original_inicial' => 8, 'posicao_valor_original_final' => 9, 'posicao_valor_baixa_inicial' => 10, 'posicao_valor_baixa_final' => 11, 'posicao_valor_juros_inicial' => 12, 'posicao_valor_juros_final' => 13, 'posicao_valor_multa_inicial' => 14, 'posicao_valor_multa_final' => 15, 'posicao_valor_desconto_inicial' => 16, 'posicao_valor_desconto_final' => 17, 'posicao_codigo_retorno_inicial' => 18, 'posicao_codigo_retorno_final' => 19, 'posicao_quantidade_erros_inicial' => 20, 'posicao_quantidade_erros_final' => 21, 'posicao_numero_documento_inicial' => 22, 'posicao_numero_documento_final' => 23, 'posicao_data_baixa_inicial' => 24, 'posicao_data_baixa_final' => 25, 'formato_data_baixa' => 26, 'posicao_data_compensacao_inicial' => 27, 'posicao_data_compensacao_final' => 28, 'formato_data_compensacao' => 29, 'posicao_campo_complementar_inicial' => 30, 'posicao_campo_complementar_final' => 31, 'formato_complementar' => 32, 'posicao_data_complementar_inicial' => 33, 'posicao_data_complementar_final' => 34, 'formato_data_complementar' => 35, 'codigo_retorno_baixa' => 36, 'codigo_retorno_resgistrado' => 37, 'codigo_retorno_recusado' => 38, 'data_cadastrado' => 39, 'data_alterado' => 40, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('layout_bancario');
        $this->setPhpName('LayoutBancario');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\LayoutBancario');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idlayout_bancario', 'IdlayoutBancario', 'INTEGER', true, 10, null);
        $this->addColumn('tamanho_nosso_numero', 'TamanhoNossoNumero', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_nosso_numero_inicial', 'PosicaoNossoNumeroInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_nosso_numero_final', 'PosicaoNossoNumeroFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_chave_lancamento_inicial', 'PosicaoChaveLancamentoInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_chave_lancamento_final', 'PosicaoChaveLancamentoFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_chave_lancamento2_inicial', 'PosicaoChaveLancamento2Inicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_chave_lancamento2_final', 'PosicaoChaveLancamento2Final', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_original_inicial', 'PosicaoValorOriginalInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_original_final', 'PosicaoValorOriginalFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_baixa_inicial', 'PosicaoValorBaixaInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_baixa_final', 'PosicaoValorBaixaFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_juros_inicial', 'PosicaoValorJurosInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_juros_final', 'PosicaoValorJurosFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_multa_inicial', 'PosicaoValorMultaInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_multa_final', 'PosicaoValorMultaFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_desconto_inicial', 'PosicaoValorDescontoInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_valor_desconto_final', 'PosicaoValorDescontoFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_codigo_retorno_inicial', 'PosicaoCodigoRetornoInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_codigo_retorno_final', 'PosicaoCodigoRetornoFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_quantidade_erros_inicial', 'PosicaoQuantidadeErrosInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_quantidade_erros_final', 'PosicaoQuantidadeErrosFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_numero_documento_inicial', 'PosicaoNumeroDocumentoInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_numero_documento_final', 'PosicaoNumeroDocumentoFinal', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_data_baixa_inicial', 'PosicaoDataBaixaInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_data_baixa_final', 'PosicaoDataBaixaFinal', 'INTEGER', true, 10, null);
        $this->addColumn('formato_data_baixa', 'FormatoDataBaixa', 'CHAR', true, null, null);
        $this->addColumn('posicao_data_compensacao_inicial', 'PosicaoDataCompensacaoInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_data_compensacao_final', 'PosicaoDataCompensacaoFinal', 'INTEGER', true, 10, null);
        $this->addColumn('formato_data_compensacao', 'FormatoDataCompensacao', 'CHAR', true, null, null);
        $this->addColumn('posicao_campo_complementar_inicial', 'PosicaoCampoComplementarInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_campo_complementar_final', 'PosicaoCampoComplementarFinal', 'INTEGER', true, 10, null);
        $this->addColumn('formato_complementar', 'FormatoComplementar', 'VARCHAR', true, 45, null);
        $this->addColumn('posicao_data_complementar_inicial', 'PosicaoDataComplementarInicial', 'INTEGER', true, 10, null);
        $this->addColumn('posicao_data_complementar_final', 'PosicaoDataComplementarFinal', 'INTEGER', true, 10, null);
        $this->addColumn('formato_data_complementar', 'FormatoDataComplementar', 'CHAR', true, null, null);
        $this->addColumn('codigo_retorno_baixa', 'CodigoRetornoBaixa', 'VARCHAR', true, 250, null);
        $this->addColumn('codigo_retorno_resgistrado', 'CodigoRetornoResgistrado', 'VARCHAR', true, 45, null);
        $this->addColumn('codigo_retorno_recusado', 'CodigoRetornoRecusado', 'VARCHAR', true, 45, null);
        $this->addColumn('data_cadastrado', 'DataCadastrado', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Banco', '\\ImaTelecomBundle\\Model\\Banco', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':layout_bancario_id',
    1 => ':idlayout_bancario',
  ),
), null, null, 'Bancos', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlayoutBancario', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlayoutBancario', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlayoutBancario', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlayoutBancario', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlayoutBancario', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdlayoutBancario', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdlayoutBancario', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? LayoutBancarioTableMap::CLASS_DEFAULT : LayoutBancarioTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (LayoutBancario object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = LayoutBancarioTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = LayoutBancarioTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + LayoutBancarioTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = LayoutBancarioTableMap::OM_CLASS;
            /** @var LayoutBancario $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            LayoutBancarioTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = LayoutBancarioTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = LayoutBancarioTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var LayoutBancario $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                LayoutBancarioTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_TAMANHO_NOSSO_NUMERO);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_NOSSO_NUMERO_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_CHAVE_LANCAMENTO2_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_ORIGINAL_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_BAIXA_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_JUROS_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_MULTA_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_VALOR_DESCONTO_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_CODIGO_RETORNO_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_QUANTIDADE_ERROS_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_NUMERO_DOCUMENTO_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_DATA_BAIXA_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_FORMATO_DATA_BAIXA);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPENSACAO_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPENSACAO);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_CAMPO_COMPLEMENTAR_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_FORMATO_COMPLEMENTAR);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_INICIAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_POSICAO_DATA_COMPLEMENTAR_FINAL);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_FORMATO_DATA_COMPLEMENTAR);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_CODIGO_RETORNO_BAIXA);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RESGISTRADO);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_CODIGO_RETORNO_RECUSADO);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_DATA_CADASTRADO);
            $criteria->addSelectColumn(LayoutBancarioTableMap::COL_DATA_ALTERADO);
        } else {
            $criteria->addSelectColumn($alias . '.idlayout_bancario');
            $criteria->addSelectColumn($alias . '.tamanho_nosso_numero');
            $criteria->addSelectColumn($alias . '.posicao_nosso_numero_inicial');
            $criteria->addSelectColumn($alias . '.posicao_nosso_numero_final');
            $criteria->addSelectColumn($alias . '.posicao_chave_lancamento_inicial');
            $criteria->addSelectColumn($alias . '.posicao_chave_lancamento_final');
            $criteria->addSelectColumn($alias . '.posicao_chave_lancamento2_inicial');
            $criteria->addSelectColumn($alias . '.posicao_chave_lancamento2_final');
            $criteria->addSelectColumn($alias . '.posicao_valor_original_inicial');
            $criteria->addSelectColumn($alias . '.posicao_valor_original_final');
            $criteria->addSelectColumn($alias . '.posicao_valor_baixa_inicial');
            $criteria->addSelectColumn($alias . '.posicao_valor_baixa_final');
            $criteria->addSelectColumn($alias . '.posicao_valor_juros_inicial');
            $criteria->addSelectColumn($alias . '.posicao_valor_juros_final');
            $criteria->addSelectColumn($alias . '.posicao_valor_multa_inicial');
            $criteria->addSelectColumn($alias . '.posicao_valor_multa_final');
            $criteria->addSelectColumn($alias . '.posicao_valor_desconto_inicial');
            $criteria->addSelectColumn($alias . '.posicao_valor_desconto_final');
            $criteria->addSelectColumn($alias . '.posicao_codigo_retorno_inicial');
            $criteria->addSelectColumn($alias . '.posicao_codigo_retorno_final');
            $criteria->addSelectColumn($alias . '.posicao_quantidade_erros_inicial');
            $criteria->addSelectColumn($alias . '.posicao_quantidade_erros_final');
            $criteria->addSelectColumn($alias . '.posicao_numero_documento_inicial');
            $criteria->addSelectColumn($alias . '.posicao_numero_documento_final');
            $criteria->addSelectColumn($alias . '.posicao_data_baixa_inicial');
            $criteria->addSelectColumn($alias . '.posicao_data_baixa_final');
            $criteria->addSelectColumn($alias . '.formato_data_baixa');
            $criteria->addSelectColumn($alias . '.posicao_data_compensacao_inicial');
            $criteria->addSelectColumn($alias . '.posicao_data_compensacao_final');
            $criteria->addSelectColumn($alias . '.formato_data_compensacao');
            $criteria->addSelectColumn($alias . '.posicao_campo_complementar_inicial');
            $criteria->addSelectColumn($alias . '.posicao_campo_complementar_final');
            $criteria->addSelectColumn($alias . '.formato_complementar');
            $criteria->addSelectColumn($alias . '.posicao_data_complementar_inicial');
            $criteria->addSelectColumn($alias . '.posicao_data_complementar_final');
            $criteria->addSelectColumn($alias . '.formato_data_complementar');
            $criteria->addSelectColumn($alias . '.codigo_retorno_baixa');
            $criteria->addSelectColumn($alias . '.codigo_retorno_resgistrado');
            $criteria->addSelectColumn($alias . '.codigo_retorno_recusado');
            $criteria->addSelectColumn($alias . '.data_cadastrado');
            $criteria->addSelectColumn($alias . '.data_alterado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(LayoutBancarioTableMap::DATABASE_NAME)->getTable(LayoutBancarioTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(LayoutBancarioTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(LayoutBancarioTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new LayoutBancarioTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a LayoutBancario or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or LayoutBancario object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LayoutBancarioTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\LayoutBancario) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(LayoutBancarioTableMap::DATABASE_NAME);
            $criteria->add(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO, (array) $values, Criteria::IN);
        }

        $query = LayoutBancarioQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            LayoutBancarioTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                LayoutBancarioTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the layout_bancario table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return LayoutBancarioQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a LayoutBancario or Criteria object.
     *
     * @param mixed               $criteria Criteria or LayoutBancario object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LayoutBancarioTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from LayoutBancario object
        }

        if ($criteria->containsKey(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO) && $criteria->keyContainsValue(LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.LayoutBancarioTableMap::COL_IDLAYOUT_BANCARIO.')');
        }


        // Set the correct dbName
        $query = LayoutBancarioQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // LayoutBancarioTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
LayoutBancarioTableMap::buildTableMap();
