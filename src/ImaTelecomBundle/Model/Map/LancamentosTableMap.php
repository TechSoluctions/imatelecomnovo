<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Lancamentos;
use ImaTelecomBundle\Model\LancamentosQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'lancamentos' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class LancamentosTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.LancamentosTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'lancamentos';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Lancamentos';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Lancamentos';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the idlancamento field
     */
    const COL_IDLANCAMENTO = 'lancamentos.idlancamento';

    /**
     * the column name for the conteudo_arquivo_saida field
     */
    const COL_CONTEUDO_ARQUIVO_SAIDA = 'lancamentos.conteudo_arquivo_saida';

    /**
     * the column name for the competencia_id field
     */
    const COL_COMPETENCIA_ID = 'lancamentos.competencia_id';

    /**
     * the column name for the data_geracao field
     */
    const COL_DATA_GERACAO = 'lancamentos.data_geracao';

    /**
     * the column name for the data_cadastro field
     */
    const COL_DATA_CADASTRO = 'lancamentos.data_cadastro';

    /**
     * the column name for the data_alterado field
     */
    const COL_DATA_ALTERADO = 'lancamentos.data_alterado';

    /**
     * the column name for the usuario_alterado field
     */
    const COL_USUARIO_ALTERADO = 'lancamentos.usuario_alterado';

    /**
     * the column name for the convenio_id field
     */
    const COL_CONVENIO_ID = 'lancamentos.convenio_id';

    /**
     * the column name for the sequencial_arquivo_cobranca field
     */
    const COL_SEQUENCIAL_ARQUIVO_COBRANCA = 'lancamentos.sequencial_arquivo_cobranca';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idlancamento', 'ConteudoArquivoSaida', 'CompetenciaId', 'DataGeracao', 'DataCadastro', 'DataAlterado', 'UsuarioAlterado', 'ConvenioId', 'SequencialArquivoCobranca', ),
        self::TYPE_CAMELNAME     => array('idlancamento', 'conteudoArquivoSaida', 'competenciaId', 'dataGeracao', 'dataCadastro', 'dataAlterado', 'usuarioAlterado', 'convenioId', 'sequencialArquivoCobranca', ),
        self::TYPE_COLNAME       => array(LancamentosTableMap::COL_IDLANCAMENTO, LancamentosTableMap::COL_CONTEUDO_ARQUIVO_SAIDA, LancamentosTableMap::COL_COMPETENCIA_ID, LancamentosTableMap::COL_DATA_GERACAO, LancamentosTableMap::COL_DATA_CADASTRO, LancamentosTableMap::COL_DATA_ALTERADO, LancamentosTableMap::COL_USUARIO_ALTERADO, LancamentosTableMap::COL_CONVENIO_ID, LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA, ),
        self::TYPE_FIELDNAME     => array('idlancamento', 'conteudo_arquivo_saida', 'competencia_id', 'data_geracao', 'data_cadastro', 'data_alterado', 'usuario_alterado', 'convenio_id', 'sequencial_arquivo_cobranca', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idlancamento' => 0, 'ConteudoArquivoSaida' => 1, 'CompetenciaId' => 2, 'DataGeracao' => 3, 'DataCadastro' => 4, 'DataAlterado' => 5, 'UsuarioAlterado' => 6, 'ConvenioId' => 7, 'SequencialArquivoCobranca' => 8, ),
        self::TYPE_CAMELNAME     => array('idlancamento' => 0, 'conteudoArquivoSaida' => 1, 'competenciaId' => 2, 'dataGeracao' => 3, 'dataCadastro' => 4, 'dataAlterado' => 5, 'usuarioAlterado' => 6, 'convenioId' => 7, 'sequencialArquivoCobranca' => 8, ),
        self::TYPE_COLNAME       => array(LancamentosTableMap::COL_IDLANCAMENTO => 0, LancamentosTableMap::COL_CONTEUDO_ARQUIVO_SAIDA => 1, LancamentosTableMap::COL_COMPETENCIA_ID => 2, LancamentosTableMap::COL_DATA_GERACAO => 3, LancamentosTableMap::COL_DATA_CADASTRO => 4, LancamentosTableMap::COL_DATA_ALTERADO => 5, LancamentosTableMap::COL_USUARIO_ALTERADO => 6, LancamentosTableMap::COL_CONVENIO_ID => 7, LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA => 8, ),
        self::TYPE_FIELDNAME     => array('idlancamento' => 0, 'conteudo_arquivo_saida' => 1, 'competencia_id' => 2, 'data_geracao' => 3, 'data_cadastro' => 4, 'data_alterado' => 5, 'usuario_alterado' => 6, 'convenio_id' => 7, 'sequencial_arquivo_cobranca' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('lancamentos');
        $this->setPhpName('Lancamentos');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Lancamentos');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idlancamento', 'Idlancamento', 'INTEGER', true, 10, null);
        $this->addColumn('conteudo_arquivo_saida', 'ConteudoArquivoSaida', 'CLOB', false, null, null);
        $this->addForeignKey('competencia_id', 'CompetenciaId', 'INTEGER', 'competencia', 'id', true, 10, null);
        $this->addColumn('data_geracao', 'DataGeracao', 'VARCHAR', false, 10, null);
        $this->addColumn('data_cadastro', 'DataCadastro', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_alterado', 'DataAlterado', 'TIMESTAMP', true, null, null);
        $this->addColumn('usuario_alterado', 'UsuarioAlterado', 'INTEGER', true, 10, null);
        $this->addForeignKey('convenio_id', 'ConvenioId', 'INTEGER', 'convenio', 'idconvenio', true, 10, null);
        $this->addColumn('sequencial_arquivo_cobranca', 'SequencialArquivoCobranca', 'INTEGER', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Competencia', '\\ImaTelecomBundle\\Model\\Competencia', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':competencia_id',
    1 => ':id',
  ),
), null, null, null, false);
        $this->addRelation('Convenio', '\\ImaTelecomBundle\\Model\\Convenio', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':convenio_id',
    1 => ':idconvenio',
  ),
), null, null, null, false);
        $this->addRelation('BoletoBaixaHistorico', '\\ImaTelecomBundle\\Model\\BoletoBaixaHistorico', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':lancamento_id',
    1 => ':idlancamento',
  ),
), null, null, 'BoletoBaixaHistoricos', false);
        $this->addRelation('LancamentosBoletos', '\\ImaTelecomBundle\\Model\\LancamentosBoletos', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':lancamento_id',
    1 => ':idlancamento',
  ),
), null, null, 'LancamentosBoletoss', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idlancamento', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idlancamento', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idlancamento', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idlancamento', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idlancamento', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idlancamento', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idlancamento', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? LancamentosTableMap::CLASS_DEFAULT : LancamentosTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Lancamentos object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = LancamentosTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = LancamentosTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + LancamentosTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = LancamentosTableMap::OM_CLASS;
            /** @var Lancamentos $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            LancamentosTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = LancamentosTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = LancamentosTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Lancamentos $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                LancamentosTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(LancamentosTableMap::COL_IDLANCAMENTO);
            $criteria->addSelectColumn(LancamentosTableMap::COL_CONTEUDO_ARQUIVO_SAIDA);
            $criteria->addSelectColumn(LancamentosTableMap::COL_COMPETENCIA_ID);
            $criteria->addSelectColumn(LancamentosTableMap::COL_DATA_GERACAO);
            $criteria->addSelectColumn(LancamentosTableMap::COL_DATA_CADASTRO);
            $criteria->addSelectColumn(LancamentosTableMap::COL_DATA_ALTERADO);
            $criteria->addSelectColumn(LancamentosTableMap::COL_USUARIO_ALTERADO);
            $criteria->addSelectColumn(LancamentosTableMap::COL_CONVENIO_ID);
            $criteria->addSelectColumn(LancamentosTableMap::COL_SEQUENCIAL_ARQUIVO_COBRANCA);
        } else {
            $criteria->addSelectColumn($alias . '.idlancamento');
            $criteria->addSelectColumn($alias . '.conteudo_arquivo_saida');
            $criteria->addSelectColumn($alias . '.competencia_id');
            $criteria->addSelectColumn($alias . '.data_geracao');
            $criteria->addSelectColumn($alias . '.data_cadastro');
            $criteria->addSelectColumn($alias . '.data_alterado');
            $criteria->addSelectColumn($alias . '.usuario_alterado');
            $criteria->addSelectColumn($alias . '.convenio_id');
            $criteria->addSelectColumn($alias . '.sequencial_arquivo_cobranca');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(LancamentosTableMap::DATABASE_NAME)->getTable(LancamentosTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(LancamentosTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(LancamentosTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new LancamentosTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Lancamentos or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Lancamentos object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Lancamentos) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(LancamentosTableMap::DATABASE_NAME);
            $criteria->add(LancamentosTableMap::COL_IDLANCAMENTO, (array) $values, Criteria::IN);
        }

        $query = LancamentosQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            LancamentosTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                LancamentosTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the lancamentos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return LancamentosQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Lancamentos or Criteria object.
     *
     * @param mixed               $criteria Criteria or Lancamentos object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LancamentosTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Lancamentos object
        }

        if ($criteria->containsKey(LancamentosTableMap::COL_IDLANCAMENTO) && $criteria->keyContainsValue(LancamentosTableMap::COL_IDLANCAMENTO) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.LancamentosTableMap::COL_IDLANCAMENTO.')');
        }


        // Set the correct dbName
        $query = LancamentosQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // LancamentosTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
LancamentosTableMap::buildTableMap();
