<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\ContasPagarParcelas;
use ImaTelecomBundle\Model\ContasPagarParcelasQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'contas_pagar_parcelas' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ContasPagarParcelasTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.ContasPagarParcelasTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'contas_pagar_parcelas';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\ContasPagarParcelas';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.ContasPagarParcelas';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the idcontas_pagar_parcelas field
     */
    const COL_IDCONTAS_PAGAR_PARCELAS = 'contas_pagar_parcelas.idcontas_pagar_parcelas';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'contas_pagar_parcelas.valor';

    /**
     * the column name for the vencimento field
     */
    const COL_VENCIMENTO = 'contas_pagar_parcelas.vencimento';

    /**
     * the column name for the conta_pagar_id field
     */
    const COL_CONTA_PAGAR_ID = 'contas_pagar_parcelas.conta_pagar_id';

    /**
     * the column name for the num_parcela field
     */
    const COL_NUM_PARCELA = 'contas_pagar_parcelas.num_parcela';

    /**
     * the column name for the caixa_id field
     */
    const COL_CAIXA_ID = 'contas_pagar_parcelas.caixa_id';

    /**
     * the column name for the esta_pago field
     */
    const COL_ESTA_PAGO = 'contas_pagar_parcelas.esta_pago';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('IdcontasPagarParcelas', 'Valor', 'Vencimento', 'ContaPagarId', 'NumParcela', 'CaixaId', 'EstaPago', ),
        self::TYPE_CAMELNAME     => array('idcontasPagarParcelas', 'valor', 'vencimento', 'contaPagarId', 'numParcela', 'caixaId', 'estaPago', ),
        self::TYPE_COLNAME       => array(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, ContasPagarParcelasTableMap::COL_VALOR, ContasPagarParcelasTableMap::COL_VENCIMENTO, ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID, ContasPagarParcelasTableMap::COL_NUM_PARCELA, ContasPagarParcelasTableMap::COL_CAIXA_ID, ContasPagarParcelasTableMap::COL_ESTA_PAGO, ),
        self::TYPE_FIELDNAME     => array('idcontas_pagar_parcelas', 'valor', 'vencimento', 'conta_pagar_id', 'num_parcela', 'caixa_id', 'esta_pago', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('IdcontasPagarParcelas' => 0, 'Valor' => 1, 'Vencimento' => 2, 'ContaPagarId' => 3, 'NumParcela' => 4, 'CaixaId' => 5, 'EstaPago' => 6, ),
        self::TYPE_CAMELNAME     => array('idcontasPagarParcelas' => 0, 'valor' => 1, 'vencimento' => 2, 'contaPagarId' => 3, 'numParcela' => 4, 'caixaId' => 5, 'estaPago' => 6, ),
        self::TYPE_COLNAME       => array(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS => 0, ContasPagarParcelasTableMap::COL_VALOR => 1, ContasPagarParcelasTableMap::COL_VENCIMENTO => 2, ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID => 3, ContasPagarParcelasTableMap::COL_NUM_PARCELA => 4, ContasPagarParcelasTableMap::COL_CAIXA_ID => 5, ContasPagarParcelasTableMap::COL_ESTA_PAGO => 6, ),
        self::TYPE_FIELDNAME     => array('idcontas_pagar_parcelas' => 0, 'valor' => 1, 'vencimento' => 2, 'conta_pagar_id' => 3, 'num_parcela' => 4, 'caixa_id' => 5, 'esta_pago' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('contas_pagar_parcelas');
        $this->setPhpName('ContasPagarParcelas');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\ContasPagarParcelas');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idcontas_pagar_parcelas', 'IdcontasPagarParcelas', 'INTEGER', true, 10, null);
        $this->addColumn('valor', 'Valor', 'DECIMAL', true, 8, null);
        $this->addColumn('vencimento', 'Vencimento', 'DATE', true, null, null);
        $this->addForeignKey('conta_pagar_id', 'ContaPagarId', 'INTEGER', 'contas_pagar', 'idcontas_pagar', true, 10, null);
        $this->addColumn('num_parcela', 'NumParcela', 'SMALLINT', true, 5, null);
        $this->addForeignKey('caixa_id', 'CaixaId', 'INTEGER', 'caixa', 'idcaixa', false, 10, null);
        $this->addColumn('esta_pago', 'EstaPago', 'BOOLEAN', true, 1, false);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Caixa', '\\ImaTelecomBundle\\Model\\Caixa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':caixa_id',
    1 => ':idcaixa',
  ),
), null, null, null, false);
        $this->addRelation('ContasPagar', '\\ImaTelecomBundle\\Model\\ContasPagar', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':conta_pagar_id',
    1 => ':idcontas_pagar',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarParcelas', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarParcelas', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarParcelas', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarParcelas', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarParcelas', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('IdcontasPagarParcelas', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('IdcontasPagarParcelas', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ContasPagarParcelasTableMap::CLASS_DEFAULT : ContasPagarParcelasTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ContasPagarParcelas object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ContasPagarParcelasTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ContasPagarParcelasTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ContasPagarParcelasTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ContasPagarParcelasTableMap::OM_CLASS;
            /** @var ContasPagarParcelas $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ContasPagarParcelasTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ContasPagarParcelasTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ContasPagarParcelasTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ContasPagarParcelas $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ContasPagarParcelasTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS);
            $criteria->addSelectColumn(ContasPagarParcelasTableMap::COL_VALOR);
            $criteria->addSelectColumn(ContasPagarParcelasTableMap::COL_VENCIMENTO);
            $criteria->addSelectColumn(ContasPagarParcelasTableMap::COL_CONTA_PAGAR_ID);
            $criteria->addSelectColumn(ContasPagarParcelasTableMap::COL_NUM_PARCELA);
            $criteria->addSelectColumn(ContasPagarParcelasTableMap::COL_CAIXA_ID);
            $criteria->addSelectColumn(ContasPagarParcelasTableMap::COL_ESTA_PAGO);
        } else {
            $criteria->addSelectColumn($alias . '.idcontas_pagar_parcelas');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.vencimento');
            $criteria->addSelectColumn($alias . '.conta_pagar_id');
            $criteria->addSelectColumn($alias . '.num_parcela');
            $criteria->addSelectColumn($alias . '.caixa_id');
            $criteria->addSelectColumn($alias . '.esta_pago');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ContasPagarParcelasTableMap::DATABASE_NAME)->getTable(ContasPagarParcelasTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ContasPagarParcelasTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ContasPagarParcelasTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ContasPagarParcelasTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ContasPagarParcelas or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ContasPagarParcelas object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarParcelasTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\ContasPagarParcelas) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ContasPagarParcelasTableMap::DATABASE_NAME);
            $criteria->add(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS, (array) $values, Criteria::IN);
        }

        $query = ContasPagarParcelasQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ContasPagarParcelasTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ContasPagarParcelasTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the contas_pagar_parcelas table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ContasPagarParcelasQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ContasPagarParcelas or Criteria object.
     *
     * @param mixed               $criteria Criteria or ContasPagarParcelas object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContasPagarParcelasTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ContasPagarParcelas object
        }

        if ($criteria->containsKey(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS) && $criteria->keyContainsValue(ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ContasPagarParcelasTableMap::COL_IDCONTAS_PAGAR_PARCELAS.')');
        }


        // Set the correct dbName
        $query = ContasPagarParcelasQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ContasPagarParcelasTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ContasPagarParcelasTableMap::buildTableMap();
