<?php

namespace ImaTelecomBundle\Model\Map;

use ImaTelecomBundle\Model\Caixa;
use ImaTelecomBundle\Model\CaixaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'caixa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CaixaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\ImaTelecomBundle.Model.Map.CaixaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'ima_telecom';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'caixa';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ImaTelecomBundle\\Model\\Caixa';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\ImaTelecomBundle.Model.Caixa';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the idcaixa field
     */
    const COL_IDCAIXA = 'caixa.idcaixa';

    /**
     * the column name for the tipo field
     */
    const COL_TIPO = 'caixa.tipo';

    /**
     * the column name for the data_pagamento field
     */
    const COL_DATA_PAGAMENTO = 'caixa.data_pagamento';

    /**
     * the column name for the data_baixa field
     */
    const COL_DATA_BAIXA = 'caixa.data_baixa';

    /**
     * the column name for the usuario_baixa field
     */
    const COL_USUARIO_BAIXA = 'caixa.usuario_baixa';

    /**
     * the column name for the valor_pago field
     */
    const COL_VALOR_PAGO = 'caixa.valor_pago';

    /**
     * the column name for the valor_juros field
     */
    const COL_VALOR_JUROS = 'caixa.valor_juros';

    /**
     * the column name for the valor_desconto field
     */
    const COL_VALOR_DESCONTO = 'caixa.valor_desconto';

    /**
     * the column name for the observacao field
     */
    const COL_OBSERVACAO = 'caixa.observacao';

    /**
     * the column name for the forma_pagamento_id field
     */
    const COL_FORMA_PAGAMENTO_ID = 'caixa.forma_pagamento_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idcaixa', 'Tipo', 'DataPagamento', 'DataBaixa', 'UsuarioBaixa', 'ValorPago', 'ValorJuros', 'ValorDesconto', 'Observacao', 'FormaPagamentoId', ),
        self::TYPE_CAMELNAME     => array('idcaixa', 'tipo', 'dataPagamento', 'dataBaixa', 'usuarioBaixa', 'valorPago', 'valorJuros', 'valorDesconto', 'observacao', 'formaPagamentoId', ),
        self::TYPE_COLNAME       => array(CaixaTableMap::COL_IDCAIXA, CaixaTableMap::COL_TIPO, CaixaTableMap::COL_DATA_PAGAMENTO, CaixaTableMap::COL_DATA_BAIXA, CaixaTableMap::COL_USUARIO_BAIXA, CaixaTableMap::COL_VALOR_PAGO, CaixaTableMap::COL_VALOR_JUROS, CaixaTableMap::COL_VALOR_DESCONTO, CaixaTableMap::COL_OBSERVACAO, CaixaTableMap::COL_FORMA_PAGAMENTO_ID, ),
        self::TYPE_FIELDNAME     => array('idcaixa', 'tipo', 'data_pagamento', 'data_baixa', 'usuario_baixa', 'valor_pago', 'valor_juros', 'valor_desconto', 'observacao', 'forma_pagamento_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idcaixa' => 0, 'Tipo' => 1, 'DataPagamento' => 2, 'DataBaixa' => 3, 'UsuarioBaixa' => 4, 'ValorPago' => 5, 'ValorJuros' => 6, 'ValorDesconto' => 7, 'Observacao' => 8, 'FormaPagamentoId' => 9, ),
        self::TYPE_CAMELNAME     => array('idcaixa' => 0, 'tipo' => 1, 'dataPagamento' => 2, 'dataBaixa' => 3, 'usuarioBaixa' => 4, 'valorPago' => 5, 'valorJuros' => 6, 'valorDesconto' => 7, 'observacao' => 8, 'formaPagamentoId' => 9, ),
        self::TYPE_COLNAME       => array(CaixaTableMap::COL_IDCAIXA => 0, CaixaTableMap::COL_TIPO => 1, CaixaTableMap::COL_DATA_PAGAMENTO => 2, CaixaTableMap::COL_DATA_BAIXA => 3, CaixaTableMap::COL_USUARIO_BAIXA => 4, CaixaTableMap::COL_VALOR_PAGO => 5, CaixaTableMap::COL_VALOR_JUROS => 6, CaixaTableMap::COL_VALOR_DESCONTO => 7, CaixaTableMap::COL_OBSERVACAO => 8, CaixaTableMap::COL_FORMA_PAGAMENTO_ID => 9, ),
        self::TYPE_FIELDNAME     => array('idcaixa' => 0, 'tipo' => 1, 'data_pagamento' => 2, 'data_baixa' => 3, 'usuario_baixa' => 4, 'valor_pago' => 5, 'valor_juros' => 6, 'valor_desconto' => 7, 'observacao' => 8, 'forma_pagamento_id' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('caixa');
        $this->setPhpName('Caixa');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ImaTelecomBundle\\Model\\Caixa');
        $this->setPackage('src\ImaTelecomBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idcaixa', 'Idcaixa', 'INTEGER', true, 10, null);
        $this->addColumn('tipo', 'Tipo', 'CHAR', true, null, null);
        $this->addColumn('data_pagamento', 'DataPagamento', 'TIMESTAMP', true, null, null);
        $this->addColumn('data_baixa', 'DataBaixa', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('usuario_baixa', 'UsuarioBaixa', 'INTEGER', 'usuario', 'idusuario', true, 10, null);
        $this->addColumn('valor_pago', 'ValorPago', 'DECIMAL', true, 8, null);
        $this->addColumn('valor_juros', 'ValorJuros', 'DECIMAL', true, 8, null);
        $this->addColumn('valor_desconto', 'ValorDesconto', 'DECIMAL', true, 8, null);
        $this->addColumn('observacao', 'Observacao', 'VARCHAR', false, 255, null);
        $this->addColumn('forma_pagamento_id', 'FormaPagamentoId', 'INTEGER', true, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Usuario', '\\ImaTelecomBundle\\Model\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usuario_baixa',
    1 => ':idusuario',
  ),
), null, null, null, false);
        $this->addRelation('CaixaMovimento', '\\ImaTelecomBundle\\Model\\CaixaMovimento', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':caixa_id',
    1 => ':idcaixa',
  ),
), null, null, 'CaixaMovimentos', false);
        $this->addRelation('ContasPagarParcelas', '\\ImaTelecomBundle\\Model\\ContasPagarParcelas', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':caixa_id',
    1 => ':idcaixa',
  ),
), null, null, 'ContasPagarParcelass', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcaixa', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcaixa', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcaixa', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcaixa', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcaixa', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcaixa', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idcaixa', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CaixaTableMap::CLASS_DEFAULT : CaixaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Caixa object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CaixaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CaixaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CaixaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CaixaTableMap::OM_CLASS;
            /** @var Caixa $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CaixaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CaixaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CaixaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Caixa $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CaixaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CaixaTableMap::COL_IDCAIXA);
            $criteria->addSelectColumn(CaixaTableMap::COL_TIPO);
            $criteria->addSelectColumn(CaixaTableMap::COL_DATA_PAGAMENTO);
            $criteria->addSelectColumn(CaixaTableMap::COL_DATA_BAIXA);
            $criteria->addSelectColumn(CaixaTableMap::COL_USUARIO_BAIXA);
            $criteria->addSelectColumn(CaixaTableMap::COL_VALOR_PAGO);
            $criteria->addSelectColumn(CaixaTableMap::COL_VALOR_JUROS);
            $criteria->addSelectColumn(CaixaTableMap::COL_VALOR_DESCONTO);
            $criteria->addSelectColumn(CaixaTableMap::COL_OBSERVACAO);
            $criteria->addSelectColumn(CaixaTableMap::COL_FORMA_PAGAMENTO_ID);
        } else {
            $criteria->addSelectColumn($alias . '.idcaixa');
            $criteria->addSelectColumn($alias . '.tipo');
            $criteria->addSelectColumn($alias . '.data_pagamento');
            $criteria->addSelectColumn($alias . '.data_baixa');
            $criteria->addSelectColumn($alias . '.usuario_baixa');
            $criteria->addSelectColumn($alias . '.valor_pago');
            $criteria->addSelectColumn($alias . '.valor_juros');
            $criteria->addSelectColumn($alias . '.valor_desconto');
            $criteria->addSelectColumn($alias . '.observacao');
            $criteria->addSelectColumn($alias . '.forma_pagamento_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CaixaTableMap::DATABASE_NAME)->getTable(CaixaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CaixaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CaixaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CaixaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Caixa or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Caixa object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CaixaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ImaTelecomBundle\Model\Caixa) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CaixaTableMap::DATABASE_NAME);
            $criteria->add(CaixaTableMap::COL_IDCAIXA, (array) $values, Criteria::IN);
        }

        $query = CaixaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CaixaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CaixaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the caixa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CaixaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Caixa or Criteria object.
     *
     * @param mixed               $criteria Criteria or Caixa object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CaixaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Caixa object
        }

        if ($criteria->containsKey(CaixaTableMap::COL_IDCAIXA) && $criteria->keyContainsValue(CaixaTableMap::COL_IDCAIXA) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CaixaTableMap::COL_IDCAIXA.')');
        }


        // Set the correct dbName
        $query = CaixaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CaixaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CaixaTableMap::buildTableMap();
