<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\ItemDeCompraQuery as BaseItemDeCompraQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'item_de_compra' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ItemDeCompraQuery extends BaseItemDeCompraQuery
{

}
