<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Contrato as BaseContrato;

/**
 * Skeleton subclass for representing a row from the 'contrato' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Contrato extends BaseContrato {

    public function __construct() {
        //$this->applyDefaultValues();

        $tipoFaturamento = TipoFaturamentoQuery::create()->findOne();
        $this->setTipoFaturamentoId($tipoFaturamento->getId());
        $this->setTemScm(0);
        $this->setTipoScm('valor');
        $this->setTemSva(0);
        $this->setTipoSva('valor');
        $this->setForcarValor(0);
        $this->setGerarCobranca(0);
    }

    public function getTipoFaturamento() {
        $tipoFaturamento = TipoFaturamentoQuery::create()->findPk($this->tipo_faturamento_id);
        if ($tipoFaturamento == null) {
            $tipoFaturamento = new TipoFaturamento();
        }

        return $tipoFaturamento;
    }

}
