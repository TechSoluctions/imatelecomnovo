<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\CronTask as BaseCronTask;

/**
 * Skeleton subclass for representing a row from the 'cron_task' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CronTask extends BaseCronTask {

    public function getTipos() {
        $tipos = array(
            array('key' => 'nenhum', 'value' => 'Nenhum'),
            array('key' => 'arquivo', 'value' => 'Arquivo')
        );

        return $tipos;
    }

    public function getPrioridades() {
        $prioridades = array(
            array('key' => 'nenhuma', 'value' => 'Nenhuma'),
            array('key' => 'baixa', 'value' => 'Baixa'),
            array('key' => 'media', 'value' => 'Média'),
            array('key' => 'alta', 'value' => 'Alta'),
            array('key' => 'muito_alta', 'value' => 'Muito Alta')
        );
        
        return $prioridades;
    }

    public function getTiposExecucao() {        
        $tiposExecucaoArquivos = array(
            array('key' => 'unica', 'value' => 'Unica'),
            array('key' => 'multipla', 'value' => 'Multipla')
        );
        
        return $tiposExecucaoArquivos;
    }

    public function getGrupos() {        
        $grupos = CronTaskGrupoQuery::create()->find();
        return $grupos;
    }

    public function getLogsExecucao($limit, $orderColumn, $orderType) {    
        $id = $this->getId();
        if (empty($id))
            return array();
        
        $logs = CronTaskLogExecucaoQuery::create()->where("cron_task_id = $id")->limit($limit)->orderBy($orderColumn, $orderType)->find();
        return $logs;
    }

    public function getArquivos() {    
        $id = $this->getId();
        if (empty($id))
            return array();
        
        $arquivos = CronTaskArquivosQuery::create()->findByCronTaskId($id);
        return $arquivos;
    }

}
