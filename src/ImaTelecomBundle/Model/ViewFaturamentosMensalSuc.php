<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\ViewFaturamentosMensalSuc as BaseViewFaturamentosMensalSuc;

/**
 * Skeleton subclass for representing a row from the 'view_faturamentos_mensal_suc' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ViewFaturamentosMensalSuc extends BaseViewFaturamentosMensalSuc
{

}
