<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Usuario as BaseUsuario;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * Skeleton subclass for representing a row from the 'usuario' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Usuario extends BaseUsuario implements AdvancedUserInterface, \Serializable {
	
    public function isEnabled() {
        //return $this->ativo;
		//return false;
        return true;
    }

    public function isAccountNonExpired() {
        return true;
    }

    public function isAccountNonLocked() {
        return true;
    }

    public function isCredentialsNonExpired() {
        return true;
    }

    public function getRoles() {
        return array("");
    }

    public function getSalt() {
        //throw new \Exception('sdafds');
        return "c";
    }

    public function eraseCredentials() {
        $this->password = null;
    }

    public function serialize() {
		/*
        return serialize(array(
            $this->idusuario,
            $this->login,
            $this->password,
        ));
		*/
		return serialize(array(
            $this->idusuario,
            $this->login
        ));
    }

    public function unserialize($serialized) {
        list (
                $this->idusuario,
                $this->login
                //$this->password,
                // see section on salt below
                // $this->salt
                ) = unserialize($serialized);
    }

    public function getUsername() {
        return $this->login;
    }

	public function getPassword() {
        return "";
    }

}