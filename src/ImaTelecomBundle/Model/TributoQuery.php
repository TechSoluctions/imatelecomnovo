<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\TributoQuery as BaseTributoQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'tributo' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class TributoQuery extends BaseTributoQuery
{

}
