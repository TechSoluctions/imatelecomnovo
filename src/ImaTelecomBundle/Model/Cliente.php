<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Cliente as BaseCliente;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for representing a row from the 'cliente' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Cliente extends BaseCliente {

    public function __construct() {
        $this->ativo = 1;
    }

    public function getNotas() {
        $notas = array();

        $itemsCobranca = BoletoItemQuery::create()
                ->joinBoleto()
                ->where('cliente_id = ' . $this->idcliente)
                ->groupByBoletoId()
                ->find();

        foreach ($itemsCobranca as $item) {
            $nota = DadosFiscalQuery::create()
                    ->withColumn('boleto.cliente_id', 'clienteId')
                    ->withColumn('pessoa.nome', 'nome')
                    ->useBoletoQuery()->useClienteQuery()->joinPessoa()->endUse()->endUse()
                    ->findByBoletoId($item->getBoletoId());
            if (sizeof($nota)) {
                $notas[] = $nota;
            }
        }

        return $notas;
    }

    public function getEnderecoCliente() {
        $enderecoCliente = EnderecoClienteQuery::create()->findByClienteId($this->idcliente)->getLast();
        return $enderecoCliente;
    }

}
