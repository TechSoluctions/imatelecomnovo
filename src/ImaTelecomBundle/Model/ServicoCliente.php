<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\ServicoCliente as BaseServicoCliente;
use \Doctrine\Common\Collections\Criteria;

/**
 * Skeleton subclass for representing a row from the 'servico_cliente' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ServicoCliente extends BaseServicoCliente {

    public function RegrasFaturamento() {
        $regras = RegraFaturamentoQuery::create()->find();
        return $regras;
    }

    public function TiposFaturamento() {
        $tipos = TipoFaturamentoQuery::create()->find();
        return $tipos;
    }

    public function Contrato() {
        $contrato = ContratoQuery::create()->findPk($this->contrato_id);
        if ($contrato == null) {
            $contrato = new Contrato();
        }

        return $contrato;
    }

    public function getNotas() {
        $notas = array();
        $itemsCobranca = BoletoItemQuery::create()->filterByServicoClienteId($this->idservico_cliente)->groupByBoletoId()->find();
        foreach ($itemsCobranca as $item) {
            $nota = DadosFiscalQuery::create()
                    ->withColumn('boleto.cliente_id', 'clienteId')
                    ->withColumn('pessoa.nome', 'nome')
                    ->useBoletoQuery()->useClienteQuery()->joinPessoa()->endUse()->endUse()
                    ->orderByNumeroNotaFiscal(Criteria::DESC)
                    ->findByBoletoId($item->getBoletoId());
            if (sizeof($nota)) {
                $notas[] = $nota;
            }
        }

        return $notas;
    }

    public function Endereco() {
        $endereco = EnderecoServClienteQuery::create()->findByServicoClienteId($this->idservico_cliente)->getLast();
        if ($endereco == null) {
            $endereco = new EnderecoServCliente();
        }

        return $endereco;
    }

    /*
      public function getContrato() {
      return ContratoQuery::create()->findPk($this->contrato_id);
      } */
    /*
      public function setPlano($plano) {
      $planoId = null;
      if ($plano != null) {
      $planoId = $plano->getIdplano();
      }

      $this->setPlanoId($planoId);
      }

      public function getPlano() {
      $planoId = $this->getPlanoId();
      if ($planoId != null) {
      return PlanosQuery::create()->findPk($planoId);
      }

      return PlanosQuery::create()->findOne();
      } */
}
