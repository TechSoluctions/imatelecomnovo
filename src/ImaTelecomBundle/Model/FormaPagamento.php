<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\FormaPagamento as BaseFormaPagamento;

/**
 * Skeleton subclass for representing a row from the 'forma_pagamento' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FormaPagamento extends BaseFormaPagamento
{

}
