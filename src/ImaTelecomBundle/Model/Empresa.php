<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Empresa as BaseEmpresa;

/**
 * Skeleton subclass for representing a row from the 'empresa' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Empresa extends BaseEmpresa
{

}
