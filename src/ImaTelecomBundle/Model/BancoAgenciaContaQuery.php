<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\BancoAgenciaContaQuery as BaseBancoAgenciaContaQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'banco_agencia_conta' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BancoAgenciaContaQuery extends BaseBancoAgenciaContaQuery
{

}
