<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\BoletoItem as BaseBoletoItem;

/**
 * Skeleton subclass for representing a row from the 'boleto_item' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BoletoItem extends BaseBoletoItem
{

}
