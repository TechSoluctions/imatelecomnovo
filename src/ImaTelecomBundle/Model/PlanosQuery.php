<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\PlanosQuery as BasePlanosQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'planos' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PlanosQuery extends BasePlanosQuery
{

}
