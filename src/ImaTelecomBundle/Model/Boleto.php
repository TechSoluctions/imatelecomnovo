<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\Boleto as BaseBoleto;

/**
 * Skeleton subclass for representing a row from the 'boleto' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Boleto extends BaseBoleto
{
    private $erros = array();
    
    public function getErros() {
        return $this->erros;
    }

    public function getNotas() {
        $notas = DadosFiscalItemQuery::create()->joinDadosFiscal(null, 'right join')->findByBoletoId($this->getIdboleto());
        return $notas;
    }
    
    public function hasNotaFiscal() {
        $itensBoleto = array();
        $boletoItem = $this->getBoletoItems();
        
        foreach ($boletoItem as $item) {
            $itensBoleto[] = $item->getIdboletoItem();
        }
        
        $itens = implode(', ', $itensBoleto);
        $notas = DadosFiscalQuery::create()->where("boleto_item_id in ($itens) or boleto_id = " . $this->idboleto)->find();        
        return $notas->count() > 0 ? true : false;
    }
    
    public function hasLancamentos() {
        $lancamentos = LancamentosBoletosQuery::create()->findByBoletoId($this->getIdboleto());
        return $lancamentos->count() > 0 ? true : false;
    }
    
    public function hasParcelaPorServicoCliente($servicoCliente) {        
        if (!is_numeric($servicoCliente))
            $servicoCliente = $servicoCliente->getIdservicoCliente();
        
        $parcela = BoletoItemQuery::create()->filterByBoleto($this)->findOneByServicoClienteId($servicoCliente);
        return $parcela != null ? true : false;
    }
    
    public function getParcelaPorServicoCliente($servicoCliente) {        
        if (!is_numeric($servicoCliente))
            $servicoCliente = $servicoCliente->getIdservicoCliente();
        
        $parcela = BoletoItemQuery::create()->filterByBoleto($this)->findOneByServicoClienteId($servicoCliente);
        return $parcela;
    }
    
    public function getValorTotalParcelas() {        
        $valorTotal = 0.0;
        $valorTotalBaseCalculoScm = 0.0;
        $valorTotalBaseCalculoSva = 0.0;
        $parcelas = BoletoItemQuery::create()->findByBoletoId($this->idboleto);
        foreach ($parcelas as $parcela) {
            $valorTotal += $parcela->getValor();
            $valorTotalBaseCalculoScm += $parcela->getBaseCalculoScm();
            $valorTotalBaseCalculoSva += $parcela->getBaseCalculoSva();
        }
        
        $retorno = array();
        $retorno['valorTotal'] = $valorTotal;
        $retorno['valorTotalBaseCalculoScm'] = $valorTotalBaseCalculoScm;
        $retorno['valorTotalBaseCalculoSva'] = $valorTotalBaseCalculoSva;
        
        return $retorno;
    }
}
