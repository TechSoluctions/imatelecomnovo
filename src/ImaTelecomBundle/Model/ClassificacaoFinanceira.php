<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\ClassificacaoFinanceira as BaseClassificacaoFinanceira;

/**
 * Skeleton subclass for representing a row from the 'classificacao_financeira' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ClassificacaoFinanceira extends BaseClassificacaoFinanceira
{

}
