<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\EnderecoAgencia as BaseEnderecoAgencia;

/**
 * Skeleton subclass for representing a row from the 'endereco_agencia' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class EnderecoAgencia extends BaseEnderecoAgencia
{
    public function cidades() {
        $cidades = CidadeQuery::create()->orderByMunicipio()->find();
        return $cidades;
    }
}
