<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\CompetenciaQuery as BaseCompetenciaQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'competencia' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CompetenciaQuery extends BaseCompetenciaQuery
{

}
