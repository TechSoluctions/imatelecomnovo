<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\SysPerfilProcesso as BaseSysPerfilProcesso;

/**
 * Skeleton subclass for representing a row from the 'sys_perfil_processo' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SysPerfilProcesso extends BaseSysPerfilProcesso
{

}
