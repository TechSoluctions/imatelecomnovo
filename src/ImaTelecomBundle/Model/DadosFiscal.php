<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\DadosFiscal as BaseDadosFiscal;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for representing a row from the 'dados_fiscal' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DadosFiscal extends BaseDadosFiscal {

    public function boleto() {
        $boletoId = $this->boleto_id;
        $boleto = BoletoQuery::create()->findPk($boletoId);
        return $boleto;
    }

    public function boletoItem() {
        $boletoItem = BoletoItemQuery::create()->findPk($this->boleto_item_id);

        if ($boletoItem == null)
            $boletoItem = new BoletoItem();

        return $boletoItem;
    }
    
    /*
    public function pessoaNota() {
        $cliente = $this->cliente();
        $pessoa = $cliente->getPessoa();

        return $pessoa;
    }*/

    public function cliente() {
        $cliente = ClienteQuery::create()->joinBoleto()->where('idboleto = ' . $this->boleto_id)->findOne();
        /*
          if ($cliente == null) {
          $cobranca = BoletoQuery::create()->joinBoletoItem()->where('idboleto_item = ' . $this->boleto_item_id)->findOne();
          $cliente = $cobranca->getCliente();
          } */

        return $cliente;
    }

    /*
    public function getBoleto() {
        $boleto = BoletoQuery::create()->findPk($this->boleto_id);
        if ($boleto == null) {
            //$boleto = DadosFiscalItemQuery::create()->filterByDadosFiscal($this)->groupByBoletoId()->findOne();
            // adicionar boleto item, modelos 2017 e 2016
            $boleto = new Boleto();
        }

        return $boleto;
    }

    public function isUltimaNota() {
        $ultimaNota = DadosFiscalQuery::create()
                ->filterByBoletoId($this->boleto_id)
                ->orderById(Criteria::DESC)
                ->findOne();

        return $ultimaNota->getId() == $this->id ? true : false;
    }
*/
}
