<?php

namespace ImaTelecomBundle\Model;

use ImaTelecomBundle\Model\Base\ServicoPrestado as BaseServicoPrestado;
use ImaTelecomBundle\Model\Planos;

/**
 * Skeleton subclass for representing a row from the 'servico_prestado' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ServicoPrestado extends BaseServicoPrestado {
    
    public function setPlano($plano) {
        $planoId = null;
        if ($plano != null) {
            $planoId = $plano->getIdplano();
        }

        $this->setPlanoId($planoId);
    }

    public function getPlano() {
        $planoId = $this->getPlanoId();
        if ($planoId != null) {
            return PlanosQuery::create()->findPk($planoId);
        }

        return PlanosQuery::create()->findOne();
    }
    
    public function usuario() {
        $usuario = UsuarioQuery::create()->findPk($this->getUsuarioAlterado());        
        if ($usuario == null)
            $usuario = new Usuario();
        return $usuario;
    }

}
