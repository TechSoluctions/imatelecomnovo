<?php

namespace ImaTelecomBundle\Lib;

include __DIR__ . '/BoletoCNAB240.php';

class BoletoSantanderCNAB240 extends BoletoCNAB240 {

    private $NOME_BANCO = "Banco Santander"; // Tamanho máximo de 30 caracteres
    protected $COMPLETE_INTEIRO = "0";
    protected $COMPLETE_BRANCO = " ";
    private $CODIGO_BANCO_COMPENSACAO = "033"; // Código do Banco na compensação
    protected $MENSAGEM_1 = "";
    protected $MENSAGEM_2 = "";
    private $TIPO_DA_INSCRICAO_DA_EMPRESA = "2"; // 1 = CPF, 2 = CNPJ
    private $NUMERO_DA_INSCRICAO_DA_EMPRESA = "10934273000167"; // CNPJ
    private $CODIGO_TRANSMISSAO = "082200008782750"; // Código de Transmissão
    private $CODIGO_CONVENIO = ""; // Código do Convênio
    private $NUMERO_DA_CONTA_CORRENTE = "013000665";
    private $DIGITO_VERIFICADOR_CONTA_CORRENTE = "5";
    private $NOME_DA_EMPRESA = "IMA TELECOM LTDA - ME";
    private $AGENCIA_DESTINATARIA_CONTA = "0822";
    private $DIGITO_VERIFICADOR_AGENCIA_DESTINATARIA_CONTA = "2";
    private $VERSAO_LAYOUT_LOTE = "030";
    private $NUMERO_SEQUENCIAL_ARQUIVO_COBRANCA = 0;
    private $dados_lancamento = array();
    private $cobrancas = array();
    private $parametrosVisualizacaoBoleto = array();

    public function __construct($parametros = array(), $parametrosVisualizacaoBoleto = array()) {
        $this->MENSAGEM_1 = substr("", 0, 30);
        $this->MENSAGEM_2 = substr("", 0, 30);

        $this->setVariaveisGlobais($parametros);
        $this->setParametrosVisualizacaoBoleto($parametrosVisualizacaoBoleto);
    }

    private function setVariaveisGlobais($parametros = array()) {
        if (array_key_exists('nome_banco', $parametros)) {
            $this->NOME_BANCO = substr($parametros['nome_banco'], 0, 30);
        }

        if (array_key_exists('codigo_febraban', $parametros)) {
            $this->CODIGO_BANCO_COMPENSACAO = $parametros['codigo_febraban']; // Código de Transmissão
        }

        if (array_key_exists('tipo_pessoa', $parametros)) {
            // 1 = CPF, 2 = CNPJ
            if ($parametros['tipo_pessoa'] == 'fisica') {
                $this->TIPO_DA_INSCRICAO_DA_EMPRESA = 1;
            } else {
                $this->TIPO_DA_INSCRICAO_DA_EMPRESA = 2;
            }
        }

        if (array_key_exists('cpf_cnpj', $parametros)) {
            $this->NUMERO_DA_INSCRICAO_DA_EMPRESA = $parametros['cpf_cnpj'];
        }

        if (array_key_exists('numero_conta_corrente', $parametros)) {
            $this->NUMERO_DA_CONTA_CORRENTE = $parametros['numero_conta_corrente'];
        }

        if (array_key_exists('digito_verificador_conta_corrente', $parametros)) {
            $this->DIGITO_VERIFICADOR_CONTA_CORRENTE = $parametros['digito_verificador_conta_corrente'];
        }

        if (array_key_exists('nome_empresa', $parametros)) {
            $this->NOME_DA_EMPRESA = substr($parametros['nome_empresa'], 0, 30);
        }

        if (array_key_exists('banco_agencia', $parametros)) {
            $this->AGENCIA_DESTINATARIA_CONTA = $parametros['banco_agencia'];
        }

        if (array_key_exists('banco_agencia_digito_verificador', $parametros)) {
            $this->DIGITO_VERIFICADOR_AGENCIA_DESTINATARIA_CONTA = $parametros['banco_agencia_digito_verificador'];
        }

        if (array_key_exists('versao_layout', $parametros)) {
            $this->VERSAO_LAYOUT_LOTE = $parametros['versao_layout'];
        }

        if (array_key_exists('ultimo_sequencial_arquivo_cobranca', $parametros)) {
            $this->NUMERO_SEQUENCIAL_ARQUIVO_COBRANCA = $parametros['ultimo_sequencial_arquivo_cobranca'];
        }

        if (array_key_exists('codigo_convenio', $parametros)) {
            $this->CODIGO_CONVENIO = $parametros['codigo_convenio'];
        }
        /*
          if (array_key_exists('ultimo_sequencial_arquivo_cobranca', $parametros)) {
          $this->NUMERO_SEQUENCIAL_ARQUIVO_COBRANCA = $parametros['ultimo_sequencial_arquivo_cobranca'];
          } */
    }

    private function getLinhaDigitavel($boleto, $nossoNumero = "") {
        if (is_array($boleto)) {
            $dataVencimento = \DateTime::createFromFormat('Y-m-d', $boleto['vencimento']);
            $valorBoleto = number_format($boleto['valor'], 2, ',', '');
            $cobrancaId = $boleto['idboleto'];            
            $nossoNumero = $this->getNossoNumero($boleto);
            $nossoNumeroDv = str_pad($nossoNumero . $this->Modulo_11($nossoNumero), 13, "0", STR_PAD_LEFT);
        } else {
            $dataVencimento = $boleto->getVencimento();
            $valorBoleto = number_format($boleto->getValor(), 2, ',', '');
            $cobrancaId = $boleto->getIdboleto();
            $nossoNumero = $boleto->getNossoNumero();            
            $nossoNumero = substr($nossoNumero, 0, strlen($nossoNumero) -1);
            $nossoNumeroDv = str_pad($nossoNumero . $this->Modulo_11($nossoNumero), 13, "0", STR_PAD_LEFT);
        }

        $codigoBanco = $this->CODIGO_BANCO_COMPENSACAO;
        $codigoMoeda = "9";
        $fatorVencimento = $this->getQuantidadeDias($dataVencimento->format('Y-m-d'));
        $valorNominal = str_pad(str_replace(",", "", $valorBoleto), 10, "0", STR_PAD_LEFT);
        $valorFixo = "9";
        $numeroPSK = $this->CODIGO_CONVENIO;

        
        //$nossoNumero = $this->getNossoNumero($boleto);	// nosso numero está gerando o dv duas vezes por isso o erro
        //$nossoNumero = substr($nossoNumero, 0, strlen($nossoNumero) -1);
        //$nossoNumeroDv = str_pad($nossoNumero . $this->Modulo_11($nossoNumero), 13, "0", STR_PAD_LEFT); //str_pad($nossoNumero . $this->Modulo_11($nossoNumero), 13, "0", STR_PAD_LEFT);
        /*
        if (empty($nossoNumero)) {
            $nossoNumeroDv = str_pad($cobrancaId . $this->Modulo_11($cobrancaId), 13, "0", STR_PAD_LEFT);
        } else {
            $nossoNumeroDv = str_pad($nossoNumero . $this->Modulo_11($nossoNumero), 13, "0", STR_PAD_LEFT);
        }*/			
		
        $IOF = "0";
        $tipoCobranca = "101";
        $digitoVerificadorCodigoBarras = $this->Modulo_11($codigoBanco . $codigoMoeda . $fatorVencimento . $valorNominal . $valorFixo . $numeroPSK . $nossoNumeroDv . $IOF . $tipoCobranca);

        // Gerando linha digitavel
        $primeiroGrupo = $codigoBanco;    // 01-03        Banco = 033
        $primeiroGrupo .= $codigoMoeda;     // 04-04        Código da moeda = 9 (real)  /   Código da moeda = 8 (outras moedas)
        $primeiroGrupo .= $valorFixo;     // 05-05        Fixo “9”
        $primeiroGrupo .= substr($numeroPSK, 0, 4);  // 06-09        Código do Beneficiário padrão Santander
        $primeiroGrupo .= $this->Modulo_10($primeiroGrupo);       // 10-10        Código verificador do primeiro grupo
        $primeiroGrupo = substr_replace($primeiroGrupo, '.', 5, 0);

        $segundoGrupo = substr($numeroPSK, -3);   // 11-13        Restante do código do beneficiário padrão Santander
        $segundoGrupo .= substr($nossoNumeroDv, 0, 7);     // 14-20        7 primeiros campos do N/N
        $segundoGrupo .= $this->Modulo_10($segundoGrupo);     // 21-21        Dígito verificador do segundo grupo
        $segundoGrupo = substr_replace($segundoGrupo, '.', 5, 0);

        $terceiroGrupo = substr($nossoNumeroDv, -6);  // 22-27        Restante do Nosso Número com DV
        $terceiroGrupo .= $IOF; // 28-28      IOF – somente para Seguradoras (Se 7% informar 7, limitado a 9%). Demais clientes usar 0 (zero)
        $terceiroGrupo .= $tipoCobranca; // 29-31      101-Cobrança Simples Rápida COM Registro
        // 29-31      102- Cobrança simples SEM Registro
        // 29-31      201- Penhor
        $terceiroGrupo .= $this->Modulo_10($terceiroGrupo);     // 32-32      Dígito verificador do terceiro grupo
        $terceiroGrupo = substr_replace($terceiroGrupo, '.', 5, 0);

        $quartoGrupo = $digitoVerificadorCodigoBarras; // 33-33     Dígito Verificador do Código de Barras

        $quintoGrupo = $fatorVencimento;
        $quintoGrupo .= $valorNominal;

        $linhaDigitavel = "$primeiroGrupo $segundoGrupo $terceiroGrupo $quartoGrupo $quintoGrupo";
        $codigoBarras = $codigoBanco . $codigoMoeda . $digitoVerificadorCodigoBarras . $fatorVencimento . $valorNominal . $valorFixo . $numeroPSK . $nossoNumeroDv . $IOF . $tipoCobranca;
		
        $retorno = array();
        $retorno['linhaDigitavel'] = $linhaDigitavel;
        $retorno['digitoVerificadorCodigoBarras'] = $digitoVerificadorCodigoBarras;
        $retorno['codigoBarras'] = $codigoBarras;

        return $retorno;
    }

    public function setParametrosVisualizacaoBoleto($parametros = array()) {
        if (array_key_exists('codigo_banco_com_dv', $parametros)) {
            $this->parametrosVisualizacaoBoleto['codigo_banco_com_dv'] = $parametros['codigo_banco_com_dv'];
        }

        if (array_key_exists('local_pagamento', $parametros)) {
            $this->parametrosVisualizacaoBoleto['local_pagamento'] = $parametros['local_pagamento'];
        }

        if (array_key_exists('beneficiario', $parametros)) {
            $this->parametrosVisualizacaoBoleto['beneficiario'] = $parametros['beneficiario'];
        }

        if (array_key_exists('numero_beneficiario', $parametros)) {
            $this->parametrosVisualizacaoBoleto['numero_beneficiario'] = $parametros['numero_beneficiario'];
        }

        if (array_key_exists('agencia_beneficiario', $parametros)) {
            $this->parametrosVisualizacaoBoleto['agencia_beneficiario'] = $parametros['agencia_beneficiario'];
        }

        if (array_key_exists('agencia_beneficiario_dv', $parametros)) {
            $this->parametrosVisualizacaoBoleto['agencia_beneficiario_dv'] = $parametros['agencia_beneficiario_dv'];
        }

        if (array_key_exists('codigo_beneficiario', $parametros)) {
            $this->parametrosVisualizacaoBoleto['codigo_beneficiario'] = $parametros['codigo_beneficiario'];
        }

        if (array_key_exists('endereco_beneficiario', $parametros)) {
            $this->parametrosVisualizacaoBoleto['endereco_beneficiario'] = $parametros['endereco_beneficiario'];
        }

        if (array_key_exists('email_beneficiario', $parametros)) {
            $this->parametrosVisualizacaoBoleto['email_beneficiario'] = $parametros['email_beneficiario'];
        }

        if (array_key_exists('telefone_beneficiario', $parametros)) {
            $this->parametrosVisualizacaoBoleto['telefone_beneficiario'] = $parametros['telefone_beneficiario'];
        }

        if (array_key_exists('especie_documento', $parametros)) {
            $this->parametrosVisualizacaoBoleto['especie_documento'] = $parametros['especie_documento'];
        }

        if (array_key_exists('aceite', $parametros)) {
            $this->parametrosVisualizacaoBoleto['aceite'] = $parametros['aceite'];
        }

        if (array_key_exists('descricao_carteira', $parametros)) {
            $this->parametrosVisualizacaoBoleto['descricao_carteira'] = $parametros['descricao_carteira'];
        }

        if (array_key_exists('especie', $parametros)) {
            $this->parametrosVisualizacaoBoleto['especie'] = $parametros['especie'];
        }

        if (array_key_exists('quantidade', $parametros)) {
            $this->parametrosVisualizacaoBoleto['quantidade'] = $parametros['quantidade'];
        }

        if (array_key_exists('valor', $parametros)) {
            $this->parametrosVisualizacaoBoleto['valor'] = $parametros['valor'];
        }

        if (array_key_exists('nome_empresa', $parametros)) {
            $this->parametrosVisualizacaoBoleto['nome_empresa'] = $parametros['nome_empresa'];
        }
    }

    public function getBoletoCobranca($boletos, $ambiente) {
        $retorno = array('boletos' => array());

        foreach ($boletos as $boleto) {
            $cobrancaId = $boleto->getIdboleto();
            $valor = $boleto->getValor();
            $dataVencimento = $boleto->getVencimento();
            $dadosLinhaDigitavel = $this->getLinhaDigitavel($boleto);
            $codigoBarras = $dadosLinhaDigitavel['codigoBarras'];
            $nossoNumeroDv = $this->getNossoNumero($boleto, true); // str_pad($boleto["idboleto"] . $this->Modulo_11($boleto["idboleto"]), 13, "0", STR_PAD_LEFT);
            $dataGeracao = new \DateTime('now');
            $dataGeracaoArquivo = date_format($dataGeracao, "d/m/Y");
            $cliente = $boleto->getCliente();
            $endereco = $cliente->getEnderecoCliente();
            $cidadeEndereco = $endereco->getCidade();
            $pessoa = $cliente->getPessoa();
            $nome = $pessoa->getNome();
            $cpfCnpj = $pessoa->getCpfCnpj();

            // pega os endereços de cobrança do cliente
            $enderecoCliente = array();
            $enderecoCliente['endereco'] = $endereco->getRua();
            $enderecoCliente['numero'] = $endereco->getNum();
            $enderecoCliente['bairro'] = $endereco->getBairro();
            $enderecoCliente['cep'] = $endereco->getCep();
            $enderecoCliente['municipio'] = $cidadeEndereco->getMunicipio();
            $enderecoCliente['uf'] = $cidadeEndereco->getUf();

            $dadosBoleto = $this->parametrosVisualizacaoBoleto;
            $dadosBoleto["servico_beneficiario"] = ''; //$boleto['nome_servico_prestado'];
            $dadosBoleto["data_vencimento"] = $dataVencimento->format('d/m/Y');
            $dadosBoleto["valor_documento"] = number_format($valor, 2, ',', '');
            $dadosBoleto["nosso_numero"] = $nossoNumeroDv;
            $dadosBoleto['linha_digitavel'] = $dadosLinhaDigitavel['linhaDigitavel']; //"$primeiroGrupo $segundoGrupo $terceiroGrupo $quartoGrupo $quintoGrupo";
            $dadosBoleto['codigo_barras'] = "data:image/jpg;base64,"; //gethostname()
            $dadosBoleto['codigo_barras_texto'] = $codigoBarras;

            if ($ambiente == "dev") {
                $dadosBoleto['codigo_barras'] .= base64_encode(file_get_contents("https://desenvolvimento01.censanet.com.br/imatelecom/lib/barcode.php?codetype=Code25&size=40&text=$codigoBarras")); //base64_encode('https://' . gethostname() . '/suc/lib/barcode.php?codetype=Code25&size=40&text=' . $bradescoBoleto->getCodigoBarras());
            } else {
                $dadosBoleto['codigo_barras'] .= base64_encode(file_get_contents("https://" . gethostname() . "/imatelecom/lib/barcode.php?codetype=Code25&size=40&text=$codigoBarras"));
            }

            // INFORMACOES PARA O CLIENTE
            $dadosBoleto["instrucoes1"] = "<p style='height:35px;'></p>"; //"Não receber após <b>" . $dadosBoleto["data_vencimento"] . "</b>";
            $dadosBoleto["instrucoes2"] = "Após o vencimento cobrar multa de 2,00%";
            $dadosBoleto["instrucoes3"] = "Após o vencimento cobrar juro de mora de 1,00% ao mês ";
            $dadosBoleto["instrucoes4"] = "   "; // "Até <b>15/01/2017</b> conceder desconto de R$ 1,00";
            $dadosBoleto["sacado_cliente"] = strtoupper($nome) . ' - ' . $cpfCnpj; //"Nome do Cliente - CPF 357.814.098-82";  // Nome do seu cliente e cpf ou cnpj
            $dadosBoleto["sacado_endereco"] = strtoupper($enderecoCliente['endereco'] . ', ' . $enderecoCliente['numero'] . ' - ' . $enderecoCliente['bairro'] . ' - ' . $enderecoCliente['cep'] . ' ' . $enderecoCliente['municipio'] . '/' . $enderecoCliente['uf']); //.  "Rua Holanda, 64 - Casa - 71200-100 Rio de Janeiro-RJ"; // Rua, Número, Complemento, CEP, Bairro, Cidade/UF
            $dadosBoleto["numero_documento"] = str_pad($cobrancaId, 15, "0", STR_PAD_LEFT);
            $dadosBoleto["data_processamento"] = $dataGeracaoArquivo; // Data atual da geração do boleto para visualização
            $dadosBoleto["data_documento"] = $dataGeracaoArquivo; // Data atual da geração do boleto para visualização

            $retorno['boletos'][] = $dadosBoleto;
        }

        return $retorno;
    }

    public function setCobrancas($boletos) {
        $this->cobrancas = $boletos;
    }

    public function gerarArquivoRemessa() {
        $dataGeracao = new \DateTime('now');

        $this->dados_lancamento['numero_sequencial_arquivo_cobranca'] = $this->NUMERO_SEQUENCIAL_ARQUIVO_COBRANCA;
        $this->dados_lancamento['quantidade_lotes_arquivo'] = 0;
        $this->dados_lancamento['numero_remessa_retorno'] = 0;
        $this->dados_lancamento['quantidade_de_lotes_do_arquivo'] = 0;
        $this->dados_lancamento['quantidade_de_registros_do_arquivo'] = 0;
        $this->dados_lancamento['data_geracao_arquivo'] = date_format($dataGeracao, "dmY");

        $retorno = array();
        $retorno['dadosCobranca'] = array();
        $retorno['dataGeracao'] = $this->dados_lancamento['data_geracao_arquivo'];

        $textoArquivo = $this->getHeaderArquivoRemessa();
        for ($header = 1; $header <= 1; $header++) {
            $textoArquivo .= $this->getHeaderLoteRemessa();

            // corpo dos lancamentos, SEGMENTOS
            foreach ($this->cobrancas['cobrancas'] as $boleto) {
                $linha = $this->getSegmentoP($boleto);
                $linha .= $this->getSegmentoQ($boleto);
                $linha .= $this->getSegmentoR($boleto);

                $textoArquivo .= $linha;
                $nossoNumero = $this->getNossoNumero($boleto);
                $dadosLinhaDigitavel = $this->getLinhaDigitavel($boleto, $nossoNumero);
                $retorno['dadosCobranca'][$boleto['idboleto']]['linhaDigitavel'] = $dadosLinhaDigitavel['linhaDigitavel'];
                $retorno['dadosCobranca'][$boleto['idboleto']]['nossoNumero'] = $nossoNumero;
                //$retorno['dadosCobranca'][$boleto['idboleto']]['numeroDocumento'] = $boleto['idboleto'];
                $retorno['dadosCobranca'][$boleto['idboleto']]['numeroDocumento'] = $this->cobrancas['lancamento'];
                $retorno['dadosCobranca'][$boleto['idboleto']]['textoCobranca'] = $linha;
            }

            $textoArquivo .= $this->getTrailerLoteRemessa();
        }

        $textoArquivo .= $this->getTrailerArquivoRemessa();
        $retorno['textoArquivo'] = $textoArquivo;
        $retorno['ultimoSequencialArquivoCobranca'] = $this->dados_lancamento['numero_sequencial_arquivo_cobranca'];

        $arquivo = fopen("arquivo_saida_santander.txt", "w");
        fwrite($arquivo, $textoArquivo);
        fclose($arquivo);

        return $retorno;
    }

    private function getNossoNumero($boleto, $comDigitoVerificador = false) {
        if (is_array($boleto)) {
            $cobrancaId = $boleto['idboleto']; //$boleto->getIdboleto();
        } else {
            $cobrancaId = $boleto->getIdboleto();
        }

        if (!$comDigitoVerificador) {
            return $cobrancaId . $this->Modulo_11($cobrancaId);
        } else {
            return str_pad($cobrancaId . $this->Modulo_11($cobrancaId), 13, "0", STR_PAD_LEFT);
        }
    }

    // HEADERS DE REMESSA
    private function getHeaderArquivoRemessa() {
        // Variáveis para geração do HEADER_ARQUIVO
        $codigoRemessa = "1";
        $numeroSequencialArquivo = 0; // salvar em banco e recuperar depois o sequencial
        $this->dados_lancamento['numero_sequencial_arquivo_cobranca'] ++;
        //$dados_lancamento['quantidade_lotes_arquivo'] = 0; // Somatória dos registros de tipo 0, 1, 3, 5 e 9
        $tipoRegistroHeaderArquivo = "0";


        // HEADER DO ARQUIVO REMESSA
        $HEADER_ARQUIVO = $this->CODIGO_BANCO_COMPENSACAO; // Código do Banco na Compensação
        $HEADER_ARQUIVO .= str_pad("0000", 4, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Lote de Serviço
        $HEADER_ARQUIVO .= str_pad($tipoRegistroHeaderArquivo, 1, $this->COMPLETE_INTEIRO); // Tipo de Registro
        $HEADER_ARQUIVO .= str_pad("", 8, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)
        $HEADER_ARQUIVO .= str_pad($this->TIPO_DA_INSCRICAO_DA_EMPRESA, 1, $this->COMPLETE_INTEIRO);  // Tipo de Inscrição da Empresa
        $HEADER_ARQUIVO .= str_pad($this->NUMERO_DA_INSCRICAO_DA_EMPRESA, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número de Inscrição da Empresa
        $HEADER_ARQUIVO .= str_pad($this->CODIGO_TRANSMISSAO, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Código do Convênio no Banco
        $HEADER_ARQUIVO .= str_pad("", 25, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)
        $HEADER_ARQUIVO .= str_pad(substr($this->NOME_DA_EMPRESA, 0, 30), 30, $this->COMPLETE_BRANCO, STR_PAD_RIGHT); // Nome da Empresa Lmt 30
        $HEADER_ARQUIVO .= str_pad(substr($this->NOME_BANCO, 0, 30), 30, $this->COMPLETE_BRANCO, STR_PAD_RIGHT); // Nome do Banco  Lmt 30
        $HEADER_ARQUIVO .= str_pad("", 10, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)
        $HEADER_ARQUIVO .= str_pad($codigoRemessa, 1, $this->COMPLETE_INTEIRO); // Código Remessa
        $HEADER_ARQUIVO .= str_pad($this->dados_lancamento['data_geracao_arquivo'], 8, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Data de Geração do Arquivo
        $HEADER_ARQUIVO .= str_pad("", 6, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)
        $HEADER_ARQUIVO .= str_pad($this->dados_lancamento['numero_sequencial_arquivo_cobranca'], 6, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número Seqüencial do Arquivo
        $HEADER_ARQUIVO .= str_pad("040", 3, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número da Versão do Layout do Arquivo
        $HEADER_ARQUIVO .= str_pad("", 74, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)

        return $HEADER_ARQUIVO;
    }

    private function getHeaderLoteRemessa() {
        $this->dados_lancamento['quantidade_lotes_arquivo'] ++;
        $this->dados_lancamento['numero_remessa_retorno'] ++;
        $this->dados_lancamento['quantidade_de_lotes_do_arquivo'] ++;

        // Variáveis para geração do HEADER_LOTE
        $tipoRegistroLoteArquivo = "1";
        // C = Lançamento a Crédito / D = Lançamento a Débito / E = Extrato para Conciliação
        // G = Extrato para Gestão de Caixa / I = Informações de Títulos Capturados do Próprio Banco / T = Arquivo de Retorno
        // R = Arquivo Remessa
        $tipoDeOperacao = "R";
        $tipoDoServico = "01";

        // HEADER DO LOTE REMESSA
        $HEADER_LOTE = "\r\n" . $this->CODIGO_BANCO_COMPENSACAO; // Código do Banco na Compensação
        $HEADER_LOTE .= str_pad($this->dados_lancamento['quantidade_lotes_arquivo'], 4, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Lote de Serviço
        $HEADER_LOTE .= str_pad($tipoRegistroLoteArquivo, 1, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); //
        $HEADER_LOTE .= str_pad($tipoDeOperacao, 1, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Tipo de Operação
        $HEADER_LOTE .= str_pad($tipoDoServico, 2, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Tipo de Serviço
        $HEADER_LOTE .= str_pad("", 2, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)
        $HEADER_LOTE .= str_pad($this->VERSAO_LAYOUT_LOTE, 3, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Nº da Versão do Layout do Lote
        $HEADER_LOTE .= str_pad("", 1, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)
        $HEADER_LOTE .= str_pad($this->TIPO_DA_INSCRICAO_DA_EMPRESA, 1, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Tipo de Inscrição da Empresa
        $HEADER_LOTE .= str_pad($this->NUMERO_DA_INSCRICAO_DA_EMPRESA, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Nº de Inscrição da Empresa
        $HEADER_LOTE .= str_pad("", 20, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)
        $HEADER_LOTE .= str_pad($this->CODIGO_TRANSMISSAO, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Código do Convênio no Banco
        $HEADER_LOTE .= str_pad("", 5, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)
        $HEADER_LOTE .= str_pad($this->NOME_DA_EMPRESA, 30, $this->COMPLETE_BRANCO); // Nome da Empresa Lmt 30
        $HEADER_LOTE .= str_pad($this->MENSAGEM_1, 40, $this->COMPLETE_BRANCO); // Mensagem 1
        $HEADER_LOTE .= str_pad($this->MENSAGEM_2, 40, $this->COMPLETE_BRANCO); // Mensagem 2
        $HEADER_LOTE .= str_pad($this->dados_lancamento['numero_remessa_retorno'], 8, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número Sequencial Remessa/Retorno
        $HEADER_LOTE .= str_pad($this->dados_lancamento['data_geracao_arquivo'], 8, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número Sequencial Remessa/Retorno
        $HEADER_LOTE .= str_pad("", 41, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Reservado (uso Banco)

        return $HEADER_LOTE;
    }

    // SEGMENTOS REMESSA
    private function getSegmentoP($boleto) {
        $this->dados_lancamento['quantidade_de_registros_do_arquivo'] ++;

        // Variáveis para geração do SEGMENTO_P
        $tipoRegistroSegmentoP = "3";
        $letraSegmentoP = "P";
        $codigoMovimentoRemessa = "1";
        $tipoDoDocumento = "1";
        $especieDoTitulo = "04";
        $identificacaoDeTituloAceitoNaoAceito = "N"; // A = Aceite / N = Não Aceite
        $codigoDoJurosDeMora = "2";
        $jurosDeMoraPorDiaTaxa = "100000";
        $codigoDoDesconto = "0";
        $dataDoDesconto = "";
        $valorPercentualASerConcedido = "0";
        $valorDoIOFaSerRecolhido = "0";
        $valorDoAbatimento = "0";
        $identificacaoDoTituloNaEmpresa = ""; // podemos ver depois o codigo da empresa mais o id do boleto
        //$codigoParaProtesto = "0";
        $codigoParaProtesto = "3"; // thiago e euclydes mudar para o tipo do contrato da empresa
        $numeroDeDiasParaProtesto = "0";
        //$codigoParaBaixaDevolucao = "1";
        $codigoParaBaixaDevolucao = "3"; // thiago e euclydes mudar para o tipo do contrato da empresa
        $numeroDeDiasParaBaixaDevolucao = "1";
        $codigoDaMoeda = "00";
        $tipoCobranca = "5";
        $formaCadastramento = "1";


        $nossoNumero = $boleto["idboleto"] . $this->Modulo_11($boleto["idboleto"]);
        //$numeroDocumentoCobranca = $boleto["idboleto"]; // boleto id  ou import_id do boleto
        //$numeroDocumentoCobranca = $this->cobrancas['lancamento']; // para identificar no arquivo de retorno a qual lançamento referente o arquivo gerado
        $numeroDocumentoCobranca = $boleto["idboleto"];
        $dataVencimentoBoleto = new \DateTime($boleto["vencimento"]);
        $dataVencimento = $dataVencimentoBoleto->format('dmY'); // data de vencimento do boleto  DDMMAAAA
        $valorNominalDoTitulo = str_replace(".", "", number_format($boleto["valor"], 2, '.', '')); // valor do boleto
        $dataEmissaoDoTitulo = $this->dados_lancamento['data_geracao_arquivo']; // Data de Emissão DDMMAAAA
        $dataDoJurosDeMora = $dataVencimento; // data de vencimento do boleto  DDMMAAAA
        $dataDoDesconto = "0";
        $valorPercentualASerConcedido = "0";
        $valorDoIOFaSerRecolhido = "0";
        $valorDoAbatimento = "0";

        $codigoDaMulta = "2";
        $dataDaMulta = $dataVencimento;
        $valorPercentualASerAplicadoMulta = "200";


        // SEGMENTO P REMESSA
        $SEGMENTO_P = "\r\n" . $this->CODIGO_BANCO_COMPENSACAO; // Código do Banco na Compensação
        $SEGMENTO_P .= str_pad($this->dados_lancamento['quantidade_lotes_arquivo'], 4, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Lote de Serviço
        $SEGMENTO_P .= str_pad($tipoRegistroSegmentoP, 1, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Tipo de Registro
        $SEGMENTO_P .= str_pad($this->dados_lancamento['quantidade_de_registros_do_arquivo'], 5, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Nº Sequencial do Registro no Lote
        $SEGMENTO_P .= str_pad($letraSegmentoP, 1, $this->COMPLETE_BRANCO); // Cód. Segmento do Registro Detalhe
        $SEGMENTO_P .= str_pad("", 1, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_P .= str_pad($codigoMovimentoRemessa, 2, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Código de Movimento Remessa
        $SEGMENTO_P .= str_pad($this->AGENCIA_DESTINATARIA_CONTA, 4, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Agência do Destinatária FIDC
        $SEGMENTO_P .= str_pad($this->DIGITO_VERIFICADOR_AGENCIA_DESTINATARIA_CONTA, 1, $this->COMPLETE_INTEIRO); // Dígito da Ag do Destinatária FIDC
        $SEGMENTO_P .= str_pad($this->NUMERO_DA_CONTA_CORRENTE, 9, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número da Conta Corrente
        $SEGMENTO_P .= str_pad($this->DIGITO_VERIFICADOR_CONTA_CORRENTE, 1, $this->COMPLETE_INTEIRO); // Dígito Verificador da Conta
        $SEGMENTO_P .= str_pad($this->NUMERO_DA_CONTA_CORRENTE, 9, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Conta cobrança Destinatária FIDC  $NUMERO_DA_CONTA_COBRANCA_DESTINATARIA
        $SEGMENTO_P .= str_pad($this->DIGITO_VERIFICADOR_CONTA_CORRENTE, 1, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Dígito da conta cobrança Destinatária FIDC  $DIGITO_VERIFICADOR_CONTA_COBRANCA_DESTINATARIA
        $SEGMENTO_P .= str_pad("", 2, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_P .= str_pad($nossoNumero, 13, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Identificação do título no Banco - Nosso Número
        $SEGMENTO_P .= str_pad($tipoCobranca, 1, $this->COMPLETE_INTEIRO); // Tipo de cobrança
        $SEGMENTO_P .= str_pad($formaCadastramento, 1, $this->COMPLETE_INTEIRO); // Forma de Cadastramento
        $SEGMENTO_P .= str_pad($tipoDoDocumento, 1, $this->COMPLETE_INTEIRO); // Tipo de Documento
        $SEGMENTO_P .= str_pad("", 1, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_P .= str_pad("", 1, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_P .= str_pad($numeroDocumentoCobranca, 15, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Número do Documento de Cobrança - Seu número
        $SEGMENTO_P .= str_pad($dataVencimento, 8, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Data de Vencimento do Título
        $SEGMENTO_P .= str_pad($valorNominalDoTitulo, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Valor Nominal do Título - Decimais sem separador
        $SEGMENTO_P .= str_pad("", 4, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Agência Encarregada da Cobrança  $AGENCIA_MANTEDORA_CONTA
        $SEGMENTO_P .= str_pad("", 1, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Dígito Verificador da Agência   $DIGITO_VERIFICADOR_AGENCIA_MANTEDORA_CONTA
        $SEGMENTO_P .= str_pad("", 1, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_P .= str_pad($especieDoTitulo, 2, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Espécie do Título
        $SEGMENTO_P .= str_pad($identificacaoDeTituloAceitoNaoAceito, 1, $this->COMPLETE_BRANCO); // Identific. de Título Aceito/Não Aceito
        $SEGMENTO_P .= str_pad($dataEmissaoDoTitulo, 8, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Data da Emissão do Título
        $SEGMENTO_P .= str_pad($codigoDoJurosDeMora, 1, $this->COMPLETE_INTEIRO); // Código do Juros de Mora
        $SEGMENTO_P .= str_pad($dataDoJurosDeMora, 8, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Data do Juros de Mora
        $SEGMENTO_P .= str_pad($jurosDeMoraPorDiaTaxa, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Juros de Mora por Dia/Taxa
        $SEGMENTO_P .= str_pad($codigoDoDesconto, 1, $this->COMPLETE_INTEIRO); // Código do Desconto 1
        $SEGMENTO_P .= str_pad($dataDoDesconto, 8, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Data do Desconto 1
        $SEGMENTO_P .= str_pad($valorPercentualASerConcedido, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Valor/Percentual a ser Concedido
        $SEGMENTO_P .= str_pad($valorDoIOFaSerRecolhido, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Valor do IOF a ser Recolhido
        $SEGMENTO_P .= str_pad($valorDoAbatimento, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Valor do Abatimento
        $SEGMENTO_P .= str_pad($identificacaoDoTituloNaEmpresa, 25, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Identificação do Título na Empresa
        $SEGMENTO_P .= str_pad($codigoParaProtesto, 1, $this->COMPLETE_INTEIRO); // Código para Protesto
        $SEGMENTO_P .= str_pad($numeroDeDiasParaProtesto, 2, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número de Dias para Protesto
        $SEGMENTO_P .= str_pad($codigoParaBaixaDevolucao, 1, $this->COMPLETE_INTEIRO); // Código para Baixa/Devolução
        $SEGMENTO_P .= str_pad("", 1, $this->COMPLETE_INTEIRO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_P .= str_pad($numeroDeDiasParaBaixaDevolucao, 2, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número de Dias para Baixa/Devolução
        $SEGMENTO_P .= str_pad($codigoDaMoeda, 2, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Código da Moeda
        $SEGMENTO_P .= str_pad("", 11, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Uso Exclusivo FEBRABAN/CNAB

        return $SEGMENTO_P;
    }

    public function getSegmentoQ($boleto) {
        // pega os endereços de cobrança do cliente
        $sqlEnderecoCliente = "SELECT ec.rua as endereco, ec.bairro, ec.cep, c.municipio, c.uf
    FROM endereco_cliente ec, cidade c where c.idcidade = ec.cidade_id and ec.cliente_id = " . $boleto['cliente_id'] . " order by ec.cliente_id desc limit 1";

        $con = \Propel\Runtime\Propel::getConnection();
        $stmtEnderecoCliente = $con->prepare($sqlEnderecoCliente);
        $stmtEnderecoCliente->execute();
        $enderecoCliente = $stmtEnderecoCliente->fetchall();
        if (sizeof($enderecoCliente)) {
            $enderecoCliente = $enderecoCliente[0];
        } else {
            $enderecoCliente = array();
            $enderecoCliente['endereco'] = "";
            $enderecoCliente['bairro'] = "";
            $enderecoCliente['cep'] = "";
            $enderecoCliente['municipio'] = "";
            $enderecoCliente['uf'] = "";
        }

        // dados do cliente
        $this->dados_lancamento['quantidade_de_registros_do_arquivo'] ++;

        if ($boleto["tipo_pessoa"] == 'fisica') {
            $tipoDeInscricaoPagador = 1;
        } elseif ($boleto["tipo_pessoa"] == 'juridica') {
            $tipoDeInscricaoPagador = 2;
        } else {
            $tipoDeInscricaoPagador = 9; // Outros
        }

        $numeroDeInscricaoPagador = substr($boleto["cpf_cnpj"], 0, 15); // cnpj ou cpf do cliente da fatura gerada
        $nome = substr($this->remove_accents($boleto["nome"]), 0, 40);
        $endereco = substr($this->remove_accents($enderecoCliente["endereco"]), 0, 40);
        $bairro = substr($this->remove_accents($enderecoCliente["bairro"]), 0, 15);
        $cep = str_replace("-", "", str_replace(".", "", $enderecoCliente["cep"]));
        $cidade = substr($this->remove_accents($enderecoCliente["municipio"]), 0, 15);
        $uf = substr($this->remove_accents($enderecoCliente["uf"]), 0, 2);

        // Variáveis para geração do SEGMENTO_Q
        $tipoRegistroSegmentoQ = "3";
        $letraSegmentoQ = "Q";
        $codigoMovimentoRemessaSegmentoQ = "1";


        // SEGMENTO Q REMESSA
        $SEGMENTO_Q = "\r\n" . $this->CODIGO_BANCO_COMPENSACAO; // Código do Banco na Compensação
        $SEGMENTO_Q .= str_pad($this->dados_lancamento['quantidade_lotes_arquivo'], 4, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Lote de Serviço
        $SEGMENTO_Q .= str_pad($tipoRegistroSegmentoQ, 1, $this->COMPLETE_INTEIRO); // Tipo de Registro
        $SEGMENTO_Q .= str_pad($this->dados_lancamento['quantidade_de_registros_do_arquivo'], 5, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Nº Sequencial do Registro no Lote
        $SEGMENTO_Q .= str_pad($letraSegmentoQ, 1, $this->COMPLETE_BRANCO); // Cód. Segmento do Registro Detalhe
        $SEGMENTO_Q .= str_pad("", 1, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_Q .= str_pad($codigoMovimentoRemessaSegmentoQ, 2, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Código de Movimento Remessa
        $SEGMENTO_Q .= str_pad($tipoDeInscricaoPagador, 1, $this->COMPLETE_INTEIRO); // Tipo de Inscrição
        $SEGMENTO_Q .= str_pad($numeroDeInscricaoPagador, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número de Inscrição
        $SEGMENTO_Q .= str_pad($nome, 40, $this->COMPLETE_BRANCO); // Nome
        $SEGMENTO_Q .= str_pad($endereco, 40, $this->COMPLETE_BRANCO); // Endereço
        $SEGMENTO_Q .= str_pad($bairro, 15, $this->COMPLETE_BRANCO); // Bairro
        $SEGMENTO_Q .= str_pad($cep, 8, $this->COMPLETE_INTEIRO); // CEP & Sufixo do CEP
        $SEGMENTO_Q .= str_pad($cidade, 15, $this->COMPLETE_BRANCO); // Cidade
        $SEGMENTO_Q .= str_pad($uf, 2, $this->COMPLETE_BRANCO); // Unidade da Federação

        if ($boleto["tipo_pessoa"] == 'fisica') {
            $SEGMENTO_Q .= str_pad($tipoDeInscricaoPagador, 1, $this->COMPLETE_INTEIRO); // Tipo de Inscrição
            $SEGMENTO_Q .= str_pad($numeroDeInscricaoPagador, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número de Inscrição
            $SEGMENTO_Q .= str_pad($nome, 40, $this->COMPLETE_BRANCO); // Nome do Pagadorr/Avalista
        } else {
            $SEGMENTO_Q .= str_pad("0", 1, $this->COMPLETE_INTEIRO); // Tipo de Inscrição
            $SEGMENTO_Q .= str_pad("", 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Número de Inscrição
            $SEGMENTO_Q .= str_pad("", 40, $this->COMPLETE_BRANCO); // Nome do Pagadorr/Avalista
        }

        $SEGMENTO_Q .= str_pad("", 3, $this->COMPLETE_INTEIRO); // Identificador de carne
        $SEGMENTO_Q .= str_pad("", 3, $this->COMPLETE_INTEIRO); // Seqüencial da Parcela ou número inicial da parcela
        $SEGMENTO_Q .= str_pad("", 3, $this->COMPLETE_INTEIRO); // Quantidade total de parcelas
        $SEGMENTO_Q .= str_pad("", 3, $this->COMPLETE_INTEIRO); // Número do plano
        $SEGMENTO_Q .= str_pad("", 19, $this->COMPLETE_BRANCO); // Reservado (uso Banco)

        return $SEGMENTO_Q;
    }

    public function getSegmentoR($boleto) {
        // Variáveis para geração do SEGMENTO_R
        $tipoRegistroSegmentoR = "3";
        $letraSegmentoR = "R";
        $codigoMovimentoRemessaSegmentoR = "1";
        $codigoDoDesconto = "0";
        $dataDoDesconto = "0";
        $valorPercentualASerConcedido = "0";
        $codigoDaMulta = "2";

        $dataVencimentoBoleto = new \DateTime($boleto["vencimento"]);
        $dataVencimento = $dataVencimentoBoleto->format('dmY'); // data de vencimento do boleto  DDMMAAAA
        $dataDaMulta = $dataVencimento;
        $valorPercentualASerAplicadoMulta = "200";

        $this->dados_lancamento['quantidade_de_registros_do_arquivo'] ++;


        // SEGMENTO R REMESSA
        $SEGMENTO_R = "\r\n" . $this->CODIGO_BANCO_COMPENSACAO; // Código do Banco na Compensação
        $SEGMENTO_R .= str_pad($this->dados_lancamento['quantidade_lotes_arquivo'], 4, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Lote de Serviço
        $SEGMENTO_R .= str_pad($tipoRegistroSegmentoR, 1, $this->COMPLETE_INTEIRO); // Tipo de Registro
        $SEGMENTO_R .= str_pad($this->dados_lancamento['quantidade_de_registros_do_arquivo'], 5, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Nº Sequencial do Registro no Lote
        $SEGMENTO_R .= str_pad($letraSegmentoR, 1, $this->COMPLETE_BRANCO); // Cód. Segmento do Registro Detalhe
        $SEGMENTO_R .= str_pad("", 1, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_R .= str_pad($codigoMovimentoRemessaSegmentoR, 2, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Código de Movimento Remessa
        $SEGMENTO_R .= str_pad($codigoDoDesconto, 1, $this->COMPLETE_INTEIRO); // Código do Desconto 2
        $SEGMENTO_R .= str_pad($dataDoDesconto, 8, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Data do Desconto 2
        $SEGMENTO_R .= str_pad($valorPercentualASerConcedido, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Valor/Percentual a ser Concedido
        $SEGMENTO_R .= str_pad("", 24, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_R .= str_pad($codigoDaMulta, 1, $this->COMPLETE_INTEIRO); // Código do Desconto 1
        $SEGMENTO_R .= str_pad($dataDaMulta, 8, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Data do Desconto 1
        $SEGMENTO_R .= str_pad($valorPercentualASerAplicadoMulta, 15, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Valor/Percentual a ser Concedido
        $SEGMENTO_R .= str_pad("", 10, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $SEGMENTO_R .= str_pad("", 40, $this->COMPLETE_BRANCO); // Mensagem 3
        $SEGMENTO_R .= str_pad("", 40, $this->COMPLETE_BRANCO); // Mensagem 4
        $SEGMENTO_R .= str_pad("", 61, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB

        return $SEGMENTO_R;
    }

    // TRAILERS DE REMESSA
    private function getTrailerLoteRemessa() {
        $this->dados_lancamento['quantidade_de_registros_do_arquivo'] += 2; // Tipo 1, 2, 3, 4 e 5   / Neste caso Tipo 1 e 5, logo == 2
        // Variáveis para geração do TRAILER_LOTE
        $tipoRegistroTrailerLote = "5";


        // TRAILER de LOTE REMESSA
        $TRAILER_LOTE = "\r\n" . $this->CODIGO_BANCO_COMPENSACAO; // Código do Banco na Compensação
        $TRAILER_LOTE .= str_pad($this->dados_lancamento['quantidade_lotes_arquivo'], 4, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Lote de Serviço
        $TRAILER_LOTE .= str_pad($tipoRegistroTrailerLote, 1, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Tipo de Registro
        $TRAILER_LOTE .= str_pad("", 9, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        $TRAILER_LOTE .= str_pad($this->dados_lancamento['quantidade_de_registros_do_arquivo'], 6, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Quantidade de Registros no Lote
        $TRAILER_LOTE .= str_pad("", 217, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Uso Exclusivo FEBRABAN/CNAB

        return $TRAILER_LOTE;
    }

    private function getTrailerArquivoRemessa() {
        $this->dados_lancamento['quantidade_de_registros_do_arquivo'] += 2; // Tipo 1, 2, 3, 4 e 5   / Neste caso Tipo 1 e 5, logo == 2
        // Variáveis para geração do TRAILER_ARQUIVO
        $tipoRegistroTrailerArquivo = "9";


        // TRAILER DE ARQUIVO REMESSA
        $TRAILER_ARQUIVO = "\r\n" . $this->CODIGO_BANCO_COMPENSACAO; // Código do Banco na Compensação
        $TRAILER_ARQUIVO .= "9999"; // Lote de Serviço
        $TRAILER_ARQUIVO .= str_pad($tipoRegistroTrailerArquivo, 1, $this->COMPLETE_INTEIRO); // Tipo de Registro
        $TRAILER_ARQUIVO .= str_pad("", 9, $this->COMPLETE_BRANCO, STR_PAD_LEFT); // Uso Exclusivo FEBRABAN/CNAB
        $TRAILER_ARQUIVO .= str_pad($this->dados_lancamento['quantidade_de_lotes_do_arquivo'], 6, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Quantidade de Lotes do Arquivo
        $TRAILER_ARQUIVO .= str_pad($this->dados_lancamento['quantidade_de_registros_do_arquivo'], 6, $this->COMPLETE_INTEIRO, STR_PAD_LEFT); // Quantidade de Registros do Arquivo
        $TRAILER_ARQUIVO .= str_pad("", 211, $this->COMPLETE_BRANCO); // Uso Exclusivo FEBRABAN/CNAB
        //$textoGerado = $HEADER_ARQUIVO . $HEADER_LOTE . $SEGMENTO_P . $SEGMENTO_Q . $TRAILER_LOTE . $TRAILER_ARQUIVO;
        return $TRAILER_ARQUIVO;
    }

    // ********************************** ARQUIVO DE RETORNO ********************************** \\

    public function obterArquivoRetorno($arquivoRetorno) {
        $boletos = array();

        $totalLinhas = 0;
        $handleTotalLinhas = fopen($arquivoRetorno['tmp_name'], "r");
        if ($handleTotalLinhas) {
            while ((fgets($handleTotalLinhas)) !== false) {
                $totalLinhas++;
            }

            fclose($handleTotalLinhas);
        }

        $handle = fopen($arquivoRetorno['tmp_name'], "r");
        if ($handle) {
            //$ultimoNumeroDocumentoCobranca = 0;
            $ultimoNossoNumero = 0;
            $linha = 1;
            while (($line = fgets($handle)) !== false) {
                // process the line read.
                switch ($linha) {
                    case 1: // HEADER DO ARQUIVO
                        $dados_retorno = $this->getHeaderArquivoRetorno($line);
                        //$boletos['numeroSequencialArquivoCobranca'] = $dados_retorno['numeroSequencialArquivo'];
                        break;

                    case 2: // HEADER DO LOTE
                        $dados_retorno = $this->getHeaderLoteRetorno($line);
                        break;

                    case ($totalLinhas - 1): // TRAILER DE LOTE
                        $dados_retorno = $this->getTrailerLoteRetorno($line);
                        break;

                    case $totalLinhas: // TRAILER DE ARQUIVO
                        $dados_retorno = $this->getTrailerArquivoRetorno($line);
                        break;

                    default: // SEGMENTOS
                        $dados_retorno = array();
                        if ($linha % 2 == 1) {
                            $dados_retorno = $this->getSegmentoT($line);
                            //$ultimoNumeroDocumentoCobranca = $dados_retorno['numeroDocumentoCobranca'];
                            //$ultimoNossoNumero = $dados_retorno['identificacaoTituloBanco'];
                            $ultimoNossoNumero = substr($dados_retorno['identificacaoTituloBanco'], 0, 12);
                            $boletos[$ultimoNossoNumero]['T'] = $dados_retorno;

                            $mensagens = array();
                            $codigosMensagem = str_split($dados_retorno['identificaoRejeicaoTarifaCustasLiquidacaoBaixa'], $split_length = 2);
                            foreach ($codigosMensagem as $codigoMensagem) {
                                $mensagem = $this->getMensagemRetorno($dados_retorno['codigoMovimento'], $codigoMensagem);
                                if (!empty($mensagem)) {
                                    $mensagens[] = $mensagem;
                                }
                            }

                            if (in_array($dados_retorno['codigoMovimento'], array('03', '26', '30'))) {
                                $boletos[$ultimoNossoNumero]['status'] = 'recusado';
                            } elseif (in_array($dados_retorno['codigoMovimento'], array('06', '09', '17'))) {
                                $boletos[$ultimoNossoNumero]['status'] = 'baixado';
                            } else {
                                $boletos[$ultimoNossoNumero]['status'] = 'registrado';
                            }

                            $boletos[$ultimoNossoNumero]['descricaoMovimento'] = $this->getDescricaoMovimento($dados_retorno['codigoMovimento']);
                            $boletos[$ultimoNossoNumero]['mensagens'] = $mensagens;
                        } else {
                            $dados_retorno = $this->getSegmentoU($line);
                            $boletos[$ultimoNossoNumero]['U'] = $dados_retorno;
                        }

                        break;
                }

                $linha++;
            }

            fclose($handle);
        } else {
            // error opening the file.
        }

        //print_r($boletos);
        return $boletos;
    }

    // HEADERS DO ARQUIVO DE RETORNO
    private function getHeaderArquivoRetorno($linha) {
        $retorno = array();
        $retorno['codigoBancoCompensacao'] = substr($linha, 0, 3);
        $retorno['loteServico'] = substr($linha, 3, 4);
        $retorno['tipoRegistro'] = substr($linha, 7, 1);
        $retorno['tipoInscricaoEmpresa'] = substr($linha, 16, 1);
        $retorno['numeroInscricaoEmpresa'] = substr($linha, 17, 15);
        $retorno['agenciaBeneficiario'] = substr($linha, 32, 4);
        $retorno['agenciaBeneficiarioDigito'] = substr($linha, 36, 1);
        $retorno['numeroContaCorrente'] = substr($linha, 37, 9);
        $retorno['numeroContaCorrenteDigito'] = substr($linha, 46, 1);
        $retorno['codigoBeneficiario'] = substr($linha, 52, 9);
        $retorno['nomeEmpresa'] = substr($linha, 72, 30);
        $retorno['nomeBanco'] = substr($linha, 102, 30);
        $retorno['codigoRetorno'] = substr($linha, 142, 1);
        $retorno['dataGeracaoArquivo'] = substr($linha, 143, 8);
        $retorno['numeroSequencialArquivo'] = substr($linha, 157, 6);
        $retorno['versaoLayoutArquivo'] = substr($linha, 163, 3);

        return $retorno;
    }

    private function getHeaderLoteRetorno($linha) {
        $retorno = array();
        $retorno['codigoBancoCompensacao'] = substr($linha, 0, 3);
        $retorno['numeroLoteRetorno'] = substr($linha, 3, 4);
        $retorno['tipoRegistro'] = substr($linha, 7, 1);
        $retorno['tipoOperacao'] = substr($linha, 8, 1);
        $retorno['tipoServico'] = substr($linha, 9, 2);
        $retorno['versaoLayoutLote'] = substr($linha, 13, 3);
        $retorno['tipoInscricaoEmpresa'] = substr($linha, 17, 1);
        $retorno['numeroInscricaoEmpresa'] = substr($linha, 18, 15);
        $retorno['codigoBeneficiario'] = substr($linha, 33, 9);
        $retorno['agenciaBeneficiario'] = substr($linha, 53, 4);
        $retorno['agenciaBeneficiarioDigito'] = substr($linha, 57, 1);
        $retorno['numeroContaBeneficiario'] = substr($linha, 58, 9);
        $retorno['numeroContaBeneficiarioDigito'] = substr($linha, 67, 1);
        $retorno['nomeEmpresa'] = substr($linha, 73, 30);
        $retorno['numeroRetorno'] = substr($linha, 183, 8);
        $retorno['dataGeracaoArquivo'] = substr($linha, 191, 8);

        return $retorno;
    }

    // TRAILERS DO ARQUIVO DE RETORNO
    private function getTrailerLoteRetorno($linha) {
        $retorno = array();
        $retorno['codigoBancoCompensacao'] = substr($linha, 0, 3);
        $retorno['loteServico'] = substr($linha, 3, 4);
        $retorno['tipoRegistro'] = substr($linha, 7, 1);
        $retorno['quantidadeRegistrosLotes'] = substr($linha, 17, 6);
        $retorno['quantidadeTitulosCobrancaSimples'] = substr($linha, 23, 6);
        $retorno['valorTotalTitulosCobrancaSimples'] = substr($linha, 29, 17);
        $retorno['quantidadeTitulosCobrancaVinculada'] = substr($linha, 46, 6);
        $retorno['valorTotalTitulosCobrancaVinculada'] = substr($linha, 52, 17);
        $retorno['quantidadeTitulosCobrancaCaucionada'] = substr($linha, 69, 6);
        $retorno['valorTotalTitulosCobrancaCaucionada'] = substr($linha, 75, 17);
        $retorno['quantidadeTitulosCobrancaDescontada'] = substr($linha, 92, 6);
        $retorno['valorTotalTitulosCobrancaDescontada'] = substr($linha, 98, 17);
        $retorno['numeroAvisoLancamento'] = substr($linha, 115, 8);

        return $retorno;
    }

    private function getTrailerArquivoRetorno($linha) {
        $retorno = array();
        $retorno['codigoBancoCompensacao'] = substr($linha, 0, 3);
        $retorno['numeroLoteRemessa'] = substr($linha, 3, 4);
        $retorno['tipoRegistro'] = substr($linha, 7, 1);
        $retorno['quantidadeLotesArquivo'] = substr($linha, 17, 6);
        $retorno['quantidadeRegistrosArquivo'] = substr($linha, 23, 6);

        return $retorno;
    }

    // SEGMENTOS DO ARQUIVO DE RETORNO
    private function getSegmentoT($linha) {
        $retorno = array();
        $retorno['codigoBancoCompensacao'] = substr($linha, 0, 3);
        $retorno['numeroLoteRetorno'] = substr($linha, 3, 4);
        $retorno['tipoRegistro'] = substr($linha, 7, 1);
        $retorno['numeroSequencialRegistroLote'] = substr($linha, 8, 5);
        $retorno['codigoSegmentoRegistroDetalhe'] = substr($linha, 13, 1);
        $retorno['codigoMovimento'] = substr($linha, 15, 2);
        $retorno['agenciaBeneficiario'] = substr($linha, 17, 4);
        $retorno['agenciaBeneficiarioDigito'] = substr($linha, 21, 1);
        $retorno['numeroContaCorrente'] = substr($linha, 22, 9);
        $retorno['numeroContaCorrenteDigito'] = substr($linha, 31, 1);
        $retorno['identificacaoTituloBanco'] = substr($linha, 40, 13);
        $retorno['codigoCarteira'] = substr($linha, 53, 1);
        $retorno['numeroDocumentoCobranca'] = substr($linha, 54, 15);
        $retorno['dataVencimentoTitulo'] = substr($linha, 69, 8);
        $retorno['valorNominalTitulo'] = substr($linha, 77, 15);
        $retorno['numeroBancoCobradorRecebedor'] = substr($linha, 92, 3);
        $retorno['agenciaCobradoraRecebedora'] = substr($linha, 95, 4);
        $retorno['digitoAgenciaBeneficiario'] = substr($linha, 99, 1);
        $retorno['identificacaoTituloEmpresa'] = substr($linha, 100, 25);
        $retorno['codigoMoeda'] = substr($linha, 125, 2);
        $retorno['tipoInscricaoPagador'] = substr($linha, 127, 1);
        $retorno['numeroInscricaoPagador'] = substr($linha, 128, 15);
        $retorno['nomePagador'] = substr($linha, 143, 40);
        $retorno['contaCobranca'] = substr($linha, 183, 10);
        $retorno['valorTarifaCustos'] = substr($linha, 193, 15);
        $retorno['identificaoRejeicaoTarifaCustasLiquidacaoBaixa'] = substr($linha, 208, 10);

        return $retorno;
    }

    private function getSegmentoU($linha) {
        $retorno = array();
        $retorno['codigoBancoCompensacao'] = substr($linha, 0, 3);
        $retorno['loteServico'] = substr($linha, 3, 4);
        $retorno['tipoRegistro'] = substr($linha, 7, 1);
        $retorno['numeroSequencialRegistroLote'] = substr($linha, 8, 5);
        $retorno['codigoSegmentoRegistroDetalhe'] = substr($linha, 13, 1);
        $retorno['codigoMovimento'] = substr($linha, 15, 2);
        $retorno['jurosMultaEncargos'] = substr($linha, 17, 15);
        $retorno['valorDescontoConcedido'] = substr($linha, 32, 15);
        $retorno['valorAbatimentoConcedido'] = substr($linha, 47, 15);
        $retorno['valorIORecolhido'] = substr($linha, 62, 15);
        $retorno['valorPagoPagador'] = substr($linha, 77, 15);
        $retorno['valorLiquidoCreditado'] = substr($linha, 92, 15);
        $retorno['valorOutrasDespesas'] = substr($linha, 107, 15);
        $retorno['valorOutrosCreditos'] = substr($linha, 122, 15);
        $retorno['dataOcorrencia'] = substr($linha, 137, 8);
        $retorno['dataEfetivacaoCredito'] = substr($linha, 145, 8);
        $retorno['codigoOcorrenciaPagador'] = substr($linha, 153, 4);
        $retorno['dataOcorrenciaPagador'] = substr($linha, 157, 8);
        $retorno['valorOcorrenciaPagador'] = substr($linha, 165, 15);
        $retorno['complementoOcorrenciaPagador'] = substr($linha, 180, 30);
        $retorno['codigoBancoCorrespondente'] = substr($linha, 210, 3);

        return $retorno;
    }

    // MENSAGENS DO ARQUIVO DE RETORNO
    private function getDescricaoMovimento($codigoMovimento) {
        $mensagem = array();
        $mensagem['02'] = "Movimento 02: Entrada confirmada";
        $mensagem['03'] = "Movimento 03: Entrada rejeitada";
        $mensagem['04'] = "Movimento 04: Transferência de carteira/entrada";
        $mensagem['05'] = "Movimento 05: Transferência de carteira/baixa";
        $mensagem['06'] = "Movimento 06: Liquidação";
        $mensagem['09'] = "Movimento 09: Baixa";
        $mensagem['11'] = "Movimento 11: Títulos em carteira ( em ser)";
        $mensagem['12'] = "Movimento 12: Confirmação recebimento instrução de abatimento";
        $mensagem['13'] = "Movimento 13: Confirmação recebimento instrução de cancelamento abatimento";
        $mensagem['14'] = "Movimento 14: Confirmação recebimento instrução alteração de vencimento";
        $mensagem['17'] = "Movimento 17: Liquidação após baixa ou liquidação título não registrado";
        $mensagem['19'] = "Movimento 19: Confirmação recebimento instrução de protesto";
        $mensagem['20'] = "Movimento 20: Confirmação recebimento instrução de sustação/Não Protestar";
        $mensagem['23'] = "Movimento 23: Remessa a cartorio ( aponte em cartorio)";
        $mensagem['24'] = "Movimento 24: Retirada de cartorio e manutenção em carteira";
        $mensagem['25'] = "Movimento 25: Protestado e baixado ( baixa por ter sido protestado)";
        $mensagem['26'] = "Movimento 26: Instrução rejeitada";
        $mensagem['27'] = "Movimento 27: Confirmação do pedido de alteração de outros dados";
        $mensagem['28'] = "Movimento 28: Debito de tarifas/custas";
        $mensagem['29'] = "Movimento 29: Ocorrências do Pagador";
        $mensagem['30'] = "Movimento 30: Alteração de dados rejeitada";
        $mensagem['32'] = "Movimento 32: Código de IOF inválido";
        $mensagem['51'] = "Movimento 51: Título DDA reconhecido pelo Pagador";
        $mensagem['52'] = "Movimento 52: Título DDA não reconhecido pelo Pagador";
        $mensagem['53'] = "Movimento 53: Título DDA recusado pela CIP";
        $mensagem['A4'] = "Movimento A4: Pagador DDA";

        if (array_key_exists("$codigoMovimento", $mensagem)) {
            return $mensagem["$codigoMovimento"];
        } else {
            return "Arquivo de Baixa Inválido. Nenhuma mensagem de Descrição de Movimento para este tipo de arquivo foi encontrada!";
        }
    }

    private function getMensagemRetorno($codigoMovimento, $codigoMensagem) {
        $mensagem = array();
        $mensagem['00'] = ""; //"O código do movimento $codigoMovimento, é inexistente para este tipo de mensagem. Aguardando arquivo de baixa.";
        // Nota 40-A: Códigos de rejeições de 01 a 64 associados ao códigos de movimento 03, 26 e 30 (nota 40)
        if (in_array($codigoMovimento, array('03', '26', '30'))) {
            $mensagem['01'] = "Erro 01: Código do banco invalido";
            $mensagem['02'] = "Erro 02: Código do registro detalhe inválido";
            $mensagem['03'] = "Erro 03: Código do segmento invalido";
            $mensagem['04'] = "Erro 04: Código do movimento não permitido para carteira";
            $mensagem['05'] = "Erro 05: Código de movimento invalido";
            $mensagem['06'] = "Erro 06: Tipo/número de inscrição do Beneficiário inválidos";
            $mensagem['07'] = "Erro 07: Agência/conta/DV invalido";
            $mensagem['08'] = "Erro 08: Nosso número invalido";
            $mensagem['09'] = "Erro 09: Nosso número duplicado";
            $mensagem['10'] = "Erro 10: Carteira invalida";
            $mensagem['11'] = "Erro 11: Forma de cadastramento do titulo invalida. Se desconto, titulo rejeitado - operação de desconto / horário limite.";
            $mensagem['12'] = "Erro 12: Tipo de documento invalido";
            $mensagem['13'] = "Erro 13: Identificação da emissão do Boleto invalida";
            $mensagem['14'] = "Erro 14: Identificação da distribuição do Boleto invalida";
            $mensagem['15'] = "Erro 15: Características da cobrança incompatíveis";
            $mensagem['16'] = "Erro 16: Data de vencimento invalida";
            $mensagem['17'] = "Erro 17: Data de vencimento anterior a data de emissão";
            $mensagem['18'] = "Erro 18: Vencimento fora do prazo de operação";
            $mensagem['19'] = "Erro 19: Título a cargo de bancos correspondentes com vencimento inferior a xx dias";
            $mensagem['20'] = "Erro 20: Valor do título invalido";
            $mensagem['21'] = "Erro 21: Espécie do titulo invalida";
            $mensagem['22'] = "Erro 22: Espécie não permitida para a carteira";
            $mensagem['23'] = "Erro 23: Aceite invalido";
            $mensagem['24'] = "Erro 24: Data de emissão inválida";
            $mensagem['25'] = "Erro 25: Data de emissão posterior a data de entrada";
            $mensagem['26'] = "Erro 26: Código de juros de mora inválido";
            $mensagem['27'] = "Erro 27: Valor/Taxa de juros de mora inválido";
            $mensagem['28'] = "Erro 28: Código de desconto inválido";
            $mensagem['29'] = "Erro 29: Valor do desconto maior ou igual ao valor do título";
            $mensagem['30'] = "Erro 30: Desconto a conceder não confere";
            $mensagem['31'] = "Erro 31: Concessão de desconto - já existe desconto anterior";
            $mensagem['32'] = "Erro 32: Valor do IOF";
            $mensagem['33'] = "Erro 33: Valor do abatimento inválido";
            $mensagem['34'] = "Erro 34: Valor do abatimento maior ou igual ao valor do título";
            $mensagem['35'] = "Erro 35: Abatimento a conceder não confere";
            $mensagem['36'] = "Erro 36: Concessão de abatimento - já existe abatimento anterior";
            $mensagem['37'] = "Erro 37: Código para protesto inválido";
            $mensagem['38'] = "Erro 38: Prazo para protesto inválido";
            $mensagem['39'] = "Erro 39: Pedido de protesto não permitido para o título";
            $mensagem['40'] = "Erro 40: Título com ordem de protesto emitida";
            $mensagem['41'] = "Erro 41: Pedido de cancelamento/sustação para títulos sem instrução de protesto";
            $mensagem['42'] = "Erro 42: Código para baixa/devolução inválido";
            $mensagem['43'] = "Erro 43: Prazo para baixa/devolução inválido";
            $mensagem['44'] = "Erro 44: Código de moeda inválido";
            $mensagem['45'] = "Erro 45: Nome do Pagador não informado";
            $mensagem['46'] = "Erro 46: Tipo /Número de inscrição do Pagador inválidos";
            $mensagem['47'] = "Erro 47: Endereço do Pagador não informado";
            $mensagem['48'] = "Erro 48: CEP inválido";
            $mensagem['49'] = "Erro 49: CEP sem praça de cobrança (não localizado)";
            $mensagem['50'] = "Erro 50: CEP referente a um Banco Correspondente";
            $mensagem['51'] = "Erro 51: CEP incompatível com a unidade de federação";
            $mensagem['52'] = "Erro 52: Unidade de federação inválida";
            $mensagem['53'] = "Erro 53: Tipo/Número de inscrição do sacador/avalista inválidos";
            $mensagem['54'] = "Erro 54: sacador/Avalista não informado";
            $mensagem['55'] = "Erro 55: Nosso número no Banco Correspondente não informado";
            $mensagem['56'] = "Erro 56: Código do Banco Correspondente não informado";
            $mensagem['57'] = "Erro 57: Código da multa inválido";
            $mensagem['58'] = "Erro 58: Data da multa inválida";
            $mensagem['59'] = "Erro 59: Valor/Percentual da multa inválido";
            $mensagem['60'] = "Erro 60: Movimento para título não cadastrado";
            $mensagem['61'] = "Erro 61: Alteração de agência cobradora/dv inválida";
            $mensagem['62'] = "Erro 62: Tipo de impressão inválido";
            $mensagem['63'] = "Erro 63: Entrada para título já cadastrado";
            $mensagem['64'] = "Erro 64: Número da linha inválido";
            $mensagem['90'] = "Erro 90: Identificador/Quantidade de Parcelas de carnê invalido";
        }

        // Nota 40 C: Código de liquidação/baixa de 01 a 13 associados ao código de movimento 06, 09 e 17 ( nota 40)
        if (in_array($codigoMovimento, array('06', '09', '17'))) {
            $mensagem['01'] = "Aprovado 01: Por saldo";
            $mensagem['02'] = "Aprovado 02: Por conta";
            $mensagem['03'] = "Aprovado 03: No próprio banco";
            $mensagem['04'] = "Aprovado 04: Compensação eletrônica";
            $mensagem['05'] = "Aprovado 05: Compensação convencional";
            $mensagem['06'] = "Aprovado 06: Arquivo magnético";
            $mensagem['07'] = "Aprovado 07: Após feriado local";
            $mensagem['08'] = "Aprovado 08: Em cartório";
            $mensagem['09'] = "Aprovado 09: Comandada banco";
            $mensagem['10'] = "Aprovado 10: Comandada cliente arquivo";
            $mensagem['11'] = "Aprovado 11: Comandada cliente on-line";
            $mensagem['12'] = "Aprovado 12: Decurso prazo – cliente";
            $mensagem['13'] = "Aprovado 13: Decurso prazo – banco";
        }

        if (array_key_exists("$codigoMensagem", $mensagem)) {
            return $mensagem["$codigoMensagem"];
        } else {
            return "Arquivo de Baixa Inválido. Nenhuma mensagem de Retorno para este tipo de arquivo foi encontrada!";
        }
    }

}

/*
  $loader = require_once __DIR__ . '/../../../../vendor/autoload.php';
  $loader->add('', __DIR__ . '/generated-classes');
  \Propel\Runtime\Propel::disableInstancePooling();

  gc_enable();

  // Create connections in Propel.
  $serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
  $serviceContainer->checkVersion('2.0.0-dev');
  // Database connection 1.
  $serviceContainer->setAdapterClass('default', 'mysql');
  $manager = new \Propel\Runtime\Connection\ConnectionManagerSingle();
  $manager->setConfiguration(array(
  'classname' => 'Propel\\Runtime\\Connection\\ConnectionWrapper',
  'dsn' => 'mysql:host=cnetdev01.censanet.com.br;dbname=ima',
  'user' => 'ima',
  'password' => 'Im4Tel3C$',
  'attributes' =>
  array(
  'ATTR_EMULATE_PREPARES' => false,
  ),
  'settings' =>
  array(
  'charset' => 'utf8',
  'queries' =>
  array(
  'utf8' => 'SET NAMES \'UTF8\'',
  ),
  )
  ));

  $manager->setName('default');
  $serviceContainer->setConnectionManager('default', $manager);
  // Set the default connection.
  $serviceContainer->setDefaultDatasource('default');

  // Set connection manangers.
  $connectionManager1 = \Propel\Runtime\Propel::getConnectionManager('default');
  $connection1 = $connectionManager1->getWriteConnection($serviceContainer->getAdapter());
  $connectionManager1->setConnection($connection1);

  use Propel\Runtime\Propel;

  // TESTES
  function getCobrancasSantander($competenciaId, $where="") {
  // produção
  $sqlBoletos = "SELECT b.*, p.*, sp.nome as nome_servico_prestado, tipo_pessoa
  FROM boleto b, servico_cliente cl, servico_prestado sp, cliente c, pessoa p, competencia cp,
  boleto_item bi
  where c.idcliente = b.cliente_id and p.id = c.pessoa_id
  and cp.id = b.competencia_id and cl.cliente_id = c.idcliente
  and sp.idservico_prestado = cl.servico_prestado_id and bi.boleto_id = b.idboleto and b.valor > 0
  and cl.idservico_cliente = bi.servico_cliente_id  $where and b.idboleto = 95152
  group by b.idboleto limit 5";

  $con = Propel::getConnection();
  $stmtBoletos = $con->prepare($sqlBoletos);
  $stmtBoletos->execute();
  $boletos = $stmtBoletos->fetchall();

  return $boletos;
  }

  $competenciaId = 48;
  $boletos = getCobrancasSantander($competenciaId, " and b.competencia_id = $competenciaId");
  //print_r($boletos);


  $sqlDadosCobranca = "select b.nome as nome_banco, b.codigo_febraban, bac.numero_conta as numero_conta_corrente,
  bac.digito_verificador as digito_verificador_conta_corrente,
  ba.agencia as banco_agencia, ba.digito_verificador as banco_agencia_digito_verificador,
  p.razao_social as nome_empresa, p.tipo_pessoa, p.cpf_cnpj

  from banco b, banco_agencia ba, banco_agencia_conta bac, convenio c, empresa e, pessoa p

  where b.idbanco = bac.banco_id and bac.banco_agencia_id = ba.idbanco_agencia
  and c.idconvenio = bac.convenio_id and e.idempresa = c.empresa_id and p.id = e.pessoa_id and b.idbanco = 20";

  $con = Propel::getConnection();

  $stmtDadosCobranca = $con->prepare($sqlDadosCobranca);
  $stmtDadosCobranca->execute();
  $dados_cobranca = $stmtDadosCobranca->fetchall();

  $boleto = new BoletoSantanderCNAB240($dados_cobranca);
  //$boleto->setCobrancas($boletos);
  //$boleto->gerarArquivoRemessa();
 */
?>
