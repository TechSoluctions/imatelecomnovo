<?php

namespace ImaTelecomBundle\Lib;

require ("phpolait/phpolait.php");

ini_set('display_errors', 0);

class SUC {

    private $sessionId = 1;
    static $instancia;

    public static function getInstancia() {
        if (is_object(self::$instancia)) {
            return self::$instancia;
        } else {
            self::$instancia = new SUC();
            return self::$instancia;
        }
    }

    private function __construct() {
        //era 07, mudei pro 02 em 18/01
        //$this->sessionId = str_rot13(file_get_contents('http://desenvolvimento01.censanet.com.br/suc_producao/index.php?view=web&sessionRequest=1'));
        $this->sessionId = str_rot13(file_get_contents('https://intranet.censanet.com.br/suc/index.php?view=web&sessionRequest=1'));
    }

    function getModule($modulename) {

        $proxy = new \JSONRpcProxy(
                //"http://desenvolvimento01.censanet.com.br/suc_producao/index.php?view=json&sessionId=" . $this->sessionId . "&modulo=$modulename"
                "https://intranet.censanet.com.br/suc/index.php?view=json&sessionId=" . $this->sessionId . "&modulo=$modulename"
        );
        return $proxy;
    }
    
    public function autenticar(){
        $login = $this->getModule("login");

        //$resp = $login->autenticar("imatelecom", "PaoComSalame5681");
        $resp = $login->autenticar("portalcnet", "lombrigamanca8891");
        
        return $resp;
    }

}

?>