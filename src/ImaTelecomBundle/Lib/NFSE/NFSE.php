<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ImaTelecomBundle\Lib\NFSE;

/**
 * Description of NFSE
 *
 * @author jrodolfo
 */
class NFSE {
    
    protected $nfse;

    public function __construct($config) {
        require_once "libs/NFSePHPGinfes.class.php";        
        
        $this->nfse = new \NFSePHPGinfes($config, 1, false);
    }
    
    public function montarLoteRps($idLote, $lotes) {
        return $this->nfse->montarLoteRps($idLote, $lotes);
    }
    
    public function enviarLoteRps($xmlLote) {
        return $this->nfse->enviarLoteRps($xmlLote);
    }
    
    public function consultarNfseRps($numrps, $tipo, $serie, $cnpj = false, $im = false) {
        return $this->nfse->consultarNfseRps($numrps, $tipo, $serie, $cnpj, $im);
    }
    
    public function consultarNfse($numNFSe = false, $cnpj = false, $im = false, $dtinicial = false, $dtfinal = false, $cpfcnpj_tomador = false, $im_tomador = false) {
        return $this->nfse->consultarNfse($numNFSe, $cnpj, $im, $dtinicial, $dtfinal, $cpfcnpj_tomador, $im_tomador);
    }

    public function gerarPDF($numNFSe, $aParser = false) {
        return $this->nfse->gerarPDF($numNFSe, $aParser);
    }

    public function getDiretorioNfse() {
        return $this->nfse->nfseDir;
    }

    public function getErro() {
        return $this->nfse->errMsg;
    }
    
    public function limparErro() {
        $this->nfse->errMsg = '';
    }
}
