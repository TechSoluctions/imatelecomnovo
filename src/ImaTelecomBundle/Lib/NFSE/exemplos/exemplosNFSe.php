<?php
/**
 * NFS-e (Nota Fiscal Eletronica de Serviço)
 * 
 * @author Bruno PorKaria <bruno at porkaria dot com dot br> 
 * @author Hugo Cegana <cegana at gmail dot com>
 */
include_once '../libs/NuSOAP/nusoap.php';
require_once "../libs/NFSePHPGinfes.class.php";

$nfse = new NFSePHPGinfes(false, 1, false);

$tarefa = $_GET['t'];
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
// CNPJ E INSCRIÇÃO MUNICIPAL DO EMISSOR
$cnpj         = '28955961000330';
$im           = '64023';
$razaoSocial  = 'CENTRO EDUCACIONAL NOSSA SENHORA AUXILIADORA';
$nomeFantasia = 'CENSANET';
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

$parametro = $_GET['p'];

switch ($tarefa) {
    case "consultar-situacao-rps":

        $retorno = $nfse->consultarSituacaoLoteRps($parametro, $cnpj, $im);
        if (!$retorno) {
            $msg = mostrarErro($nfse->errMsg);
        } else {
            $msg = mostrarSucesso($retorno);
        }
        break;

    case "consultar-lote-protocolo":
        $retorno = $nfse->consultarLoteRps($parametro, $cnpj, $im);
        if (!$retorno) {
            $msg = mostrarErro($nfse->errMsg);
        } else {
            $msg = mostrarSucesso($retorno);
        }

        break;

    case "consultar-nfse-rps":
        $numrps = $parametro;
        $tipo = 1;
        $serie = 1;
        $retorno = $nfse->consultarNfseRps($numrps, $tipo, $serie, $cnpj, $im);
        if (!$retorno) {
            $msg = mostrarErro($nfse->errMsg);
        } else {
            $msg = mostrarSucesso($retorno);
        }

        break;

    case "consultar-nfse-numero":
        $numnfse = $parametro;
        $retorno = $nfse->consultarNfse($numnfse, $cnpj, $im);
        if (!$retorno) {
            $msg = mostrarErro($nfse->errMsg);
        } else {
            $msg = mostrarSucesso($retorno);
        }
        break;

    case "consultar-nfse-datas":
        $numnfse = $parametro;
        $retorno = $nfse->consultarNfse(false, $cnpj, $im, '2014-06-05', '2014-06-05');
        if (!$retorno) {
            $msg = mostrarErro($nfse->errMsg);
        } else {
            $msg = mostrarSucesso($retorno);
        }
        break;


    case "cancelar-nfse":
        $numnfse = $parametro;
        $retorno = $nfse->cancelarNfse($numnfse, $cnpj, $im);
        if (!$retorno) {
            $msg = mostrarErro($nfse->errMsg);
        } else {
            $msg = mostrarSucesso($retorno);
        }
        break;

    case "pdf":
        $numnfse = $parametro;
        $pdf = $nfse->gerarPDF($numnfse, array('NomePrefeitura'=>
            "PREFEITURA MUNICIPAL DE CAMPOS DOS GOYTACAZES\n SECRETARIA MUNICIPAL DE FAZENSA\nNOTA FISCAL ELETRÔNICA DE SERVIÇO - NFS-e",
            'Municipio'=>'CAMPOS DOS GOYTACAZES', 'UfMunicipio'=>'RJ'));
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="'.$pdf.'.pdf"');
        echo file_get_contents($pdf);
        //exit(0);
        break;
    case "enviar":
        //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=PREENCHENDO DADOS-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        // EXEMPLO COM DUAS NOTAS 
        
        $idLote = $parametro; // número qualquer desde que não exista ainda no ginfes
        
        $rps    = $parametro; // número superior ao ultimo rps enviado
        
        $aLote = array();

        //$this->numeroLote   = date("ym").sprintf("%011s", $idlote);
        // classe para preenchimento dos dados
        $oNF = new NFSePHPGinfesData($rps++);

        //- - - - - - - - - - - - - - DADOS DO EMISSOR - - - - - - - - - - - - - - 
        
        $oNF->set('razaoSocial', $razaoSocial);
        $oNF->set('nomeFantasia', $nomeFantasia);
        $oNF->set('CNPJ', $cnpj);
        $oNF->set('IM', $im);
        

        // série da nota fiscal
        //$oNF->set('numSerie', $parametro);

        // 1 = nota conjugada / 2-mista / 3-cupom
        $oNF->set('tipo', '1');

        /*
          01 – Tributação no municipio;
          02 – Tributação fora do municipio;
          03 – Isenção;
          04 – Imune;
          05 – Exigibilidade suspensa por decisão judicial;
          06 – Exigibilidade suspensa por procedimento administrativo.
         */
        $oNF->set('natOperacao', '4');

        // 1 = sim | 2 = não
        $oNF->set('optanteSimplesNacional', '2');

        // 1 = sim | 2 = não
        $oNF->set('incentivadorCultural', '2');

        // 1 = sim | 2 = não
        $oNF->set('regimeEspecialTributacao', '2');

        // 1 - normal 2 - cancelado (status da nota fiscal)
        $oNF->set('status', '1');

        // código do municipio do prestador segundo tabela do ibge
        $oNF->set('cMun', '3301009'); 
        
        $oNF->set('Competencia', '201403');
        //- - - - - - - - - - - - - - DESCRIÇÃO DOS SERVIÇOS - - - - - - - - - - - - - - 
        $oNF->setItem('valorServicos', 200);
        $oNF->setItem('valorDeducoes', 0);
        $oNF->setItem('valorPis', 0);
        $oNF->setItem('valorCofins', 0);
        $oNF->setItem('valorInss', 0);
        $oNF->setItem('valorIr', 0);
        $oNF->setItem('valorCsll', 0);
        $oNF->setItem('issRetido', '2');
        $oNF->setItem('valorIss', 0);
        $oNF->setItem('valorIssRetido', 0);
        $oNF->setItem('outrasRetencoes', 0);
        $oNF->setItem('aliquota', 0);
        //$oNF->setItem('valorIss', 0);
        $oNF->setItem('descontoIncondicionado', 0);
        $oNF->setItem('descontoCondicionado', 0);
        $oNF->setItem('itemListaServico', '1.07');
        $oNF->setItem('codigoTributacaoMunicipio', '619060100');
        $oNF->setItem('discriminacao', "Serviço de Valor Adicionado\\nPeríodo de Referência de 02/04/2014 à 01/05/2014\\nVencimento: 15/05/2014");

        /*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *
         * 
         * OBS: 
         * No item, os campos 'itemListaServico'  e 'codigoTributacaoMunicipio' precisam ter o EXATO formato conforme cadastro na prefeitura do municipio         
         * 
         * Para verificar qual o código de tributação referente ao serviço informado no arquivo acesse o Ginfes: http://PREFEITURADASUACIDADE.ginfes.com.br 
         * com o usuário e senha da empresa. 
         *
         * Clique em emitir NFS-e / clique em serviços prestados / clique na lupa ao lado de Código do Serviço/Atividade: informe o código ou a 
         * descrição do serviço na barra de pesquisa e clique em pesquisar / será exibido uma lista com todos os serviços referente ao código / 
         * descrição pesquisado, o código de tributação é a coluna código de atividade copie exatamente como demonstrado no sistema.
         * 
         * *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  */

        //- - - - - - - - - - - - - - DADOS DO TOMADOR DO SERVIÇO - - - - - - - 
        //$oNF->set('tomaCPF', '08005738625');
        $oNF->set('tomaCNPJ', '02291777000188');
        $oNF->set('tomaRazaoSocial', 'E.M.P.LANDIM GOMES SERV. DE LIMPEZA LTDA');
        $oNF->set('tomaEndLogradouro', 'RUA COMENDADOR BERNADINO SENA');
        $oNF->set('tomaEndNumero', '6');
        $oNF->set('tomaEndComplemento', '');
        $oNF->set('tomaEndBairro', 'PQ. TAMANDARE');
        $oNF->set('tomaEndxMun', 'CAMPOS DOS GOYTACAZES');
        //$oNF->set('tomaEndcMun', '3301009');
        $oNF->set('tomaEndUF', 'RJ');
        $oNF->set('tomaEndCep', '28035310');
        $oNF->set('tomaTelefone', '22998507096');
        $oNF->set('tomaEmail', 'tjamancio@censanet.com.br');
        $oNF->set('tomaIM', '64298');
        //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
        $aLote[] = $oNF;
        
        $oNF = new NFSePHPGinfesData($rps++);
        //- - - - - - - - - - - - - - DADOS DO EMISSOR - - - - - - - - - - - - -
        $oNF->set('razaoSocial', $razaoSocial);
        $oNF->set('nomeFantasia', $nomeFantasia);
        $oNF->set('CNPJ', $cnpj);
        $oNF->set('IM', $im);

        // série da nota fiscal
        //$oNF->set('numSerie', $parametro+1);

        // 1 = nota conjugada / 2-mista / 3-cupom
        $oNF->set('tipo', '1');

        /*
          01 – Tributação no municipio;
          02 – Tributação fora do municipio;
          03 – Isenção;
          04 – Imune;
          05 – Exigibilidade suspensa por decisão judicial;
          06 – Exigibilidade suspensa por procedimento administrativo.
         */
        $oNF->set('natOperacao', '4');

        // 1 = sim | 2 = não
        $oNF->set('optanteSimplesNacional', '2');

        // 1 = sim | 2 = não
        $oNF->set('incentivadorCultural', '2');

        // 1 = sim | 2 = não
        $oNF->set('regimeEspecialTributacao', '1');

        // 1 - normal 2 - cancelado (status da nota fiscal)
        $oNF->set('status', '1');

        // código do municipio do prestador segundo tabela do ibge
        $oNF->set('cMun', '3301009'); 
        //- - - - - - - - - - - - - - DESCRIÇÃO DOS SERVIÇOS - - - - - - - - - -
        $oNF->setItem('valorServicos', 2013.20);
        $oNF->setItem('valorDeducoes', 0);
        $oNF->setItem('valorPis', 0);
        $oNF->setItem('valorCofins', 0);
        $oNF->setItem('valorInss', 0);
        $oNF->setItem('valorIr', 0);
        $oNF->setItem('valorCsll', 0);
        $oNF->setItem('issRetido', '2');
        $oNF->setItem('valorIss', 0);
        $oNF->setItem('valorIssRetido', 0);
        $oNF->setItem('outrasRetencoes', 0);
        $oNF->setItem('aliquota', 0);
        //$oNF->setItem('valorIss', 0);
        $oNF->setItem('descontoIncondicionado', 0);
        $oNF->setItem('descontoCondicionado', 0);
        $oNF->setItem('itemListaServico', '1.07');
        $oNF->setItem('codigoTributacaoMunicipio', '619060100');
        $oNF->setItem('discriminacao', "Serviço de valor adicionado");

        /*  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  * 
         * OBS: 
         * No item, os campos 'itemListaServico'  e 'codigoTributacaoMunicipio' precisam ter o EXATO formato conforme cadastro na prefeitura do municipio         
         * 
         * Para verificar qual o código de tributação referente ao serviço informado no arquivo acesse o Ginfes: http://PREFEITURADASUACIDADE.ginfes.com.br 
         * com o usuário e senha da empresa. 
         *
         * Clique em emitir NFS-e / clique em serviços prestados / clique na lupa ao lado de Código do Serviço/Atividade: informe o código ou a 
         * descrição do serviço na barra de pesquisa e clique em pesquisar / será exibido uma lista com todos os serviços referente ao código / 
         * descrição pesquisado, o código de tributação é a coluna código de atividade copie exatamente como demonstrado no sistema.
         * 
         * 
         * *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  */

        //- - - - - - - - - - - - - - DADOS DO TOMADOR DO SERVIÇO - - - - - - - 
        //$oNF->set('tomaCPF', '');
        $oNF->set('tomaCNPJ', '17463456001910');

        $oNF->set('tomaRazaoSocial', 'PATRUS TRANSPORTES URGENTES LTDA.');
        $oNF->set('tomaEndLogradouro', 'EST AMARAL PEIXOTO');
        $oNF->set('tomaEndNumero', 's/n');
        $oNF->set('tomaEndComplemento', 'KM 69,2');
        $oNF->set('tomaEndBairro', 'PARQUE SANTO AMARO');
        $oNF->set('tomaEndxMun', 'CAMPOS DOS GOYTACAZES');
        $oNF->set('tomaEndcMun', '3301009');
        $oNF->set('tomaEndUF', 'RJ');
        $oNF->set('tomaEndCep', '28040000');
        //$oNF->set('tomaEmail', 'jorgeribeiro@patrus.com.br');
        //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        //$aLote[] = $oNF;
        
        //var_dump($aLote);
        
        //- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

        
        //-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

        $xmlLote = $nfse->montarLoteRps($idLote , $aLote);
        //file_put_contents('teste.xml', $xmlLote);
        
        $retorno = $nfse->enviarLoteRps($xmlLote);
        
 
        
        if (!$retorno) {
            $msg = mostrarErro($nfse->errMsg);
        } else {
            $msg = mostrarSucesso($retorno);
        }
         
        break;
}

function mostrarErro($msg) {
    return '<div style="width:100%;border:solid 1px #000;background-color:#c0c080">' . nl2br($msg) . '</div>';
}

function mostrarSucesso($msg) {
    return "<textarea rows=50 style='width:100%'>$msg</textarea>";
}

$operacoes = array(
    "consultar-situacao-rps" => "Consultar Situação do Lote Por Protocolo",
    "consultar-lote-protocolo" => "Consultar Lote Por Protocolo",
    "consultar-nfse-rps" => "Consultar Por RPS",
    "consultar-nfse-numero" => "Consultar Por Número da NFS-e",
    "consultar-nfse-datas" => "Consultar Entre Datas",
    "cancelar-nfse" => "Cancelar NFS-e Por Número",
    "pdf" => "Gerar PDF (Por número da NFS-e)",
    "enviar" => "Enviar Lote RPS"
);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="Pragma" content="no-cache" />
        <title><? echo $_GET['t'] . " - " . $_GET['p']; ?> / TESTADOR</title>
    </head>
    <body>
        <form action="exemplosNFSe.php" method="get">
            Operação: 
            <select name="t">
                <?
                foreach ($operacoes as $v => $r) {
                    $chk = ($v == $_GET['t'] ? "selected" : "");
                    echo "<option $chk value='$v'>$r</option>";
                }
                ?>
            </select>
            Parametro:
            <input type="text" value="<? echo $_GET['p'] ?>" name="p" /> 
            <input type="submit" value="OK" />
        </form>
        <hr size="1" />
            <? echo $msg; ?>
    </body>
</html>