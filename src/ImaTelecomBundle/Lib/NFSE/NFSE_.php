<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ImaTelecomBundle\Lib\NFSE;

use NFePHP\Common\Certificate;
use NFePHP\NFe\Tools;
use NFePHP\NFe\Make;
use NFePHP\NFe\Common\Standardize;

/**
 * Description of NFSE
 *
 * @author jrodolfo
 */
class NFSE_ {

    //put your code here
    protected $pathFile = '';
    protected $filename = '';
    protected $password = '';
    private $cert;
    private $tools;

    public function __construct($pathFile, $filename, $password) {
        $this->pathFile = $pathFile;
        $this->filename = $filename;
        $this->password = $password;

        $this->loadCertificate();
    }

    public function loadCertificate() {
        $pfx = file_get_contents($this->pathFile . $this->filename);
        $this->cert = Certificate::readPfx($pfx, $this->password);

        //return $this->cert->isExpired();

        if ($this->cert->isExpired()) {
            echo "Certificado VENCIDO! Não é possivel mais usa-lo";
        } else {
            echo "Certificado VALIDO!";
        }

        $razao = $this->cert->getCompanyName();
        echo "\nRazao: " . $razao;
        echo "\nCNPJ: " . $this->cert->getCnpj();
        echo "\nData de Inicio: " . $this->cert->getValidFrom()->format('d/m/Y');
        echo "\nData de Término: " . $this->cert->getValidTo()->format('d/m/Y');

        echo "\n";


        /*
          //carrega a cadeia de certificados, usar apenas se necessário
          $strchain = file_get_contents('<PATH TO CHAIN IN PEM FORMAT>');
          $chain = new CertificationChain($strchain);

          $cert->chainKeys = $chain; */
    }

    public function setConfiguracao($configJson) {
        if (is_array($configJson)) {
            $configJson = json_encode($configJson);
        }

        $this->tools = new Tools($configJson, $this->cert);

        /*
          $configJson = [
          "atualizacao" => "2015-10-02 06:01:21",
          "tpAmb" => 2,
          "razaosocial" => "Fake Materiais de construção Ltda",
          "siglaUF" => "SP",
          "cnpj" => "00716345000119",
          "schemes" => "PL_008i2",
          "versao" => "3.10",
          "tokenIBPT" => "AAAAAAA",
          "CSC" => "GPB0JBWLUR6HWFTVEAS6RJ69GPCROFPBBB8G",
          "CSCid" => "000002",
          "aProxyConf" => [
          "proxyIp" => "",
          "proxyPort" => "",
          "proxyUser" => "",
          "proxyPass" => ""
          ]
          ]; */
    }

    public function taginfNFe() {
        $std = new \stdClass();
        $std->versao = '3.10'; //versão do layout
        $std->Id = ''; //se o Id de 44 digitos não for passado será gerado automaticamente
        $std->pk_nItem = null; //deixe essa variavel sempre como NULL
        //$std->det = null;
        return $std;
    }

    public function tagide() {
        $std = new \stdClass();
        $std->cUF = 35;
        $std->cNF = '80070008';
        $std->natOp = 'VENDA';

        $std->indPag = 0; //NÃO EXISTE MAIS NA VERSÃO 4.00 

        $std->mod = 55;
        $std->serie = 1;
        $std->nNF = 2;
        $std->dhEmi = '2015-02-19T13:48:00-02:00';
        $std->dhSaiEnt = null;
        $std->tpNF = 1;
        $std->idDest = 1;
        $std->cMunFG = 3518800;
        $std->tpImp = 1;
        $std->tpEmis = 1;
        $std->cDV = 2;
        $std->tpAmb = 2;
        $std->finNFe = 1;
        $std->indFinal = 0;
        $std->indPres = 0;
        $std->procEmi = 0;
        $std->verProc = '0';
        $std->dhCont = null;
        $std->xJust = null;
        return $std;
    }

    public function tagrefNFe() {
        $std = new \stdClass();
        $std->refNFe = '';
        return $std;
    }

    public function tagrefNF() {
        $std = new \stdClass();
        $std->cUF = 35;
        $std->AAMM = 1412;
        $std->CNPJ = '21500974000104';
        $std->mod = '01';
        $std->serie = 3;
        $std->nNF = 587878;
        return $std;
    }

    public function tagrefNFP() {
        $std = new \stdClass();
        $std->cUF = 33;
        $std->AAMM = 1806;
        $std->CNPJ;
        $std->CPF;
        $std->IE = 'ISENTO';
        $std->mod = '04';
        $std->serie = 0;
        $std->nNF = 5578;
        return $std;
    }

    public function tagemit() {
        $std = new \stdClass();
        $std->xNome = 'IMA TELECOM LTDA ME';
        $std->xFant = 'IMA TELECOM LTDA ME';
        $std->IE = '78829257';
        $std->IEST = '';
        $std->IM = '83155';
        $std->CNAE = '';
        $std->CRT = '1';
        $std->CNPJ = '10934273000167'; //indicar apenas um CNPJ ou CPF
        $std->CPF = '';
        return $std;
    }

    public function tagenderEmit() {
        $std = new \stdClass();
        $std->xLgr = 'Teste';
        $std->nro = '1';
        $std->xCpl = 'teste';
        $std->xBairro = 'Centro';
        $std->cMun = '3301009';
        $std->xMun = 'Campos dos Goytacazes';
        $std->UF = 'RJ';
        $std->CEP = '28000000';
        $std->cPais = '';
        $std->xPais = '';
        $std->fone = '';
        return $std;
    }

    public function tagdest() {
        $std = new \stdClass();
        $std->xNome = 'PETERSON ALVES DA CONCEIÇÃO';
        $std->indIEDest = '1';
        $std->IE = '';
        $std->ISUF = '';
        $std->IM = '';
        $std->email = '';
        $std->CNPJ = '21500974000104'; //indicar apenas um CNPJ ou CPF ou idEstrangeiro
        $std->CPF = '';
        $std->idEstrangeiro = '';
        return $std;
    }

    public function tagenderDest() {
        $std = new \stdClass();
        $std->xLgr = 'Teste';
        $std->nro = '2';
        $std->xCpl = 'Teste';
        $std->xBairro = 'Centro';
        $std->cMun = '3301009';
        $std->xMun = 'Campos dos Goytacazes';
        $std->UF = 'RJ';
        $std->CEP = '28000000';
        $std->cPais = '';
        $std->xPais = '';
        $std->fone = '';
        return $std;
    }

    public function tagprod() {
        $std = new \stdClass();
        $std->item = 1; //item da NFe
        $std->cProd = '422190400';
        $std->cEAN = '';
        $std->xProd = 'CONSTRUÇÃO DE ESTAÇÕES E REDES DE TELECOMUNICAÇÕES';
        $std->NCM = '01022990';

        $std->cBenef = ''; //incluido no layout 4.00

        $std->EXTIPI = '';
        $std->CFOP = '2404';
        $std->uCom = '1';
        $std->qCom = '2';
        $std->vUnCom = '10';
        $std->vProd = '10';
        $std->cEANTrib = '0';
        $std->uTrib = '0';
        $std->qTrib = '0';
        $std->vUnTrib = '0';
        $std->vFrete = '';
        $std->vSeg = '';
        $std->vDesc = '';
        $std->vOutro = '';
        $std->indTot = '0';
        $std->xPed = '0';
        $std->nItemPed = '0';
        $std->nFCI = '';
        return $std;
    }

    public function taginfAdProd() {
        $std = new \stdClass();
        $std->item = 1; //item da NFe
        $std->infAdProd = 'informacao adicional do item';
        return $std;
    }

    public function tagimposto() {
        $std = new \stdClass();
        $std->item = 1; //item da NFe
        $std->vTotTrib = 1.00;
        return $std;
    }

    public function tagICMS() {
        $std = new \stdClass();
        $std->item = 1; //item da NFe
        $std->orig = '';
        $std->CST = '';
        $std->modBC = '';
        $std->vBC = '';
        $std->pICMS = '';
        $std->vICMS = '';
        $std->pFCP = '';
        $std->vFCP = '';
        $std->vBCFCP = '';
        $std->modBCST = '';
        $std->pMVAST = '';
        $std->pRedBCST = '';
        $std->vBCST = '';
        $std->pICMSST = '';
        $std->vICMSST = '';
        $std->vBCFCPST = '';
        $std->pFCPST = '';
        $std->vFCPST = '';
        $std->vICMSDeson = '';
        $std->motDesICMS = '';
        $std->pRedBC = '';
        $std->vICMSOp = '';
        $std->pDif = '';
        $std->vICMSDif = '';
        $std->vBCSTRet = '';
        $std->pST = '';
        $std->vICMSSTRet = '';
        $std->vBCFCPSTRet = '';
        $std->pFCPSTRet = '';
        $std->vFCPSTRet = '';
        return $std;
    }

    public function tagICMSPart() {
        $std = new \stdClass();
        $std->item = 1; //item da NFe
        $std->orig = 0;
        $std->CST = '90';
        $std->modBC = 0;
        $std->vBC = 1000.00;
        $std->pRedBC = null;
        $std->pICMS = 18.00;
        $std->vICMS = 180.00;
        $std->modBCST = 1;
        $std->pMVAST = 40.00;
        $std->pRedBCST = null;
        $std->vBCST = 1400.00;
        $std->pICMSST = 10.00;
        $std->vICMSST = 140.00;
        $std->pBCOp = 10.00;
        $std->UFST = 'RJ';
        return $std;
    }

    public function createMake($std) {
        $nfe = new Make();
        $nfe->taginfNFe($this->taginfNFe());
        $nfe->tagide($this->tagide());
        //$nfe->tagrefNFe($this->tagrefNFe());
        //$nfe->tagrefNF($this->tagrefNF());
        //$nfe->tagrefNFP($this->tagrefNFP());
        $nfe->tagemit($this->tagemit());
        $nfe->tagenderEmit($this->tagenderEmit());
        $nfe->tagdest($this->tagdest());
        $nfe->tagenderDest($this->tagenderDest());
        $nfe->tagprod($this->tagprod());
        $nfe->taginfAdProd($this->taginfAdProd());
        $nfe->tagimposto($this->tagimposto());
        $nfe->tagICMS($this->tagICMS());
        $nfe->tagICMSPart($this->tagICMSPart());
        //$nfe->tagICMS($this->tagICMS());

        $nfe->montaNFe();
        $xml = $nfe->getXML();

        //echo $xml;
        /* echo "TaginNfe";
          print_r($elem);

          echo "tagide";
          print_r($nfe->tagprod($std));
         */

        try {

//            $certificate = Certificate::readPfx($content, 'senha');
//            $tools = new Tools($configJson, $certificate);
//            $tools->model('55');

            $uf = 'RJ';
            $cnpj = '21500974000104';
            $iest = '';
            $cpf = '';
            $idLote = str_pad(1, 15, '0', STR_PAD_LEFT);
            $response = $this->tools->sefazEnviaLote([$xml], $idLote);

            //você pode padronizar os dados de retorno atraves da classe abaixo
            //de forma a facilitar a extração dos dados do XML
            //NOTA: mas lembre-se que esse XML muitas vezes será necessário, 
            //      quando houver a necessidade de protocolos
            $stdCl = new \NFePHP\NFe\Common\Standardize($response);
            //nesse caso $std irá conter uma representação em stdClass do XML
            //$std = $stdCl->toStd();
            //nesse caso o $arr irá conter uma representação em array do XML
            $arr = $stdCl->toArray();
            //nesse caso o $json irá conter uma representação em JSON do XML
            $json = $stdCl->toJson();

            //echo $json;
            var_dump($response);
        } catch (\Exception $e) {
            //echo $e->getMessage();
            var_dump($e->getMessage());
        }
    }

    public function teste() {
        $nfe = new Make();

        $std = new \stdClass();
        $std->versao = '3.10';
        $nfe->taginfNFe($std);

        $std = new \stdClass();
        $std->cUF = 33;
        $std->cNF = '80070008';
        $std->natOp = 'VENDA';
        $std->indPag = 0;
        $std->mod = 55;
        $std->serie = 1;
        $std->nNF = 2;
        $std->dhEmi = '2018-06-14T14:28:00-02:00';
        $std->dhSaiEnt = '2018-06-14T14:28:00-02:00';
        $std->tpNF = 1;
        $std->idDest = 1;
        $std->cMunFG = 3301009;
        //$std->cMunFG = 3518800;
        $std->tpImp = 1;
        $std->tpEmis = 1;
        $std->cDV = 2;
        $std->tpAmb = 2; // Se deixar o tpAmb como 2 você emitirá a nota em ambiente de homologação(teste) e as notas fiscais aqui não tem valor fiscal
        $std->finNFe = 1;
        $std->indFinal = 0;
        $std->indPres = 0;
        //$std->procEmi = '3.10.31';
        $std->procEmi = '0';
        $std->verProc = 1;
        $nfe->tagide($std);

        $std = new \stdClass();
        $std->xNome = 'IMA TELECOM LTDA ME';
        $std->IE = '78829257';
        $std->CRT = 3;
        $std->CNPJ = '10934273000167';
        $nfe->tagemit($std);
        /*
          $std->xLgr = 'Teste';
          $std->nro = '1';
          $std->xCpl = 'teste';
          $std->xBairro = 'Centro';
          $std->cMun = '3301009';
          $std->xMun = 'Campos dos Goytacazes';
          $std->UF = 'RJ';
          $std->CEP = '28000000';
          $std->cPais = '';
          $std->xPais = '';
          $std->fone = '';
         */
        $std = new \stdClass();
        $std->xLgr = "Rua Teste";
        $std->nro = '203';
        $std->xBairro = 'Centro';
        $std->cMun = '3301009';
        $std->xMun = 'Campos dos Goytacazes';
        $std->UF = 'RJ';
        $std->CEP = '28000000';
        $std->cPais = '1058';
        $std->xPais = 'BRASIL';
        $nfe->tagenderEmit($std);

        $std = new \stdClass();
        $std->xNome = 'Empresa destinatário teste';
        $std->indIEDest = 1;
        $std->IE = 'ISENTO';
        $std->CNPJ = '21500974000104';
        $nfe->tagdest($std);

        $std = new \stdClass();
        $std->xLgr = "Rua Teste";
        $std->nro = '203';
        $std->xBairro = 'Centro';
        $std->cMun = '3301009';
        $std->xMun = 'Campos dos Goytacazes';
        $std->UF = 'RJ';
        $std->CEP = '28000000';
        $std->cPais = '1058';
        $std->xPais = 'BRASIL';
        $nfe->tagenderDest($std);

        $std = new \stdClass();
        $std->item = 1;
        $std->cProd = '0001';
        $std->xProd = "Produto teste";
        //$std->NCM = '66554433';
        $std->NCM = '39199090';
        $std->CFOP = '5102';
        $std->uCom = 'PÇ';
        $std->qCom = '1.0000';
        $std->vUnCom = '10.99';
        $std->vProd = '10.99';
        $std->uTrib = 'PÇ';
        $std->qTrib = '1.0000';
        $std->vUnTrib = '10.99';
        $std->indTot = 1;
        $nfe->tagprod($std);

        $std = new \stdClass();
        $std->item = 1;
        $std->vTotTrib = 10.99;
        $nfe->tagimposto($std);

        $std = new \stdClass();
        $std->item = 1;
        $std->orig = 0;
        $std->CST = '00';
        $std->modBC = 0;
        $std->vBC = '0.20';
        $std->pICMS = '18.0000';
        $std->vICMS = '0.04';
        $nfe->tagICMS($std);

        $std = new \stdClass();
        $std->item = 1;
        $std->cEnq = '999';
        $std->CST = '50';
        $std->vIPI = 0;
        $std->vBC = 0;
        $std->pIPI = 0;
        //$nfe->tagIPI($std);

        $std = new \stdClass();
        $std->item = 1;
        $std->CST = '07';
        $std->vBC = 0;
        $std->pPIS = 0;
        $std->vPIS = 0;
        $nfe->tagPIS($std);

        $std = new \stdClass();
        $std->item = 1; //item da NFe
        $std->CST = '07';
        $std->vBC = 0;
        $std->pCOFINS = 0;
        $std->vCOFINS = 0;
        $std->qBCProd = 0;
        $std->vAliqProd = 0;
        $nfe->tagCOFINS($std);

        $std = new \stdClass();
        $std->item = 1;
        $std->vCOFINS = 0;
        $std->vBC = 0;
        $std->pCOFINS = 0;
        $nfe->tagCOFINSST($std);

        $std = new \stdClass();
        $std->vBC = 0.20;
        $std->vICMS = 0.04;
        $std->vICMSDeson = 0.00;
        $std->vBCST = 0.00;
        $std->vST = 0.00;
        $std->vProd = 10.99;
        $std->vFrete = 0.00;
        $std->vSeg = 0.00;
        $std->vDesc = 0.00;
        $std->vII = 0.00;
        $std->vIPI = 0.00;
        $std->vPIS = 0.00;
        $std->vCOFINS = 0.00;
        $std->vOutro = 0.00;
        $std->vNF = 11.03;
        $std->vTotTrib = 0.00;
        $nfe->tagICMSTot($std);

        $std = new \stdClass();
        $std->modFrete = 1;
        $nfe->tagtransp($std);

        $std = new \stdClass();
        $std->item = 1;
        $std->qVol = 2;
        $std->esp = 'caixa';
        $std->marca = 'OLX';
        $std->nVol = '11111';
        $std->pesoL = 10.00;
        $std->pesoB = 11.00;
        $nfe->tagvol($std);

        $std = new \stdClass();
        $std->nFat = '100';
        $std->vOrig = 100;
        $std->vLiq = 100;
        $nfe->tagfat($std);

        $std = new \stdClass();
        $std->nDup = '100';
        $std->dVenc = '2017-08-22';
        $std->vDup = 11.03;
        $nfe->tagdup($std);

        $xml = $nfe->getXML(); // O conteúdo do XML fica armazenado na variável $xml

        echo '<br/><br/>Assinar XML<br/><br/>';
        var_dump($xml);

        echo '<br/><br/>XML Assinado<br/><br/>';

        $xmlAssinado = $this->tools->signNFe($xml); // O conteúdo do XML assinado fica armazenado na variável $xmlAssinado
        print_r($xmlAssinado);
       /*
          //echo '<br/><br/>Enviar Lote<br/><br/>';
          $idLote = str_pad(100, 15, '0', STR_PAD_LEFT); // Identificador do lote
          $resp = $this->tools->sefazEnviaLote([$xmlAssinado], $idLote);

          $st = new \NFePHP\NFe\Common\Standardize();
          $std = $st->toStd($resp);
          if ($std->cStat != 103) {
          //erro registrar e voltar
          exit("[$std->cStat] $std->xMotivo");
          }

          echo '<br/><br/>Enviar Lote<br/><br/>';
          $recibo = $std->infRec->nRec; // Vamos usar a variável $recibo para consultar o status da nota
          print_r($recibo);

          echo '<br/><br/>Consultar Recibo<br/><br/>';
          $protocolo = $this->tools->sefazConsultaRecibo($recibo);
          print_r($protocolo);
        
          echo '<br/><br/>Finalizando o nosso script<br/><br/>';
          $protocol = new \NFePHP\NFe\Factories\Protocol();
          $xmlProtocolado = $protocol->add($xmlAssinado, $protocolo);
          print_r($xmlProtocolado);

          echo '<br/><br/>Download nota xml<br/><br/>';
          file_put_contents('nota.xml', $xmlProtocolado);
*/
        $chave = $nfe->getChave();

        echo '<br/><br/>Download nota xml<br/><br/>' . $chave;
        echo '<br/><br/>';
        /*$danfe = new \NFePHP\DA\NFe\Danfe($docxml, 'P', 'A4', '', 'I', '');
        $id = $danfe->montaDANFE();
        $teste = $danfe->printDANFE($id . '.pdf', 'F');*/
        //$response = $this->tools->sefazDownload($chave);
        //header('Content-type: text/xml; charset=UTF-8');
        //echo $response; 
        //return $response;
        /*
          $response = $this->tools->sefazConsultaChave($chave);
          $stdCl = new Standardize($response);
          //nesse caso o $json irá conter uma representação em JSON do XML
          $json = $stdCl->toJson();
          echo $json; */

        /*
          //333002180373178
          $protocolo = $this->tools->sefazConsultaRecibo($recibo);
          print_r(json_decode($protocolo));
         */
        //echo '<br/><br/>CANCELADO<br/><br/>';
        //return $this->cancelar('33180610934273000167550010000000021800700088', '333180004145761'); 
        /*
          $response = $this->tools->sefazStatus('RJ', 2);
          $stdCl = new Standardize($response);

          echo $stdCl->toJson(); */

        $idLote = str_pad(2, 15, '0', STR_PAD_LEFT);
        //envia o xml para pedir autorização ao SEFAZ
        $resp = $this->tools->sefazEnviaLote([$xmlAssinado], $idLote);
        
        echo $this->tools->sefazDownload($chave);
         /*
        $protocolo = $this->tools->sefazConsultaRecibo('refe');
        $protocol = new \NFePHP\NFe\Factories\Protocol();
        $xmlProtocolado = $protocol->add($xmlAssinado, $protocolo);*/
        
    }

    private function cancelar($chave, $nProt) {
        try {
            //$chave = '35150300822602000124550010009923461099234656';
            $xJust = 'Erro de digitação nos dados dos produtos';
            //$nProt = '135150001686732';
            $response = $this->tools->sefazCancela($chave, $xJust, $nProt);

            //você pode padronizar os dados de retorno atraves da classe abaixo
            //de forma a facilitar a extração dos dados do XML
            //NOTA: mas lembre-se que esse XML muitas vezes será necessário, 
            //      quando houver a necessidade de protocolos
            $stdCl = new \NFePHP\NFe\Common\Standardize($response);
            //nesse caso $std irá conter uma representação em stdClass do XML
            $std = $stdCl->toStd();
            //nesse caso o $arr irá conter uma representação em array do XML
            $arr = $stdCl->toArray();
            //nesse caso o $json irá conter uma representação em JSON do XML
            $json = $stdCl->toJson();

            //verifique se o evento foi processado
            if ($std->cStat != 128) {
                //houve alguma falha e o evento não foi processado
                //TRATAR
            } else {
                $cStat = $std->retEvento->infEvento->cStat;
                if ($cStat == '101' || $cStat == '155') {
                    //SUCESSO PROTOCOLAR A SOLICITAÇÂO ANTES DE GUARDAR
                    $xml = Complements::toAuthorize($tools->lastRequest, $response);
                    //grave o XML protocolado 
                    return $xml;
                } else {
                    //houve alguma falha no evento 
                    //TRATAR
                    echo 'Erro';
                }
            }

            return $response;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

}
