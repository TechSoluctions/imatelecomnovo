<?php

namespace ImaTelecomBundle\Lib;

use PHPExcel_Style_Fill;
use PHPExcel_Style_Border;
use PHPExcel_Style_Alignment;

class Sici {

    private $ancora = 1;
    private $excel;
    
    
    public function gerarExcel($phpExcel, $itens) {            
        $sici = array();
        $cidades = array("3188", "3192");
        $tecnologias = array("radio", "fibra");
        $garantiaBandas = array("30", "40", "50", "60", "100");
        $faixas = array("0-256", "257-512", "513-1024", "1025-2048", "2049-8192", "8193-12288", "12289-34816", "34817-");
        $tipoPessoas = array("pj", "pf");
        
        foreach ($cidades as $cidade) {
            $key1 = $cidade;
            foreach ($tecnologias as $tecnologia) {
                $key2 = $key1 . $tecnologia;
                foreach ($garantiaBandas as $garantiaBanda) {
                    $key3 = $key2 . $garantiaBanda;
                    foreach ($faixas as $faixa) {
                        $key4 = $key3 . $faixa;
                        foreach ($tipoPessoas as $tipoPessoa) {
                            $key5 = $key4 . $tipoPessoa;
                            $sici[$key5] = 0;
                        }
                    }
                }
            }
        }
        
        $tipoPessoa = "";
        foreach ($itens as $item)
        {
            $tipoPessoa = $item['tipo_pessoa'] == "fisica" ? "pf" : "pj";
            
            if ($item['download'] <= 256) {       
                $key = $item['codCidade'] . $item['tecnologia'] . $item['GarantiaBanda'] . "0-256" . $tipoPessoa;
                $sici[$key]++;
            } else if ($item['download'] >= 257 && $item['download'] <= 512) {
                $key = $item['codCidade'] . $item['tecnologia'] . $item['GarantiaBanda'] . "257-512" . $tipoPessoa;
                $sici[$key]++;
            } else if ($item['download'] >= 513 && $item['download'] <= 1024) {
                $key = $item['codCidade'] . $item['tecnologia'] . $item['GarantiaBanda'] . "513-1024" . $tipoPessoa;
                $sici[$key]++;
            } else if ($item['download'] >= 1025 && $item['download'] <= 2048) {
                $key = $item['codCidade'] . $item['tecnologia'] . $item['GarantiaBanda'] . "1025-2048" . $tipoPessoa;
                $sici[$key]++;
            } else if ($item['download'] >= 2049 && $item['download'] <= 8192) {
                $key = $item['codCidade'] . $item['tecnologia'] . $item['GarantiaBanda'] . "2049-8192" . $tipoPessoa;
                $sici[$key]++;
            } else if ($item['download'] >= 8193 && $item['download'] <= 12288) {
                $key = $item['codCidade'] . $item['tecnologia'] . $item['GarantiaBanda'] . "8193-12288" . $tipoPessoa;
                $sici[$key]++;
            } else if ($item['download'] >= 12289 && $item['download'] <= 34816) {
                $key = $item['codCidade'] . $item['tecnologia'] . $item['GarantiaBanda'] . "12289-34816" . $tipoPessoa;
                $sici[$key]++;
            } else if ($item['download'] >= 34817) {
                $key = $item['codCidade'] . $item['tecnologia'] . $item['GarantiaBanda'] . "34817-" . $tipoPessoa;
                $sici[$key]++;
            }
        }
     
        $excel = $phpExcel->createPHPExcelObject();

        $excel->createSheet();
        $excel->setActiveSheetIndex(0);
        $excel->getActiveSheet()->setTitle('ChartTest');
        $objWorksheet = $excel->getActiveSheet();
        $this->excel = $excel; 
        
        // set the font style for the entire workbook
        $excel->getDefaultStyle()
                ->getFont()
                ->setName('Arial')
                ->setSize(10)
                ->setBold(false);                                       
        
        //$diferenca = 0;
        $numeroCidade = 1;
        foreach ($cidades as $cidadeId) {                                       
            // radio
            $this->EscreverFaixa($objWorksheet, $sici, "radio", "40", $cidadeId, $numeroCidade);                       
            $this->EscreverFaixa($objWorksheet, $sici, "radio", "50", $cidadeId, $numeroCidade);
            $this->EscreverFaixa($objWorksheet, $sici, "radio", "60", $cidadeId, $numeroCidade);
            $this->EscreverFaixa($objWorksheet, $sici, "radio", "100", $cidadeId, $numeroCidade);

            // fibra            
            $this->EscreverFaixa($objWorksheet, $sici, "fibra", "30", $cidadeId, $numeroCidade);
            $this->EscreverFaixa($objWorksheet, $sici, "fibra", "40", $cidadeId, $numeroCidade);
            $this->EscreverFaixa($objWorksheet, $sici, "fibra", "50", $cidadeId, $numeroCidade);
            $this->EscreverFaixa($objWorksheet, $sici, "fibra", "60", $cidadeId, $numeroCidade);
            $this->EscreverFaixa($objWorksheet, $sici, "fibra", "100", $cidadeId, $numeroCidade);
            
            $numeroCidade++;            
        }
         
        for($col = 'A'; $col !== 'Z'; $col++) { 
            $excel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);              
        }
        
        return $excel;
        /*
        // Creating streamed excel file created with php
        // create the writer
        $writer = $this->get('phpexcel')->createWriter($excel, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'stream-fileee.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response; */
    }
    
    private function EscreverFaixa($objWorksheet, $sici, $tecnologia, $garantiaBanda, $cidadeId, $numeroCidade) {                        
        $cidade = "";
        $inicioChave = $cidadeId . $tecnologia . $garantiaBanda;
        
        if ($cidadeId == "3188") {
            $cidade = "Campos dos Goytacazes";
        } else {
            $cidade = "São João da Barra";
        }
        
        $backgroundColor = "0000FF";
        $backgroundColorTotal = "838B8B";
        $letterColor = "FFFFFF";
        
        //Linha 1
        $this->cellColor("A".$this->ancora.":B".$this->ancora, $backgroundColor, $letterColor);        
        $objWorksheet->SetCellValue("A".$this->ancora, "Municipio $numeroCidade");                
        $objWorksheet->getStyle("A".$this->ancora)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objWorksheet->mergeCells("A".$this->ancora.":B".$this->ancora);
        
        $this->cellColor("D".$this->ancora.":E".$this->ancora, $backgroundColor, $letterColor);        
        $objWorksheet->SetCellValue("D".$this->ancora, "Via de Distribuição");
        $objWorksheet->getStyle("D".$this->ancora)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objWorksheet->mergeCells("D".$this->ancora.":E".$this->ancora);
        
        $this->cellColor("I".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("I".$this->ancora, "Garantia de Banda");
        $objWorksheet->getStyle("D".$this->ancora)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        
        $this->ancora++;
        //Linha 2        
        $objWorksheet->SetCellValue("A".$this->ancora, $cidade);
        $objWorksheet->getStyle("A".$this->ancora)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objWorksheet->mergeCells("A".$this->ancora.":B".$this->ancora);
        
        if ($tecnologia == "radio") {
            $objWorksheet->SetCellValue("D".$this->ancora, "Speed Spectrum (Rádio)");
        } else {
            $objWorksheet->SetCellValue("D".$this->ancora, "Fibra Óptica");
        }
        
        $objWorksheet->getStyle("D".$this->ancora)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $objWorksheet->mergeCells("D".$this->ancora.":E".$this->ancora);
            
        $objWorksheet->SetCellValue("I".$this->ancora, "$garantiaBanda%");
        $objWorksheet->getStyle("I".$this->ancora)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        
        $this->ancora += 2;
        //Linha 4
        //$this->cellColor("B".$this->ancora.":J".$this->ancora, $backgroundColor, $letterColor);
        $this->cellColor("B".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("B".$this->ancora, "até 256 Kbps");
        $this->cellColor("C".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("C".$this->ancora, "até 512 Kbps");
        $this->cellColor("D".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("D".$this->ancora, "até 1 Mbps");
        $this->cellColor("E".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("E".$this->ancora, "até 2 Mbps");
        $this->cellColor("F".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("F".$this->ancora, "até 8 Mbps");
        $this->cellColor("G".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("G".$this->ancora, "até 12 Mbps");
        $this->cellColor("H".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("H".$this->ancora, "até 34 Mbps");
        $this->cellColor("I".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("I".$this->ancora, "acima de 34 Mbps");
        $this->cellColor("J".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("J".$this->ancora, "Total");
        
        
        $this->ancora++;
        //Linha 5
        $this->cellColor("A".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("A".$this->ancora, "Pessoa Física");
        $objWorksheet->SetCellValue("B".$this->ancora, $sici[$inicioChave . "0-256pf"]);
        $objWorksheet->SetCellValue("C".$this->ancora, $sici[$inicioChave . "257-512pf"]);
        $objWorksheet->SetCellValue("D".$this->ancora, $sici[$inicioChave . "513-1024pf"]);
        $objWorksheet->SetCellValue("E".$this->ancora, $sici[$inicioChave . "1025-2048pf"]);
        $objWorksheet->SetCellValue("F".$this->ancora, $sici[$inicioChave . "2049-8192pf"]);
        $objWorksheet->SetCellValue("G".$this->ancora, $sici[$inicioChave . "8193-12288pf"]);
        $objWorksheet->SetCellValue("H".$this->ancora, $sici[$inicioChave . "12289-34816pf"]);
        $objWorksheet->SetCellValue("I".$this->ancora, $sici[$inicioChave . "34817-pf"]);
        
        $totalPf = 0;
        $totalPf += $sici[$inicioChave . "0-256pf"];
        $totalPf += $sici[$inicioChave . "257-512pf"];
        $totalPf += $sici[$inicioChave . "513-1024pf"];
        $totalPf += $sici[$inicioChave . "1025-2048pf"];
        $totalPf += $sici[$inicioChave . "2049-8192pf"];
        $totalPf += $sici[$inicioChave . "8193-12288pf"];
        $totalPf += $sici[$inicioChave . "12289-34816pf"];
        $totalPf += $sici[$inicioChave ."34817-pf"];
            
        $this->cellColor("J".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("J".$this->ancora, $totalPf);
        
        
        $this->ancora++;
        //Linha 6
        $this->cellColor("A".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("A".$this->ancora, "Pessoa Jurídica");
        $objWorksheet->SetCellValue("B".$this->ancora, $sici[$inicioChave . "0-256pj"]);
        $objWorksheet->SetCellValue("C".$this->ancora, $sici[$inicioChave . "257-512pj"]);
        $objWorksheet->SetCellValue("D".$this->ancora, $sici[$inicioChave . "513-1024pj"]);
        $objWorksheet->SetCellValue("E".$this->ancora, $sici[$inicioChave . "1025-2048pj"]);
        $objWorksheet->SetCellValue("F".$this->ancora, $sici[$inicioChave . "2049-8192pj"]);
        $objWorksheet->SetCellValue("G".$this->ancora, $sici[$inicioChave . "8193-12288pj"]);
        $objWorksheet->SetCellValue("H".$this->ancora, $sici[$inicioChave . "12289-34816pj"]);
        $objWorksheet->SetCellValue("I".$this->ancora, $sici[$inicioChave . "34817-pj"]);
        
        $totalPj = 0;
        $totalPj += $sici[$inicioChave . "0-256pj"];
        $totalPj += $sici[$inicioChave . "257-512pj"];
        $totalPj += $sici[$inicioChave . "513-1024pj"];
        $totalPj += $sici[$inicioChave . "1025-2048pj"];
        $totalPj += $sici[$inicioChave . "2049-8192pj"];
        $totalPj += $sici[$inicioChave . "8193-12288pj"];
        $totalPj += $sici[$inicioChave . "12289-34816pj"];
        $totalPj += $sici[$inicioChave ."34817-pj"];
        
        $this->cellColor("J".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("J".$this->ancora, $totalPj);
                
        $this->ancora++;
        //Linha 7
        $this->cellColor("A".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("A".$this->ancora, "Total");
        $this->cellColor("B".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("B".$this->ancora, $sici[$inicioChave . "0-256pf"] + $sici[$inicioChave . "0-256pj"]);
        $this->cellColor("C".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("C".$this->ancora, $sici[$inicioChave . "257-512pf"] + $sici[$inicioChave . "257-512pj"]);
        $this->cellColor("D".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("D".$this->ancora, $sici[$inicioChave . "513-1024pf"] + $sici[$inicioChave . "513-1024pj"]);
        $this->cellColor("E".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("E".$this->ancora, $sici[$inicioChave . "1025-2048pf"] + $sici[$inicioChave . "1025-2048pj"]);
        $this->cellColor("F".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("F".$this->ancora, $sici[$inicioChave . "2049-8192pf"] + $sici[$inicioChave . "2049-8192pj"]);
        $this->cellColor("G".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("G".$this->ancora, $sici[$inicioChave . "8193-12288pf"] + $sici[$inicioChave . "8193-12288pj"]);
        $this->cellColor("H".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("H".$this->ancora, $sici[$inicioChave . "12289-34816pf"] + $sici[$inicioChave . "12289-34816pj"]);
        $this->cellColor("I".$this->ancora, $backgroundColorTotal, $letterColor);
        $objWorksheet->SetCellValue("I".$this->ancora, $sici[$inicioChave . "34817-pf"] + $sici[$inicioChave . "34817-pj"]);
        
        $this->cellColor("J".$this->ancora, $backgroundColor, $letterColor);
        $objWorksheet->SetCellValue("J".$this->ancora, $totalPf + $totalPj);
        
        
        $this->ancora += 3;                
    }
    
    
    private function cellColor($cells, $backgroundColor, $letterColor) {
        // style config font
        $styleFont = array(
            'font'  => array(                
                'color' => array('rgb' => $letterColor),
                'size'  => 10,
                'name'  => 'Arial'
            )
        );
        
        $styleBorder = array(
            'borders' => array(
                'outline' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => "FFFFFF")
                )
              )
        );
        
        $styleContent = array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array(
                 'rgb' => $backgroundColor
            )
        );
           
        $this->excel->getActiveSheet()->getStyle($cells)->applyFromArray($styleFont)->getFill()->applyFromArray($styleContent);
        $this->excel->getActiveSheet()->getStyle($cells)->applyFromArray($styleBorder);
    }
}

?>