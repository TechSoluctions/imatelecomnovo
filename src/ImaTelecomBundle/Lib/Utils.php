<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ImaTelecomBundle\Lib;

/**
 * Description of Utils
 *
 * @author jrodolfo
 */
class Utils {

    //put your code here

    public static function validarCPF($cpf) {
        $cpf = str_replace(array('.', '-'), '', $cpf);
        $status = false;
        if (!is_numeric($cpf)) {
            $status = false;
        } else {
            //VERIFICA            
            if (($cpf == '11111111111') || ($cpf == '22222222222') || ($cpf == '33333333333') || ($cpf == '44444444444') || ($cpf == '55555555555') ||
                    ($cpf == '66666666666') || ($cpf == '77777777777') || ($cpf == '88888888888') || ($cpf == '99999999999') || ($cpf == '00000000000')) {
                $status = false;
            } elseif (strlen($cpf) != 11) {
                $status = false;
            } else {
                //PEGA O DIGITO VERIFIACADOR
                $dv_informado = substr($cpf, 9, 2);

                for ($i = 0; $i <= 8; $i++) {
                    $digito[$i] = substr($cpf, $i, 1);
                }

                //CALCULA O VALOR DO 10º DIGITO DE VERIFICAÇÂO
                $posicao = 10;
                $soma = 0;

                for ($i = 0; $i <= 8; $i++) {
                    $soma = $soma + $digito[$i] * $posicao;
                    $posicao = $posicao - 1;
                }

                $digito[9] = $soma % 11;

                if ($digito[9] < 2) {
                    $digito[9] = 0;
                } else {
                    $digito[9] = 11 - $digito[9];
                }

                //CALCULA O VALOR DO 11º DIGITO DE VERIFICAÇÃO
                $posicao = 11;
                $soma = 0;

                for ($i = 0; $i <= 9; $i++) {
                    $soma = $soma + $digito[$i] * $posicao;
                    $posicao = $posicao - 1;
                }

                $digito[10] = $soma % 11;

                if ($digito[10] < 2) {
                    $digito[10] = 0;
                } else {
                    $digito[10] = 11 - $digito[10];
                }

                //VERIFICA SE O DV CALCULADO É IGUAL AO INFORMADO
                $dv = $digito[9] * 10 + $digito[10];

                if ($dv != $dv_informado) {
                    $status = false;
                } else {
                    $status = true;
                }
            }//FECHA ELSE VERIFICA
        }//FECHA ELSE(is_numeric)
        return $status;
    }

    public static function validarCNPJ($cnpj) {
        $cnpj = preg_replace("@[./-]@", "", $cnpj);

        if (strlen($cnpj) <> 14 or ! is_numeric($cnpj)) {
            $status = false;
        } else {
            $k = 6;
            $soma1 = 0.0;
            $soma2 = 0.0;
            for ($i = 0; $i < 13; $i++) {
                $k = $k == 1 ? 9 : $k;
                $soma2 += ($cnpj{$i} * $k);
                $k--;
                if ($i < 12) {
                    if ($k == 1) {
                        $k = 9;
                        $soma1 += ($cnpj{$i} * $k);
                        $k = 1;
                    } else {
                        $soma1 += ($cnpj{$i} * $k);
                    }
                }
            }

            $digito1 = $soma1 % 11 < 2 ? 0 : 11 - $soma1 % 11;
            $digito2 = $soma2 % 11 < 2 ? 0 : 11 - $soma2 % 11;

            $status = ( $cnpj{12} == $digito1 and $cnpj{13} == $digito2);
        }
        return $status;
    }

}
