<?php

namespace ImaTelecomBundle\EventListener;

use ImaTelecomBundle\Interfaces\CompetenciaAtualInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class CompetenciaAtualListener {

    private $twig;

    public function __construct(\Twig_Environment $twig) {
        $this->twig = $twig;
    }

    public function onKernelController(FilterControllerEvent $event) {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        $request = $event->getRequest();
        $route = $request->attributes->get('_route');
        
        //$session->set('competenciaAtualHeader', $competenciaAtual);

        if ((strpos($route, 'administrativo') === false) &&
                (strpos($route, 'financeiro') === false) &&
                (strpos($route, 'fiscal') === false)) {
            $this->twig->addGlobal('menuAtivo', 'gestao');
        } else {
            $this->twig->addGlobal('menuAtivo', $route);
        }

        if ($controller[0] instanceof CompetenciaAtualInterface) {
            $competenciaAtual = $controller[0]->getCompetenciaAtual();

            $session = $request->getSession();
            $session->set('competenciaAtualHeader', $competenciaAtual);
            $this->twig->addGlobal('competenciaAtualHeader', $competenciaAtual);
        }
    }

}
