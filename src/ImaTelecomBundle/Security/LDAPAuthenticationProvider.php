<?php

namespace ImaTelecomBundle\Security;

use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\Security\Core\Authentication\SimpleFormAuthenticatorInterface;
use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class LDAPAuthenticationProvider implements SimpleFormAuthenticatorInterface {

    private $diretorioImagemPerfil;
    private $encoder;

    public function __construct($diretorioImagemPerfil, UserPasswordEncoderInterface $encoder) {
        $this->diretorioImagemPerfil = $diretorioImagemPerfil;
        $this->encoder = $encoder;
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey) {
        $username = $token->getUsername();
        try {
            $user = $userProvider->loadUserByUsername($username);
        } catch (UsernameNotFoundException $e) {
            throw new AuthenticationException("Usuário $username inválido.");
        }

        $host = 'auth.censanet.com.br';
        $domain = 'auxiliadora.censanet.com.br';

        $ldap_conn = \ldap_connect('ldap://' . $host, 389);
        if ($ldap_conn) {
            $session = new Session();

            // attempt binding
            $binding = @ldap_bind($ldap_conn, $username . '@' . $domain, $token->getCredentials());
            if ($binding) {                
                // pegar foto do usuario
                $filter = "(samaccountname=$username)";
                $dn = 'OU=Censanet, DC=auxiliadora, DC=censanet, DC=com, DC=br';
                $sr = ldap_search($ldap_conn, $dn, $filter, array('thumbnailphoto'));
                $u = ldap_get_entries($ldap_conn, $sr);
                $picture = $u[0]['thumbnailphoto'][0];
                                           
                if (!file_exists($this->diretorioImagemPerfil)) {
                    mkdir($this->diretorioImagemPerfil, 0777, true);
                }
                
                file_put_contents("$this->diretorioImagemPerfil/{$username}_thumb.png", $picture);
                                
                //$session->set('photo_perfil', base64_encode($picture));
                $session->set('ck_authorized_site_imatelecom', true);
                
                // authenticated                
                return new UsernamePasswordToken($user, $user->getPassword(), $providerKey, $user->getRoles());
            }
        }

        ldap_close($ldap_conn);

        throw new AuthenticationException('Senha inválida para o usuário: ' . $token->getUsername());
    }

    public function supportsToken(TokenInterface $token, $providerKey) {
        return $token instanceof UsernamePasswordToken && $token->getProviderKey() === $providerKey;
    }

    public function createToken(Request $request, $username, $password, $providerKey) {
        return new UsernamePasswordToken($username, $password, $providerKey);
    }

}
