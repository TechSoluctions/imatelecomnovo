<?php

namespace ImaTelecomBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsAlphanumeric extends Constraint
{
    public $message = 'Teste The string "{{ string }}" contains an illegal character: it can only contain letters or numbers.';
}