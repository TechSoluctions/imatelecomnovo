<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use \DateTime;
use ImaTelecomBundle\Model\PessoaQuery;
use ImaTelecomBundle\Model\Pessoa;
use ImaTelecomBundle\Model\TelefoneQuery;
use ImaTelecomBundle\Model\Telefone;
use ImaTelecomBundle\Model\ClienteQuery;
use ImaTelecomBundle\Model\Cliente;
use ImaTelecomBundle\Model\CidadeQuery;
use ImaTelecomBundle\Model\EnderecoClienteQuery;
use ImaTelecomBundle\Model\EnderecoCliente;
use ImaTelecomBundle\Model\ServicoClienteQuery;
use ImaTelecomBundle\Model\ServicoCliente;
use ImaTelecomBundle\Model\ServicoPrestadoQuery;
use ImaTelecomBundle\Model\EnderecoServClienteQuery;
use ImaTelecomBundle\Model\EnderecoServCliente;
use ImaTelecomBundle\Model\BoletoQuery;
use ImaTelecomBundle\Model\Boleto;
use ImaTelecomBundle\Model\BoletoItemQuery;
use ImaTelecomBundle\Model\BoletoItem;
use ImaTelecomBundle\Model\CompetenciaQuery;
use ImaTelecomBundle\Model\Contrato;
use ImaTelecomBundle\Model\Map\ServicoClienteTableMap;
use \PropelException;
use ImaTelecomBundle\Lib\InscricaoEstadual;
use Symfony\Component\HttpFoundation\Request;
use Propel\Runtime\Propel;

//use ImaTelecomBundle\Model\PlanosQuery;

/**
 * Description of ImportacaoController
 *
 * @author jrodolfo
 */
class ImportacaoController extends WSController {

    public function pessoaAction(Request $request) {
        $teveErros = false;
        $retorno = array();
        $retorno['erros'] = array();

        $pessoas = array();
        $conteudo = $request->getContent();
        if (!empty($conteudo)) {
            $pessoas = json_decode($conteudo);
        }

        foreach ($pessoas as $pessoa) {
            $retornoPersistencia = $this->pessoa($pessoa);

            if (!$retornoPersistencia['status']) {
                $teveErros = true;
                $retorno['erros'][] = array_merge(array('Código ID: ' . $pessoa[0]), $retornoPersistencia['erros']);
            }
        }

        if ($teveErros) {
            $mensagemErros = '';
            foreach ($retorno['erros'] as $erros) {
                $mensagemErros .= implode(" | ", $erros) . "\r\n";
            }

            $filename = 'tmp/pessoa_log_' . time() . '.txt';

            $arquivo = fopen($filename, "w");
            fwrite($arquivo, $mensagemErros);
            fclose($arquivo);
        }

        $retorno['status'] = !$teveErros;
        return new JsonResponse($retorno);
    }

    private function pessoa($dados) {
        $status = false;
        $erros = array();

        $usuarioSistema = 1;
        $importId = $dados[0];
        $nome = utf8_encode($this->clean(trim($dados[1])));
        $razaoSocial = utf8_encode($this->clean(trim($dados[2])));
        $cpfCnpj = trim($dados[8]);
        $rua = utf8_encode($this->clean(trim($dados[3])));
        $bairro = utf8_encode($this->clean(trim($dados[4])));
        $cidadeId = trim($dados[16]);
        $contato = utf8_encode($this->clean($dados[18]));
        $responsavel = utf8_encode($this->clean($dados[17]));
        $observacao = utf8_encode($this->clean($dados[15]));
        $tipoPessoa = $dados[13];
        $complemento = utf8_encode($this->clean($dados[12]));
        $email = $dados[11];
        $rgInscrest = $dados[9];
        $cep = trim($dados[5]);
        $pontoReferencia = utf8_encode($this->clean($dados[10]));
        $uf = empty($dados[7]) ? 'RJ' : trim($dados[7]);

        $numero = empty($dados[6]) ? 0 : $dados[6];
        $dataNascimento = empty($dados[14]) ? null : $dados[14];
        $sexo = str_replace(' ', '', $dados[19]);
        $sexo = empty($sexo) ? 'm' : $sexo;


        if (empty($cidadeId)) {
            $erros[] = 'Cidade: Campo não pode ser vazio.';
        } else {
            $cidade = CidadeQuery::create()->findPk($cidadeId);
            if ($cidade == null) {
                $erros[] = "Cidade: Não foi possível localizar, Código de Importação: $cidadeId";
            } else {
                $uf = $cidade->getUf();
            }
        }

        if (empty($nome)) {
            $erros[] = 'Nome: Campo não pode ser vazio.';
        }

        if ($tipoPessoa == 'juridica') {
            if (empty($razaoSocial)) {
                $erros[] = 'Razão Social: Campo não pode ser vazio.';
            }

            if (!InscricaoEstadual::validar($rgInscrest, $uf) && !empty($rgInscrest)) {
                $erros[] = 'Inscrição Estadual: Campo inválido. ' . $rgInscrest . '  ' . $uf;
            }
        } else {
            if (empty($rgInscrest)) {
                $erros[] = 'RG/Insc. Estadual: Campo não pode ser vazio.';
            }
        }

        if (empty($importId)) {
            $erros[] = 'Id: Campo não pode ser vazio.';
        }

        if (empty($cpfCnpj)) {
            $erros[] = 'CPF/CNPJ: Campo não pode ser vazio.';
        }

        if (empty($rua)) {
            $erros[] = 'Rua: Campo não pode ser vazio.';
        }

        if (empty($bairro)) {
            $erros[] = 'Bairro: Campo não pode ser vazio.';
        }

        if (empty($cep)) {
            $erros[] = 'CEP: Campo não pode ser vazio.';
        }

        if (sizeof($erros) == 0) {
            gc_enable();
            Propel::disableInstancePooling();

            $pessoa = PessoaQuery::create()->findByImportId($importId)->getFirst();
            if ($pessoa == null) {
                $pessoa = new Pessoa();
                $pessoa->setDataCadastro(new DateTime('now'));
                $pessoa->setImportId($importId);
            }

            $pessoa->setNome($nome);
            $pessoa->setRazaoSocial($razaoSocial);
            $pessoa->setCpfCnpj($cpfCnpj);
            $pessoa->setTipoPessoa($tipoPessoa);
            $pessoa->setEmail($email);
            $pessoa->setRgInscrest($rgInscrest);
            $pessoa->setCep($cep);
            $pessoa->setPontoReferencia($pontoReferencia);
            $pessoa->setNum($numero);
            $pessoa->setRua($rua);
            $pessoa->setBairro($bairro);
            $pessoa->setCidadeId($cidadeId);
            $pessoa->setUf($uf);
            $pessoa->setComplemento($complemento);
            $pessoa->setContato($contato);
            $pessoa->setResponsavel($responsavel);
            $pessoa->setObs($observacao);
            $pessoa->setDataNascimento($dataNascimento);
            $pessoa->setSexo($sexo);
            $pessoa->setDataAlterado(new DateTime('now'));
            $pessoa->setUsuarioAlterado($usuarioSistema);

            try {
                $pessoa->save();

                $status = true;
            } catch (PropelException $ex) {
                $erros[] = $ex->getMessage();
            }

            $pessoa->clearAllReferences(true);
            unset($pessoa);
            gc_collect_cycles();
        }

        return array('status' => $status, 'erros' => $erros);
    }

    public function telefoneAction(Request $request) {
        $teveErros = false;
        $retorno = array();
        $retorno['erros'] = array();

        $telefones = array();
        $conteudo = $request->getContent();
        if (!empty($conteudo)) {
            $telefones = json_decode($conteudo);
        }

        foreach ($telefones as $telefone) {
            $retornoPersistencia = $this->telefone($telefone);

            if (!$retornoPersistencia['status']) {
                $teveErros = true;
                $retorno['erros'][] = array_merge(array('Código ID: ' . $telefone[0]), $retornoPersistencia['erros']);
            }
        }

        if ($teveErros) {
            $mensagemErros = '';
            foreach ($retorno['erros'] as $erros) {
                $mensagemErros .= implode(" | ", $erros) . "\r\n";
            }

            $filename = 'tmp/telefone_log_' . time() . '.txt';

            $arquivo = fopen($filename, "w");
            fwrite($arquivo, $mensagemErros);
            fclose($arquivo);
        }

        $retorno['status'] = !$teveErros;
        return new JsonResponse($retorno);
    }

    private function telefone($dados) {
        $status = false;
        $erros = array();

        $usuarioSistema = 1;
        $importId = $dados[0];
        $pessoaImportId = $dados[1];
        $tipo = trim($dados[2]);
        $ddd = trim($dados[3]);
        $numero = trim($dados[4]);


        if (empty($pessoaImportId)) {
            $erros[] = 'Pessoa: Código de Importação vazio.';
        } else {
            $pessoa = PessoaQuery::create()->findByImportId($pessoaImportId)->getFirst();
            if ($pessoa == null) {
                $erros[] = "Pessoa: Não foi possível localizar, Código de Importação: $pessoaImportId";
            }
        }

        if (empty($tipo)) {
            $erros[] = 'Tipo: Campo não pode ser vazio.';
        }

        if (empty($ddd)) {
            $erros[] = 'DDD: Campo não pode ser vazio.';
        }

        if (empty($numero)) {
            $erros[] = 'Número: Campo não pode ser vazio.';
        }

        if (sizeof($erros) == 0) {
            gc_enable();
            Propel::disableInstancePooling();

            $telefone = TelefoneQuery::create()->findByImportId($importId)->getFirst();
            if ($telefone == null) {
                $telefone = new Telefone();
                $telefone->setDataCadastro(new DateTime('now'));
                $telefone->setImportId($importId);
            }

            $telefone->setTipo($tipo);
            $telefone->setDdd($ddd);
            $telefone->setNumero($numero);
            $telefone->setPessoa($pessoa);
            $telefone->setDataAlterado(new DateTime('now'));
            $telefone->setUsuarioAlterado($usuarioSistema);

            try {
                $telefone->save();

                $status = true;
            } catch (PropelException $ex) {
                $erros[] = $ex->getMessage();
            }

            $telefone->clearAllReferences(true);
            unset($telefone);
            gc_collect_cycles();
        }

        return array('status' => $status, 'erros' => $erros);
    }

    public function clienteAction(Request $request) {
        $teveErros = false;
        $retorno = array();
        $retorno['erros'] = array();

        $clientes = array();
        $conteudo = $request->getContent();
        if (!empty($conteudo)) {
            $clientes = json_decode($conteudo);
        }

        foreach ($clientes as $cliente) {
            $retornoPersistencia = $this->cliente($cliente);

            if (!$retornoPersistencia['status']) {
                $teveErros = true;
                $retorno['erros'][] = array_merge(array('Código ID: ' . $cliente[0]), $retornoPersistencia['erros']);
            }
        }

        if ($teveErros) {
            $mensagemErros = '';
            foreach ($retorno['erros'] as $erros) {
                $mensagemErros .= implode(" | ", $erros) . "\r\n";
            }

            $filename = 'tmp/cliente_log_' . time() . '.txt';

            $arquivo = fopen($filename, "w");
            fwrite($arquivo, $mensagemErros);
            fclose($arquivo);
        }

        $retorno['status'] = !$teveErros;
        return new JsonResponse($retorno);
    }

    private function cliente($dados) {
        $status = false;
        $erros = array();

        $usuarioSistema = 1;
        $importId = trim($dados[0]);
        $pessoaImportId = trim($dados[1]);
        $ativo = in_array(trim($dados[2]), array('0', '1')) ? $dados[2] : '0';
        $dataDoContrato = empty($dados[3]) ? null : DateTime::createFromFormat('Y-m-d H:i:s', $dados[3]);
        $dataDoCancelamento = empty($dados[4]) ? null : DateTime::createFromFormat('Y-m-d H:i:s', $dados[4]);

        if (empty($pessoaImportId)) {
            $erros[] = 'Pessoa: Código de Importação vazio.';
        } else {
            $pessoa = PessoaQuery::create()->findByImportId($pessoaImportId)->getFirst();
            if ($pessoa == null) {
                $erros[] = "Pessoa: Não foi possível localizar, Código de Importação: $pessoaImportId";
            }
        }

        if (sizeof($erros) == 0) {
            gc_enable();
            Propel::disableInstancePooling();

            $cliente = ClienteQuery::create()->findByImportId($importId)->getFirst();
            if ($cliente == null) {
                $cliente = new Cliente();
                $cliente->setDataCadastro(new DateTime('now'));
                $cliente->setImportId($importId);
            }

            $cliente->setPessoa($pessoa);
            $cliente->setAtivo($ativo);
            $cliente->setDataContrato($dataDoContrato);
            $cliente->setDataCancelado($dataDoCancelamento);
            $cliente->setDataAlterado(new DateTime('now'));
            $cliente->setUsuarioAlterado($usuarioSistema);

            try {
                $cliente->save();

                $status = true;
            } catch (PropelException $ex) {
                $erros[] = $ex->getMessage();
            }

            $cliente->clearAllReferences(true);
            unset($cliente);
            gc_collect_cycles();
        }

        return array('status' => $status, 'erros' => $erros);
    }

    public function enderecoClienteAction(Request $request) {
        $teveErros = false;
        $retorno = array();
        $retorno['erros'] = array();

        $enderecoClientes = array();
        $conteudo = $request->getContent();
        if (!empty($conteudo)) {
            $enderecoClientes = json_decode($conteudo);
        }

        foreach ($enderecoClientes as $enderecoCliente) {
            $retornoPersistencia = $this->enderecoCliente($enderecoCliente);

            if (!$retornoPersistencia['status']) {
                $teveErros = true;
                $retorno['erros'][] = array_merge(array('Código ID: ' . $enderecoCliente[0]), $retornoPersistencia['erros']);
            }
        }

        if ($teveErros) {
            $mensagemErros = '';
            foreach ($retorno['erros'] as $erros) {
                $mensagemErros .= implode(" | ", $erros) . "\r\n";
            }

            $filename = 'tmp/endereco_cliente_log_' . time() . '.txt';

            $arquivo = fopen($filename, "w");
            fwrite($arquivo, $mensagemErros);
            fclose($arquivo);
        }

        $retorno['status'] = !$teveErros;
        return new JsonResponse($retorno);
    }

    private function enderecoCliente($dados) {
        $status = false;
        $erros = array();

        $usuarioSistema = 1;
        $importId = trim($dados[0]);
        $clienteImportId = trim($dados[1]);
        $rua = $this->clean($this->remove_accents(trim($dados[2])));
        $bairro = $this->clean($this->remove_accents(trim($dados[3])));
        $numero = empty($dados[4]) ? 0 : $dados[4];
        $complemento = $this->clean($this->remove_accents(trim($dados[5])));
        $cep = trim($dados[7]);
        $uf = empty($dados[8]) ? 'RJ' : trim($dados[8]);
        $pontoReferencia = $this->clean($this->remove_accents(trim($dados[9])));
        $contato = $this->clean($this->remove_accents(trim($dados[10])));
        $cidadeId = trim($dados[6]);


        if (empty($rua)) {
            $erros[] = 'Rua: Campo não pode ser vazio.';
        }

        if (empty($bairro)) {
            $erros[] = 'Bairro: Campo não pode ser vazio.';
        }

        if (empty($cep)) {
            $erros[] = 'CEP: Campo não pode ser vazio.';
        }

        if (empty($clienteImportId)) {
            $erros[] = 'Cliente: Código de Importação vazio.';
        } else {
            $cliente = ClienteQuery::create()->findByImportId($clienteImportId)->getFirst();
            if ($cliente == null) {
                $erros[] = "Cliente: Não foi possível localizar, Código de Importação: $clienteImportId";
            }
        }

        if (empty($cidadeId)) {
            $erros[] = 'Cidade: Código de Importação vazio.';
        } else {
            $cidade = CidadeQuery::create()->findPk($cidadeId);
            if ($cidade == null) {
                $erros[] = "Cidade: Não foi possível localizar, Código de Importação: $cidadeId";
            }
        }

        if (sizeof($erros) == 0) {
            gc_enable();
            Propel::disableInstancePooling();

            $endereco = EnderecoClienteQuery::create()->findByImportId($importId)->getFirst();
            if ($endereco == null) {
                $endereco = new EnderecoCliente();
                $endereco->setDataCadastro(new DateTime('now'));
                $endereco->setImportId($importId);
            }

            $endereco->setCliente($cliente);
            $endereco->setRua($rua);
            $endereco->setBairro($bairro);
            $endereco->setNum($numero);
            $endereco->setComplemento($complemento);
            $endereco->setCep($cep);
            $endereco->setUf($uf);
            $endereco->setPontoReferencia($pontoReferencia);
            $endereco->setContato($contato);
            $endereco->setCidade($cidade);
            $endereco->setDataAlterado(new DateTime('now'));
            $endereco->setUsuarioAlterado($usuarioSistema);

            try {
                $endereco->save();

                $status = true;
            } catch (PropelException $ex) {
                $erros[] = $ex->getMessage();
            }

            $endereco->clearAllReferences(true);
            unset($endereco);
            gc_collect_cycles();
        }

        return array('status' => $status, 'erros' => $erros);
    }

    public function servicoClienteAction(Request $request) {
        $teveErros = false;
        $retorno = array();
        $retorno['erros'] = array();

        $servicoClientes = array();
        $conteudo = $request->getContent();
        if (!empty($conteudo)) {
            $servicoClientes = json_decode($conteudo);
        }

        foreach ($servicoClientes as $servicoCliente) {
            $retornoPersistencia = $this->servicoCliente($servicoCliente);

            if (!$retornoPersistencia['status']) {
                $teveErros = true;
                $retorno['erros'][] = array_merge(array('Código ID: ' . $servicoCliente[0]), $retornoPersistencia['erros']);
            }
        }

        if ($teveErros) {
            $mensagemErros = '';
            foreach ($retorno['erros'] as $erros) {
                $mensagemErros .= implode(" | ", $erros) . "\r\n";
            }

            $filename = 'tmp/servico_cliente_log_' . time() . '.txt';

            $arquivo = fopen($filename, "w");
            fwrite($arquivo, $mensagemErros);
            fclose($arquivo);
        }

        $retorno['status'] = !$teveErros;
        return new JsonResponse($retorno);
    }

    private function servicoCliente($dados) {
        $status = false;
        $erros = array();

        $usuarioSistema = 1;
        $importId = trim($dados[0]);
        $servicoPrestadoId = trim($dados[1]);
        $clienteImportId = trim($dados[2]);
        $dataDoContrato = empty($dados[3]) ? null : DateTime::createFromFormat('Y-m-d H:i:s', $dados[3]);
        $dataDoCancelamento = empty($dados[4]) || trim($dados[4]) == '0000-00-00 00:00:00' ? null : DateTime::createFromFormat('Y-m-d H:i:s', $dados[4]);
        $codCesta = $dados[5];


        if (empty($dataDoContrato) || $dataDoContrato == null) {
            $erros[] = 'Data do Contrato: Campo não pode ser vazio.';
        }

        if (empty($servicoPrestadoId)) {
            $erros[] = 'Serviço Prestado: Código de Importação vazio.';
        } else {
            $servicoPrestado = ServicoPrestadoQuery::create()->findPk($servicoPrestadoId);
            if ($servicoPrestado == null) {
                $erros[] = "Serviço Prestado: Não foi possível localizar, Código de Importação: $servicoPrestadoId";
            }
        }

        if (empty($clienteImportId)) {
            $erros[] = 'Cliente: Código de Importação vazio.';
        } else {
            $cliente = ClienteQuery::create()->withColumn('pessoa.tipo_pessoa', 'tipoPessoa')->joinPessoa()->findByImportId($clienteImportId)->getFirst();
            if ($cliente == null) {
                $erros[] = "Cliente: Não foi possível localizar, Código de Importação: $clienteImportId";
            }
        }

        if (sizeof($erros) == 0) {
            gc_enable();
            Propel::disableInstancePooling();

            //$plano = PlanosQuery::create()->filterByServicoPrestadoId($servicoPrestadoId)->findOne();
            $servicoCliente = ServicoClienteQuery::create()->findByImportId($importId)->getFirst();
            if ($servicoCliente == null) {
                $servicoCliente = new ServicoCliente();
                $servicoCliente->setDataCadastro(new DateTime('now'));
                $servicoCliente->setImportId($importId);
            }

            $servicoCliente->setDataContratado($dataDoContrato);
            $servicoCliente->setDataCancelado($dataDoCancelamento);
            $servicoCliente->setCodCesta($codCesta);
            $servicoCliente->setServicoPrestado($servicoPrestado);
            $servicoCliente->setCliente($cliente);
            //$servicoCliente->setDescricaoNotaFiscal();
            $servicoCliente->setDataAlterado(new DateTime('now'));
            $servicoCliente->setUsuarioAlterado($usuarioSistema);
            //$servicoCliente->setPlano($plano);

            $con = Propel::getWriteConnection(ServicoClienteTableMap::DATABASE_NAME);
            $con->beginTransaction();

            $contrato = $servicoCliente->getContrato();
            if ($contrato == null) {
                $contrato = new Contrato();
                $tipoPessoa = $cliente->getTipoPessoa();
                if ($tipoPessoa == 'juridica') {
                    $contrato->setRegraFaturamentoId(3);
                    $contrato->setTipoFaturamentoId(4);
                } else {
                    $contrato->setRegraFaturamentoId(3);
                    $contrato->setTipoFaturamentoId(4);
                }

                $contrato->save();
                $servicoCliente->setContratoId($contrato->getIdcontrato());
            }

            try {
                $servicoCliente->save();

                $con->commit();
                $status = true;
            } catch (PropelException $ex) {
                $con->rollBack();
                $erros[] = $ex->getMessage();
            }

            $servicoCliente->clearAllReferences(true);
            unset($servicoCliente);
            gc_collect_cycles();
        }

        return array('status' => $status, 'erros' => $erros);
    }

    public function enderecoServicoClienteAction(Request $request) {
        $teveErros = false;
        $retorno = array();
        $retorno['erros'] = array();

        $enderecoServicoClientes = array();
        $conteudo = $request->getContent();
        if (!empty($conteudo)) {
            $enderecoServicoClientes = json_decode($conteudo);
        }

        foreach ($enderecoServicoClientes as $enderecoServicoCliente) {
            $retornoPersistencia = $this->enderecoServicoCliente($enderecoServicoCliente);

            if (!$retornoPersistencia['status']) {
                $teveErros = true;
                $retorno['erros'][] = array_merge(array('Código ID: ' . $enderecoServicoCliente[0]), $retornoPersistencia['erros']);
            }
        }

        if ($teveErros) {
            $mensagemErros = '';
            foreach ($retorno['erros'] as $erros) {
                $mensagemErros .= implode(" | ", $erros) . "\r\n";
            }

            $filename = 'tmp/endereco_servico_cliente_log_' . time() . '.txt';

            $arquivo = fopen($filename, "w");
            fwrite($arquivo, $mensagemErros);
            fclose($arquivo);
        }

        $retorno['status'] = !$teveErros;
        return new JsonResponse($retorno);
    }

    private function enderecoServicoCliente($dados) {
        $status = false;
        $erros = array();

        $usuarioSistema = 1;
        $importId = trim($dados[0]);
        $servicoClienteImportId = trim($dados[1]);
        $rua = $this->clean($this->remove_accents(trim($dados[2])));
        $bairro = $this->clean($this->remove_accents(trim($dados[3])));
        $numero = empty($dados[4]) ? 0 : $dados[4];
        $complemento = $this->clean($this->remove_accents(trim($dados[5])));
        $cep = trim($dados[7]);
        $uf = empty($dados[8]) ? 'RJ' : trim($dados[8]);
        $pontoReferencia = $this->clean($this->remove_accents(trim($dados[9])));
        $contato = $this->clean($this->remove_accents(trim($dados[10])));
        $cidadeId = trim($dados[6]);


        if (empty($rua)) {
            $erros[] = 'Rua: Campo não pode ser vazio.';
        }

        if (empty($bairro)) {
            $erros[] = 'Bairro: Campo não pode ser vazio.';
        }

        if (empty($cep)) {
            $erros[] = 'CEP: Campo não pode ser vazio.';
        }

        if (empty($servicoClienteImportId)) {
            $erros[] = 'Serviço Cliente: Código de Importação vazio.';
        } else {
            $servicoCliente = ServicoClienteQuery::create()->findByImportId($servicoClienteImportId)->getFirst();
            if ($servicoCliente == null) {
                $erros[] = "Serviço Cliente: Não foi possível localizar, Código de Importação: $servicoClienteImportId";
            }
        }

        if (empty($cidadeId)) {
            $erros[] = 'Cidade: Código de Importação vazio.';
        } else {
            $cidade = CidadeQuery::create()->findPk($cidadeId);
            if ($cidade == null) {
                $erros[] = "Cidade: Não foi possível localizar, Código de Importação: $cidadeId";
            }
        }

        if (sizeof($erros) == 0) {
            gc_enable();
            Propel::disableInstancePooling();

            $endereco = EnderecoServClienteQuery::create()->findByImportId($importId)->getFirst();
            if ($endereco == null) {
                $endereco = new EnderecoServCliente();
                $endereco->setDataCadastro(new DateTime('now'));
                $endereco->setImportId($importId);
            }

            $endereco->setServicoCliente($servicoCliente);
            $endereco->setRua($rua);
            $endereco->setBairro($bairro);
            $endereco->setNum($numero);
            $endereco->setComplemento($complemento);
            $endereco->setCep($cep);
            $endereco->setUf($uf);
            $endereco->setPontoReferencia($pontoReferencia);
            $endereco->setContato($contato);
            $endereco->setCidade($cidade);
            $endereco->setDataAlterado(new DateTime('now'));
            $endereco->setUsuarioAlterado($usuarioSistema);

            try {
                $endereco->save();

                $status = true;
            } catch (PropelException $ex) {
                $erros[] = $ex->getMessage();
            }

            $endereco->clearAllReferences(true);
            unset($endereco);
            gc_collect_cycles();
        }

        return array('status' => $status, 'erros' => $erros);
    }

    public function boletoAction(Request $request) { // não atualizar cobranças com notas já geradas ou emitidas
        $teveErros = false;
        $retorno = array();
        $retorno['erros'] = array();

        $boletos = array();
        $conteudo = $request->getContent();
        if (!empty($conteudo)) {
            $boletos = json_decode($conteudo);
        }

        foreach ($boletos as $boleto) {
            $retornoPersistencia = $this->boleto($boleto);

            if (!$retornoPersistencia['status']) {
                $teveErros = true;
                $retorno['erros'][] = array_merge(array('Código ID: ' . $boleto[0]), $retornoPersistencia['erros']);
            }
        }

        if ($teveErros) {
            $mensagemErros = '';
            foreach ($retorno['erros'] as $erros) {
                $mensagemErros .= implode(" | ", $erros) . "\r\n";
            }

            $filename = 'tmp/boleto_log_' . time() . '.txt';

            $arquivo = fopen($filename, "w");
            fwrite($arquivo, $mensagemErros);
            fclose($arquivo);
        }

        $retorno['status'] = !$teveErros;
        return new JsonResponse($retorno);
    }

    private function boleto($dados) {
        $status = false;
        $erros = array();

        $usuarioSistema = 1;
        $importId = trim($dados[0]);
        $valor = $dados[1];
        $dataDoVencimento = empty($dados[2]) ? '' : DateTime::createFromFormat('Y-m-d', $dados[2]);
        $clienteImportId = trim($dados[3]);


        if ($valor <= 0) {
            $erros[] = 'Valor: Campo não pode ser menor ou igual a zero.';
        }

        if (empty($dataDoVencimento)) {
            $erros[] = 'Data do Vencimento: Campo não pode ser vazio.';
        }

        if (empty($clienteImportId)) {
            $erros[] = 'Cliente: Código de Importação vazio.';
        } else {
            $cliente = ClienteQuery::create()->findByImportId($clienteImportId)->getFirst();
            if ($cliente == null) {
                $erros[] = "Cliente: Não foi possível localizar, Código de Importação: $clienteImportId";
            }
        }

        if (sizeof($erros) == 0) {
            gc_enable();
            Propel::disableInstancePooling();

            $competencia = CompetenciaQuery::create()->findOneByAtivo(1);

            $boleto = BoletoQuery::create()->findByImportId($importId)->getFirst();
            if ($boleto == null) {
                $boleto = new Boleto();
                $boleto->setDataCadastro(new DateTime('now'));
                $boleto->setImportId($importId);
                $boleto->setNotaScmCancelada(false);
                $boleto->setNotaScmGerada(false);
                $boleto->setNotaSvaCancelada(false);
                $boleto->setNotaSvaGerada(false);
            }

            $notaScmCancelada = $boleto->getNotaScmCancelada();
            $notaScmGerada = $boleto->getNotaScmGerada();
            $notaSvaCancelada = $boleto->getNotaSvaCancelada();
            $notaSvaGerada = $boleto->getNotaSvaGerada();

            if (!$notaScmGerada && !$notaScmCancelada && !$notaSvaGerada && !$notaSvaCancelada) {
                $boleto->setValor($valor);
                $boleto->setVencimento($dataDoVencimento);
                $boleto->setCliente($cliente);
                $boleto->setCompetencia($competencia);
                $boleto->setDataAlterado(new DateTime('now'));
                $boleto->setUsuarioAlterado($usuarioSistema);

                try {
                    $boleto->save();

                    $status = true;
                } catch (PropelException $ex) {
                    $erros[] = $ex->getMessage();
                }
            } else {
                $erros[] = "Esta cobrança $importId já possui nota, entre em contato com o administrado.";
            }
            
            $boleto->clearAllReferences(true);
            unset($boleto);
            gc_collect_cycles();
        }

        return array('status' => $status, 'erros' => $erros);
    }

    public function boletoItemAction(Request $request) {
        $teveErros = false;
        $retorno = array();
        $retorno['erros'] = array();

        $boletoItems = array();
        $conteudo = $request->getContent();
        if (!empty($conteudo)) {
            $boletoItems = json_decode($conteudo);
        }

        foreach ($boletoItems as $boletoItem) {
            $retornoPersistencia = $this->boletoItem($boletoItem);

            if (!$retornoPersistencia['status']) {
                $teveErros = true;
                $retorno['erros'][] = array_merge(array('Código ID: ' . $boletoItem[0]), $retornoPersistencia['erros']);
            }
        }

        if ($teveErros) {
            $mensagemErros = '';
            foreach ($retorno['erros'] as $erros) {
                $mensagemErros .= implode(" | ", $erros) . "\r\n";
            }

            $filename = 'tmp/boleto_log_' . time() . '.txt';

            $arquivo = fopen($filename, "w");
            fwrite($arquivo, $mensagemErros);
            fclose($arquivo);
        }

        $retorno['status'] = !$teveErros;
        return new JsonResponse($retorno);
    }

    private function boletoItem($dados) {
        $status = false;
        $erros = array();

        $usuarioSistema = 1;
        $importId = trim($dados[0]);
        $valor = $dados[1];
        $boletoImportId = trim($dados[2]);
        $servicoClienteImportId = trim($dados[3]);


        if ($valor <= 0) {
            $erros[] = 'Valor: Campo não pode ser menor ou igual a zero.';
        }

        if (empty($boletoImportId)) {
            $erros[] = 'Boleto: Código de Importação vazio.';
        } else {
            $boleto = BoletoQuery::create()->findByImportId($boletoImportId)->getFirst();
            if ($boleto == null) {
                $erros[] = "Boleto: Não foi possível localizar, Código de Importação: $boletoImportId";
            }
        }

        if (empty($servicoClienteImportId)) {
            $erros[] = 'Serviço do Cliente: Código de Importação vazio.';
        } else {
            $servicoCliente = ServicoClienteQuery::create()->findByImportId($servicoClienteImportId)->getFirst();
            if ($servicoCliente == null) {
                $erros[] = "Serviço do Cliente: Não foi possível localizar, Código de Importação: $servicoClienteImportId";
            }
        }

        if (sizeof($erros) == 0) {
            gc_enable();
            Propel::disableInstancePooling();

            $boletoItem = BoletoItemQuery::create()->findByImportId($importId)->getFirst();
            if ($boletoItem == null) {
                $boletoItem = new BoletoItem();
                $boletoItem->setDataCadastro(new DateTime('now'));
                $boletoItem->setImportId($importId);
            }

            $boletoItem->setValor($valor);
            $boletoItem->setBoleto($boleto);
            $boletoItem->setServicoCliente($servicoCliente);
            $boletoItem->setDataAlterado(new DateTime('now'));
            $boletoItem->setUsuarioAlterado($usuarioSistema);

            try {
                $boletoItem->save();

                $status = true;
            } catch (PropelException $ex) {
                $erros[] = $ex->getMessage();
            }

            $boletoItem->clearAllReferences(true);
            unset($boletoItem);
            gc_collect_cycles();
        }

        return array('status' => $status, 'erros' => $erros);
    }

}
