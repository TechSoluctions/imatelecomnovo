<?php

namespace WSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function testeAction()
    {
        $retorno = array();
        $retorno['teste'] = 'OK';
        return new JsonResponse($retorno);
    }
}
